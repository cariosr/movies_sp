
                      Una película muy interesante y original tanto en su historia como en su diseño creativo. Los actores están que se salen. Lo único que falla, en mi opinión, es que peca de ser demasiado sentimental y de sobreexplicar la situación al final de la película... Si hubiesen hecho un final más elegante y más inteligentemente intenso en vez de facilitar su comprensión e intentar hacer llorar al público de forma tan evidente, la película se llevaba mis dieses. Aún así, la recomiendo absolutamente.
        
            