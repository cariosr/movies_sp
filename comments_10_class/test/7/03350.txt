
                      Preciosa película, digna del arte de los Coen, con ese retrato de personajes, como solo ellos lo saben hacer. El protagonista genial, digno de Oscar, el ritmo de la película se hace un poco lento en determinados momentos y a veces hasta pesado, pero eso no impide que sea una obra de arte en su conjunto.
        
            