
                      Entretenida, profunda con pretensiones de serlo más (BSO demasiado acentuada y forzando enfatizar lo sensible del tema). La peli está bien, el título es el idóneo por lo increíble de que algo así pueda suceder pero está a años luz de "Mas allá de la vida" dirigida por Dios (Mr. Clint Eastwood), aunque en historias completamente diferentes.
        
            