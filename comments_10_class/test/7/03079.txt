
                      Tarantino en estado puro. Película entretenida aunque increíble, sobre todo la última hora. A partir de la llegada a Mississippi, el guión nos cuenta una batallita imposible. Imaginarse a un negro comiendo en la misma mesa con el dueño de la plantación... Lo bueno, que acaba bien. Final feliz como en los cuentos, que es lo que es esta película.
        
            