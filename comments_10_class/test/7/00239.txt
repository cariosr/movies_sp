
                      Dr. Strange (2016).
El universo Marvel vuelve al ataque; en esta ocasión con un personaje que juega más con la fantasía y la magia que otros vistos anteriormente. Podríamos decir que estamos ante una simbiosis entre "Airbender: El Último Guerrero" de Shyamalan y "Origen" de Christofer Nolan. Esto es, mucha imaginación y más efectos visuales.

El reparto, eso sí, es magnífico. Estamos viviendo una década, e inclusive un inicio de siglo con una cochura de nuevos actores de altísima calidad (nos gusten o no las películas en las que actúan). En este caso podemos disfrutar del gran Benedict Cumberbatch, a quien acompaña Chiwetel Ejiofor, la bella Rachel McAdams y la camaleónica Tilda Swinton, quien lo mismo te hace de reina malvada en Las Crónicas de Narnia como de villana déspota en Rompenieves. En la zona de los malos tenemos a uno de los daneses más atractivos del momento (según ciertas revistas), Mads Mikkelsen (serie Hannibal), y como curiosidad al acrobático Scott Adkins, quien se mueve y patea como quiere... aunque creo que no llega a pronunciar una palabra...

El director de la cinta no cambia excesivamente de registro, ya que del género de terror pasa al de fantasía, que aunque con algunos pequeñísimos golpes de humor, es bastante seria y dramática.

Raúl Cabral.
        
            