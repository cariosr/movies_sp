
                      No la puedes amar ni odiar. Es extraña, graciosa, tensa  e intenta ser emotiva. Podría darse un montón de interpretaciones de por qué todas las aves de un pueblo atacan a sus habitantes, pero creo que esa no es la idea para disfrutarla. Es mejor sentarse a verla tranquilo, y dejarse maravillar por  grandiosa técnica cinematográfica  que pone  en juego Hitchcock  en esta extraña obra suspense de culto
        
            