
                      Como siempre y aunque me de un poco de vergüenza admitirlo, he pasado un muy buen rato con este filme de acción. Algo tienen estas películas de coches que me han gustado todas (a excepción de una) de las 5. Vin Diesel en su papel de Don, tan bien como siempre. La acción y los efectos geniales. Una buena opción para la tarde de domingo.
        
            