
                      La película muestra el peor derrame de petróleo en la historia de Estados Unidos y el director Peter Berg recrea el cataclismo de ese día con una tensión insoportable y una compasión curativa.
        
            