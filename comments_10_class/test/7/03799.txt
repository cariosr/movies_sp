
                      Se trata de una historia sobre la rutina de dos amigos que muestran el lado más auténtico de una amistad, en la que el humor, maravillosamente absurdo, sustenta gran parte de la película a pesar del estado de salud de uno de ellos. Sin lugar a dudas, una película sencillamente divertida, aunque con un toque triste, que te hará empatizar con los personajes de una forma muy ingeniosa. Si quieres ver una película de netflix agradable, corta, tranquila, e incluso en la que puedas apreciar las pequeñas cosas de la vida, ésta es tu película.
        
            