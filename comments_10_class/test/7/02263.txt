
                      Interesante película que cuenta la historia delicada de las victimas de las FARC. Recomendable por el tipo de tema que plantea y la realización de la historia basada en hechos reales. Cruda y real de cómo una persona mueve cielo y tierra por cuidar de su familia y de un pobre bebé enfermo que es hijo de la exsecuestrada Clara Rojas. 

Excelente interpretación de Luis Tosar en un nuevo registro y un cuidado acento colombiano bien estudiado y buen ritmo, suspense, documentación y análisis de este tema tabú en países sudamericanos.
        
            