
                      El Secreto de Marrowbone (2017).
El debutante en la realización de largometrajes para la gran pantalla, Sergio Sánchez (apadrinado por J.A. Bayona), nos trae esta cinta en la línea de Los Otros y recordándonos bastante a El Sexto Sentido de Shyamalan. 

Bien facturada y sobre todo, muy sobria, la película nos narra la oscura historia de una  familia en la ausencia de su progenitor. Una vez más, la casa que habitan nuestros protagonistas se convierte en un elemento más del reparto, formado, por cierto, por varias jóvenes promesas anglosajonas (destacando a Anya Taylor-Joy, habitual del género de terror y Charlie Heaton, conocido recientemente por su participación en la serie Stranger Things).

Como quiera que la crítica- comenzando por el ignorante Carlos Boyero- la trata de mal a muy mal, no puedo más que recomendarla, ya que gracias a su ambientación, interpretación y giros argumentales se hace digna de ver. Más aún, siendo la ópera prima de su realizador en el cine no televisivo. 

Raúl Cabral.
        
            