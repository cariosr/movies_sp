
                      Es una de esas películas que no destaca ni por su gran argumento ni su innovadora animación. Pero, careciendo de estos dos factores, consigue transmitir el mismo mensaje que por años han querido transmitir otras productoras de animación: corre tras tus sueños.

Una buena película para verla juntos en familia, sobretodo si hay pequeños en casa. Como ya he dicho, no es una gran película, pero si todavía no lo habéis echo y queréis darle una oportunidad, no os decepcionará.

Tierra Cero Blog
        
            