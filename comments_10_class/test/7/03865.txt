
                      “La Sombra de la Ley” (2018).
-‘Yo no me fío de los hombres que no tienen ideales.'
-‘Tampoco te fíes de los que sí los tienen.'
Dani de la Torre vuelve a contar con el infalible Luis Tosar y la bella Michelle Jenner para narrarnos esta interesante historia ambientada magistralmente en la Barcelona de 1921, año convulso donde los haya, y en la que nos acerca a los problemas laborales y de corrupción política y policial de aquella época... os suena, ¿verdad?... no era tan diferente a nuestro tiempo, sin duda.

Al policía venido de Madrid a echar una mano a la corrupta brigada de información barcelonesa en el caso de un  importante atraco en la capital catalana -Tosar- se le une una activista de ideas anarquistas, Jenner. Como secundarios destacan un sorprendente y desagradable Ernesto Alterio (estupendamente caracterizado), y el gaditano afincado en Sevilla cerca del barrio de S. Pablo, Paco Tous, quien encarna a un importante sindicalista. Un hombre justo y contrario al uso de la violencia, que nos da una lección de comportamiento con su honestidad e incorruptibilidad.

La factura en general de la cinta es poco menos que impecable: unas interpretaciones magníficas, una banda sonora preciosa y muy ajustada al ritmo de las escenas, un vestuario perfecto, y hasta unas escenas de acción y de lucha dignas, pero lo que en mi opinión destaca sobre todo en el largometraje que nos ocupa es el diseño de producción y los efectos especiales. Como dijo alguien una vez en referencia a los efectos de la película “Black Hawk Derribado”, unos buenos efectos especiales son aquellos que no se notan. Y en "La Sombra de la Ley" los efectos visuales no se notan.
Aunque el filme pueda llevarnos a recordar la obra maestra de Brian de Palma "Los Intocables de Eliott Ness", o incluso el extraordinario thriller de los hermanos Coen "Muerte Entre las Flores", la producción (totalmente española) que nos ocupa no llega a tanto, pero es notable indiscutiblemente.

“Nena, la vida es de todo menos justa."
Para concluir creo que puedo decir, sin miedo a equivocarme, que la fidelidad histórica de la obra de Dani de la Torre es muy ajustada; en ella hace una crítica al férreo capitalismo y a la corrupción policial y política de la época precedente al golpe de Estado de Primo de Rivera, y antesala de la II República Española, donde mientras los hombres de bien luchaban por sus derechos, el poder corrupto de los ricos y poderosos hacía lo que fuera necesario, por indeseable que fuera, para conseguir sus objetivos.

Raúl Cabral.
https://www.filmaffinity.com/es/film939356.html
        
            