
                      Cuando todo parece petrificado por el hielo, renace un corazón dormido que latía allí al fondo. Reaparece un atisbo que se mantiene en la memoria a pesar de los cuarenta años pasados. Dos hermanos que son humanos y que tienen golpes escondidos que no aparentaban para nada. Ambos caminan reconciliados por un objetivo que los une por unos lazos ancestrales.
Una película mínima, sencilla, sobria y veraz. Todo ello quizás un poco excesivo pero necesario si no se quiere traicionar el alma de una ruralidad de la que proviene el mismo director. No le pida empatía, ni un animado desenfreno. Dele paciencia y confianza. El resultado final vale la pena. Un retrato contundente que le dejará un buen sabor por su autenticidad. (7/10)
        
            