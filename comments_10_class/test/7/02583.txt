
                      Excelente film de Almodóvar. Banderas y Anaya desarrollan una de sus interpretaciones más logradas. Nos encontramos ante el Pedro Almodóvar más intimista y personal. Es una película que te atrapa desde el primer instante. Chapeau!!!
        
            