
                      Gravity es una buena película a la que se le tiene que tener paciencia, si sos por ahí de esas personas que no aguantan las películas que no tienen mucho dialogo no vas a durar viendo esta peli. Como dije, hay que saber llevarla. Gravity nos trae una historia nunca antes vista, tiene temática espacial y no es de ciencia ficción, cuenta una aventura que tranquilamente podría ocurrir en la vida real, obvio, todas las ficciones tienen su toque de alejamiento con la realidad. Personalmente no me gusto nada Sandra Bullock, yo como director hubiese elegido a Amy Adams, o apostando mas Julia Roberts. En cuanto al papel de Clooney, me agrado, si bien no tiene mucha participación en el film lo poco que hizo sentó bien a la historia.
Los efectos especiales son grandiosos, tanto si la ves en el cine como si la ves en tu casa, la dirección de arte estuvo bien, tampoco se podía hacer mucho mas.
El guion no es nada de otro mundo pero tampoco es como los de Fast And Furious, que esos si dan asco.
Resumiendo, mirala, no la tengas como prioridad a ver, pero tampoco la rechaces.
        
            