
                      A pesar de no ser del todo lógica y pecar de enormes lagunas de guión, las secuencias de acción están muy logradas, escenas brutales y gore muy satisfactorias (estoy enfermo), y Aja tiene un manejo de la cámara magnífico. Buen remake de la original de Wes Craven.
        
            