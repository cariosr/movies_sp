
                      'Vida y amores de una diablesa' es una comedia romántica dirigida por Susan Seidelman, directora de varias serie de televisión, entre ellas Sexo en NY (1ª temporada).

Ruth Patchett (Roseanne Barr) es una mujer no demasiado agraciada, que está casada y con dos hijos. Un día, en una fiesta, su marido conoce a la famosa escritora de novelas románticas Mary Fisher (Meryl Streep). Ambos se enamoran casi de inmediato y empiezan una aventura.
Ruth es consciente de la situación, y decide arrebatarle la felicidad a su marido Bob (Ed Begley Jr.).

Meryl Streep está fabulosa, como de costumbre. Sabe cómo hacer una interpretación cómica más que satisfactoria.
Sin embargo, la auténtica protagonista de la historia es Roseanne, que con su personaje se luce a la hora de planear su venganza.
Es una películas simple, pero muy entretenida e ingeniosa, que cuenta con muchas situaciones cómicas geniales.

La banda sonora de la película es exquisita. Contiene canciones de artistas de la talla de Elvis Presley y una versión moderna del mítico 'I Will Survive' entre otras.
        
            