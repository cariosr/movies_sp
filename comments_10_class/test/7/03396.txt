
                      Una continuidad de la 1 con los protagonistas ya más adentrados en la adolescencia y un Chris D'amico (The Motherfucker, ex Red Mist) que buscará vengarse de Kick Ass tras la muerte de su papá. Una legión aficionada de supervillanos vs una legión aficionada de superhéroes, grandes efectos, escenas muy sangrientas y sádicas al estilo "Kill Bill", su cuota de comedia y el afianzamiento de la relación entre el protagonista y la heroína "Hit Girl". Entretiene y está a la altura de su antecesora, la cual recomiendo ver previamente.
        
            