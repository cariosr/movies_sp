
                      Yo jugué a los juegos y hoy fui a verla siendo que acá, en Argentina, se estrenaba hoy. 
La verdad es que tiene muchos aspectos positivos como negativos. Prefiero empezar con los positivos que, en mi opinión, fueron la banda sonora (de la cual me enamoré), la fotografía (me fascinaron esos planos que se hacían a la ciudad de España durante la inquisición), los actores que logran interpretar a sus personajes bastante bien, los efectos especiales, y me encantó el animus; me gustó que en vez de estar recostado, la máquina te permita moverte y realizar las acciones de tu antepasado, permitiendo darle más realismo a éste. 
Por otro lado yo encontré un sólo aspecto negativo pero que perjudica enormemente a la película...la historia. No me refiero a que el concepto de ésta sea mala pero el director y los guionistas no lo supieron llevar bien. Llega un punto en que la narrativa se pierde y lo único que se ve son peleas. 
Sé que la película no se compara a los juegos, que logran contarte una historia y la desarrollan junto con los personajes (me refiero a los primeros juegos), pero creo que podrían haberlo hecho mejor.
        
            