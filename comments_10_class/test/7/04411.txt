
                      Una película con un trasfondo muy utilizado en las últimas dos décadas, pero con una forma un tanto distinta, esto es la historia de "Dos homosexuales que deben afrontar los obstáculos sociales y/o familiares  para concretar su amor". Películas de este tipo siempre funcionan, más cuando el director de la cinta es un reciente ganador del Oscar con una película de la misma linea y cuando las actuaciones de Rachel McAdamds y Rachel Weisz  son espectaculares. Sin embargo, la cinta no aporta nada nuevo al género, aparte  de su contextualización judía.
        
            