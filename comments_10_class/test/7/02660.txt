
                      Sully (2016).
El incombustible responsable de Malpaso Productions dirige en su última película al inconmensurable Tom Hanks, envejecido para la ocasión, de manera sobria como nos tiene acostumbrados, y sin un movimiento innecesario de cámara. 

Bien filmada, aunque sin excesiva pasión, nos cuenta la historia del piloto héroe del accidente aéreo  (o como el comandante "Sully" Sullenberger gusta decir: el 'aterrizaje forzoso') del Río Hudson de hace 7 años con una narrativa en plan película documental dramatizada. 
Sin cansarnos en ningún momento (solo dura hora y media), Tom Hanks es un placer verlo desenvolverse por la gran pantalla apoyado por un secundario de lujo, Aaron Eckhart, quien suele acertar en los proyectos que lleva a cabo.

Recomendable para cualquier apasionado, seguidor o amante del cine de Eastwood, y de quien quiera pasar un buen rato sin grandes sobresaltos, violencia gratuita o acción desmedida.

Raúl Cabral.
        
            