
                      Margin Call, es una buena película centrada en el dialogo de cada personaje y como va reaccionando cada uno a los distintos acontecimientos que se van sucediendo. Una buen film para tratar de entender como comenzó la crisis financiera en EEUU y que después se ha extendido al resto del mundo. No es una película demasiado intensa ni tiene momentos de tensión lo cual puede provocar que a los que no les interesen los  términos económico-financieros pueda
 acabar aburriéndoles.  Una película recomendable pero si el único objetivo de ver Margin Call es entender la crisis en youtube encontraras vídeos de mucha más corta duración que explican lo mismo y con los que te divertirás mucho más.
        
            