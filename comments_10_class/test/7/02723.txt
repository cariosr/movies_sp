
                      Película de Oscar que nos cuenta una dura historia de un niño y una madre secuestrados y criados en una habitación, tenemos a un niño y a una madre que hacen un papel excelente, pero el niño levanta la película en cada segundo. La película es lenta con partes un poco pesadas, con unas buenas actuaciones, un niño-actor que dará mucho que hablar y una historia muy dura sobre un secuestro.
        
            