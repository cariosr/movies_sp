
                      Entretenida y disfrutable, con unos efectos especiales bastante solventes y una aventura para ver en familia, "Pesadillas" es de esas películas que puede gustar a todas las edades y que podemos disfrutar tod@s junt@s. La experiencia en 3D aumenta, si cabe, la diversión y el entretenimiento.
        
            