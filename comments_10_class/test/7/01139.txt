
                      Todo gira en torno a los misterios de la familia y los secretos de un pasado oscuro. Suspenso bien logrado con algunas escenas de sobresaltos y un giro rotundo, quizás algo previsible, durante el desenlace. Una muy buena historia del estilo de "El orfanato" con gran ambietacion lúgubre y excelentes actuaciones. La recomiendo sin dudar.
        
            