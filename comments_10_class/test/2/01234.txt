
                      Tiene todos los ingredientes para ser un peliculón de terror; Clima frío como un tempano, ambientación magnífica y un caserón terrorífico.
Pues bien, no lo han podido hacer peor!
El terror brilla por su ausencia y el final sin sorpresa.
        
            