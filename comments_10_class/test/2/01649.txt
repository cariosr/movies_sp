
                      Pésima... demasiado irreal; semejantes ternuras en una época en la que todo se trataba de sobrevivir? Que un adolescente no sepa comerse una lombriz o un insecto? (demasiado drama para hacerlo), lobos “tamaño perro”?
Concedo que el juicio depende la expectativa que cada quien tenga, pero al que espere una aventura con elementos de coherencia y realidad, no se la recomiendo.