
                      Con la trilladísima excusa de una despedida de soltera, el cine español vuelve a hacer un intento en el humor adolescente. Ese que se le resiste tanto, pues la comedia con jóvenes promesas no tiene muy buena aceptación en nuestro país.

Humor para adolescentes en la que una vez más tenemos a Úrsula Corberó, esa actriz que en la academia de arte dramático solo asistió a clase de cómo interpretar a pijas infames, pues no ha hecho más que meterse en la piel de estos personajes toda su carrera. Un guión escrito por un grupo de suscriptoras a la Súper POP y despechadas por el cierre de su revista y ya tenemos otro bajo producto que no serviría ni para el late night de Factoría de Ficción. Pues si pensabas que “Fuga de cerebros 2“, “Mentiras y gordas” y “Xp3D” habían batido el récord de películas adolescentes malas hasta decir basta, aquí llega “Cómo sobrevivir a una despedida“. Con una media de 2 a 4 chistes cutres y absurdos por minuto, lo peor no es que no tenga gracia ninguna, sino que quiera abarcar todo lo que a los jóvenes les gusta y les obsesiona y lo quieran llevar al terreno del humor. Patético.

Lo mejor: su banda sonora (y por momentos).
Lo peor: todo en su conjunto.
        
            