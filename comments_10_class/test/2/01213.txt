
                      Coincido con la crítica de SesaCine, la forma en que la película minimiza la importancia de Margaret Thatcher es decepcionante; casi desaparece los grandes logros que obtuvo La Dama de Hierro, los numerosos obstáculos que enfrentó y sólo destaca su decadencia, como si no tuviera derecho a envejecer o quisiera borrar su participación en la historia, me apena que Phyllida LLoyd deteriore así los éxitos de esta heroína.
        
            