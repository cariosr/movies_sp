
                      Una pintora impresionista de la vida tranquila de una aldea medieval de Calabria, de la cuarta dimensión. El ciclo de los cuatro elementos, que no obedece mas que a una ley: la de la naturaleza. Además, para mí la Calabria era un sinónimo de pizza. Conjuga paisajes magníficos, tradiciones y aburrimiento mortal: el silencio, un árbol, las cabras... Contemplativo, desconcertante, fascinante, burlesco, singular, mágico.
        
            