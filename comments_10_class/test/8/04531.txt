
                      Muy buena peli con el estilo propio de Tarantino. Un trio de actores excelentes, una  trama que es capaz de atrapar y una realización de calidad hacen de esta película un peliculón. Si, es cierto, demasiada salsa de tomate que no aporta nada a la peli pero es un capricho del director. Que le vamos a hacer, nadie es perfecto.
        
            