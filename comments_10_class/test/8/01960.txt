
                      Original y entretenida. 
Fui al cine con muy bajas expectativas por lo leído en otras reseñas y puede que eso mismo hay hecho que la disfrute más. 

Mantiene el interés a lo largo de la película. Los decorados y los efectos especiales están a muy alto nivel y los actores están a la altura. 
Quizá alguno de los detalles de la trama en la parte final de la película no son del todo de mi agrado, pero para mí es una película que cumple con su cometido: entretener.
        
            