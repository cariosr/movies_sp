
                      Bajo la Misma Estrella es la adaptación cinematográfica del best seller de John Green. Está dirigida por Josh Boone, director de Stuck in Love.

La película es conmovedora desde un principio, en el que se presenta a la protagonista, Hazel Grace, que sufre cáncer de pulmón.
Probablemente, lo más remarcable es que no se trata de una historia idílica de lucha contra el cáncer, y esto lo deja muy claro la protagonista.

Algo a favor de está adaptación es lo mucho que se parece a la novela a partir de la cual ha nacido, y que los actores saben captar la esencia de los personajes. Todo esto debido, con cierta seguridad, a la supervisión de John Green durante el rodaje.

Willem Dafoe, ya acostumbrado a hacer de malo, realiza una actuación sobresaliente, aunque breve.

Como cualquier otro drama, este no deja indiferente a las lágrimas, y deja un sabor de boca amargo.
A pesar del dolor y el sufrimiento que padecen los protagonistas, entre ellos surge una historia de amor perfecta.

La historia deja una lección esencial que todo el mundo debería apreciar, y es que: "la vida no tiene que ser perfecta para ser extraordinaria".
        
            