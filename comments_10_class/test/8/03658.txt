
                      "Alcanza un equilibrio ideal entre la acción salvaje, el humor desechable, un estilo refrescante y, quizás lo más impresionante, una conciencia indiferente de su propia modesta importancia en su gran esquema de las cosas."
        
            