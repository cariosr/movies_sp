
                      Hola amigos de Sensacine, aquí tenéis mi crítica de el Regreso de la Momia. Es una película que no te recomiendo ver si no has visto la primera de la saga pues hay algunos detalles que no entenderás como por ejemplo quien es Ardeth Bay y qué hace ahí. Tiene una buena trama que se mantiene firme, aunque se trata de la típica película de arqueologos como Indiana Jones, tenemos al arqueólogo fuerte, la chica guapa, el amigo inútil y gracioso y el enemigo con el gran ejército. Tiene algunos pequeños fallos casi imperceptiles, pero fallos son y en las escenas del Rey Escorpión, este parece sacado de un videojuego. Pero esto no significa que la película tenga malos diseños, las momias, el diseño del dirigible de Izzy, y el ejército del Rey Escorpión son muy realistas. Por último, Imhotep, al principio de la película nos lo presentan con un aspecto similar al de la momia y debería de ser así, pero a los pocos minutos aparece con el aspecto humano que le quita el terror y poder que infundía este personaje. Concluyendo, si eres fan de las películas de Indiana Jones te gustarán estas películas.

El personaje que más destaca es Jonathan Carnahan (John Hannah), un personaje tan inútil como cómico, un personaje que desearás que siempre tenga más escenas y diálogos.

"Si un hombre no abraza su futuro"
        
            