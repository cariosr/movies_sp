
                      Una película de animación con guión clásico con una animación preciosa en stop motion marca de la casa (los mundos de Coraline y el alucinante mundo de Norman).
Lo más importante y sobresaliente es sin duda su apartado artístico: El Stop motion  brilla cuando se hace bien y denota un gusto artesanal de recuerda a tiempos pasados. Este retorno al pasado se nota también en los temas de la película que son categóricamente clasicotes. Quizás demasiado, ya que el guión mete la fantasía nipona a la perfección pero no se atreve a salirse del modelo de cuenta tradicional.
Con todo, son pequeñas pegas para la que sin duda es la mejor obra del estudio Laika.
        
            