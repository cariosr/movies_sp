
                      En este película plasman a lo que nosotros llamamos "ordinario". La vida de los personajes es ordinaria, sus problemas son ordinarios, sus trabajos son ordinarios. Y justo lo que hace tan especial a esta película es esa vibra de transmita de que todo esta donde debe de estar y nos hace sentir como en casa porque todos nosotros han vivido algo como lo que pasaron nuestros protagonistas, un amor de verano, pero no hay que olvidar que solo es de un verano. Uno no espera que sea tan ordinaria la película y eso la hace extraordinaria.
        
            