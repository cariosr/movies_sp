
                      Divertida y entretenida. Ideal para ir al cine. Los personajes están a la altura, como nos tiene acostumbrados. Un argumento que sin ser excelente nos mantiene expectantes ante el transcurso de los acontecimientos. Ideal para conocer el personaje creado por Herge y para los fans.
        
            