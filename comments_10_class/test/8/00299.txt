
                      No sé cuántos guionistas y directores, con más o menos acierto, han hecho de las suyas para exhibir el periodismo al desnudo. Pero una cosa tengo clara, los films de este palo que más me han impactado no son complacientes con la moral simplificada que acompaña a todo desnudo y que tanto gusta en Hollywood. A muchos no les basta el obsceno romanticismo de la mirada. Quienes no se conforman con el espejo del alma lo hacen añicos arrojándose a la búsqueda de la verdad, sin miedo a airear una atmósfera enrarecida o producir el sofoco por un olor incómodo.
Spotlight, Todos los hombres del presidente, Network, Nightcrawler.
Acomódense, así es el periodismo visto desde una butaca.
        
            