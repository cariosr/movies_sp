
                      La verdad es que me sorprendió gratamente. Una película muy bien hecha, con buenas actuaciones y una trama estupenda. Sólo le pondría un pero a la revelación final que parece un tanto increíble. Una buena película de suspense, muy recomendable.
        
            