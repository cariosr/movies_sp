
                      Una película muy interesante, de guion cuidado en un tema digamos dificil de abordar. Se muestra como una afinada crítica hacia la política imperialista de los Estados Unidos y es bastante directa, conteniendo grandes dosis de realidad que pueden molestar a más de uno: muerte, injusticia, causas equivocadas, etc.
Matt Damon una vez más nos ofrece una gran interpretacion es este género en el que parece haberse especializado. La mayor parte de los actores están geniales en su papeles, realizando interpretaciones de un modo absolutamente creible.
Con grandes dosis de acción e intriga logra mucho más que captar la atención, te hace refelxionar sobre los problemas de actualidad de una buena parte del mundo, a los que muchas veces permanecemos indiferentes. Muestra aspectos reales de una guerra que todos seguimos de algún modo pero que no nos acaba de llegar.
        
            