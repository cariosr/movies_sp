
                      Teniendo en cuenta las maquinaciones detrás de escena que se sucedieron durante la realización de Superman II (con el director original Richard Donner despedido, habiendo rodado alrededor del 70% del guión, para ser reemplazado por Richard Lester), el producto final debería haber sido apenas un desorden coherente. En cambio, de alguna manera, Superman II es una de las películas de cómics más emocionantes jamás realizadas.
        
            