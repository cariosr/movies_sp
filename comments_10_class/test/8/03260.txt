
                      Nunca me he considerado un fan aférrimo de la saga Star Wars, no recuerdo todos y cada uno de los nombres de los personajes, seres, armas y naves, pero puedo decir que he visto las 6 películas prévias más de una vez. Y es que me crié con las reposiciones en televisión de la trilogía original, por lo que les tengo un cariño especial, y la segunda trilogía me pilló en plena adolescéncia, aunqué estas últimas nunca me convencieron.

Cuando vas al cine a ver una pelicula de Star Wars, es difícil no ir con algún tipo de prejuicio o idea desde casa. Con un anhelo de que sea una pelicula digna de la saga, una continuación de las peliculas originales que te transporte a ese universo muy, muy lejano. Y creo que lo han conseguido con la nueva entrega "El despertar de la fuerza", una vez empieza el film, te atrapa y no puedes apartar la vista de lo trepidante que se vuelve la película. 
Si habeis visto las anteriores entregas de la saga no os costará identificar los innumerables guiños a la trilogía oríginal, disfrutaréis de reencuentros con aquellos míticos personajes y "muppets", eso sí un poco desfasados ya pero debían aparecer en esta entrega..

Además la ejecución técnica y artística es impecable, claro está que no gustará a todos, pero a mi me parece que han acertado, pues tiene la esencia de las películas originales. Prefiero no desvelar ningun fragmento ni linea argumental por que ya sabemos que es mejor descubrirlo viendo la película y hacer nuestras propias congeturas y averiguaciones.

Yo como aficionado al cine he pasado dos horas bien entretenidas y me voy con buen sabor de boca al ver que La fuerza de la saga, al fin, ha despertado.
        
            