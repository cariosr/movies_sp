
                      Hacía tiempo que no me reía tanto y tan a gusto con una película, la originalidad y frescura del guión es genial, jugando con las creencias religiosas y el ateísmo, pero sin caer en la crítica y el menosprecio del adversario, simplemente presentando a una familia original y creando una situación tan divertida entre todos sus protagonistas. Sencillamente genial.
        
            