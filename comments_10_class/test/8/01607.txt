
                      La vi porque era lo más decente de las sesiones que quedaban en ese cine 
y la verdad es que me sorprendió muy gratamente. Tiene un gran guión y hace que mantengas la atención durante las más de dos horas de película. Recomendable.
        
            