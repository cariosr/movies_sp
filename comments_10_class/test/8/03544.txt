
                      Boyhood narra la historia de Mason, la vida de un niño de seis años hasta que llega a la universidad, y la película es básicamente el transcurso del tiempo, como bien dice su título, “Momentos de una vida”. El film no narra ninguna historia en particular, no hay un objetivo claro que se vaya a resolver, sino los acontecimientos más importantes de la vida de un adolescente.

Como curiosidad mencionar que el director Richard Linklater rodó la película durante 12 años, cada año durante este periodo utilizaba un par de semanas para volver a conectar con su proyecto Boyhood. Particularmente me parece un dato curioso pero que aporta poco al resultado final de la obra, si acaso algo de naturalidad al encontrarnos siempre al mismo actor pero no es un cambio sustancial al valor de la película.

Y como valoración de la película que es lo que realmente interesa, la tengo en gran estima porque no es el estereotipo de película al que muchas veces estamos acostumbrados, es algo nuevo y eso en el cine no es poco, además es una historia que te atrapa y sus casi tres horas de duración no la hacen una película interminable. Aunque para la persona que haya estado esperando doce años para poder verla quizá si se le quede corta.

En resumen, “Momentos de una vida”, más que recomendable.
        
            