
                      La vigésimo primera película, basada en el espía de Ian Fleming, Casino Royale vuelve a las raíces de James Bond (Craig) y se enfoca en su primera misión como agente de Double-O. Mads Mikkelsen interpreta al villano Le Chiffre, un banquero sospechoso de terroristas de todo el mundo, a quien Bond debe hacer mejor en un juego de póquer de alto nivel en el Casino Royale de Montenegro con la ayuda del representante del Tesoro Vesper Lynd (Verde).

Daniel Craig fue, por las razones más bien delicadas de ser rubio y "feo", fue una elección extrañamente controvertida para el papel. Concedido, su aspecto más herniado lo diferencia de Connerys y Brosnans del mundo, pero para una reinvención exitosa de la franquicia de larga duración (44 años), esta fue una excelente pieza de casting. Lejos de reducir la serie, Craig ayuda a sacar lo mejor de su número durante años.

Casino Royale's Bond es un hombre diferente en otro sentido. Bajo la guía de Martin Campbell ( Goldeneye ), nuestro "héroe" es un personaje menos simpático pero mucho mejor para él. Lo que puede haber parecido ser amenazas ociosas del escritor Paul Haggis acerca de que este sea un Bond más realista y realista no fue una exageración. Cuando la película comienza con un violento flashback en blanco y negro del agente que hace que sus dos muertes sean necesarias para el estado de Doble-O, sabemos que los cineastas hablan en serio.

Su próxima pista se puede encontrar en el perfil del primer participante de Celebrity Big Brother para abandonar el programa.

Por más fresco que esté, Casino Royale no elude la entrega de los elementos vitales de la franquicia: la acción para uno es de primera categoría. Después de la apertura animada estándar (respaldada por un tema subestándar, la mayoría de las audiencias nunca han oído hablar de Chris Cornell y tampoco querrán volver a hacerlo), se produce una escena de persecución de sitios de construcción verdaderamente apasionante que nos coloca firmemente en el territorio 007. Mientras tanto, ausentes están Q y sus artilugios y el coqueto Moneypenny.
        
            