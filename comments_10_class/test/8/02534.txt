
                      Una comedia muy divertida en la que el actor protagonista, el infravalorado Brendan Fraser no nos defraudada, como siempre borda el personaje. También la actriz Brooke Shields, que no se prodiga mucho últimamente, ofrece una de sus mejores actuaciones en décadas. Es más, me atrevería a decir que no he visto una buena actuación suya desde La pequeña, la película de Louis Malle que la lanzó a la fama cuando apenas era una niña. 
El caso. En pata de guerra es disparatada y muy ágil, me ha encantado, desde el título, con la tachadura, me parece muy oríginal hasta el final. 
Excelente para pasar una tarde de este caluroso verano en el cine con los niños.
        
            