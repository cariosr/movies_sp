
                      Para Hollywood, el verano es cada vez más la temporada de la apuesta de gran presupuesto. Las devoluciones de Batman pueden ser la apuesta de taquilla más segura del año, pero cuando superas la comercialización de saturación de la película, es difícil no notar que esta vez no hay ningún Joker en la baraja.

 Ocasionalmente ocurre que la verdadera medida de la contribución de un gran actor a una película original solo se puede sentir cuando está necesariamente ausente de la secuela. Jack Nicholson, el alma exuberante y satánica de Batman, hizo su extravagante salida como el Joker que realmente era salvaje, dejando al director Tim Burton con un agujero enorme que no ha podido llenar. El choque de Batman y el Joker permitió que Michael Keaton y Nicholson nos dieran una consideración del mal y el mal sabiamente sardónica y tristemente conocedora. En el pozo de corrupción de Gotham, la línea entre los dos estaba borrosa y cambiando.
        
            