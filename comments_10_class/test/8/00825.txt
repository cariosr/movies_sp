
                      Si bien hay momentos en que quizás la sensiblería esté un poco enfatizada, hay que reconocerle al maestro Spielberg la capacidad de hacer una combinación de blockbuster con mucha épica, de factura impecable, con una bonita fotografía, una ambientación estupenda y una historia que te atrapa y te hace disfrutar de principio a fin
        
            