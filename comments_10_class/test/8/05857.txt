
                      Una ácida crítica a la política internacional cargada de palabrotas, malentendidos y humor inglés. En ocasiones resulta demasiado ágil y vulgar, pero los diálogos y las soberbias actuaciones lo compensan.
        
            