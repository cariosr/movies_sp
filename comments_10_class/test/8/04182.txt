
                      Una película hecha con mucho cariño por su director donde cuenta la historia de dos quinceañeros de procedencia muy distinta que no se terminan de encontrar agusto con las personas que les rodean hasta que se encuentran y lo que empieza siendo una amistad termina siendo algo más.  Amor, amistad e inmigración se entrelazan en esta historia.
Una película rodada a saltos entre distintos momentos de la historia para empezar dejando un poco descolocado al espectador (intentando transmitir la sensación de confusión que vive el protagonista Rafa) y finalizar uniendo todos los fragmentos en una única historia.
Se trata de la última película finalizada por Alex Ángulo, que hace un papel pequeño de forma desinteresada y como siempre lo borda.
Para los Bilbainos (Bilbao es la ciudad donde está rodada) se nos hace divertido ver nuestra ciudad y reconocer los distintos lugares donde se sucede la película.
Para los que no conocen Bilbao o lo conocen poco, verán un Bilbao muy diferente al que se publicita en las oficinas de turismo, casi desconocido, mostrando un Bilbao de barrio muy distinto al "nuevo Bilbao" surgido al rededor del Gugenheim.

Si quieres ver un cine español distinto, no dudes en ir a verla y ayudar a que producciones de este tipo puedan ver la luz en el futuro.
        
            