
                      [spoiler]
Me ha parecido una película genial, de las que haces que te comas la cabeza. Pero si pasan 25 años desde 1989 estaríamos en 2014, y aparecen coches con matrículas posteriores a esa fecha (empezadas por J y K), es un pequeño fallo a mi modo de ver. Aparte de eso, nos queda la duda de si en la dimensión final, David le ponía los cuernos a Vera tal y como se los ponía a la chica con la que estaba en la otra dimensión (yo creo que no y mi novia dice que sí).
        
            