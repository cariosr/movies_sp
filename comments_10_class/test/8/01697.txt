
                      Un mas que interesante film de terror, que acierta de lleno en hacer una perfecta mezcolanza del cine de terror más clásico con lo mejor de las últimas cintas de un decadente género. Angustiosa y vibrante, te mantendrá pegado a la butaca. Una grata sorpresa.
        
            