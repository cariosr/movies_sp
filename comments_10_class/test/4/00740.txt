
                      ¡Por Dios, Jesús y todos los santos junto a Satanás, el demonio y toda su tropa unida! ¿Qué le están haciendo a la herencia recibida desde Drácula hasta nuestros días y mucho antes?
De normal, en estos casos, cuando una película es mala y no hay por dónde cogerla escribo sobre sus carencias, debilidades y ausencias, todos aquellos "peros" que la hacen retroceder a un lugar tan ínfimo, penoso y alejado de la esperanzadora línea de salida.
En este caso, la lista es tan larga -argumento ridículo, payasada de guión, cutres diálogos, protagonistas desesperantes, interpretaciones ridículas, ñoño aroma pueril y sigue, sigue, sigue...-que he decidido mencionar lo que vale la pena para acabar pronto con esta tortura.
Mmmmmm, ummmmm..., estoy pensando, mmmmmm, mnnnnnnn, ummmmmmm, ¡no me deis prisa!, unmmmmmm..., mejor lo dejo.
Un "Crepúsculo" ridiculizado, un "Harry Potter" mancillado, un "Sensación de vivir" llevado a la máxima tontería y estupidez, frases tontas, conversaciones infantiles, pijería cutre y barata, romances que no rozan el aire y un ambiente pijoterro, garrulo, pésimo y de mal gusto que ofende todo lo que pretende insinuar y a aquellos a los cuales se atreve a copiar.
Es que ni siquiera por el lado de la risa, la diversión y la coña ¡da para nada!
Ya "Buffy, la cazavampiros" antes de que se pusiera de moda la sobreexplotación de colmillos, sangre y mordeduras por doquier de vampiros y demás ésta daba la talla con sobriedad, elegancia y decoro.
¡Ya les gustaría a éstos contar con el seductor y atractivo David Boreanaz  y su buen hacer como colega, enemigo o lo que fuera!
Es la primera entrega de la saga escrita por Richelle Mead de la que no tengo ni idea.
Como espectadora que ve una película, ésta es un fiasco total, deprimente y dolorosa por su constante -no mejora ni queriendo- absurdidad e insensatez.
No se que me da más miedo que ésta fantasmada sea culpa exclusiva del director, guionista y demás participantes o que los personajes del libro, su texto y contenido sean tan tontos, tristes y patéticos como parecen y, por lógica consiguiente, debamos esperar la agonía y asfixia de la continuación de dicha saga.
¡Por Lucifer, el diablo y todos sus amigos que sea culpa de Daniel y Mark S. Waters!
        
            