
                      Sin duda tendrá mérito desde el punto de vista técnico porque refleja con crudeza y realismo (?) el horror de la guerra, pero artísticamente no aporta nada. Pim, pam, pim, pam, los recursos de siempre. Dame un buen documental sobre este chaval interesante y no este larguísimo largometraje... No necesito ver ratas ni cuerpos destripados y mutilados en el campo de batalla para conocer la proeza de este objetor de conciencia, adventista del séptimo día, entre heroico e iluminado.
        
            