
                      En realidad, hay muchas similitudes entre esta tercera entrega en la serie de películas de Superman y la tercera entrega de ese otro héroe de la pantalla de DC Comics, Batman. Para empezar, ambos están entre los peores de la serie.

Al igual que en Batman Forever, diez años después, Superman III tiene las cosas en mal estado por otro comediante (probablemente también contratado en un intento desesperado de ganar mucho dinero e inyectar vida en lo que se estaba convirtiendo rápidamente en una franquicia cansada). En Batman Forever tenían a Jim Carrey. Superman III tiene a Richard Pryor. Son muy similares: ambos están seriamente fuera de lugar en películas como esta, ambos son ruidosos y ambos no tienen gracia. Juntos hicieron un lío de ambas películas. La única diferencia es que Batman Forever hizo un asesinato mucho más grande en la taquilla.

Las similitudes no terminan ahí: mientras que las dos películas anteriores de Superman se tomaron en serio la historia, ésta no lo hace y lo hace para reírse. Tome la secuencia de apertura de esta película. En cualquier otra película hubiera funcionado. En este caso, parece muy fuera de lugar. Lo mismo sucedió con Batman Forever . Entonces, ¡la historia se repite!)
        
            