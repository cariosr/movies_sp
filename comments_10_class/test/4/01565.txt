
                      Fui a verla sobre todo por haber sido ganadora de los Oscar y opinión de la crítica pero me decepcionó mucho. Es lenta y aburrida. Lo que creo que intenta transmitir lo hace tan mal que al final no te atrapa para nada. Sólo tiene de vez en cuando algún destello interesante y nada más. Supongo que yo no entiendo nada de cine para que en opinión de los académicos y la crítica especializada esté tan valorada. No la recomiendo para nada.
        
            