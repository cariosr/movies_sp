
                      Si bien el comienzo parece prometedor y lo bastante original como para mantener el interés, e incluso el nudo de la trama sea también lo bastante seductor como para pasar un buen rato, lo que acaba matando las expectativas es el ritmo tan irregular que la película tiene, lo que acaba desdibujándola y haciéndola, por momentos, aburrida
        
            