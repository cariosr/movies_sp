
                      La película repite la misma fórmula, los mismos clichés, la misma técnica de contar la historia, las mismas frases "disque" reflexivas y lo que más fastidia es que le quieren meter profundidad, pero eso se nota muy forzado y no calza.
La única diferencia es que la nueva protagonista de esta película es más carismática y conectas fácilmente con ella y te importa lo que le pase.
Fuera de ahí, es más de lo mismo.
        
            