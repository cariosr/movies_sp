
                      Una comedia algo forzada centrada en un protagonista con dislexia que no asume su problemática hasta que pierde su empleo y debe volver a cursar el secundario (17 años después) para reinsertarse en el mercado laboral. Cuenta con esporádicos pasajes divertidos basados principalmente en golpes y caídas, algún que otro diálogo aislado y los métodos poco ortodoxos de la docente para lograr enfocar a su alumno. El final es bastante cuadrado y previsible. Poco para rescatar.
        
            