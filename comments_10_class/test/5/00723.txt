
                      Es una película de atmósfera densa y difícil interpretación. Aunque por la fotografía es una película muy reseñable, lo cierto es que a mí, particularmente, me resultó larga, pretenciosa y voluntariamente críptica. Los juegos de luces, espacios y tiempos resultan muy interesantes y complejos pero serían a mi parecer más interesantes si como trasfondo tuvieran una idea más allá del puro esteticismo y de la voluntad de autoafirmación de su autor.
        
            