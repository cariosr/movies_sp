
                      Aunque esta película funciona más o menos bien durante la primera hora, siendo coherente (característica que tampoco es un logro teniendo en cuenta que no es un argumento muy enrevesado) y despertando el interés del espectador, lo cierto es que se ha desaprovechado totalmente lo que podría haber sido el momento más épico de la cinta. Y ya no solo se ha desaprovechado por la no inclusión de ciertos personajes, sino porque el humor facilón e insulso tan característico de Marvel, que hasta entonces estaba bastante bien controlado en trama, de golpe y porrazo se ha disparado de forma inoportuna en lo que podría haber sido el verdadero clímax de la película. 

Creo que después de tanta publicidad, después tanto trailer haciéndote creer que esto iba a ser serio, después de revelar la gran sorpresa en el trailer, la gente merecia una batalla y no un festival de chistes malos que lo único que consigue hacer es que dejes de creerte el argumento, hasta entonces serio y bastante emocionante. 

Aunque para mi lo peor de lo peor, y esto ya es personal, ha sido Antman... No tuvo suficiente en su película con reirse de la física y de la mirmecología, no, necesitaba más películas para seguir... en fin es igual, no quiero revelar lo que ha hecho ahora jajaja...

Oportunidad desaprovechada, a ver si por lo menos los vengadores III cumplen en ese objetivo o tal vez deba depositar mi fé en xmen apocalypsis.
        
            