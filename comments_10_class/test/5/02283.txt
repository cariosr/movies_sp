
                      Me ha dejado un poco indiferente. La historia como tal tiene poca chicha. He visto la misma trama más de una vez. Lo que destacaría sería la manera de contarla. Se entremezclan imágenes supuestamente reales con las de la propia película, de manera que vemos a la vez lo que supuestamente ocurrió con la dramatización de esas imágenes. Aunque al principio es curioso al final llega a entorpecer y a que no acabemos de entrar en la historia.
        
            