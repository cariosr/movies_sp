
                      Sacado directamente de la churrera de superhéroes de la factoría Marvel, llegó a nuestras pantallas DeadPool un antihéroe corroído por un deseo atroz de venganza y un armado con un sentido del humor de lo más particular. Bocachancla, irreverente, mordaz hasta el punto de ser hiriente con sus bromas es su principal carta de presentación.
Acción e insultos/bromas a cascoporro en esta cinta que, pese a ser una gran fuente de ingresos, no pasará a los anales de la historia del cine.
        
            