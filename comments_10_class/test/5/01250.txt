
                      Cuando Daredevil no seleccionó a los críticos hasta el último minuto, tuvo que pensar en esta versión cinematográfica de Marvel Comic, protagonizada por Ben Affleck como el abogado ciego que patea el culo de un malo por la noche como un vigilante enmascarado en un traje de cuero del color. De regaliz rojo - apestaría a lo grande.

Sorpresa. Daredevil no se ríe a carcajadas, es algo peor: profunda, deprimente promedio. El guionista y director Mark Steven Johnson, responsable del conmovido Simon Birch, trata de mezclar el mundo oscuro del Batman de Tim Burton con el toque ligero que Sam Raimi trajo a Spider-Man y el resultado se presenta como una película sin personalidad propia.
        
            