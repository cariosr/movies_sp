
                      Mezcla de largometraje de carretera y de amigos a la fuerza, con un humor fácil, simple, directo, llano e incluso en algunos momentos escatológico. Entretenido y comercial, aunque no aporta ninguna novedad al género, se ve fácilmente. A su favor, diré que la poca gente que había en la sala se reía a carcajadas sonoras. Cuenta además con el aliciente de Robert Downey Jr. En resumen: Largometraje para gente de risa fácil, que le gusta ir al cine a pasar un rato divertido y no calentarse la cabeza.
        
            