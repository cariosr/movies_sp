
                      Durante uno de estos golpes, Kick-Ass es rescatado por Hit Girl (Chloe Grace Moretz) y Big Daddy (Nicolas Cage), un dúo de padre e hija similar a Batman y Robin que no están por encima de lisiar o matar criminales.

Kick-Ass y Hit Girl han decidido formar equipo a medida que se abre la secuela. Ella le enseña los métodos del vigilante, ayudándolo a crecer e instándolo a conquistar su miedo. Sin embargo, después de que el tutor de Hit Girl se entera de sus travesuras, se ve obligada a asistir a la escuela y enfrentarse a un enemigo mucho peor que los matones callejeros: las chicas malas.

Kick-Ass, mientras tanto, decide unirse a otro equipo de aspirantes a héroes, este dirigido por el coronel Stars and Stripes (Jim Carrey). Los buenos al principio pasan sus noches realizando servicio comunitario. Pero los comedores pronto pierden su atractivo, y el Coronel los convence de maltratar a algunos tipos realmente malos.
        
            