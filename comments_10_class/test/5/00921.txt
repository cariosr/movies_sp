
                      Largometraje de acción para lucimiento de Angelina Jolie. Esta película cuenta con todos los ingredientes del cine de acción: persecuciones tanto sin vehículo como con vehículo, disparos, explosiones, peleas, saltos imposibles, un guión simple y flojo, una guapa protagonista, un malo malísimo y un final complaciente. 

Largometraje tan fácil de ver, como tan fácil de olvidar que te distrae durante los 90 minutos que suele durar. Pero al precio que están las entradas de cine hoy en día, mejor esperar a que salga en DVD, y alquilarla y verla en el salón de casa.

En resumen: película para seguidores de Angelina Jolie, o del cine de acción.
        
            