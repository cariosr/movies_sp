
                      Una excursión atrapante, un ritual que no convence.
Arranca interesante, generando intriga con situaciones anómalas que se van suscitando en el bosque. Sueños, alucinaciones, suspenso ascendente. Sin embargo el absurdo desenlace no estuvo a la altura de todas las buenas intenciones que propone el film. Regular.
        
            