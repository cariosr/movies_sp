
                      Creo que consigue lo que pretende,  agobiar con las discusiones familiares. No hay que olvidar que es una adaptación de una obra de teatro.
Las interpretaciones son espectaculares; Meryl Streep demuestra ser la mejor actriz que existe con otro papel para recordar y Julia Roberts ha vuelto con un excelente papel tras años sin hacer nada. El resto del reparto no se queda atrás...

Para mí,  la mejor fotografía que he visto en esta temporada de premios.
        
            