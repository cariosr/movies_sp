
                      No la he visto en pantalla grande, pero como está y sí la he visto sólo puedo decir que es imprescindible su visión. ¿Cuáles son las razones? Pues bastará con citar una: fue candidata al Óscar como mejor película, y eso que es una película animada y "dirigida a un público infantil". Además es uno de los mejores musicales y está basada en una obra literaria, aunque Disney modifica el final. Magnífica película.
        
            