
                      De las películas de origen del UCM más destacables. No aburre, tiene buena trama, interesante y diferente a otras. Las actuaciones son geniales, desde Brie hasta Samuel, aunque Annette Bening, Jude Law y Ben Mendelsohn se lucen de igual forma. La película es, sin duda, una presentación efectiva del personaje.
        
            