
                      Una película muy bien documentada, que enlaza y sustenta la relación entre las apariciones de Fatima, los misterios revelados y el devenir de la Europa del Siglo XX. Retrata sin aspavientos Las implicaciones del comunismo en la historia de la humanidad, siendo esta una ideología totalitaria y sangrienta que cambió para siempre el destino del mundo, al haber tocado fondo con los gulags sovieticos cohartando el derecho a pensar, discernir o sentir del pueblo ruso y extrapolando su esclavitud a occidente..
        
            