
                      Es poco común que una película refleje tanto el espíritu del libro. Logra registrar con maestría la angustia de los personajes al enfrentarse, muy jóvenes, a estos siniestros juegos. La angustia la vemos en cada momento en la expresión Katniss ( J Lawrence ), excelente en su papel. Desasosiego, temor, esperanza, cariño, audacia, tristeza, todo en su rostro. Vale la pena reveer la película ( y las dos posteriores ) siguiendo sus expresiones. Un lujo poco frecuente en películas de acción.Gran actuación además de Sutherland y Harrelson.
        
            