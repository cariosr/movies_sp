
                      Impactante y emotiva fiel representación del Holocausto Nazi. Sobrecogedora, realista y cruda en ciertos momentos, rodada en blanco y negro acentúa aún más su dramatismo. Esta película galardonada con 7 Oscars es una auténtica obra maestra y casi un documental. El papel de Liam Neeson es fantástico. Imprescindible.
        
            