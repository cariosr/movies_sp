
                      Eduardo Manostijeras es una película de fantasía dirigida por Tim Burton, director de Pesadilla Antes de Navidad, Sleepy Hollow y Alicia en el País de las Maravillas.

Eduardo Manostijeras (Johnny Depp) es el resultado de la inacabada invención de un extravagante y anciano inventor (Vincent Price).
Un día, una representante de cosméticos (Dianne Wiest) lo encuentra solo en una mansión abandonada. Sintiendo lástima por el pobre muchacho, lo invita a pasar una temporada a su casa.

La actuación de, el por entonces jovencísimo, Johnny Depp es brillante. Aún sin decir mucho a lo largo de la película, su rostro refleja cada emoción con perfecta claridad.
Es imposible no sentir empatía y afecto por él, incluso con sus feroces manos y su siniestro atuendo.

A diferencia de películas como Bitelchus o Batman, en ésta Tim Burton crea unos escenarios coloridos, muy llamativos. El pueblo donde tiene lugar la acción destaca por su aspecto limpio y luminoso, y por sus casas de colores pastel, lo que contrasta con la mansión de Eduardo Manostijeras, destartalada y oscura.
En su conjunto es un filme conmovedor y entrañable a la vez que entretenido, ya un clásico del cine que merece la pena ver por el reparto, los escenarios y vestuarios, y la fotografía, además de por la maravillosa banda sonora de Danny Elfman.

La película obtuvo cuatro nominaciones a los BAFTA y una a los Oscar (mejor maquillaje).
        
            