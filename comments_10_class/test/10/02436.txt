
                      Un envoltorio de cuadros en movimiento y un reparto espectacular, encabezado por Robin Williams y escoltado por estupendos secundarios como Cuba Gooding Jr. o Max Von Sydow, hacen de este drama fantástico algo único y diferente, cine para disfrutarlo y sufrir a partes iguales. Original y emotiva, es una película distinta y que deja huella, no solo por el lienzo en movimiento que, por momentos, parece, sino por la mezcla de drama y fantasía tan peculiar
        
            