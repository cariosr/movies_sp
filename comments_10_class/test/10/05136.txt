
                      Fracture, narra la historia de dos hombres con muy pocas cosas en común:
Ted Crawford (Anthony Hopkins), un ingeniero de mediana edad que acaba de dispararle a su mujer a sangre fría, y Willy Beachum (Ryan Gosling), un joven y ambicioso fiscal cuya carrera está empezando a despegar. Ambos hombres se encuentran cara a cara en un camino directo al desastre. Crawford resulta ser tan complejo e impredecible como el crimen que ha cometido y, mientras el caso perfecto de Willy comienza a desmoronarse, el futuro del joven fiscal parece destruirse con él.

Gregory Hoblit director de esta cinta, nos trae una vez más un filme que gira en torno a los casos policíacos y jurídicos. Él parece haberse especializado ya en este tipo de temas, así lo confirman todas las películas dirigidas por él.
Por otra parte, se cruzan ciertos juegas de palabras entre sus dos personajes centrales, la lucha de caracteres, el manejo del orgullo, del poder y del amor al dinero, que por supuesto son interesantes. Destacar la impecable factura técnica, como su fotografía, montaje y finalmente la banda sonora que acrecienta la intriga en sus puntos más álgidos.

Las actuaciones son muy buenas sobre todo la de Hopkins que se come la pantalla, nos recuerda casi palmo a palmo su interpretación de Hannibal Lecter en las distintas películas que se crearon sobre este histórico psicópata. Simplemente un genio. Ryan Gosling no me termina de convencer en esta cinta.
En definitiva una maravilla.
        
            