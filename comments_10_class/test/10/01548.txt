
                      Es simplemente una de las grandes peliculas de la historia del cine, y una de las obras maestras de Tarantino.
Con un reparto fabuloso, en una trama en la que se entrelazan varias historias entre matones, policías, mafiosos, boxeadores, soldados, etc, ¿Qué podría salir mal? En esta ocasión nada.
Los diálogos son tremendamente ingeniosos, hasta en los momentos más relajados y ociosos, y es que incluso hablando de una simple hamburguesa, consiguen que te den ganas de probarla únicamente por cómo lo expresan, y desde luego hay que saber expresar para convencer a la gente. 
Esta película es una parada obligatoria para todo cinéfilo que se precie, y desde luego el sello de Tarantino está presente en todo momento. El humor negro y la acción junto a las referencias al cine de la época y los diálogos tan ocurrentes hacen de esta una película diez con un reparto brillante. Totalmente recomendada.
        
            