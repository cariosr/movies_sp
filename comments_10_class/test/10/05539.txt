
                      Mamma Mia! es un musical dirigido por Phyllida Lloyd y producido por Tom Hanks y dos de los integrantes de ABBA; Björn Ulvaeus y Benny Andersson, quienes ayudaron con el guión y la banda sonora.

Sophie (Amanda Seyfried) está apunto de casarse con Sky (Dominic Cooper). Quiere que su padre la lleve al altar, pero su madre Donna (Meryl Streep) nunca le dijo quién era. Por eso, tras descubrir el antiguo diario de su madre, invita a tres posibles candidatos a padre (Pierce Brosnan, Colin Firth y  Stellan Skarsgard) a espaldas de Donna.

Con esta película, se vuelve a demostrar lo divertida que es Meryl Streep, y lo bien acompañada que está con Julie Walters y Christine Baranski. Las tres protagonizan gran parte de las mejores escenas de la película.
El resto de actores son igual de estupendos, al igual que el vestuario y los escenarios paradisíacos.

La banda sonora consta de las canciones más conocidas del grupo musical sueco, ABBA. Unas versiones muy fieles interpretadas por magnificas voces.

Seis nominaciones obtuvo la película, de las cuales dos eran Globos de Oro.
        
            