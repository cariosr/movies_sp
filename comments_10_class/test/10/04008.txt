
                      Una película que logra ponerte todos tus sentimientos a flor de piel solo se puede tachar de obra maestra! 
Reirás, llorarás, disfrutarás y gozarás con esta maravillosa película. 
Espero que el señor Hanks nos deleite con su cine al menos 40 años más.
        
            