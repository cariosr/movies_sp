
                      Maravillosa película... es sublime, hermosa y triste al mismo tiempo. Ries y lloras. Una creatividad fabulosa. Muestra cómo en medio del caos, del dolor más fuerte, de donde se piensa que jamás se podría ver algo positivo, se crea como una visión paralela, donde todo ese sufrimiento se convierte en un juego. Muestra lo que solo un padre puede hacer por amor y para mantenerla ilusión y la inocencia de su pequeño. Ame esta película, toco lo más profundo de mi corazón.
        
            