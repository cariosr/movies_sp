
                      Es una superproducción muy bien hecha, con muy buenos efectos especiales, una película de aventuras. Hace más de 40 años era Charlton Heston quien hacía de Moisés en Los Diez Mandamientos y ahora lo ha interpretado Christian Bale, que borda el papel. La música es envolvente y te traslada totalmente. Las localizaciones, la fotografía. Recomendad absolutamente.
        
            