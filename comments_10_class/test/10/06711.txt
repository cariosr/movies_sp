
                      Me parece una excelente película, lejos de ser aburrida y lenta,  es un drama muy realista de lo que sufren las madres al perder un hijo.  Jennifer hizo un excelente papel y muy bien actuado. Si pretendes la típica película yankie que te haga llorar por una emotiva música de fondo,  no te gastes en verla porque es totalmente distinta. Esta película va más allá del estilo Hollywoodense. Me encantó y la recomiendo :)
        
            