
                      A mi opinión una de las mejores sagas cinematográficas de toda la historia, no solo por una trama original sino por ese mensaje de una amistad tan fuerte y el deseo de un joven con encontrar y descubrir al asesino de sus padres.
        
            