
                      Kubrick, crea una atmósfera realmente terrorífica para el film. Un hotel incomunicado por largos periodos de nevadas intensas. Jack Torrance, un psicópata que pierde cada vez más el contacto con la realidad y se recluye allí para escribir su libro, su familia arrastrada por el y unos pocos empleados encerrados para dar sus ¿Servicios? A partir de aquí si no has visto la película,no se a que esperas.

No hay escenas con planos oscuros o fotografía con penumbras para crear el climax de agobio, para crear el miedo. Kubrick da una patada a todos los cánones escritos y por escribir y realza los colores vivos, los espacios claros y con ello consigue introducir el miedo pasito a pasito dentro de nosotros.

No hay giros de cámara con el sobresalto de la banda sonora de repente para que des un respingo en el sofá, nada de eso, no señor. Si buscas el susto fácil, tienes otras películas para ello que también son de terror, pero de otra manera.

Lo que si hay es moqueta, un número de habitación, dos niñas gemelas adorables, un enfermo corriendo con un hacha por los pasillos. Lo que si hay es “Redrum” Un niño con un don compartido, un correpasillos, un ascensor. Terror del bueno. No de sustos, no del que te hace ponerte en situación y te haga pensar que tu en su lugar estarías pensando que coño hago aquí joder... 
Terror psicológico del bueno.

Que decir de Jack Nicholson... Que grande es... 
Cómo hace que te metas en su personaje, en su mente, imaginación y embrujo de lleno que sufre. Interpretación que no puedes imaginar hasta que punto de trastorno mental llega a mostrarnos con esos gestos y esas caras tan polifacéticas, Una de sus mejores interpretaciones en su carrera. Por lo demás correctas actuaciones de cada uno de los demás actores pero es que este Jack los eclipsa a todos.

El Resplandor a mi parecer, es una de las mejores adaptaciones al cine de un libro, y mira que era complicado crear ese ambiente tan peculiar de Stephen King pero realmente lo clava.
En definitiva, Obra maestra.
        
            