
                      Excelente film de Sheridan, con unas actuaciones soberbias, destacando a un infinito Daniel Day-Lewis que, incomprensiblemente, no consiguió el Oscar por esta actuación. Dura, emocional e intensa, todo en ella rezuma verdad y los años no la han maltratado, todo lo contrario
        
            