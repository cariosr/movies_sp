
                      Una química, un enlace casi irreal une a ésta película que yo calificaría de obra maestra a la obra de Hergé. Spielberg rinde sin duda el mejor homenaje que podría hacerse a este genio de los dibujos animados. Ya sea por el estilo gráfico, por el ambiente o por los diálogos todo está ahí para ponernos en lo que yo calificaría como el "ambiente Tintín". Y aún si yo sé que no todos los Tintinófilos apreciarán ésta película los aliento a ir a verla no como una adaptación cinematográfica si no como una aventura reinventada en homenaje a Hergé.
        
            