
                      Cuando en el cine no de ficción, en el realista, los personajes resultan poco creíbles, es que la película hace aguas. Es la sensación que deja un trabajo en el que destaca, como casi siempre, Emma Suárez, pero que tanto sus lagunas en la acción como la irregularidad en el ritmo hacen que el manto de estética quede empañado por una floja película en su conjunto si miramos el todo
        
            