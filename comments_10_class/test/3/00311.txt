
                      Aunque se reconoce el enorme genio de Pedro Almodóvar como director cinematográfico, la película es una superficial historia de cine negro. Se echa en falta una mayor profundidad en la relación de los personajes, lo que la hubiera podido convertir en una gran historia. Lástima, otra vez será.
        
            