
                      o puedo decirte cómo se compara la película con su origen: mi único roce con los libros de historietas viene en forma de ósmosis de mi compañera de piso, que está tratando de engancharme dejando los cómics al lado del retrete, pero puedo decirte que esta es una pálida imitación de una película de acción.

Parte del problema de la película es su configuración. Casi toda la película se centra en el "descubrimiento" de los poderes especiales de los personajes. Esos rayos cósmicos pueden causar estragos en tu ADN, ¿sabes?

Esto está bien hasta cierto punto, pero solo hay muchas escenas entremezcladas de Mr. Fantastic (Ioan Gruffud) con unos pocos pies de brazo para Bogot Roll y Johnny Storm (Chris Evans, no, no el DJ) intentando conseguir a Ben Grimm's Thing (Michael Chiklis) para golpearse en la cara que una persona puede tomar.
        
            