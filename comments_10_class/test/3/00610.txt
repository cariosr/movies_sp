
                      Hubiera pasado por buena película de la saga si hubieran sido fieles a la idea del juego. No me molesta que sean nuevos personajes pero no tiene historia alguna, no te explican el por qué de nada y al final la gente que no haya jugado la saga entenderá a los Assassins como unos fanáticos que actúan por el "credo" y a los Templarios como una empresa que pretende erradicar la violencia del mundo. La película es solo una sucesión e lagunas narrativas y escenas de acción; que por cierto son de las mejores, te ponen los pelos de punta. 
Lo peor de todo casi es el final, predecible, aburrido, mediocre y sin pies ni cabeza. Intentan dejarte un final abierto pero te acabas yendo con mal sabor de boca...
        
            