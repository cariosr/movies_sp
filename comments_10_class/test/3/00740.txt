
                      Por primera vez en mi vida, Clint Eastwood me ha decepcionado. No es ni entrañable. Falto de ritmo y de historia. Solo se salvan los 3 primeros minutos por la gran recreación del tsunami. A partir de ahí y salvo una escena en la clase de cocina, el resto es un horrendo relleno de situaciones cotidianas típicas de un programa matutino.
        
            