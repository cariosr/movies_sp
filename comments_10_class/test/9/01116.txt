
                      Excelente película que, por una vez, no es una "películas de niños que pueden ver los padres", sino que en este caso es una "películas de padres que pueden ver los niños". Mi hijo de 5 años salió encantado con la película, pero yo salí sorprendido de ver una película tan inteligente y bien hecha.
        
            