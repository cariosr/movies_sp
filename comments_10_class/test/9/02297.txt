
                      El infierno de Spike Lee sobre un policía negro que se infiltró en el KKK en la década de 1970 también es una acusación incendiaria de nuestro momento actual de Trump y una de las mejores películas del año. Un gigante del cine ha vuelto a encontrar su voz y el poder de hacerlo oír.
        
            