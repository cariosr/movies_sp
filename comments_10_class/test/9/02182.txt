
                      A pesar de que el argumento es un auténtico drama, la directora turca y su co-guionista, la francesa Alice Winocour, consiguen que el guión se convierta en una historia de hadas luminiscente y esperanzadora. Y no lo hacen omitiendo gravedad, ni siquiera violencia psicológica y explícita, lo consiguen mediante la firme actitud de resistencia y valentía de las cinco hermanas: ni asomo de pasteleo o edulcorantes artificiales. Ya con el título de la película (Mustang hace referencia a los caballos salvajes americanos) nos adelanta la belleza, el orgullo y la resistencia de sus protagonistas.

Leer reseña completa en:
        
            