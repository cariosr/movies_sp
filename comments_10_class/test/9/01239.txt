
                      ¡Impresionante película! Sacha Baron Coen crea uno de los personajes más buenos del cine. No se llevó la recompensa con premios cinematográficos por la película ya que hay escenas en que el humor puede despertar la crítica (y la dura). Un guión excelente, aunque muy descarado. Pasas muy buen rato y me parece una originalidad de film. En estos tiempos, una película de estas características, te alegra el día.
        
            