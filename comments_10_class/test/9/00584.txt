
                      Trepidante guión -que huye de nuestros peores tópicos-, excelente interpretación, muy buena ambientación y gran fotografía. De lo mejor del cine reciente español. Muy, muy recomendable.
        
            