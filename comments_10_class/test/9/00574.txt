
                      De qué hablamos cuando hablamos de amor

Si en la serie de Cartoon Network “Birdman: Attoney at law”, el superhéroe creado por Hanna-Barbera malvivía como abogado de los antiguos personajes de dibujos animados de la productora, en su nuevo trabajo Alejandro González Iñárritu presenta a un actor venido a menos que sigue viviendo a la sombra del héroe alado que le brindó la fama dos décadas atrás, mientras se embarca en la odisea de adaptar una ambiciosa obra de Raymond Carver que le permita surcar los cielos del estrellato de nuevo. Aunque su director ha negado cualquier conexión con estas referencias, no deja de ser llamativo el no origen de esta película.

Lo que no puede negar es que no es casualidad el haber escogido a Michael Keaton como protagonista de su historia. En el que es el mejor papel de toda su carrera, Keaton dibuja una caricatura de sí mismo, de sus tiempos como Batman y su ascenso y caída como estrella rutilante del séptimo arte. El actor da una de las mejores interpretaciones del año y se presta al juego del cineasta mexicano, el de ofrecer lo que no vemos tras los escenarios. 

Original, divertida, mágica, brutalmente ácida y arriesgada en una estructura formal que bien merece desde ya el Oscar a mejor director, montaje y fotografía, pero que también puede cansar a más de uno u obligarle a buscar posibles fallos, “Birdman o (La inesperada virtud de la ignorancia)” es un prodigio de narrativa cinematográfica. El magnífico uso de su banda sonora, su espíritu teatral respaldado por uno de los mejores repartos del cine reciente –todos, desde Edward Norton hasta Emma Stone, pasando por Zach Galifianakis y Naomi Watts, están soberbios-, y su guión ágil y mordaz que disecciona sin miramientos por igual todo lo que sucede entre bambalinas y lo que rodea al mundo del espectáculo son otros de sus grandes aciertos. 

Egos desatados, lazos familiares y conyugales por unir, compañeros de profesión difíciles, críticos que no necesitan ver una obra para hundirla… Iñárritu dispara dardos envenenados contra todo lo que se mueve por las humeantes calles de Broadway. Pero si de algo trata esta joya es del amor. Del amor confundido con la admiración, ése del que viven esos actores que se meten en la piel de superhéroes de Marvel, o esos directores embebidos en franquicias taquilleras sin fin. Todo ese éxito acabará un día, y a partir de entonces, o sigues siendo Birdman o te elevas a los cielos como una leyenda. 

A favor: su arriesgada y original propuesta, y su prodigio narrativo e interpretativo
En contra: su estructura formal podría hacérsele cargante y rocambolesca a más de uno
        
            