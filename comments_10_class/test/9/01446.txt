
                      Rodríguez y Cobos brindan con suciedad una de las mejores películas españolas de los últimos tiempos. Todo es urbano, degradante y creíble, no hay ningún personaje escondido, las interpretaciones de Casas y De La Torre son excelentes, nada que rescatar de las sombras. Los diálogos son excelentes, y posee una atmósfera fría y nacional, permitiendo al espectador saber que es una buena película desde el comienzo.
        
            