
                      Con el cine desde hace mucho tiempo en horas bajas, se agradece un peliculón como este. 2h30' inmerso totalmente en la tensión de la trama. Exige seguirla bien para atar posibles cabos sueltos. Muy buena interpretación por parte de Hugh Jackman y Jake Gyllenhaal.
        
            