
                      Un aplauso para el director! Refleja con valentía y descaro el espíritu de la generación Nintendo. No ganará grandes premios, ni romperá la taquilla, pero se ha ganado un hueco en mi filmografía personal, y creo que en la de muchos también.
        
            