
                      A simple vista no es más que un impulso, de esos difíciles. No es otra cosa que un salto al vacío. La edad podría ser una excusa. Es algo que hacen los alocados jóvenes, diría más de uno. 
Pero pronto entiendes que es algo más que una inquietud. Es la necesidad  de conectar consigo mismo, apartarse de todo y de todos para dar respuesta así a todo aquello que aún no se atreve a preguntar.
Es solo que todo el mundo parece encontrar la manera y el simplemente no es capaz. 
En el zenit del dolor nos preguntamos ¿por qué? Es una pregunta sin respuesta, supongo. Es la vida en todo su espectro. A otros les toca vivir otras cosas.
Necesitamos una respuesta para nosotros o una excusa para los demás. 
No quieres que llegue ese momento en el que te das cuenta que no has vivido nada en absoluto. Que tu vida está vacía de contenido, porque tú lo has escogido así.
Necesitas explicar algo que no eres capaz de comprender. 
Y esa lucha puede ser mortal. Puedes llegar a sentir la debilidad en cada uno de tus pasos. Como la naturaleza se las ingenia para colocarte justo en el sitio que quiere.
        
            