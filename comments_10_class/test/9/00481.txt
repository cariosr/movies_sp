
                      Una de las sorpresas del 2012

Drive es una película genial,con mucho estilo,Nicolas Winding nos trae un filme y un protagonista memorable que con su silencio ha encandilado al espectador.La pelicula tiene un buena historia,un guión que es una auténtica maravilla y un sonido espectacular que hace que pegues algunos botes,el rugido del motor del coche constante,la BSO,buenísimas,y por supuesto también destacar la interpretación del joven Ryan Gosling,un nombre que debemos aprendernos desde ya ,porque este chico tiene mucho futuro.

Todos los personajes,toda la película en general te llegan desde el principio en una narración que da envidia.Para concluir digo que es una película salvaje,dura y con fuerza,si solo tuviera que ponerle una adjetivo sería TREMENDA. 

Lo mejor: El personaje principal que es un Real Hero
Lo peor: No soy capaz de encontrar nada.
        
            