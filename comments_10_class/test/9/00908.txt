
                      La mejor película de dinosaurios que hay, desde 1993, aguanta el paso de las eras y puede verse hoy en día como si se hubiese estrenado ayer. La saga de Parque Jurásico es como el vino, cuanto más años, mejor.
        
            