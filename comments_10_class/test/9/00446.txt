
                      Extraordinaria simbiosis la que se ha dado entre la obra de Hergé y Steven Spielberg. La película recoge en toda su pureza el universo tintinesco y el espíritu del mejor cine de aventuras del director norteamericano.
Se podrían organizar amplios debates de tintinófilos sobre los cambios en el argumento, pero creo que se ha acertado en esta traslación al cine, donde han combinado elementos de varias de las aventuras, al margen de sacarse cosas de la manga. 
La película sirve de maravilloso y trepidante entretenimiento, y evita así convertirse en un continuo spoiler para los que conocemos muy bien las aventuras del reportero belga. 
El apartado técnico, impecable y los títulos de crédito son una pequeña joya en sí mismos. La tintinmanía está servida, leidis an yentelmen. Más que recomendable.
        
            