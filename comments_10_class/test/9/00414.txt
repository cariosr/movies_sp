
                      Llevaba mucho tiempo sin ver una película de terror y, sin ser una obra de arte, es una película que te hace pasar un buen rato. La trama es bastante buena y la banda sonora ayuda a que sea más interesante.
        
            