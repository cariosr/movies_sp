
                      Grandissima película de Ron Howard, que nos muestra con total claridad la rivalidad entre James Hunt I Niki Lauda. Impresionantes actores que interpretan muy bien el papel y que tienen un parecido muy razonables con los personajes reales. Una fotografia impecable. Las carreras de coches adrenalínicas y totalmente realistas. La caracterización del rostro quemado de Lauda realmentebes muy creible. Muy recomendable y entretenida de ver.
        
            