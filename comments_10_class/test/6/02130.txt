
                      Entretenida, durante mucho rato te atrapa con el ritmo de su acción aunque en algunos momentos sientes que algo no cuadra y que se pierde el ritmo. La idea de la droga que todo lo puede es original, pero pronto esa ciencia ficción te pone los pies en el suelo cuando ves como decae el protagonista. Robert de Niro, como siempre, me encanta...
        
            