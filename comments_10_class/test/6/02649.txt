
                      El guión es sólido y coherente en todo momento y las interpretaciones son muy buenas, especialmente la de John Malkovich y la de Brad Pitt. Además de hacernos pasar un buen rato, los Coen no podían dejar de lanzar sus dardos envenenados hacia la sociedad norteamericana, hacia su cultura y hacia el sistema de trabajo de la CIA.
        
            