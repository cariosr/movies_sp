
                      Una fotografía perfecta y unos efectos visuales de los que no se abusa complementan a una película que cumple con creces el objetivo de mantenerte pegado a la butaca con una obra cuidada y detallista. Una ópera prima de un nuevo director a tener en cuenta. (6/10)
        
            