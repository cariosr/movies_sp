
                      Tiene todas las virtudes y defectos que una película basada en una serie podría tener: No puede condensar el ritmo de un episodio en una hora y media y por tanto tiene los altibajos propios que cabría esperar, pero a su vez tiene la frescura y la chispa que los aficionados a esta serie animada esperan. Si no eres fan, es totalmente prescindible; si te gustan las andanzas de Bob Esponja, al menos resulta entretenida.
        
            