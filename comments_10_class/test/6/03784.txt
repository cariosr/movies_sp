
                      Podemos decir de la película que se queda un poco en la superficialidad de las relaciones de pareja. Entiendo, sin embargo, que plantea bien los interrogantes más básicos sin pretender en ningún caso hacer un enrevesado viaje por todas los puntiagudos ángulos de la naturaleza humana. La sencillez en los planteamientos es su carta de presentación que la hace bastante atractiva. (6/10)
        
            