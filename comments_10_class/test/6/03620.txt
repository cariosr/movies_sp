
                      Una bajada de línea contundente en favor de la portación de armas y la toma de justicia por mano propia ante hechos delictivos. En este caso un cirujano, hombre de familia, que se transforma en justiciero tras la muerte de su esposa luego de un brutal asalto en su domicilio. Está bien lograda, mostrando la metamorfosis del protagonista ante la inoperancia policial. Cumple su objetivo pese al polémico mensaje.
        
            