
                      Cuando la tecnología se vuelve en contra e los humanos.

Segunda entrega de la saga Avengers, donde Marvel demuestra su dominio con los efectos especiales.

Ademas de los principales protagonistas se suman los dos nuevos hermanos Quicksilver y Bruja Escarlata a la par que también hace su fenomenal aparición, Vision. 

El argumento por parte de este nuevo largometraje resulta un poco más desarrollado y prudente de lo que fue en su anterior entrega.

Pero donde está su punto fuente ésta vez (aparte de los apartados visuales que previamente mencioné) es en los personajes. Ahora si se puso más enfoque ellos y se ve un claro progreso positvo a lo que habíamos visto en el 2012. Por ejemplo: Ojo de Halcón (Jeremy Renner) ahora ganó más minutos en pantalla, y acaba siendo un personaje decisivo en el final del film.
Steve Rogers (Evans) y Tony Stark (Downey Jr.) siguen siendo los mismos, no recaen nuevamente en los clichés con sus personajes. 
Y el personaje de Natasha (Johansson) se desarrolla mucho mas, se hace mucho mas complejo y funcional a la vez... la cual crea y asenta esa química con Bruce Banner que había empezado en The Avengers.

Pero la frutilla del postre es sin dudas Vision. El personaje mejor logrado de lo que va de la saga.
        
            