
                      Irónicamente, le falta imaginación

Es cierto que la película resulta entretenida en líneas generales y tiene algunos toques de humor que se agradecen (faltaría más siendo una comedia), pero se queda a medio camino entre las aventuras, el romance y la comedia sin llegar a satisfacer totalmente ninguna de las áreas. Hay que reconocer, sin embargo, que tampoco se llega a hacer aburrida en ningún momento, pues las situaciones que vive el protagonista son bastante peculiares por sí mismas, pero se echa en falta algo más de valentía e imaginación en las divagaciones mentales del protagonista (las cuales quedan relegadas a un segundo -o tercer- plano cuando comienza la aventura) que es donde verdaderamente podía haberse distinguido esta película.
        
            