
                      Con una producción de calidad muy similar a la reciente “El Médico” esta “La ladrona de libros” se parece a aquella en que pese a ser una historia en el fondo muy sencilla la puesta en escena y los intérpretes la convierten en una historia con encanto que más parece rodada en otras épocas que en este siglo XXI que nos tiene acostumbrados a cosas más vacías e impersonales. Personajes sencillos que simplemente te caen bien, escenarios reducidos pero con personalidad…..y lo mejor de todo….y que no sirva de precedente…..La Muerte.
        
            