
                      Una película de miedo que te provoca algún que otro sobresalto en algunos momentos y que tiene un final bastante bueno.

La película empieza en las montañas donde ves como la familia de Kevin Bacon se divierte por ahí (su mujer me suena de Silent Hill y su hijo es Bruce Wayne en la serie Gotham), todo va bien hasta que el hijo de Bacon se cae en una cueva donde encuentra unas piedras que se lleva consigo. Hasta ahí todo bien pero luego empiezan a pasar cosas raras como puertas moviéndose solas, sombras de personas que no están ahí y manos negras en las paredes. Entre susto, trama y susto transcurre la película la cual habla de unos antiguos demonios que asustaban a la gente para alimentarse de su miedo.

Una película normalita que consigue hacerte pasar un buen rato viéndola y muy buenos actores sobre todo David Mazouz.
        
            