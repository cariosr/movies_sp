
                      Apunta a vivir el hoy y el ahora intentando transformar todo aquello que esté a nuestro alcance. Argumento ya visto pero encarado desde otro lugar. Podría ser comparado con "Feliz día de tu muerte" pero esta cinta ofrece una mayor profundidad argumental. La recomiendo.
        
            