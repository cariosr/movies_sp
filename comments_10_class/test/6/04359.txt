
                      Entretenido largometraje de periodismo de investigación, con unas dosis de Thriller, aunque se echa de menos un poco mas de acción. Con unas actuaciones moderadas, pero creíbles. A pesar de su duración, no se hace largo ni pesado. Se puede llevar a la familia, pues no contiene escenas,  ni de sexo ni de violencia gratuita, aunque la trama puede resultar compleja para los pequeños. Y con un final..
        
            