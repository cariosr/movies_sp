
                      Una vez más, la cruda realidad regala al cine una oportunidad para narrar la belleza oculta en todo catastrofismo. Peter Berg se las arregla para que 'Marea Negra' parezca un documental en vez de una historia profunda y sentimental con personajes construidos. De hecho, el film se limita al impacto explosivo de los acontecimientos, hasta llegar al agotamiento atómico producido por el fuego y las explosiones.
        
            