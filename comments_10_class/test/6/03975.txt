
                      Había leído que la película era una copia plano por plano de la clásica de 1994, y lo es. Pero no por eso tiene que ser mala como la han puesto algunas críticas. Al contrario. El Rey León es el clásico más querido de Disney y este remake no lo estropea, aunque tampoco añade nada nuevo.... 

Verla en el cine me ha servido para revivir el gran clásico de mi infancia en una versión más realista e incluso diría que menos infantil. Han añadido algunos detalles que explican mejor las relaciones entre personajes: 