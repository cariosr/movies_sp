
                      Joseph Gordon-Levitt se mete en camisa de once varas dirigiendo, escribiendo y protagonizando una película, algo que según algunos solo debería estar permitido a verdaderos genios. Lo cierto es que la ópera prima no es redonda, y los fallos (cómicos, de tempo, de guión) se ven de lejos, pero hay talento aquí.
        
            