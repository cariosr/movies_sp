
                      La nueva película de Tim Burton puede dejarte indiferente. Para los incondicionales fans de este director la película es técnicamente perfecta. Lejos de eso estamos el resto de los mortales que una vez finalizada tenemos la sensación de vacío: bonitas imágenes, buena interpretación y buena ambientación pero nada nuevo a esta particular versión. 
Eso si, las actuaciones de Johnny Deep y Helena Bonham Carter no dejaran a nadie indiferente.
        
            