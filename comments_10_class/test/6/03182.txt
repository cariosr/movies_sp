
                      Estamos frente a una película típica Tarantino donde más que terror se mezclan comedia y viscosidad. Interesante experimento al recobrar el aire de cintas como "La matanza de Texas" y llevarlo a lo puramente irrealista hasta llegar a lo absurdo. Un absurdo que te saca la sonrisa si eres consciente de quien y para qué está hecha está película. Destaca la ambientación, la estética del film, y la imaginación aplicada al personaje femenino principal. Esto último, para los aficionados a los cómics les encantará.
        
            