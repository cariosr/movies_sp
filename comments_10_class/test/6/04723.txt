
                      Kingsman: El Círculo de Oro (2017).
"Los modales hacen al hombre".
El realizador de la primera parte de esta peli de espías superdotados vuelve a la carga con su secuela muy en la línea de la anterior, aunque esta vez con más artistas de renombre. A saber: aparecen ahora Julianne Moore como la villana despiadada pero tan infantil como el cobijo en el que se desenvuelve. Le acompañan Elton John (interpretándose a sí mismo y poniendo música a muchas de las escenas con sus canciones de los 70's), Bruce Greenwood que parece hacerle un guiño al descerebrado Donald Trump haciendo también de presidente (demasiado sobreactuado, aunque conociendo a Trump, quizá no tanto...), Jeff Bridges, Emily Watson, Pedro Pascal (quien tras Narcos no hace más que aparecer en la gran pantalla), y repiten Mark Strong y un recuperado Colin Firth (ya veréis por qué...).

"Recuerda que tener algo que perder hace que la vida merezca la pena".
En esta parte, a los Kingsman se les unen sus homólogos de EE.UU. Statesman comandados por Bridges, y con Halle Berry como colaboradora tecnológica. 

Lo cierto es que resulta entretenida como cinta de acción (las imposibles coreografías son explosivas), y sus más de dos horas de metraje no dejan mucho tiempo para el hastío, pero sigue siendo, como su antecesora, exagerada, muy fantástica e infantil, como apuntábamos arriba con el personaje de Moore.

Atentos a la versión del Word Up de Cameo interpretada por The Bosshoss en la escena de acción final.

Raúl Cabral.
        
            