
                      Ciencia ficción con una estética y una atmósfera deslumbrantes, un buen reparto, con una gran Amy Adams, una fotografía perfecta y una trama decente. Denis Villeneuve sin duda es un gran director y hace las cosas muy bien, pero sigo pensando que está sobrevalorado, la película está muy bien hecha, con una imagen excepcional, pero le falta algo más de contenido para poder disfrutarla plenamente y que sea una gran película, por ese motivo le pongo un seis y por cierto no tiene nada que ver con Interestelar, que a la gente le encanta comparar.
        
            