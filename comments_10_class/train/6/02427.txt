
                      Esta saga me dio buenas vibraciones, hasta pensé en leerme los libros. Aunque tengo que decir que me gustaron bastante más las primeras películas, esta ya no me pareció que estuviera a la misma altura.
La película comienza bien atando algunos cabos sueltos y hablando sobre más allá del muro, cosa que te intriga y despierta tu curiosidad con las películas anteriores, el exterior me a recordado un tanto a Mad Max, pero ahí se acaba lo bueno. Me a parecido muy lenta, con poca acción para ser la saga Divergente, pero sobre todo lo que me atraía era las separaciones por facciones, que para mí es lo que hacía especial esta historia, pero ya con las facciones eliminadas, el interés ahora en gobernar todo, ya me parece bastante pobre. 
Es como si las cosas que atraían de esta historia, se fueran consumiento poco a poco. No tengo muchas esperanzas en la próxima. Pero ya que hemos visto todas, habrá que terminarlas.
        
            