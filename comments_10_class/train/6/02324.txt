
                      Una peli de vaqueros, de indios y de invasores venidos de otro planeta, es la originalidad de este western moderno. En general, el elenco y el juego son buenos, pero el guión es algo simplista y no responde a todas nuestras preguntas, especialmente a aquellas que conciernen a Ella (Olivia Wilde). Pero si eres fan de la acción y de los western, tienes más que de sobra.
        
            