
                      Película bien hecha, muy bien ambientada, piensas que estás en ella y llegas a sentir ese miedo a no sobrevivir en una guerra. Sensación de tristeza por la pérdida inútil de vidas jóvenes en las guerras.
        
            