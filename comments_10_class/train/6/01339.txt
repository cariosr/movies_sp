
                      Género de Élite

Vuelve el cine que mas nos gusta,el cine de gangster de la mano de Ruben Fleischer y un gran reparto.La película prometía ser muy entretenida,con una historia sencilla,una buena fotografía como hemos visto en el trailer, y unos personajes dinámicos en el que destacaría el villano de Sean Penn,por lo que prometía,cualquier comparación con las míticas de gangster sería perjudicial pero después de ver la película te das cuenta de que es EXACTAMENTE LO QUE PROMETE. Es entretenida,tiene buenas escenas de acción y un buen desarrollo,quizás esperaba más por parte del personaje interpretado por Sean Penn,aunque éste esta genial,la fotografía muy bien en algunos momentos da la sensación de estar viendo un videojuego de dentro de 20 años y eso está bien,tiene un buen ambiente,una buena adaptación de los Ángeles y quizá un uso de la música mejorable.

En el resto del reparto Emma Stone hace un buen papel,el joven y prometedor Ryan Gosling ha demostrado no tener que demostrar nada más,y un Josh Brolin que se defendia bien frente a la interpretación eclipsante de Sean.

Lo mejor: Sean Penn enfadado
Lo peor: Que la película no te mete con totalidad dentro del filme y el personaje principal no interesa demasiado
        
            