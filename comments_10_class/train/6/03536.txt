
                      No soy un admirador del cine de Kaurismäki pero veo todas sus películas con mucho respeto por dos razones: Ha conseguido un lenguaje propio y sus historias siempre respiran una profunda compasión por los seres humanos desfavorecidos, marginados y un tanto al margen casi siempre de esta insaciable sociedad que no da nada sin alienarnos cada vez un poco más.
Pero yo voy al cine a emocionarme, a empatizar con los personajes de la historia o a admirarme por sus vicisitudes y con la forma de narrar de Kaurismäki me siento como si estuviera bailando con una mujer que me estuviera pisando continuamente y a la que no soy capaz de seguir el ritmo.
Porque ritmo narrativo no hay en el director finlandés. Pone escenas con intenciones un tanto infantiles en un momento dado, en otro hace un homenaje al cine de Tati, en otro se vuelve irónico, en otro nos muestra algo más propio del cine barato de la Mafia, en otro se pone trascendental y trágico y nos muestra como se rompe un matrimonio de la manera más cruda y así no hay manera de conectar con la historia que en este caso trata de las aventuras y desventuras de los refugiados de ahora (Siria e Irak) por Europa, y más en concreto en Finlandia.
Con unos personajes que parecen de cartón piedra, o robots, más próximos a las mascaras del teatro griego o japonés, más cerca del símbolo que de la persona, igual buscado conscientemente pues no en vano forman parte de este planteamiento cinematográfico unos planos largos, sin casi dialogo y unos actores a los que más de una vez les tiene que haber dicho Kaurismäki
- Al que interprete, lo despido.
Pues tenemos una película, otra, con muy buena intención social, muy original pero de escaso valor artístico.
No entiendo a que vienen tantos premios.
Kaurismäki es  en el cine como en la canción esos cantautores muy reivindicativos, con letras muy peleonas y voz muy personal pero que de música nada de nada.
De todas formas iré a ver la próxima que haga. Llámenme masoquista si así lo desean.
        
            