
                      El maleable Gus Van Sant

Agradezco que Gus Van Sant haga cine experimental, ajeno a los convencionalismos de la industria cinematográfica, ese que le ha permitido regalarnos trabajos más o menos acertados según los gustos de cada uno. Irregulares, sí, pero atractivos y estimulantes. “Mi Idaho privado”, “Drugstore Cowboy”, “Elephant” y, por qué no, su particular visión de “Psicosis” son buena muestra de ese interés por transgredir los encorsetados márgenes de Hollywood. Desgraciadamente, entre medias se ve obligado a aceptar proyectos más alimenticios, convencionales y comerciales, aunque pulcros y bien dirigidos, pero que se degustan tan rápido como se excretan, siendo quizá hasta la fecha “El indomable Will Hunting” su mayor acierto en este ámbito.

“Tierra prometida” pertenece sin duda a este segundo grupo de películas. Un vehículo de lucimiento personal para Matt Damon, que hace las veces de guionista y protagonista absoluto, en esta historia con el fantasma de la crisis financiera y los actos de las grandes corporaciones sobrevolando cada fotograma. El film pone como protagonistas a los malos, a los enviados por una gran compañía que pretende comprar las tierras de un pueblo tocado económicamente para extraer el gas natural que subyace bajo la superficie, sin exponer claramente a los habitantes los perjuicios que la práctica conlleva. 

Ya desde sus primeros minutos, se advierte el tono que Van Sant ha querido imprimirle a la cinta. “Tierra prometida” tiene un tufillo a película feel good, sus personajes son bondadosos, la banda sonora de Danny Elfman es jovial, el guión escrito codo con codo por Damon y John Krasinski destila buenas intenciones, y el semblante de gnomo de este último no ayuda a desprenderse de esta sensación. Ya el simple hecho de poner como protagonista a una malvada empresa debería ponernos sobre aviso. Sus intenciones son buenas, y en su tramo final sucumbe por fin a ese buen rollo que llevaba bordeando durante más de noventa minutos.

Aún así, no es una película incómoda a la vista. “Tierra prometida” es directa y clara tanto en la exposición de los hechos que presenta como en sus intenciones, y no lo oculta. Un producto ligero, amable, bien orquestado e interpretado, de esos que el maleable Van Sant hace para pagarse su próximo capricho. Porque sabemos que volverá a ofrecernos propuestas mucho más desafiantes en el futuro. Al menos siempre es gratificante ver de nuevo en pantalla a ese gran secundario que es Hal Holbrook. Consuelo de tontos.

A favor: que se degusta con suma facilidad
En contra: no pasa de ser un producto cargado de buenas intenciones
        
            