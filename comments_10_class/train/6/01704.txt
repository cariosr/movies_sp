
                      Hacía años que no veía una película buena de zombis y por fin ha llegado.

Train To Busan ha llegado para darle un aire fresco a los zombis actuales. Es una película muy completa y con muchos momentos agobiantes, no tiene grandes efectos especiales, pero son suficientes para narrarnos una cinta de este calibre, con un ritmo muy bueno y una trama diferente de zombis, gran parte de la película transcurre en un tren en el que los protagonistas se las tienen que ingeniar para sobrevivir en el tren de camino a Busan. Una de las novedades de estos zombis es que no son de los que van andando y silenciosos, son putas balas, corren y se retuercen muy locamente y a parte no pueden ver si no hay luz. Que es una gran novedad en este género, chapó por los zombis coreanos.
        
            