
                      Simpática comedia con Andreu Buenafuente y Berto Romero en el papel de dos antiguas estrellas del pop de los 80 que vuelven a su pueblo a dar el pregón de las fiestas. Sin ser de humor fino, tampoco cae en lo chusco o lo soez y te saca la risa en varias ocasiones, resultando muy divertida y entretenida. Para un rato ameno sin pretensiones
        
            