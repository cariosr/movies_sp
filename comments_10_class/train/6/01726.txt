
                      Critica Yesterday: La Apocalíptica Idea De Un Mundo Sin The Beatles.

Este jueves llega a los cines Yesterday, la nueva pelicula de Danny Boyle (Slumdog Millonaire) en la que un joven de hindu-británico se hace de la musica de The Beatles cuando estos dejan de existir luego de un misterioso apagón mundial.

Tenemos la historia de Jack Malik (Himesh Patel), un músico aficionado de Londres que luego de varios años de intentos frustados de ser una figura toma la decisión de dejar la musica. Hasta que una noche un apagón eléctrico en todo el mundo hace que The Beatles desaparezca de la faz de la tierra y solo Jack sea quien recuerde la obra de los 4 de Liverpool.

Hasta ahi la premisa del nuevo film del director Danny Boyle suena mas que interesante, divertido y totalmente apocalíptico. Jack rapidamente asciende a la fama con los clásicos temas compuestos por Paul y John , ayudado por Ed Sheeran – en una hilarante performance – quien lo suma como telonero de sus shows. Pero el guión empieza a flaquear ya que recurre constantemente a humoradas que no dejan nunca intensificar el drama del protagonista que siente la culpa de haberse apropiado de la música de The Beatles.

El ritmo por momentos es frenetico y ciertas animaciones en el montaje quedan muy despegadas y fuera del tono de la historia que se esta contando. Donde mas se destaca la mano de Danny Boyle es en el recorrido de Jack por Penny Lane, Strawberry Field y otras locaciones como la tumba de Eleanor Rigby que inspiraron a The Beatles a crear sus mas bellas canciones. Mas allá de la originalidad del concepto lejos está Yesterday de sentirse una película del director de Trainspotting.

En conclusión Yesterday es una película que presenta una genial idea con una torpe resolución, pero que aún así tiene momentos encantadores y llenos de nostalgia que celebran la mejor música del mundo.
        
            