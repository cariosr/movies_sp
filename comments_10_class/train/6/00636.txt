
                      La verdad es que si hay algo que se le da bien al cine francés es hacer comedia. Es cierto que casi todas las películas acaban teniendo un tono parecido, pero lo saben hacer bien. La película mezcla el boom reciente del fútbol en pequeñas localidades y poetiza un poco su influencia en dichas zonas. Entretenida.
        
            