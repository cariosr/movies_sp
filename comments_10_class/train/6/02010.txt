
                      Muy buenas, como lo prometido es deuda, esta vez nos toca hablar de un estreno, la secuela de una película de humor que en la primera parte dejó buen sabor de boca entre el público. Se trata de: Mi gran boda griega 2, dirigida por Kirk Jones.

Con la primera parte, dirigida por Joel Zwick, se consiguió una muy buena aceptación por parte del público pero bajo mí punto de vista Kirk ha resultado un tanto repetitivo y no ha podido superar el trabajo realizado en 2002.

Si el primer film se caracterizó por un humor catalogado como "fácil" o "sencillo" que atrapó a todo el mundo, en esta secuela, resulta un tanto exagerado volver al mismo punto de partida.

Dejando a un lado el estilo del humor que se muestra, la historia acaba resultando previsible y se centra en la vida de una adolesceesto transcurre mientras se suceden constantemente problemas dentro de la familia Portokalos.

Prácticamente con el mismo reparto de la primera producción, la película no sale de su encuadre pasado, y la parte positiva sería la interpretación de Lainie Kazan, a quién se le dan genial este tipo de producciones.

El antítesis de Lainie se encuentra en Andrea Martín, que da vida a una tía de la familia, pesada y sabelotodo su figura acaba por hacerse insoportable a mí forma de ver.

Comenzamos señalando a los papeles mas relevantes de esta producción, por un lado tenemos a la actriz Elena Kampouris que da vida a Paris una adolescente cansada de vivir atosigada por su familia y deseosa de escapar de la residencia familiar, malencarada y con un carácter duro. Realiza un buen trabajo.

Sus padres, caracterizados por Nia Vardalos (Toula) y John Corbett (Ian), luchan por encontrar momentos para estar a solas pero siempre surge algo que se lo impide. Nia está correcta, en cambio su compañero realiza un papel totalmente forzado. 

Para mí, los dos personajes que aportan alegria y grandeza a este film son los encarnados por Michael Constantine y Lainie Kazan como Gus y Maria, los abuelos de la familia, las personas que lo mantienen todo bajo control. En todo momento aportan un toque gracioso, en especial Lainie. 

Y por último, un papel ha destacar que en todo momento desata las risas es el de la Mana-Yiayia realizado por Bess Meisler. 

Los demás actores del gran elenco que posee esta producción, consiguen hacer de este clan una familia entrañable y graciosa. 

Una comedia para pasar un buen rato que lucha por hacer sombra a su primera parte pero que bajo mí punto de vista esta un escalón por debajo.
        
            