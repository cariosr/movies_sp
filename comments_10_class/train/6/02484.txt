
                      Una aventura muy tranquila que entretiene, con una historia muy normalita tirando a pasable y con un gran elenco de actores que no consiguen mejorar nada de la película.

Todo comienza con una familia en carretera atravesando un gran bosque de los Estados Unidos, luego tienen un accidente y solo sobrevive un niño que empieza a deambular por el bosque y casualmente se encuentra con un dragón el cual le acoge y lo cría. Después de unos años de estar viviendo felizmente Peter y su dragón Eliot, se topan con el ser humano y en un descuido Peter es descubierto por una niña y su madre (Bryce Dallas Howard) ahí es cuando empieza la película ya que Peter intentara volver a casa con Eliot y Eliot intentara encontrar a Peter através de su rastro. Podremos ver como unos leñadores se topan con Eliot y veremos como Robert Redford y Bryce Dallas Howard van en busca de Eliot con la ayuda de Peter. Un poco de emoción al final de la peli pero nada mas.

Una película sencilla de pasar la tarde que no llega a emocionar mucho su historia. Para pasar el rato nada mas.
        
            