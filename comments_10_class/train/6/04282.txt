
                      Con un cierto aire a Nordic Noir (no en vano es producción sueca), Fares Fares, co-protagonista de la saga de los casos del Departamento Q encarna al detective corrupto que se enfrenta al caso del Hotel Nile Hilton. Si bien tiene buena ambientación y Fares Fares resulta bastante solvente, la trama no acaba de tener la tensión necesaria para hacer de El Cairo Confidencial un buen thriller policíaco y por momentos te desengancha
        
            