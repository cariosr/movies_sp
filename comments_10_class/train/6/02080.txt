
                      Si el argumento de la película es tan simple que podría resumirse en una sola frase: (“ten cuidado con lo que deseas”, la estructura narrativa no es mucho mejor. Y, sin embargo, R. Leonetti, mostrando una vez más su experiencia y su talento, consigue transformar un guión previsible en una de esas películas comerciales que, si bien no pasaran a las páginas de oro del cine, cumple su función de entretener hasta el final al espectador.
        
            