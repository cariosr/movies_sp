
                      Glass (2019).
“Todo el mundo sabe que el cura muere al final de El Exorcista."
El inclasificable M. Night Shyamalan nos trae la tercera parte de la trilogía que iniciara hace casi dos décadas con “El Protegido”, continuara con “Múltiple” en 2016, y ahora termina con la paranoia claustrofóbica que nos ocupa. Para ello cuenta con los villanos de las dos anteriores cintas y con el héroe de la primera, el personaje de Bruce Willis.

Aunque ya se esperan giros argumentales cuando hablamos del  realizador indio que, como si de Hitchcock se tratara, aparece en cada uno de sus dramas a modo de cameo, lo cierto es que las más de dos horas de metraje de esta parte se hacen algo lentas. 
Ayudan a no mirar mucho el reloj la interpretación de Samuel L. Jackson, Willis, Anya Taylor-Joy -única superviviente de la segunda parte- y cómo no, James McAvoy con sus dos docenas de personalidades aflorando continuamente ante nosotros.

La sobriedad y la forma de manejar la cámara por parte de Shyamalan (quien como de costumbre hace las veces de productor y guionista) siguen siendo unas de sus señas de identidad, pero lo que más nos llama la atención es la continua metamorfosis de McAvoy; sobre todo cuando se transforma en “La Bestia” (atentos al maquillaje, efectos y, desde luego, el cambio físico al que se ha sometido el actor gracias a una sacrificada rutina). https://www.abc.es/play/cine/noticias/abci-exigente-rutina-convirtio-james-mcavoy-autentica-bestia-201901200124_noticia.html

La cinta se deja ver a pesar de que la crítica la ponga de buena... 😉 pero no supera, indudablemente, a su primera gran obra (“El Sexto Sentido”) para la que a finales del pasado siglo contó por vez primera con Bruce Willis.

Raúl Cabral.
https://www.filmaffinity.com/es/film271893.html
        
            