
                      Una película tremendamente emocionante y entretenida que te atrapa de principio a fin, pero que no es esa experiencia que dicen que es ni una obra maestra. Aunque es cierto que es un oasis entre tanta secuela y remake. 

Por encima de todo destaca, positivamente, el elenco, pese a lo desdibujados que están los personajes secundarios, razón por la cual quienes realmente se lucen son DiCaprio y Cotillard. No hay que olvidar la fantástica banda sonora, que acompaña perfectamente a las imágenes.

Sin embargo que Nolan racionalice tanto los suelos y que estos resulten tan cercanos a la realidad no hace más que demostrar una preocupante falta de imaginación (que en las películas de Batman jugó a su favor), debería aprender de Lynch, Buñuel, Satoshi Kon o Cronenberg. Otro defecto es que Nolan se cree muy inteligente y llena sus películas de lo que él cree que son verdades como puños, pero que en el fondo no es más que un maquillaje pseudo intelectualoide que no aporta ni fondo ni inteligencia. Además, tampoco es tan original como se dice y se me ocurren varias referencias como "Dark City", "Nivel 13", "Matrix" o "2001 Odisea del espacio" entre otras.

Aún así es un film emocionante y tremendamente entretenido que vale mucho la pena ver. Su gran logro es hacer que una película de más de dos horas y media se pase en un suspiro.
        
            