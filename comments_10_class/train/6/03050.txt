
                      Sólo se me ocurre una razón por la que se ha llegado hasta una tercera entrega: los fans están cada vez más y más hambrientos de la saga de Tom Six (me encanta hacer esta referencia en estas películas).

Six reúne a los dos villanos de las dos primeras entregas, Dieter Laser y Laurence R. Harvey, en el cierre de la trilogía que al igual que la segunda, se vuelven a marcar un “cine dentro del cine”. El que fuera un maníaco y enfermizo doctor en la primera parte y su adorado y nauseabundo fan en la segunda, son aquí un director de cárcel y su contable. Ambos trabajan en la penitenciaria más problemática y con mas gastos de EEUU. En un intento por cambiar esta situación, se les ocurre crear el ciempiés humano mas grande, un ciempiés con los quinientos reclusos. Y ahorremos en gastos (y en comida)..

Las dos primeras entregas se centraban en la propia asqueridad del elemento ciempiés: personas conectadas boca-ano. Sin embargo, en “The Human Centipede 3“, como buena “secuencia final” vamos al todo o nada: no solo tenemos escenas vomitivas por la unión de los reclusos, sino tambien por la demente y perturbadora personalidad del director de la cárcel y sus actos repulsivos. Mucho mas políticamente incorrecta que las anteriores, pero con mucho menos (o nada) nivel de suspense y momentos de sobresaltos. Aquí nos traen mas escenas gores, brutas, desagradables y retorcidas, en un ejercicio de superar los estómagos levantados de sus predecesoras.

El propio director ya ha comentado que no cierra las puertas a una cuarta entrega. ¿Veremos por fin a Dieter Laser (mientras, por favor, alguien le grita al oído) y a Laurence R. Harvey dentro de su ciempiés humano?

Lo mejor: su idas de olla.
Lo peor: que no se la terminen creyendo ni ellos.

NOTA: 7/10
        
            