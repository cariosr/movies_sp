
                      El planteamiento inicial es original y divertido (por ejemplo, la escena en IKEA es estupenda) pero poco a poco la cosa va perdiendo fuelle y el final es de un convencional que asusta. Eso sí, Zooey Deschanel es preciosa. La pondría en el salón de mi casa si puediese. Y la música en general está bien, aunque hay un momento en el que tanta canción indie metida con calzador acaba resultando estomagante.
        
            