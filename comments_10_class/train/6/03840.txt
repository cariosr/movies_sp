
                      Vuelta de tuerca a la original Jumanji del 95, dejando atrás el tono más oscuro de aquella y centrándose en la belleza visual del entorno en una aventura al estilo de videojuegos como Uncharted y cambiando el tablero por una consola. Resulta sorprendentemente entretenida, con mucho ritmo, siendo un blockbuster para toda la familia que no aburre para nada.
        
            