
                      Como viene siendo habitual, el Chaplin cierra trimestre recuperando un clásico, en esta ocasión con Pasolini (que tiene un pequeño papel de pintor en la película) y esta obra que causó bastante revuelo por el tratamiento que hace de las relaciones a través de diversas historias de contenido jocoso y erótico, una de ellas protagonizada por unas monjas bastante libidinosas. Por lo demás ha envejecido bastante y ha perdido parte de su poder transgresor y provocador.
        
            