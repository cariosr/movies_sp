
                      La película no está mal, es bonita y es fácil que casi te identifiques con ella, sin embargo, lleva un ritmo demasiado acelerado y te lleva a no terminar de identificarte y emocionarte del todo con ella, con esto me refiero a que cambia una relación de 5 años por un chico que ha conocido de apenas... ¿un mes?

Por lo demás, no está mal. Es bonita, aún más cuando la disfrutas con tu pareja en el cine y os ponen una fila de dos asientos al otro lado de la escalera.
        
            