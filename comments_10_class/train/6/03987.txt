
                      Steve Jobs (2015).
"No es binario; puedes tener decencia y talento al mismo tiempo". Pero el personaje que el responsable de "127 Horas" nos presenta no tenía mucho de simpático y sí mucho de déspota, aunque no dejaba de ser un genio.
Como en cada trabajo en el que participa el gran Michael Fassbender, aquí lo borda, y no solo gracias a él, sino al magnífico guión de Sorkin y a la secundaria de lujo que lo acompaña, Kate Winslet. 
Danny Boyle, experimentado director de actores, nos cuenta esta historia a base de grandes diálogos de Jobs con sus allegados y cercanos colaboradores (en la cinta encarnados por Seth Rogen y Jeff Daniels entre otros) y de las presentaciones de algunos de sus productos estrella (y de varios de sus fracasos), culminando con el inigualable iMac, desde uno de los cuales escribo estas líneas.

Aunque tenemos un metraje de dos horas, la actuación de su protagonista y su forma de llenar la pantalla nos hace no mirar el reloj si te metes en la historia de lleno.
Raúl Cabral.
        
            