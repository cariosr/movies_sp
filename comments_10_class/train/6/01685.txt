
                      Bien construída, no exenta de casualidades. La entereza de una madre, aunque tardía, de buscar al hijo que le arrebataron. Sin embargo, no estraña la capacidad de comprensiòn de una madre, cuando descubré  toda la realidad.
        
            