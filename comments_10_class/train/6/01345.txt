
                      Antes de empezar quiero aclarar dos cosas: la primera que soy un admirador de la carrera de Clint Eastwood, en todas sus facetas y la segunda que soy bastante escéptico en estos temas de médiums y todo lo relacionado a ello. 

En cuanto al largometraje en cuestión, no considero que sea uno de los mejores trabajos del cineasta, pues considero a Sin Perdón su obra maestra, y entre las últimas realizadas me quedo con Gran Torino y Million Dollar Baby.

Realizada en una línea clásica y sosegada, al estilo de anteriores largometrajes como Los puentes de  Madison, o la más reciente Invictus. Esta película no consigue calar en los sentimientos de los espectadores. También es de agradecer que no recurra al melodrama fácil. El comienzo con el Tsunami, tal como se muestra en el tráiler hay que reconocer que es espectacular, se nota la mano de Steven Spielberg aunque sea de productor asociado, pero poco a poco la trama se va moderando, eso sí, sin llegar al aburrimiento en los 120 minutos de duración.

En resumen: Cine bien hecho, pero sin profundidad.
        
            