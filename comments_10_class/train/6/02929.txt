
                      Entretenida película que te hace disfrutar de un buen rato, aunque la primera estuviera mucho mejor que esta.

La peli empieza con Zipi y Zape escribiéndole una carta a los reyes magos y diciéndoles que este año no los necesitaban para nada ya que ellos se encargarían de hacerse sus propios regalos, como es lógico la gamberrada se les va de las manos y acaba en incendio. Al padre de los gemelos le llaman para presentar un libro en una isla y allí que se va la familia al completo pero al poco de llegar se pierden y acaban en un orfanato gobernado por Elena Anaya. Ahí empieza la aventura cuando la gobernanta les cuenta que sus padres se han ido sin ellos y ellos intentan integrarse en el grupo de chicos que ahí allí. Descubren los secretos del orfanato y forman un grupo de amigos de lo mas peculiar. Hasta el final que se descubre el gran secreto que guarda la gobernanta e intentan truncar sus deseos.

Una película muy parecida a El Hogar de Miss Peregrine, pero sin tantos efectos especiales y con una historia mas que normalita con unos personajes sacados de un cómic. Entretenida nada mas.
        
            