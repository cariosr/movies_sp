
                      Atómica (2017).
Casi dos horas de belleza en acción. Charlize Theron vuelve a la gran pantalla con un papel digno del mejor 007, del más mortífero Bourne o del más eficaz Jack Reacher con esta cinta de espionaje y violencia de lo más realista (haremos alguna concesión para ello...).

El director del primer John Wick se ocupa de mostrarnos el Berlín de finales de los 80's (incluida la magnífica música europea de esa década) donde se desenvuelve y desarrolla la historia (algo enrevesada, la verdad), ambientada en el final de la guerra fría.
Acompaña a nuestra preciosa protagonista el cada vez más cotizado James McAvoy (deja las fantasías de Wanted, con su compañera de reparto Angelina Jolie para pasar a esta película mejor facturada y bastante más entretenida).

Lo mejor, a mi entender, las coreografías de lo más violentas y bien hechas. Lo peor- aparte de lo lioso de la trama- el vicio continuo de todos los personajes de vivir unidos a un cigarro hasta para ducharse...

Raúl Cabral.
        
            