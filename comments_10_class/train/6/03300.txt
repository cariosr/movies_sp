
                      Si eres fan de Jason Statham estoy seguro de que te gustará, y si te llaman las películas ambientadas en el mar pues aún más. Es cierto que la acción no para y la película tiene unos efectos especiales muy buenos, tanto por la recreación del "Meg" como del entorno marino. Por lo demás no hay mucho donde rascar, todo se centra en el "bicho" y pensar las maneras de cómo acabar con él. Este film provoca cierta nostalgia a los que vimos en su día la saga tiburón y se agradecen estos remakes refrescantes aunque queden lejos de ser buenos.
        
            