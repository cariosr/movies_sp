
                      Una película dramática de impecable factura técnica y que cuenta con un gran trabajo por parte del reparto, destacando a la actriz principal, Montse Germán, que refleja de manera muy cercana el dolor que experimenta su personaje. También es reseñable el modo en el que la música ambienta la historia, a cargo de Gerard Pastor, que depara momentos realmente conseguidos en medio de un metraje no demasiado frenético (tampoco lo pretende), pero que deja un buen sabor de boca.
        
            