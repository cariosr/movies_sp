
                      Es cierrto, hay varias cosas inverosímiles en la película, pero después de todo se trata de cine. Dicho esto, uno no se aburre, hay un buen ritmo, hay acción, mucha (demasiada a mi parecer) violencia, una punta de exotismo y evasión entre Colombia y los Estados Unidos, una banda sonora original simpática y una  actriz  principal con talento. Mi conclusión: está más bien lograda, aunque no se trate de la película del año. ¡Merita ser vista!
        
            