
                      El ritmo es laborioso (la productora Apatow obliga a una broma cada cinco minutos) y Kristen Wiig tiene una tendencia de ocupar mayor escena en detrimento de sus compañeros más carismáticos (Ellie Kemper, muy poco presente, es adorable) pero el conjunto constituye un buen rato de diversión si nos gusta el humor trash (aquí bien gratinado) y le da justo al clavo al tema de una amistad molesta.
        
            