
                      Una nueva versión de 'Orgullo y prejuicio' pero basada en una novela posterior al que añadimos una nueva variante: los zombies. Y, francamente, es una película que no está mal. Desarrolla la historia de los orgullos y prejuicios aristocráticos con su relación con el pueblo. Conspiración y amores. Mejor unos actores/actrices que otros. El caso es que se puede tomar hasta con humor y gracia. Vale la pena ver esta versión basada en la obra de Seth Grahame-Smith y a su vez de Jane Austen.
        
            