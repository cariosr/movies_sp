
                      Una comedia fresca y original que se deja mirar con placer. Demaison y Efira son encantadores , tanto como Rafael Personaz. Sin embargo, la actuación realmente exagerada de Elie Semoun  estropea un poco la película. Fuera de esto, es una película divertida, los actores son buenos. Un muy buen momento !
        
            