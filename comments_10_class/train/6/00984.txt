
                      Domingo por la tarde, primera sesión, poca gente en la sala, palomitas, refresco de cola, y en la pantalla una de Superhéroes. ¡Pinta bien! Hombres enmascarados, efectos especiales, acción a cámara rápida, cámara lenta y a veces violenta. A pesar de su larga duración, no se hace pesada (salvo en algunos momentos filosóficos), donde yo la vi, hicieron intermedio. Sin llegar a ser una buena película, entretiene. La única pega quizás la caricatura del personaje de Nixon. ¿Por qué no contrataron a Frank Langella?
        
            