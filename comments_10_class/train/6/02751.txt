
                      Es una película de acción que tiene amor, humor y mucha aventura. Estás dos horas la mar de entretenida viéndola. Como "pero", solamente decir que su guión no es muy original: si escarbamos un poco podemos intuir "escenas" que aparecen en otras películas. 
Aún así estupenda para pasar un buen rato.
        
            