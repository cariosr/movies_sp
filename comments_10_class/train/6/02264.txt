
                      Algunas son graciosas, es cierto, y la repartición de la película es agradable. Dicho esto, la película parece dudar entre el humor trash y el romance, una crónica un poco fuerte y una comedia gentil. El resultado: bajas en el régimen de tiempo y un interludio "scato" poco necesario.
        
            