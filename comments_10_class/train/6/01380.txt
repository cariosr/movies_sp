
                      Mortdecai es una comedia dirigida por David Koepp, director de La Ventana Secreta y Ghost Town.

Mortdecai es un conocido marchante de arte que anda en busca de un famoso cuadro perdido que contiene los números de una cuenta bancaria, en la que hay suficiente dinero como para cubrir todas las deudas que ha ido acumulando.

Es una película infravalorada. El reparto de brillantes actores, encabezado por Gwyneth Paltrow, Ewan McGregor, Paul Bettany y Johnny Depp y la dirección de David Koepp, dan esperanzas y crean grandes expectativas que luego pueden quedarse en nada, por lo que la crítica negativa ha sido mayor.
Es verdad que los diálogos no son demasiado ingeniosos, y el humor que impera es simple y predecible. Sin embargo es entretenida, y los escenarios y la fotografía son bellos.

La película recibió tres nominaciones a los premios Razzie, incluyendo peor actor, Johnny Depp. Lo que él nos muestra con este personaje es otra de sus múltiples facetas, humorística y disparatada. De nuevo, algo totalmente diferente a todo su trabajo anterior.
        
            