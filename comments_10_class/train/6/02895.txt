
                      Jose U.
Danzon
	Una mujer soltera tiene que embarcar en el viaje de su vida cuando su pareja de baile se desaparese. Julia, la mujer, deja todo atrás en esperanza de reunirse con su pareja de baile. Durante su viaje Julia busca a Carmelo, su pareja de baile, pero en realidad se encuentra a sí misma. La película está llena de escenas que tienen buen sentido y que expresan la ideología feminista.
	Maria Rojo, la mujer que hace el papel de Julia, se convierte en una mujer independiente que inicialmente era una mujer sin su propia voz. La directora María Novaro, una de las directoras más prominentes del Nuevo Cine Mexicano, captó la ideología feminista con una historia simple y entretenida. No se arrepentirá de mirar esta película ya que las actuaciones de los personajes son buenos y inspiran la humildad.
