
                      ]“La invención de Hugo” o “Sin propósitos en esta vida estás averiado”

 

Hugo es un joven huérfano que vive en una estación de trenes y se encarga del reloj de la torre de la estación a escondidas, para no ser atrapado y llevado a un orfanato.

 

Hugo sabe reparar relojes y se dedica luego de la muerte de su padre a reparar a un Autómata, una especie de robot que en este caso escribe. Hugo sigue los pasos de su padre en el entusiasmo de “reparar” las máquinas y esta huella lo lleva al encuentro con la historia del cine en un director olvidado.

 

Hugo insiste, no se detiene, ni duda en ningún momento en reparar su autómata, y como buen “reparador”, hace lo posible para reparar algunos malos entendidos con el viejo director. Se lo ve en el film arreglando desde un ratón a cuerda hasta el problema que enfrenta el director con su pasado en un presente de olvido. 

 

El psicoanálisis es una aventura por los desfiladeros del deseo y los síntomas son a veces la puerta de entrada para descubrir, digámoslo en términos de Hugo: “el propósito que tenemos en esta vida”.

El interés para mi recae en una frase pronunciada por este joven: “Si pierdes tu propósito estas averiado” Y es que esa es la premisa de Hugo: “todas las máquinas tienen su propósito, todas las piezas encajan en la maquinaria de este mundo, no hay ninguna pieza que sobre”.

 

El propósito es la cuestión en este film:

En otro momento Hugo le cuenta a su joven amiga que un hombre de la estación le dio unos libros y la muchacha le responde: “Siempre hace eso. Le da a los libros un buen hogar. Así lo llama él. Tiene un verdadero propósito.”

Hugo nos cuenta acerca del fundamento de su hacer y lo que lo lleva a ese hacer:

“Todo tiene su propósito. Hasta las máquinas. Los relojes te dicen la hora. Los trenes te transportan. Hacen lo que deben hacer. Quizá por eso las máquinas rotas me ponen triste. No hacen lo que deben hacer. Quizá sea igual con las personas. Si pierdes tu propósito es como si te rompieras.” 

 

P.D.: A propósito, que sea en tres dimensiones favorece en disfrutar la magia del cine desde sus inicios.
        
            