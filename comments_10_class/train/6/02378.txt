
                      No aporta nada nuevo. Está hecha para el disfrute de la vista con unos efectos especiales impresionantes que están presentes durante toda la película.
De lo que no cabe duda, es de que Cameron sabe lo que busca el espectador, y lo consigue de una manera excelente. Los más de 2.700 millones de dólares recaudados lo demuestran.
        
            