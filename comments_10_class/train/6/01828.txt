
                      Nueva entrega de la saga James Bond 007, esta vez con una trama relacionada con las películas anteriores. Película de larga duración en la que llega el punto de que se hace larga y algo pesada para el espectador, eso sí efectos especiales bien, entretenimiento bien, imagen visual muy bien, acción excelente, la única pega es que se hace larga y cuándo crees que va a acabar no termina. Pero la trama es para mi una de las mejores de James Bond.
        
            