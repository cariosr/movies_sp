
                      A J. A. Bayona le vuelve a pasar lo mismo que en "El imposible". A una primera media hora larga llena de fuerza e impactante le sigue un buen puñado de minutos construidos para llenar más que para sumar complejidad e interés en la cinta.
Me quedo sentado en la silla observando como la sala se vacía con un silencio que demuestra que la cinta ha conseguido lo que pretendía. No se puede negar en absoluto esta conquista. Otra cosa es que una vez pasan algunas horas todo se desinfla y se convierte en un mero entretenimiento de una eficacia magnífica. Poco más. (6/10)
        
            