
                      Una Vida a lo Grande (2017).
"No es el Homo Sapiens una especie de mucho éxito: ha sobrevivido 200.000 años. El cocodrilo ha sobrevivido 200 millones de años..."

El director de los plomazos A Propósito de Schmidt y Los Descendientes nos trae esta historia de ciencia ficción, con cierto interés en la premisa que presenta pero que se desinfla por más que quieran darle chispa al guión a base de giros supuestamente originales...

Encabeza el reparto Matt Damon encarnando a un hombre gris, simplón, sin suerte, ingenuo y con las compañeras de alcoba menos atractivas de su carrera. Y aunque el resto del reparto lo completan secundarios bien valorados (especialmente el infalible Christoph Waltz, quien hace de su irónico vecino) como Jason Sudeikis, Joaquim de Almeida o Laura Dern para, en algunos casos, solo tener un par de frases de guión, lo cierto es que a la tragicomedia que nos ocupa no la sube de un suficiente ni la música de vals que le pone banda sonora. Y eso que hasta sale un conocido bar sevillano al principio de la peli...

Premios y nominaciones no le faltan, pero... cortito con sifón, la verdad.

Nota: atentos a la déspota vietnamita del cast; posible nominación al Oscar. Al Globo de Oro ya lo está, entre otros muchos premios de nivel.

Raúl Cabral.
https://www.filmaffinity.com/es/film115853.html
        
            