
                      “…Llego a ser un guerrero tan terrible que ejércitos enteros se batían en retirada con la simple mención de su nombre, Vlad el Empalador, el hijo del dragón…”, es una de las frases introductorias con las que se nos presenta a Drácula, y la que me hizo pensar que iba a ver un Cid Campeador a la rumana.

En cuanto al argumento de la película, es satisfactorio la visión de Drácula desde un punto de vista alternativo al que la historia nos tiene familiarizado, Drácula no es ese ser terrorífico que mata sin compasión con el único fin de satisfacer su sed de sangre, sino que va a ser un héroe dispuesto a todo para salvar a su pueblo.

En el lado negativo es que a pesar de contar una historia sobre Drácula diferente a la que estamos acostumbrados no deja de ser una película de relleno. Si esperas una obra maestra del terror es mejor que te quedes en tu casa antes que ir al cine, realmente la definiría como una mera película de acción escasamente entretenida, película que nadie recordará y en la que solo destacan los efectos especiales.
        
            