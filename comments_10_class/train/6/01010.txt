
                      Me gustó. La vi con pocas expectativas ya que tiene malas críticas, pero realmente me entretuvo. Describe con bastante aproximación la personalidad prototípica del actor, su egocentrismo y esa búsqueda insegura de reconocimiento constante. Destacada actuación de ambos. La recomiendo.
        
            