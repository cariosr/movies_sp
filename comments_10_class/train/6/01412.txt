
                      Segunda entrega de la saga Divergente, que incluye muchas más acción y mejores efectos audiovisuales que su predecesora, pero con un guión más mediocre y un drama más inmotivado.
Los protagonistas (Woodley y James) aun no logran resaltar y mostrar su máximo talento. 

Aun así el final de ésta entrega nos deja con ganas de más.
        
            