
                      Película para disfrutar de la parte más técnica del cine. La fotografía y el sonido son sublimes. Hay 3-4 escenas que te sobrecogerán, recordarás al salir de la sala y que tienen una intensidad suficiente para hacer que merezca la pena ver la película. Lo malo es que el resto de ella no es así. Dura 2 horas y media y te parece incluso más. Es muy, muy lenta. Además el tráiler se encargó de destripar todo el argumento. Di Caprio está bien aunque creo que con pocos registros en este papel. No me parece ni de lejos su mejor interpretación si al final se lleva el Óscar.

Lo dicho, buena película e increíble realización, pero es demasiado pausada.
        
            