
                      Aunque está claro que los fans del género no van a poner muy por debajo ninguna de las películas de la saga y la van a ver como un todo (lo cual no deja de ser cierto), no deja de ser verdad que con esta sexta entrega hay algún altibajo en la trama y da a veces la sensación de ser un interludio antes de la traca final más que un interesante capítulo en sí mismo. De todos modos sigue teniendo ese ambiente oscuro del que hace ya varias entregas que no se despega y un reparto de secundarios de lo más atractivo.
        
            