
                      En mi opinión como espectador,El Increíble Hulk es la mejor película que se ha hecho del personaje,pero no está para nada al nivel de Iron Man,su película antecesora en este universo cinematográfico,a pesar de tener buenos y convincentes efectos especiales y elenco.Un buen punto en la trama,al igual que iron man,es el protagonista,Edward Norton hace un gran trabajo como Bruce Banner,y el resto del elenco tampoco se queda atrás,Hulk es imponente y su lucha contra La Abominación,quien tampoco resulta ser "El Villano",es de lo mejor en la película,pero no compensa a la trama,la cual es bastante olvidable y poco épica en comparación a otras películas del género,en algún punto se puede llegar a sentir algo lenta,pero lo compensan la acción y los efectos especiales.En resumen es la película del hombre verde es irónicamente,una película débil para un fuerte personaje,pudo haber sido mejor,aunque para nada es una mala película,no es de esas que podrías repetir 5 veces en poco tiempo sin llegar a cansar,y no alcanza el clímax esperado que nos deja satisfechos,recomendable para aquellos que les guste Marvel.

Puntos a favor:Edward norton ,efectos especiales,escenas de acción.

Puntos en contra:Trama bastante lenta y olvidable,villano fuerte físicamente,pero no estructuralmente.

Mi calificación:6/10
        
            