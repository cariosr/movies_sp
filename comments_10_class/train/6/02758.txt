
                      Un abordaje a la disciplina del kung-fu y la diferente utilización que se le da en oriente y occidente. Una historia maquillada para Hollywood de un enfrentamiento real ocurrido en 1964. Entretenida, bien interpretada y con buenas peleas. La recomiendo.
        
            