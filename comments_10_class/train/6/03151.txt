
                      Vale la pena ver la película, ese es el resultado final. Quizás no ames a los personajes, pero esa no es la verdadera atracción de todos modos, son las secuencias de acción, que son muy buenas. Roeper se entusiasmó con Prometheus y le dio un A +, sin embargo, el desarrollo del personaje en esa película fue horrible (pero excelentes imágenes y también vale la pena verlo). Muy pocas películas son perfectas, pero esta película hace lo suficiente para obtener una buena calificación, esa es la conclusión.
        
            