
                      Los largometrajes de Almodóvar, no es que me entusiasmen, la verdad, voy a verlos más por curiosidad que por agrado, tampoco es que haya mucho donde elegir en la cartelera. 
Pedro Almodóvar es como Woody Allen en español, sale a una producción anual y en ellas suele incluir sus tics, sus manías y sus desvaríos. Hacen largometrajes personales, que o te gustan o detestas. Tienen sus seguidores, que les aplauden aunque hagan una mierda (siempre será una mierda de Allen, o de Almodóvar),  y  sus detractores que siempre le sacaran pegas aunque esta vez hayan realizado una obra maestra.
 Este largometraje es la historia de una venganza y sus consecuencias. No se le puede considerar una película de terror, sino más bien un drama psicológico. Pues todos los protagonistas se nos presentan como seres delirantes y enfermizos que causan más lástima que miedo. 
En lo que se refiere a las interpretaciones, Elena Anaya consigue destacar dentro del equipo actoral. En el equipo técnico, la fotografía y la música, están bastantes conseguidos.
        
            