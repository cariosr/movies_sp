
                      El cine no siempre te tiene que hacer pensar, comerte la cabeza, recapacitar o simplemente alterar tus emociones. También necesitamos que nos den una historia con la que sentirnos identificados y reírnos de eso. Descubrir que no somos los únicos que tenemos problemas y que los personajes de una película los solucionan tomándoselo con humor. Y es que si hay algo que todos los espectadores necesitan de vez en cuando es reírse.

Con dos carreras, tres masters y mil ganas de comerse el mundo laboral. Así comienzan los protagonistas de “Perdiendo el norte“. Pero pronto descubren que si en tu país no encuentras lo que necesitas, quizás debas buscarlo fuera. Lo que en un principio iba a ser una oportunidad de evolucionar, puede convertirse en todo lo contrario..

Nacho G. Velilla, creador de dos de las mejores series de televisión de este país: “Siete vidas” y “Aída“, se embarca con dos jóvenes promesas salidas también de la televisión, en una comedia que hereda el humor de sus series. Rebosando chistes ingeniosos y sarcasmo, “Perdiendo el norte” sirve única y exclusivamente para pasarlo bien. Al igual que con “Ocho apellidos vascos“, “Las ovejas no pierden el tren“, “Fuga de cerebros” y “Ahora o nunca“, la película utiliza un plantel de secundarios de lujo para atraer y poner ese punto de calidad interpretativa (si es que le faltara, no es el caso). Jóvenes que buscan su sitio, como tantos y tantos espectadores que se sentirán identificados, y un humor irreverente. Esa son la bazas de una película que sin grandes pretensiones consigue lo que se propone: que lo pases bien y disfrutes de un rato de humor.

Lo mejor: José Sacristán. Único.
Lo peor: siempre se echa de menos en estas películas más protagonismo de sus secundarios.
        
            