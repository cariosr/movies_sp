
                      Ha tenido su interés descubrir un personaje como Emily Dickinson, mujer y poetisa en permanente conflicto  con una sociedad encerrada en unos atavismos demasiado latentes. Diálogos densos cargados de sentido y de sarcasmo. Otra obra de Terence Davies dedicada al inabarcable universo femenino.
        
            