
                      Me esperaba algo mas de una película protagonizada por Mel Gibson. Acción la justa para no dormirte.

Todo empieza con una chica ayudando a su novio a atracar en una casa, todo se tuerce y la chica le pega un tiro a su novio matándolo, sus compañeros van tras ella, pero les da el esquinazo y se va con su padre Gibson. Ahí empieza la parte "interesante" de la película, Gibson hace todo lo posible por que su hija salga sana y salva después de descubrir lo que a echo y descubrir que mafioso anda detrás. Durante hora y media veremos como Gibson va de contacto en contacto para juntar información, también veremos algo de acción (muy poca) y al final la cosa acaba de la manera mas simple que se podía, a tiros.

Este no es el Mel Gibson de Arma Letal o el de Vacaciones en el infierno, aquí veremos a un Gibson que le encanta hablar y mete tiros los justos. Espero que la próxima que haga sea el chico de acción que yo conocía.
        
            