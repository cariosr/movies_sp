
                      ¿Entretiene? Sin lugar dudas, no lo niego, pero no no hay que esperar más. Tom Holland me gusta como Peter Parker, el personaje que le queda bien, y Michael Keaton es un Buitre estupendo, pero... ¿Me lo parece a mí solo, o cada película de Marvel que veo está más enfocada al público infantil...?

La trilogía de Sam Raimi me encanto, (no tanto la tres), pero prefiero la oscuridad del Spiderman de Marc Webb, para mí, el mejor hasta ahora. Me quedé con muchas ganas de una tercera.

No esperéis nada épico de esta nueva entrega, en la que se le da demasiado protagonismo al apartado cómico y se da por hecho que ya sabemos muchas cosas del pasado de Peter Parker.  Si habéis visto los trailers, os han fastidiado la mitad de las escenas de acción, y solo os queda lo impredecible de algún giro del guión que sí sorprende.
        
            