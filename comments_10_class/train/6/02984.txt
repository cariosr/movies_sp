
                      Utilizando casi un argumento gemelo al de su predecesora, la historia carece de la originalidad que debería de ofrecernos un estudio tan celebre como Pixar. Es un hecho que en los últimos años todo el trabajo del estudio ha sido hacer secuelas de sus creaciones, pero no creo recordar una secuela donde la historia sea tan parecida a su predecesora como me ha pasado al ver ésta. A pesar de la carencia de un argumento genuino y único, Buscando a Dory cumple los requisitos para pasar un buen rato viéndola en familia.
        
            