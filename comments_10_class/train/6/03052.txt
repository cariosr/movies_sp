
                      Una película muy curiosa. Primero yo no me la esperaba así y segundo yo no veía ni a Tom Hanks ni a Julia Roberts en estos papeles. Divertida e inesperada, los diálogos están a veces fuera de la norma y el rodaje es algo cercano, pero encontramos una historia original y simpática.
        
            