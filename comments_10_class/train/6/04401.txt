
                      Es una película perfectamente realizada y un trabajo de animación increíble. No he visto la de dibujos así que no puedo comparar pero me ha faltado algo más de emoción. Creo que al ser los animales tan reales les quita la posibilidad de proyectar más emoción en sus caras. De todas maneras es recomendable verla y no sólo para niños.
        
            