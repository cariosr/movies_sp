
                      “¿Es Captain Marvel una película necesaria para el MCU?”
La ganadora del Oscar Brie Larson interpreta a Carol Danvers (Captain Marvel) una guerrera “Kree” la cual hace parte de la “Fuerza Estelar” entidad encargada de poner orden en la galaxia, pero Danvers pronto descubre que no todo es lo que parece.   

La película tiene momentos divertidos, buenas interpretaciones (Larson, Jackson y Law) y una buena ambientación, pero la historia es simple el guion es plano y al final no logra el impacto deseado. Brie Larson como Captain Marvel hace un buen trabajo que puede llegar a perfeccionar en futuras películas. Samuel L Jackson como Nick Fury nos muestra una faceta distinta del personaje la cual puede ser aceptada por unos y rechazada por otros. Jude Law como Yon-Rogg comandante de la fuerza estelar hace un trabajo notable, pero por culpa de falencias en el guion de la película su personaje resulta olvidable. 

En resumen, Captain Marvel es una película agradable correcta, pero una más del montón en el genero de superhéroes, se nota que Marvel realizo este proyecto solo por el marketing de su próxima película. 

Lo Bueno: la película no resulta aburrida, tiene momentos e interpretaciones agradables.

Lo Malo: Marvel se apresuro en este proyecto por lo tanto se nota que tiene falencias las cuales se pudieron evitar si la producción hubiese tomado mas tiempo.
        
            