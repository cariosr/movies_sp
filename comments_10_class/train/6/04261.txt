
                      Me ha defraudado un poco, quizás por las expectativas que tenía en la ganadora del Óscar este año por un rebote. Tres historias, tres tiempos y un personaje bien tratado con cierta sensibilidad y sin concesiones a lo más tópico en unas cosas, en otras no tanto (drogas, trapicheos, violencia...) aunque la película se va desinflado poco a poco. Muy meritorios las interpretaciones, el guión flojo.
        
            