
                      Una historia de amor-odio, amor interesado, amor acaparador. Y por otro lado una historia de sacrificio altruista, amor al prójimo. Una historia con humor negro y amargo.
Una historia que podía haber ocurrido en cualquier pueblo de España. Miedo a la soledad, evitándola aunque sea a costa de la bondad de los demás. Una historia de grandes interpretaciones, especialmente la de Adriana Ozores. Divertido largometraje para una tarde de domingo en el cine.
        
            