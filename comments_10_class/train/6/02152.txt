
                      Domingo por la tarde, hacia buen tiempo para tomar el sol, era la primera sesión y en la pantalla una película de un héroe de Marvel. Largometraje repleto de acción y efectos especiales, con algún que otro giro en el guión. Largometraje que entretiene, lo suficiente para hacerte olvidar que el día siguiente es lunes. Precuela entretenida y amena, (en algunos momentos me recordó a Monstruos contra Alienígenas).
        
            