
                      El interés de este thriller radica en la relación elaborada entre personajes, pero falla su división en dos partes y la diferencia de tratamiento entre partes. La parte histórica es hechizante, bien escrita y bien interpretada. La segunda parte me parece una chapuza, no se reconoce para nada a sus personajes. Los tres actores "viejos" no aparecen en esta parte. De repente, es como si estuviera viendo otra película. A pesar de esto, en conjunto está bien llegando incluso a hacer creer, a veces, que toda la historia es verdadera.
        
            