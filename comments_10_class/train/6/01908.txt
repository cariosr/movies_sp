
                      La Visita es una cinta de terror con un toque claramente distintivo que sabe jugar a la perfección con el terror y el humor, sin embargo una idea buena no siempre consigue una buena película, su forma de grabar es ortopédica e incomoda, y las escenas que de verdad aportan algo a la cinta son escasas.
        
            