
                      Tengo la sensación que JA.Bayona acaba estando a la altura de la película, pero la película no acaba estando a la altura de JA.Bayona. 
Floja en su triste y repetitivo argumento, tan solo sorpresivo en su tramo final y en su desenlace; el film solamente se salva por el buen hacer del director en su segundo tramo, en el que se lleva la acción a un (su) terreno más intimista, pese a la grandilocuencia de la trama; y también se salva por la que, probablemente, sea la mejor master piece de toda la saga: la secuencia de la erupción del volcán. 
Pese a todo: entretiene, que al final es lo que cuenta...
        
            