
                      Quien a Hierro Mata (2019).
“...a hierro muere.” (Evangelio según S. Mateo).
Paco Plaza deja a un lado el terror de la trilogía de “Rec” para introducirnos en una historia mucho más terrorífica: el narcotráfico y sus consecuencias.

Será el actor gallego Luis Tosar quien se meta en la piel de un enfermero (también gallego), que esconde un duro pasado en el cual influyó fatalmente el anciano al que ahora le toca cuidar en la residencia de la tercera edad en la que trabaja.
Drogas, mafias gallegas, colombianas y china, intereses y venganzas conforman esta interesante historia con giro argumental incluido.

La cinta, rodada a caballo entre La Coruña y Pontevedra, aprueba tanto para el público como para parte de la crítica, y consigue “engancharnos” gracias a la relación que se forma entre nuestro protagonista (el enfermero a punto de ser padre interpretado por Tosar), y al narcotraficante en sus últimos días; el actor y escenógrafo Xoán Cejudo, desaparecido justo hace un año, y que no pudo ver el estreno de esta película a quien se le ha dedicado.

Digno de mencionarse es la caracterización de Cejudo para la ocasión.

Raúl Cabral.
https://www.filmaffinity.com/es/film713289.html
        
            