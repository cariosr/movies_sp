
                      Buena película dramática que nos cuenta la crudeza de la vida en distintas zonas de África donde los grupos rebeldes asaltan, secuestran, violan y matan con toda impunidad y utilizan a los niños y jóvenes como trabajadores en las minas de Coltan así como de soldados. Gran papel de Belén Rueda en la búsqueda de su hermana cooperante en estas tierras.
        
            