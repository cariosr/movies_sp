
                      Impresionante actuación de Oscar Isaac. Una de las interpretaciones más veraces que he visto en mi vida. La línea de la película es inconfundiblemente obra de los hermanos Coen, quienes no vuelven a fallar con sus formas.
Si es cierto que en ciertos momentos la película se queda sin fuerza, pequeñas escenas que a nivel general no tienen tanta importancia pero sí decae un poco la genialidad de la película.
Os invito a que os paséis por mi blog para ver una crítica personal, más amplia por supuesto, además de estar complementada con detalles técnicos.
        
            