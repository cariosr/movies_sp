
                      Me ha parecido una película con una idea para la trama muy buena. El reparto y la interpretación muy buena también, pero para un director como Alejandro Amenábar, que tiene grandes películas, no ha sabido aprovechar todo el potencial que podía ofrecerle la trama. Es una película de thriller psicológico, y esa parte la cumple perfectamente, pero es muy intuitiva y en seguida descubres el final de la película una vez te presenta a la protagonista. 
 Os animo a que la veais igualmente, porque se disfruta, pero igual me esperaba a verla en casa tranquilamente, porque tampoco hace falta grandes efectos visuales y de sonido.
        
            