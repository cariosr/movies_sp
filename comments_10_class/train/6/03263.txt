
                      En 'La cura del bienestar' Gore Verbinski demuestra que a pesar del paso de los años no ha perdido el pulso del suspense, desde que rodase el remake de 'The Ring'. La película funciona muy bien tanto como cuento fantástico al puro estilo Lovecraft, o una historia de ciencia ficción digna de J. G. Ballard. Quizá sea reprochable sus excesivo metraje y quizá su enrevesado final en el que, en un intento de explicar todo, quizá se crea más confusión en el espectador. También hay que reseña que tiene escenas bastante desagradables que harán las delicias de los fans del terror, pero no son aptas para almas sensibles.

Puntos notables: su increíble fotografía y su inquietante banda sonora. 
Puntos en contra: su excesivo metraje y sus ganas de explicar todo al final, con el resultado contrario.
        
            