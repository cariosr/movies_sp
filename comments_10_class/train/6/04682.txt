
                      Estoy segurísimo de que si la hubiese visto hace unos años me hubiese gustado mucho más. No cuenta con grandes efectos especiales ni con la espectacularidad de Narnia, pero nos cuenta una historia sobre el poder de la imaginación muy bonita y adolescente. Es un perfecto telefilm para un domingo cualquiera.
        
            