
                      Nicolas Cage no es solo ese actor raro que te hace superproducciones de todos los tipos. De vez en cuando también te hace algún thriller que no llega a las salas españolas como “Left Behind” o como este caso, “Pay The ghost“.

La película, bebe mucho de “Insidious” o “Sinister“, pero se ve claramente influenciada por “El orfanato” de J. A. Bayona. El vástago que desaparece y los padres que se obligados a recurrir a una médium que aclare los fenómenos paranormales que suceden en la casa. Una sucesión de escenas con fantasmas, una detrás de la otra. Posesiones, alguna que otra criatura voladora y un mensaje que se transmite toda la película para generar intriga. Thriller sobrenatural que se deja ver, y que al llegar a la resolución final, podrás pasar sin más, a la siguiente película de Nicolas Cage. Eso sí, preguntándote por qué todos los actores de Hollywood no toman nota de lo que hace Cage para no encasillarse nunca.

Lo mejor: su olor a suspense noventero.
Lo peor: el nefasto doblaje español de Sarah Wayne Callies.
        
            