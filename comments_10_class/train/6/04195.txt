
                      Técnica: 7/10
Interpretaciones: 7,5/10
Guión: 6/10

Puro entretenimiento al estilo castizo, con chascarrillos de cultura popular y actualidad social.
Mero pasatiempos sin alardes de ningún tipo.
Guión muy predecible y escasamente elaborado.
El elenco tira de oficio, sin ningún tipo de reto para sus interpretaciones. Con sus más básicos registros superan sobradamente lo que el guión les pide.
Recomendable al 70 %
        
            