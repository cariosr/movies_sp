
                      Película que parece más de humor que de terror y eso son muchos puntos a su favor porque está bien llevado a cabo, en el momento que te hacen reír se te olvida que estás viendo una película de terror y no es así. Tiene una buena historia, con unos niños como protagonistas que son la bomba y los abuelos no sabes si dan miedo o pena. En resumen, película distinta, muy bien hecha, con un poco de humor y siendo una película de terror.
        
            