
                      Serenity (obsesión) del director Steven Knight, es una peli rara. 

Steven Knight tiene en su haber varias series buenas y una peli muy buena y otra pasable. En las series están "	Peaky Blinders" y "Taboo". Como película interesante "Locke" del 2013 con Tom Hardy.

En Obsesión trata de sorprender con algo brillante, tiene una idea genial, pero no sabe plasmarlo en pantalla. Se pierde entre sus propias reflexiones. No es una mala película, pero he de avisar de algo, intentando no entrar en spolier. 

Basicamente trata de un excombatiente de Irak que se compra su propio barco pesquero. Vive entre la pesca y las excursiones de turistas que buscan aventuras a bordo. Hasta que aparece su exmujer pidiéndole que asesine a su pareja, de pasatiempos violentos.

Comienza como una película noir, presentando muy bien a los personajes y consiguiendo una primera parte donde se palpa la tensión. 
Los actores son muy buenos. Un dúo interpretativo entre Matthew McConaughey y Anne Hathaway de buena calidad.
Una banda sonora que cumple perfectamente para aumentar las sensaciones de ansiedad o la cercanía de peligro.
La fotografía de Jess Hall es muy buena, buscando siempre recursos estilísticos consiguiendo plasmar perfectamente tanto la quietud como la tensión.

Pero entremos directos en la peculiaridad de este largometraje.
La primera parte es muy correcta y por momentos hasta brillante. Pero la segunda parte es una auténtica ida de olla. Un despropósito que sólo estarás dispuesto a aceptar, si ves la peli con una amplitud de mente muy abierta. Muuuuuy abierta.
Como digo, en la segunda parte el director hace un giro de 180° que te deja con la mandíbula en el ombligo. No se trata de un giro efectivo donde descubres algo sorprendente. Sino que todo lo que crees haber visto o entendido, no sirve para nada. Se cambia todo el concepto del argumento entrando casi en términos de física cuántica.

Como digo es una idea brillante, pero no funciona. Y no lo hace porque el director se pierde. Se difumina toda la tensión y el buen trabajo realizado en la primera hora.

Es una pena, podía haber sido algo impresionante, pero se queda en una simple anécdota entretenida. Pero está bien que se arriesgue, si no flizteamos en territorios nuevos...no evolucionamos.

Mi puntuación es un 3 sobre 5.

C) 2019 Fran Bretones.
        
            