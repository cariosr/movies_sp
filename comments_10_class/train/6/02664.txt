
                      Por francis arrabal

Decía Woody Allen que la inspiración de ésta película le vino de un hecho real, de un suceso que le contó su mujer, y que él no pudo contener su mente y dejar de llevar estos hechos a la gran pantalla. Era un gran filón, y más en los tiempos que corren, el plasmar las desdichas de la esposa de un corrupto que ve caer su imperio. Y aquí es justo donde me falla la película.

Blue Jasmine solo es un esbozo, casi una parodia, de una mujer acostumbrada al lujo, que ha vivido de su marido, sin importarle más que la marca de sus bolsos. Que ve como su status de repente se derrumba tras enterarse de las reiteradas infidelidades de su marido y su deseo de emprender una nueva vida, en la que ella, ya no tiene cabida. Así, Janet, o Jasmine, como se hace llamar, tendrá que emprender un viaje hacia una vida más pegada a la cruda realidad, e independiente, aunque se siga aferrando a su antigua vida, y haga todo lo necesario por resistirse al cambio. Llegó su particular catársis. 

Pero, uno siempre espera más de Woody Allen, la genialidad. Es lo que ha conquistado a su numeroso público europeo. Y es a lo que nos ha malacostumbrado. El toque filosófico, tanto en la comedia, como en el drama, que sólo él sabe imprimir a sus películas. Además, las tramas secundarias nunca llegan a funcionar, lastrando la película y volviéndola algo tediosa. Sólo sobresale en el reparto una impresionante Cate Blanchett, que tras 90 minutos con la cámara pegada a la cara, entre lágrimas y vodka, salva la película -Ojo al Oscar-. Su papel, y el trabajo de Woody Allen en este apartado, es excepcional. El retrato de una mujer histérica que se ve avocada al salto al vacío, y que tras su fachada, no es más que una pobre diabla.

Blue Jasmine ni se aproxima al mejor Woody Allen -como muchos apuntan-, pero no deja de ser él, y con Blue Moon sonando de fondo, mejora bastante.
        
            