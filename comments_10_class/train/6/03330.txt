
                      En esta película se nos muestra la agudez y picaresca típicamente italianas, tanto que nos dicen de la española. Quien se aprovecha más de uno o de otros, el administrador del protagonista o las madres de éste, como siempre dependiendo de la perspectiva desde donde se mire, porque al fin al cabo todos salen beneficiados en unas vacaciones donde ninguno de ellos tienen vacaciones o por lo menos económicamente pueden permitírselo como tantos ciudadanos de a pie. Me gustó mucho el humor que muestra ya que nunca es agresivo con la gente mayor.
        
            