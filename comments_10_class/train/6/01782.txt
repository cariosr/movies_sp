
                      La película esta bien y realmente merece la pena verla pero es excesivamente larga. Trasmite el exceso, la opulencia, el derroche, la falta de ética, de valores familiares... No creo que Di Caprio fuera merecedor del Óscar por esta peli, por una vez de acuerdo con la academia.
        
            