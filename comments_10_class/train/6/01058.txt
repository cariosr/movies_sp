
                      Film con un excelente reparto protagonista y realizado con una sobriedad y corrección excepcional, así como con una estupenda ambientación, pero cuyas emociones quedan muy por debajo de la apariencia y de la historia original, resultando bastante aséptica a pesar de todas las virtudes que presenta. Destacar también la magnífica banda sonora
        
            