
                      "¿Con quién estás enojado?"

"Todo el mundo."

Este es el primer intercambio que escuchas en The First Purge, y para ser justos, no puedes acusar a la última entrada de la franquicia de enterrar la Lede. Dado que la persona que responde la pregunta es una caricatura maníaca cicatrizada y sonriente de un demonio del crack, volverá, confía en nosotros en esto, y está escupiendo ev-er-y sin-gle-syl-la-ble como si se tratara de veneno de cobra de grado de armas, tampoco dirías que esta precuela de tráfico pulido aumenta con sutileza. Regrese con nosotros al principio, cuando la idea de un caos legalizado de libre para todos no era más que un destello a los ojos de una oficial de gobierno racista y una mera hipótesis de hipótesis científica para una científica del comportamiento (Marisa Tomei). Ahora podemos ver la versión de prueba, la versión beta de este escenario sancionado con pesadilla sancionada. Como con todas las grandes sagas, comienza en Staten Island.
        
            