
                      La familia Addams, es una comedia de fantasía dirigida por Barry Sonnenfeld, director de la trilogía Men In Black, Wild Wild West y Get Shorty.

Los Addams son una familia macabra, totalmente fuera de lo normal.
Se encuentran en una situación financiera complicada y su avaro abogado decide aprovecharse de ellos para hacerse con su mansión y la fortuna del hermano mayor desaparecido de Gómez Addams, Fétido.

Morticia y Gómez Addams son un matrimonio apasionado y bastante peculiar.
Sus dos hijos son mostrados en segundo plano, exhibiendo la educación que han recibido. Juegan con el peligro, justo todo lo contrario que haría un niño normal de su edad.

A pesar de la extravagancia de la historia, la película tiene mucho encanto. La trama que puede llegar a ser demasiado simple, queda adornada con el vestuario, los escenarios que representan su casa y sus tierras y la banda sonora, que combinan a la perfección con la personalidad de los personajes.
        
            