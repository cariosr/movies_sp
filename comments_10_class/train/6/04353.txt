
                      Película sobre agentes secretos con acción y mucho humor, en la que los protagonistas no han usado casi dobles y queráis o no eso se nota y hace que te creas más el personaje. Imanol Arias parece un autentico James Bond pero jubilado, le va que ni pintado el papel y ha Quim Gutiérrez le ha venido de perlas hacer este tipo de películas para que no se quede encasillado en el prototipo de actor romántico. Tiene muchísimo humor la película, acción, buena fotografía y cameos de humoristas. Es bastante completa y entretenida.
        
            