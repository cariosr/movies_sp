
                      Actuación: 5
Vestuario: 10
Efectos visuales: 5
Efectos sonoros: 10
Música: 10
Guión: 8
Edición fílmica: 5
Dirección: 5
Cinematografía: 10
Escenografía: 10

Calificación: 73/100

Otra película de Cenicienta pero con un toque realista, la princesa no necesita ser rescatada, el príncipe es rebelde y inmaduro, quédate con quien respete tu opinión e ideales y además los tome en cuenta para sus futuros proyectos. 

Bonita película la verdad, entretenida pero como varias escenas de más que la hacen sentir un poco pesada, las actuaciones no son muy buenas, pero si divertidas y logras empatizarse co ellas. Vale la pena para palomear y pasar un buen rato en familia.
        
            