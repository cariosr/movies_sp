
                      Domingo, puente de la Constitución y la Inmaculada, primera sesión, poca gente en la sala. En la pantalla el último largometraje de Fernando Trueba, un film difícil de clasificar, que no se decide ni por el drama ni por la comedia, ni tan siquiera como un western urbano. Una película realizada por españoles, con actores argentinos que sitúa la acción en Chile. A destacar la fotografía y la banda sonora, y la interpretación de Ricardo Darín.
        
            