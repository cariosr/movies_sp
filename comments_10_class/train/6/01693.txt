
                      Tengo serios cuestionamientos personales con esta saga, donde el poder y la frivolidad son un arma de dominación y seducción constante. Independientemente de esa bajada de línea que me parece nefasta, el film en si es llevadero. Cuenta con un argumento fácil de seguir y mucho sexo. Por momentos se pueden sentir sentimientos encontrados con la protagonista, de muy buena actuación por cierto, donde muestra una personalidad frágil, persuasible pero tierna a la vez.
Película aceptable, para pasar el rato.
        
            