
                      EL HOBBIT: LA BATALLA DE LOS CINCO EJÉRCITOS (PETER JACKSON 2014

La Batalla de los sentimientos encontrados.
 
Llega el final de 2014 y con el mismo asistimos al final de la  segunda trilogía en la tierra media. Cerramos por lo tanto un circulo, no sin cierto desconsuelo,  pues tenemos la sensación de que vamos a tardar pero muy mucho ( y no sin cierto escepticismo ) en volver a transitar los caminos de fantasía que Tolkien erigió para nosotros.
 
           Hoy se me va a hacer difícil tener las cosas claras. El Fanboy se va a enfrentar al crítico. El señor de los anillos fue lo mejor que había ocurrido en el cine fantástico en mucho tiempo. Una nueva saga para una nueva generación y a la par, una nueva saga a añadir para aquellos amantes ya de sagas como Star Wars.  Vamos, que allá por 2001 no esperaba emocionarme de nuevo cual crio.   Se acabó lo que se daba en 2003 y tuvimos que esperar hasta 2012 para volver a saber de hobbits, elfos, enanos, magos, trasgos, orcos y resto de galería tolkiana.   Una espera larga y tediosa pues, son por muchos conocidos los problemas burocráticos que sufrió esta producción para largar velas.
 
          SIPNOSIS:
          Después de enfrentarse con el dragón Smaug Bilbo se ve obligado a tomar una peligrosa decisión para ayudar a su amigo Thorin. Mientras legiones de orcos se dirigen a la Montaña Solitaria a la vez que hombres y elfos se enfrentan a los enanos por sus reclamaciones sobre el tesoro.

           La Batalla de los cinco ejércitos es la más entretenida de los 3 hobbits, además de ser la más corta.  Por esto que acabo de exponer debería ser la mejor de las 3 pero no me lo ha parecido.  La película sufre lo que yo llamo el mal de Miguelito Bahía ( Michael Bay), es decir, sobre saturación de acción.  Lo bueno, si poco mejor, porque si no aburre. Me ha gustado a partes mientras otras me han resultado insultantes.
 
            No voy a entrar a saco con todo lo que han inventado o construido para llevar a la pantalla las últimas 20 paginas del libro. Ahí tiene su merito, pues el autor pocos detalles da de la contienda. Manos libres pues para Peter Jackson y su equipo.  
 
            Comienza el film con lo que para muchos debería haber sido el desenlace natural de La Desolación de Smaug.   Haciendo cábalas muchos sabemos que cuando se trataba solo de 2 películas el final de la primera era la llegada de los Enanos a Erebor, condensando la segunda toda la etapa con Smaug y la Batalla que nos presenta esta tercera parte, o sea, una pasada de desenlace.
             La verdad es que Smaug luce en todo su esplendor y el hedor a maldad que desprende se come la pantalla.   Todos sabemos como acaba nuestro dragón así que pasamos pagina y volvemos con nuestros Enanos.   Qué nos queda a partir de ahora?  Pues hay que llenar las 2 horas que quedan con una batalla.  Por supuesto esta no puede ser tan grande como la presentada en el Retorno del Rey pero es que ni siquiera es equiparable a la de Las Dos Torres.  ¿Cómo puede ser esto posible? 
 
           Muy fácil queridos amigos.  Peter Jackson se olvida de los protagonistas naturales de la historia: La compañía de Thorin y nuestro amigo Bilbo.  Es increíble pero cierto.  Tenemos un grupo de 13 enanos perfectamente caracterizados y diferenciados ¿para qué? Pues para nada.  Durante toda la batalla los vemos muy, pero muy brevemente. En cambio nos concentramos en Bardo, este tiene un pase, pues lo conocemos de la anterior parte y se ha ganado cierta simpatía, pero también se concentra en Thranduil, un elfo que a nadie cae bien y que nos importa un pimiento al igual que el presentado cinco minutos antes "Dain", pie de hierro, personaje casi caricaturesco.  
             ¿Donde están nuestros héroes sufriendo, luchando, pasando miedo o apuros?   Así de claro Peter, te has olvidado del corazón de toda obra y te has dejado llevar por la parafernaria del espectáculo.   Triste es que ,con este plantel de protagonistas, les des la espalda.  Si teneis la oportunidad fijaros bien, pues creo que hay más de un enano que no tiene ni una línea de dialogo en todo el film.  
              Además está muy mal montada, tan pronto estamos en la batalla como nos apartamos de ella y entre medias no entendemos bien que es lo que ha o está ocurriendo.  Todos recordamos lo bien visualizadas las batallas de las Dos Torres y El retorno del Rey, está última era mucho mayor y se entendía perfectamente, mientras que aquí muchas veces acabamos desorientados.     Muchos dirán que esto se arreglará en la edición extendida.  Yo no lo tengo tan claro.  Vuelvo a recordaros que esta película es la más corta de las seis; solo 144 minutos. ¿Seguro que no era posible tener más añadidos de personajes en medio de la misma?
             Hablando de escenas cortadas es un poco chocante como han quedado las escenas con Gandalf. De estar en una escena al limite de sus fuerzas pasa a la siguiente con Sombrero y espada incluidos y con unas energías que ya quisiera yo un lunes por la mañana.
            
               Gracias a dios todavía Peter Jackson encuentra el espacio necesario para mostrarnos acciones individuales como todo lo que atañe a la historia de Thorin y determinados enanos, que tan pronto entran como salen de la historia.   
             Si en la Desolación de Smaug me quejé de falta de épica en esta no puedo hacer lo propio pero debo decir que queda un tanto diluida y no tan conseguida como debiera.   Por último debo decir que la película no cierra los diferentes arcos narrativos como debiera dejando más de una cabo suelto.
 
           Yo, como fanboy, seré de esos que digan que agradece tener 3 películas en vez de dos, pues me da la oportunidad de poder disfrutar de más metraje de la tierra media, no por ello dando por sentado que esto sea mejor.   Tengo muy claro que los añadidos no eran necesarios.   De entre todos el metraje, el de Legolas y Tauriel es el que peor parte lleva. Meros minutos añadidos.  Minutos dije? Mucho metraje para ellos, y como digo, para mi los protagonistas son los enanos.  Por cierto, Legolas siempre se lleva la peor parte en cuanto a escenas con CGI, de las que hacen pupita a los ojos.
 
              Mención a parte para Howard Shore que en La Batalla de los Cinco Ejércitos vuelve a hacer que su música alcance de nuevos altas cotas de épica, no como en La Desolacíón de Smaug, o como en Un viaje inésperado, donde, desarrolló el tema de la compañía de Thorin para luego olvidarse de el.
            
             La Batalla de los Cinco Ejércitos queda por lo tanto la segunda en mi podio personal, como primera sigo quedándome con Un viaje inésperado, lenta para muchos, pero donde la presentación de personajes era exquisita, aunque una mera puesta al día de La Comunidad del anillo y como finalista tenemos La Desolación de Smaug, por larga, lenta y falta de épica.
      
           Muchas gracias Peter Jackson por todo lo que nos has dado, podrías haberlo hecho mejor pero también es cierto que podrías no haber hecho nada.
            Siempre quedará esa sombra de duda de como hubiera resultado todo en manos de Guillermo del Toro con solo 2 películas.  Seguramente mucho más compensado y dinámico.   Guillermo, te queremos para el Silmarillon. 
 
Puntuación 7/10
        
            