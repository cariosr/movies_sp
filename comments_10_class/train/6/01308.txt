
                      Mi Semana con Marilyn, es una película británica dirigida por Simon Curtis y escrita por Adrian Hodges.

Está basada en dos novelas escritas por el británico Colin Clark, las cuales narran la grabación de la película 'El príncipe y la corista'(1957), protagonizada por Marilyn Monroe y Laurence Olivier.
Ambientada en 1956, Clark trabaja como asistente en el set de la película 'El príncipe y la corista'. Marilyn está de luna de miel con, su por entonces marido, Arthur Miller. Clack aprovecha la partida de Miller para pasar una semana con Marilyn y enseñarle todo sobre Londres.

Michelle Williams recibió 14 premios diferentes a la mejor actriz por su interpretación de Marilyn.
Ella se muestra frente a las cámaras de forma decidida, alegre y seductora, pero en su vida privada, cuando ya no la enfocan, se vuelve frágil, insegura y depresiva.
Una bipolaridad de la que se aprovecha el personaje de Eddie Redmayne, con el que Marilyn tiene una breve historia de amor durante esa semana que pasan juntos.

A pesar del gran elenco de actores que aparece en la película y de lo mucho que parece prometer, esta podría haber dado mucho más de sí.
        
            