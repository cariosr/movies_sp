
                      Es una secuela bien hecha que bebe de la primera película. No pretende superar la original. A destacar la manera en la que Neeson se ubica geográficamente cuando le secuestran y la conversación que mantiene con su hija por el móvil para lograr que ella le encuentre a él. Por lo demás, es predecible. Un producto comercial digno, aunque las escenas de lucha estaban mejor coreografiadas en el primer film.
        
            