
                      No me lo esperaba pero pase muy buen rato con esta pelicula y me rei muchisimo. Tampoco es una obra maestra y a veces el humor es muy escatologico, tienes ganas de decir al director "Oye, ya no tengo 12 años para reir de esto". Pero me gusto el hecho de que las mujeres no solo sean princesitas que quieren casarse, pero locas, celosas, malas, aunque sea una pena que el final sea bastante previsible. Y la actriz Kristen Wiig esta genial!!! La pelicula le debe mucho, es muy graciosa y sus momentos son para morirse de risa!
        
            