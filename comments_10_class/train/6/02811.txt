
                      Tratando el tema que trata y siendo una historia real (una más) sobre la II Guerra Mundial, a "La casa de la esperanza" le falta alma, no llega a consternar como debería y el paso del tiempo está mal explicado, da la sensación de que los saltos son demasiado bruscos. Esto no empaña una buena ambientación y el buen trabajo de Jessica Chastain o Daniel Brühl, pero podría haber sido mucho más emotiva
        
            