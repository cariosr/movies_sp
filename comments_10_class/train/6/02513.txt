
                      Poca carne de celuloide

La relación de Thomas Hardy con el cine es muy similar a la que mantienen autores como Emily Brontë, Jane Austen, o incluso Charles Dickens y Gabriel García Márquez. La magia y poesía que transmiten las páginas de sus novelas es tan inabarcable que se pierde en sus adaptaciones cinematográficas, por muchos medios con los que cuenten sus responsables o el respeto y fidelidad que profesen al material original.

La versión que de “Lejos del mundanal ruido” propone Thomas Vinterberg supone una traslación ambiciosa, quizá la más ambiciosa de su filmografía, que va emparejada a la ampulosidad que transmite la novela de Hardy. Todo en ella es grandioso y exquisito. La dirección de Vinterberg es clásica y elegante, ayudada por su directora de fotografía habitual Charlotte Bruus Christensen, la diseñadora de vestuario Janet Patterson (“El piano”), victoriana partitura de Craig Armstrong, y un excelente cuarteto protagonista formado por Carey Mulligan, Mattias Schoenaerts, Michael Sheen y Tom Sturridge, todos ellos estupendos en sus respectivos papeles.

Y sin embargo, a esta nueva y resumida versión de la novela le ocurre lo mismo que a la más extensa y fiel de John Schlesinger. “Lejos del mundanal ruido” es un melodrama romántico que funciona mucho mejor en papel que en imágenes en movimiento. Su envoltorio es embriagador, pero al final no existe en ella esa pasión que desprendían las frases de Hardy, y el guión trata de prescindir de aspectos de la novela cruciales para entender los actos de algunos de sus personajes, como el propio carácter de su protagonista o el poco dibujado personaje de Michael Sheen. Para muchos, incluso, puede hacerse pesada, le puede costar coger el vuelo durante su primera mitad, ya que al fin y al cabo estamos ante una historia de desarrollo lineal, pausado y lento.

Lo que queda es una adaptación correcta, sin ningún arrojo por parte de un realizador tan arriesgado como Vinterberg, que se limita a ofrecer un relato decimonónico cargado de lirismo en sus imágenes y plagado de personajes despechados, orgullosos y vanidosos, movidos por lo que les dicta el corazón y no por las convenciones sociales. Pero tampoco puede hacer mucho como cineasta. Este relato se cuenta por sí solo, y demuestra una vez más que no ha sido nunca carne de celuloide. 

A favor: el apartado técnico y que funciona como melodrama
En contra: Hardy no es carne de celuloide
        
            