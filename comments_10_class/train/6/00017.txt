
                      Técnica: 6,5/10
Interpretaciones: 7/10
Guión: 6/10
No pasa de ser una película entretenida sin mayor proyección.
Guiño nostálgico al cuento popular de la Cenicienta.
Intuyo que musicalmente ganará mucho en V.O. con Justin Timberlake, en castellano bastante justita.
Huye en gran parte del humor de doble sentido (infantil y adulto) tan manido los últimos años en las películas de animación infantil, aunque en alguna ocasión puntual aparece.
Recomendada 60% adultos, 80% niños
        
            