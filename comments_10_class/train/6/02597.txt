
                      Una visión preciosa y descriptiva de las inquietudes que a todos nos asaltan sobre lo que hay después de la vida. Me ha gustado especialmente el papel del niño y todo lo que nos dejan ver contando su historia. Otro gran trabajo de Cleant Eastwood.
        
            