
                      Venom (2018).
“No estoy loco, lo que es de locos es cómo vivimos hoy día: no hacemos más que coger y coger..."
Mientras el director de Zombiland tiene pendiente de estreno su segunda parte a partir de 2019, nos presenta la película del villano y archienemigo de Spiderman, Venom.

“Que te vaya bien la 'vida’..."
Tom Hardy encarna a, podríamos decir, la mitad del protagonista de nuestra historia, un periodista en horas bajas necesario para la supervivencia de Venom, un ser -generado por ordenador para su traslado a la gran pantalla- de origen extraterrestre y con tanto poder como malas pulgas... 
Michelle Williams, quien interpretara a la malograda Marilyn Monroe hace ya unos años, será la chica de Hardy en esta ocasión. 

Aunque no pasará como el mejor largometraje de los muchos de la compañía Marvel, el filme entretiene pese a un principio algo tedioso y lento. Digna de mención merece la persecución nocturna en motocicleta del doble personaje de Hardy, pero sin llegar al nivel de la que pudimos ver en la última parte de Mission Impossible con Tom Cruise a los mandos de su estupenda BMW.

Raúl Cabral.
https://www.filmaffinity.com/es/film122208.html
        
            