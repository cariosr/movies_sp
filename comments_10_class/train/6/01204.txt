
                      Su animación familiar gritona y agitada convierte las mesas en el mito del abominable muñeco de nieve. ¿Qué pasaría si un pueblo de yetis descubriera la existencia del pie pequeño legendario, esa pequeña criatura adorable, virtualmente libre de pieles, con una cabeza no más grande que una uña yeti?

El engreimiento está bien hecho, y el inesperado y sincero mensaje de la película sobre la empatía y mirar el mundo a través de los ojos de alguien más compensa su animación suave, su guión inteligente y sus genéricas canciones de clappy-blah.

Channing Tatum es la voz del adolescente yeti Migo, que vive en lo alto del Himalaya. Durante siglos, los yetis de su aldea han obedecido leyes y creencias talladas en tablas de piedra sagrada. Su artículo número uno de fe es que el pie pequeño no existe.
        
            