
                      Bien futurista con un estilo retro que mezcla algo de Star Wars con pizcas de algunas otras películas ochentosas donde se aprecian naves, seres de otros planetas y diferentes culturas que convergen entre si. Muy buenos efectos, situaciones cómicas y una bajada de línea contundente hacia la devastadora raza humana, aniquilante de todo lo desconocido. Entretenida, no mas que eso.
        
            