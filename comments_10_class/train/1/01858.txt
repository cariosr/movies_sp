
                      Con un doblaje pésimo nos enfrentamos a una película sin argumento que no pasa de ser entretenida y nada más. Muy larga para no decir nada y centrada exclusivamente en la historia de este personaje sin ninguna gracia. El mismo protagonista es el director, guionista y productor, parece que ha querido exteriorizar algún trauma con su papel de padre de familia y con la cocina. En fin, hay cosas mucho mejores que hacer que ver este bodrio.
        
            