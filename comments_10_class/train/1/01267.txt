
                      Después del revuelo con el cambio de protagonista, y la baja calidad de los libros de E.L. James, habíamos decidido no ir a ver “50 sombras de Grey”, pero este fin de semana, nos hemos liado la manta a la cabeza y nos hemos lanzado al estreno de dicha adaptación cinematográfica.

Con no más de media sala llena (en su mayoría de señoras que rondaban los 50 años), nos invadió un mal presagio… lo íbamos a pasar mal.

Argumentalmente la historia es la misma que se nos cuenta en el primer libro de la trilogía, aunque eso sí, gracias a la elección de 4 o 5 pasajes del mismo durante las casi dos horas que dura la cinta. 

Con un ritmo más lento que un caracol cojo, un Grey que no trasmite nada se enamora de una Anastasia que, aunque correcta, no puede más que evidenciar que están actuando (la falta de química entre ambos es más que notoria) en un supuesto drama erótico sadomasoquista que, en ningún momento roza ni el erotismo ni el sadomasoquismo (salvo en los latigazos que finalizan la trama, y el sufrimiento de los espectadores).

En definitiva, una adaptación más que floja, nefasta.

Para nuestra opinión completa, no dudéis pasar por nuestra página.

Hemos Visto
        
            