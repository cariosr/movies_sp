
                      Del mismo modo que 'La vida de Brian' (1979) fue un rotundo éxito en España, otros films con aura de freak o película de culto (la línea que divide ambos géneros es muy delgada), tuvieron casos iguales, dignos de estudio. Somos como somos, no podemos remediarlo. Fuimos capaces de mandar a Eurovisión a un personaje como el Chiquilicuatre (con el que comparte rasgos nuestro personaje) por lo que no sorprende que asumamos como propios iconos desfasados o trasnochados como el que nos va a ocupar hoy. La delgada frontera entre el culto a un símbolo y la veneración a una aberración es tan fina que seguidores de Lucas Grijander o El Tío de la Vara entenderán que catalogar ciertos films como "buenos", por el simple hecho de contar con un personaje tan imposible como deleznable, es tarea de titanes. De lo más profundo de la Norteamérica de la liposucción, de los oscuros senderos que separan lo irrisorio de lo vulgar, os traemos al blog un producto sin par. El único detective capaz de dejar a Ace Ventura o al inspector Gadget como héroes de elevada condición. Niños y niñas...Ford Farlaine.

La película, término que me atrevo a utilizar para referirme a este ser, es un largo y prolongado monólogo del actor principal, Andrew Dice Clay, que se rodea de fastuosas decoraciones, escenarios llenos de color y una atmósfera que no sabe bien si situarse en los años 50 o en su época actual. Incansable verborrea del actor que disfrazado de detective, con un incapaz talento para imitar a figuras como Sam Spade o Phillip Marlowe, propone un viaje astral hacia su verdadera personalidad. No nos engañemos, las dos horas de duración, no son otra cosa que un vehículo para el lucimiento del supuesto humor que destila el propio actor. Un acto de total egocentrismo al servicio de una historia que quita el hipo.

Que necesitasen tres guionistas (menos mal que no fueron cuatro) para llevar a la pantalla el personaje creado por Rex Weiner, para una serie de novelas baratas, no dice mucho de la capacidad del productor o del director para manejar el asunto. Traída de los pelos, la trama es una sucesión de chulerías para beneplácito del personaje (actor) principal. Se intenta elevarlo por encima de la historia a base de frases supuestamente lapidarias, humor grueso cuando no grotesco y una peculiar forma de socarronería que eclipsa, cuando no mata, al conjunto del film.

Dice Clay (Ford Farlaine) no interpreta, se limita a hacer lo que durante años hizo en la MTV en su programa de monólogos, donde con un humor obsceno y brutal atacaba a razas, credos y condiciones. Trasladado todo esto a un personaje enfocado con mitos de los 50's, coches de lujo, vestuario de lo más cruel para los ojos y un decepcionante mal gusto para compararse con Elvis, hace de la visión de la película un tremendo esfuerzo para las neuronas. Reiteradas poses de cara a la galería, supremacía como macho alfa y enorme narcisismo tan petulante como su tupé. Renny Harlin, responsable de este crimen, venía de filmar la cuarta parte de 'Pesadilla en Elm Street' (1988) y había entrado por los ojos a los ejecutivos de los estudios para llevar a cabo este proyecto, que dicho sea de paso, era un capricho de su productor (Joel Silver, 'Matrix', 1999). Si bien, uno es capaz de terminar el film sin sufrir escozor en el esfínter, podrá preguntarse: ¿Por qué tuvo y tiene aún tanta fama este tipo?

La respuesta es sencilla. España es la reina del boca-oreja. Nos transmitimos los chascarrillos de la misma forma que Bárcenas los fondos, a lo bruto. Tenemos esa capacidad innata en nuestro código genético (llamado ADN-ibérico) para adaptar personajes y frases que nos encajan como guantes en nuestra cultura popular. Fueron las frases más mordaces de este personaje, unido al doblaje que se hizo en España (Pablo Carbonell), las que dieron vida y popularidad a la película. Perlas tan cultivadas como: 

"Me llaman Ford Fairlane, el detective rockanrolero, tengo derecho a entrar en los mejores clubs, en los mejores camerinos,....y en las mejores chicas. Reconozco que esto puede parecer una chulada. ¿Y qué hago yo aquí? ¿Por qué me gustaría que la industria de la música y el resto del globo me tocaran la minga dominga? Muy sencillo, yo no lo sabía pero iba a hacerme cargo de la peste negra. Y no es esa enfermedad que mató a millones de personas en el siglo XII, era algo mucho peoooor."
" Y lo peor de todo: Tengo un banco de jurelillos en los calzoncillos y nadie me cree. "
"Que cabrón." "Agárrame un cojón."
"He oído gatos follando con más oído que este chaval."
"Ya llegó la poeta moviendo las tetas,¡oaaaaaaaaaao...!"
2Angorami, angorami, pero que mierda es esta,podría mover las orejas con más ritmo que este chaval. Keith Richards se removeria en su tumba si no fuera porque el muy mamón ni siquiera esta muerto,uh no,no,. Me estás destrozando eh? me estas destrozando achanta la mui y vete por ahí a tomar nota... Rock'n'Roll chicos,hacedlo bien einng!"
"Siempre he dicho que hay que depilarse para llevar un vestido como ése... y no me refiero a las piernas"
"Malditos australianos, odio ese país, ese continente o lo que sea, ¿es que no hacemos pruebas nucleares ahí?
"Oye, como te vuelva a oír decir un taco te reviento la cabeza, ¿está claro?... ¡Ahora vete a tomar por culo por ahí!"
"No esta mal, no esta mal, se puede beberciar"
"Yo personalmente no tengo nada contra los homosexuales, incluso tengo un amigo que es medio sos-pichoso."
"Te saco por ahí, te meto una hamburguesa en la boca y lo pasamos guay"
"¿Pero tu vendes rosarios o que? Menosmola ¿Booty Time? tocame las bolas"
"Vete a tomar por culo con tu puta madre, hijo la gran puta"
"¿¿cómo se te ocurre tirar una pistola tan bonita??... BANG. Gracias por ser un gilipollas, gilipollas. Mano a mano, hombre a hombre, qué chorrada."
"La madre que os parió, hijos de puta"
"No, tú eres un zoquete" "Tócame el paquete"
"Joder, espero que hayas firmado la tarjeta de donante, VAYA PEEEERASSSS UAUUU"
"Menosmola" "Eso es lo que pienso yo de ti" "No, tú eres un menosmola" "Tocame las bolas"
"Toma una magdalena, ridículo."
"Huevos fritos con jamón, te los comes cuando quieras, pero folla siempre con condón."
"Tanto gilipollas y tan pocas balas."
Todas estas frases imaginarlas con la voz de Pablo Carbonell para que resulten algo graciosas.

Pues bien, tras esta gran demostración de calidad de guión, diálogos sagaces y humor inteligente, nos hicimos acopio de sus frases y elevamos a Ford Farlaine a la categoría de mito. Entre ser chulo y un ridículo se encontraba Farlaine. Lástima que no supiéramos colocarlo en su lugar. Menos mal que tenemos a 'Torrente'.
        
            