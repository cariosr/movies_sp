
                      Esta película supone un insulto para los que verdaderamente comprenden qué es Star Wars.

En realidad es un remake de la primera película, un film con un argumento de 40 años de antigüedad y un guión que no es más que el instrumento para introducir todos los tópicos imaginables.

Así pues, es predecible y aburrida hasta la saciedad. Además de ofensiva para quien considera que Star Wars es mucho más que una sucesión de naves que se disparan las unas a las otras.

Al igual que pasó con Star Trek, Abrams ha conseguido simplificar e idiotizar los mitos que han supuesto la ambrosía de varias generaciones.
        
            