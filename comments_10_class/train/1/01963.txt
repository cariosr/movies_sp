
                      Drama.Irreal y poco o nada representativa, El tema no es interesante desde ningún punto de vista, ya que el protagonista es un ser patológico, incapaz de empatizar , y que va de fracaso en fracaso arrastrando con él a cuantos le ayudan. El guionista une de manera absolútamente irracional el destino de este paria con Stephani, una mujer plena, que en un revés inesperado, pierde todo cuanto tiene en esta vida y tambien, de manera artificial, se pone en contacto con nuestro actor principal para iniciar una sórdida relación.Poco aconsejable.
        
            