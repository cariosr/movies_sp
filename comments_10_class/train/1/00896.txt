
                      Estoy MUY harta de escuchar tantas alabanzas absurdas a esta película
que no es más que el desahogo pornográfico de las obsesiones de un
director déspota. Fui a verla ilusionadísima porque el cómic me había
encantado y tenía las esperanzas de encontrarme con algo igual de bueno o
quizá mejor, pero no puedo expresar mi sorpresa al encontrarme tamaña
basura... Quince minutos de porno lésbico completamente gratuito e
injustificado que ensucian el resto del metraje y actúan a modo de
llamada de atención desesperada (así como llamada a la recaudación, a la
audiencia y a la crítica masculina) para disculpar tres horas
insustanciales, desaprovechadas y vacías, con lo que podía haber dado de
sí una temática inicial tan fantástica. El director sólo se preocupó de
rodar tijeras y cunnilingus, no hay rastro de la profundidad de la
novela gráfica, de su estética cautivante, de su buen gusto, de su
sensibilidad, de su despliegue en cuanto a temas y motivos... sólo sexo
explícito, posturas ridículas y morbo insultante.
Esta película es tan absurda y ofensiva como ver a Ingrid Bergman follando a cuatro patas en Casablanca. Seguro que ganó Cannes por su profundísimo guión y trascendentales diálogos. Las tijeras y las tetas bamboleantes no tuvieron nada que ver.
        
            