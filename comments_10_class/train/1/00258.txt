
                      Primero quiero aclarar que en Venezuela no se le llama malo a ese tipo de pelo, sino crespo y que en las escuelas públicas no se hacen anuarios.
Es un película pesada, donde se quiere responsabilizar al gobierno de la  amargura de una joven mujer tan sumida en sí misma, que no es capaz de andar ni de ver   más allá de la corta ruta que recorre en vano todos los días. Totalmente irrespetuoso el haber cerrado con una entonación llena de errores del Himno Nacional. 
Después de ver el crecimiento y brillo  que ha tenido el cine venezolano en los 
últimos años, "Pelo malo" queda pequeña, triste y opaca...
        
            