
                      Deja bastante que desear.
Llevaba tiempo esperando el lanzamiento de esta película, acudí el mismo día de su estreno y sinceramente me a defraudado bastante, mis expectativas se han ido disminuyendo sobre el transcurso de la película.
Una película que deseas que termine según va pasando el tiempo y que no te mete dentro de su trama.... significa un mal film que no vas a disfrutar para nada.

No me e dejádo influir por los malos comentarios sobre ben affleck ni mucho menos, es más esperaba que hiciera un buen papel de batman pero la verdad esque se a quedado bastante lejos de lo deseado quizás por el gran papel al que nos tiene acostumbrados el batman anterior de Christopher Nolan con Christian bale como batman, un batman con gran peso difícil de superar.

Respecto al papel de superman....sin más, un superman que se ve en constante lucha (bastante descafeinada) con las autoridades y la gente demostrando que no supone un riesgo para ellos, pero que no va más allá....y en cuanto a su lucha con batman..  5 min de lucha hasta que se vuelven amigos del alma para el bien de la humanidad.

Toda una película para solo una lucha entre estos dos personajes de 5 min? demuestra que nos han estado vendiendo un producto que no es el esperado....

Mi recomendación esque si no has ido al cine aún a verla, no vayas y te ahorres el dinero 💵. Es una película que perfectamente puedes ver una tarde de domingo en tu salón.

Espero que la película de escuadrón suicida si cumpla con sus expectativas y no se quede en humo como esta....
        
            