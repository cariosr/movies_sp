
                      LOS MERCENARIOS 2 (THE EXPENDABLES 2)

Veamos… ¿qué decir de ella?

¿Qué se puede esperar de una película con estos actores?
Pues una película absurda, demoledora, impactante, contundente, efectiva, directa al grano, sin sorpresas, sin giros imprevistos de guión ya que todos sabemos lo que pasará y estamos deseando que pase, y que arranca varios aplausos en la sala de cine en algunas escenas que se convertirán en épicas en este género.

La historia, su guión, se puede resumir en una conversación mantenida entre Statham y Stallone:
-	“¿Cuál es el plan?”
-	“Buscarlos, encontrarlos y matarlos”

Así de sencillo, así de efectivo ¿para que necesitamos más? ¿realmente necesitan más’ pues no. Una trama clara y concisa, sin rodeos, sin posibilidad de error, eso sí, con puñetazos, patadas, cuchilladas, con sangre y tiros, ¡muuuuuuchos tiros!. 

¿Habéis visto “Hot Shots  2“? Si es que sí recordareis esa escena en la que Charlie Sheen parodiando a Rambo coge una metralleta y empieza a pegar tiros y salen unas letras que dicen “más tiros que en Robocop”, sigue disparando y sale otra frase “más tiros que en Rambo”… y así sigue hasta tener una montaña de balas (que no se acaban nunca) a su alrededor y aparece un aviso en la pantalla “¡La película más violenta de la historia!”. Pues Los Mercenarios me ha recordado a esa escena pero durante los 102 minutos de metraje

Ojo, no estoy diciendo que sea mala ni mucho menos. Al ir a verla sabía lo que me encontraría y no me ha decepcionado y me ha encantado. He pasado un rato verdaderamente divertido y he disfrutado de lo lindo. Como he dicho antes una película directa y efectiva. Da al espectador exactamente lo que ha ido a ver.

¿Segundas partes nunca han sido buenas? No es totalmente cierto ya que podríamos encontrar numerosas segundas partes que sí fueron buenas y en este caso he disfrutado más viendo la segunda parte que la primera.
Es un auténtico tributo que los actores se hacen a ellos mismos. Una parodia de su carrera ochentena sin ser una peli cómica.
Los diálogos son una serie de frases sueltas socarronas enlazadas una con otra y aun así consiguen darle sentido al guión. Difícilmente llegan a decir más de dos frases enlazadas, pero ¿para qué? Frases socarronas una con otra, planos perfectamente rodados para realzar los músculos y venas de los brazos, coreografías de luchas impresionantes sin hacer uso de cuerdas o efectos digitales. En la película hay sutiles referencias a Rambo, a La Jungla de cristal, Terminator,… todas ellas de los actores que ahí aparecen. Chuck Norris hace gala de las típicas frases que corren por Internet respecto a el al estilo “El hombre de negro” del programa “El Hormiguero”

A continuación un breve diálogo (como todos) entre Stallone y Norris:
- (Stallone a Norris) “Había oído que estabas muerto, que te había mordido una Cobra Real”
- (contestación de Norris) “Sí, así fue, pero después de cinco días de insufrible dolor la cobra murió”

En definitiva, una excelente película llena de testosterona excelente para ver con el encefalograma plano y disfrutar de lo que se ve en pantalla y de la recopilación de actores de acción ochentenos que tantos buenos recuerdos nos traen.
Evidentemente no será galardonada a ningún Oscar por su guión, dirección, vestuario, ni mucho menos por sus interpretaciones masculinas, pero es efectiva al dar al espectador todo aquello que has ido buscando cuando se sienta a ver esta película.

Y para finalizar unas perlitas que se sueltan durante la peli:

“Le sacaremos la verdad a golpes” (Statham)
“Descanse en pedazos”(Stallone)
“La teoría de la relatividad de Einstein mejorada por mi” (Lundgren)
“Si quieres ser un hombre yo te haré un hombre” (Stallone a Van Damme)
        
            