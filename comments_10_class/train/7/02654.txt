
                      Tan buena como la primera, esta secuela de "Journal d'un dégonflé" es exitosa. Al igual que en la primera película, no todo es sobresaliente. Sin embargo, se pasa un buen momento, divertido e interesante. Escenas buenas y divertidas hacen que esta película sea placentera y agradable de ver.
        
            