
                      Una pelicula que se sale de lo convencional, esta por encima de la media, aunque tampoco deja una huella memorable. Gere y Sarandon ponen su talento al servicio de una trama que muestra el lado humano de un tiburòn financiero.
        
            