
                      Hace 15 años, Gore Verbinski llevo a Hollywood su peculiar visión de una película de terror japonesa, “The Ring“, y nos demostró a todos que su particular forma de ponernos los pelos de punta era brillante. Por ello, había muchas ganas de volver a verle en este género. Como digo, 15 años han bastando para volver a ponerse frente a una historia con una premisa importante, que da lugar a una película aterradora a la par que inquietante. Una historia que vuelve a regalarnos la oscuridad y el misterio que tanto nos enamoro en “The Ring” y que tan marca de la casa Verbinski se ha convertido.

Un empresario debe viajar a los Alpes suizos para traer de vuelta al CEO de su compañía, el cual está pasando unas vacaciones en un balneario. Cuando llega allí, descubre que el lugar es de todo menos un idílico paraje para descansar.

Podríamos considerar “La cura del bienestar” como una versión claustrofobica de “La invasión de los ultracuerpos“, mezclada con los mejores capítulos que nos regalo la temporada “Asylum” de “American Horror Story“. Durante la primera hora, ya sabes que estás frente a una de esas películas llenas de misterios, de secretos, una de esas películas en la que debes prestar atención a cada plano, cada gesto o cada frase de sus protagonistas. Que cada minuto que avanza en la historia, pueden ser piezas de un puzzle, que deberás resolver durante la siguiente hora. Y ese quizás sea, el mayor entretenimiento de “La cura del bienestar”: creer que formas parte de la trama, creer que debes ayudar al protagonista a desenmascarar a todos y cada uno de los misteriosos personajes con los que se va topando.

Además, Verbinski nos regala las mejores escenas escatologicas de toda su carrera. Esas escenas, que siempre nos revuelve el estómago y que nos hacen mirar hacia otro lado. Pero no lo hacemos, seguimos mirando la pantalla porque nos encantan estas escenas. Y este balneario nos las regala todas. Gracias, en parte, a unas simpáticas anguilas que poblan toda la película hurgando en los estómagos del espectador.

Pero un final demasiado precipitado y rápido, con muchas prisas por acabar con la función, sea quizás el punto negativo de la cinta, y eso que dura casi 150 minutos. Pero Verbinski está de vuelta, el Verbinski más terrorífico y misterioso lo esta. Y nos encanta.

Lo mejor: la banda sonora de Benjamin Wallfisch.
Lo peor: su excesiva duración.
        
            