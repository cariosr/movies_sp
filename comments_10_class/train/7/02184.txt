
                      Cuando salió "Harry Potter y la piedra del hechicero", era difícil concentrarse en otra cosa que no fueran las limitaciones de la película. El director Chris Columbus, como lo ha demostrado en películas antes y desde entonces, tiene poco sentido de la urgencia, no hay regalo para el ritmo.

Mire la lista completa de sus créditos y maravíllese de que el director de "Home Alone" recibió la serie de novelas juveniles más popular de la historia, comedias de mano dura que no son tan graciosas, intentos torpes de acción y una racha sentimental que probablemente fue el rasgo que más lo calificó para manejar lo que podría haber sido la primera y la única de dos películas, si hubieran fracasado.

Pero una docena de años más tarde, esto es lo que se destaca de "Harry Potter y la Piedra del Hechicero", todas las cosas que hizo bien. No se escatimaron gastos para hacer de este mundo, desde los suburbios británicos de las primeras escenas, a través del viaje en tren hasta Hogwarts. El detalle es hermoso parque temático, por lo que se convirtió en un parque temático tan exitoso aquí en Orlando.
        
            