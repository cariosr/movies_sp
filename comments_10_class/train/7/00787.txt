
                      No será una de esas historias originales pero el tratamiento que recibe la convierten en una de esas historias con las que pasar un buen rato, entretenido, en familia o con los amigos, riendo y saliendo del cine con una sonrisa por una familia prehistórica, Chico y su perezoso de tres dedos, y los animales que acompañan en la aventura por su salvación. Recomedable su visionado.
        
            