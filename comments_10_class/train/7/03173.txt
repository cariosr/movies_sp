
                      La película de Aja funciona ya que equilibra ser respetuoso con el original y desarrollar su propia y personal visión de la historia. Destaca por ser mucho más fantasiosa que la original, creando un clan de mutantes deformes que son claramente "monstruos de película", pero creo que es un acercamiento que resulta efectivo ya que ayuda a digerir las escenas de violencia más explícita, igual que la narrativa mucho más estilizada.
        
            