
                      "Es tal su sinceridad y profunda sensibilidad que provoca inconscientemente una conexión distante entre la mente creadora y el espectador en varios momentos. En el lado cinematográfico, posiblemente sea lo mejor de Almodóvar"
        
            