
                      Di que sí, es una comedia estadounidense dirigida por Peyton Reed y producida por David Heyman, productor de la saga Harry Potter.

Carl es un bancario estancado en su vida y su trabajo. A cualquier cosa que se le plantea dice que no.
Un día se encuentra con un viejo conocido al que le ha cambiado la vida a mejor tras asistir a un seminario motivacional, en el que le proponen decir que sí a todo. Carl decide asistir y probar suerte con el reto.

Esta comedia, protagonizada por Jim Carrey tuvo una aceptación favorable, llegando a recaudar más dinero que otras películas del actor, como 'Dick y Jane: ladrones de risa', pero sin llegar a superar a 'Como Dios (Bruce Almighty)'.

Jim Carrey ganó el premio MTV Movie Awards al mejor actor de comedia, debido a su gran interpretación cómica a la que nos tiene acostumbrados.
A pesar de los merecidos méritos que recibió, Carrey lleva su papel a su estilo, muy marcado por las muecas y tics en la cara, que este personaje no requería.
Los personajes secundarios quedan eclipsados con respecto al protagonista, aunque no por ello son menos importantes, ya que realizan un estupendo apoyo con sus interpretaciones.

Es una película con unas dosis de humor muy bien repartidas, en la que las escenas van desarrollando un tono cómico adecuado, de forma que dejan al espectador con un buen sabor de boca.
        
            