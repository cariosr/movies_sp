
                      Un final más que aceptable para una saga que ya ha durado lo suyo. Con tantas entregas, cada una más enrevesada que la anterior, en este caso la historia vuelve a donde todo comenzó para dar carpetazo con una sorpresa más que agradable.
        
            