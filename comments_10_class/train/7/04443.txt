
                      La peli te hace pensar.

Intenta demostrar que el poder de una persona depende solo de si misma, y que es necesaria referencia exterior para realizarlo.

La transformacion de Vikram es una parte importante del film. De construir su personaje guru, que es una farsa, pasa a darse cuenta que representa lo mejor que existe en el. Y que el personaje es real, pues tiene impacto real y saca la mejor sabiduria de el para ayudar a los demas

Lo peor: la peli no se define y no explica bien el grado de transformacion de Vikram, si lo hay. Las reacciones de algunas personas sometidas al "experimento" no quedan bien explicadas al final. Quiza se podia haber indagado algo mas en las motivaciones y vidas de los "discipulos".

Es neutra en cuanto a que no toma posicion respecto a algo habitual en la cultura del yoga, relacion maestro - guru. Empieza con un claro cinismo al principio. Al final el punto de vista queda a opinion del espectador. 

Lo mejor: Peli fresca, abierta. Te hace pensar. Tiene un punto agradable de farsa o comedia.

Buena peli,para debates espirituales. En segun que personas, podria levantar ampollas
        
            