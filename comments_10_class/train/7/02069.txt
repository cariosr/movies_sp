
                      Una de las peores cosas que nos pueden pasar al ir al cine, es ir con una idea preconcebida y encontrarnos que no es para tanto. Más cuando lees hasta la saciedad que algo es oro puro y cuando lo tiene delante ves que perfectamente podría pasar por bisutería. Pero en el mundo del cine es muy difícil evitar esto. Con “Deadpool” me ha ocurrido algo parecido. En un nivel mucho más bajo que con otras películas, pero no me he encontrado con el peliculón que esperaba ver.

Si es cierto que estamos frente a una película de superhéroes totalmente inusual. El spin-off de los “X-Men” llega con un toque distinto al del resto de películas Marvel. Tanto la estructura de la trama, como el guión, e incluso la interpretación de Ryan Reynolds es una grata sorpresa. Además, si es cierto que la cinta tiene unas ganas tremendas de hacértelo pasar bien, pone todo de su parte para entretener y divertir. Pero quizás se le vea el plumero a tantas ganas de ser diferente. Hay ciertos momentos en los que se nota demasiado que el personaje se esfuerza en hacerse el gracioso y esas ganas son las que precisamente le quitan gracia. Por lo que tenemos escenas que sí nos producen momentos tronchantes pero hay otros que incluso dan un poco de vergüenza.

Un típico producto blockbuster, totalmente para mayores de 18 años y que solo sirve para hacerte pasar un buen rato. La película es amena y está  llena de humor, si, pero no más que “Ant-Man” (el hombre hormiga sí que fue para mí una verdadera sorpresa) y “Guardianes de la galaxia” (Pratt, amazing).

Lo mejor: su banda sonora.
Lo peor: la escasez de escenarios que llenen la pantalla.
        
            