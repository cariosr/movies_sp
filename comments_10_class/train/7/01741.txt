
                      Dentro de las limitantes que tienen los films destinados a la televisión, éste indaga desde la arista del drama, en aspectos tan complejos como la manipulación, la pedofilia -abordada como un telón de fondo- y la carencia de empatía del personaje abominable que construye Clancy Brown, capaz de inducir al homicidio a su propia hija, como un modo de deshacerse de ella y de su esposa y hacerse con su adolescente cuñada. Inquietante este monstruo que posa de padre adorable...
        
            