
                      La Cabaña en el Bosque (2012)
Una deconstrucción del género slasher que le da un sabor novedoso y original a la película.
Los protagonistas son los clásicos arquetípicos del slasher: el musculitos, el empollón,... pero esto sirve nuevamente para jugar con los géneros para crear un nuevo terror casi cómico.
Una dirección correcta que bebe mucho del estilo de Edgar Wright. Aparte de esto, vemos un claro amor por el cine de terror clásico del que se recogen muchos homenajes sin llegar a estorbar en ningún momento.
        
            