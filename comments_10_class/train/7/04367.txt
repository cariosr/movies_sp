
                      Muy distraida pero "se queda corta". La pelicula intenta crear un mundo diferente a través de la física cuántica y los programa de ordenador, algo ya visto en "Matrix", "Tron" o "Deja Vu", es decir, "la realidad" es un mundo y "la ficción informática" otro aunque parezca muy real. No se entiende que ambos mundos "converjan" por lo que esperas que el 
final tenga un giro inesperado, pero "te quedas con las ganas". Creo que la idea es muy interesante pero no han sabido desarrollarla y la pelicula termina haciendote pensar que han intentado complicarlo todo sin "saber dar con la tecla adecuada".
        
            