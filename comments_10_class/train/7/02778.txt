
                      Hay destellos de acción y humor orientados comercialmente, pero el sentimiento general es el de una depresión lánguida que brota directamente del corazón de su autor.
        
            