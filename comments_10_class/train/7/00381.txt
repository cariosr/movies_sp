
                      John Wick: Pacto de Sangre (2017).
Vuelve Keanu Reeves con la secuela de su personaje ultraviolento con más coreografías, más acción y más sangre que nunca. Una mezcla de tiros, Sutemis (un tipo de proyección de Judo), Atemis (golpes a tope) y puñaladas en un improbable mundo organizado de sicarios, donde Wick hará temblar los cimientos de Roma y NY al verse obligado a volver a retomar su "oficio". 
Dos horas de muerte y destrucción para los amantes de este género, dirigido por el mismo responsable de la primera parte en un estilo cruce de John Woo y Sam Peckinpah.

La crítica, curiosamente, la trata bastante bien. En mi opinión no supera a su antecesora, pero no deja títere con cabeza, y los secundarios suben de nivel: a parte del "ruso" Peter Stormare, tan repetido en este tipo de cintas, tenemos al británico Ian McShane, a Common como el archienemigo de nuestro héroe y a Larry Fishburne.
¡A disfrutar sin tener que usar mucho la cabeza...!

Raúl Cabral.
        
            