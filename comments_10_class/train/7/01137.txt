
                      Tras un par de películas que fueron un fiasco de crítica y de taquilla, Shyamalan vuelve a trabajar como le gusta, con sus ideas propias, creando una pequeña historia llena de tensión sutil, de guiños al cine clásico pero con el toque personal que lo caracteriza y los giros que ya son santo y seña de su filmografía. Cada trabajo de Shyamalan es un huevo de Pascua para un / una cinéfil@.
        
            