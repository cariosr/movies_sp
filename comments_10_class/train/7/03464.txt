
                      Un cuento antes de Navidad

Un buen guión,una historia divertida que contar,la película parte de una premisa realmente interesante,el mundo de Halloween quiere celebrar la navidad,una película que crea unos personajes curiosos e icónicos en el cine,y por supuesto en el cine de Burton aunque está pelícua la dirija Henry Selick,quien nos díó mejores obras como "Los mundos de Caroline" o "James,el melocotón gigante" .A pesar de eso,los personajes no me han llegado y la historai no me ha mantenido interesado,no ha tenido giros interesantes,se ha mantenido demasiado regular.Pasando a las canciones, "This is Halloween" es mítica,pero realmente las demás no son para tirar cohetes. Para ser sinceros no me ha entusiasmado el filme al que sin embargo le coloco 3 estrellas y media porque si,porque es atrevida,porque es valiente y tiene un sello propio que busco en los filmes,y  esto hay que premiarlo.

Recomendable,sí,imprescindible no,pero si creo que para cualquier cinefilo el visionado de una pelicula de animación tan diferente como esta es necesaria.
        
            