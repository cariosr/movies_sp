
                      Mientras que tus ojos pueden brillar durante la introducción que se adentra en el mundo de los elfos oscuros y antiguas batallas entre deidades paganas, hay una buena razón para mantenerte despierto por este tonto, explosivo y extrañamente cálido hilo que teje una fría historia de disfunción familiar con espectáculo matinee.

Por supuesto, la escena de apertura se parece más a un triste regreso a las hinchadas escenas digitales de Peter Jackson, ya que establece el gran dilema a través de una batalla épica entre elfos y asgardianos, pero no dejes que se te llenen las barbas de jengibre y las fantasmagóricas máscaras faciales. largo.

Hay un arma de destrucción masiva llamada el Éter - una nube roja enfermiza que impregna la carne y la infunde con poder especial, y quien posee este veneno especial puede ganar cualquier guerra o conquistar cualquier mundo, razón por la cual el abuelo de Thor ocultó el arma en un mundo muy, muy lejos.

Sin embargo, por alguna misteriosa razón, el Aether se está moviendo y está causando estragos en la Tierra.

Agujeros en el continuo espacio-tiempo están comenzando a aparecer, lo que nos lleva a un territorio familiar cuando nos reintroduce a Thor (Chris Hemsworth) - el héroe con grandes músculos y un hermano pequeño y malvado Loki (Tom Hiddleston).
        
            