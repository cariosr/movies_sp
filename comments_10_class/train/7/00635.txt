
                      Esta original historia sobre una convivencia de los vikingos con sus enemigos los dragones que nos trajo Dreamworks es otro de sus numerosos aciertos: Visualmente es muy bonita, los diseños de los dragones (en especial el principal) son graciosos y vistosos, la historia es muy entretenida y gustará tanto a peques como a no tan peques. Destacar las animaciones de vuelo, visualmente vertiginosas.
        
            