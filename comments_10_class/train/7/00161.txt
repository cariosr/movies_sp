
                      La Llegada es una película de extraterrestres que va más allá de la ciencia ficción. 

Hasta ahora gracias a las novelas y al cine, hemos conocido muchas historias de visitantes planetarios y de diferentes tipos: de Abducciones (Intruders, La Llegada), de Invasiones (Señales, Mark Attack), de Acción (Independence Day, Skyline, La Guerra de los Mundos), de Terror (Alien,The Thing) y otras muchas entre las que también hay de Primeros Encuentros (E.T., Encuentros en la Tercera Fase), pero  “La Llegada”, no tiene parecido con ninguna de ellas.

El relato, es lo mejor de la película, y  está basado en una novela corta del escritor Ted Chiang, que también participa en el guion, se titula: “Stories of your Life – La Historia de tu vida” y bajo ese título se encierra la trama de la película, donde el medio y la narración, es el primer contacto con 12 naves extraterrestres que aterrizan en nuestro planeta, sin manifestación alguna de actividad, salvo unos sonidos y el acceso a un punto intermedio de su nave para poder comunicarse.

El Gobierno de los EE.UU recurre a la reconocida lingüista Louis Banks (Amy Adams) para poder traducir y comunicarse con ellos, contando con la ayuda de Ian Donnelly (Jeremy Renner). La película está hecha para el lucimiento de Amy Adams, que es protagonista absoluta de la cinta, complementada por Jeremy Renner  y los demás papeles son completamente intrascendentes, salvo una única intervención de Tzi Ma, que interpreta al General Shang.

Su desarrollo es muy natural, y quizás más real de lo que son muchas películas del género, ya que descubre las dificultades de poder comunicarse con una raza alienígena que no utiliza ninguna lengua; que emite sonidos, y que están sumergidos en una atmosfera densa, separados de los humanos por un cristal, donde escriben símbolos. Y digo que es natural y real, porque les lleva mucho tiempo el poder encontrar la forma de comunicarse; y ahí entra la segunda parte de esta película: el tiempo.

No desvelaré nada del argumento, porque es importante descubrir lo que se nos presenta desde el principio, su evolución y el final de la misma; pero sí que descubre mucho más que el contacto con los visitantes, y es la transcendencia de nuestra propia vida.

Si vas al cine con la idea de ver una peli más de ciencia ficción, no te gustará…. Si por el contrario te dejas llevar por la historia, te dejará pensando en muchas cosas de tu propia vida, y entonces no quedará como una película más; sino como una reflexión existencial de nuestros hechos, lo que suponen, a donde nos llevan y aun así llevarlos a cabo aunque nos cueste hacerlo.

Calificación: 🎥🎥🎥🎥🎥🎥🎥🎥 (8 sobre 10)
        
            