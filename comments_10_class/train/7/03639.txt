
                      Esta película está protagonizada por dos grandes actores, que hacen dos grandes papeles. Los dialogos y el entorno en si, da una harmonia a la pelicula que devuelve a Diane Keaton a una de sus más clasicas peliculas con Woody Allen "Manhattan". 
La misma senzillez y sensibilidad que la envuelve, hace de ella una película muy especial.
        
            