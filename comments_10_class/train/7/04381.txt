
                      Esta película es espectacular, sobre todo por los paisajes de la jungla tropical de las cataratas de Iguazú que son de una gran belleza. La banda sonora es sublime, para mi la mejor que ha realizado el gran Ennio Morricone. El guión de Robert Bolt de esta aventura épica esta muy bien. Las interpretaciones principales de Robert de Niro y Jeremy Irons son magnificas (como siempre) al igual que los secundarios como Liam Neeson o Ray McAnally.
        
            