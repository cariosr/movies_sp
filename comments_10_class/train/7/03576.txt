
                      Agárralo como puedas es una comedia dirigida por David Zucker, y escrita por su hermano, Jerry Zucker, y Jim Abrahams.

La película está basada en la serie de televisión Police Squad!, creada por David Zucker, en la que ya aparecía Leslie Nielsen.

El teniente Frank Drebin (Nielsen) sigue las pistas de un caso que su compañero policía investigaba, cuando le dispararon en un embarcadero. Todas las pistas apuntan a que Vincent Ludwig (Ricardo Montalbán), está detrás de todo.
Con la ayuda de su amigo Ed (George Kennedy) y de su novia Jane (Priscilla Presley), intentará detenerlo.

Leslie Nielsen, acostumbrado a actuar en comedias absurdas, realiza el típico papel de héroe despistado que resuelve todos sus casos a base de errores. La cara afable del protagonista ayuda mucho a creernos su inocencia y estupidez infantil.

La cinta cuenta con innumerables gags y conversaciones sin sentido, que logran arrancar la risa a cualquiera. Debido a esto, la trama deja de tener interés, ya no importa la buena, mala u original que pueda ser.

La película tuvo tanto éxito que se prepararon dos más: 'Agárralo como puedas 2 y 1/2: El aroma del miedo' y 'Agárralo como puedas 33 1/3: El insulto final'.
        
            