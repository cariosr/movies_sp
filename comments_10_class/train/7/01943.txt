
                      Documental amable, sobre esos locos bajitos que tanto se nos parecen, que nos invita a reflexionar sobre las comodidades que tenemos en occidente, a la vez que nos mantiene la sonrisa en los labios, y nos consigue sacar una risa en determinados momentos.

Cuatro culturas, cuatro formas de ver la vida y de criar a los hijos, diferentes formas de pensar, de vivir y una cosa en común el milagro de la vida. Los primeros logros y frustraciones de cualquier ser humano, los primeros pasos, el instinto de supervivencia, la protección de las madres, tan iguales y a la vez tan diferentes. De la escasez a la abundancia, de las llanuras inhóspitas a la apabullante civilización.

Nos ayuda a apreciar lo que tenemos, enseñándonos desde el punto de vista de un bebe que no es más feliz el que mas tiene, sino el que se conforma con lo que tiene.

Recomendable para todas aquellas personas que en su familia haya o esperen un bebe, pues mientras ves este documental, te vas dando cuenta de los posibles errores que puedes tener al educar a ese bebe, sin dejar de tenerlo en mente.

Agradable, divertido y a la vez didáctico e interesante.
        
            