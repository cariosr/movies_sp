
                      La redención del antihéroe americano 


Al igual que ocurre con el coche que da título a la película y se puso de moda en la década de los 60 y 70 en E.E.U.U. - Gran Torino-, Clint Eastwood nos regala una obra de elevadas prestaciones y gran potencia dramática lejos del trazo clásico y mirada sobria de su anterior film El intercambio, de ahí que el aspecto formal de la misma pase a un segundo plano ( pocos escenarios y una historia mínima de gran profundidad moral). Este valorado coche de época, tan presente en cine y televisión ( en la serie de los setenteros policías Starsky & Hutch ), es un personaje más que adquiere una dimensión simbólica como metáfora de un tiempo de esplendor.

La historia se centra en un viudo y héroe de guerra de Corea – Walt Kowalski- y las relaciones de éste con la familia y sus vecinos hmongs (etnia del sudeste asiático). El intento de robo de su preciado Ford Gran Torino por el joven vecino Thao, hijo pequeño de los vecinos, impulsa una relación cada vez más cercana con el objetivo de hacer de éste alguien de provecho ante la amenaza de convertirse en un pandillero más.

Clint Eastwood interpreta a Walt Kowalski entre gruñidos y voz rota aunando a personajes de obras anteriores como el militar inquebrantable que dice tacos ( El Sargento de Hierro) o el justiciero ( Harry el Sucio) para poner de relieve la capacidad de transformación del ser humano en un plano social en el que convive lo tradicional ( el maestro devenido de la experiencia veterana de Kowalski y la juventud de Thao) frente a lo contemporáneo ( racismo, interculturalidad, globalidad).

No deja de ser una mirada cercana a su esencia, a su universo temático (relaciones familiares, la iglesia, la justicia, las relaciones humanas) que explosiona en un final abrupto y violento de forma inesperada y, también, conciliadora por la trayectoria vital del ex combatiente que crece emocionalmente conforme avanza la película hasta llegar a la redención marginando todo tipo de prejuicios que pudieran resultar reaccionarios.

En definitiva, no dejen de ver Gran Torino por su magistral dirección e interpretación que nunca decepcionan en el cine del maestro Clint, por su humor destacable (relación con el sacerdote), por sus buenas intenciones integradas en una historia llena de diálogos sencillos que pueden resultar estridentes ( interpelaciones malhumoradas constantes de Kowalski hacia los personajes que habitan el film) y, no por ello menos importante, por la partitura final compuesta y ensalzada por la mística voz de Eastwood.
        
            