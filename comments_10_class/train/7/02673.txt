
                      Lecciones de infinitos de Bajo la misma estrella.

Fui ayer a ver “Bajo la misma estrella”, de Josh Boone, basada en la novela de John Green, que por cierto, llega a España con un título que nada tiene que ver con el original: “Un fallo en nuestras estrellas”, y totalmente opuesto con el que se publicó en Francia, “Nuestras estrellas contrarias”, y una no acierta a saber con qué criterio se traducen los títulos de libros y novelas. Espero que sea con alguno lógico.

La película es un libro rodado, quiero decir con esto que no hay concesión para los amantes del cine como expresión de fotografía o maestría de planos; es una película literaria, donde la historia es el centro y absoluto universo de la pantalla. Y es esta historia, que podría parecer un tonto cuento de amor adolescente, la que se rebela como una historia plena, holística, en el que toda la infinitud del amor, del dolor, de la vida y de la muerte está contado con honestidad. (Salvo a mi criterio el episodio del viaje a Ámsterdam que tiene una buena dosis de impostura).

El poso de la película está en relación con los infinitos de Cantor; los infinitos no son iguales, pueden ser grandes y pequeños y, aplicada esa abstracción a la vida, la eternidad puede ocurrir en cualquier momento, con independencia de que sea finito el instante siguiente. Vivir con plenitud pequeños infinitos sin más preguntas colaterales que están ahí pero que deben ser obviadas para conseguirlo. A esa capacidad llegan los personajes porque aprendieron a ignorar preguntas inútiles. No hay en la película cuestiones de ¿por qué me pasó esto a mí?; lo maravilloso de estos personajes es que ya ellos se resolvieron a sí mismos como problema y ese trabajo anterior (que si está en el libro) les permite, a pesar de su enfermedad terminal, construir infinitos. Infinitos en ínfimos espacio de tiempo, infinitos en la peores condiciones de salud… un amor eterno en el breve tiempo que les es concedido.

Y se agradecen esas dos lecciones: no son absolutamente relevantes en nuestras vidas la magnitud de los infinitos que nos sean permitidos; la relevancia está en su vocación de eternidad; pero para saber amar con infinitud, para vivir con eternidades sucedidas, antes mejor que nos resolvamos a nosotros mismos como problema. Mejor que nos hagamos las preguntas adecuadas y desechemos las que no nos conducen a ninguna parte.

Esa valentía no sólo nos da la plenitud del ahora. También nos enseñó Cantor, (aunque no aparece en la película) que existen infinitos potenciales a partir de nuestros infinitos actuales. Y con el optimismo que da la potencialidad me he levantado esta mañana, concibiendo posibilidades…

Ocurre que luego la vida va a su bola, y en vez de acompañarme a los infinitos de Cantor, me mete de pleno en sus conjuntos: tengo que poner una lavadora de ropa de color, otra de ropa blanca que es la mitad de la anterior… ¿será esto a lo que los matemáticos llaman fractal, aquella figura que no cambiaba nunca ni dependía de la escala con la que se observaba?…

En fin, una estupenda tarde de sábado… y una película para ir a ver con alguien inteligente. Yo ayer tuve esa suerte.
        
            