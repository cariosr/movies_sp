
                      Amable, emotivo y  recomendable largometraje, que trata temas como el valor, la cobardía y la traición, vistos a través de los ojos de unos niños, y en el marco de la Francia ocupada  por los  nazis. Consigue mantenerte  la  mueca de la sonrisa en la cara, durante toda la proyección.
Primeras y viejas historias de amor, avanzan paralelas a las batallas entre los niños de ambos pueblos, batallas que incluso llegan a tener hasta su punto épico como si batallas de adultos se tratasen. Véase el pequeño homenaje a Braveheart.
A destacar una excelente banda sonora, a través de la cual canalizamos nuestras emociones, acompañada de una estupenda fotografía, con el fondo de los verdes prados franceses.
Lástima que al final no llegue a transmitir toda la fuerza dramática que pretende.
        
            