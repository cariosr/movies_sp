
                      ¡Ah, la infancia! Cuando crecemos, lo queremos saber todo; de niño, lo que vale (lo que hace vida) son los enigmas... ¡Qué fotografía más bonita, qué bien cuidada (no solo exteriores)!
        
            