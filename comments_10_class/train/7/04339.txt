
                      Una buena película donde director y reparto están a un altísimo nivel. Una grata sorpresa los actores que interpretan a la pareja de ladrones y el director con unos planos muy bien elegidos y unas escenas de gran realismo.
Lo único que hace que no sea una gran película es un guión flojo ( lo cual da aun más mérito al director)
Muy recomendable
        
            