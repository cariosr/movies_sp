
                      La primera parte de la trilogía cinematográfica del director Peter Jackson de J.R.R. El Hobbit de Tolkien obliga al público a correr una carrera de obstáculos antes de que comience la diversión.

En primer lugar, es necesario superar el aspecto de la misma. El Hobbit: Un viaje inesperado está haciendo un extraño tipo de historia al salir en versión limitada a 48 cuadros por segundo (el doble del estándar habitual). Combina eso con 3D y la película parece tan real que puedes ver todo lo que es falso, desde juegos pintados hasta narices protésicas. El efecto desagradable es similar a mirar una película en un nuevo monitor de cine en casa HD, sombras borradas por la luz cegadora como ... ¡Ay! - Reality TV.
        
            