
                      Una película del director Paolo Virzí que mezcla dos géneros, el thriller y el romanticismo. Su comienzo es desconcertante por la forma magistral de mezclar escenas que no se comprenden, pero que según transcurre la historia, van encajando y van teniendo su coherencia. Crítica valiosa sobre la mediocridad de las clases altas, no solo italianas, sino en general, capaz de llegar a donde se le antoje a través del único modo que conocen, el poder ecónomico.
        
            