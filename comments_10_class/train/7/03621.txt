
                      Hasta un final que vuela fuera de lugar, la comedia de suspenso y comedia de Paul Feig saca lo mejor de Anna Kendrick y Blake Lively y ofrece una visión inesperada de los giros y las vueltas de la amistad femenina.
        
            