
                      Fui a verla con mi mujer, mi hija y mis tres sobrinos...sentados en segunda fila...jajajaja...sus caras de impresión ante la magia del cine...que consigue recrear tan magníficas criaturas...mereció la pena...disfruté, viendo disfrutar y aunque había indudables guiños a las anteriores películas de la Saga...y consiguió entretenerme...supongo que será cosa de la edad...pero ya no me emocioné tanto como con Jurassic Park.
        
            