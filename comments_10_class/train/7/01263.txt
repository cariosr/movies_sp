
                      "La fórmula continúa, pero el resultado no es le mismo"
Ahora es el turno de satirizar los clichés de la secuelas, con toda una discusión universitaria que supone un resumen sobre las mismas, resultando toda una delicia. Los asesinatos, el Gore, las referencias a clásicos de terror vuelven, logrando un resultado muy parecido a la primera; pero con un ligero cambio en la dirección. 
A favor: El Gore, las referencias a los clichés de las secuelas y una vez más el final impredecible.
En contra: Lo ruidosa que es a comparación de la primera le resta cierto "cinismo" y en ocaciones abusa del humor.
        
            