
                      Si dejamos de lado una emotividad forzada y falsa, "Buscando a Dory" es una película que, sin ser de las mejores de Pixar, destaca por su calidad, por su ritmo y que no engaña a nadie con el mensaje que transmite: una aventura mínimamente transgresora que exalta los valores ya muy bregados de la amistad y la familia unida.
¡Ah! Y al final no os vayáis hasta terminar los créditos: falta la última sorpresa. (7,5/10)
        
            