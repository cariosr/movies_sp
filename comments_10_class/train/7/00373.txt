
                      Película  recomendable, con sus pros y sus contras con una historia de supervivencia excepcionalmente rodada. 
Si consigue engancharos cuando llega la ola sentimental, seguro que os parecerá magistral e inolvidable.
        
            