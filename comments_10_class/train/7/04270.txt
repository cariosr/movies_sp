
                      Muy buena!!!. Similar a la película "Identidad" del 2003, el protagonista posee personalidades múltiples con características muy disímiles entre sí. Tensión en todo momento, drama y suspenso se combinan en el film. Además vale destacar la brillante actuación de James McAvoy en su rol protagónico. Quizás el final, sin entrar en detalles, no me convenció del todo. Muy recomendable.
        
            