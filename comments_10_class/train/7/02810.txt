
                      Una vez más el cine oriental nos muestra que una producción con actores reconocidos y una gran inversión, no es sinónimo para una excelente película. 

Occidente presencia quizá la catapulta fílmica de muchos proyectos que nos quitarán el mal acostumbrado cine Hollywoodense. Tren a Busan, lleva la bandera del cine independiente que surge a partir de crear nuevas historias en el cine. Nos quita ese empalagoso "Remake" y de muchas continuaciones de hartazgo. 

El virus zombie, detonante para un apocalipsis mundial, lo lleva muy parecido a World War Z (2013) (Guerra mundial Z). Quitando los grandes escenarios y los efectos especiales dando un estilo The Walking Dead y zombies tipo Dawn of The Dead (2004) (El amanecer de los muertos). Aquí el contagió es rápidamente transmitido a todo ser vivo. No se enfocó en los detalles de las especies infectadas y se encapsuló en una área preferente en un tren, la estación, alguna parte urbana y  panorámicas de ciudad. Los efectos especiales entran en lo normal para esta propuesta de cine independiente. Lo resolvieron generando secuencias más cuidadas dedicadas en la actuación y en cada personaje que por momento se volvieron parte de nosotros. Los que vivimos en occidente, en general no conocemos quien son estos actores y sus anteriores trabajos. Y creo que eso no importó mucho para entender lo que transmitió está película.

Nos presenta un padre dedicado a los negocios, su empresa y olvidado de sus allegados. Faceta que la modernidad laboral nos encarcela con trabajo y nos quita la verdadera vida en familia. Una hija con la inocencia y su fiel valentía al respeto de un padre. La vida de muchos y el cariño de la fuerza de la esperanza. Considero quien realmente fueron esos zombies, es la sociedad que vive en este limbo encerrado de egoísmo. Y aquí, es peor que el virus.

Tren a Busan, transmite y lleva secuencia del cine que Hollywood en un momento nos vendió. Compitió en cartelera con Rogue One y levanta las cejas de muchos Directores.
        
            