
                      Tom Hanks, como siempre, hace un papel buenísimo.
La mezcla de culturas y costumbres que se ven en la película, siempre con buen humor, le dan el punto cómico.
Y la búsqueda del bienestar del personaje hace que sea una de esas películas que te hacen reflexionar.
        
            