
                      No soy fan del formato 3D en las películas, ya que parece a veces hasta forzado meter alguna chorrada volando delante enfrente tuyo para justificar el precio de la entrada, pero en esta película si está preparado para disfrutar del espectáculo visual que ofrece.
Una historia acerca de una catástrofe (el protagonista Pi naufraga en medio del océano en una balsa junto a un tigre de bengala), muestra la supervivencia, la fe (este punto es discutible, pero es lo que quiere vender la cinta) y sobretodo como el ser humano es capaz de superar cualquier acto traumático con el poder de nuestra mente.
A la película le doy un 7, pero los últimos cinco minutos un 10.

