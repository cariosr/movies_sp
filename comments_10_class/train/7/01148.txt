
                      La primera Poltergeist, es una de las primeras películas de terror que muchos vieron cuando eran niños, y es que en cierto modo, Poltergeist es una película de terror para niños, y este remake no es tan diferente de aquella película, es una película de terror para niños, pero bueno puede que para algunos eso signifique que es mala, pero no tiene por que ser así, y de he hecho no es así, este remake respeta a la original, no intenta aprovecharse de ella como el remake de posesión infernal o el de Pesadilla en elm street, es más como el remake de las colinas tienen ojos o el de la matanza de Texas, que sin ser tan buena como la original, es bastante decente, además ayuda que tenga como protagonista a alguien tan bueno como Sam Rockwell.

Reparto: El ya citado en el anterior parrafo Sam Rockwell, es muy bueno y eso no hay quien lo dude, Jared Harris como cazafantasmas burlón y contando batallitas sobre sus heridas esta en estado de gracia, el resto del reparto esta correcto sin estar mal en ninguna escena no esta genial en ninguna otra tampoco.

Dirección: Gil Kenan es el director de este remake y no solo demuestra lo bien que sabe usar el scope, si no que homenajea a peliculas como terror en Amityville, en la escena en la que agarran al personaje de Saxon Sharbino y le hunde el pie en una especia de fango, o a la Poltergeist original, en la escena del payaso cambiada y alargada para bien, en mi opinión la escena en la que Rockwell vomita una especia de gusanos, extrañamente me recordo al cine de Raimi, sobre todo a Arrastrame al infierno, más que a Posesión Infernal, no he visto más películas de Gil Kenan, pero espero ver más dentro de poco.

En conclusión: Un muy buen remake, con una muy buena direcciónn y un gran protagonista y un reparto correcto, unos cuantos homenajes y algunas escenas angustiosas como la del taladro en la habitación de la niña o perturbadoras, todo lo que se ve en el terreno astral de manos de la camara del dron.

Lo mejor: Sam Rockwell, Jared Harris, la dirección y algunas escenas muy buenas.

Lo peor: Es inferior a la original y a la hora de compararlas (algo innecesario) saldrá perdiendo a pesar de ser un producto muy notable
        
            