
                      A todos los que de una manera u otra han vivido alguna experiencia sobre el escenario o detrás de él esta película les traerá muy buenos momentos. Aunque tiene ciertos altibajos en las dos horas de duración, a mí me parece una candidata a todo en los próximos Oscar. Iñarritu, una vez más, no me decepciona.
        
            