
                      El guión, fluye entre el humor -excelentemente- absurdo, y la sátira, que agrega a la mezcla el componente picante y crítico, y hace que lo demás, un poco endulzado, no parezca demasiado meloso. Y, aunque conocemos la historia real, el hilo del argumento en la película no parará de sorprenderos, con escenas genuinamente disparatadas, y el magnetismo entre los actores Spacey y Shannon queda retratado. Estoy segura de que estos factores os harán disfrutar muchísimo.
        
            