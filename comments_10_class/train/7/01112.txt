
                      Para todos los que duden en ir a ver una nueva adaptación en carne y huesos de la productora del ratón, debo decir que Pedro el inglés y su viejete dragón Elliot desborda poesía y ternura, mucha pero que mucha ternura, pero deberéis aguantar un comienzo sin nada relevante que ver aparte de un paisaje de ensueño.
        
            