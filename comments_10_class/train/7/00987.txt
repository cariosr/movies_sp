
                      Una película a la vez ligera e profunda, sobre la vida, que alcanza su espectador por ser justa como sincera. Es evidente que el exito de este tipo de película recae en gran medida en sus actores y los de "Silencio por amor" presentan personajes en los cuales creemos al instante. Un autentico buen momento, simple y generoso.
        
            