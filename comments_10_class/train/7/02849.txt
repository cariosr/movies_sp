
                      La nueva película de Greengrass es capaz de tenerte pegado a la butaca durante dos horas. Es otra vuelta de tuerca al sueño americano, y tiene su parte de denuncia social, pero básicamente es un thriller muy entretenido.
        
            