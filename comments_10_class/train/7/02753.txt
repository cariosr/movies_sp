
                      No contaré la trama. No me gusta hacerlo ni creo que sea lo que se busca en estos foros. Mi aportación si les sirve, es la siguiente:

  Una película excesivamente larga y bastante sobre valorada. Después de los primeros 15 minutos de acción, muy bien filmados y que sirven para detonar la historia de la película, se les acaba el argumento y la trama se soporta únicamente en la magnífica fotografía y el increíble paisaje luchando por soportar la ausencia de argumento; hilando situaciones poco creíbles y, desde el minuto 25, ya todos conocemos el final. Esperamos pacientemente por un giro sorpresivo que nos justifiqué la expectativa que genera combinar a Iñárritu con Di Caprio, pero este giro nunca llega. El final que esperábamos desde el minuto 25 llega sin sorpresas entre bostezos y cajas vacías de palomitas.

  Es una lástima porque una de las partes más importantes de la Dirección es saber manejar los tiempos del filme para éste que "transmita" sensaciones al espectador, y en este rubro Iñárritu se queda muy pero muy corto. Olvidó que una de las premisas de una buena película es que "menos, es más".

  La actuación de Di Caprio es buena pero la película y la pobreza del guión no dan para explotar más al personaje. Lo mismo pasa con Tom Hardy y se desperdicia el potencial de este actor. 

  Si te sobran más de dos horas, podría valer la pena verla, pero hay mejores opciones en el cine.
        
            