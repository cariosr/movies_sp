
                      Batman es una película de superhéroes dirigida por Tim Burton. Está basada en el cómic homónimo de DC Comics, creado por Bill Finger y Bob Kane.

Harvey Dent y James Gordon son detectives de Gotham, una ciudad llena de corrupción. Sus investigaciones sobre la mafia los llevan hasta Jack Napier (Jack Nicholson), a quién su jefe le ha tendido una emboscada.
Mientras tanto, la periodista Vicki Vale (Kim Basinger), investiga al misterioso enmascarado conocido como Batman (Michael Keaton), que se dedica a atacar a los delincuentes.

Michael Keaton, en un principio, no gustó como opción a ser Batman, ya que era más un actor de comedia. Sin embargo, el resultado de su interpretación es indiscutible.
Pero sin duda, el que destaca en el filme es Jack Nicholson, que realiza una actuación soberbia en su papel de villano psicópata.

En mi opinión, esta es una de las mejores películas del superhéroe de DC, aunque no sea la que cuente con mejores efectos visuales.

La oscuridad que caracteriza las películas de Tim Burton queda muy bien en esta, ya que a Gotham City se la distengue por la penumbra.

La película estuvo nominada a ocho premios diferentes, entre ellos un Oscar y un Globo de Oro.
        
            