
                      Como Trillher funciona muy bien. Durante casi toda la película uno mantiene la expectativa sobre cómo la familia protagonista va a salir de esa situación, llegando a la cima del suspenso en los últimos 15 minutos. 

Como producción americana no podemos esperar algo distinto de la familia americana en medio de un país asiático "del cuarto mundo", como lo llaman en la propia película. Asi que quienes han tildado de racista este filme no les falta motivos. Quizá el guión pretendía abordar los efectos de las inversiones corporativas en países en vías de desarrollo, pero si ese era el objetivo el resultado en este punto no fue muy satisfactorio. 

Creo que en todo caso, omitiendo el detalle de la nacionalidad de las víctimas, esta película llega en un momento oportuno para pensar el drama de los refugiados y de quienes huyen de la guerra.
        
            