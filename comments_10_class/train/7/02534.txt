
                      Yo cuando vi la precuela de esta película, es decir Ocho apellidos vascos, a mí me encantó. 1 años más tarde, como era de esperar con la cantidad de dinero que generó Ocho apellidos vascos, se estrenó su secuela Ocho apellidos catalanes.
Ocho apellidos catalanes trata de que Amaia y Rafa tienen una crisis y deciden separarse, Rafa se queda en Sevilla y por el contrario Amaia decide irse de vacaciones a Girona, allí conoce a Pau, un artista hipster 
nieto de una independentista. Poco a poco se enamoran y deciden casarse.
Cuando Koldo (el padre de Amaia) se entera de que se van a casar decide ir a Sevilla a buscar a Rafa para intentar recuperar a Amaia. Esta es básicamente la sipnosis de la película.
La película es bastante divertida, pero lamentablemente crea algunos hoyos argumentales considerables y además el trozo final de la película se hace un poco pesado...
Los actores Dani Rovira y Karra Elejalde están fantásticos como en la anterior película.
El personaje de Judit (interpretado por Belén Cuesta) para mí opinión es innecesario.
En resumen, Ocho apellidos catalanes es la película perfecta para ver en familia y pasar un buen rato riéndose estas navidades.
Lo bueno: Dani Rovira y Karra Elejalde.
Lo malo: Belén Cuesta y los hoyos argumentales
        
            