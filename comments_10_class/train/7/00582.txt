
                      Entretenidísmo film de superhéroes, con unos personajes que resultan (al igual que en la primera entrega) bastante carismáticos y divertidos, unos efectos especiales conseguidos y unos diálogos que la acercan más al humor de Deadpool que a la saga de Vengadores, a la que pertenecen. Marvel ha dado en el clavo - una vez más - en este macrouniverso que van enlazando paso a paso
        
            