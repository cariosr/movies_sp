
                      En relación a esta película, tengo que decir que fui gratamente sorprendida, porque introducen al espectador en una maraña de información que cada detalle cobra sentido una vez se ilumina la bombilla y porque me gustan las películas con un principio, desenlace y final, sin cabos sueltos.
Michelle Williams tiene una mirada de profunda candidez con un punto de tristeza pero con un trasfondo de absoluta decisión, como si la cosa no fuera con ella...
Mark Ruffalo es un melancólico policía enviado a una isla donde pasan cosas extrañas despidiendo ese halo de hombre gris con traje gris en una ciudad gris, y en ese halo también va incluido el personaje de Leonardo DiCaprio; en un momento dado, uno hasta se imagina dónde colgaría su gabardina gris, en el gancho de la izquierda o el de la derecha de una puerta semidesvencijada.
A todo esto, el tiempo no ayuda,apareciendo como otro personaje imprescindible para imprimir una pátina desangelada a estos personajes tan claramente definidos en un mundo en el que nada es lo que parece.
Y finalmente Ben Kingsley, enigmático por su porte, por su mirada o por esa medio escondida arrogancia de ser el verdadero cocinero de esta salsa.
        
            