
                      Con un Dani Rovira y un Karra Elejalde muy creíbles, película entrañable que nos cuenta una historia de enfermedad y superación gracias al deporte, basada en hechos reales. Entretenida, simpática, conmovedora... Recomendable.
        
            