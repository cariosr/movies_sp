
                      Una nueva misión de Tom Cruise es alcanzar y lograr que nos lo pasemos bien, entretengamos y también nos sigamos divirtiendo. Así volvemos a ver acción a raudales, y algo tan habitual como las carreras en las  calles y carreteras sin olvidar los momentos frenéticos y apasionantes... todo ello acompañado de la ópera de Puccini. Con películas así habrá nuevas misiones posibles.
        
            