
                      The Prodigy (2019).
“A veces, cuando abandono mi cuerpo, pasan cosas malas."
'El Prodigio’... o quizá podría llamarse 'El Escalofrío’, por lo que te recorre el cuerpo durante gran parte de la hora y media de metraje de esta estupenda cinta de terror donde el crío primera víctima de la magistral “It” (versión 2017) hace aquí las veces de la maldad transformada en niño. 

La sobriedad del rodaje (donde el director -acostumbrado al género- nos hace estremecernos a base de inquietantes planos), la buena música sin exceso de sobresaltos innecesarios (también compuesta por un habitual del cine de terror), una digna fotografía y una buena interpretación por parte del “inocente” infante, nos hacen pegarnos a la butaca hasta la aparición de los títulos de crédito.

“Mamá, ¿seguro que vas a quererme siempre, haga las cosas que haga?"
Una de las antiguas, aunque efectivas fórmulas del cine de miedo, es el uso de niños encarnando el mal de uno u otro modo; y en esta ocasión funciona. Jackson Robert Scott, el joven actor que se pusiera en la piel, como antes apuntaba, del primer sacrificio del payaso Pennywise en “It” es aquí nuestro perturbador protagonista. En la línea de la célebre “Damien”, “The Prodigy” nos acerca a la malicia y la vileza extremas donde seremos testigos del sufrimiento y frustración de unos padres ante el terrible comportamiento de un hijo de 8 años tan inteligente como cruel.

No os la perdáis. Merece la pena a pesar de la mediocre crítica recibida.

Raúl Cabral.
https://www.filmaffinity.com/es/film235862.html
        
            