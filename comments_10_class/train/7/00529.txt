
                      "El Sabe Si Has Sido Un Niño Bueno o Malo"

Es casi global el saber que San Nicolas es aquel que da felicidad a todos los niños del mundo. Santa Claus es sinónimo de Navidad y la Temporada Navideña es solo amor y calor hasta cierto punto. ¿Alguien debe encargarse de los niños malos? ¿No?

Sin duda esta película marca huella, no solo por su excéntrico argumento, marca huella por su fecha de lanzamiento (Navidad). Es algo totalmente diferente e innovador. 

Tom (Adam Scott) es el típico padre que solo tiene tiempo para el trabajo, el tiene que pasar algunos días atrapados con la familia de su esposa. Sarah (Toni Collette) es una apática madre que quiere mantener forzosamente el espíritu de la navidad en su quebrantada familia. Su perfeccionismo y poca tolerancia la llevara a fortalecer sus lazos familiares. Bet (Stefania LaVie Owen) es la típica adolescente problema de Holywood (personaje cliche) y Max (Emjay Anthony) un pequeño niño que aun cree en la Navidad, Galletas y Santa Claus que defraudado de su familia decide olvidar sus creencias para abrir paso al mal de navidad. Otro personaje cliche es el tío Howard (David Koechner) que solo le interesa criticar, arruinar la cena de Navidad, presumir sus armas y ser presa fácil del monstruo. Su esposa (Conchata Ferrell) curiosamente no es la tía cliche, es una sorpresa en la película, y sus dos odiosas hijas que solo sirven de relleno.

El desarrollo de la historia es bueno pero característico de una película de horror, Paso Uno: Odiamos la Navidad, Paso Dos: Una tormenta infernal se desmantelada momentáneamente, Paso Tres: Por extrañas circunstancias, la chica mala sale y recibe su merecido, Paso Cuatro: Desde este punto, la fracturada familia es forzada a unirse con el fin de defenderse de un asedio prolongado de tales criaturas repugnantes como muñecos demoníacos, arlequines infernales y elfos malvados todos estos solo para la apertura del mismo infierno "Krampus". Paso Cinco: Familia asustada, Familia Muerta. Paso Seis: Lección aprendida.  

Michael Dougherty se ha convertido en un maestro del horror, teniendo a sus espaldas al clásico de culto "Trick 'r Treat." Como la anterior, Michael honra  a clásicos como "Tales from the Crypt"y "Creepshow" en su formato de antología. "Gremlins" de Joe Dante sirve como referencia para hallar la mezcla perfecta entre humor subversivo, horror y el sentimiento genuinamente ganado, para que sea una de las favoritas en la lista de entretenimiento navideño. "Krampus" intenta trabajar en las mismas lineas pero nunca llega a la cima. Aunque,la apertura es bastante original (recreando una comedia navideña) divertida y eficaz con la introducción de piezas de una buena película de terror, por desgracia, la decision de clasificarla PG - 13 ( a pesar que los padres en sano juicio subestimen el contenido de esta) nunca puede lograr lo que quiere ser, dado que habría funcionado mucho mejor clasificandola R y aun mejor hubiera funcionado si la primera parte no se hubiera demorado tanto en ponerse en marcha.

"Krampus" tiene aun mas, es una cajita llena de sorpresas, y el punto mas notable es que Michael utiliza a la perfección la mezcla de humor - horror no volviendo su película en un típico film navideño, cayendo a lo cursi y estúpido. También plantean notables actuaciones serenas Scott, Collette y hasta Koechner. La madre de Tom es una palabra es "Espectacular" retratando su pasado animadamente, aterrorizandonos con su simple silencio. Los temibles demonios son verdaderamente horribles, nos muestra algo no visto hace mucho tiempo, realmente estos monstruos dan miedo (sobre todo cuando están en el circulo alabando a su rey "Krampus" ¡Que escena!)  El agridulce mas grande es su forzado y obligado final que solo nos confunde y por desgracia cae en el detestable cliche. Lastima "Krampus" tenia talento.

Sin lugar a dudas "Krampus" es la mejor elección para esta fecha, superando a "Love The Coopers" o "Home Alone". Este film no llega a ser un memorable clásico pero logra refrescar el genero este año. Fue extraña la decision que tomo Universal Studios de no dar conferencias de prensa sino solo lanzarla en cines subestimando su calidad, tan solo por eso le dieron una recomendación bastante regular.

¡Oh No! ¡Perdón!  No era mi intención criticar tu trabajo, no soy estúpido... Sere un niño bueno el año que viene.

"Krampus" es atrevida, no teme en jugar con lo que tiene, entretenida, pero no logra asustar a los demás clásicos navideños.

¡Horrorosa Navidad!
        
            