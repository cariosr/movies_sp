
                      Segundo origen es de esos largometrajes que te sorprenden, porque a pesar de ser una película pequeña con una trama tan difícil de sacar adelante como lo es el fin de la humanidad, consigue que la historia avance a través de los sentimientos y emociones de unos modernos Adán y Eva representados por dos jóvenes que enganchan desde el principio.
        
            