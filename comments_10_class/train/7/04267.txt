
                      Jumanji (2017).
Vuelven las aventuras del famoso juego que ya protagonizara el malogrado Robbin Williams, ahora dirigido por el hijo del gran Lawrence Kasdan (guionista de obras como El Imperio Contraataca y director de peliculones como Silverado; y me refiero al padre...).

Es ahora "La Roca" quien encabeza el reparto, y junto al actor mejor pagado de Hollywood, están cómicos como Kevin Hart o Jack Black. Y es que en esta ocasión el guión tiene más golpes cómicos (aunque no desternillantes) que su antecesora, y por supuesto, muchos momentos de acción, y no solo por encontrarse Dwayne Johnson en el reparto.

El largometraje está rodado básicamente en Hawaii, que se convierte en un personaje más donde el responsable de fotografía no habrá tenido mucho esfuerzo que hacer para lucirse gracias a los asombrosos paisajes que se ven. Solo por esto merece la pena ver la película.

Muy entretenida a pesar de rozar las dos horas de metraje.

Raúl Cabral.
https://www.filmaffinity.com/es/film740753.html
        
            