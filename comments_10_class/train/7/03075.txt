
                      Bajo la capa superficial de un survival de aventuras, Infierno Blanco es un entretenimiento con un toque existencial que la hace mucho más original que la media de este tipo de películas. Si añadimos a un Liam Neeson clavando el retrato de persona atormentada por su pasado y sin ganas de vivir, estamos ante un film diferente , estéticamente llamativo, muy entretenido y tenso y a la vez algo más.
        
            