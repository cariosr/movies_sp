
                      Big Hero 6 es una película de animación dirigida y escrita por Don Hall. Está basada en los cómics de Marvel, creados por Steven T. Seagle y Duncan Rouleau.

Hiro Hamada es un niño prodigio de 14 años que se graduó en el instituto a los 13. Solo está interesado en las peleas de robots ilegales, donde gana mucho dinero.
Su hermano mayor, Tadashi, le convence para que entre a la universidad en la que él estudia para sacarle partido a su inteligencia.
Una serie de accidentes hacen que Hiro y sus amigos formen un equipo de supehéroes usando toda la tecnología de la que disponen para salvar la ciudad de San Fransokyo.

Marvel y Disney se juntan para hacer esta película, que como se esperaba, es un éxito. La combinación de batallas y momentos; tanto tiernos como graciosos, crean una historia de la que pueden disfrutar igual mayores que pequeños.

El protagonista indiscutible es Baymax, un robot enfermero de aspecto adorable creado por Tadashi, que consigue enamorar a cualquiera.
De entre los demás amigos de Hiro, el que más destaca es Fred, un amante de los cómics, despistado pero ingenioso.

La película ha recibido once nominaciones en distintas categorías y premios, uno de ellos a los Globos de Oro.
        
            