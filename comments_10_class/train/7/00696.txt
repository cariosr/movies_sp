
                      Ni perfecta ni desastrosa, una buena peli en general, entretenida, alternativa y disfrutable siempre y cuando "te vaya el rollo pirado".

Ojo, posibles spoilers, sutiles eso sí, ninguno explícito.

Primero hay que comentar un par de cosillas que objetivamente han falladado en esta película:

1-Se han notado cortes, claro y evidentes. No se quien habrá sido el responsable de esos cortes, pero hay un corte en concreto que es una cagada y que generará la única "incoherencia" de la película. Pero como mínimo, dos cortes han habido seguro (uno de ellos es una escena que sale en el trailer), y no descartaria que hubiese más. 

2- Se ha llegado a abusar un poco de algunos clichés clásicos del cine.

3- el CGI del malo, algo regulero.

Luego, resaltaría un par de cosas que no tienen porque ser negativas, pero bajo mi punto de vista lo han sido:

1-Le ha faltado gore y clasificacción R para ser el escuadrón suicida de verdad. Eso es así, esta película con un par de escenas gore hubiese sido de alto notable.

2-Para mi hay dos personajes en la película que se han salvado de forma innecesaria. Se agradecen ciertas muertes, pero cojones, ¡es el escuadrón tienen que caer como moscas! No pero ahora en serio, hay un personaje que se ha salvado simplemente por que... es ese personaje. 

3-A Deadshoot le han cambiado su peculiar sentido del humor por otro peculiar sentido del humor más...más... más Will Smith, hay que decirlo, aunque ya me esperaba algo así.

4- ¡¡No... Waylon Jones no puede ser el estereotipo de un afroamericano!!

Y ahora, las cosas buenas:

1- La história es simple, clara y directa pero funciona. Creo que siendo justos nadie puede decir que no es lo que se anunciaba porque en si, los cómics del escuadrón suicida són eso, y el trailer muestra el concepto principal, guardándose obviamente el verdadero plan y los mejores chistes. En si, como historia en los cómics no es nada nuevo, pero como historia en los cines si, se trata de algo atrevido, alternativo y un soplo de aire fresco a un género que lo necesita. Ese es el punto fuerte de Warner para con estas pelis, la alternatividad. Tienen mi respeto. 

2- Buen detalle una de las muertes, muy al estilo de los cómics. Mejor detalle todavía otra de sus muertes la cual pocos se habian imaginado. Ahora si que podemos decir que warner tiene lo que hay que tener para matar (aunque ojalá hubiese matado a más).

3- Muchas referecias que solo pocos vamos a pillar, pero que los que las pillen lo van a flipar. 

4-Buen humor.

5-Buena y justa presentación de los personajes, ni demasiado larga ni demasiado corta, la justa para dar a conocer a los verdaderamente importantes en la trama y la justa para no malgastar demasiado tiempo de la cinta con ellas.

6- Gran escena a mitad de los créditos, revela un par de cosillas muy interesantes.
        
            