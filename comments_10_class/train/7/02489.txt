
                      Uno de los conceptos más inteligentes es cómo juega con el enigma central de Pixar: ¿Cómo se conectan todos estos animales o juguetes o robots, seres que, para los humanos, no tienen emociones o pensamientos o incluso caras, con el mundo humano que caminar alrededor?
        
            