
                      Una buena idea, con un buen e interesante inicio, pero que se queda en lo más superficial de algo a lo que se le podría sacar más partido. Una buena película de acción recomendable al 100%, pero con la sensación de que lo que podría haber sido una gran película acaba siendo una historieta demasiado plana con la que pasar la tarde.
        
            