
                      El cierre a la saga de Harry Potter reúne las cualidades de las que la primera parte de las Reliquias de la Muerte carecía: Ritmo apropiado, un desarrollo interesante y sin rodeos sin sentido, y un desenlace a la altura de las aventuras del protagonista, poniendo el broche final. La ambientación está a la altura (aunque este aspecto es una constante en la saga) y viéndola, hace pensar que quizás se podía haber condensado todo el capítulo de las Reliquias en un solo film
        
            