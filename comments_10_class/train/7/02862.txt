
                      El regreso por quinta vez de Jack Sparrow no es más que una confirmación de que, continúe o no la saga, 'Piratas del Caribe' va a permanecer como uno de los mejores entretenimientos de su década. 'La venganza de Salazar' no hace demasiados esfuerzos por distinguirse de sus antecesoras, es más, recurre al simbolismo de sus ya conocidos personajes y trata de ampliar su universo. Lo mejor, Depp y Bardem; lo peor, que parezca un final definitivo.
        
            