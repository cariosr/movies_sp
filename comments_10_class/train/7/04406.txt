
                      Esta película es una de las mejores, sino la mejor, del director Vicente Aranda, contando un drama muy fuerte que queda retratado de forma magistral por el director con un guión muy bien adaptado, en parte gracias a las buenas interpretaciones del elenco principal, Victoria Abril, Jorge Sanz y Maribel Verdú. Es una de las grandes obras que nos ha dado el cine español.
        
            