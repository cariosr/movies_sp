
                      Capitán América: El Soldado de Invierno, es la segunda parte de la historia de este superhéroe creado por Joe Simon y Jack Kirby, editores de Marvel Comics.

La acción empieza cuando Steve Rogers descubre que hay un topo en la agencia S.H.I.E.L.D.
Sin salirse de su patrón, Marvel ofrece una historia llena de acción y sorpresas.

Los efectos especiales muy bien conseguidos, hacen que uno se meta en situación.
Además, la película cuenta con un repertorio de actores elegido de manera muy acertada, con Chris Evans, Scarlett Johansson y Samuel L. Jackson como personajes principales.

Esta acaba con cierta intriga, dejando al espectador a la espera de la tercera entrega de la trilogía creada por Marvel.

Recomendable para los amantes del cómic, los superhéroes y la ciencia ficción.
        
            