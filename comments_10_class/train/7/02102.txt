
                      Está
 a un nivel por encima de las producciones habituales. Y, quitando aparte las molestas lecciones de mística, un tanto forzadas, lo extraordinario de esta película y lo que le da pleno sentido a la historia mágica de un tigre en una barca en medio de un océano enigmático es el contraste visual realmente brutal entre lo que se nos ha contado y la versión realista que el muchacho relata a los periodistas japoneses una vez en el hospital. Ang Lee es un contador de historias experto y la prueba de ello es que un argumento tan arriesgado habría hecho caer en el ridículo a más de un director.
        
            