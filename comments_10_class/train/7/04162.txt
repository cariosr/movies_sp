
                      'El Asedio de Jadotville' no es otra pelìcula "belica" convencional y pro-yankee... trata sobre la valentìa de 150 soldados irlandeses abandonados en pleno Àfrica enfrentandose a cientos y cientos de hombres armados. 
Aquì, Jamie Dorman deja en claro que tiene mucho talento para la actuaciòn saliendo de su papel màs conocido en '50 Sombras de Grey' (aunquè tambièn lo demuestra en 'Operaciòn Anthropoid').
Basado en hechos reales, està cinta nos deja con una sensaciòn de impunidad y 'odio' a los altos cargos de las Naciones Unidas y a los generales de èste grupo de soldados.
        
            