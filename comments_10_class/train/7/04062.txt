
                      J
eff Nichols firma una obra sobre la incomunicación en una sociedad abocada al desastre, un ensayo sobre la lógica del caos desde los rincones de una mente en el abismo. “Take shelter” es brillante, con una insultante capacidad para perturbar.
        
            