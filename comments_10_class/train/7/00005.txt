
                      La película es una película para niños, quiere decir que no se le puede pedir mucho más. A mis niños les encanto, se rieron y sobretodo aguantaron hasta el final. Está claro que para mí no será una de mis películas para volver a ver. Pero si necesitas estar entreteniendo a los peques es una buena apuesta.
        
            