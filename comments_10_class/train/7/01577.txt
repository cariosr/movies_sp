
                      Madre! (2017).
La mayor paranoia de Darren Aronofsky, el responsable de Cisne Negro y Noé, la protagonizan la actriz mejor pagada de Hollywood y su compañero sentimental en la ficción, Javier Bardem.

Una auténtica locura surrealista con una interesante fotografía y unos estupendos efectos visuales y sonoros que, junto a la increíble casa que habitan nuestros protagonistas, nos cuentan una historia que dice mucho más de lo que parece a primera vista.
Como diría Joaquín Reyes, tras verla nos quedamos con el "culo torcío", y es que el desarrollo de la película nos sorprende conforme va avanzando. Como podemos leer en la crítica de Fotogramas. Madre! puede verse desde distintos ángulos, pero lo que no se nos escapa es que la crítica de Aronofsky es feroz. 

A caballo entre El Angel Exterminador de Buñuel y La Semilla del Diablo de Polanski, Madre! nos mantiene en vilo gran parte de sus dos horas de metraje, y sobre todo, nos saca de nuestras casillas con la situación que Jennifer Lawrence vive a partir del segundo acto (particularmente no hay cosa que me enferme más que el allanamiento de morada tanto física [hogar] como virtual [redes sociales]).
Abusando quizá de los primeros planos de esta bella actriz y de numerosos planos subjetivos, el realizador nos introduce en su estresada y desagradable experiencia.

Completan el reparto Ed Harris y la casi sexagenaria Michelle Pfeiffer, quien nos hace odiarla con casi la misma fuerza que Lawrence (señal de su buena interpretación). Los hermanos Gleeson hacen también de hermanos en la ficción (dos auténticos Caín y Abel).

Mi impresión: no dejará indiferente a nadie. O se odia o se ama, o bien te deja reflexionando un buen rato (es mi caso). Sugiero leer la crítica/explicación que antes apuntaba, siempre después de ver el filme. 

Raúl Cabral.
        
            