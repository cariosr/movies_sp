
                      Siendo bigfoots, osos letales, o simplemente los monstruos de toda la vida. El cine siempre nos ha dado muy buenos momentos, haciéndoselo pasar muy mal a pequeños pueblos americanos rodeados de árboles. Desde “El bosque” a “Wayward Pines” pasando por “Sleepy Hollow” o “Silent Hill“. Ahora, nos llega una nueva y genial obra de suspense donde de nuevo lo mejor es encerrarse en las casas una vez que la noche cae en el pueblo, y en los oscuros bosques de alrededor.

Kevin Durand (el tipo duro de “The strain“) protagoniza un título sobre un peligroso animal que esta causando estragos en un pueblo después de que unas maquinas talando árboles, invadieran su hábitat.

Con un halo de misterio que recuerda a “Mothman: la última profecía“, “Dark was The night” recupera ese aire de película de suspense sobre criaturas que aterrorizan en los bosques que rodean los pueblos. La cinta, con una trama simple, consigue no dejar a los personajes de lado, muy frecuente en este tipo de películas, y tiene tiempo de indagar en sus propias vidas. Vidas que se verán truncadas por el fatídico monstruo que mostrando muy poco él, hace que le temamos a base de oscuridad y figuras escalofriantes, que es precisamente lo que más nos gusta. Aunque algo lenta en sus primeros 30 minutos,  mediante una atmósfera oscura, atrapa, entretiene y asusta.

Lo mejor: la escena final.
Lo peor: que logremos ver al monstruo.
        
            