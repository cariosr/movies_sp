
                      Brutal. Nada es lo que parece en esta película. Comienzas la película creyendo una cosa, pero finalmente te das cuenta de que nada era así. No es la típica película de miedo. Suspense y misterio. Personalmente, para mí ha sido un quebradero de cabeza esta película. Recomendable totalmente para la gente que disfrute con este tipo de películas.
        
            