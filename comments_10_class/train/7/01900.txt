
                      En su primer largometraje, Daniel Sánchez Arévalo, nos muestra una historia muy arriesgada pero gracias a lo bien que la dirije posiblemente sea su mejor película. El guión es muy bueno. La banda sonora de Pascal Gaigne acompaña la historia de manera magistral. Las interpretaciones de Quim Gutiérrez, Marta Etura, Antonio de la Torre y Raúl Arévalo son estupendas.
        
            