
                      Debo admitir que estoy gratamente sorprendido después de ver Jack Reacher. Sinceramente me esperaba una película de acción de las que hacen últimamente, con peleas, tiros, explosiones y viguerías varias, quizás debido a tener muy interiorizado a Tom Cruise en el papel de Ethan Hunt en Misión Imposible. Pero no fue así. 

Dirigida por Christopher Mcquarrie y protagonizada por Tom Cruise y Rosamund Pike, Jack Reacher resulta ser un efectivo thriler policiaco muy al estilo de los 80 bastante bien llevado, bien dirigido, con un tipo duro como protagonista, con dosis de intriga, con peleas muy realistas, sin patadas voladoras ni tíos volando por los aires. Las técnicas o artes marciales utilizadas son muy militares y policiales para inmovilizar o reducir rápidamente al contrario, tanto las que son con los puños como las que son con armas de fuego. Las persecuciones con coches también están muy bien rodadas con un sonido abrumador de los motores revolucionados. Los tiroteos también resultan muy realistas ya que los efectos de sonido están muy bien logrados, secos y contundentes. Hay una escena corta de una pelea en un baño muy bien rodada que muestra realmente que no es fácil pelear entre varios en espacios reducidos ya que lo normal es molestarse unos a otros.

En la película no se usan efectos digitales lo cual acentúa más su realismo y nos devuelve a esos thrilers antiguos que se basaban en un guión y no en un recital de efectos visuales, despropósitos varios y peleas gratuitas.

Este personaje está sacado de las novelas del autor Lee Child, que se basan en un exoficial de la policía militar norteamericana que, después de dejar el ejército, decide comenzar una vida de vagabundo a lo largo de E.E.U.U. Son unas 16 novelas sobre este personaje, así que nos podemos preparar para el inicio de una nueva franquicia para la gran pantalla

No estoy encumbrando a Jack Reacher como la mejor peli del año, ni mucho menos, pero si que es una efectiva película de intriga y acción que merece ser vista.
        
            