
                      Un buen thriller del estilo "Jack, el destripador". Todo el film se centra en la investigación de un detective que va atando cabos para descubrir al asesino serial que acecha a la ciudad de Limehouse cuyo seudónimo es "Golem".
Gran ambientación de época (siglo XIX), notables actuaciones y un acertado guión que demanda concentración para seguir el hilo argumental de la investigación. Solo cuestionaría el giro del desenlace que me resultó algo predecible.
La recomiendo
        
            