
                      Interesante planteamiento sobre un futuro distópico en el que se abre la veda del crimen durante una noche al año, lo que da rienda suelta a venganzas, odios, etc. Todo resulta muy yankee, y a su vez, la idea funciona como un tiro (nunca mejor dicho): Tensión y terror a partes iguales, violencia extrema y un reparto encabezado por Ethan Hawke, para sobrevivir a una sola noche. ¿Qué puede salir mal? :)
        
            