
                      Ves los horarios en casa, vas al cine, pagas la entrada y en todo momento sabes a lo que te vas a enfrentar cuando vas a ver esta película. Acción, acción y acción. La película roza la comedia sobretodo por el constante aparecer de míticos personajes. Al nivel de la primera pero todavía van más al grano, desde el primer minuto tienes acción, argumento fácil y diálogos escuetos.

Mi puntuación es alta ya que ofrece lo que esperas, ni más ni menos.
Si quieres pasar un rato divertido, te invito a verla.
        
            