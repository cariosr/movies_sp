
                      Cuarta entrega del la serie de películas del Departamento Q, sección de investigación policial ante casos imposibles. La presente entrega mantiene los mismos objetivos de las otras tres por lo que sigue siendo entretenida, sorprendente y bien llevada. De lo mejor que se puede ver sobre este género policial de los últimos años.
        
            