
                      "Es la soledad, la razón que lleva a sus protagonistas a un destino desolador que durante su trayecto desestructurado no cala en igualdad de emociones con las demás peliculas de su director. Aún así, se saca de ella mucho jugo actoral y narrativo"
        
            