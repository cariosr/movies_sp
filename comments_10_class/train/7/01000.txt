
                      Creo que siempre es difícil que la segunda película iguale o supere a la primera pero el hecho es que esta película la ha igualado (a su manera).Esta  continuación de la anterior película sigue manteniendo una historia solida, unos buenos actores y esa chispa de fantasía que caracterizan a estas películas. Le doy un notable, gran película.
        
            