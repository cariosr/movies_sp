
                      Desde la primera película del 007 hasta "Spectre" de San Mendes se ha impregnado su sello original, pero esto no quiere decir que las peliculas de James Bond son solamente originalidad. Era sólo cuestión de tiempo para que los productores comenzaran a realizar imtaciones de franquicias exitosas (Batman). Las ultimas tres últimas películas de James Bond se basan en estas franquicias llenas de clichés, explosiones, amor, el bien y el mal, chicas Bond y más relleno.

'Spectre' esta llena completamente de trucos originales y explosiones, trenes, aviones, helicópteros, México, el día de los muertos, las chicas Bond, secretos, salvavidas, bombas, bromas, escenarios excéntricos y el descubrimiento del autor de todo el sufrimiento del agente 007 .

Si te gusto "Skyfall" y todo el resto de la lista, te encanta el juego de decisiones y conclusiones de 'Spectre'. Los antagonistas de esta historia es el familiar Franz Obenhauser que regresa para terminar destruyendo la poca vida que tiene 007 (Recuerda que él es el autor de todo su sufrimiento) .Su desarrollo en la película es impresionante, y nos puede mostrar su verdadero odio contra su medio hermano (incluso con una cicatriz catastrófica en el ojo

Sí, indudablemente, 'Spectre' fue una gran película con escenas llenas de drama y acción, pero poco a poco se disminuye el nivel del film, evaporando nuestras mentes como 'Skyfall'.

O tal vez el guión es el verdadero problema,  la historia es bastante ilógica y suave logrando confundir a los espectadores. Las explosiones, persecuciones y tiroteos no arreglaran una mala historia.

Cualquiera sea la causa, "Spectre" es una película en la que los medios de comunicación muestra una cara completamente diferente a lo que realmente es (Al igual que "Terminator: Génesis"). Hoy en día, los medios de comunicación abusan de su autoridad, juegan con la mente de los espectadores con un propósito de captar la atención a través de anuncios de televisión, Teaser, Trailers, Clips, Featurette y más publicidad.

Incluso a simple vista, el Tráiler de "Spectre" hace promesas que no puede cumplir. El toque oscuro que le dio Sam y Vals es aterrador, tratándonos de dar la primera película de horror sobre Bond.

Es casi innecesario mencionar que uno de los puntos fuertes de la película es el fabuloso tema musical de Sam Smith "Writing's On The Wall", que se introdujo en la película al comenzar. Personalmente, presentaron la canción en la película de una manera bastante extraña (Una breve explicación acerca de la organización 'Spectre'). Fue bastante peculiar.

El personaje de Moneypenny (Naomie Harris) no es muy importante en la película, pero ella es la ayuda de Bond, es su función, ¿no ?. Las Chicas Bond (Monica Bellucci - Léa Seydoux) hicieron sus actuaciones por lo alto, pero definitivamente y, obviamente, la cara de la película es para Bond. Realmente, no sé mucho acerca de las viejas películas de Bond, pero Daniel Craig 'Spectre' ha hecho una caracterización fascinante y sin duda es uno de los mejores James Bond por su elegancia, estilo, elegancia, alegría, humor, trabajo duro y todo lo que tiene un gran agente 007,

Pero, por supuesto 'Spectre' se queda corta de tratar de imitar el clásico de Nolan. Esta película no fue del todo una oportunidad perdida, pero en realidad no hizo uso de todos los privilegios que tiene una película de Bond . 'Spectre' tiene belleza, clase y todo el peso de sus predecesores (más que cualquiera 'Skyfall '). James Bond está de vuelta por veinte cuarta vez, no de la mejor manera, pero él vuelve, y sí:

- "Él es Bond, Él es James Bond"
        
            