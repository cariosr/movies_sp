
                      En esta (bonita) película no pasa (casi) nada.... Y sin embargo le esencial está en ella.
Una bonita historia de amor en una familia de privilegiados. Privilegiados social, cultural e intelectual mente, para su tiempo y para la sociedad de la época… Una familia políglota, con un padre investigador de arte grecorromano, que pasa sus vacaciones de verano en una villa de Lombardía haciendo investigación y que invita a un estudiante de arte que termina por dejarse seducir el hijo, muy avanzado intelectualmente (lector impenitente y músico vanguardista). Hasta ahí el argumento, el resto es la película en su esencia.
Hueca de acción, pero llena de emociones y de sensaciones entre las que uno de los puntos álgidos más emotivos es sin duda cuando el padre de Elio (extraordinario descubrimiento: Timotée Chalamet) se sincera a su hijo hacia el final de la película.
Como en muchas de las grandes películas, sales del cine con una extraña sensación, mezcla de tristeza y de alegría, de vacío y de plenitud, de dulzura y de amargor… en la que muchos habrían querido identificarse, incluso uno se siente muy lejos de ese medio...
        
            