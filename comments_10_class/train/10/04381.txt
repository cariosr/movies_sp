
                      En mi opinión como espectador,Iron Man no solo fue el comienzo perfecto para lo que se conoce hoy como el U.C.M (universo cinematográfico de Marvel) sinó que también la considero una obra maestra en el género de súper héroes/acción,el cual remasterizó de la mejor manera posible,con un increíble elenco estelarizado por el talentoso Robert Downey Jr como protagonista.Además de increíbles efectos especiales,trama súper entretenida de inicio a a fin,creo que uno de los secretos del éxito de esta película y de sus respectivas secuelas es Robert Downey Jr.el actor no solo te compra tanto como Iron Man y como tony stark con su característica carisma,sinó que bajo mi punto de vista el es uno de los actores que nacen para interpretar un específico personaje.Siguiendo con el elenco,Wyneth Paltrow Pepper Potts,Jeff Bridges como el villano Obadahia Stane y Terrence Howard como el Coronel Rhodes hicieron un gran trabajo con sus personajes.En fin,la primer película del hombre de hierro fue el mejor comienzo para el personaje y de lo más recomendable dentro del género de súper héroes,100% recomendable que te hará pasar un entretenidísimo rato viendo acción y más acción.

Puntos a favor:Robert Downey Jr,efectos especiales,escenas de acción,trama fresca y entretenida.

Puntos en contra:Obadahia Stane,(Jeff Bridges)no llega a ser un villano tan memorable como en otras películas del género.

Mi calificación:10/10
        
            