
                      Es una obra maestra, una película que habla de una simple vida. Enseña muchísimas cosas y la actuación de Tom Hanks es increíble. Un Oscar más que merecido.
        
            