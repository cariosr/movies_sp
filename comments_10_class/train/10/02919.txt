
                      Espectacular! Me ha gustado mucho.
Creo que es una adaptación muy buena del libro, aunque quizás le falta un poco de dureza... lo he pasado peor recordando momentos que había leído que por lo que me mostraba la película, ya que el libro te mete en la historia sufriéndola mientras lo lees. Aún así creo que pocas adaptaciones se ciñen tanto al libro como este.

Los efectos especiales dan un millón de vueltas a la primera parte. Me ha sorprendido muy gratamente como han representado la "arena" en la película.

Me he quedado con ganas de más...
        
            