
                      Es mi película favorita sobre todas las demás, me hace apelar a mi ignorancia sobre todo el mal que recae en este mundo, para apreciar lo bello que es. 
De todos modos estamos aquí solo una vez, un viaje solo de ida, mejor aprovecharla con lo bueno que existe, y no con lo contrario. 

-Búho
        
            