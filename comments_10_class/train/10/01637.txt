
                      Una aventura mágica que te deja absorto durante casi 2 horas. Impresionante es poco para esta peli.

Todo empieza con unos monjes que buscaban algo en una biblioteca de monjes y acaban decapitando al bibliotecario, luego se enfrentan a un gran monje y el malo malisismo que es el de Hannibal se escapa. Luego vemos al Benedict haciendo de cirujano, hasta que tiene un accidente y tanto el como sus manos acaban echas polvo, luego buscara ayuda que no le resolverá su problema hasta que le hablan de un paciendo que se curo milagrosamente.
Encuentra al paciente y este le habla de unos monjes en Katmandu, y para allí se va a buscar a los monjes. Ahí empieza la parte emocionante de la película. Los monjes no son muy normales y tratan con las artes místicas del universo, donde el doctor Strange se ve inmerso para curar su manos, tras el aprendizaje descubre que ahí algo mas en el universo y se ve envuelto en una guerra contra otro mago que domina las artes místicas y quiere liberar a un malo llamado Dormamu.
Una gran película como todas las de Marvel que te deja con ganas de que hagan una segunda parte tan buena como la primera. Yo la recomiendo para ver, ya seas fan o no de la casa Marvel.
        
            