
                      La pelicula la vi como hace 2 semanas pero ha persistido en mi memoria, por lo que me atrevo a criticarla y, es más, a realizar una crítica positiva sobre la misma. 
En realidad no me agradan mucho las películas de accion, esta la vi motivada por el encanto del protagonista, el actor Jake Gyllenhaal. que se ha convertido en uno de mis intérpretes favoritos de los últimos tiempos, además de ser bastante guapo. Se pone más guapo a cada año que pasa. 
Prince of Persia es una cinta digna de volverse a ver ya que las acrobacias de los personajes, la banda sonora y los efectos especiales son realmente increíbles.
Si los espectadores no demasiado exigentes quieren divertirse un rato y quedarse con la boca abierta se la recomiendo.
        
            