
                      Creada para los soñadores. Excelente fotografia, edicion y obviamente musica. La la land es un homenaje a los musicales clasicos de hollywood. Pelicula nostalgica con un final atipico pero satisfactorio. Emma Stone nos entrega una actuacion sutil pero excelente, la cual logra que te identifiques con su personaje, el cual lucha por alcanzar su sueño en la ciudad que se encarga de destruirlos. Sin duda es una de las mejores peliculas del año y seguramente un clasico en el futuro.
        
            