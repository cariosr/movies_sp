
                      Aunque Batman es un ícono estadounidense, se necesitó una producción británica para obtener la mejor adaptación de pantalla del superhéroe del cómic hasta la fecha. Esta última entrega fue dirigida por el londinense Christopher Nolan, cuyo misterio de asesinato, Memento, fue segundo en la lista de las 10 mejores listas de este crítico de 2000.

Nolan no solo filmó la película en los EE. UU., Sino que también contrató a su compatriota Christian Bale para interpretar al personaje principal. De hecho, los actores británicos constituyen la mayor parte del elenco principal y de apoyo, e incluyen a Michael Caine, Gary Oldman, Tom Wilkinson, Linus Roache, Colin McFarlane, además de los irlandeses Liam Neeson, Cillian Murphy, Gerard Murphy y Larry Holden. Morgan Freeman y Katie Holmes son los únicos actores estadounidenses en papeles principales.

Aparte de los acentos ocasionalmente inauténticos, Batman Begins funciona espléndidamente por dos razones: Primero, prescinde de la idea de que los villanos tienen que ser extravagantes, coloridos, sabios superficiales, como la serie de televisión de Batman en los años sesenta. Esta película ilustra lo convincente que puede ser una trama cuando se trabaja con antagonistas descarnados en lugar de con caricaturas del mal.

En segundo lugar, la imagen presenta al millonario Bruce Wayne como un ser humano melancólico y vulnerable, que nos lleva a lo más profundo de su tortura psique para explicar la angustia existencial que condujo a la creación de su alter ego que lucha contra el crimen. Nos enteramos de que, de niño, desarrolló un miedo paralizante a los murciélagos después de estar atrapado en un pozo mientras intentaba recuperar una punta de flecha india para su compañera de juegos Rachel Dawes (Holmes). Estaba más traumatizado cuando sus padres fueron asaltados y asesinados justo delante de él.

Abrumado por su pérdida, Bruce abandonó Gotham City para Oriente, donde estudió artes marciales a los pies de Ducard (Neeson). Bruce Wayne estaba aprendiendo a canalizar su sed de venganza de una manera constructiva que honraría el nombre de la familia Wayne.

Mientras tanto, Gotham City ha degenerado en un pozo de corrupción política y corporativa. Después de completar su entrenamiento como ninja, Bruce jura regresar a casa para erradicar la injusticia y el miedo.
        
            