
                      Sin duda, estamos ante una película que estremece nuestros cinco sentidos y requiere de paciencia para mostrarnos una Odisea en donde la naturaleza permea su salvajidad,belleza,intensidad pero ante todo refleja en lo que una vez fuimos y en lo que eventualmente, nos hemos convertido.

Estamos ante una cinematografía y dirección que sobrepasa barreras, la cuál logra inmiscuirnos ante la templanza y dolor de un personaje, el cuál es interpretado brillantemente por Leonardo Dicaprio, un personaje que se resarce de las cenizas para combatir un sentido de venganza vació que solamente es enaltecido y embellecido por la naturaleza,la cuál refleja un simbolismo de aguante y redención.

Como toda película, tiene sus fallas, como por ejemplo:secuencias un poco innecesarias y además una qu otra cosa ilógica y aún así, es una película que pasará a la historia por reflejar el lugar que poseemos ante la naturaleza y por los salvajes que nos hemos convertido.
        
            