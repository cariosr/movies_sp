
                      Si digo que es una gran película de Woody Allen probablemente me quede corto y si digo que es una de las mejores que ha hecho quizás alguien me lo discuta, pero nadie podrá llevarme la contraria en que Cate Blanchett esta maravillosa y consigue echarse toda la película a las espaldas y mantenernos incómodos, como su personaje, todo el rato. Magnífica, como casi siempre la radiografía social de Allen y como no, el análisis de las relaciones de pareja. Para quitarse el sombrero y disfrutar de un buen rato de cine.
        
            