
                      Con 'District 9' se nos ofrece una cinta de ciencia ficción de las buenas, humana y con la peculiaridad de una película de imagen en tiempo real/documental.
Sorprendente resultado.
Gran interpretacion de su protagonista, el hasta ahora desconocido Sharlto Copley, que logra llegarnos de un modo increible. 
Gran dirección que se implica del modo mas íntimo en la trama. No da la sensación de ser un filme que contara con altos medios y aun asi, con su bajo presupuesto lograron, pienso yo, el mejor filme de ciencia ficción que hemos visto en mucho tiempo y, sin duda alguna, el mejor del 2009. Y no me reconozco una fiel seguidora de este genero. Añadir que escogieron una perfecta banda sonora.
En resumidas cuentas, una muy buena pelicula de ciencia ficción, altamente recomendable.
        
            