
                      Para mi, la película que más impacto tuvo en mi infancia. 
Fue un antes y después en el mundo cinematográfico. Un Techno - Thriller en toda regla y una obra maestra en la historia del cine.

Lo mejor: a pesar de sus 25 años, sigue siendo un impacto visual actualmente.
Lo peor: que no se base al 100% al libro de Michael Crichton, por ponerle alguna pega.
        
            