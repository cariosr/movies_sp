
                      Es una película espectacular, antes se verla me dije a mi mismo que me aburriría por el hecho de ser muda, ahora me trago mis palabras, por otro lado no le daría el Óscar a la mejor película por el mismo hecho de que hoy en día una película ha de contener todos los elementos para ser merecedora de dicho reconocimiento. Ha recibido un Óscar a la nostalgia. Pero no deja de ser magnífica. La interpretación de Dujardin es exquisita.
        
            