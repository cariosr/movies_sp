
                      La La Land, venía precedida de muchos comentarios, respaldos y buenas críticas, y sorprende ver que efectivamente, era todo más que merecido, por lo que la ha convertido en la película musical del momento y ganadora de un culto que se mantendrá en el tiempo, junto a sus 7 Globos de Oro, que la convierte en la película con más número de ellos de la historia, siendo el musical un género que no triunfa especialmente, solo nos queda ver su paso por los Óscar, pero ya va muy bien respaldada para los mismos.

Y que es lo que tiene este musical para cautivar al público? Pues fundamentalmente 3 pilares básicos:  su referencia al cine musical clásico americano, la música y su historia; parece que no es nada especial, pero seguidamente desglosare cada uno de ellos para entenderla mejor.

Ya se han publicado muchas referencias que contiene La La Land de los clásicos, y que son fácilmente reconocibles, en algunas prácticamente iguales emulando a Fred Astaire y Cyd Charisse en “Melodías de Broadway” o a Gene Kelly y Leslie Caron en “Un americano en Paris”, sumando “Cantando Bajo la Lluvia”, “Las Señoritas de Rochefort”, “West Side Story” bebe de lo que gusta al público, pero no cae en un sentimentalismo añorado, como si tiene el protagonista de la película Sebastián (Ryan Gosling), sino que nos lo presenta como algo nuevo y lo consigue de una forma muy sencilla, situando el mundo actual y sus protagonistas en su devenir artístico bajo la ciudad de Los Ángeles buscando el sueño hollywoodense.

La música es fundamental en las películas musicales y aquí está muy presente y de una forma muy variada, no solo en la parte instrumental, sino también en las canciones, algunos temas son más que bonitos, y al oírlos en el futuro lejano, nos traerán todos los sentimientos que despiertan, ya que siempre se desarrollan como una evasión de la realidad necesaria y que se hace posible cantando y bailando. Destacan para mí: “Another Day of Sun”, “Someone in the Crowd”, “Star a Fire” y muy especialmente y por encima de “City of Stars” que es la ganadora del Globo de Oro a la mejor canción, están el maravilloso “Mia & Sebastian’s Theme” en todas sus variaciones del  film y más en el Epilogo Final; y también la Audición de Mia (Enma Stone) que está sublime en toda la cinta, pero que me tocó el corazón interpretando “The Fools Who Dream” y que desde ahora lo hago un himno para mí, mis felicitaciones a Justin Hurwitz. 

Finalmente está el guion, que corre a cargo del también Director “Damien Chazelle”, y que supera con creces a su sobrevalorada “Whiplash” que a mí no me gustó nada. En este caso el guion se centra en  la historia de estos dos artistas: músico y aspirante a actriz, que intentan llevar a buen puerto sus sueños e ilusiones; y lo hacen juntos pero afrontando las dificultades que nacen en el camino, llevándoles a extremos donde deberán decidir si para hacer realidad su sueño habrá que hacer muchos sacrificios, y dejar en el camino aquello que no priorizamos en el momento que nos surge, aunque tengamos que arrepentirnos toda la vida de haberlo hecho. 

El final de la película es el que te deja un sabor de triunfo y derrota, algo que nos sucede a diario, y por eso te toca el corazón haciéndonos pensar que ha acontecido, que habría sucedido y cuál es el desenlace  final que aceptamos sin consuelo.

Globos de Oro:
- Mejor Comedia o Musical.
- Mejor Director Damien Chazelle.
- Mejor Actor de Comedia o Musical Ryan Gosling.
- Mejor Actriz de Comedia o Musical Enma Stone.
- Mejor Guion.
- Mejor Banda Sonora.
 - Mejor Canción City of Stars.

Calificación: 🎥🎥🎥🎥🎥🎥🎥🎥🎥 🎥   (10 sobre 10)
        
            