
                      Ayer fui a ver Avatar. La sala estaba a rebosar, incluso con discusiones por las localidades que molestaron al principio de la película.

Estaba nervioso. He leido tanto sobre esta película que temía ser defraudado. Sabía su argumento y conocía lo bueno de los efectos visuales.

Mientras iba viendo la peli me recordaba a otras grandes películas de la historia reciente del cine. Avatar tiene un poco de Braveheart, de Star Wars: Episode I – The Phantom Menace, de Terminator, de Bailando con lobos , de Apocalipse Now y de Parque Jurásico. Es como si James Cameron hubiera cogido grandes momentos de estas pelis y los hubiera combinado.

Las imágenes de la película son de una belleza extraordinaria y los efectos visuales son sencillamente perfectos. A los Na'vi te los crees completamente y al final de la peli los sientes totalmente reales.

Todo esto está muy bien en una película pero además una gran peli tiene que tener una gran historia y Avatar la tiene. Es una historia ya conocida pero magníficamente contada.

Además Avatar tiene mensaje ecologista e historia de amor interracial (o interplanetario).

Pero es que además de todo lo dicho Avatar emociona, te conmueve y te hace tomar partido por los buenos osea los Na'vi.

George Lucas creó toda una galaxia imaginaria. Cameron ha sido menos ambiocioso y ha creado un planeta (Pandora) que inventó ya en su juventud y que ahora la tecnología actual le ha permitido llevar a la pantalla.

El 3D está bien pero la película es seguro igual de buena en 2D.

Se hablaba de que Cameron iba a cambiar la historia de cine con Avatar. No sé si cambiará la historia del séptimo arte pero de lo que sí estoy seguro es que ha hecho una grandísima película. Sencillamente imprescindible.
        
            