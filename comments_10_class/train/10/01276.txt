
                      Un día, en el norte de California, las aves deciden hacer la guerra a la humanidad. Después de unos pocos picotazos se utilizaron un asalto masivo, y el machista Taylor local, con la niña de Dippy, tiene que hacer frente a las gaviotas y periquitos.

Una rara incursión en lo absolutamente fantástico, esto no ofrece ninguna explicación para la crisis y, por lo tanto, se enfrenta a sus personajes prosaicos con lo irracional. Gran momento: los pájaros se reúnen lentamente en un marco de escalada fuera de la escuela mientras Tippi tiene un cigarrillo mientras que los niños cantan. El mejor momento emocionante: la casa bajo el ataque y la insensata incursión de Tippi en el ático.
        
            