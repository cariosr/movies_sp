
                      13 años han sido más que suficientes para que Pixar se diera cuenta que de “Buscando a Nemo” pedía a gritos una secuela. Económicamente hablando, pues aquella cinta del pez payaso era lo suficientemente buena como para valerse por sí sola y era una historia con un final cerrado de la que nadie se imaginaria una continuación. Pero es Disney y es Pixar, aquí esa regla no existe.

Para todos los que pensábamos que Nemo no necesitaba continuación, “Buscando a Dory“, nos da la sorpresa máxima de que es la mejor segunda parte de una película de Pixar. De que es la mejor segunda parte de una película Disney. Y de que si me apuras, es la mejor segunda parte de la historia del cine. “Buscando a Dory” es una manera perfecta de hacerte una maratón de las dos películas y que casi no notes diferencia entre el final de una y el comienzo de otra. En lo que en historia se refiere, claro. Ésta, lo hace tan solo un año después de los acontecimientos de la primera entrega, y lo hace dándole protagonismo a la pérdida de memoria de Dory y todo lo que eso ha conllevado en su vida. Por lo que también podría haberse llamado “El origen de Dory”, pero no hubiera tenido tanto tirón.

Los 13 años que han transcurrido entre una película y otra se notan, y mucho. Y es que obviamente, no se ha usado la misma tecnología de entonces, con lo que el aspecto visual se ve claramente mejorado. Incluso la trama ha aprendido y aquí tenemos todo mucho más magnificado. Es una segunda parte y la fórmula de “el más difícil todavía” debe estar presente. Con lo que la acción, el humor, los sentimientos (como se nota que “Up” existe entre una película y otra), y sobre todo el alma de Disney Pixar es más grande, más impresionante y definitivamente, más mágico.

Vuelvo a repetir que podemos estar ante una de las mejores secuelas del cine. “Dory” merecía esta película. Los fans merecíamos esta película.

Por supuesto, como buena cinta de la factoría, existe escena tras los créditos y es, exactamente, la escena que esperas ver desde el comienzo de la historia. Un broche de oro para una nueva obra maestra.

Lo mejor: su esencia mejorada, sus ganas de llegar a todos los públicos y su forma de saber que, todos los que crecimos con Nemo, ahora somos adultos.
Lo peor: absolutamente nada.

NOTA: 10/10
        
            