
                      Título: Rush
Año: 2013
Duración: 123 min.
Dirección: Ron Howard
Guion: Peter Morgan
Música: Hans Zimmer 
Crítica: Muy buena película basada en hechos reales de Ron Howard, que muestra la verdadera rivalidad de James Hunt y Niki Lauda (para mi,  el mejor piloto de todos los tiempos). Unos actores de primera, clavados a los personajes que interpretaban. Unos gráficos y una calidad de imagen, que aunque estuviera hecha en 2013 hicieron que pareciera grabada en 1976, el año que sucedieron los hechos. Las carreras parecían muy reales, los coches eran auténticos del año de los echos.
Muy entretenida y recomendable para todas las edades.
        
            