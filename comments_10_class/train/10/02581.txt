
                      Una preciosa historia de amor, con grandísimos actores (que saben actuar, bailar y cantar), un maquillaje y decorados geniales y sobre todo, una espectacular banda sonora ganadora del globo de oro, tanto las adaptaciones como los temas originales valen la pena. Totalmente recomendable.
        
            