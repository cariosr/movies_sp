
                      Película de culto para los amantes de la ciencia ficción. No sé cuántas veces he podido verla y aún me pone la carne de gallina... Especialmente el precioso diálogo de Roy Batty (Rutger Hauer) con su famosa frase: "todos esos momentos se perderán en el tiempo... como lágrimas en la lluvia". Sin duda alguna una obra maestra.
        
            