
                      A pocas peliculas le doy 5 estrellas, contadas con los dedos de una mano estan. Para concederselas el Film debe reunir todos los requisitos.
-Sublime Fotografia.
-Sublime Banda Sonora.
-Sublime Trama.
-Sublime actuacion.
-Y un mensaje.... sublime.

Y es que "La vida de Pi" es una historia de comprension, respeto, civismo y amor.... Donde el creer en un dios no es un concepto, si no una inspiracion. Donde la supervivencia es a veces tan cruel, como el propio abandono de la misma. Un film para disfrutar, emocionarse y reflexionar.... Sin lugar a dudas, uno de los Films mas hermosos y entrañable de los ultimos 25 años, que ademas, por si fuera poco, te entretendra de principio a fin.
Si te deja indeferente, es que no tienes alma.
        
            