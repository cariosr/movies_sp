
                      Mi película preferida de la infancia, fue la que despertó mi pasión por el cine, nunca me cansaré de volver a verla repetidas veces, han quedado grabadas en mi memoria y en mi corazón muchas escenas inolvidables que me hicieron disfrutar a los 5 años desde que la vi por primera vez.
        
            