
                      Fuí a ver la película después de leer numerosas críticas en contra de ella y sólo tengo que decir que hacía ya mucho tiempo que no oía aplaudir a una sala de cine al acabar la película. Para mí eso es más que suficiente para saber si la película es buena o no, si ha llegado al público o no. No entiendo qué ven los críticos en contra de este film con el que pasas un rato muy, muy agradable, te sumerges en un mundo fantástico, te emocionas,...lo tiene todo. 
Muy recomendable para todos los públicos y los niños se lo pasarán genial.
        
            