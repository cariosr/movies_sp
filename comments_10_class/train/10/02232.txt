
                      Maravillosa. Magnifica la ambientacion, la fotografia, el vestuario... Increible el guion que nos habla de una mujer adelantada a su epoca, en un mundo que no le corresponde. Impresionante Rachel Weisz, cautivando al espectador desde el minuto uno. Excelente critica al poder de las religiones, a la intolerancia. Canto al amor, a la sabiduria, a la lealtad... Imprescindible!! Amenabar vuelve a dar una leccion de cine. Que aprendan algunos...
        
            