
                      Por fin ha llegado a España la segunda parte de la película Sexo en Nueva York, que tanto hemos esperado. Algunos, al menos. 
Y no nos ha defraudado. Las actrices lucen tan estupendas como siempre, destacando a Kim Catrall, que interpreta a la divertidísima Samantha.
El vestuario y la fotografía son aún mejores que en la primera parte, lo cual es decir mucho.
Y hay que hablar de la aparición, semiestelar, de nuestra Penélope Cruz, que, a pesar de sus numerosos premios como actriz, incluído el tan preciado Oscar, no brilla tanto en este film, aunque quizás es porque su intervención es realmente muy breve. Aunque quizá simplemente no ha sabido sacarle partido. 
Seguiremos esperando que éste no sea el final de la saga y que los productores se decidan ante el gran éxito obtenido a realizar la tan ansiada triología.
        
            