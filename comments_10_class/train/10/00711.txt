
                      Si! Me gustó más la segunda, que su antecesora. 
Cumple con todos los requisitos para ser una buena película de héroes gamberros, ciencia ficción y absolutamente fantástica. Todos los personajes dan su buen perfil, pero el director y los creadores, dieron a la perfección al personaje Baby Groot, debería empezar a ser ya un icono de esta época! Solamente por el personaje en si. 
En el aspecto negativo, varias escenas que sobrarían ( no quiero Spoilear ) y la música realmente es mejor la primera, a excepción de varias canciones como las de Fleetwood Mac, Cat Stevens y ELO.
        
            