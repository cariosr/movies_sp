
                      Una muy buena película, al parecer a los no-creyentes les molesta que les digan la verdad en su cara a través de un film.

La película narra la historia de la resurrección de Jesús desde un punto de visto de un ateo. Aún no entiendo como es que los estúpidos agnósticos meten su nariz donde no les conviene, desacreditan esta gran película simplemente porque dice la verdad y porque es Cristiana (aparentemente), mientras que ellos se tragan de que algo tan complejo como el ADN, las leyes físicas, el mismo Universo, de la nada, para nada, por nada, al parecer ellos tienen mucho más fe que yo. 

Conclusión: En mi opinión, si eres creyente, la película te parecerá muy buena, y realmente lo es, pero si no lo eres, la película te parecerá algo confusa, y acabarás criticando lo que ni siquiera puede entender tu mente diminuta.
        
            