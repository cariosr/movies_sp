
                      Película dirigida por el director mas prestigioso del panorama actual, protagonizada por Leonardo di caprio que nos lleva a ser casi protagonistas de la historia. Siempre recordaremos todas la famosa escena de la proa del barco y como no una música inolvidable que te hace evocar los confines marinos.Indispensable de ver al menos una vez en la vida.Además de ser una de mis favoritas como película oscarizada .Recomendable de ver a todos los amantes del cine que le gusten los efectos especiales espectaculares
        
            