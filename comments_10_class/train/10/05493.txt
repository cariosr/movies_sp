
                      John Q. Narra la historia de un obrero (Denzel Washington), a quien una reducción de jornada laboral le bloquea, sibilinamente, su seguro médico, de manera que no puede hacer frente a una costosa operación de corazón a que deberá someterse su hijo pequeño, su pena de muerte a plazo fijo.
El guión está muy bien elaborado y es preciso, con los giros necesarios para que la intriga y la emoción no decaigan. Además la película mezcla muy bien el drama, el thriller policiaco y las películas de secuestros. El montaje es rápido y bien realizado, sin confusiones y directo.

Esta película es una buena muestra de crítica social. Fue número 1 en la taquilla estadounidense he hizo replantearse muchas cosas a un país que no cuida nada la sanidad para la gente que no tiene dinero o trabajo, y que solo se preocupa de las capas altas de la sociedad. Esta cinta es la lucha de un padre contra dos injusticias: la muerte de su hijo y la sanidad americana.

Las actuaciones son impecables, encabezada por un soberbio Denzel Washington, que hace un interpretación brutal, haciendo que te metas en la piel de ese padre desesperado que no le importa morir para salvar la vida de su hijo y que te arranca las lágrimas con esa charla que le da a su hijo al final del film.
Los secundarios están bastante bien, sobre todo Robert Duvall, buenísimo actor que siempre cumple a pesar de que ya no sale en tantas películas como antes. Ray Liotta, que exeptuando “Uno de los nuestros” no es un actor muy santo de devoción, está pasable. James Woods, hace un papel correcto.
En definitiva una maravilla.
        
            