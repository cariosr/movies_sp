
                      Extraordinaria. No resta nada al musical; por el contrario si suma algo imposible en un teatro: las extraordinarias interpretaciones en primeros planos de grandísimos actores, que no cantan nada, pero nada mal. A destacar la impresionante actuación y voz de Anne Hathaway. Su solo de 5 minutos en primerísimo plano pone los pelos de punta. De las escenas mas intensas y sobrecogedoras que he visto en mi vida en un cine. Emociona hasta las lagrimas. Huele a Oscar a mejor actriz. Una de las mejores películas de los últimos años. Al final el cine aplaudió
        
            