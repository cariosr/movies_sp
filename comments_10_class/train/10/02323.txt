
                      Cada vez que voy con mis hijos a ver una película de Píxar casi estoy seguro de que me lo voy a pasar en grande. Son películas que valen igual para pequeños que para mayores, muy divertidas, con mucha acción...
Es una película emocionante, divertida, melancólica, con acción, aventuras, fantasía, risas y sobre todo: no es en absoluto convencional, empezando por el protagonista, un viejo gruñón
        
            