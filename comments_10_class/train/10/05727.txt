
                      Excelente de principio a fin; juega con los conceptos de amor, soledad, imaginación e idealización dejando espacio para una nueva construcción social. Fotografía implacable, guión conciso e idea final -a pesar de ser bastante confusa- brillante.
        
            