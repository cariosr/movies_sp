
                      Sin duda una obra maestra como era de esperar de un director como; Antonio Banderas.
Una película llena de frescura , realidad ,magia , desilusiones y fracasos de la que pudo ser cualquiera de nuestras adolescencias.
Retrata de un modo tremendamente intenso y hermoso ,las épocas del corazón .Primer amor ,primer dolor... .
De personajes tremendamente bien marcados eh interpretados es una historia dotada de una elegante fotografía y melodía perfecta ,una música que acompaña todos y cada uno de los grandes matices de "color" y vida de este film de sencillo a la par que buen ;argumento.
        
            