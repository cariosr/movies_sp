
                      recomendable 100%. Muy bien hecha, se nota la mano de un buen director. Y fantástica banda sonora. Una pequeña joya del cine español, una increíble historia magníficamente resuelta a través de flash backs ente uno de los verdugos y el sentenciado a muerte. El protagonista es un cura, pero no por ello es una película religiosa, va más allá entrando en los valores humanos, en como lucha por las desigualdades sociales, tanto económicas, como de género. Tiene sus momentos simpáticos, como esos momentos en los q se te escapa una lagrima. Enhorabuena
        
            