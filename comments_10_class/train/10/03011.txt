
                      "El uso selectivo, pero acumulativo, de experiencias aparentemente arbitrarias, pero significativas, de la adolescencia le otorga al filme un carácter distintivo y un peso impresionante".
        
            