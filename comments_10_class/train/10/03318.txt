
                      Una magnífica adaptación de la obra de Roald Dahl, irreverente y divertida. Nada es lo que parece y los cuentos clásicos se rebelan y se entremzclan. Un entretenimiento inteligente para toda la familia!
        
            