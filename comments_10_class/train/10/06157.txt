
                      Excelente documental que muestra una vida emocionante de un artista español. La llegada al éxito no es fácil pero si juntas trabajo y talento puedes ser uno entre un millón. Me ha encantado, emocionado e inspirado para seguir adelante. Bravo!!!
        
            