
                      De todas las películas de la saga, sin duda esta es mi preferida, cuando se articula toda la relación y se definen los personajes. Aunque su guión y diálogos lo encumbren en el cine adolescente, permite pasar un buen rato disfrutando de una relación tan irracional como la que puede surgir entre un humano y un vampiro. 
Además han sabido actualizar la estética de los vampiros, alejándolos de esa imagen clásica que nos dan de ellos en todas las películas.
        
            