
                      Sin duda es una de las mejores películas de este año. Refleja mucho la angustia que vive el personaje y  tiene unos efectos especiales impresionantes. Un buen elenco, y sobretodo un drama espacial único.
        
            