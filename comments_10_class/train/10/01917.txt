
                      La mejor producción desde el Batman de Nolan sobre el Universo DC.
Cámara, guión, desarrollo, actrices y actores sobresalientes en sus papeles y soberanamente bien construidos y argumentados.
Lo unico criticable el empleo de la noche para mejorar el CGI, pero eso es más un gusto personal que un fallo, ya que durante el desarrollo de la trama no repercute.
Gracias Gal Gadot por hacer historia en el género de superhéroes.
        
            