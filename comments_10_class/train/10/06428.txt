
                      Ya era hora que películas como esta lleguen a cambiar el cine o a agregar detalles profundos. la película es muy innovadora, es un romance bastante real, se siente cada interacción con los personajes, las situaciones son muy acertadas y el personaje principal es impecable, magnifica.
        
            