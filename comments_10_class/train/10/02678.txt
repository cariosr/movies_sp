
                      Brillantemente sombrío y escalofriante, gótico (en reconocimiento a la fascinante cinematografía de Wally Pfister), The Dark Knight emerge como la primera película verdaderamente genial de 2008.
        
            