
                      No solo capta bastante la esencia del clásico de Carpenter, sino que Zombie es capaz de crear una precuela dentro de la misma película con un origen de uno de los mayores iconos del terror de lo más interesante, tanto desde el punto de vista de lo argumental como de lo cinematográfico. Un Michael Myers brutal y un remake que debería ser paradigma de cómo dignificar un personaje paradigma del cine de terror en una versión estupenda
        
            