
                      Una película que no decae nunca, las actuaciones son estelares, el guión impecable y la ambientación excelente. Más allá de la sociopatía de esta familia, basada en hechos reales, lo que me ha parecido brillante es la descripción del trasfondo social de Argentina.
        
            