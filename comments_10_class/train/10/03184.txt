
                      Pensar que la primera vez que la vi, fue porque mi profesor de la Facultad me "obligó" a verla para un trabajo de Redes y Nuevas Tecnologías. Descubrí en ella una historia dentro de una historia. Con un gran comienzo, un portentoso primer encontronazo con Mark y Erika en el bar y toda la supuesta historia que viene detrás del origen de la mayor red social que ha dado el planeta Tierra. Un guión de diez, una buenísima dirección de David Fincher y dos grandes como Gardfield y Eisenberg. ¿Alguna pega? Puede que Justin Timberlake me sobre del elenco.
        
            