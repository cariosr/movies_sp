
                      El cambio de juego de Barry Jenkins sobre el crecimiento de negros, gays y alienados en los proyectos de Miami es íntimo y épico. Se mete dentro de tu cabeza, te hace ver el mundo con nuevos ojos, y luego te posee.
        
            