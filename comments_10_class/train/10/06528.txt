
                      R e-lanzado en 40 años, y digitalmente pulido, impresionantemente ambicioso precuela-secuela de Francis Coppola a su primera película padrino es tan apasionante como siempre. Es incluso mejor que la primera película, y tiene la mejor escena final en la historia de Hollywood, un verdadero golpe de cine .

Michael Corleone (Pacino) , su rostro ahora una máscara arrugada de odio implacable, ha cobrado venganza a todos sus enemigos, y luego, de repente, recortamos 20 años al joven Joe College, aún capaz de mostrar una sonrisa infantil, sorprendiendo a su Hermanos en la mesa de la cena anunciando que se ha unido al ejército y que defienden firmemente su patriotismo. Es un florecimiento narrativo impresionante: misterioso y conmovedor.

Regístrese en nuestro correo electrónico de Film Today.
 Lee mas
Las películas de El Padrino han sido acusadas, por alguna razón, de glamourizar a los acosadores del crimen organizado y, de hecho, por ser un manual de cómo comportarse durante generaciones de aspirantes a compradores. Pero qué poder, incluso la grandeza que hay en estas películas: una historia de intriga política de arriba hacia abajo.

Es un lugar común llamar al Padrino 2 "Shakespeare"; Me encuentro recordando el yo de la BBC , Claudio . Después de la muerte de Vito, el joven Michael Corleone es susceptible a una nueva figura de cuasi padre, el anciano jefe de Florida Hyman Roth, jugado con estilo y potencia silenciosos por Lee Strasberg. Michael está convencido de probar las lealtades familiares al expandir su imperio con Roth en Cuba: juntos tipificarán la corrupción que creó a Fidel Castro. La acción transcurre de un lado a otro con la vida y carrera del joven Vito Corleone, interpretado por Robert De Niro., en 1920 Nueva York. Vito muestra su comprensión instintiva de cómo combinar la violencia estratégica con demostraciones operísticas de sentimentalismo y generosidad. En su adolescencia, él ve por primera vez cómo se necesita un poco de alianza contra el florido Don local, que amenaza a la hija del dueño del teatro. (Cuando estaba en Sicilia, un guía me dijo que la palabra "mafia" se derivaba de la frase " non tocca ma figlia ", "no toques a mi hija", una explicación interesante que aún no he visto confirmada.

Robert Duvall es brillante como Tom, y Diane Keaton es memorable como la extraña esposa de Michael, Kay, la única voz infeliz de la cordura y la modernidad.
        
            