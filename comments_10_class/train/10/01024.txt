
                      Curiosamente la primera película que he visto en 2012 ha sido una de las mejores películas del 2011, Drive. 

Drvie es una película de mafia, de acción, de persecuciones... pero lo mejor de todo es que a la vez no es ninguna de las tres. Durante los 102 minutos que dura vas descubriendo distintas sensaciones según avanza la película de la mano de su protagonista, un conductor no demasiado hablador interpretado por Ryan Gosling. Además, los fans de la seire Breaking Bad disfrutaremos en la gran pantalla de la actuación de Bryan Cranston. 

Tres consejos. El primero, no vayas con niños pequeños a ver esta película. El segundo, siéntate en la butaca y sigue el ritmo de la película, pero sin prisas porque si lo que buscas es acción, tranquilo, que llegará. Y, por último, deja las palomitas para después de la película.
        
            