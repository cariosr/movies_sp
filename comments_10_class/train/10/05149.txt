
                      Todo amante de Tarantino va a sentir un placer inmenso con este film que parece que dure la mitad de lo que dura. Te engancha desde el inicio! Su guión sencillo y claro, sus planos brutales y gran fotografia!!! Y la interpretación de todos los actores espectacular! Sin olvidar un Samuel L.Jackson que me hizo reir mucho!!! Tarantino sabe lo que necesita el espectador a cada momento y te complace momento a momento convinando drama, acción y humor de un modo magistral!!! No se la pierdan!!!!!!
        
            