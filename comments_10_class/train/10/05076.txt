
                      Con un presupuesto mínimo, una película máxima. Paisajes preciosos, una dirección de imagen inmejorable, actores humildes y entregados. Una crítica y reivindicación que hace reflexionar sobre el esfuerzo, y sobre el valor de lo que tenemos a día de hoy. Recomendable.
        
            