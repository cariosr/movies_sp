
                      ¡A-DO-RO esta película! Es simplemente genial, con un Jason Statham que de nuevo mata a los policías psicópatas que merecían ser asesinados. ¡Bran lo logra! Esto hace ver bien a una película donde el sujeto no es tratado mucho. Me encanta el final. El ambiente obscuro es excelente, los actores son muy buenos al igual que el escenario. Al final, tenemos derecho a un gran "thriller", que sin ser una obra maestra, nos tiene sujetos a nuestros asientos durante 1:32 horas. Una violencia placentera, por lo tanto, para mí este filme es ¡EXCELENTE!
        
            