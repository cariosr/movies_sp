
                      Va mucho más profundo que El Padrino al analizar las retorcidas mentalidades de estos hombres que pervierten el sistema capitalista para su propio beneficio. La película es más rica en textura y da más evidencia de la conciencia social.
        
            