
                      Realmente, es una nueva obra maestra de Spielberg. Genio Steven! Lo volviste a hacer...
Si no tuviste la suerte de vivir tu infancia durante las décadas de los 80s y 90s, te va a resultar una película muy entretenida, con escenas de acción increíbles. Es recomendable verla en la pantalla más grande que puedas.
Pero si naciste en esa época, directamente te va a volar la cabeza! La cantidad de referencias y "golpes a la nostalgia" son tantas, que no te van a alcanzar los ojos para encontrarlas. Es realmente el mejor homenaje que se pudo hacer a una época hermosa del cine y los videojuegos. Ya con solo volver a ver al DeLorean en la pantalla grande, la entrada se paga sola... una locura!
        
            