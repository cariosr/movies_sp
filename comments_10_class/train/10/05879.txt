
                      Una comedia exitosa donde malentendidos y réplicas estrepitosas se siguen a gran velocidad para el mayor agrado del espectador y gran desgracia de sus principales personajes. Esta obra esta brillantemente escrita y la interpretan actores formidables como el siempre genial Tommi Eronen y la magnifica Anna Easteden, divorcio a la Finlandesa es una comedia picante y delirante que bajo ningún pretexto se debe dejar de ver.
        
            