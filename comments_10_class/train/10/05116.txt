
                      La película es sensacional, no
Es fácil encajar en la gran pantalla una segunda parte de un taquillazo como fue Mamma Mía. 
Música increíble para hacerse con la banda sonora, escenarios paradisíacos y vestuario y colorido alegre y jovial, de llamar la atención la fotografía. 
Argumento
Encaja a la perfección con la primera parte y reafirma el sentido de la película de ver la vida con optimismo y de perseguir los sueños.
        
            