
                      Me ha parecido una obra maestra. Yo no soy creyente pero cada escena y cada comentario te enseña algo importante de la vida. Engancha con historias paralelas y el drama es precioso, no soy el unico que ha llorado en el cine viendola. 

Para los que esperais la tipica heroina americana en la tipica peli española meteros en otra sala, porque esta es una pelicula humilde, real, española y muy bien trabajada desde el corazon. Muy buenos actores sobre todo segundarios y sobre todo una banda sonora que emociona solo con escucharla.
        
            