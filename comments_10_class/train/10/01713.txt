
                      The Master (2012) Una historia de Freddie Quell (Joaquim Phoenix) es un hombre perdido, profundamente solitario, incapaz de conectar con el resto de la sociedad.
Viene del legado con el que sobrevivía Travis Bickle (Robert DeNiro), de Taxi Driver (1976).
Anderson es un cineasta que viaja entre antónimos: tradición frente modernidad, pasión y frialdad, la locura desde la lógica, sin llegar a contradecirse La historia sectoreligiosa es original e interesante ya que refleja a la América mas inepta y crédula.
Hay que tener valor para realizar una película tan distinta para los tiempos que corren, una película que cuida al detalle cada elemento que la conforma.
        
            