
                      Para mi la más emotiva. Imborrable en la memoria. Un recuerdo cuando fui a verla al estreno con mis hermanas y sobrinos, el más pequeño de 1 año y medio cuando la escena de la muerte del Rey ( todo el mundo con un nudo en la garganta), simba llama a su padre. En medio del silencio mi sobrino gritó "papaaa" y el publico tuvo que reirse, lo cual aflojo un poco la tensión del momento. Inolvidable.
        
            