
                      La película más madura de Marvel.

Se podría clasificar como un "thriller" de acción de muy buena calidad y con el que te quedas satisfecho. A pesar de su título, es más una tercera entrega de Los Avengers que de Capitan America. 

Una historia emocionante y con buenos toques de humor a ratos. Y que recae en la amistad de estos superheroes.

