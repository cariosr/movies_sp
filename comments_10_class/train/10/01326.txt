
                      Yo soy una de las personas que no hemos leído el libro y puedo asegurar que la historia engancha así como sus personajes, su fotografía, su banda sonora y sobre todo sus paisajes. Me ha dejado una sensación tan buena como lo fue en su día Leyendas de Pasión. No os la perdáis en pantalla grande. Es una de esas películas que recordaré siempre.
        
            