
                      Otra obra maestra de Darren Aronofsky. La fotografía es impresionante (sobre los efectos especiales con reacción químicas). El guión es un poco complicado pero nunca absurdo (quizás hay que ver la dos veces para entenderla).
El final puede ser interpretado de diferente forma, asi que aviso a los espectadores con están acostumbrado a tener repuestas antes de ver el "THE END".
        
            