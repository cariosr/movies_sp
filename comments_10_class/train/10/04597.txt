
                      Star Wars: El Imperio Contraataca es la segunda entrega de esta saga de películas. Está dirigida por Irvin Kershner (RoboCop 2, Nunca digas nunca jamás) y producida por George Lucas.

A pesar de que los rebeldes consiguieron destruir La Estrella de la Muerte, gran parte de las tropas imperiales, junto con Darth Vader, han sobrevivido.
Los rebeldes se han visto obligados a dejar sus bases y asentarse en el planeta helado Hoth, ya que Vader busca con vehemencia a Luke Skywalker. Por ello, enviará a sus tropas imperiales tras él.

Una secuela muy digna de la primera película. Al igual que la acción, el suspense aumenta en esta entrega. La dirección es muy buena, y los giros dramáticos inesperados están muy bien utilizados. Además cuenta con la que pueda llegar a ser la frase más famosa en la historia del cine ("yo soy tu padre").
El humor sigue estando presente en momentos concretos, lo que la hace más completa.
El final muy abierto, para una tercera película, deja al espectador con ansias de más.

La banda sonora, original de John Williams, radica en su mayoría en las canciones de la primera película, con algunas incorporaciones nuevas igual de magistrales.

El filme estuvo nominado a un Globo de Oro y a cuatro Oscar, de los cuales ganó dos.
        
            