
                      Un padre que no le presta atención a su hija, una pareja que espera un hijo, estudiantes de insituto que viajan a un partidor, un par de hermanas, un mendigo y más, son los personajes de esta película donde en el pequeño espacio en un tren tratarán de sobrevivir.
La película llega a los corazones de todos los espectadores, te hará sentir
mariposas ya que llegarás a querer a muchos personajes, pero eso no asegura su
supervivencia, para mí es la mejor película de genero zombie de   la década y aseguro
que quien vaya a ver la película no se arrepentirá.
        
            