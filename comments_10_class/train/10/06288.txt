
                      Una obra maestra. Desde El Pianista de Román Polanski, no me había impresionado tanto un largometraje. Duro, violento, donde se demuestra de la manera más cruda la inutilidad, y la crueldad de las guerras, el deshonor de todas las victorias. Rodada en blanco y negro a modo de reportaje grafico de la época, con unos protagonistas que con solo la mirada, lo dicen todo.

    Si señores, por fin una obra maestra. Un largometraje de los que cuando sales de la sala, te sientes lleno, lleno de buen cine. Con la sensación de haberlo visto todo, de no haberse quedado nada en el tintero. Como el que después de un banquete, dice ya no quiero más porque ya no tengo más hambre.

    Muy recomendable.
        
            