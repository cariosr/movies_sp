
                      La incógnita del tiempo, las realidades paralelas y la posibilidad de vivir otra vida más maravillosa que aquella de la que se cree que ya lo es todo.
Un excelente ejemplar del género, los protagonistas, brillantes.
Contenido, forma, estética, actores...
        
            