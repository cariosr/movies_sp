
                      Peli costumbrista en la línea de nuestro querido Woody cuando era realmente Woody Allen... Exhibe un Manhattan real y genuino y sus personajes llenan de sentido la historia, muy bien enlazada, muy autentica! Disfrute cada minuto, el guión. La frescura de los personajes, genial actuación, diversificación de caracteres y situaciones, la fotografía neoyorquina fantástica! Que gusto poder volver a ver pelis así ; )
        
            