
                      FANTÁSTICA ésta inmensa Australia que abarca todos los géneros: drama, bélico, comedia, romance, aventuras, fantasía... Luhrman nos ofrece una historia emocionante con unos actores que quitan el hipo (ayyy Jackman... con razón te eligieron el más sexy del 2008 xDD) y una banda sonora como no podía ser de otra manera excelente. Mención especial para la preciosa fotografía, el hermoso vestido de Kidman en el baile, y el niño aborigen (me lo quedo! xD)
A diferencia de otros, me gustó mucho el final y me pareció lo más justo... :D:D Se la recomiendo a todos los fans de las grandes películas como las de antes ;)
        
            