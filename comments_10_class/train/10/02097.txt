
                      Más allá de los prejuicios, no se trata de una película que desafía a la ciencia, sino que nos enseña a escuchar a nuestro cuerpo. Tendemos a ignorar lo que nuestra fisiología nos dice, escapándonos en las drogas, remedios, y otras recetas médicas. Capturar la energía que perdemos en el momento de la digestión, no es tan científicamente abstruso.
        
            