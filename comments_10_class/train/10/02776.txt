
                      Gran película, que pese a su duración, unas dos horas y veinte minutos, no se hace para nada larga. Con el toque especial de Mel Gibson como director, una gran actuación de sus protagonistas y una magnífica mezcla entre la fé cristiana y la guerra, hacen de esta película una obra de arte.
        
            