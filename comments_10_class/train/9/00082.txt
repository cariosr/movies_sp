
                      La trama de la cinta me pareció acertada y más teniendo como referencia que la información pasa por una extensa gama de imágenes que son las que llaman a seguir o no determinada noticia y si es más sangrienta aún mejor. Además fue llevada de una forma ordenada aunque en ocasiones se me hizo bastante más densa de lo que un principio podría haber pensado pero sin ser pesada o aburrida, todo lo contrario. Te va envolviendo hacía un mundo turbio donde los protagonistas son los encargados de informarnos.

Me quedé con ganas de escenas o secuencias algo más impactantes dado el desarrollo de los hechos aunque las que hubo, no me resultaron nada desagradables y como seguimiento psicológico de su personaje principal, me resultó perturbador desde el punto de vista ético y moral. Un film que entretiene y cumple con el propósito que supongo tenía en mente su director, mostrar el lado más salvaje del ser humano que siempre busca el morbo o la sangre para poder encontrar una satisfacción. 

Jake Gyllenhaall está fenomenal, uno de los mejores papeles de su carrera. 
Así como la resurrección interpretativa de René Russo fabulosa.
En definitiva, muy buena película.
        
            