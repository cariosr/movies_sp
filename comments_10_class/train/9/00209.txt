
                      PELICULÓN con mayúsculas. No es tanto la historia en sí, si no cómo han secreado el juego hasta el más mínimo detalle (salen hasta las piedras de invocación de los mapas). Impresionante, nunca me había quedado en una película con los escenarios y la ambientación tanto como ésta.
        
            