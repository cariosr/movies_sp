
                      Agentes de reemplazo

Es asombrosa, casi imposible, la misión a la que ha tenido que enfrentarse esta franquicia, la de no autodestruirse en cuestión de segundos. Y la ha cumplido con enorme éxito. Desde la renovación de J.J. Abrams que recuperaba la esencia de la serie original hasta la acción heredera del cartoon de un experto en la animación como Brad Bird, pasando por la denostada action movie hiperbólica y autorreferencial del maestro John Woo, cada nueva entrega ha supuesto un soplo de aire fresco con respecto a la anterior. 

Lo ha vuelto a hacer. “Misión: Imposible – Nación secreta” no innova sobre lo ya visto, algo que tampoco pretendían sus predecesoras, pero sí que se preocupa por no transitar por lugares comunes y logra ofrecer un envoltorio distinto, un cambio de rumbo que imaginamos se quedará en este quinto capítulo. Porque cada nueva misión de Ethan Hunt es hija de su creador, y en este caso la impronta de Christopher McQuarrie se aprecia ya desde un guión enrevesado –quizá a veces demasiado- que juega con las constantes del cine de espías –hay no pocas referencias a James Bond, y cierto pasaje operístico de reminiscencias hitchcockianas- y pone en entredicho las que han regido la propia franquicia, como las máscaras o las capacidades físicas de su protagonista.

Pero también en la realización. La dirección de McQuarrie derrocha pura clase y elegancia, y lo que ofrece es un thriller de acción a la antigua usanza, cine de espías de la vieja escuela donde la acción está supeditada al suspense, muy en la línea de su “Jack Reacher”, sin por ello dejar de lado el entretenimiento que debe proporcionar todo buen blockbuster. Un divertimento tremendamente inteligente que tiene el enorme acierto, además, de introducir a la ambigua y letal Rebecca Ferguson y ampliar el papel de ese contrapunto cómico perfecto que es Simon Pegg.

Estamos, posiblemente, ante el mejor film de toda la saga, y también ante el más personal y el que desprende un mayor aroma de cine de autor. Una aventura sin respiro protagonizada por agentes de reemplazo, predominando el cumplimiento de la misión sobre la figura del agente de secreto, y que ofrece desde su premisa argumental, basada en lo anticuados que están los métodos de la FMI, justamente la metáfora opuesta de lo que es en sí la película. “Misión: Imposible” no está pasada de moda, ni mucho menos muerta. Está más viva que nunca. Y su protagonista, Tom Cruise, vuelve a demostrar que sigue en plena forma. 

A favor: su cambio de rumbo desde su propia condición de cine de autor
En contra: su guión quizá se complique demasiado
        
            