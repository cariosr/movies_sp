
                      Impresionante cuento lleno de metáforas no apto para mentes dispersas. Un espectáculo visual. Ang Lee se lleva el merecido Óscar por esta belleza llena de mensajes que llegan al alma. 
La banda sonora es sublime, en especial Pi's Lullaby.
Arte cinematográfico en estado puro.
        
            