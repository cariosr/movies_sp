
                      Una razón de peso para ver 'Cisne negro' es su protagonista, Natalie Portman. La peripecia de tratar un personaje esquizofrénico, demente y fatídico hace que su presencia sea formidable. Sabe calcar las necesidades exigidas por el guión, y la obra de Chaikovski hace el resto. La fábula del cisne negro y del cisne negro queda bien representada en ese tándem destructivo de Portman y Kunis, con un "tour de force" final que hace que esos últimos cinco minutos de metraje sean de los más emotivos de los últimos años.
        
            