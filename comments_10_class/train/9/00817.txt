
                      Como la vida misma. Ese era uno de los comentarios más repetidos a la salida del cine. Woody Allen, una vez más , nos hace entrar en un mundo real y compartir con sus personajes episodios que pueden suceder dos calles más abajo de donde vivimos. Cate Blanchett está sensacional ( nominación para Oscar en mi opinión garantizada) con primeros planos muy difíciles y con una cantidad de registros magníficos. Película recomendable como todas las del magnífico Sr. Allen.
        
            