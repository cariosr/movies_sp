
                      Maravillosa, la pel?cula que me define. Aunque comparada con el c?mic hay cosas que podr?an haberse pulido m?s (las relaciones de Scott con sus amigos y exes, por ejemplo), realmente es una adaptaci?n muy fiel en cuanto al mensaje y al look. Dirigida en?rgicamente por Edgar Wright, este consigue imprimir su ADN personal a esta ya personal?sima historia. La ambientaci?n, los efectos, los actores, la m?sica... Todo est? perfectamente escogido! Algo m?s que recomendable.
        
            