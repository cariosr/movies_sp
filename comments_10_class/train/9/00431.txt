
                      Estupenda y muy fiel representación de la segunda parte del conocido libro de Tolkien El Señor de los Anillos.
Al igual que en la primera y la tercera película, hay partes del libro o ciertos hechos que no se muestran en la pelicula, aunque por lo general es una buena representación y muy fiel que incluso gustará a los fans de la novela.
Esta, al no resultar una introducción como la primera, contiene algo más de acción y resulta un poco más fluida. 
Profundizan más en las historias de los personajes, que ahora llevan caminos separados: Por un lado Frodo, Sam y Gollum se dirigen a Mordor para destruir el anillo único, y por otro lado Aragorn, Légolas, Gimbli, Gandalf y el resto de aliados tratan de proteger la Tierra media; lo cual hace que los diferentes puntos de vista mejoren la trama. La batalla del Abismo de Helm es impresionante y contiene algunas de las mejores y más elaboradas escenas de la historia del cine.
Muy recomendada para los fanáticos de las novelas y películas de fantasía.
        
            