
                      'La vida es bella' es una cinta que consigue dividir al público y llevarlo a los dos extremos: o la amas o la odias. Creo que ganan los primeros en número, aunque pienso que depende mucho de la predisposición con la que abordes el film.
Aunque pueda parecer lo contrario, esta película no va sobre el fascismo o sobre nazis, va más allá y trata sobre emociones, miedos, pasiones, etc... Es decir, trata sobre el espíritu humano.

La vida es bella está escrita, dirigida y protagonizada por Roberto Benigni. Fue nominada a siete premios Oscar: mejor película, mejor dirección, mejor actor, mejor guion original, mejor película extranjera, mejor banda sonora, y mejor montaje. Ganó tres de ellos, mejor película extranjera, mejor banda sonora e incomprensiblemente mejor actor. Y en este punto discrepo totalmente con la academia. Ese mismo año estaba nominado (Edward Norton) por “American History X”. Y le dan el Oscar a Benigni? En fin... 
Creo que gasta un histrionismo que le hace insoportable. Me parece lo peor de la película. Todo el tiempo pretende llevarse toda la atención y está muy exagerado y cargante. El resto de actores y actrices cumplen sobradamente con lo que se espera de ellos. Giorgio Cantarini, con tan solo cinco años, consigue emocionar con su actuación, y Dora, interpretada por Nicoletta Braschi, esposa en la vida real de Benigni, tiene un papel más discreto pero más que aceptable.

Podríamos decir que la cinta tiene dos partes claramente diferenciadas. Una primera parte, llena de color, donde nos cuentan una historia de amor y una segunda, mucho más oscura, donde empieza a aflorar el dramatismo.
En definitiva, una joya que se ha convertido en todo un clásico de nuestro cine.
        
            