
                      Laika, depositora de la experiencia y buen hacer de los Will Vinton Studios, se ha caracterizado siempre por realizar películas en stop motion de muy bella factura y cuidada animación basadas en historias originales, convirtiéndose en verdaderos hacedores de cuentos y en una de las productoras más admiradas del momento; una vez tras otra nos ha sorprendido con "Coraline" (2009), "Paranorman" (2012) y "Los Boxtrolls" (2014), y yendo cada vez a más nos brinda hoy la bellísima experiencia que es "Kubo y las Dos Cuerdas Mágicas", que será una de las mejores películas de animación del año.
(...)
Lo mejor: que Laika se esfuerza con cada película para ofrecer una experiencia nueva y arriesgada, y lo logra.
Lo peor: que no haga películas con más frecuencia... hasta ahora ;)
        
            