
                      'Animales nocturnos' es lo que todo amante del Thriller desea ver en la pequeña pantalla, ya que representa al género asombrosamente bien, ya que tiene todos los ingredientes necesarios para adentrarse en su historia y quedar maniatados entre sus idas y venidas.

La película ya nos deja entrever con ese arranque tan peculiar que no estamos ante una cinta cualquiera, sino más bien ante una de las películas más hipnótica que el año 2016 nos ha dado.
Gracias a su atmósfera ideal, que nos hace entrar en su terreno y que nos envuelve en él, la cinta tiene un arranque perfecto. Una puesta en escena sumamente cuidada nos guiará durante toda la trama, la cuál está llena de pequeños detalles que hilan demasiado fino que nos sirven para entremezclar lo irreal con lo ficticio.

Su equilibrio entre lo psicológico, lo perverso y lo ético, es una de las grandes bazas que la película nos ofrece, ya que entremezcla a la perfección una trama con un ritmo irregular pero logra que el espectador mantenga el interés y las ganas de avanzar. 

Las actuaciones son tremendas, con muy buen trío protagonista, encabezado por Amy Adams, poco puedo decir de ella sus ojos azules tristes trasmiten todas las emociones que tiene que trasmitir su personaje y da fuerza a la fría fotografía, seguido por Jake Gyllenhaal, poco puedo decir de este gran actor que ya le llevamos siguiendo la pista desde que se metía en la piel del joven Donnie Darko, y para terminar Michael Shannon, está increíble. Muy merecida su nominación al Oscar.

En definitiva, podemos decir que Animales nocturnos ha sido la gran olvidada por las académicos, ya que es sin duda una de las mejores películas de su año.
        
            