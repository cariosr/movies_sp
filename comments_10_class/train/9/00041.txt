
                      Inspiradora y mágica. El argumento no es gran cosa, pero los protagonistas (Emma Stone y Ryan Gosling están de 10) y la Banda Sonora son geniales. Una película diferente a lo que se suele ver en los cines, deja muy buen sabor de boca.
        
            