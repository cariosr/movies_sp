
                      Lo bueno:

-Las actuaciones e interpretaciones de los protagonistas son excepcionales. Algunas actuaciones de los personajes secundarios también son buenas.

-La película no aburre en ningún momento y te hace sentir una explosión de emociones

- Los paisajes mostrados son hermosos. Las canciones son muy buenas y muy precisas para cada situación.

-La presencia de narrador crea la un ambiente soñador. Las frases y guiones usados son encantadores y harán que el espectador se enamore más de la película.

- Esta película no intenta ser morbosa.Presenta al tema del amor como algo dulce e inesperado, sin morbo o casi nada de ello a diferencia de lo que muchos pensarían al conocer su argumento. 

Lo malo:

- La chica que interpreta al personaje principal (Nikki) no aparente tener la edad de 17 años, sino mucho más.

-Presenta una historia de amor de una forma que puede resultar irreal (enamoramiento entre un hombre 37 y una chica de 17). Sin embargo, la frescura de los personajes hace que el espectador se comprometa en la historia y se llene de su encanto

-La mayoría de las actuaciones de los personajes secundarios son malas. Uno que otro se salva, pero contados con los dedos.

-Las historias de amor entre los personajes secundarios no resaltan en la película y pareciera que en vez de englobar a la historia de amor entre los protagonistas, la estorban o parecieran innecesarias.

CONCLUSIÓN:

Esta historia tiene un toque de picardía,ternura,pasión,encanto. Es una de las mejores películas de amor que he visto
Ningún adicto a las películas de amor debe dejar de verla.
Los que no se convencen fácilmente con las películas de amor podrán tener 2 reacciones: Les parecerá entretenida o les resultará empalagosa.
        
            