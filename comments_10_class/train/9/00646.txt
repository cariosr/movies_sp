
                      Me parece injustamente criticada en general. Magnifica película en todos los sentidos y destacando la magistral interpretación de Meryl que demuestra ser camaleónica y oscarizable. Es un estudio introspectivo de una mujer extraordinaria y no un análisis de su trayectoria política, que parece que es lo que la gente esperaba.
        
            