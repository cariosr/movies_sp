
                      Es una película fantástica en todos los sentidos de la palabra. La historia es apasionante. Sus imágenes son de las que perduran en la memoria, lo cual es un logro que muy pocos directores de cine consiguen. Una obra maestra no sólo del cine japones, sino del séptimo arte en general, de esas películas que hacen que consideremos el cinematógrafo un arte y no una chorrada para entreternos.
        
            