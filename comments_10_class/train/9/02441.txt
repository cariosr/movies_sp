
                      Excelente película. No esperéis ver una biografía, que narra la evolución de un genio. Para eso está la de Aston.

En esta película se describe la personalidad de Jobs y como se define un visionario. Entusiasmo, perseverancia, capacidad de seguir creyendo en uno mismo y la fe en uno mismo. Perfeccionismo y contancia del genio de nuestra época. 

La película carece de acción y dinamismo. Pero si no esperáis nada y la idea es centrarse en los valores que definen y modelan a un genio, la interpretación de Frassbender y un guión sensacional, hacen de la película una excelente opción que junto con la de Aston, nos da una idea muy fidedigna de la vida y carácter de este genio.
        
            