
                      Su aparente "sencillez" hace que su belleza se exponga de forma más penetrante, lo que provocará que el espectador sienta la angustia del protagonista en cualquiera de sus tres etapas. La sustitución del desarrollo de ciertas cuestiones dramáticas por un lirismo intimista transmite una intensidad en la narración que resultará difícil de olvidar. Además, el autor ahonda en la identidad afroamericana y logra una gran descripción social que resulta necesaria actualmente en la gran pantalla.
        
            