
                      La película es muy buena, los efectos especiales son impresionantes y a cualquiera le puede sorprender;
aunque es una nueva trilogía tiene mucho parecido a la primera película de la saga, alguno de los parecidos es la canción, la entrada del parque entre otros.

Cuando uno esta en la sala de cine y se abren las puertas del parque, uno entre en su infancia y recuerda lo que paso 22 años antes, es algo muy emotivo.
También una de las partes en las que a uno se les ponen los pelos de punta es cuando se escucha el característico  rugido del T.Rex al final del film.
        
            