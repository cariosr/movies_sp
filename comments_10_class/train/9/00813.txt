
                      Visualmente preciosa, clara muestra en su momento del potencial de Pixar (toda una realidad hoy), la aventura de estos peces payaso y su amiga (una pez cirujano) es un viaje espectacular a través del océano lleno de personajes interesantes. Una película vistosa, entretenida, dinámica y emotiva. Una maravilla de la animación que no te cansas de ver.
        
            