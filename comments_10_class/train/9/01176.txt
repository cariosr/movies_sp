
                      Muy buena.  Los dramas cotidianos son tratados sin exageraciones y desde una óptica realista.  Graciosa, a pesar de tocar temas dramáticos, sin caer en la vulgaridad. Retrata un lado que no suele mostrarse de la sociedad norteamericana. Mantiene un ritmo constante, ágil, que mantiene al espectador interesado en todo momento.
        
            