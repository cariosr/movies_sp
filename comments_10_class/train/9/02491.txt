
                      Poco hay que decir sobre esta película, que recordemos catapulto la carrera de Christopher Nolan, una trama que se cuenta desde el final hasta en comienzo de la aventura del protagonista y con giros narrativos que te van a dejar confuso. El guion es muy bueno, la ambientación no se queda atrás y el trabajo actoral es excelente. Carrie Anne Moss en una actuación brillante asi como Guy Pearce, el actor que no me termino de convencer fue Joe Pantoliano. Pero de todas formas es una película muy digna de ver.
        
            