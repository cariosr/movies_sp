
                      Un señor thriller de pies a cabeza, con Ryan Gosling haciendo la interpretación más frívola de su carrera, con una elegancia narrativa sobrecogedora, unos secundarios excelentes y una banda sonora para recordar. Winding Refn logra construir un clímax nocturno que recuerda a 'Collateral' y 'Taxi Driver', sin olvidarse, por descontado, del magistral primer plano de casi un minuto al rostro de Gosling.
        
            