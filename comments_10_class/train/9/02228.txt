
                      Una historia con magia, de esas que te dejan con un buen sabor de boca al salir del cine. No sé si será el buen hacer del elenco que forman parte de esta película (me encanta Gorka Otxoa) o la trama tan bien pensada y mimada que hace mantenerte en vilo, pensando cual será lo próximo en ocurrir (y casi nunca acertaras porque esta película está llena de imprevistos). Totalmente recomendable.
        
            