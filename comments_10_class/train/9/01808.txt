
                      Estamos ante una historia de superación, que traspasa nuestras pantallas, consiguiendo que el espectador se impregne de ese positivismo que desprende Hilary Swank, el mejor papel de su vida en mi opinión. Vemos como a lo largo de todo el largometraje no deja de mostrar ni un segundo su valentía, su entusiasmo, su forma tan positiva de ver la vida, no se viene abajo en ningún momento. Logra transmitir esa alegría, cariño, entusiasmo a Frankie, (Eastwood) quién poco a poco va demostrando que él también tiene un corazoncito y que le importa algo más que una simple pelea.

Million Dollar Baby es una historia de amor sobre una persona atormentada por su falta de comunicación con su hija, que descubre a una hija adoptiva en esa chica obsesionada con dejar huella en el mundo del boxeo. Obtuvo, entre otros premios, 4 Oscar (película, director, actriz principal y actor secundario) y 2 Globos de Oro (actriz dramática y director. La actriz Hilary Swank tuvo que ganar 20 kilos de masa muscular para prepararse para esta cinta.

No quiero entrar en detalles sobre la película porque opino que cuantos menos datos se tengan más se va a disfrutar de ella. Me cautivó desde el primer minuto hasta el último, es de esas historias que no te dejan indiferente, y disfruté de las magníficas interpretaciones de estos tres grandes actores y de una dirección impecable, algo a lo que ya nos tiene acostumbrados Clint Eastwood.
En definitiva, una buena historia muy bien contada y muy bien interpretada que hacen de esta película una maravilla.
        
            