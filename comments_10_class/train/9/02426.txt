
                      La actuación de James McAvoy es magnífica, no imagino otro actor en ese papel, la película cae sobre sus hombros y Anya Taylor-Joy le aporta el misterio, la incertidumbre y ﻿el drama emocional con la historia que carga. El final no es lo esperado pero tiene su significado, además de que está abierto a interpretaciones del espectador.
        
            