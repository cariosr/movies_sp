
                      ¡He aquí una película que me ha encantado de verdad ! Amantes de las peliculas de acción abstenganse...los eventos se suceden con un ritmo más bien lento, pero ello permite apreciar realmente la sinceridad de los sentimientos que se muestran en esta película. Me ha conmovido y emocionado la historia de un hijo con su padre y del nacimiento de una historia de amor. ¡Tengo muchas ganas de volver a verla !
        
            