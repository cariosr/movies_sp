
                      Es una película de terror psicológico, angustioso, no la típica película de sustos tontos a los que Hollywood nos tiene acostumbrados. En ese sentido se nota que no es americana, sino australiana. El desarrollo es perfecto, lento, te da tiempo a establecer relaciones con los personajes, a empatizar con ellos u odiarlos. Inteligente y con una buena evolución. Yo sentí tanta angustia viéndola, tanta ansiedad que no pude dejar de llorar. Fue toda una experiencia. 

El final es digno de debatir, y leyendo las críticas de muchos usuarios me da la impresión de que buscaban algo de terror de consumo rápido, que no hiciera falta masticar, y por eso Babadook los decepcionó.
        
            