
                      Un clásico del cine ya , esa mezcla de romanticismo y de suspenso es increíble , los efectos son alucinanes y la historia de amor de los dos es muy buena y triste (aunque toda la primera parte romántica de la película se me hace un poco cursi y aburrida) , la última parte es lo mejor y verdaderamente emocionante , una joya.
        
            