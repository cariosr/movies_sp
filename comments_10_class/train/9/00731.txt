
                      Este Tintín de Spielberg es un entretenimiento muy bien orquestado. El espíritu del pequeño héroe queda intacto, SS da a esta historia un tempo moderno con una 3D eficaz pero no mareante. En algunos momentos, podríamos llegar a pensar que IJ ha tomado posesión del cuerpo de Tintín. Resulta ser muy buena sorpresa. Corred a verlo !
        
            