
                      En mi opinión, una película redonda. Es como vivir aquel horrible momento en primera persona compartiendo las emociones, las peores y las mejores con esos chicos que nuestra sociedad llama niños.
        
            