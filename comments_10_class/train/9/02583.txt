
                      Una de las mejores películas de Drácula de la historia, por no decir la mejor. En mi opinión, es una adaptación casi perfecta de la obra de Bram Stoker. En esta película vemos cómo progresivamente se produce una humanización del personaje de Drácula, mostrándonoslo como un villano con matices, mientras que los cazadores de Drácula como Johnathan Harker o Abraham Van Helsing terminan perdiendo parte de su humanidad en la lucha contra el Rey de los vampiros.

La dirección de Francis Ford Coppola es sublime, tan buena que consigue repartir los distintos personajes, de forma que no tenemos únicamente a uno o dos personajes que catalogar como protagonistas, sino que funciona como una película hasta cierto punto coral donde todos aportan. Creo sinceramente que era la mejor manera de adaptar la novela de Drácula, que quizá sea una de las primeras novelas río de la historia.

En lo referente a las actuaciones, Johnathan Harker es encarnado por Keanu Reeves, que lo hace bastante bien. Por desgracia, su personaje, que en un primer momento parece que será el absoluto protagonista, acaba perdiendo bastante protagonismo, pero esto no deja de ser algo que también ocurre en la novela. Wynona Rider está fabulosa como Mina Harker, la prometida de Johnathan, encarnando a la que sin lugar a dudas es la mejor Mina del cine. 
Anthony Hopkins interpreta de manera sublime al cazador de vampiros Abraham Van Helsing, dotándolo de un carisma y una vitalidad increíbles. 

Aunque sin lugar a dudas, quien se come la película es Gary Oldman encarnando a Drácula de una forma absolutamente increíble, tanto en su forma de anciano conde como de extranjero seductor. Una pena que este actor esté tan infravalorado en Hollywood porque por esta película debería haber ganado el Óscar. De hecho, creo que con toda probabilidad Gary Oldman es uno de los tres mejores Dráculas de la historia, no sé si por encima de Bela Lugosi y Christopher Lee, pero sí a la misma altura.
En definitiva, una joya del cine.
        
            