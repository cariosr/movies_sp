
                      No cabe duda de que Marvel continúa imparable en su Universo Cinematográfico.
Doctor Strange fue más allá de mis expectativas, ya que creía muy difícil traer una adaptación de este personaje al cine, mi emoción es tanta, que ya no puedo esperar a ver nuevamente en la pantalla grande a nuestro Hechicero Supremo.

Desde que se confirmó la película, se desató mi emoción por verla en pantalla grande, pues confiaba en que Marvel Studios nos traería a Doctor Strange de una manera nunca antes vista. Y, además, sentí que sería una de las mejores películas de superhéroes en la historia del cine. 

un punto a favor de esta película es que está hecha para todo público, en el sentido de que no se necesita ser un erudito de los cómics para entender lo que está pasando en pantalla, ni tampoco se necesita que seas un conocedor profundo de Doctor Strange para asimilar lo que sucede. Sin duda es una película extraordinaria.

Indudablemente, Benedict Cumberbatch, interpretando a nuestro Doctor Strange, es uno de los mejores actores que ha tenido Marvel Studios en todas sus películas de superhéroes. Naturalmente, Benedict Cumberbatch logró mostrar mediante su actuación, a un Stephen Strange carismático, gracioso y divertido, pero a su vez, logró mostrarnos su lado egoísta y egocéntrico que lo caracteriza.

Debo admitir que Tilda Swinton es de lo mejor en la adaptación, ella interpretando a Ancestral fue de lo más perfecto e impecable de la cinta, sentí que llevó su personaje de la mejor forma posible y que, realmente, en ningún momento logró decepcionarme. La forma en que ella enseña a Stephen Strange sus habilidades místicas, y como logra llevar esas habilidades al máximo, fue de lo más impactante en la película.

Otro punto a favor sobre esta cinta, es el reparto de Mads Mikkelsen como Kaecilius, él me convenció totalmente haciendo el papel de villano en la película, no solo porque es un excelente actor, sino porque logró mostrar a un Kaecilius mucho más elaborado y misterioso durante el rodaje: Un villano con habilidades místicas que intenta exponer al mundo las múltiples amenazas de otras dimensiones.

Dentro de esta película también encontramos escenas visuales muy particulares. Una de ellas es cuando Strange se encuentra con Dormammu en la dimensión oscura y que en mi opinión, fue uno de los escenarios en donde se acercaron más a los cómics, ya que de alguna manera homenajearon a Steve Ditko, el creador del personaje.

Los efectos visuales de la cinta estuvieron excepcionales. Pude observar en numerables ocasiones el gran trabajo de M.C Escher como director de fotografía, junto con la ayuda de Industrial Light & Magic (creada por George Lucas) y Luma Pictures. Gracias a ese gran trabajo detrás del film, lograron mostrarnos escenas extraordinarias y peculiares.

Infortunadamente, pude encontrar algunos detalles en la película que no fueron de mi total agrado, uno de ellos fue que le quitaron protagonismo a Rachel McAdams (Dra. Palmer) en la película. Si bien, se había confirmado que ella sería una de las protagonistas de la historia, sentí que pasó a un segundo plano durante todo el rodaje, y por si fuera poco, se manifiesta durante algunos minutos.

Como sabemos, a Marvel siempre le ha gustado introducir humor en sus rodajes, me encanta ver humor en Ant-Man, Iron Man y Guardianes de la Galaxia, porque son películas muy entretenidas y divertidas; sin embargo, esta no fue la excepción: me pareció acertada la forma en que introdujeron esas características a la película y lograr divertir a su público de esa forma, pero lo que realmente no puedo aceptar es que hayan introducido ese humor en escenas de batalla campal, se supone que son escenas en las cuales se debe sentir adrenalina y emoción, y el interrumpir escenas supremamente importantes como esas con chistes, me parce totalmente absurdo y de mal gusto.

Otro punto importante a resaltar es el gran villano de Doctor Strange en la película: Dormammu. Me pareció muy acertado de parte de Marvel mostrarnos a este poderoso villano de una forma muy bien elaborada, pero sentí que definitivamente le arrebataron sus poderes, me hubiese encantado ver a estos dos personajes luchando casi hasta la muerte, e intentando destruirse entre sí, después de todo, Dormammu es el archienemigo definitivo que tiene Strange por excelencia, y que además es extremadamente oscuro y poderoso, pero sentí que este villano pasó a ser alguien irrelevante y común, como cualquier otro, y eso extinguió mis emociones completamente.

En conclusión, Marvel revolucionó totalmente lo que estábamos acostumbrados a ver antes en una película de superhéroes, me parece que Doctor Strange es un total acierto en todo el UCM y del cine en general, ya que traer a un personaje tan complicado como Doctor Strange a la pantalla grande, no es tan fácil como parece. Gracias a Marvel Studios, el cine de superhéroes ha venido creciendo de una manera excelente, y siento que Marvel logró reafirmar su universo cinematográfico con esta adaptación.
        
            