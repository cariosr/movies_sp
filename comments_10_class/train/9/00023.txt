
                      La última película de la "trilogía del dolar" o "trilogía del hombre sin nombre" es la mejor de las tres, el director Sergio Leone nos muestra un film muy profundo, con una dosis de humor negro y con más violencia que en las anteriores. La banda sonora, como todas las de Ennio Morricone, es una auténtica obra maestra. Los tres protagonistas principales, Clint Eastwood, Eli Wallach y Lee Van Cleef, realizan unas interpretaciones estupendas.
        
            