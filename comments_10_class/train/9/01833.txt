
                      Para aquellos que hemos mamado la ciencia ficción directamente de la tinta de los libros escritos por los maestros del género; para todos aquellos que al oír las palabras "Ciencia Ficción" nos viene a la mente  "2001, una odisea espacial" o  "Blade Runner" y no "La Guerra de las galaxias", y para todos aquellos que no creemos en un cine que mide su valía en proporción a los ingresos ni al número de espectadores, " La Llegada" supone un soplo de esperanza, una burbuja de oxígeno y una revitalización de nuestra fe en el género.

Insisto, si esperas algo medianamente parecido a "Indepence Day", huye, ni te acerques a esta película, te puede dar un colapso o algo peor.

Denis Villeneuve es uno de esos directores que con una corta carrera a sus espaldas están envueltos en un halo de respetabilidad. Que resultan convincentes. Que gozan del beneplácito de crítica, industria y público. Supongo que aún tiene que afianzar todo eso que se le está concediendo hasta la fecha, pero desde luego va muy bien encaminado.

"La Llegada" es una película de "primer contacto" con civilizaciones alienígenas. Al menos en apariencia, ya que como cualquier buen producto de ciencia ficción, el propósito final va mucho más allá.

Lo que la diferencia de sus muchas otras predecesoras es, sobre todo, Denis Villeneuve; o lo que es lo mismo, un director con la capacidad y sobre todo la intención de crear una obra personal, con un estilo propio e identificable, sin ceder a las tentaciones de la autocomplacencia o a las presiones mercantilistas de las productoras. 

Lo primero de lo que es consciente este señor, es de que tiene una buena historia entre manos, y a pesar de alguna pequeña licencia, sobre todo en su parte final, no se deja arrastrar por los derroteros facilones de la acción por la acción, del melodrama sensiblero, del folletín pseudo-filosófico o del panfleto patriotero. 

Se esfuerza a lo largo de toda la cinta por mantenerse dentro de sus propios límites autoinfringidos y no desviarse por esas tangentes tan peligrosas, y consigue centrase exactamente en lo que nos quiere contar, sin distracciones, sin trampas y sin adornos.

Lo siguiente de lo que es consciente es que tenemos ya tal empacho de desparrames digitales grandilocuentes, que la única forma de sorprender es con un diseño austero, frío y eficiente que se convierte en el tercer gran protagonista de la película. Capta toda nuestra atención y se erige en protagonista al retratar lo extraterreste, dosificándose, haciéndose desear; intrigando, desvinculándonos del mundo exterior; etéreo místico y afuncional. Un diseño que se torna físico, palpable, reconocible y funcional al tratar lo terrestre.

Sí, el diseño de producción es el tercer gran protagonista, porque la segunda, tras la ya mencionada coherencia narrativa, es  Amy Adams, que se hace dueña absoluta de la película desde su brutal arranque hasta su excesivamente subrayado final. 

Ella es sobre quien recae todo el peso actoral, y no porque el resto del reparto sea fallido. sino todo lo contrario: porque son meticulosamente eficientes en cumplir su función, que es dar solvencia al personaje de la doctora Banks. Porque toda la película se centra en el punto de vista de su protagonista, encarnado por la brillante Adams, dejando de lado, una vez más, todo lo superfluo y las variantes inútiles.

"La llegada" es una película directa y arriesgada que habría acabado encallada en alguno de los fangos mercantilistas que antes hemos mencionado, de no ser por la dirección férrea de Villeneuve, que hace lo que cualquier buen director que se precie: dar coherencia a todo el conjunto. 

Él es quien imprime carácter a la cinta, quien decide el ritmo y el tono. Opta, como en todas sus cintas, por un ritmo pausado, que no lento. Porque señores, lenta es una película que no avanza, y ésta no deja de avanzar. 

Arranca con un prólogo brutal que nos sitúa de lleno en el quid de la historia y se toma su tiempo, como la lingüista encarnada por Amy Adams, para hacernos entender cada fragmento de la historia como si fuera un vocablo, utilizando esos mismos tiempos largos para crear un estado de tensión e incertidumbre que no decae en ningún momento, e intercalando recuerdos para que no perdamos en ningún momento la perspectiva de lo que nos está contando, que es precisamente esa llegada, aunque...¿la llegada de quién?

Y es aquí donde le hago el único reproche que le puedo hacer a Villeneuve, que es cargar las tintas, subrayar y recalcar en exceso ese final perfecto, esa llegada que todos sabemos ya cuál es y estirarlo en demasía, como si no confiase suficientemenete en nuestra inteligencia. Pero eso una nimiedad en una película tan estimulante, tan inspirada, tan imprescindible y tan certera como ésta. Una película que en mi caso cumple no sólo uno, sino los tres requisitos que pido al cine: entretener, sorprender, emocionar.

Y no sólo nos hace no perder la fe en que el buen cine de ciencia ficción no ha sucumbido definitivamente a directores incapaces, egos desorbitados y beodas concesiones mercantilistas, sino que nos hace temer un poco menos esa segunda entrega de Blade Runner al saber que ha caído en manos del señor Villeneuve.
        
            