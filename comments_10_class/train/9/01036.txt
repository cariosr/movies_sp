
                      Es una película que hay que ver, independiente de la creencia ateísta o espiritual nos muestra el respeto mutuo que debe existir entre ciencia y religión, porque nos guste o no nada es concluyente en este momento.
Creo que el tema que toca es absolutamente contemporáneo, y me gusta la magia que el director imprime a tópicos tan trascendentales como la tolerancia y la búsqueda de la verdad universal. Encontré que la película era simple y profunda.
        
            