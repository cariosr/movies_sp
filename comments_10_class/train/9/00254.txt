
                      Es la mejor película de la llamada trilogía de la venganza, por mucho que Old Boy sea mucho más famosa. Ésta tiene mucha más enjundia y está mejor resuelta, aunque a veces al director se le vaya un poco la mano, en especial en las escenas que pretenden ser conmovedoras y que resultan un poco gazmoñas. Pero el desarrollo del dilema moral de la última media hora, que bordea el ridículo a veces, acaba resultando sublime. Una pequeña joya.
        
            