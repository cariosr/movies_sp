
                      Partiendo de que nunca me ha gustado mezclar el género de acción con el de humor, iba a ver esta película sin grandes pretensiones, aunque con ciertas ganas después de leer sus tremendas críticas y, efectivamente, me sorprendió gratamente hasta convertirse en una de mis películas favoritas de Marvel.
La primera parte, que corresponde a la introducción, siempre es la que más pesada se me hace, pero aquí todo pasa rápidamente, amenizado con esos toques de humor, en forma de puntos, que están tan bien conseguidos y puestos en su lugar adecuado que es difícil que no se escape una carcajada generalizada en la sala.
Cuando nos ponemos a hablar de los efectos especiales, hay que decir que son espectaculares, al nivel que requiere una buena cinta de ciencia ficción de estas características.
Para muchos de nosotros, la BSO nos traerá grandes recuerdos, eligiendo unos buenísimos temas, todos ellos partiendo de una cinta de casette que a más de uno le gustaría tener entre las manos.
Creo que sin duda estamos ante una de las mejores creaciones de la factoría Marvel y en la que demuestra que sabe explotar todos los campos posibles a la perfección.
        
            