
                      Danzón     
La película Danzón se trata de una mujer enamorada con el baile del Danzón que hace todo posible para lograr su sueño. 
La directora María Novaro es una directora mexicana y fue una de las primeras mujeres en graduarse de la escuela de cine en México. Ha sido directora de cinco otras películas y catorce películas cortas. Usualmente escribe sobre el feminismo y esta película hace un buen trabajo en enseñarlo con la historia de Julia. 
En toda la película se ven las formas de cómo se baila el danzón y las tradiciones de la gente, formas de vestimenta y como se hablan entre ellos es muy diferente comparado a la de nosotros y Maria Novaro nos enseña eso. 
Recomiendo ver esta película si quieres aprender sobre una nueva cultura y si en realmente estas interesado en el danzón pero antes de verlo tienes que saber que no te va a enseñar cómo bailar el danzón, solamente te va a dar visuales de como se baila. Te habla sobre la aventura de Julia y como el danzón cambió su vida.
        
            