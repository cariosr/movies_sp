
                      Entre obra contemplativa y documental metódico, este lugar campirano se presta con humildad a todas las referencias y complejidades de interpretación. Cualquiera que sea su humor, no podemos más que colocar muy alto las fuertes impresiones sentidas a lo largo de las cuatro etapas de naturaleza generosa y de vida auténtica.
        
            