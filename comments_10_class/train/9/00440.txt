
                      Me ha llamado mucho la atención que simula estar rodada sin cortes, se va moviendo la cámara por todos lados sin ningún salto. Esto quizás atrapa más en la película y hace seguir el ritmo, sin apenas bajadas. Ciñéndose a la película en sí, es muy entretenida y no se hace nada larga, es un drama con algún toque cómico que engancha.
        
            