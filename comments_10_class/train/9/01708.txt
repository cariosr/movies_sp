
                      Tiempos Modernos es una comedia de crítica social cuya dirección, producción, guión y música están a cargo de Charles Chaplin.

La película narra las dificultades de los trabajadores de una fabrica de montaje en cadena cuando esta cierra.
El protagonista pasa por una serie de sucesos desafortunados que lo llevarán hasta la hija de uno de sus compañeros de trabajo en la fábrica.

A pesar de que ya existía el cine con audio, Charlie Chaplin continuaba con el cine mudo, y la verdad es que no necesita de las voces de los protagonistas para ser entretenida.
Además de destacar como actor, en esta película, Chaplin se luce como director, ya que se trata de un filme complejo.
Es considerada una de sus mejores películas.

La última escena en la que se ve a los dos protagonistas, es un resumen muy hermoso de la película; tener siempre una visión positiva frente a situaciones difíciles.
        
            