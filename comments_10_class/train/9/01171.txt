
                      Empezaré diciendo de esta WONDER WOMAN de Patty Jenkins que es una buena película. Así, sin anestesia. Y tengo pruebas: al acabar la proyección, una pandilla de quinceañeros se levantaron (vociferando,a su estilo)..."Claro, es que esto no es Marvel".
¿hay alguna mejor evidencia de que estamos ante algo bueno? 
Efectivamente, no es Marvel ni se le parece
Entendámonos, desde mi punto de vista, Marvel tiene alguna buena película (las menos). Y no tiene más porque no quiere, se ha especializado en producto estandarizado y clonado de otros productos estandarizados y clonados de su propia factoría, en su mayoría comprimidas al gusto de la masa adolescente devorapalomitas (de la cual no reniego, porque todos hemos pasado esa fase). 

Recordemos que fue DC quien inventó los cómics de superhéroes y que Marvel se sumó al carro. Lo hizo con vocación más "teenager" y más "social". Una especie de democratización de la historieta gráfica; un poco como la Coca Cola y la Pepsi.

DC en vez de renegar de él,  ha conservado y convertido en marca de la casa ese toque irreal de historieta ihnerente al mundo de los superhéroes. Un toque deliberadamente camp y anticuado sin el cual estos "metahumanos" carecen de sentido. Marvel opta por convertirlos en ciudadanos de nuestro mundo.
Dos caminos distintos, dos elecciones que no voy a juzgar, aunque sí elijo cuál de ellas me satisface más.

Aclarado ésto, Wonder woman sorprende por algo que debería ser lo normal en cualquier película (y no lo suele ser): deja ese agradable regusto de cuando te han contado bien una historia, y además entretiene.

Sorprende primero, por lo distinta que es al resto de películas del género y por su marcada personalidad. Patty Jenkins la dirige con convicción y sin un solo titubeo, como si lo que nos está contando fuera su propia historia. Una linea argumental clara, sencilla, directa y con buena narrativa. Un guión consistente, bien dosificado, bien dirigido y sin demasiadas concesiones se suma a muchos otros aciertos para construir una película con alma, con vida propia, con personalidad, con fuerza, contundente y sencilla.

Gal Gadot dota de carne y de alma a su personaje con convicción. Se transforma en esa pieza discordante de otro mundo recién aterrizada en éste. Una extraña llena de humanidad, compasión e inocencia. Como una niña pequeña que cuestiona desde su pureza todos aquellos convencionalismos e incongruencias que hemos asumido como normales en nuestra hipócrita sociedad. Quizás sea la elección de ésta actriz el mayor acierto de casting en mucho tiempo y la clave para que una película tan arriesgada funcione tan bien. 
Sería prácticamente imposible imaginar a Diana sin la cara de Gal Gadot, al igual que se hace casi imposible pensar en Superman sin Christopher Reeve bajo su capa, en catwoman sin Michele Pfeiffer o en el Capitán América sin Chris Evans
Un personaje totalmente ajeno a nuestro mundo, que convive en un siglo distinto al nuestro, con su moral y sus costumbres, y que nos tiene que resultar verosimil con los ojos de hoy.. Algo que ya consiguió la estupenda "El Capitán América: el primer Vengador", y que le da otra vuelta de tuerca esta Wonder Woman. 

Gal Gadot/Diana no es un personaje movido por el odio, la ira, la sed de venganza...ni siquiera por un trauma infantil: es la convicción lo que la mueve. Ella cree en su misión, porque ha sido criada y educada en la justicia más primaria. Actúa porque no puede no hacerlo. Toda la película se convierte en un viaje de crecimiento personal y de descubrimiento de quién es en realidad. Patty Jenkins sabe que esa oportunidad no se volverá a presentar en futuras entregas y muy acertadamente lo aprovecha y lo convierte en el motor de la película. Si ya en la estupenda e incomprendida "Batman contra Superman" Gal Gadot robaba la película, en esta Wonder Woman afianza una larga existencia. 

El hecho de que sea una mujer quien dirige esta película hace que muchos detalles que un hombre hubiera pasado por alto, o hubiera considerado irrelevantes para entender la psicología del personaje, tomen aquí especial importancia, dotándole de aún mayor cercanía y humanidad.

La prevalencia de la historia sobre la espectacularidad y los fuegos de artificio (que los hay) no cede (casi) en ningún momento. Un argumento sencillo que en vez de ciclarse con efectos y acción huecos se afana en dotar de vida a sus personajes. Se toma tiempo para darles textura, para tejer vínculos, para que lo mucho o poco que narre la historia, lo narre bien, de manera directa, sin flecos, sin ramificaciones. 

Se permite escenas apoyadas sólo en diálogos. Diálogos cuidados, llenos de humanidad, de ironía, de dobles sentidos y de un delicadísimo sentido del humor. Es precisamente en esta escenas donde la película crece y nos gana.

¿Por qué funciona tan bien una historia tan "antigua" y tan poco verosímil? 
Honestidad. Como dice mi amigo Álvaro: " es moderna porque es antigua"
No se ha querido cambiar el origen de esta guerrera para adaptarla a estos tiempos que corren, ni el estilo visual que le correspondería a su época y su condición. Camina siempre a un paso del abismo de lo absurdo, de lo cursi y de lo hortera, pero lo hace con decisión que nunca se despeña.. 

Hay un concepto llamado "verosimilitud", que es ese margen de credibilidad que gratuitamente debemos aportar los espectadores a una historia, a todas luces imposible, para que funcione y sea convincente. Un concepto que nos hace cómplices de la película siempre y cuando sepan construir en ese espacio cedido un todo coherente. Se tiende a no respetar los límites de ese espacio cedido, se titubea o simplemente se nos quiere engañar. No es el caso, más bien al contrario. Todo aquí es imposible, pero coherente.
. 
Hay deliciosos guiños a la madre de todas las películas de superhéroes: "SUPERMAN" de Richard Donner y al único Clark Kent creíble hasta el momento encarnado por Christopher Reeve, incluída la escena del callejón, la puerta giratoria o las mismísimas gafas sello de la casa.

Buenas interpretaciones y buena dirección de actores. Robin Wright en un papel corto, pero que ella sabe hacer grande. Connie Nielsen idem de idem. Chris Pine convence sin necesidad de alardes, Elena Anaya (sí, la nuestra) construye una villana como recién arrancada de una viñeta, Lucy Davis da un imprescindible contrapunto cómico y ...Bueno, no voy a desvelar quién es el auténtico villano.

Por cierto, allí donde la mayoría de las películas de superhéroes fracasan, que es el enfrentamiento final con el malo malísimo (bien insultando y menospreciando nuestra inteligencia con una solución precipitada y absurda, o bien confiando en que los adolescentes (que son quienes suelen importarles) se suelen conformar con unas cuantas lucecitas acompañadas de música estridente), Wonder Woman vuelve a ganar la batalla con un final espectacular y brillante pero con sentido, con emoción y sin trampas. Aunque he de reconocer que los planos de concordia fraternal entre los soldados me chirrían un poco bastante.

Como nota final, si bien la banda sonora no es especialmente destacable (sí lo son la dirección de fotografía y la dirección artística), su mayor logro es haber dotado al personaje de su propio tema musical, tan identificable como el de Superman, o el de Batman. De hecho, creo que estos tres son los únicos que lo tienen. 

¿Recomendable? al cien por cien. Sobre todo para aquellos que creemos que el cine de superhéroes, el comercial y el cine de verdad no tienen por qué ser cosas distintas.
        
            