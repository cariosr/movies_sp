
                      Alien es uno de los grandes clásicos de la ciencia ficción, en una lograda combinación con cine de terror, para crear a una de las criaturas repugnantes más icónicas del cine. 
La dirección de Ridley Scott es sublime, el guión es lento y pausado pero logra mantener el interés durante toda la película, la atmósfera está muy conseguida, y el diseño del Alien es increíble e inquietante a partes iguales.

Uno de los puntos más a favor de Alien es que los protagonistas, la tripulación de la nave Nostromo, ni son héroes, ni tienen armas de destrucción masiva, ni son luchadores natos, son simplemente unos comerciantes de mineral, que se dedican a realizar un trabajo normal y corriente por un salario aceptable. 
Un lugar extraño y hostil, con una criatura impredecible y terrorífica que supone una amenaza acojonante y tú encerrado en una nave espacial.

La actuación de Sigourney Weaver en la piel de la heroína Ellen Ripley hace un papel magistral que se quedará grabado por siempre en el imaginario colectivo. Por algo fue su mayor empujón en su carrera como actriz, hasta hacerse oír por todo Hollywood. La escena del Revientapechos saliendo de Kane es también una actuación excelente por parte de todo el elenco.

Los efectos especiales son muy buenos estamos hablando de una película de 1979... Las maquetas de la Nostromo y los planos de la nave abandonada encontrada en el planeta LVL 426 son muy realistas. Los pasillos de la Nostromo son claustrofóbicos. Mucho detalle en todos los decorados.
En definitiva una joya de nuestro cine.
        
            