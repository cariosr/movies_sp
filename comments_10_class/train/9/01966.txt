
                      En busca de la felicidad es un drama biográfico basado en la historia de Chris Gardner, un hombre que luchó a favor de las personas sin hogar en Estados Unidos a principios de los 80. La película está dirigida por Gabriele Muccino y Fabricio Samaniego.

Chris Gadner, interpretado por Will Smith, se dedica a vender escáneres óseos por las consultas médicas de la ciudad. Ha invertido todos sus ahorros en estos aparatos, y debe venderlos todos.
Al no tener éxito, la familia queda en la ruina, y su mujer se va de casa, dejando a Chris con su hijo pequeño (Jaden Smith).

Will Smith está impresionante en este drama. Aquí demostró que no es solo un actor de acción, y que es capaz de más cosas.
Es la primera vez que aparece en la gran pantalla con su hijo mediano, Jaden, un chico con talento.
La relación que tienen padre e hijo es muy tierna. Ambos pasan por muchas dificultades, y debido a la corta edad de su hijo, Chris camufla la realidad para él.
Los personajes sufren una desgracia tras otra, pero eso no le impide al padre luchar por brindarle una vida mejor a su hijo.

La película solo recibió cuatro candidaturas, tres de ellas para Will Smith; Oscar, Globo de Oro y Premio del Sindicato de Actores. Con el paso de los años se ha reconocido que el largometraje fue poco valorado cuando se estrenó, y que Will era merecedor de los premios.
        
            