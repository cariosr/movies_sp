
                      Todo fluye, tiene ritmo y alma en esta deliciosa película. Te admira el virtuosismo técnico con el que está realizada, su fuerza visual, su perfeccionismo, pero ese lenguaje no está al servicio de una historia trivial o destinada exclusivamente a los espectadores infantiles, sino que puede ser paladeada por los adultos. Lo que cuenta es poético, posee el aroma de los mejores relatos, te hace reír, te fascina, te envuelve. Es una película familiar simplemente perfecta. Los 10 primeros minutos son arte.
        
            