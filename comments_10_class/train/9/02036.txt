
                      TARANTINO EN MAYÚSCULAS: peli muy divertida, con guiños a los mejores western (desde un cameo de Franco Nero hasta música "Taranterizada" den gran Morricone). Son casi tres horas de film que pasan en un suspiro. Hay ciertos planos / escenas / diálogos de auténtica genialidad, la interpretación de Fox es muy sólida, oportuna y bestial, pero la de Waltz es IMPRESIONANTE (posiblemente le den su segundo Óscar... Otra vez de la mano de Tarantino).
        
            