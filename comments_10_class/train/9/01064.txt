
                      Visualmente espectacular, escenografías brillantes, un personaje protagonista divertido y carismático y una historia con ese tinte nostálgico del cine oriental de kung-fú hace de Kung Fu Panda una verdadera delicia para disfrute de pequeños y mayores. Si a eso añadimos el elenco de voces originales (en España Flo y Nuria Mediavilla, entre otr@s), estamos ante uno de los productos más acertados de Dreamworks.
        
            