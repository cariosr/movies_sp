
                      Es un tema un tanto trillado: el individuo que, atrapado en un coche, un ascensor, una cabina de teléfono e incluso en un ataúd, es chantajeado por el villano cuestión normalmente para reclamar dinero o simplemente por venganza. Ahora encima le añadimos que está hecha en nuestro país y a más de uno (no lo negamos) le echará para atrás. Pero no. A “El desconocido” no se le puede meter en el mismo saco. A “El desconocido” no.

Una mañana, mientras lleva a sus hijos al colegio, un ejecutivo de un banco es amenazado con hacer explotar su coche si alguien sale de él.

Como he dicho antes, esta película no se la puede incluir en el grupo de películas de temática similar a la que pertenece. “El desconocido” tiene algo que la diferencia del resto. Y es que, la ópera prima de Dani de la Torre, posee un grado de adición importante. Como una montaña rusa en la que no tenemos subidas y bajadas. Aquí tenemos diez minutos de subida y a continuación su hora y media restante de caída llena de adrenalina. Pues cuando pensabas que la siguiente escena que viene te dará un pequeño respiro, sucede algo que te vuelve que poner el corazón en un puño. Escena tras escena en la que no podrás ni dejar de mirar, ni de morderte las uñas, ni mucho menos podrás pararte a pensar. Todo con una dirección exquisita, unos planos a vista de pájaro (muy a lo Google Maps) que son una verdadera delicia y unos movimientos de cámara realmente apabullantes. Tan solo la interpretación de un flojo Luis Tosar puede dejar algo que desear, pero que se suplanta por los magistrales hijos de su personaje que dejan boquiabiertos a cualquiera y que llegan a nuestro cine pisando fuerte.

Un thriller de suspense intenso, atroz, potente, vivo, un soplo de aire fresco. Pero sobre todo es un puntito en la boca a todos aquellos que piensan que el cine español no es capaz de hacer de una manera tan magistral, este tipo de cine.

Lo mejor: el personaje de Belén, jefa de artificieros.
Lo peor: ya sabiendo que la película tendrá un giro final, hubieramos querido que fuera más drástico e inesperado para el espectador.
        
            