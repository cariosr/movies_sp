
                      Nightcrawler ó El ojo público psicodigital

Muchos paralelismos con la película de 1992 El ojo público  del director Howard Franklin, Joe Pesci y Barbara Hershey actores principales. 
En este caso Jake Gyllenhaal le da el punto electrizante al personaje que hace que la película enganche y tenga alta intensidad de principio a fin. La naturaleza delictiva del protagonista hace que nos esperemos del argumento tan bien estructurado casi siempre, cualquier giro hacia el lado más oscuro. 
En momentos el trabajo de Gyllenhaal es tan bueno que da que pensar si es un tio desequilibrado en la vida real o es sólo interpretación. 
Rene Russo, la deseada. En este caso secundaria de lujo.  Mujer madura con el codigo moral algo estropeado,  le da la réplica al protagonista creando en algún momento una tensión sexual que en un principio parece irresoluble pero que a lo largo de la película se hace más probable y deseada. 
Alguna escena en la que interactúan los dos realmente funciona tan bien que apetecen unos minutos más de metraje dedicados a ellos dos solos. 
La película ambientada casi toda de noche tiene una extraña atmósfera de lo más particular, Dan Gilroy y el director de fotografía Robert Elswit hacen con esto un thriller con mucha personalidad y originalidad. 
Merece la pena verla sin dudarlo. Original y extraña,  escenas de acción muy bien hechas y buena interpretación.
        
            