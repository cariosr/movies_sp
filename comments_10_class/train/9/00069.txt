
                      Gran película con estupenda historia. Engancha desde el primer minuto. La  historia es espectacular y cuenta con momentos muy emotivos. A pesar de ser cruel no muestra escenas demasiado duras o sangrientas como se podría esperar. Muy bien llevada. El único pero quizá sea el final, que deja un sabor incompleto.
        
            