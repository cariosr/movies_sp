
                      Es inevitable no mencionar que "Fifty Shades of Grey" es una de las mejores películas del año 2015. No fue tan doloroso como todos pensaban.

Sin lugar a dudas "Jurassic World", "Star Wars:The Force Awakens", "Avengers: Age of Ultron" y "Fast & Furious 7" marcaran trayectoria en la taquilla y en la critica general, pero este año "Fifty Shades of Grey" impulsado por su lanzamiento el Día de San Valentín, ha hecho récord.

A nivel mundial, esta película ha atraído la atención de la audiencia por la extraña historia de amor entre la estudiante de Literatura Inglesa Anastasia Steele y el empresario multimillonario Christian Grey.

La trilogía erótica de libros dejó sin palabras a todo el mundo y la película no se quedó atrás, Su calidad para capturar cada detalle del libro en la vida real fue lo más atractivo logrando atraer la atención de los aficionados y no aficionados del Fan-fiction.

Dakota Johnson, hija de los actores Don Johnson y Melanie Griffith, ella estaba complacida de ejecutar el personaje de Anastasia Steele y se noto su entusiasmo, lo ha hecho de una manera perfecta, mostrando la virginidad, la inocencia y la dulzura de una mujer que nunca había estado en el amor, que estaba a punto de graduarse, que tenía la presión de los exámenes finales, que tenia un compañero (Jose) que la acosaba y estaba a punto de ir a un mundo lleno de peligros, sola.

Jamie Dornan, después de actuar como un asesino en la serie de BBC Two - "The Falll",  se presenta como un reemplazo del primer electo Charlie Hunnam. La pregunta es: ¿Podría haberlo hecho mucho mejor Charlie como Christian? Lo Dudo. Jamie hizo una labor ardua y difícil, que fue alcanzar plenamente el personaje y eso fue evidente.

De manera significativa, la relación Anastasia - Christian fue moderada y estrechamente vinculada a los terminos del Best-seller, pero la relación Dakota - Jamie nos hace dudar. Según especulaciones afirman que las dos estrellas apenas pueden soportar la compañía del otro. Así Taylor - Johnson respondió abiertamente:
" The film's biggest single asset is Dakota Johnson: gone is the book's blithering simpleton " 

Curiosamente, a pesar de la controversia entre Sam y James, la baja confianza del público en este proyecto, la baja afinidad aparente entre los personajes principales y la dificultad de llevar a la pantalla un libro que revolucionó el mundo, "Fifty Shades of Grey" logro ser una de las mejores películas del 2015, por lo que "Fifty Shades Darker" (2017) no tendrá un camino fácil porque, Sam no va a dirigir la secuela, sera James Foley quien estará en la silla del director.

El atractivo más grande de la película es Johnson, que trabajo duro con Marcel y Taylor-Johnson para realizar un trabajo de rescate entre tres mujeres para el personaje de Anastasia. Un aspecto curioso es que en la película no se evidencia ni una sola vez la tan mencionada frase de "La Diosa que Llevo Dentro" y tal vez fue mucho mejor córtala para darle un poco mas de seguridad y fuerza al personaje de Dakota. Ella en ningún momento presenta un temblor, un lío húmedo, y no hace el error ruinoso de pensar en la palabra "mie___" porque realmente es un epíteto. También omiten (Y gracias a Dios que lo hicieron) la escena mas gráfica del libro en donde Christian interactua con un tampon de Anastasia, Todos en el film coincidieron en que la escena no encajaba en el perfil que querían realizar de la película.

Grey, por razones obvias, es mucho más admirable que Anastasia por la intimidad y misterio con el que se le ve en los libros.

Personalmente, esta película de principio a fin, fue excelente, mostrando una introducción de un cuento de hadas, un desarrollo totalmente agresivo y un final que nos sorprendió a todos. Los Libros de E. L James son enigmáticamente receptores para el público, pero la película es sin duda lo mejor de lo mejor.

Las escenas de sexo son como un intenso termómetro, que amenaza con llegar al rojo vivo, pero nunca logra llegar a la cima, y ​​con más participación de Dakota que Jamie. Se podría decir que "La Sumisa" desde todos los ángulos capturados está a punto de desnudarse mientras que "El Amo" no es suficientemente dominante, y logra desviar momentáneamente la cámara hacia ella. Pero cuando Grey se encuentra, con sus fustas, látigos de cuero y el Salón Rojo del Dolor, sería afirmar lo contrario, estas secuencias se mantienen dentro de los límites de sabor vainilla de la corriente principal.

Anastasia no es ninguna victoria fácil. La escena más divertida - indiscutiblemente la más sexy, también - es cuando se encuentra en la "Reunión de Negocios" con una gran mesa de cristal, sentados al extremo del comedor, mientras que Ana lo azota a través del contrato por su relación experimental, logrando lo que ella considera franqueable. El guión no tiene miedo de llamar a la vagina, vagina o al mencionar: "Tachar el fisting anal. " es totalmente franco.

Es increíble el auge que tuvo la banda sonora de la película, teniendo entre sus listas a Ellie Goulding, The Weeknd, Sia, entre otros. La música es un viaje tranquilo y elegante que distorsiona de buena manera cada momento erótico de la película, haciéndolos mucho más tolerable para oír gemidos y golpes constantemente.

El castigo a la diosa interior es muy impactante y sorprendente, y el dolor que siente, nos hace llorar a todos. "Uno - Dos - Tres - Cuatro - Cinco - Seis"

El final de la película es el elemento perfecto, dejando un final abierto y nos estremece con un simple "No". (Este final es mucho mejor que el libro

No hay más palabras para elogiar el proyecto, "Sexo, Amor, Drama, Romance, Controversia, Música, Fama, Estilo y otras cosas que encierra " Fifty Shades of Grey "
        
            