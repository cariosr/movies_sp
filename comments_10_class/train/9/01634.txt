
                      Tras largos meses de espera, donde uno se había hecho unas espectativas bastante altas, por fin podemos disfrutar de la última cinta del padre de Jigsaw. Esperaba bastante de ella, ya no por tratarse del creador de la saga Saw, sino porque sus otros trabajos también me habían resultado bastante notables. Y sin duda, ha conseguido volver a sorprenderme, pues hacía mucho tiempo que no lo pasaba tan mal como he hecho con Isidious.

Para empezar, su historia puede parecer lo más típico que te puedes encontrar, pero no es del todo cierto. Si bien parece la tópica historia de casas encantadas llena de clichés que hacen pensar que te sabes hasta los diálogos de memoria, el guión se guarda más de una sopresa que el espectador no se espera ni de lejos. Si bien el principio de la película parece algo lento, no tardan en llegar las situaciones incómodas que hacen que a uno se le tensen los músculos. 

Cuenta con escenas realmente escalofriantes, que te sacan de quicio y te ponen de los nervios, pudiendo llegar a pasarlo realmente mal. Destacar la parte en la que nuestra protagonista se queda sola en la segunda casa y la escena de la alarma, desesperantes como pocas. Buenos sustos y dosis de intriga llenan la historia que si bien es cierto que en su tercio final decae ligeramente nunca llega a parecer ni tan siquiera algo regular.

La explicación dada al problema que sufre esta familia es, ante todo, bastante original. Yo al menos no esperaba que fuera algo así, y la verdad que me ha gustado. Aunque bien es cierto que resulta bastante previsible en el aspecto de cuál era la solución al problema. 

Las actuaciones son buenísimas. Destaquemos a la señorita Byrne, a la que vimos hace poco en la última aventura de X-men y que cambia de registro para demostrar que ella sí sabe poner cara de terror y sufrimiento. Por otro lado tenemos al protagonista masculino de la cinta (después del niño, claro) que también da la talla. Por último, destacar la intervención de Lin Shaye (esta mujer está en todos los fregaos) y el prota de Saw Leigh Whannell. (Resultó gracioso que en la escena en la que el cabeza de familia está en el colegio hubiera un dibujo del muñeco de la saga en la pizarra).

En cuanto a la ambientación, en muchos momentos realmente acojonante, es uno de los aspectos que consigue meterte de lleno en la historia y causar gran parte de la tensión que hay en la cinta. La dirección de Wan bastante exquisita y tan buena como demostró en trabajos anteriores. La banda sonora es otro aspecto a tener en cuenta; en algunos momento resulta acertada pero hay otros en los que exagera y se vuelve demasiado estruendosa. El final de la historia, pues aunque previsible, no deja de ser aceptable.

En conclusión, una de las mejores películas de terror de lo que llevamos de siglo...!!
        
            