
                      Al encuentro de Mr. Banks es un drama biográfico dirigido por John Lee Hancock, director de El Novato y The Blind Side (Un sueño imposible).

La película narra la historia de Pamela Travers, creadora de Mary Poppins, y Walt Disney. Este quería los derechos del libro de Travers para crear una película sobre su personaje, pero ella se muestra muy reticente.
Veinte años después de la oferta de Walt Disney, Pamela accede a la lectura de guión de la película antes de ceder los derechos. La aventura resulta un infierno para todos, ya que no se ponen de acuerdo en nada.

Para los fans de la película estrenada en el año 64, desvela muchas cosas que desconocíamos, como el hecho de que todos los personajes del libro de Travers son miembros de su propia familia.

Tom Hanks logra captar y expresar correctamente el espíritu de Walt Disney, esa imagen de hombre bueno y soñador que todos tenemos de él.
La película ganó siete premios diferentes, dos de ellos fueron para Emma Thompson, a la mejor actriz, ya que esta se sale con su personaje. Transmite al espectador con mucha claridad la lucha interna emocional que sufre a lo largo de la historia.

La banda sonora es sencillamente perfecta. En ella se encuentran las canciones de la película de Mary Poppins que todos nos sabemos de memoria.
        
            