
                      Me ha encantado!!!

Muy muy divertida y más profunda de lo que aparenta.

Una película que refleja muy bien el clásico “la vida es como un  juego”.

Anda que no molaría poder elegir y ser por un día un avatar que tuviera las cualidades de las que carecemos y que mejor nos vendrían para afrontar nuestro día a día.

¿Qué conseguiríamos si fuera así?

Recomendadísima!!!

Luis Miguel García Arribas fundador de Pausing Cine Alto Impacto.

“Porque no hay película mala sino ojos que no saben verla”
        
            