
                      Una película trepidante . ”Los próximos tres días”; posee todos los ingredientes de una buena película .De grandes escenas tanto de acción como emotivas hacen de ella una película absolutamente creíble tanto en su argumento como en sus interpretaciones .Ni por asomo esperaba tanto de ella.
Me gusto, muy sinceramente.
Si deseas pasar una tarde de cine de esas que deja un muy buen sabor de boca, no dejes de ir a verla.
Sorprendente transformación de Russell Crowe para dar el papel.
Sin duda un gran film.
        
            