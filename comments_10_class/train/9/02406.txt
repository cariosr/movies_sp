
                      Kingsman: Servicio Secreto es una película de acción y espionaje dirigida por Matthew Vaughn, director de Kick-Ass: Listo para machacar y X-Men: Primera generación.

La película está basada en el cómic de Mark Millar.
Harry Hart (Colin Firth) es un agente secreto británico que trabaja para Kingsman, una agencia de espías cuya tapadera es una sastrería.
Eggsy (Taron Egerton) es el hijo de un ex-compañero de Harry. Cuando el joven se mete en un lío, él aprovecha para reclutarlo para la agencia.
Mientras tanto, Valentine (Samuel L. Jackson), un conocido millonario, anda trazando su malvado plan contra la humanidad.

Impresionante de principio a fin. Las peleas son sin duda lo mejor; a pesar de que la cámara no para de moverse, la imagen es clara y no produce sensación de mareo, lo que es de agradecer.
Humor, acción para dar y regalar, y un suspense que te hace pasar un mal rato (como la escena de los paracaídas), hacen de esta película una reinvención del género de espías muy digna.

Aquí Colin Firth ha podido deslumbrar como no lo había hecho en estos últimos años; con su aire británico, muy James Bond.
Samuel L. Jackson también es digno de destacar en su papel de villano, poco convencional, pero con una personalidad perversamente atractiva.
El resto de este maravilloso elenco lo completan Taron Egerton, Sofia Boutella, Michael Caine y Mark Strong (además de una pequeña intervención de Mark Hamill).
        
            