
                      Es una película lenta, pero en mi opinión, hay que saborearla tranquilamente. La intriga se mantiene todo el tiempo. Los personajes de Tarantino se mueven como pez en el agua. Samuel L Jackson es el amo. Quizás no es Kill Bill pero si está a la altura de Reservoir Dogs o Malditos Bastardos. Los toques de humor negro, el formato 70mm, la música y el montaje le dan un aire de obra maestra.
        
            