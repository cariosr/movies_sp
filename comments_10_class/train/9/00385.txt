
                      Una gran película de ciencia ficción desde un enfoque nada fantasma, muy realista y "humanista". Excelentemente llevada, no pierde la tensión argumental en ningún momento.

Eso sí, el desenlace final es un poco cogido por los pelos. Cerca de la obra maestra si no fuera por ese detalle.
        
            