
                      Alberto es implacable como siempre en todas sus películas. Que no se descubra (ni siquiera preocupe) el que varias jóvenes desaparecieran o aparecieran torturadas hace apenas 20 años en España te deja noqueada. El meterte por los ojos la miseria y desesperanza de un pueblo (aún es frecuente si lo miras, pero casi nunca lo hacemos) es desolador. La realidad te impregna como lo que es, real como la vida misma. Pero además lo hace con una belleza estética y amor por sus personajes que destroza esquemas y por eso mismo duele más. Creo que es una maravilla de película, cultura con mayúsculas a pesar de hacerse con dos duros, que  es la verdad igualmente dura.
17 nominaciones a los Goya!!! Y pocas me parecen!
        
            