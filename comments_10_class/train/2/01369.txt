
                      No puedo entender las altísimas puntuaciones del público de esta película, a no ser que sólo la puntúen y comenten incondicionales de los Coen. Bajo mi punto de vista, es una película aburrida, triste, plana, lenta y que no logra mantener el interés. Es cierto que tiene una fotografía excelente y una buena banda sonora, aunque eso sí, sólo para los amantes del country.
        
            