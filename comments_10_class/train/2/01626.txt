
                      Me parece una burla a cualquiera que tenga una pizca de conocimiento de lo que es una película de terror. Aburrida, la trama pretende tener sentido ético  pero acaba siendo un callejón sin salida que de hecho ni siquiera da "miedo". Los screams son lo único que puede hacerte mover de la silla a no ser que prefieras ir a pedirte un refresco en medio de la película. Es tan mala que ha sido la única película que me ha hecho venir aquí y criticarla negativamente.
        
            