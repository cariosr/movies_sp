
                      La verdad es que Almodóvar me cansa. Tengo la sensación de que se repite siempre, a pesar de que cambie el envoltorio de sus películas en mayor o menor medida. Creo que su mayor mérito es rodearse de los mejores profesionales: actores, directores de fotografía, músicos, directores de arte, etc. pero eso solo enmascara algo que sentí hace tiempo y que he comprobado ahora: que Almodóvar sería mucho mejor director si el guión lo escribiera otro. Eso le ayudaría a renovarse e impediría que estuviese siempre haciendo la misma película.
        
            