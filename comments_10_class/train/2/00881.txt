
                      Para empezar tengo que decir que me he leído el libro, a diferencia de las niñas de 11 años hacia las cuales estaba dirigida esta película, y tengo que decir que la película no tiene nada que ver con el libro, el cual me fascinó.
Primeramente que Hardin no es para nada como lo describe el libro en cuanto a su personalidad. Han quitado una de las cosas que mas me han impactado del libro,  que es lo de las sábanas (los que lo hayan leído ya lo entenderán) Además,  las escenas sexuales, que son muy importantes ya que el libro es básicamente erótico, dan mucha pena, son escasas, aburridas y cortas. 
Tambien hay que decir, que se han saltado muchos diálogos importantes y que el diálogo de la película en general está muy mal hecho ya que parece escrito por una niña pequeña. 
Creo que está muy lejos de ser una buena película y que no se han esforzado nada en intentarlo.
        
            