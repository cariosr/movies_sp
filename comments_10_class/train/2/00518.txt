
                      Nefasto. Patetico. Un videojuego para entretener a la plebe. Patriotismo a mansalva. Acabose la película y aún intento ver dónde está la esencia de un conflicto bélico: negociación, táctica, estrategia... Nulo. Humanos y caballos muertos por doquier y balas para regalar. Siendo una temática tan interesante y real, no supieron plasmarla. Demasiado Call of Duty. Una falta de respeto para los que se esgrimaron los sesos en lograr su objetivo tan heroicamente.
        
            