
                      Una comedia, o más bien, intento de comedia poco acertado, con imágenes repetidas en el montaje y momentos cómicos predecibles y vistos ya hasta la saciedad. Roger Kumble dirige una película con la que nos intenta alegrar la tarde, con animales que hablan en comuna y que defienden su territorio frente a la amenaza de la devastación de su bosque para edificar un gran complejo urbanístico. El encargado de enfrentarse a estos animales es Brendan Fraser, con un  papel de padre de familia desastroso que intenta hacer su trabajo de la mejor manera posible, pese a que siempre acaba haciéndolo todo al revés.
Una comedia familiar más que tampoco pasará a la historia, pero que te puede entretener un rato. Poco más.
        
            