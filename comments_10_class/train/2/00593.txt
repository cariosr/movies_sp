
                      Sinsajo parte I no puede ser más sosa y aburrida, con unas interpretaciones lamentables (ella está especialmente sobreactuada) y una ausencia total de guión. En realidad se compone de 4 escenas con secuencias y planos estirados hasta el infinito para conseguir llenar el metraje. Una película totalmente prescindible, 2 horas de mi vida desperdiciadas que no podré recuperar.
        
            