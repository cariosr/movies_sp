
                      Fue una película que se pasó muy rápido y no contó nada nuevo, nada que pudiera aportar a la trama. No se desarrollan los personajes, no hay mucho diálogo, no se ve cronología y hay más música que nada. Si una persona que no sabe de qué tratan los libros ve esta película, no entenderá nada y se llevará a casa una historia diferente. Pienso que lo que hicieron en la película solo es para atraer más lectores con sed de romanticismo, pero debo advertir, que lo que pasa en la película, no pasa en el libro, así que se llevarán una gran decepción.
Nada fiel al libro, y pues tampoco es que los libros sean la gran cosa. 
En definitiva, pésimo libro, mala película.
        
            