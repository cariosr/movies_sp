
                      Para mí esta película deja mucho que desear, al igual que en la primera, tampoco he encontrado ningún "aliciente" que me motive a recomendar a mis conocidos ir a verla;      pienso que es un capítulo más de la serie aunque de dos horas de duración. No destaco el papel de ninguna de las protagonistas porque todas ellas me parecen un poco "maduritas" para interpretar esos papeles y el de Sarah Jessica Parker en especial no suscita ningún tipo de interés, ya que creo que se agotó con el final de la serie y que lo único realmente destacable de su actuación es su interminable (y un tanto insufrible) desfile de modelitos. 
Como comedia tampoco tiene demasiada gracia. Muy del montón.
        
            