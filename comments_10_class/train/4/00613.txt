
                      Una biografía sobre un personaje político revelador en su tiempo. Me ha gustado y enternecido en algunos momentos pero nada más. Para quién quiera conocer cómo fue a grosso modo la vida política de esta mujer sin tener que leer un libro esta película es ideal.
        
            