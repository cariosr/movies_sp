
                      Una propuesta infantil, algo tediosa y sin demasiado sentido. Una niña que cobra poderes gracias a unas flores y entra a una escuela de magia, al estilo Hogwarts, donde se realizan experimentos macabros. La historia en si carece de profundidad y no logra atraparte en ningún momento, solo es rescatable la hermosa fotografía animada, con colores vivos y abundancia de flora y fauna. Muy aburrida.
        
            