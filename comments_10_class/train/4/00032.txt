
                      'Buen rollo ibérico' habría sido un título más apropiado para esta comedia que, si cinematográficamente deja mucho que desear, profesa con esfuerzo un ejercicio de transmisión de valores positivos. Quede claro que este objetivo lo consigue a través de clichés y tópicos muy vistos, conversaciones un tanto banales e interpretaciones que, finalmente, dignifican la historia.
        
            