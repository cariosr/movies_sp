
                      ¿Es esta realmente la quinta entrada en la franquicia de películas Piratas del Caribe? Se siente como el 50 - hinchado, aburrido, repetitivo, agotador. (Déjame contar las formas). Excepto por el recién llegado de la serie Javier Bardem, quien trae una pequeña porción de travesura a esta fiesta de pago, Dead Men Tell No Tales tiene todo el sabor de las sobras podridas.

La historia es el mismo refrito que fue las últimas tres veces. En el año 2003, Johnny Depp nos deleitó a nosotros y a la Academia (recordó una nominación al Oscar, recuerden) como el Capitán Jack Sparrow, abriéndose paso y tambaleándose a través de un papel que se negó a interpretar correctamente. En efecto, estaba orinando en el parque temático, el cinismo familiar de esta empresa comercial calva. No es una visión bonita, ver a uno de los talentos más creativos del cine contemporáneo, reducido a repetirse para apuntalar un barco que se hunde.
        
            