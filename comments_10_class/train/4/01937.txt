
                      Dos estrellas para mi, igual a mediocridad. La producción buena, pero las actuaciones y dirección, en un ritmo lento, la protagonista nos entrega una imagen bulimica, anorexica, tristona, incluso antes de la muerte de JFK. Alli la trama se hace sosa y aburrida.
        
            