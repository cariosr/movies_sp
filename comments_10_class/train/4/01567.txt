
                      Una nublada tarde de domingo. En esta película se percibe un gran dominio de la dirección y de la fotografía, pero falla el escaso guión lo que hace que sea aburrida y en momentos soporífera. Yo no se si es que estaba cansado, pero me estaba entrando sueño y la película no me lo quitaba. Quizás sea que yo no entiendo de cine, pero si cuando salga en DVD, la ves en el sofá, te duermes seguro.
        
            