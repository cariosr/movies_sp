
                      Soy de los que piensan que el cine español sabe hacer películas de terror igual de buenas que otras nacionalidades. Solo basta con unos pocos ejemplos (“[REC]“, “El orfanato“, “Los ojos de Julia“, “Secuestrados“) para darse cuenta. Ejemplos en los que la taquilla ha respondido igual de bien que la crítica.

En “Eskalofrio“, una madre se muda con su hijo a un remoto y oscuro pueblo del norte donde comenzar una nueva vida, pues el chico padece una rara enfermedad que le impide exponerse a la luz del sol. Pero algo extraño, peligroso y asesino acecha escondido en el bosque del pueblo..

Como si de un capítulo de los libros infantiles “Pesadillas“, de R.L. Stine o una continuación de la malograda serie de Antena 3 “El pantano” se tratase, “Eskalofrio” no produce precisamente ningún escalofrío. Un guión que parece escrito por un grupo de adolescentes y una madre y un hijo que parecen físicamente adolescentes (aunque uno de ellos lo sea). Todo ello amenizado con una banda sonora sacada de un capítulo emocionante de “Los pitufos” para una cinta sin maldad ninguna que podría ser emitida perfectamente en horario protegido.

Lo mejor: el secreto que se esconde en el bosque.
Lo peor: el vestuario (con piercing incluido) de una principiante Blanca Suárez.
        
            