
                      Teniendo en cuenta los estándares establecidos por las dos primeras películas de "Superman", SUPERMAN III es una decepción. Las cualidades míticas de la historia se habían debilitado cuando se hizo esta película, por lo que los creadores tuvieron que confiar en Richard Pryor como su receptor de audiencia. Christopher Reeve interpreta al reportero-superhéroe.

como un chico confundido tratando de entender los aspectos duales de su personalidad. Decidiendo renunciar a hacer cosas buenas para el mundo, se involucra con la vieja amiga Lana Lang (Annette O'Toole), pero finalmente logra actuar cuando el malo Ross Webster (Robert Vaughn) y el genio de la computadora Gus Gorman

Pryor) trata de usar las participaciones corporativas para apoderarse del mundo. El resultado es un gag-fest cansado, con escenas de lucha que se deben más a la tecnología de videojuegos que los superpoderes del Hombre de Acero. Como de costumbre, Pryor se roba el espectáculo con un rendimiento maníaco que se eleva muy por encima del material. Reeve es

competente, aunque no está a la altura de su trabajo anterior de SUPERMAN, pero la dirección de Richard Lester no inyecta nada nuevo en el material, dando la sensación de que ve todo esto como un ejercicio de comedia. Después de que Lois Lane descubrió la identidad de Clark Kent en SUPERMAN II, la leyenda

Perdí algo.
        
            