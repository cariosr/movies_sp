
                      Desde tiempos ancestrales, y no sólo en lo que al cine se refiere, los conflictos de pareja han sido un tema frecuente, recurrente y eficaz. Ya sea mediante dramas, musicales, o a través de situaciones diversamente cómicas, el amor y las dificultades para conseguirlo son un tema tan universal que no caducan, sino que permanecen y nunca pasarán de moda.

Eternamente comprometidos trata sobre la relación de una pareja que se compromete pero, por circunstancias más achacables al género femenino que al masculino, nunca termina de decidirse a dar el paso definitivo llamado boda. Arrancan aquí una sucesión de acontecimientos, algunos más naturales y otros más esperpénticos, algunos graciosos y otros extremadamente tópicos, que hacen que el momento cumbre de toda comedia romántica, la llegada de la felicidad en pareja, se vaya retrasando tanto que hasta los elementos que pretenden sorprender se transforman en anodinos y prescindibles.

Tenemos lo básico, que son unos novios enamorados, rodeados de familiares entrometidos y extravagantes y algún colega que se ha caído colgando de la película de arriba. Pero la trama, aunque se va construyendo sola gracias a un planteamiento claro, sufre la torpeza del que no consigue plasmar la claridad en concisión y convierte la gracia en hastío.

El guión lo firman a pachas Nicholas Stoller, que también dirige, y Jason Segel, que protagoniza. Este último, actor cada vez más frecuente en comedias blandas o de tono independiente, sigue buscando su sitio en un mundo ambicioso en el que lo tiene bastante difícil, ya que su cara y cuerpo de maizón no ayudan a desencasillarlo del Marshall que todos conocemos desde que alguien se puso a buscar a la madre de sus hijos. Aquí, simplemente, ha cambiado a una pelirroja por una morena inglesa (Emily Blunt), tal vez más guapa, pero con menos gracia, que le putea más que aquella pero, a cambio, le cede más protagonismo y a la que, a pesar de los pesares, no me termino de creer.

Supongo que el título original, equivalente a El compromiso de cinco años, no le pareció acertado al encargado de traducirlo porque, ciertamente, la película se hace más que larga. Y no es una cuestión de ritmo solamente, sino también de duración e, incluso, sensación a posteriori. Y ahora, ¿qué?

http://diarioenred.com/2012/09/eternamente-comprometidos/

sígueme en twitter: @jgripweed
        
            