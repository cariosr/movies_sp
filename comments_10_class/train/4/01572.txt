
                      Va de más a menos. Una primera parte que ofrece suspenso y  tensión mostrando a una protagonista que, mientras vacaciona con su familia, se la ve perturbada por un pasado que la acecha. Hasta ahí interesante. Y más aún cuando aparecen intrusos iguales a ellos que generan cierto escalofrío y se desconocen sus intenciones. A partir de ahí el argumento empieza a derrapar y carece de todo tipo de lógica. Lo único más o menos aceptable es el giro del final y algunas escenas cómicas. No me convenció.
        
            