
                      El marketing nos lleva a veces al error. Unos buenos efectos especiales no bastan si sólo hay cuatro escenas de acción. Película sin ritmo y demasiado fantasiosa en su desarrollo.
        
            