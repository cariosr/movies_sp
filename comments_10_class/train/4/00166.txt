
                      Todo se reduce a un carrusel de escenas teóricamente de peligro, alguna aceptablemente rodada (las del minotauro y la hidra) pero todas ellas carentes de vigor e inventiva cuando no al borde lo insoportable (el largo y penoso episodio en Las Vegas).
        
            