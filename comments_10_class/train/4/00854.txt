
                      Poniendo su énfasis en la comedia amplia a expensas de la ingeniosa conspiración y la hechicería técnica, prácticamente no tiene la sensibilidad mítica o cósmica que marcó a sus predecesores.
        
            