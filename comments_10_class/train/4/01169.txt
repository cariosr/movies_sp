
                      Una relación fría como el sur argentino.
Un adolescente maleducado, apático y conflictivo y un padre de características similares con el que se reencuentra en Chapelco tras el fallecimiento de su madre. Un film en el que no ocurre casi nada. Se torna lenta, larga y con muy poco diálogo. Solo destacaría la linda fotografía del lugar donde se aprecian las montañas nevadas, alguno que otro animal autóctono, los lagos y los bosques. Muy aburrida.
        
            