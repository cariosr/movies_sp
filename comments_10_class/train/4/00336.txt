
                      Los movimientos dramáticos son tan obvios y agotados que ni siquiera las expresiones tristes de basset-hound de la estrella pueden redimirlos.
        
            