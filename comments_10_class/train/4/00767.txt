
                      Iba con altas expectativas a ver esta película y lo que me lleve fue una gran decepción. Esta historia ya había sido vista con anterioridad y lo único que hace es reemplazar a Rocky por el hijo de Apollo, creo que la película recae en cliches, además de escenarios poco plausibles y una escena que me hizo reír innecesariamente.

Por cierto, a destacar la labor técnica de la primera pelea en una sola toma mostrando la visceralidad del boxeo y una destacada actuación de Sylvester como mentor de Creed, de ahí en fuera nada más que destacar.
        
            