
                      Comedia en la que prácticamente todo el peso cae sobre el humorista Leo Harlem. Sucesión de gags esperpénticos alrededor del personaje protagonista y sus líos en una serie de situaciones que resultan poco creíbles y que en el caso de algunos secundarios sus actuaciones tampoco ayudan. Tiene algún momento pero en general la historia se cae por inverosímil.
        
            