
                      Tres son multitud

Ver “Insidious” era como montarse en el tren de la bruja, una montaña rusa de clichés del género sabiamente mezclados por James Wan y Leigh Whannell. Su secuela directa suponía una inteligente vuelta de tuerca a lo que ya habíamos visto, pasando de los viajes astrales a los temporales sin despeinarse en absoluto, y siempre bordeando la delgada línea que separa la verosimilitud del esperpento en forma de espectáculo de marionetas. Juntas suponían un soplo de aire fresco a algunas ideas ya trilladas, como las casas encantadas o los espíritus y demonios.

Por ello, tratar de exprimir una idea que ya parecía marchita era un riesgo enorme, especialmente sin Wan en la silla de dirección. Y lo primero que llama la atención de este tercer capítulo es que su heredero, el propio Whannell, no se defiende nada mal con la cámara. Consigue no repetir los esquemas y maneras de su amigo, no hay imágenes aceleradas y en general la fotografía es efectiva, ya no parece que asistamos a un film con estética tan fría y luminosa que da la sensación de ser televisivo. 

Whannell encuentra su propio camino como director, pero sólo desmarcándose de la  estética de anteriores entregas, no porque acuñe un estilo personal. Porque le falta la capacidad de su compañero para recrear atmósferas y generar tensión, y en su lugar tira del susto fácil y los tópicos del cine de terror de bajo calibre, más cercano a la reciente “Ouija” que a “The Conjuring”, a la cual homenajea de una manera bastante graciosa.

Sin embargo, su debilidad en la dirección podría perdonarse si hubiera sido capaz de ofrecer algo mínimamente original. Y aquí es donde falla “Insidious: Capítulo 3”. Hay muy poco en ella que te remueva en la butaca –quizá la figura de su villano, y algún momento conseguido, pero que recuerdan a lo ya visto en otras películas, como el ojo en la garganta-. Es una precuela que se contenta con ser, no con lo que puede dar de sí. Una propuesta de espíritus típica, sin más interés que pasar un rato entretenido –a veces, que le cuesta arrancar y tiene serios bajones de ritmo-, y donde lo realmente relevante transcurre en su media hora final, pero sin llegar a resultar escalofriante ni ofreciendo esa chispa de ingenio que la haga mantener el tipo con respecto a las anteriores.

Eso sí, es un vehículo de lucimiento para la gran Lin Shaye. La trama central de la chica que siente la presencia de un ente maligno está de más. “Insidious: Capítulo 3” es la película de Elise, y Shaye vuelve a ser lo que sobresale por encima de todo el conjunto. Aún así, supone un retazo de la vida de esta médium totalmente descafeinado, un spin-off involuntario que bien podríamos habernos ahorrado. Ya saben, tres son multitud. 

A favor: Lin Shaye, y su tramo final
En contra: su falta de originalidad y su terror de bajo calibre
        
            