
                      Pelicula rodeada de polemica y escándalo, ya que el director, el inefable Roman Polanski, la rodó en parte desde la carcel, propaganda buena o mala para el film, aunque propaganda al fin y al cabo. Pero ni aún con esto sorprende el argumento de la cinta, una vez más empeñados en trasladarnos al problema de los países occindentales con Irak y Afganistan.
Sí hay que subrayar las grandes interpretaciones de todos sus actores, como la ofrecida por el veterano Pierce Brosnan, que resulta más que interesante y convincente en su papel de "malo", demostrando que es mucho más que una cara bonita.
¿Qué más se puede añadir? Sí se respira el estilo que ha hecho famosos a Polanski durante todo el film pero, personalmente, opino que no es de sus mejores obras. Interesante pero fallida.
        
            