
                      Intentare no sin dificultades ser benévola.
Obsoleta en cuanto a argumento se refiere más la escasa muestra y creación del personaje, no negaremos que los efectos especiales y el nuevo aspecto de bestias y dioses mitologicos es más que increible. Yo diría que muy espectacular. 
Hay que subrayar no obstante la escasa aparicion de personajes citados en el libro, que malogra un poco el resultado final.
Yo diria desde mi humilde opinion que es una peli digna de... Una tarde de domingo con pipas en casa.
Nada tiene que envidiar la extensa y escasa en medios antigua pelicula a esta de hoy.
Resumiendo de 1 a 10, sumaría un tres raspado, que logra debido al apabullante aparato visual que despliega, magnífico en determinados momentos, aunque acaba haciéndose bastante cansino a medida que comprobamos que la cinta está completamente vacía. 
Digamos que es bastante mala con todos los respetos.
        
            