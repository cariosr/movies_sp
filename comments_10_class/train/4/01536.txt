
                      Lorax es una película de animación dirigida por Chris Renaud (Gru, mi villano favorito) y Kyle Balda (Los Minions). Está basada en el libro infantil del Dr. Seuss. 

En un intento de impresionar a Audrey (Taylor Swift), Ted (Zac Efron) emprende una aventura que lo llevará hasta El-Una-Vez (Ed Helms). Este le relatará a Ted la historia de El Lorax (Danny DeVito) y de cómo por su culpa, en la actualidad no hay árboles. 

Lo malo de verla en español es que traducen las canciones, y los dobladores no cantan muy bien.
Por otro lado, hablando de la película en sí, no hay atractivo alguno. Es una película para niños, con un mensaje de cuidar y preservar el medio ambiente muy poco endulzado, a pesar del colorido que hay.
Al ser infantil, empieza a aburrir a la media hora de haber empezado.

Lorax es el único personaje con algo de encanto, además de la abuela de Ted, que interviene muy poco. 
Danny DeVito tiene especial mérito, ya que además de doblar a Lorax en inglés, lo hace en español, y muy bien.
        
            