
                      No muy lejos del clásico de los 80´s esta nueva versión se puede decir que es correcta…. y listo. Las protagonistas son las adecuadas, destacando Kristen Wiig que resulta la más natural y creíble de todas. Las apariciones del antiguo reparto son un tanto forzadas (busto de Harold Ramis incluido), los efectos especiales no aportan gran cosa,….. pero no se puede decir que sea un mal trabajo aunque tal vez un guion más currado no estaría de más…………..
        
            