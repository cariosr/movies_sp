
                      El consejero se puede resumir de una manera: filosofía de garrafón para los no avisados.
Es la prueba evidente de que el éxito literario le permite a un escritor reverenciado escribir un guión forzado con frases rimbombantes como si fueran balas. No tengo que desperdiciar ni una. Un disparo, una frase. Es la manera más corta de matar una película. ¡Enhorabuena, Cormac!
El señor Boyero y otros críticos que admiten su debilidad por el alcohol y el sexo y desprecian al vulgo como fórmula para aparecer modernos o originales estarán encantados con esta película. No es mi caso. Boyero acierta a veces, también yerra. Con La Mejor Oferta ajustó su crítica presentándola como la Mejor Oferta del Verano. Con El Consejero nos dice que es "poderosa", cuando es mucho menos película que la macarrada de Fast&Furious 6. 
Amigos, Scott es un buen director, los actores están bastante bien, pero lo que falla de esta historia no es la historia, es el guión y la forma que tiene de desenvolverse.
Por eso ha sido un fracaso de público. Bien, se que hay algunas películas excelentes que fueron fracasos en taquilla, como Copying Bethooven, El Velo PIntado, y otras tantas. Estas películas encuentran una segunda vida tras la pantalla. Blade Runner fue un fracaso de taquilla.
El Consejero quedará simplemente como un pestiño en los videoclubs del futuro (si es que queda alguno), sencillamente por la intención de su insigne escritor de hacernos pensar a los idiotas. ¿por todos los Santos! ¿Quien osa criticar a nuestrio amigo Cormac? ¡Sacrilegio!
        
            