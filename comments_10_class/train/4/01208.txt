
                      Bueno la cuarta pelicula de los Piratas del Caribe esta muy lejos de la calidad de la primera pero tampoco esta tan mal. Hay aventuras y unos momentos divertidos. Aunque se echa de menos a Will Turner y Elizabeth Swann, el nuevo malo, Barba negra, esta bien gracias a Ian McShane. Lo que pasa es que ya hemos visto lo mismo cuatro veces... O sea, o hay que saber parar o hay que ser un poco original. Pero tienen una receta que funciona y no la van a dejar. Venga, una estrella para Johnny Depp y una para Ian McShane.
        
            