
                      Convencional, aunque con una dirección eficaz. Abunda en los tópicos de siempre, y Wang, el director, insiste un poco más en el tema del estudio científico de los demonios, tratando de darle una patina de verosimilitud a un tema que es inverosímil salvo en el celuloide. Es un producto que esta un poco por encima de los subproductos del género del exorcismo-demonio-comeniñas.
        
            