
                      Cazafantasmas (2016).
Tres décadas después de la película original vuelven los Cazafantasmas en versión femenina con cuatro actrices no especialmente graciosas (incluida la actriz fetiche del director, la "bien criada" Melissa McCarthy), o al menos comparadas con los protagonistas de la versión del '84 (quienes hacen aquí sendos cameos muy de agradecer por los que vivimos su estreno, a excepción del recientemente desaparecido Harold Ramis, a quien se le dedica la película).

La cinta cuenta con diversos secundarios conocidos como es el caso de Chris Hemsworth, en un papel que podían haberse ahorrado en mi opinión, (aunque siempre es agradable para las espectadoras su presencia en la gran pantalla...), o el caso de Andy García, en plan alcaldesa Ana Botella o alcalde de la serie Padre de Familia... no digo más...

Lo mejor, la banda sonora con su tema principal, Ghostbusters, versionando al gran Ray Parker, Jr. sin llegarle a su nivel, desde luego. Los efectos, como corresponden a este siglo, muy vistosos, pero que tampoco consiguen superar la cinta de Ivan Reitman, ni mejorar el guión que en su día escribiera Dan Aykroid con sus compañeros de reparto Harold Harris -QEPD- y el "gran" Rick Moranis.

Ciertamente estamos ante un ejemplo más de falta de imaginación por parte de Hollywood. Un filme bastante simplón e infantil enfocado a hacer caja, y que puede ser visionado por todos los públicos tanto en el cine como en casa cuando esté disponible.

Raúl Cabral.
        
            