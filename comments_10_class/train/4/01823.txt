
                      Película que no es una secuela, ni un remake, ni un reboot, es una idea original de esas que ya no vemos casi en el cine actual, es entretenida, con buen ritmo y con valores, cumple perfectamente con sus objetivos, pero dentro de un año nadie se acordará de ella, ni los niños, creo que no es lo suficientemente profunda para llegar al público y conseguir que no se pierda en el recuerdo.
        
            