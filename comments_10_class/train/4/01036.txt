
                      Había un poderoso motivo para ir a ver ésta película y otro más poderoso para no hacerlo: El primero era STEPHEN FREARS, el segundo MERRYL STREEP... Al final, ni uno merecía tanto la pena, ni el otro espantaba tanto.

Stephen Frears construye un ¿retrato? absolutamente descafeinado, incompleto y superficial de la reina de la cultura trash. No funciona como biopic, porque las pinceladas que deberían imprimir carácter a este retrato, son gruesas, titubeantes y sin textura. 

Sí, nos muestra los últimos días de la diva trash, pero desaprovecha todas las oportunidades de contarnos su vida a través de esos momentos.

Es cierto que el primer tercio de la película recuerda a las grandes comedias del Hollywood de aquella época, con ritmo y un montaje vivos, que se apoya sobre todo en el carácter coral del reparto y en la maestría tras la cámara de Frears. Sin embargo, cuando la historia se ralentiza, despojándose de la acidez que destila Frears, y se diluye en escenas interminables al servicio de la diva Streep, toda la energía inicial se convierte en tedio.

Sí, en este punto la cinta se vuelve aburrida, lenta, no avanza, no cuenta nada, no aporta nada...y en su tercio final apenas remonta.

Posiblemente el que la película acabe siendo una comedieta amable y liviana, se deba al desaprovechamiento sistemático  del potencial de los magníficos actores secundarios y a no saber, o querer desarrollar sus personajes. Ahí, por ejemplo, es donde más perjudicadas salen interpretaciones tan acertadas como la de Rebecca Ferguson.

El mayor descubrimiento, el auténtico "robaescenas" y el único personaje (e interpretación) que no decae en ningún momento es Simon Helberg, al que todos conocemos de "the big bang theory", interpretando al amanerado y delirante pianista/compositor Cosme McMoon. 

Él sí es un personaje que hubiera salido de la cabeza de Frears, haciendo la película suya desde su primera aparición, con un recital de sutilidades que convierte al público en cómplice. Sin excesos y con una vis cómica envidiable.

Y por cierto, el único actor con quien la Streep consigue tener algo de química y que la ayuda a salvar alguna de sus escenas más insufribles.

Otra robaescenas es Nina Arianda, es su papel de nueva rica/rubia explosiva/señora de magnate. 
Todo el elenco de "actores de reparto" es impecable. Incluso Hugh Grant, en su papel de siempre consigue brillar y transmitir.

Y es aquí dónde la película se convierte en una metáfora de la historia que nos cuenta: todo un entramado de talento y medios supeditados a la mayor gloria de una diva sin más talento que su propio ego. Sí, la Streep, que reconozco que cada vez me molesta más y me la creo menos, en esta ocasión duda que pueda ser convincente salvo para sus más devotos. Su recital de muecas, de gorgoritos insufribles y sus excesos, aquí se quedan cortos. No alcanza el patetismo que necesita, no hay convicción y resulta eclipsada por cualquiera que comparta escena con ella. Ni siquiera molesta, sólo aburre, que es peor.

Es como si Frears se convirtiera en Clair Bayfield (Marido de Jenkins), intentando que la diva (Streep) tenga su espectáculo a toda costa ya cualquier coste.

Resumiendo: una peliculilla discreta, superficial,  a medio camino de todo, disfrutable y entretenida por momentos, que olvidarás a los cinco minutos de salir de la sala...y que seguro que su director olvidará en cuanto cobre el cheque del encargo.

Seguramente un biopic sobre Leticia sabater (sin la Streep, por supuesto), le dé mucho más juego.
        
            