
                      Si Spidey es super y Batty es brillante, entonces cuatro superhéroes deben ser fantásticos, ¿verdad? Mal, me temo.

No puedo decirle cómo se compara la película con su origen: mi único pincel con los cómics se presenta en forma de ósmosis de mi compañera de casa, que está tratando de engancharme dejando los cómics al lado del baño, pero puedo decirles que Esta es una pálida imitación de una película de acción.

Parte del problema de la película es su puesta en escena. Casi toda la película se centra en el 'descubrimiento' de los poderes especiales de los personajes. Esos rayos cósmicos pueden causar estragos en tu ADN, ¿no lo sabes?

Esto está bien hasta cierto punto, pero solo hay tantas escenas intercaladas de Mr Fantastic (Ioan Gruffud) yendo esos pocos pies extra con su brazo para el bog roll y Johnny Storm (Chris Evans, no, el DJ) tratando de conseguir a Ben La cosa de Grimm (Michael Chiklis) para golpearse a sí mismo en la cara que una persona puede tomar.

El mayor problema con esta película es la dirección. Es superficial en el mejor de los casos. Tim Story realmente no tiene talento para la acción; de hecho, gracias a sus ofertas anteriores de Taxi y Barbershop, no estoy seguro de si tiene talento en alguna dirección. Recorre la acción de una pieza a otra de una manera más aburrida que recuerda más a una película de televisión de los 90 que a una superproducción de verano. Su falta de arte solo se compara con el puntaje, que seguramente provino de la película 4 de Now That What Call Call a Super Hero.
        
            