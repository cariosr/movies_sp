
                      Documental artificial vendido como cine. Profesor de lengua enseñando ciudadanía. De cómo cambiar la sociedad por mi cuenta. Consecuencia: cachondeo general que casi le lleva al paro. Al menos allí todavía se levantan cuando llega alguien a clase y llaman señor al profesor. Y no se habla ni un minuto del fondo de la cuestión. Mala, fea y barata. Un buen ejemplo para entender por qué en España todo buen profesor se coge la baja. Huir.
        
            