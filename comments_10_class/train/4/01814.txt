
                      A pesar de que "Daddy's Home" se encuentra en el área de Nueva Orleans y "Sisters" se encuentra en Orlando, se siente como si fueran vecinos de al lado en Mediocre Boulevard.

Al igual que la comedia de Amy Poehler-Tina Fey de la semana pasada, "Daddy's Home" cuenta con un talentoso y agradable dúo que hizo clic antes en la pantalla.

Ambas películas tienen escenas ambientadas en los aeropuertos. Ambas películas tienen segmentos en los que un hogar encantador está seriamente dañado por un supuesto efecto cómico. Ambas películas cometen el error de pensar que es gracioso que uno de los personajes principales se emborrache seriamente y actúe como un idiota abusivo y desquiciado.

Y ambas películas se sienten como oportunidades desperdiciadas.

En "Daddy's Home", Will Ferrell interpreta a Brad, una variación de un personaje de Will Ferrell que hemos visto una docena de veces: un tipo agradable que es tan agresivo, maravilloso, tan difícil, incluso los niños a su alrededor a veces ojos en su cursi salubridad. Por supuesto, sabemos que cualquier persona pasiva tiene dentro una gran probabilidad de explotar algún día.

Brad está casado con la encantadora y dulce Sarah (Linda Cardellini, criminalmente infrautilizada), que tiene dos hijos pequeños, Dylan (Owen Vaccaro) y Megan (Scarlett Estevaz), de su matrimonio anterior.

Aunque Brad es una figura paterna solidaria que pone notas inspiradoras en los almuerzos de los niños, los deja en la escuela todas las mañanas, es un líder de scouts y un entrenador y se inscribe en todos los deberes paternos bajo el sol (no estoy seguro de lo que mamá está haciendo) Brad parece estar disponible las 24 horas, los 7 días de la semana), los niños tardan en acostumbrarse a él. La pequeña Megan, en particular, parece resentirse con Brad, como lo demuestran una interminable serie de dibujos en los que mata a Brad en una variedad de modas. ¡Encantador!
        
            