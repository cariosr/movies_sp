
                      Una película oscura de género policíaco, con tintes de drama, e infulas de terror (aunque no pasa de las infulas). Un guión que se hace pesado desde casi el principio y que estira una historia que se podía contar en media hora. Por contra una fotografía bastante decente que te sumerge en la creciente locura del protagonista. Emma Watson se libra de la pesada sombra de Hermione con una interpretación decente con la que consigue hacernos sentir compasión por la situación que vive su personaje. Ethan Hawke destaca, pero no llega a brillar.
        
            