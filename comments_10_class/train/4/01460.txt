
                      Es de esas adaptaciones de Disney que, sinceramente, está totalmente enfocada a l@s más pequeñ@s, puesto que es obvia, previsible y demasiado histriónica para tomársela en serio. Glenn Close resulta de lo más convincente en el papel de Cruella, pero lo cierto es que va en la línea del resto del film, que requiere de una gran sobreactuación.
        
            