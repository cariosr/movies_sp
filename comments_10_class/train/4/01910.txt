
                      Bajo la superficie de las aguas

Siempre es interesante ver cómo un actor se pasa al otro lado de una cámara y asume el rol de director, cómo recoge influencias de todos los cineastas para los que ha trabajado y encuentra su propio camino como contador de historias. Ya lo han hecho con éxito Clint Eastwood, Mel Gibson o Robert Redford, y más recientemente Ben Affleck, George Clooney o Tommy Lee Jones, todos ellos con resultados en muchos casos sobresalientes.

Ahora es Ryan Gosling quien se sienta en la butaca del director para presentar “Lost River”, una oscura fábula que se mueve entre los suburbios de un Detroit en bancarrota, una ciudad fantasma sometida a un hechizo que la impide salir a flote de su propia inmundicia. Lo que el actor propone es un perturbador cuento de hadas repleto de lobos al acecho de los débiles, de los que hacen lo que sea necesario por sobrevivir y los monstruos que se aprovechan de esa situación. 

A nivel argumental, este siniestro relato sobre el capitalismo y la conquista del sueño americano escondido bajo la superficie del agua está bien perfilado, a pesar de un desenlace que cruza la línea del absurdo y la presuntuosidad. Gosling consigue transmitir al espectador su mensaje a nivel de guión, y logra un mundo tan retorcidamente onírico como aterradoramente real repleto de personajes bastante bien matizados sometidos a todo aquello que se le pasa por la cabeza al director. 

Sin embargo, a nivel de dirección, pese a momentos puntuales de ingenio y fuerza autoral, Gosling se acomoda en la silla como realizador tanto como viene haciendo como intérprete desde “Drive”. Y es precisamente al cine de Nicolas Winding Refn a lo que recuerda esta ópera prima, pero como una versión sucia y empantanada del trabajo del cineasta danés. Se mira tanto en el espejo de su maestro que otras referencias al cine de David Lynch o Terrence Malick quedan como meras copias a un nivel mucho menor.

Lo que resulta en definitiva es una obra mal equilibrada, con una historia potente, una buena dirección de actores –aunque Ben Mendelsohn se los come a todos- y un universo bastante atractivo, pero que adolece de una realización totalmente desprovista de personalidad. Puede hacerse extremadamente extraña para muchos espectadores, pero éste es el menor de sus males cuando trata de asemejarse a otros directores. Una lástima, porque de haber sido más original tras la cámara, su debut apuntaría maneras de prometedor realizador, y no se hallaría ahogado bajo la superficie de las aguas de ese río perdido del título. Y ya que como actor viene repitiendo el mismo personaje desde hace cuatro años, siempre le quedará como salvoconducto ser guionista. Su río aún no está perdido.

A favor: la potencia del relato relato y el muestrario de personajes del que hace gala Gosling en su guión
En contra: la dirección nada personal, que recuerda demasiado al cine de Winding Refn
        
            