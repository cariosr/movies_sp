
                      Lucy es una película francesa de ciencia ficción dirigida por Luc Besson, director de Nikita, Arthur y los Minimoys y El Quinto Elemento, entre otras.

Lucy es una joven que sin quererlo se ve envuelta en una red de tráfico de una nueva droga, que se reparte en bolsas de un kilo. Tras una serie de sucesos la droga se extiende por el organismo de Lucy, lo que hace que empiece a adquirir poderes tanto físicos como mentales.

Scarlett Johansson realiza una interpretación admirable. Se ve a la perfección en su rostro como Lucy, al adquirir más conocimientos sobre la vida y el universo, va perdiendo la capacidad de sentir.

Las imágenes intercaladas sobre naturaleza que usan a lo largo de la película, son innecesarias en su mayoría.

Hay que reconocer el mérito de los efectos especiales; las peleas, las persecuciones en coche por Francia, etcétera.
Sin embargo, la película deja mucho que desear en general, ya que en un principio crea altas expectativas.

El final recuerda mucho a la película Transcendece (2014), en la que también aparece Morgan Freeman junto con Johnny Depp
        
            