
                      Producción china de 2014, escrita y dirigida por Leo Lee, y protagonizada por unos curiosos personajes de cuadradas cabezas y una sola pierna, habitantes de otro planeta, que plantearán a los jóvenes espectadores el significado de la heroicidad.(...)
Lo mejor: los niños y niñas a los que va dirigida la película se encontrarán con una historia y unos personajes originales que les enseñarán lo que de verdad importa para ser un héroe.
En contra: la frecuencia con que se muestran encuadres y movimientos de cámara que despistan de lo que les ocurre a los protagonistas y marean al espectador.
        
            