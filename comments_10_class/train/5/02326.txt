
                      La Llorona (2019).
“No hace falta ser religioso para tener fe."
El debutante en el terreno del largometraje Michael Chaves nos trae esta leyenda de origen mejicano donde una joven madre en el S:XVII llevó a cabo un terrible asesinato en el seno de su familia, desquiciada por los celos. En la década del 'Me Too’ y del feminismo exacerbado, asistimos en pantalla grande a una historia políticamente incorrecta en la que la que daña es la mujer.

“-Nos enfrentamos a un mal que no conoce límites.
 +No es humana, ¿verdad?
 -No, pero lo fue..."
En el filme, producido por el rey Midas del cine de terror James Wan, aparece la actriz que el público recordará por su participación en la saga de "La Momia", la bella exmodelo venezolana Patricia Velasquez, caracterizada para la ocasión y con algunas décadas más (sí, han pasado dos desde el estreno de la primera parte de la conocida saga con Brendan Fraser). 

“-El padre Pérez dice que ha dado la espalda a la Iglesia.
  -A la Iglesia, sí. A dios, nunca."
También podemos ver al sacerdote que apareciera en “Annabelle”, haciendo un guiño a ésta última. Y es que el novel director de la cinta se ocupará en 2020 de la nueva entrega de "The Conjuring”.

En resumen: un largometraje de tan solo hora y media, con sustos al estilo de la saga arriba mencionada, con una realización modesta, pero con una puesta en escena y una ambientación (1973) que nos mete en la historia y optimiza las subidas de volumen en los momentos clave.

Raúl Cabral.
        
            