
                      Tercera película de la saga de El Hobbit que pone fin al universo Tolkien en el cine. He de decir que esperaba bastante mas de esta entrega final, siendo sincero, ha sido un poco decepcionante, merecía un final mejor. No es que sea una mala película, pero dejando a un lado que si lo comparamos con "El Señor de los Anillos" siempre saldrá perdiendo, podría ser bastante mejor. La primera mitad resulta muy pesada y poco fluida, únicamente en el tramo final gana algo más de fuerza, pero sin llegar a encontrar la épica, que es lo que pretende.
        
            