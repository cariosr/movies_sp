
                      En medio de un guión excelente, se esconde el retrato de una historia célebre y sugerente centrada en tres momentos de impacto, sin embargo, uno acaba echando de menos más, sabe a poco el ruido provocado por la crítica y el espectador tiende a experimentar cierta decepción. Demasiado cautelosa en la escenificación. Lo mejor, el tremendo parecido del actor con el Steve Jobs real que te introduce en la historia con mayor énfasis. Lo peor, lenta en muchos puntos que lleva a que te pierdas con facilidad.
        
            