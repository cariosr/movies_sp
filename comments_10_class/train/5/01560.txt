
                      ESTAFADORES SIN 'CHICHA NI LIMONÁ'

Cabe decir que en estos últimos años no se han prodigado demasiado las películas de estafadores, debido principalmente al cambio de la tendencia cinematográfica en los espectadores, estando las adaptaciones de cómics de superhéroes y los blockbusters de rabiosa actualidad. Aunque, a lo largo de la historia del séptimo arte han habido algunos largometrajes enmarcados en este género que bien merecen tenerlos en consideración, como por ejemplo la multipremiada en los Oscar "El golpe" (George Roy Hill, 1973), la cinta argentina "Nueve reinas" (Fabián Bielinsky, 2000) o la lujosa "Ocean's Eleven" (Steven Soderbergh, 2001), entre otras muchas.

El filme que nos concierne, titulado "Focus", nos relata las andanzas de un experimentado estafador que acoge a una atractiva joven bajo su protectorado; no obstante, las cosas se complican cuando ambos comienzan un romance. Mientras le enseña los trucos del oficio, ella desea estrechar demasiado los lazos con él, y éste termina por romper la relación. Tres años más tarde, la mujer que había encendido una llama en él, convertida en una mujer fatal, reaparece en Buenos Aires en un circuito de carreras en el que las apuestas son muy elevadas.

La película se compone de dos partes bien diferenciadas, estando la primera planteada de forma excelente, con un entramado en el que nada es lo que parece, y una certera presentación del dúo protagonista; sin embargo, la segunda baja el pistón considerablemente, quedándose la resolución del desaguisado a medias, dando la impresión de que ha habido mucho ruido y fuegos de artificio, para luego haber, a decir verdad, muy pocas nueces, mermando el resultado global del filme.

Es destacable el trabajo solvente que realiza la pareja protagonista, formada por Will Smith y Margot Robbie, que demuestran tener una buena química entre ellos y carisma, reluciendo su habitual palabrería; no obstante, el personaje de Rodrigo Santoro, que entra a escena en la discutida segunda parte, no acaba de cuajar, estando desdibujado y por supuesto, totalmente desaprovechado.

En definitiva, pese a sus fallos, "Focus" es un filme entretenido, con un interesante planteamiento de su premisa, en el que destaca la buena química entre su pareja protagonista, aunque está lejos del nivel ofrecido por otras cintas de estafadores similares.
        
            