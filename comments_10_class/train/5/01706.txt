
                      "Beginners" es un film intimista, que se toma (mucho) su tiempo y que valorisa a sus 3 actores principales. Es una obra a veces poética,  a veces sin relieve. Me hizo sonreír algunas veces, vibrar prácticamente nunca y encontré a Mélanie Laurent radiante. Es un filme en el que me aburrí un poco pero por el cual entiendo que uno se entusiasme pues el punto de vista y la manera cinematográfica de abordarlo son atípicos y pueden gustar. Yo personalemente no me"
        
            