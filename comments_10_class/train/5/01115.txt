
                      La Llegada (2016).
El director de Sicario se propone aburrirnos un par de horas con esta visión particular de una hipotética visita extraterrestre a nuestro planeta. Sí, en ciertos momentos denota originalidad y positivismo con la ayuda de un buen reparto, (Amy Adams a la cabeza, seguida de Jeremy "Ojo de Halcón" Renner y Forest "Ojo tangado" Whitaker) y un guión curioso, pero luego vemos que a alguien se le ha ido un poco la mano con la "maría" en algún momento...

Bradford Young; quedaos con este nombre. Es el responsable de la fotografía. Y vaya fotografía... oscura... pero tela de oscura. Supongo que para disimular los efectos visuales que de una película como esta se esperan, y de los cuales carece el filme.

En resumen: demasiado larga, lenta, apagada y tediosa, aunque con cierta originalidad en la manera de llevar la comunicación de los extraterrestres con los humanos. 
Lo que más miedo da, antes de verla, es la opinión de los críticos. ¡La ponen de buena! 

Raúl Cabral.
        
            