
                      Película Española y Búlgara, dirigida por un poco conocido Gabe Ibañez. Autómata cuenta la historia de un futuro donde el planeta tierra se encuentra al borde del apocalipsis y la raza humana sobrevive en refugios, y de Jacq Vacuan, interpretado por Antonio Banderas, un investigador de seguros de una empresa robótica el cual se ve inmerso en un caso de manipulación ilegal de un robot. un caso que parece ser rutinario pero que empezará a tomar gran importancia para el y para toda la raza humana.

Esta película se encuentra claramente influenciada por otras de ciencia ficción y tiene ciertas similitudes con Yo Robot y Blade Runner tanto en su guión, como en su estética, pero carece de la magia de estas.

Hay que decir que el cine Español no suele tratar la temática de la ciencia ficción y la robótica. El estilo, ambientación y la estética conseguida en esta película son fantásticos, tanto en la ciudad como en el posterior desierto. Transmiten perfectamente las sensaciones deseadas. Es todo un acierto y un gran logro para una industria que no suele tratar esta temática y es lo más positivo de Autómata.

La historia es un tanto lenta y parece hecha para el claro lucimiento de Antonio Banderas el cual es productor y se lo ha montado para ser el protagonista indiscutible de este film. Banderas nos tiene acostumbrados a sus sobreactuaciones las cuales sí son convincentes en según que películas y según que papeles, pero en algunos momentos de autómata sus aspavientos están de más.

La película intenta sin mucho éxito tratar la moralidad de la vida e inteligencia artificial y de dotar de sentimientos a la robótica y de la ética de sus creadores. Como pasa en Yo Robot o con los replicantes de Balde runner empiezas a empatizar con las máquinas y parece que nuestra simpatía irá cada vez a más, pero ahí se queda, a medias tintas y al final te sientes un tanto frío hacia ellos lo cual hace que te sientas de la misma manera hacia la película en si.

No negaré que tiene algo, la película a pesar de no ser novedosa en su planteamiento empieza con una buena base y con una buena historia, no es mala, pero le falta gas, ritmo, y se queda sin fuelle en su desenlace que no convence ni satisface.
        
            