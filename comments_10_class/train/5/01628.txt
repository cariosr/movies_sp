
                      Me transmite un erotismo esperpéntico, siento como si Kubrick estuviera expresando que no tiene nada que contar, que solo quiere provocar y enseñar desnudos, cuanto menos variopintos. Todavía no sé por qué lo único que he sacado en claro es que Cruise y Kidman juegan un papel bien interpretado, digno, en medio de una historia tan rara como obsesiva.
        
            