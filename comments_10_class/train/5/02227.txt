
                      Aquí el diablo no habla, el mercado habla. Y no hay nada de malo en eso: el problema es que no hay nada realmente perturbador, potencialmente luciferino.
        
            