
                      Técnicamente impresionante. Mis hijos se quedaron pasmados. Pero le falta un poquito de poesía y de alegría a este guión demasiado serio como para ser una obra maestra. También le falta mucho a este opus la antigua voz de Martín. Es una lástima, empezaba bastante bien. ¡Pero a los niños les va a encantar! ¡Y, claro, Pixar se dirige ante todo a ellos!
        
            