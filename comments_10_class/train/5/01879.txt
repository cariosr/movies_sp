
                      La película es una muestra no muy diferente de lo que ya nos tiene acostumbrado a mostrar Len Wiseman, una explosión de adrenalina y escenas de acción sin mucha sustancia. Me atrevo a decir que Arnold Schwarzenegger tenía mejores diálogos en la primera versión.
        
            