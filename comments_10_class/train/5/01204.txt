
                      Tras muchas expectativas por todo lo oído, leído y los trabajos anteriores de Coronado, así como las películas de Enrique Urbizu, me llevé una ligera decepción, pues considero que la película carece en todo momento del ritmo adecuado y propicio para la trama desarrollada, la cual creo que se enmaraña de una forma un tanto ilógica en más de un momento.
Lo mejor, seguramente, la interpretación de Coronado en el inicio de la película, pues luego va decayendo y perdiendo fuerza en los gestos y miradas, cosa que no ocurría por ejemplo en La Caja 507, aunque ya sabemos que no es bueno comparar ni debieramos hacerlo.
Por otro lado, en mi opinión, el resto de interpretaciones deja mucho que desear, incluida la repesca de Pedro Mari Sánchez.
En fin, creo que el guión habría dado para más si se hubiera hilado mejor, pues esa combinacíón de mafias colombianas de la droga con prostíbulos y celdas islamistas que plantean ataques terroristas no llega a calar y hacerse creíble en absoluto.
        
            