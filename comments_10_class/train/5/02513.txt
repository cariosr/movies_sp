
                      Dios Mío, ¿Pero Qué Te Hemos Hecho? (2014).
Christian Clavier, quien se hiciera famoso hace dos largas décadas con cintas como Los Visitantes, protagoniza esta divertida comedia francesa que no me extraña haya sido un éxito de taquilla en el país galo. No para uno de reír. Un buen modo de acabar el año viejo y comenzar el nuevo.
Sin llegar a la calidad de muchas de su estilo "made in Hollywood", aunque muy superior a otras, la película trata con humor el asunto de la inmigración y el racismo que hasta los mismos inmigrantes sienten hacia otros inmigrantes. Una crítica desenfadada a la europea. Recomendable.

Raúl Cabral.
        
            