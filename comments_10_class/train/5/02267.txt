
                      El niño se ve aburrido. Dos películas en la sin vida de la franquicia de Harry Potter, y la pequeñísima imagen de Danny Radcliffe de perpetua sorpresa, deleite y miedo se han mezclado en una larga mueca. ¿Puedes escuchar el "¿Cuánto nos pagan, mamá?" ¿Susurros a través de sus dientes perfectos en cada escena? Puedo.

Atrapado en un set durante años con Chris (Solo en casa) Columbus le hará eso a un niño. Columbus pasa a través de otro tedioso Pottering con toda la urgencia de una babosa vomitada, una mordaza de sobreviviente en la película, y toda la imaginación que su equipo de efectos especiales puede prestarle.

En Harry Potter y la Cámara de los Secretos, ese equipo hace volar un viejo auto, los magos de los niños se acercan a escobas preparadas y una araña gigantesca, pero en realidad no. La araña está en un rincón oscuro. Todo ese dinero para la amenaza, y no pudieron hacer que sus labios se movieran.

¿En la oscuridad? Yo también. A diferencia del primer Potter, este, que tiene que ver con la historia de fondo de Harry y un poco de historia semi-antigua en la Escuela Hogwarts, requiere que el espectador lea el libro primero. A pesar de un estilo de adaptación que se inclina hacia lo literal, seguramente cada página en J.K. El libro de Rowling aparece en estas dos horas y 41 minutos: Columbus pierde el hilo de lo que los acontecimientos aquí tienen que ver con el pasado de Harry y su futuro, su destino como el mago súper, algo proclamado por su nacimiento.
        
            