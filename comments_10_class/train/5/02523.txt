
                      Aunque al principio pinta para ser otra más , llega a ser interesante por momentos. aunque cae en el mismo pecado que otras películas de este estilo, hasta que entiendan que deben tener efectos muy buenos, banda sonora penetrante, y un buen enfoque, sus villanos sólo darán risa en vez de atemorizar, esto desenfoca totalmente la trama y hace que de alguna forma dejes de prestar cierta atención. un desenlace bueno. rescatando lo último que tenía el film. recomendada para pasar el rato
        
            