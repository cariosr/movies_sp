
                      Otra adaptación de un cómic, lejos de parecerse a la saga X-Men. Se queda una película entretenida con 4 actores conocidos que interpretan un film en el que desarrollan habilidades "fantásticas" y un villano que no está dispuesto a consentirlo, movido por la envidia y el deseo de venganza. Poco más se puede sacar de una película meramente entretenida, cuya secuela quizás fue igual de acertada, pero ese es otro asunto. No dejan de ser superhéroes  que serás puestos a prueba y evidentemente tendrán que salvar el mundo.
        
            