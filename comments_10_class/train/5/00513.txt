
                      Tienes que amar a esas ardillas. Los adorables roedores sin pantalón hicieron su primera grabación en 1958, cuando Ross Bagdasarian escribió "The Chipmunk Song" para ellos.

La pista de la novedad navideña (las voces grabadas se aceleraron para sonar como niños en helio) fue la número uno con una bala. Luego, en 1983, los pequeños reaparecieron y entraron en la vida animada como un dibujo animado de la mañana del sábado.

Ahora los peludos moppets se dirigen a la taquilla de boffo en una película con verdaderos humanos en vivo. La pantalla grande es el medio adecuado para este trío. A través de la magia de CGI, son más adorables que nunca, la banda de chicos más lindos desde Jackson 5.

Jason Lee (Mi nombre es Earl) se ajusta bien a la parte de Dave. Es un tipo desgraciado, como Earl. Cuando tres ardillas de ardilla con ambiciones antropomorfas se mudan a su casa, la vida aburrida de Dave se anula. Pero después de que él escribe "The Chipmunk Song" para ellos, Dave comienza a ver un futuro para sí mismo en el negocio de la música.
        
            