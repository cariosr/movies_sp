
                      La gente que paguemos por ver Torrente, ya sabíamos más o menos lo que íbamos a ver. En un país que triunfan los programas y las revistas del corazón, donde una selección de balompié, une a propios y extraños, es normal que un producto como Torrente, sea el más taquillero del cine español.

Sin ritmo, sin acción, sin un humor realmente efectivo, “Torrente 4” se revela como lo que podría haber sido un especial televisivo de final de temporada.

Como en los otros filmes, abusa de las escenas de sexo, y de las bromas guarras.

Humor grueso, incorrección política, un desfile interminable de cameos españoles, 3D, unos créditos al más puro estilo de "James Bond”, da a su público lo que pide, más de lo mismo. Es mucho mejor y más divertida que la tercera parte y compite con la segunda por el puesto de plata en el ranking de esta saga.

Me esperaba algo más de esta película
        
            