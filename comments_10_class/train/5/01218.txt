
                      Ni sorprende tanto como su predecesora, ni aprovecha los alargados minutos de metraje. El espectáculo y los efectos especiales son mayores, sí, pero el túnel por el que el espectador se mueve no alcanza un final que valga la pena. A ello se le suma el indefinido papel antagonista de Radcliffe, elección poco agraciada por parte del cásting. El guion flojea en el mismo espacio y tiempo que la primera entrega, pero esta vez duele más, pues los magos ya no esconden tantos trucos.
        
            