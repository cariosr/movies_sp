
                      Independence Day: Contraataque (2016).
Vuelve R. Emmerich con su invasión alienígena, sus efectos especiales, sus personajes de la primera entrega y su presidente salvador del mundo... Ay, señó, llévame pronto...!

Un poco más de lo mismo, aunque con algunos guaperas añadidos, como el pequeño de los Hemsworth.

Entretenida como la primera, y recomendable para los que estén inmunizados a los discursos patrio de estos chicos guiris...

Raúl Cabral.
        
            