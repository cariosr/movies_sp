
                      Es una película que deja tanto que desear por sus posibilidades, que todo lo malo (bajo perfil del guión, efectos que no provocan interés, confuso montaje...) eclipsa lo bueno (buena actuación, maestría del director). Esperaba mucho más de ella. Quizá ese fue mi error, ir con tantas expectativas.
        
            