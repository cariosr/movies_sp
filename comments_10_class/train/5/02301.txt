
                      El primer Kick-Ass sigue siendo una de esas pequeñas gemas en el léxico cinematográfico, una película que la mayoría de los espectadores tropezaron por accidente o boca a boca. Una cuestión de tiempo, tema y un papel de estrella de un niño de 11 años de mala boca: Kick-Ass fue esa pequeña película de cómic que pudo.

Avancemos unos años y ahora viene la secuela. Esta vez, vemos que Dave (Aaron Taylor-Johnson) todavía está en la escuela secundaria y, después de decidir inicialmente colgar sus medias Kick-Ass, ha decidido unirse al grupo de superhéroes "Justicia para siempre". Liderar este grupo no es otro que un Jim Carrey casi irreconocible como Coronel Stars.

Mindy (Chloe Grace Moretz), por otro lado, decide desde el principio que necesita retirar a Hit-Girl y cumplir una promesa a su tutor, Marcus (Morris Chestnut). Su promesa de olvidar las enseñanzas de su difunto padre, Big Daddy, y disfrutar de la vida simple de una adolescente normal.
        
            