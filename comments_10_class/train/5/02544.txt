
                      Cuando Streep, Walters y Baranski comparten la pantalla, ¡Mamma Mia! cobra vida y gana su signo de exclamación. El resto es la alegría forzada empujada al punto de ruptura.
        
            