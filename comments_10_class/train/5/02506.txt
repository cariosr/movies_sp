
                      Típica película de pareja de policías, solo que en este caso aderezada de un mundo fantástico, en el que con los humanos conviven orcos, elfos o hadas. A la acción que se espera de este tipo de "buddy movies" se añade por tanto la magia. Si bien resulta entretenida a ratos, la mezcla no enriquece el film y se antoja innecesaria, no diferenciándose de otros productos de similar corte más que en eso.
        
            