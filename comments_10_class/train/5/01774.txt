
                      Con el paso de los años, a Jumanji se le ven aún más las costuras que en su momento eran patentes: Los animales hechos por ordenador aún resultaban demasiado artificiales y eso le resta mucho. La película en sí resulta entretenida con el siempre genial y solvente Robin Williams, aunque en ésta no pudiese resaltar excesivamente. Aún así, es entretenida
        
            