
                      Train to Busan es una sobrevalorada pelicula más de zombies, que se han empeñado en revalorizar y una vez que la visionas, solo te deja en un estado de indeferencia como si la hubieses visto en televisión y con anuncios...

Despues de ver varios "28..", "Resident Evil", "Walking Dead", "Rec" y un largo repertorio de titulos: "Guerra Mundial Z", "Extiction", "Dead Rising", y otro largo etcetera de peliculas donde se mezcla el terror y la comedia, esta pelicula solo es una pelicula más de zombies y no aporta nada nuevo.

Pero es que aqui nos empeñamos en alabar cualquier cosa que nos llegue de los mercados menos comerciales y queremos destacar que somos más alternativos si estamos en la onda que se nos dicen: "es la mejor pelicula de zombies..." "...una brillante apuesta..." "...una nueva forma de renovar el género..." Nada de esto lo encontras en la pelicula.

Además de no tener apenas efectos especiales, y los pocos que encuentras son los más comunes filmicos; solo tiene el maquillaje para caracterizar las oleadas de zombies que nos muestran.

Si hay algo que la pueda salvar, podria ser el descubrir nuevas formas de presentar a los muertos vivientes, pero es que si no fuera por eso, menos todavia tendria que decir de ella.

Aparte del drama propio que debe llevar toda pelicula de este tipo que es la reducción progresiva de protagonistas en manos de los infectados, y en eso cumple los roles, también llega a exponer puntos comicos, que no dejan de ser absurdos y que no te dejan situar una realidad de un tipo ú otro en las escenas que se desarrollan.

No creo que nadie la recuerde en un mes....
        
            