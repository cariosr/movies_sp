
                      Se centra en la misteriosa muerte de 2 mujeres en el mismo sitio y una investigación donde se sospecha de los maridos de ambas. Todo el proceso es llevadero donde pueden apreciarse contradicciones por partes de los acusados y sospechas cruzadas pese a que nada es suficiente para completar el rompecabezas. Un buen thriller, interesante, fácil de seguir y con una buena ambientación sesentosa, sin embargo el final deja mas dudas que certezas dejando un sabor agridulce. Regular para mi gusto.
        
            