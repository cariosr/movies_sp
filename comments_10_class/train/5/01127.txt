
                      Cuesta creer que las riendas de 'Passengers' hayan recaído sobre Morten Tyldum, tras su éxito en 'The Imitation Game'. El argumento de esta película sabe a poco y se nota: de sus dos horas de metraje, más de una se emplea en desarrollar la tensión sexual entre Lawrence y Pratt. Aunque, siendo sinceros, son estas dos estrellas hollywoodienses las que salvan a una historia rodeada de una buena estética galáctica, independientemente de que el espectador se vaya del cine con ganas de más.
        
            