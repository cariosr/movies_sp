
                      El Día que Vendrá (2019).
“Bombardeamos más Hamburgo en un fin de semana que los nazis Londres en toda la guerra."
Una actriz británica (Keira Knightley), un actor australiano (Jason Clarke), uno sueco (el imponente último Tarzan, Alexander Skarsgard, hijo del veterano Stellan Skarsgard) y una joven actriz alemana en el papel de la atormentada hija de este último, forman el plantel principal de este drama romántico ambientado en los primeros meses de la posguerra de la última contienda mundial.

“Es ‘die nullstunde’: la hora cero. Todo puede empezar de nuevo..."
La presión, las pérdidas familiares de los protagonistas en la citada guerra y unas pasiones tan irresistibles como poco creíbles, dan lugar al enésimo triángulo amoroso en la Gran Pantalla. Jason Clarke, al que podemos ver estos días en la revisión del “Cementerio de Animales” de Stephen King, es el coronel que debe ocuparse de "algo más" que de reconstruir Hamburgo, ciudad donde se ambienta y rueda gran parte de la película (la otra es Budapest).

Aunque la música, la fotografía y el diseño de producción ayudan sensiblemente al desarrollo de la historia, lo cierto es que no pasará por ser el drama del año, aún estando detrás del proyecto Ridley Scott.

Raúl Cabral.
https://www.filmaffinity.com/es/film776289.html
        
            