
                      Con buena intención nos cuenta cómo una pareja artística, cantantes, separados hace unos años han de volver a unirse con el fin de pregonar el inicio de las fiestas de una localidad rural. Cuenta con momentos graciosos, cuenta con una cuidada ambientación retro y busca hacernos pasar un buen rato. Al ser una película especial gustará a unos y a otros no. Sencillamente se deja ver.
        
            