
                      Esperar 15 años para contarnos el origen de una de las más grandes sagas del cine tiene sus méritos, pero también sus riesgos, y hacerlo con "Star Wars" fue un poco intentar rizar el rizo y continuar explotando el universo de las taquillas.
Con unas interpretaciones decentes y un argumento que no daba para más de dos horas de metraje, en 1999 llegó la cuarta parte de la saga que a la vez fue la primera en el orden cronológico... Os dejamos nuestra crítica completa (sin spoilers, evidentemente)
        
            