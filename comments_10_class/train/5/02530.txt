
                      Película post-apocalíptica ambientada en Australia en una epidemia zombie. Alejada de la acción, se centra más en el thriller dramático del protagonista, su mujer y su pequeña hija en ese ambiente hostil e inhóspito. Destaca la estupenda actuación de Martin Freeman y algunos momentos de cierta tensión, pero no es una película que vaya a marcar en el género
        
            