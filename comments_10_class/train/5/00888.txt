
                      ¡Bueno!. esto es Michael Bay en estado puro. No sé si llamarla película, no al menos lo que yo entiendo por tal. Vaya por delate que me gustan las películas de efectos especiales. De hecho pienso que es una de las pocas razones que hay para pagar los altísimos precios que tienen hoy los cines, poder ver un espectáculo que no puedes ver en casa. Pero es que esto ya es otra cosa, un film larguísimo en que se suceden una increíble retáila de secuencias de acción una tras otra, muy bien creadas, incluso diría que espléndidamente, pero todo tiene un límite. No es factible aguantar 165 min de lo mismo. Con 90 minutos sin duda le hubiera puesto un 3,5.
    En cuanto al elenco protagonista, Mark Walberg, es Mark Walberg, Kelsey (Fraiser) Grammer no está creible, y Sophia Myles hace lo que le piden; chica monilla permanentemente asustada.
    No merece la pena, con lo mucho que se han gastado en efectos especiales y lo poco en guionistas.
        
            