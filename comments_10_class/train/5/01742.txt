
                      Me esperaba bastante más tras leer las críticas de la prensa. 
Muy floja, el guion deficiente con muchos altibajos y momentos aburridos. Las escenas de acción regulares, no aportan nada nuevo, muchas explosiones y efectos especiales pero se echan en falta peleas cuerpo a cuerpo reales. Daniel Craig como en todas las peliculas de Bond, completamente anodino e inexpresivo, sin esa picardia que caracterizaba a Sean Connery o a Pierce Brosnan. Bardem impecable una actuación que salva la película del fracaso, pero que no la pone a la altura de aquellas peliculas del auténtico Bond, James Bond.
        
            