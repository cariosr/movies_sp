
                      La más apocalíptica de la saga y por lo tanto la más violenta. Esta tercera entrega no tiene nada que ver con una película destinada a los más jóvenes... Remontando el nivel con respecto a las anteriores, la película se pierde, por desgracia una vez más, en la sucesión  de  interminables escenas de acción y no se priva en pescar público masculino gracias a una improbable chica cañon como novia del héroe. Todos los ingredientes de una receta fácil y sin riesgos.
        
            