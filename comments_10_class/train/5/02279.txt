
                      “Leprechaun” fue un duende irlandés bastante macabro, con un sentido del humor muy negro y que no dejaba títere con cabeza. Protagonizó una saga de 6 películas, de muy dudosa calidad, allá por los años 90, pero que hizo crearse una legión de fans que esperaban ansiosos una nueva entrega de sus sangrientas aventuras.

Al igual que le ocurrió a Freddy Krueger, a Jason Voorhees o a Michael Myers, qué mejor que traerle de vuelta en una nueva película que un reboot de la saga. Un reboot, según sus creadores “con un aire renovado y mas oscuro y sangrienta que nunca”. Y estaban en lo cierto, pues “Leprechaun: el origen” es completamente una nueva película donde pierde por completo a aquel duendecillo malhumorado. Aquí, tenemos una especie de monstruo con orejas puntiagudas que hace la estancia imposible a un grupo de mochileros por las Villas de Irlanda. Además, le añadimos a una familia que da algo de grima, y que nos recuerda en muchas escenas a la familia Hewitt de “La matanza de Texas“.

Una dirección que deja bastante que desear con un abuso de planos con infrarrojos simulando lo que ve el duende y bastantes escenas en cámara lenta en plenas persecuciones que no pinta absolutamente nada. Eso, junto con una banda sonora absurda (daría mas sensación de suspense un chirriante disco de gaitas escocesas)  y que es relegada a un segundo plano donde casi ni podemos escucharla.

Aún así, tenemos una película de terror de serie B bastante angustiosa que nos recuerda a otras como “The descent” o “The cave“. Unas muertes sangrientas y un par de sustos hacen pasable el nuevo reinicio de esta saga, que al menos, se deja ver.

Lo mejor: que no tengas la sensación de estar viendo un producto de bajo presupuesto (aunque lo sea)
Lo peor: no respetar lo más mínimo al original “Leprechaun”.
        
            