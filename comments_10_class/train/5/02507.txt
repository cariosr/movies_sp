
                      Aceptable. Una certera muestra de como eran las dinámicas familiares en EEUU allá por la década el 50, donde el hombre, era quien proveía y decidía por el resto de sus integrantes. En este caso un hombre de color, con un duro pasado, antepone sus valores patriarcales y egoístas por encima de las voluntades individuales de su esposa e hijo. 
Sin ser algo sobresaliente, me pareció una buena historia, algo lenta y MUY conversada para mi gusto, pero bien contada y con muy buenas actuaciones.
        
            