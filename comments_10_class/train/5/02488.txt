
                      El Pacto (2018).
“La tentación de volver a hacerlo es demasiado grande. El precio, demasiado caro..."
Belén Rueda protagoniza su cuarto largometraje de este año, y una vez más en su extensa carrera vuelve al terreno del thriller sicológico. Como es sabido, nuestro país es puntero en Europa en el cine de terror y suspense (que no en otros géneros...), pero no será éste un gran ejemplo de ello. 

“Nadie debería decidir sobre la vida de otra persona."
La historia mejora en su segunda mitad, pero no aprovecha la premisa con la que cuenta: el sacrificio de una vida por otra. Lo cierto es que no consigue enganchar del todo a pesar de contar con estupendas interpretaciones (sobre todo la de Rueda). 

Por otro lado, la fotografía parece estar a cargo de un invidente... más oscura no puede ser. Ni en las tomas diurnas se ve bien del todo... Y en cuanto al realizador, David Victori, le faltan tablas (¿talento?) en mi opinión. Acostumbrado a dirigir y guionizar producciones B, es normal el resultado del producto en cuestión. Como la crítica y el público, le doy un aprobado raspado.

Raúl Cabral.
https://www.filmaffinity.com/es/film188636.html
        
            