
                      Impresionante escena inicial, lo mejor del film, Iñarritu repite ese tipo de entrada espectacular, que tuvo, para mi gusto la malisima Birdman, Luego las cosas no caminan. La escena del oso alucinante pero poco creíble. El villano de turno es estupendamente interpretado. Di Caprio destaca por el esfuerzo físico de sus escenas, en una película que tiene pocos diálogos para el. Sobrevalorada dirección, que tal vez sea premiada. Solo regular.
        
            