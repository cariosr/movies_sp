
                      La Monja (2018).
Nuevo “Spin-off” de la inagotable saga Expediente Warren (concretamente del Caso Enfield). 
Unos pocos sustos, bastante oscuridad, un ente maligno.. y ya tienes montado un drama de terror sin mucho que más que aportar...

La hermana pequeña de la gran Vera Farmiga (la señora Warren en las otras partes) encabeza el reparto de esta cinta junto al mejicano Demián Bichir (uno de los Odiosos Ocho). Ambos- religiosos enviados por el Vaticano- intentarán resolver el caso de una monja suicida en la mitad del pasado siglo, época en la que se ambienta la historia.

Quizá la más ‘flojita’ de todas las partes, entretiene lo indispensable para acabar de verla (hora y media pasa rápido), pero lo que es dar miedo, lo cierto es que no da demasiado aún haciendo ciertas concesiones (la existencia de espíritus endemoniados y de dioses en los que gran parte de la población mundial aún cree, y la ausencia de un solo policía en un caso como el que nos ocupa...).

Raúl Cabral.
https://www.filmaffinity.com/es/film211433.html
        
            