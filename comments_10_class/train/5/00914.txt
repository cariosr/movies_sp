
                      Para mi gusto una película muy lenta, bien hecha, pero que para los que no recomiendo para nadie que no le gusten las películas de vida extraterrestre y con poco movimiento. No se puede decir que la película sea mala, porque se nota que está trabajada, pero yo mientras la veía miré bastantes veces el reloj, se me hizo pesada, y realmente todo el rollo que meten con los extraterrestres no sirve casi para nada. No la recomendaría a casi nadie, salvo a los muy interesados en vida extraterrestre.
        
            