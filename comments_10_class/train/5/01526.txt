
                      El inicio de la película parece prometedor: al más puro estilo de las películas de James Bond.   Sin embargo, a medida de que avanza la trama, va perdiendo el hilo conductor: se vuelve aburrida, predecible y solo pides que ya termine.

Añádele a que la chica Bond es una rubita sosa, sin mucho encanto. Hubiese sido estupendo que Monica Bellucci tuviera una actuación más destacada.

Lo rescatable de la película: las locaciones y la fotografía.

Si esperas ver una película como Skyfull, déjame decirte que saldrás decepcionado. Si no lo crees, míral, y, después, hablamos.
        
            