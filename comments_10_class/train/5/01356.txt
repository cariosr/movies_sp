
                      Aceptable. Aclaro que jamás leí el manga ni vi el animé de esta saga. Por ello, siendo una novedad para mi, les puedo decir que me pareció un argumento novedoso y llevadero sin tratarse de ninguna joya ni mucho menos. Similar a "Destino final" con la diferencia que las víctimas fatales, malvivientes en su mayoría, son seleccionados por el protagonista a través de la anotación de su nombre en un libro mortal. Entretenida como para pasar el rato.
        
            