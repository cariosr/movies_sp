
                      Soporífero romance sostenido en el físico de los protagonistas.
 Más que aventura espacial es una exhibición de amor "pasteloso", que transcurre a ritmo de tortuga.
 Sólo en el desenlace hay acción, antes es todo flirteo y cierto dilema moral de efectos efímeros. 
 En definitiva: un aburrido film más digno del género romántico que de la ciencia-ficción.
        
            