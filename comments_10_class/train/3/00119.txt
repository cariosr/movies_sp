
                      Sin rendir cuentas ni ante Dios

“Drive” nos descubrió a un sibarita de la imagen, un entusiasta de los travellings frontales y laterales, las escenas con ritmos sintetizados y la violencia gráfica para muchos insoportable e innecesaria. Pero aquella joya, un homenaje al thriller ochentero que ya es toda una obra de culto, era solamente un espejismo de lo que es realmente su director. Porque su condición de delicatesen retro del celuloide fue atribuida gracias a que se trataba de una versión suavizada de su creador, un producto destinado al consumo comercial y asimilable por un sector más amplio de la audiencia. 

“Only God Forgives” vuelve a ofrecer un muestrario de las pulsiones de Nicolas Winding Refn. Estéticamente es brillante, como siempre. La fotografía de Larry Smith y la banda sonora de Cliff Martinez son brillantes, y sus ansias estetas están al servicio de una nueva historia de venganza, de un cruce entre el western crepuscular de Ford y Leone y el cine de yakuzas y samuráis, una especie de thriller coreano con algunas pinceladas kubrickianas y lynchianas en la composición de planos. 

El director ya no tiene nada que demostrar. “Drive” le abrió las puertas del star system de manera definitiva, y ésta es su primera película cien por cien propia desde hace unos cuantos años, una cinta desbocada y ensimismada que no tiene que rendir cuentas ante nadie, ni siquiera ante Dios. Y es precisamente esta condición de película nada complaciente más que consigo misma lo que juega en su contra. Recoge tanto lo mejor de Winding Refn, su sello personal en la realización, como lo peor, un hermetismo derivado de un universo tan propio que no hace concesiones al espectador. O entras en él, o te quedas fuera.

La culpa la tiene ahora, especialmente, un guión muy simple, pero al que su director pretende dotar, tirando de montaje y dirección técnica, de una profundidad de la que en realidad carece. No hay hondura en sus personajes, ni en esa fallida relación madre-hijo, supuesta piedra angular de la trama. Ni siquiera hay doble fondo en la expresión de un Ryan Gosling que está de paso y que va camino de convertirse en el intérprete monótono y pétreo en el que jamás imaginamos que se transformaría en sus prodigiosos comienzos, al que se come crudo una estupenda Kristin Scott Thomas, madre amenazante, posesiva y frágil a la vez.

Lo que queda es un thriller inconexo, muy mal explotado a nivel argumental, que deja la sensación de que el guión no está a la altura. Y, eso sí, puro Winding Refn, el mismo de las también poco digeribles “Bronson” o “Valhalla Rising”, y que me temo que no volverá a regalarnos otro “Drive”. Un cineasta que, eso sí, sirve imágenes de una extraordinaria belleza pese a su crudo contenido. Aunque no siempre los ejercicios de estilo son suficientes para sostener una película.

A favor: Kristin Scott Thomas, y el toque de su director
En contra: la casi nula profundidad bajo su precioso envoltorio
        
            