
                      Todos tenemos películas que recordamos con cariño, sobre todo aquéllas que hemos visto en nuestra infancia o adolescencia y que nos acercaban a un mundo de mayores, lejos de la animación inocente y para todos los públicos. Según pasa el tiempo, los recuerdos de esas películas se transforman en memorias dignas de culto y admiración, tremendamente sobrevaloradas, pero con un valor sentimental impoluto y creciente.Desafío total es una de esas cintas que para mí, y para tantos de mi generación, suponen mucho, más por lo que nos molaron en su día que por su verdadera calidad cinematográfica.

Cuál fue mi sorpresa hace unos meses al descubrir que iba a llegar a la gran pantalla un remake de la misma, protagonizado por Colin Farrell, Jessica Biel y Kate Beckinsale. ¡Qué emoción! ¡Qué ganas! ¡Qué iluso e inocente!

Como cuando alguien molesta o perturba a un ser querido, la sensación al ver la nueva Desafío total es como una puñalada en lo más profundo del corazón. En condiciones normales protestaría porque, en comparación, me parece más mítico Arnold que Colin, más atrevido Verhoeven que Wiseman o más sensual Sharon que Kate. El resto de las cosas no se pueden comparar realmente, porque multitud de elementos míticos de la película original no existen. Por no existir, en la trama no existe ni Marte.

Intentando ser justo, esta película, de haberse llamado de otra manera, podría pasar por una simplona y visualmente correcta cinta de acción futurista. O sea, en la misma categoría que tantas y tantas películas olvidadas e incluso descatalogadas a los pocos años de ser editadas. Lo que pasa es que también es justo tener unas expectativas a la altura de lo que fue la película original.

¿Acaso nos toman por tontos? Pues sí, realmente lo hacen. Como dijo en su día Samuel Goldwyn ”A esta industria no se la denomina show-art. Se la denomina show-business“. Pero, con estas altas traiciones al espectador, lo único que consiguen es decepcionar a la clientela fiel. Al final, ¿quiénes son los tontos? Ellos, porque con el horizonte puesto a largo plazo, invirtiendo tan poco en los aficionados al cine -sobre todo al cine comercial- se van a quedar sin métodos de recaudación en favor de una auténtica industria de la piratería. Casi mejor, porque si lo recaudado lo invierten en hacer basuras como ésta, mejor que se lo donen a Stanley.

goo.gl/cfhFa

sígueme en twitter: @jgripweed
        
            