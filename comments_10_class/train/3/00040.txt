
                      Floja. Lenta. Guión errático. Sin chispa. Lo mejor: los actores que hacen lo que pueden para que este film no sea un desastre absoluto.
        
            