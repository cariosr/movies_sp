
                      Pues lo siento en el alma porque le tenia mucha fe a esta película, pero... Me ha parecido insufrible. 

Para empezar, le he encontrado al menos tres posibles errores o bien flaquezas de guión, cosa que me parece bastante serio teniendo en cuenta que el guión és típico-tópico-simple como el mecanismo de un chupete, y bueno, algunos dirán: "És una peli para crios, no seas tan duro", ok... Pero que demonios, tampoco hay que subestimar tanto a los crios. 

Sin embargo, al margen de las flaquezas de guión, lo que realmente me ha matado la experiencia cinematográfica ha sido la cantidad ingente de fan service y la cantidad de referencias a "memes" o chistes viejos de internet sobre el personaje. Vamos a ser sinceros, el chiste de: Porque soy Batman, ya está muy pero que muy trillado, los raps de Batman también... Y bueno, que decir que tanto todo el rap, como el resto de temas musicales han sido un verdadero dolor para mis oidos, no me recuperaré... Menuda banda sonora más sumamente irritante, por dios, que mal... 

Para mi, esta és una película mala con ganas, y la única cosa que saco en claro és que la crítica especializada y yo jamás nos iremos de cañas, no podriamos congeniar jaja...
        
            