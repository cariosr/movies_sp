
                      La estrella y media se la doy a la actriz Natalie Portman por crear el papel de Nina. Pero la dirección y el guión se pierden en la complejidad de un relato que debería ser más íntimo y menos excentrico.
        
            