
                      Frankie and Johnny es un musical romántico dirigido por Frederick de Cordova, director de 'Por el amor de Mary', 'El halcón del desierto' y 'Lecciones de amor en Suecia'.

Frankie (Donna Douglas) y Johnny (Elvis Presley), además de ser pareja, son los protagonistas de una función musical a bordo de un barco que viaja a través del Mississipi.
Johnny es un supersticioso adicto al juego, que se gasta todo su salario apostando. Tras visitar a una adivina gitana, este decide seguir sus consejos e ir tras una chica pelirroja que le de suerte.
Su relación con Frankie peligra cuando ella se da cuenta de que no tienen futuro si Johnny sigue jugando.

Lo único bueno de la película son las canciones de Elvis. La trama es insulsa, los diálogos aburridos y las interpretaciones poco creíbles.
Tratándose de cine, Elvis no ha tenido mucho éxito, debido a sus papeles en cintas como 'Persigue tus sueños' o ésta misma.
Una película hecha casi exclusivamente para los fans del cantante que sean lo suficientemente incondicionales como para verla entera.
        
            