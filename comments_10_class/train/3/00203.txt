
                      Lasse Hallnosnos trae esta adaptación del Best Seller Sueco del mismo nombre de Lars Kepler, que cuenta la trama al rededor de la investigación de un crimen múltiples.

En mi humilde opinión El Hipnotista es lenta, carente de interés y soporífera. La trama no engancha, el guión no tiene ritmo y aunque al principio parece que va a arrancar con algo interesante no se decide a hacerlo.

Sus protagonistas son Tobias Zilliacus,  Mikael Persbrand y Lena Olin.
No se como será en V.O. pero el doblaje es pésimo, monótono y sin emociones. Las interpretaciones son igual de frías, ya que ninguno de los protagonistas consigue trasmitir nada de nada, la única que se salva en este aspecto es Lena Olin quien demuestra que tiene bastantes tablas y se desenvuelve bien. Los diálogos son bobos en su mayoría. Las conversaciones que tienen los policías entre ellos parecen de tebeos de risa, de lo más simples y bobas.

Había leído buenas críticas pero personalmente no se por donde pillar la peli ya que me aburrió muchísimo y no le veo nada positivo.
Así que pasemos página y veamos otra cosa.
        
            