
                      Si hay una adjetivo para definir a esta película es inenarrable. No creo que haya nadie que sea capaz de entrar a verla y ser capaz de explicar después lo que ha visto. Si lo hay, desde luego, se merece un buen premio. 

Transformers es un totum revolutum de todos los elementos que pudieron imaginar los guionistas, mezclados sin ton ni son y combinados con diálogos rápidos y cortes continuos. Eso sí, entre esos elementos se olvidaron de añadir un argumento. La película no tiene ni ton ni son, pero encima está mal narrada. Menos mal que los efectos especiales son buenos, son lo mejor de la película después del momento en el que acaba. 

Por si fuera poco, una película que ya se hace larga a los 10 minutos, dura dos horas y media de suplicio. Será por eso de que si no quieres arroz toma dos tazas. 

En fin, que me la tuve que tragar porque la vi trabajando, pero que me hubiera ido muy a gusto a los cinco minutos de comenzar. Así que, si estás pensando ir a verla, tú que puedes, huye como si no hubiera un mañana. Te ahorrarás dinero y. sobre todo, el sufrimiento de verla.
        
            