
                      Si la verdad duele, empecemos predicando los espectadores... La película es una auténtica bazofia. Tiene un buen guión, la historia es interesante, pero está mal adaptada. Es lenta, los diálogos son insulsos y las actuaciones no terminan de convencer. Estaba deseando que acabara o que me renovaran las palomitas. En fin. No la vean.
        
            