
                      Demasiado larga. Técnicamente impecable. Visualmente increíble, pero tan, tan inverosímil que ya cansa. No se le puede dar demasiada seriedad a una temática así, pero no se, al menos la primera parte tenía un punto de grandeza que desde la segunda parte empezó a perderse. Tampoco me gustó el diseño de los rostros, aunque quisieran dotarlos de personalidad.
        
            