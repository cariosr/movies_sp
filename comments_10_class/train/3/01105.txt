
                      Con una historia de la maestra del suspense Agatha Christie, con un elenco de protagonistas y secundarios tan estelar y con la dirección de Kenneth Branagh, cabría esperar mucho más de lo que al final nos encontramos. Se hace muy pesada, no te hace conectar ni resulta lo atractiva que cabría esperar ni mucho menos. Da la sensación de haberse desperdiciado un buen potencial con la materia prima que se tenía y que el desarrollo es muy aburrido.
        
            