
                      La película esta muy bien filmada y muy bien actuada (sobre todo la actuación de Mario Casas)
El problema es que la película se vuelve muy fantasiosa, contiene algunos guiños machistas y cae en clichés típicos de este tipo de películas.
La protagonista femenina carece de carisma lo cual es un grave error ya que la película gira casi en torno a ella.
La película pudo haber tomado un buen rumbo, pero decidió tomar el rumbo de una telenovela rosa.
        
            