
                      Es como ver una peli porno de pingüinos. Muy bien hecho y realizado, pero me da igual todo. Imposible empatizar con Tintín. No existe, no tiene vida: es completamente belga. ¿Quién es Tintín? Indy me emocionaba, sabía de dónde venía, a quién quería, quién era su padre. Tintín, como buen pingüino, me deja frío. Y sí, vale, muchas escenas de acción, pero sin personaje
, me he sentido como si estuviera mirando el tráfico. Da la impresión de que el guión de Tintín se ha escrito al tuntún.
        
            