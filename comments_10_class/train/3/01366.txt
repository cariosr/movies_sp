
                      Entretenida película de fiestas universitarias, pero está tan visto este género que ni sorprende, ni innova. Es un revuelto de todos los tópicos de este genero, tres amigos (el listo, el gracioso/tonto y el que se desmadra) que se van a graduar en la universidad y deciden salir de fiesta cuando no deberían y eso les llevará a meterse en multiples líos y momentos graciosos durante toda la película. Lo hemos visto mil veces, pero eso no quiere decir que sea una basura, cumple con su función, nada más.
        
            