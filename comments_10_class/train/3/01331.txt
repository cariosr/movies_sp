
                      Me considero un incondicional de Bruce Willis pero hay que reconocer que este actor ha participado en películas bastante malas y esta es una de ellas. La interpretación de él es totalmente apática, careciendo de entusiasmo durante todo el largometraje. Con un guión requetequemado y predecible sólo capta algo el interés del espectador las pocas escenas de tiroteos que tiene.
        
            