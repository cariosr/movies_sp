
                      Star Wars: Los Últimos Jedi (2017).
"La esperanza es como el Sol: si solo crees en él cuando lo ves no superarás la noche."
El realizador de Looper nos presenta el nuevo episodio de la tercera trilogía de la famosa saga en un tono quizá más dramático que su antecesora, con más batallas interestelares y con mayor trabajo por parte de los actores. Entre estos destacan la joven Daisy Ridley, quien participará en los tres capítulos de esta trilogía, John Boyega (exsoldado imperial), Oscar Isaac (piloto entregado) y unos recuperados Mark Hamill (quien solo se vislumbró tímidamente en la anterior parte), y la recientemente fallecida Carrie Fisher (eterna princesa Leia).

"Así es como ganaremos; no luchando contra lo que odias, sino viviendo por lo que amas."
La cinta no desmerece respecto a la anterior; factura impecable, nuevas y llamativas criaturas, mayor aparición del personaje del fantástico Andy Serkis (Snoke), y acción a raudales. Dos horas y media de entretenimiento como no podía ser de otra manera.

En la película abunda la iconografía de inspiración nazi y Samurai (Aquí y Ahora, como decimos en Karate), y un diseño de producción que nos recuerda a las dos guerras mundiales, tanto en el aspecto de cierto armamento como en los uniformes.

Sin duda, imprescindible para los amantes de La Guerra de las Galaxias a pesar de la elección del villano (Adam Driver, quien da menos miedo que Heidi... hasta cuando lleva casco).

Raúl Cabral.
https://www.filmaffinity.com/es/film668878.html
PD: La música de John Williams no es necesario que sea ni comentada; es magnífica.
        
            