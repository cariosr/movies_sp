
                      Aunque el director se deja llevar por recursos sencillos para meter al espectador en la piel del personaje, lo consigue. James Franco no actúa, lo vive. Muy recomendable.
        
            