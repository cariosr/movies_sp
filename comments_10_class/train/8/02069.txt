
                      El director argentino Fabián Bielinsky nos muestra en su primera película  un film muy equilibrado y al mismo tiempo emocionante. También muestra nos muestra la crisis económica que sacudía Argentina en aquellos años. Los actores realizan unas muy buenas interpretaciones, tanto Ricardo Darín y Gastón Pauls como la actriz Leticia Brédice. El guión es muy bueno y la banda sonora no desentona en la película.
        
            