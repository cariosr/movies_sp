
                      Muy buena. Interpretación espectacular de Anne Hatthaway y Hugh Jackman. Muy fiel al musical. Antes de ir al cine hay que saber que la peli es casi toda cantada en inglés con subtítulos en español. Mucha gente por lo visto no lo sabía y se salieron de la sala. Muy recomendable.
        
            