
                      Cuando fui al cine a verla no esperaba tampoco gran cosa, pero he de decir que me sorprendió. La historia se nota que esta mimada y cuidada al milímetro, sin flecos sueltos y a mí me gustan los temas sociales actuales, así que me ha encantado Igelak desde el principio hasta el final. Gorka Otxoa excelente, música vasca con grandes intérpretes... no le falta nada a la película. Yo la vi en euskera con subtítulos en castellano y sin saber euskera con mi edad me enteré bien de la historia, es fácil de llevar. Totalmente recomendada
        
            