
                      Película que dramatiza un hecho acaecido a principios del s. XX. Varios hombres son atraídos con promesa de ganar dinero con partidas de carta ilegales cuando, en realidad, les matan y roban. La película es un flash back que parte del día en que ajustician a los malhechores.
        
            