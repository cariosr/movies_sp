
                      La verdad es que la idea de la película no es nada nueva, ya en 2005 el escritor Antonio L. Galán Gall publicó una novela alrededor de este tema "¿Cual es el problema?". El ayuntamiento de una ciudad tiene que contratar a un "problemólogo" (delicioso descubrimiento, por cierto) para resolver a qué se debía que, tras una intensa campaña informativa sobre el uso del preservativo, se hubieran disparado los embarazos entre adolescentes, y también había agujeros de por medio.
La película es buena, muy bien interpretada, Vinco Bresan no puede estar mejor. Tierna y divertida.
        
            