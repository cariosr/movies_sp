
                      HOMENAJE A LOS CLÁSICOS MGM.

Bonita banda sonora la que acompaña a los protagonistas de esta historia de sueños, renuncias, fracasos, amor, éxito y nostalgia. Un rodaje que homenajea a los musicales clásicos de MGM (por momentos me vienen flashes de Fred Astaire y Eleanor Power o Gene Kelly & Debbie Reynodls, salvando las distancias, claro) en los que los decorados y atrezzo comparten protagonismo y enmarcan escena a escena.

Si disfrutas con los musicales y adoras los clásicos esta es tu película.

https://youtu.be/2PaXK01JYcQ
https://youtu.be/PqsrVQfNYPc
        
            