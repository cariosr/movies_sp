
                      Esta obra cinematográfica de ciencia ficción consigue superar los estereotipos de su género con un enfoque muy original y bien planteado. No es la típica película de extraterrestres, es algo mucho más profundo. Buena película sin duda.
        
            