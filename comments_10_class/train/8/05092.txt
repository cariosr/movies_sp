
                      Un espectáculo distinto. Emociones expresadas con el cuerpo, la música y el paisaje. Sensacional banda sonora y magnifica composición del espacio y las formas. Una evolución original del cine de Wim Wenders. "Bailad bailad. Sino estamos perdidos", la última frase con la que se cierra el espectáculo, es toda una declaración de intenciones frente a la vida. Si queréis ver algo especial y emocionante  esta es una de vuestras películas.
        
            