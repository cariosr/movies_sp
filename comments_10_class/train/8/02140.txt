
                      De alguna manera, la mayor parte de lo que se supone que debe ser extraño parece extrañamente familiar. Burton`s Gotham City -construida en un escenario de sonido masivo en Londres- es una sorprendente mezcla de art déco gótico, decadente y arquitectura institucional sombría que parece estar a solo un par de paradas de metro de Ridley Scott `` Blade Runner`` y Terry Gilliam`s

"Brasil". Es genial mirarlo, pero parece que hemos estado aquí antes.

Inspirándose en los recientes cómics revisionistas de Batman, escritos por Sam Hamm (que también se desempeñó como coguionista de la película), Burton se esfuerza por lograr una mezcla de realismo psicológico y exageración expresionista que no se lleva del todo.

El estilo visual abarrotado y espacialmente distorsionado de la película deja suficientemente claro que `` Batman`` ya está teniendo lugar en un universo mental subjetivo y bastante torturado. Junto a toda esa retorcida energía psíquica, el trauma relativamente realista que Burton emplea para explicar al Bruce Wayne de Michael Keaton -como en los cómics, vio a sus padres asesinados en un atraco cuando era un niño- parece casi mezquino. No explica Batman y en cambio lo encoge al nivel de un pobre niño rico.
        
            