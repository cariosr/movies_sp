
                      John Wick,  el diablo se viste de Keanu Reeves. 

Que por un coche y su mascota un elemento sea capaz de traer el mismísimo infierno a Nueva York, da la sensación que nos quieren decir que lo realmente importante en esta película no es ni el inicio, ni el final,  ni el argumento en sí.  Aquí lo que realmente importa es, lo que une todo esto.  
Esta manera de hacer escenas de acción es un punto y aparte de lo que estábamos acostumbrados hasta ahora,  al verlas da la sensación de estar viendo algo extraordinario en su tratamiento. Además de estar perfectamente coreografiadas y realizadas,  algunas a pesar de largas son dinámicas y  entretenidas da la sensación que estas escenas son historias en sí mismas, no se reducen a una colección de golpes interminables, sino que están bien estructuradas con inicio, nudo y desenlace en su justa medida.  
Ayuda muchisimo que el protagonista sea un pedazo de artista haciéndolas, todo el bagaje que tiene en este tipo de películas aquí lo desarrolla en toda su extensión. 
Keanu Reeves en su linea, mirada difícil de encontrar, serio como él solo y peleas con alto contenido en artes marciales impecablemente hechas. 
Michael Nyqvist su alter ego esta muy bien de  mafioso,  serio, cabal dan ganas de tomarse un copazo con el,  aunque el innecesario último baile con Reeves se lo podían haber ahorrado, junto con Willem Dafoe los dos personajes mejor interpretados de la película. 
John Leguizamo y  Alfie Allen también están muy bien. 
Película de acción pura y dura, ideal para pasar un rato muy muy divertido.
        
            