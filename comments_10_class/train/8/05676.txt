
                      Nos sumerge en la acción muy pronto y el argumento gira en encontrar una cura y sobrevivir en el intento. Grandes efectos especiales con ciudades sumidas en el caos y la confusión. Brad hace un buen papel y nos mantiene atentos durante todo el desarrollo. Una película con una base argumental no muy fuerte pero que funciona, con momentos de tensión y con unos efectos sobresalientes.
        
            