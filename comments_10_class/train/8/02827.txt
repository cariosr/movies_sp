
                      La película es muy buena y entretenida, sin embargo, no apta para los que se aburren fácilmente. Los primeros 40 minutos aproximadamente es lenta, sin acción. Luego, una gran desenvoltura. La trama no es muy especial, pero fue muy bien llevada por su director Alejandro G. Iñaritu. La recomiendo.
        
            