
                      Que “Escuadron Suicida” es una de las películas del verano es algo incuestionable. Forma parte del nuevo giro en las películas de DC donde pretenden ser más una franquicia unida a lo Marvel, que cintas independientes. Y eso es algo que juega a su favor, pero también en su contra. Y es que “Escuadron Suicida” puede que sea la película más Marvel y menos DC de las que han hecho hasta ahora.

Tanto es así, que de la oscuridad de “Batman V Superman” aquí no tenemos prácticamente nada, aunque la historia continua donde la terminó aquella. Ahora nos centramos en un grupo de villanos divertidos, algunos que otros bastante coloridos y muy cachondos. En realidad Margot Robbie se come prácticamente ella sola toda la película. Y es que parece que la chica se ha puesto un cartel en la frente que pone “ENAMÓRATE DE MI Y DE MI PERSONAJE”, pues no podemos hacer otra cosa más que amarla. No sería justo decir que es la protagonista absoluta, pero no estaríamos muy desencaminados. Eso sí, queda claro que Jared Leto no es ni de lejos, uno de los mejores Joker de la saga. De hecho, no le llega ni a la suela de los zapatos a Jack Nicholson o Heath Ledger.

Por lo demás, tenemos una cinta veraniega entretenida, bastante divertida, llena de acción trepidante y cargada de los habituales cameos del gremio. En ocasiones puede parecer “Power Rangers” y en otras, puede ser una parodia (que no spoff-movie) de “Los Vengadores“). No pasará a ser una obra maestra, pero si es una cinta más de superhéroes, cañera, digna y muy agradable de ver.

Lo mejor: sin duda, su banda sonora mezcla de temazos con un score potente de Steven Price.
Lo peor: que quiera parecerse a Marvel.
        
            