
                      La cinta comienza con unos cuarenta minutos memorables que te enganchan sin remedio con unos números musicales para enmarcar. El primer plano secuencia del baile en medio de un atasco de tráfico es de los que se convertirá seguramente en icónico. Los últimos veinte minutos son una lección de cine y montaje con una diversidad de técnicas que culminan en una escena final reservada a la pareja. Una moraleja que no se queda en un simplismo insulso y un poco, sólo un poco más allá.

La trama y los diálogos no presentan ninguna complicación y todo se configura al servicio de unas escenas musicales que fluyen en una continuidad llena de colores, luz y coreografías que configuran una estética espectacular y sugerente.

Un jovencísimo Damien Chazelle, que ya nos sorprendió con la más trascendente Whiplash, se supera con un cine más ligero que consigue recuperar aquella conexión de la época dorada de Hollywood a base de buen oficio y un dominio impecable del montaje. Una gran película que cautiva y convence aunque no te gusten los musicales o las historias de amor azucarado. (8,5/10)
        
            