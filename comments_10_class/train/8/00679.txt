
                      Me esperaba una buena película pero no a ese punto, me pareció muy sobria y realista, los temas que se tocan son delicados aunque se concentra sobre todo en el dolor de una madre al perder su hijo y su marido en un atentado a la bomba, Diane Kruger interpreta su papel a la perfección ( aunque para mi gusto no merece premio), a mi me gustan las películas que logran tener coherencia de principio a fin y el final me pareció muy apropiado, y por terminar la música va muy bien con la puesta en escena!
        
            