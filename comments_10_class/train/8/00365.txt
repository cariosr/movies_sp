
                      Esta es una de las sagas que aunque hayas oído o leído malas criticas respecto a algunas de sus entregas, vas a verlas, con la intención de pasar dos horas entretenido, atiborrándote de palomitas y refresco. 

Entretenido largometraje de aventuras,  con conseguidos escenarios y ambientación, logradas escenas de combate y un carismático Barbanegra interpretado por Ian McShane (Scoop, El aprendiz de brujo), todo ello amenizado con lo que para mí es lo mejor de la saga, que es la banda sonora compuesta por Hans Zimmer.

A pesar de los 138 minutos de duración, y con un ritmo discontinuo con altibajos, no se hace muy largo. El personaje de Jack Sparrows, interpretado por Johnny Depp, ya ha perdido la chispa y la gracia de las primeras entregas y Penélope Cruz pierde protagonismo a la media hora de salir en escena.

En cuanto a la dirección me quedo con anteriores trabajos del director como Chicago o Nine.

Recomiendo quedarse hasta acabar los créditos finales, pues guarda sorpresa.
        
            