
                      Hablar de Slumdog Millionaire es hablar de una película con un ritmo trepidante, una potente trama argumental y un gran poder de enganche.

Nosotros hemos tardado más de 6 años en darle una oportunidad y la verdad, nos arrepentimos un montón. Cinematográficamente tiene un montaje espectacular, que aunque tiene visos de abusar de los "flashback" hasta el punto de hacer que el espectador se pierda, eso no llega a ocurrir.

Os la recomendamos totalmente
        
            