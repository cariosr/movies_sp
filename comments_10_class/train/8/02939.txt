
                      Bien escrita e ingeniosamente dirigida, Don Jon tiene algo que contar. 

Tal y como me pasó con CLICK: Perdiendo el control, estoy ante una cinta que en sus avances me prometía algo muy superfluo y sin sentido, pero sorpresa: estaba equivocado, y con una tranquila sonrisa agradezco que así haya sido. 

Si bien la película inicia con Don Jon narrando su vida y lo que para él es importante, debo alabar que la cinta poco a poco anuncia el tono del tema que aborda, en este caso, las relaciones sin compromiso y su opuesto, la monogamia, pues lo hace de un modo pausado, relajado, realista y eso sí, muy honesto. 

Como yo, conoce a Don Jon (interpretado por Joseph Gordon-Levitt) en su filme debutante como escritor, director y protagonista, un sujeto que, en cierto sentido, lleva una vida monótona vista como una religión personal: familia, amigos, ejercicio, limpieza hogareña, sexo, y la pornografía. 

Y precisamente el carácter y herramienta del último elemento aquí mencionado donde el director-actor-escritor de la cinta recurre a la pornografía como una herramienta inusual para hablar de lo bueno que tiene la monogamia. Dejando de lado el acento con el que "narra" la película, clara referencia a los estereotipos clásicos, con su ópera prima Gordon-Levitt puede presumir una historia interesante que sorprende de principio a fin, pues gracias a su guión nos encontramos con personajes comunes pero que tienen un propósito (enseñarle algo al protagonista), lleno de ocurrencias y momentos empáticos, algunos otros realmente graciosos, otros astutos, todos ellos sin meterse de lleno con nuestros principios, pero tampoco ignorando la verdad detrás de la trama. 

Gracias a eso encontramos aquí su gran acierto: la película aborda circunstancias por las que todos pasamos, en las relaciones personales, de pareja, con nosotros mismos y con la familia, pero todo esto lo refleja con tal honestidad, y retratando nuestra sociedad actual como es, que al final uno no puede engañarse y sólo le queda el camino de la honestidad. 

Por un lado tenemos el rol de Scarlett Johansson, que para este filme su personaje funciona a la perfección, y por el otro tenemos a la "ocasional" Julianne Moore, también creíble y que enseña a Jon el otro lado de la vida al mismo tiempo que lo ayuda a entender sus acciones. Ambas le enseñan cosas, aunque a mi parecer, es la psicología del personaje de Moore la que está mejor construida por encima de la de Johansson. 

Es en los momentos cíclicos o comunes donde la cinta recurre a lo cómico para ser crítica personal y colectiva. Valdría la pena hacer una introspección con frecuencia sobre lo que esperamos de las relaciones humanas, porque es en el concepto equilibrio, o la falta de, donde hallamos muchos problemas y es justo eso lo que señala y enfatiza esta cinta, de una manera, a mi parecer, un poco sutil, pero eso sí, clara. 

Es una cinta interesante, inusual, bien dirigida y con un mensaje muy importante, especialmente para el mundo en que vivimos hoy.
        
            