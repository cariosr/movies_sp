
                      Cada vez que recuerdo la película me entra la risa, jajaja, no paré de reirme en toda la película, ya que la química de la pareja protagonista, las situaciones y los diálogos son bastante graciosos, aunque en principio no parezcan muy originales. Jennifer Aniston demuestra por qué es la nueva reina de la comedia americana y Gerard Butler, a pesar de su condición de héroe de acción, no lo hace nada mal. 
Los actores son muy buenos, como ya he dicho, y la película te hace estar entretenido y reírte mucho, lo cual no es decir poco para las comedias que se hacen hoy en día, ya que muchas no tienen ni gracia y encima suelen ser aburridas. Me ha gustado bastante, os la recomiendo
        
            