
                      La cuarta película de Quentin Tarantino esta dirigida, otra vez, de manera brillante, al igual que las interpretaciones esta muy trabajadas, con una que destaca entre las demás, esta es la de Uma Thurman. El film esta lleno de acción, con su respectiva dosis de sangre, y no falta el humor, como en cualquiera de las películas de Tarantino. La banda sonora es bastante buena, esta en la línea de las anteriores y también posteriores. El guión esta muy pulido.
        
            