
                      La verdad es que la peli sí que era linda, sobre todo en cuanto al trato personal y la fotografía espectacular, así como los cambios de luz y contrastes que hay continuamente. Sin duda lo mejor. En el lado negativo, a mi parecer, un ritmo demasiado lineal, que en algún momento puede amagar con cierto cansancio. También mencionar el claro guiño que hace el director a "Contra viento y marea" (Breaking the waves) de Lars Von Trier, con esas secuencias de los ofrecimientos al borde del mar. Recomiendo verla sin lugar a dudas.
        
            