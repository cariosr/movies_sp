
                      Película donde dejas tu vergüenza y tus miedos a un lado por una buena cantidad de dinero. Bastante entretenida.

Todo empieza con un par de chicas, una que le gustan las emociones fuertes Emily Meade y otra mas "aburrida" Emma Roberts 😍😍😍. La primera enfada a la segunda y le habla de una pagina donde ganas dinero haciendo las cosas mas impensables y le incita a participar, después de un mal entendido entre ambas la sosilla se apunta y en su primer reto conoce a un chico (Dave Franco) con el cual congenia. Se pasan toda la película haciendo retos juntos hasta llegar a la final, podremos ver como Franco y Roberts se van enamorando cada vez mas y su relación con sus otros amigos va decayendo. Con un final que no esta nada mal.

No esta nada mal la película con estos protagonistas y la historia no esta nada mal. Entretiene bastante.
        
            