
                      Una pelicula superentretenida, aborda un tema diferente a lo que vemos en las películas colombianas, el terror psicológico es muy bueno, las caracterizaciones de los personajes son muy creíbles y compenetran al espectador en la crueldad de la guerra.
        
            