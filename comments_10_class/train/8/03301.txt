
                      Siendo una película con prácticamente el mismo set todo el tiempo,  los diálogos son bien interesantes,  sobre todo cuando muestran a una persona en todos sus estados de ánimo... y es que en realidad así somos en nuestra vida!  a veces bipolares,  felices,  tristes,  histéricos,  inseguros, ansiosos,  hirientes hasta con nosotros mismos... eso hace de esta película una buena película.   quien piense que es aburridor,  bienvenido a la vida,  porque la vida también es en muchas ocasiones aburrida,  incluso incoherente.
Interesante de igual manera ver la lucha de una persona por seguir siendo alguien importante cuando está en decadencia de su carrera, quien no se ha sentido así,  sin ser necesariamente famoso?
        
            