
                      Alamar es el cuento de un joven en un viaje con su padre. Natan, de cinco años de edad, deja Italia para vivir con su padre Jorge en México. Su vida cambia completamente. La película es un documental dirigido por Pedro González-Rubio. El cuento está bien planeada. Natan y Jorge tienen una relación fuerte, hace la película más interesante.
El movimiento de la cámara es despacio. El movimiento cámara lenta muestra una vida tranquila. El océano es un aspecto bien elegido de la película porque es tranquilo y diferente. El viaje entre Natan y Jorge es hermoso y bien hecho.
        
            