
                      Un guión inteligente.

Estamos ante una obra maestra, una película épica, pero con algunos prejuicios. 
La sobriedad del diálogo hacen que la película 🎥 con sus 2 horas y veinte minutos, se pasen rápido ante tus ojos.
Pero el argumento, la misma repetitividad del asunto, el encuentro del padre que es Juez, en un pequeño condado y su hijo abogado, egocéntrico y que partió de su pueblo muy joven abandonando su hogar por diferencias con su padre, hacen desesperar un poco, y hasta puede que la butaca te moleste un poco.
A mi, particularmente, disfrute mucho con su guión tan fresco y lleno de vida, que a la interpretación de Robert Downey Jr. deslumbraba.
        
            