
                      De lo mejor que vi en los últimos años. Para empezar, un film con un humor muy bueno contado desde un niño de unos 12 años (creo). El hecho de que éste hombrecito tienda a sufrir muchos accidentes desde pequeño me llamó la atención y me quedé aferrado a la pantalla. Por otro lado, a destacar las muy buenas interpretaciones de la bellisima Sarah Gadon (que me hace acordar a la Patricia Arquette de los 90's) y por otro lado el brillante Aaron Paul que conmueve en la culimnación de la película.
        
            