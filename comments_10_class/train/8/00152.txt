
                      Como en "El Código Da Vinci" hay que estar igualmente atento a las elucubraciones de los protagonistas para no perder el hilo y en esta nueva entrega también hay conspiraciones, traición, crímenes y una nueva guerra entre la ciencia y la religión. 
Ell trasncurso del film es vertiginoso, cargado de tensión y momentos de acción. Se ha invertido más en efectos especiales que aunque breve, es espectacular. Recomendada si te gustó la primera.
        
            