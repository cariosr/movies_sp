
                      Kubo y las Dos Cuerdas Mágicas, no me pareció esa obra de arte de los que muchos hablan, pero si me pareció un gran cuadro del que insto contemplar tranquilamente y con la mente en blanco, para que el filme la use de lienzo y dibuje en ella una bonita leyenda en Stop-motion.
        
            