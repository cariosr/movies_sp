
                      No negando su posible inclinacion hacia un cine digamos "comercial", esta adaptación del clásico de Oscar Wilde resulta cuanto menos intrigante y, no en pocas ocasiones, nos traslada a un gran estado de angustia, lo cual siente su personaje principal, el Dorian Gray encarnado por el interesante actor inglés Ben Barnes. 
Llena de grandes interpretaciones más que convincentes, resaltando ante todo al ya citado Ben Barnes y al siempre excelente Colin Firth, también posee buen ritmo y matices más que interesantes en su argumento (ya conocido por los lectoores de la novela), hacen de ella una buena pelicula que te trasladará sin duda a una época desconocida, oscura, misteriosa... Notable.
        
            