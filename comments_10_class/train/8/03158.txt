
                      Cuando allá por 2007, Seth Grahame-Smith público una versión alternativa de la novela de Jane Austin “Orgullo y prejuicio“, todos sabíamos que esta película llegarían tarde o temprano. Más aún con el auge del mundo zombi que estamos viviendo en los últimos años. La moda por los muertos vivientes parece no acabar nunca y gracias a ello, tenemos esta genial adaptación de la novela de otra novela.

“Orgullo prejuicio y zombis” tiene todos los elementos que nos gustan a los fanáticos de los muertos vivientes. Tiene comedia, frases ingeniosas y momentos tronchantes. Tiene una banda sonora maravillosa con nombre español: Fernando Velázquez. Tiene además acción (aunque para algunos pecará de demasiado poca). Tiene zombis, zombis algo modificados y genialmente adaptados a esta trama. Tenemos cinco señoritas con corsé, refinadas y educadas del siglo XIX convertidas en cinco divas samurais de armas tomar. Con sus hachas, espadas y escopetas que hacen que nos enamoremos perdidamente de todas ellas. Y por supuesto, tenemos la trama que ideó Austen para su novela con el aliciente de estar rodeados de caminantes.

Aquí la comedia, la acción, los efectos especiales y la historia “Orgullo y prejuicio” se entremezcla de tal manera que nos hacen pasar hora y media llena de diversión, buenos momentos y deseos absolutos de tener una segunda entrega.

Lo mejor: la primera batalla de las hermanas Bennet contra los zombis.
Lo mejor: que sea una montaña rusa de movimientos y haya 30 minutos solo de diálogos.
        
            