
                      Señores de Sensacine ¿De verdad viajan a Marte los protagonistas de la película??? No, no viajan a Marte, la acción transcurre en la ISS, en la órbita terrestre.
  Bueno, en cuanto a la crítica diré que es un buen thriller; te mantiene todo el tiempo cosido a la butaca.
 Aunque tiene muchas analogias con "Alien, el 8° pasajero" le da cierta categoría la propuesta (¡Por fin!) de que un ser inteligente no tiene que ser necesariamente antropomorfo; ni siquiera tiene que ser vertebrado; que es lo que  se han empeñado obsesívamente en mostrarnos, en muchas otras películas en el cine.
  Incluso veo cierto trasfondo ecologista al recordarnos que, tal vez los humanos no seamos tan especiales ni tan poderosos como creemos.
  Sobre alguna crítica de otros usuarios diré que lo realmente patético es creer que solo pueden ser inteligentes los seres que tengan manos, piernas, cabeza y columna vertebral ¡EL ALIEN DE RIDLEY SCOTT PODIA SER UN HOMBRE CON UN DISFRAZ, CARAMBA!!
        
            