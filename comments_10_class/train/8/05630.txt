
                      www.contraste.info

Astrid Anna Emilia Ericsson nació en la granja Näs, cerca de Vimmerby (Småland), al sur de Suecia, el catorce de noviembre de 1907. Era la mayor de cuatro hermanos. Su padre era el sacristán de la iglesia y su madre dirgía la casa desde profundas creencias religiosas y, a menudo, un tanto rígidas. Astrid entra a trabajar en el diario local y se enamora de su director. Poco después, se queda embarazada y tiene que marchar de su pueblo para poder tener a su hijo.

La belleza de la fotografía, los acompasados movimientos de cámara de Erik Molberg Hansen, y, sobre todo, la maravillosa interpretación de Alba August como una Astrid Lindgren adolescente consiguen una cinta casi perfecta. La lucha por su hijo en una sociedad conservadora, el amor por su familia y la generosidad de los extraños dotan al guión de una carga humana que hará disfrutar a los amantes de las historias verdaderas y equilibradas que huyen de falso melodrama.

Conociendo a Astrid es una película entrañable y muy recomendable para la educación de los hijos mayores e ideal para los que les guste llorar con el buen cine.

Contraste.info
        
            