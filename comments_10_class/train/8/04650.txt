
                      La película goza de una historia única, de un personaje histórico. El guión es de notable bajo y la actuación de Benedict sobresaliente. Este actor tiene magia y sus actuaciones tienen un magnetismo, que permiten a sus personajes ser carismáticos. 

Sin embargo, le falta algo de dinamismo al metraje para que podamos pensar en una obra maestra en ceneral, a la altura del trabajo de Benedict.
        
            