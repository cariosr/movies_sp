
                      Quiero comenzar diciendo que la musicalización es un deleite, a pesar de que la mayor parte de la película esta ambientada con solo un instrumento (batería), logra hacer la conexión con las diferentes emociones que se muestran en la misma.
Por otra parte la temática es genial, ya que permite al espectador penetrar en la mente de un actor y como este puede llegarse a sentir en un momento de su carrera.
Aunque la escenografía es muy buena, en algunas ocasiones se vuelve un poco densa y lenta. Por lo demás me parece genial el matiz de locura y la actuación de Edward Norton es destacable.
        
            