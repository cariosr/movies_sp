
                      Pienso que "El Bola" es una buena historia porque habla de un problema que puede ser un problema de muchas personas. Aprecié la película porque habla con sinceridad de los malos tratos que sufre un chico. Pablo es valiente y Alfredo no puede aguantar que su amigo sea maltratar. La amistad que les une es fuerte y conmovedora. El espectador espera que los niños salgan de apurros al final de la historia y que Pablo no sufra màs. Es una historia sincera y pasamos un buen momento.
        
            