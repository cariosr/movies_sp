
                      Cold Mountain es un drama dirigido por el inglés Anthony Minghella, director de 'El paciente inglés' y ´El talento de Mr. Ripley'.

Ambientada en 1860, la película narra la historia de dos personas enamoradas, separadas por la Guerra Civil. El soldado Inman (Jude Law) debe acudir a la guerra, pero le promete a su amada (Nicole Kidman) volver sano y salvo junto a ella lo antes posible.
Tras casi ser asesinado en varias ocasiones, Inman logra escapar del ejército.
En su camino de vuelta a casa, se encuentra con una serie de obstáculos que le harán la vida imposible. El nunca se rinde, y se mantiene cuerdo únicamente con el recuerdo de Ada.

Los escenarios que se muestran a lo largo de la historia son realmente bellos, al igual que todo el vestuario.

El reparto es realmente asombroso, ya que cuenta con muchas caras conocidas por todos, como la de Philip Seymour Hoffman, Natalie Portman y Donald Sutherland como personajes secundarios.
Renée Zellweberg destaca sobre el resto de los protagonistas.
Su personaje es una pueblerina inculta pero fuerte e independiente, que ayuda a Ada a mantener sus tierras, lo cual logra interpretar muy bien.

La película recibió treinta nominaciones a diferentes premios, de los cuales solo recogió cinco; cuatro para Renée Zellweger, entre ellos un Globo de Oro y un Oscar, y uno para Gabriel Yared, por la mejor música original.
        
            