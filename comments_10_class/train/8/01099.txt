
                      Lejos de entrar en debates religiosos de cualquier índole, esta es una de las cintas que daría igual el mes o año para verla.

La trama realmente no es nada que no se sepa da igual si eres creyente o no de la vida de este personaje bíblico. No creo tampoco que la intención de Gibson haya sido esa precisamente. Quiénes hemos seguido la trayectoria de este director sabemos de antemano que busca siempre otra cosa distinta en las historias. Huye en cierto modo del romanticismo pragmático en algunas de sus obras y nos trae una versión más dura, personal y en mi opinión realista.
En esta cinta no sería la excepción. Muestra el lado más realista de una crueldad sólo realizada por los seres humanos. Así que en muchos aspectos no entenderé mucho esa oposición de algunos sectores, porque incluso para mí, desmitificar una figura haciéndola más humana me pareció una apuesta interesante.

En esto juega su puesta en escena. Realista, cruda, dura, sin ese romanticismo que mencionaba. Es una cinta de esas que ponen a pensar en las imágenes y su dureza. Es casi imposible no empatizar con el personaje más allá de a quién representa. Dolor, sangre, realidad… En esto la verdad que Jim Caviezel estuvo soberbio. Dio vida a su personaje de forma casi brillante y encima en un lenguaje impropio de estos actores haciendo que la el film cobre particular vida.
En definitiva, buena película.
        
            