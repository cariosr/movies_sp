
                      Ridley Scott ha enseñado muy bien a su hijo a base de lecciones de buen cine. Pues si el padre logró encandilarnos con “Alien: el octavo pasajero“, Luke intenta hacer lo propio también mediante ciencia-ficción. “Morgan” es un extraño “experimento humano” que vive encerrada en un búnker bajo el cuidado de una empresa que controla y supervisa todos sus movimientos. Pero “Morgan” encierra un misterio que puede resultar mortal.

Quizás con unos primeros 45 minutos un tanto densos y pesados, esta inquietante historia comienza a tomar forma hacia la mitad de la cinta, cuando los peligros y los misterios van saliendo a la luz y lo que en un principio es una investigación, se convierte en una lucha por la supervivencia. De ahí en adelante, la película se convierte en un festival de buenos contra malos donde ni los buenos son tan buenos ni los malos son tan malos.

Pero si algo hay que resaltar en esta atmósfera oscura y espantosa, son Anya Taylor Joy (“La bruja“) y Kate Mara (“Marte“). Ambas bordan increíblemente a unos personajes desgarradores, perturbados, misteriosos y llenos de odio. Joy como el extraño experimento y Mara como una consultora que lleva el caso con muchos aires de diva, se comen toda la cinta hasta un apoteósico final de esos que no dejan indiferente a nadie.

Lo mejor: una soberbia Kate Mara.
Lo peor: que algunos la vean como una mezcla entre “Ex-Machina” y “Splice“.
        
            