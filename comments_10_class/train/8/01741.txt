
                      La verdad es que entretiene. La película está bien y tampoco es tan dificil de entender... solo hay que seguir el hilo. Es la gracia de este tipo de películas. Para disfrutar "solo de acción" hay miles para ver...pero esta es diferente. Es muy parecida a "Desafio Total", sobretodo por el hecho de que el director al final te deje con la duda de si realmente "todo es un sueño o no",  Incluso en algunas ocasiones durante la película te dan pistas (frases que le dicen a Di Caprio como " Â¿todavía soñando?". También podríamos decir que es del tipo "Matrix Primera Parte" o "Nivel 
13"... Si te gustan ese tipo de pelis tienes que ir a verla.
        
            