
                      Mejor película de lo que me esperaba. Tiene un argumento bastante desarrollado para su estilo desenfadado, aunque echa a perder gran parte del trabajo conseguido con un cameo innecesario que lo único que consigue crear es una ligera sorpresa y posteriormente un gran enfado en el espectador por no explicar ni lo más mínimo qué hace ese personaje ahí.
        
            