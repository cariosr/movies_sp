
                      La impresión que me ha quedado después de verla se resume en una palabra: absoluto y definitivo PELICULÓN. Es entretenida, creíble, de buena factura, emocionante, con valores… buena fotografía, buena música, interpretaciones, ambientación,… todo digno de lo que es: una película de primera fila. Rodada en inglés y con un presupuesto de 36 millones de dólares.

La peli tiene dos grandes momentos, con un punto de inflexión en el estallido de la guerra civil española. En la primera parte, muy bien ambientada en la España del primer tercio del siglo XX, se presenta la figura de Josemaría Escrivá a través de los ojos de un supuesto antiguo amigo suyo y se plantea la relación entre ellos, sus familias, sus luchas, etc.; es una primera parte de corte más “costumbrista”. En un segundo momento se desencadena la acción, desarrollando una historia de amor, guerra y traición que se relata en paralelo a la aventura que tuvo que correr San Josemaría en los primeros meses de la guerra. Quizá se podría hablar de una tercera parte de desenlace.

¿Y de qué va?. Yo diría que el gran tema de la película es el perdón. Destaca mucho la figura del sacerdote y la importancia del perdón como factor que diferencia a unas personas de otras ante los “dragones” que van surgiendo en sus vidas.

Lógicamente, en la peli hay cosas que me parecen mejorables: el ritmo en la primera parte, algunas escenas de ambiente… como todas las pelis, algo se puede mejorar. No obstante, esto no empaña para nada la buena factura de la cinta y la capacidad de enganchar al espectador.

Muy recomendable. Imprescindible, diría yo. Y probablemente acabará siendo, como La Misión, una película de cine club.
        
            