
                      Llevaba semanas queriendo ir a ver esta gran película y ayer llegó el día. Como admirador reconocido de Charles Chaplin, me atraía mucho la idea de poder ver en el cine una película de cine mudo y en blanco y negro.

Empieza la película y lo primero que es sorprende es el gran silencio que se hace en la sala a excepción de unos pocos depredadores de palomitas. La historia, un actor de cine mudo cuya carrera y vida parecen llegar al fin con la aparición del sonido en las salas de cine, se complementa perfectamente con el tipo de película y con todo lo que le ha acompañado desde su estreno. 

Es una película muy buena, pero considero que se le está dando más publicidad de la que debiera por el hecho de ser muda y en blanco y negro. Y, por una vez, no puedo recomendar ir a un cine con sesiones en versión original...
        
            