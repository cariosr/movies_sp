
                      Los Hombres Libres de Jones (2016).
"Al arma no le importa quien la dispara" (el subtexto podría ser: ya sea una mujer, un niño, un hombre blanco o uno negro...).

El director de Los Juegos del Hambre cuenta con el oscarizado Matthew McConaughey (a quien acompaña el villano de la primera temporada del nuevo héroe de Marvel Luke Cage) para mostrarnos un capítulo real de la Historia de EE.UU., donde podemos ver una clara crítica al racismo, al capitalismo y a la guerra hecha por pobres en beneficio de los ricos (como siempre). En este caso ambientada en la guerra del algodón, más conocida por la Guerra de Secesión norteamericana.

Quizá con media hora más de metraje de lo necesario, la cinta nos muestra la dureza de esa época del país de la libertad sufrida tantísimo por los negros. Cinta notable a mi entender aunque no sobresaliente.

Raúl Cabral.
        
            