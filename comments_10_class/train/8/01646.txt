
                      Como dicta su subtítulo Boyhood no es más que los "momentos de una vida". La vida de un niño normal, como tú y como yo. Con él podrás rememorar tu propia, infancia, el primer amor o el excitante miedo a los cambios. En definitiva se trata de una hermosa historia que podría ser la tuya, la de todos.Y que es, sin embargo, una de las mejores del año.
        
            