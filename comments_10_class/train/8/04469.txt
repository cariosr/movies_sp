
                      Hola,
Película visualmente muy atrayente, con un director que sabe que esa parte es importante hoy en día y que hay que aprovechar todos los recursos tecnológicos disponibles, ademas de horas de dedicación que son inocultables en el trabajo final. En cuanto al guión, la cinta patina en explotar los problemas de memoria de Dory, aunque la única forma de no hacerlo era ingresar otros personajes secundario mas sólidos y que hicieran una trama paralela (situación que no genera Nemo y su padre). Esperemos que no saquen  "Buscando a Hank" (El Pulpo), ya vemos que rendir la "sopa" no genera ni desencadena bien una franquicia (si no pregúntele a la Era de Hielo).

Sin embargo, hay también que destacar apuntes graciosos a lo largo de la película y en especial la secuencia con la canción de LOUIS ARMSTRONG  genial.

Saludos.  JUAN
        
            