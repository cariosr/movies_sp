
                      En un año en el que nos hemos “acostumbrado” a películas como Misión Imposible – Nación Secreta o Fast and Furious 7, sobresalientes por sus espectaculares escenas de acción, por el uso de imponentes vehículos de tierra, mar o aire en situaciones extremas, y unos personajes con mucho calado entre el público, no era fácil para la franquicia más longeva del cine hacerse un hueco y lograr sorprendernos. Y sin esperarlo, Spectre lo consigue en cierto modo.
Lee la crítica completa en el siguiente enlace:
        
            