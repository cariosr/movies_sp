
                      Destaca una asombrosa interpretación de la actriz protagonista con su capacidad para reflejar un dolor incurable. La película me atrapó por su trama bien llevada y por su belleza visual 
y a medida de que se acercaba el final sólo deseaba que no me defraudara, algo que no hizo en absoluto. Muy buena película en mi humildísima opinión.
        
            