
                      ¡Gustave Kervern y Benoit Delépine me sorprenden con esta película nada mala! No me había encantado Louise-Michel, así que no me esperaba a engancharme demasiado. Pero la historia no está mal ¡y cuenta sobre todo con un inmenso Gérard Depardieu! Muchas de las escenas son irresistibles, aquella donde cuenta los coches, o aquella donde se pelea con el carnicero, o aquella muy glauca en la cama con su primo...
        
            