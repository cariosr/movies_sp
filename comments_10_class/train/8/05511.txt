
                      Es una película documental. Perfecta para niños. Con unas imágenes espectaculares. Nada más que por ver las tomas fotográficas en conjunto con la banda sonora merece la pena ir. 
Mis hijos han disfrutado de la película. No es violenta. 
La recomiendo.
        
            