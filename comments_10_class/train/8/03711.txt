
                      Me cree una cuenta solo por la gran necesidad que tenía de calificar esta peli, conciso.

He quedado muy sorprendido con esta película, de principio a fin. Si buscan una historia real y sin ficción (muy apegado a los hechos), no se las recomiendo; más bien, si buscan una historia creíble que exponga los aspectos más crudos de la guerra (pero sin precipitarse), aquí tienen esta maravilla de peli.
      PROS:  Me encantó todo, como dije, desde el principio. Es brutal como exponen la manera en la que viven los soldados el último mes de guerra 