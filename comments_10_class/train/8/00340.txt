
                      La verdad que me esperaba mucho menos de lo que me ha dado. Me ha entretenido y sorprendido. Personajes interesantes en un mundo distinto a lo de siempre.

Lo mejor; la ausencia de conexión con Los Vengadores, que de tanto estirar el chicle ya repele mucho.
        
            