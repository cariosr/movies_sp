
                      ¿Quién no recuerda a Tom Hanks en su papel de “Náufrago“? Ese personaje que vivió meses aislado en una isla desierta y que nos dió uno de los mejores papeles del cine y de toda la carrera de Hanks. Pues bien, Matt Damon se atreve con un papel similar pero cambiando el escenario. Las paradisíacas playas de una isla desierta por el peligroso y nada agradecido espacio exterior, en concreto, Marte.

Una tripulación abandona a uno de sus astronautas dándolo por muerto en el planeta Marte. Pero está vivo, y ahora tendrá que sobrevivir solo y en las condiciones que un planeta que no permite la forma de vida humana le exige.

Más que con “Naufrago”, del que solo comparte ciertos tramos de la historia, “Marte (The Martian)” tiene un aire a la maravillosa “Interstellar” e incluso en sus escenas finales a “Gravity“. Aunque hay algo que la diferencia de todas ellas: esta película posee un nivel de buenrollismo muy interesante, pues más que ser una historia de sensiblería, ñoña o sentimental como se podría esperar, es una historia de supervivencia vista desde el más puro optimismo. Con muchas ganas de divertir, la cinta deja a un lado el poner los sentimientos a flor de piel, para abrirse paso a través de las sonrisas, de la aventura y de la música disco. Pues incluso su banda sonora es de lo más pegadiza y brillante.

Ya desde el minuto uno, no se anda con tonterías ni con escenas de relleno con la excusa de “para que conozcas a los personajes”. No. Aquí la historia comienza pronto, no dándote tiempo a colocarte bien cuando el pobre Damon ya está haciendo malabares (nunca mejor dicho) para salir con vida de su situación. Aunque por su larga duración (casi dos horas y media) la película tiene que pasar el típico altibajo a media película donde todo es conversación, estudio de la situación y una pequeña perdida de interés. Por supuesto, no tarda en remontar, pues enseguida vuelven a poner la carne en el asador para darte un desenlace de la historia como se merece. Aunque para final de la historia, los últimos 10 minutos (con títulos de crédito incluidos) que son la mar de espléndidos.

Lo mejor: que no quiera dar imagen de película de klinex, sino más bien todo lo contrario.
Lo peor: que algunos secundarios quieran ser principales (y que algunos lo merezcan).
        
            