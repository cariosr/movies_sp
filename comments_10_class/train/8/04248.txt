
                      Esta película es imprescindible para los amantes del universo Twin Peaks y ayuda a entender aun más la serie. El film esta dirigido por David Lynch de una forma magnifica, como nos tiene acostumbrado en casi todas sus cintas. La banda sonora de Angelo Badalamenti es muy buena, como el guión de David Lynch y Robert Engels. Las interpretaciones son buenas, destacando las de Ray Wise y Sheryl Lee.
        
            