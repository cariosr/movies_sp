
                      Es muy agradable ir al cine y ver que aún hay gente que sabe contar historias y hacerte disfrutar de una película que además te hace pensar.
Mantiene la tensión en todo momento, se deja ver, la historia te da que pensar, y en definitiva, te entretienes.
        
            