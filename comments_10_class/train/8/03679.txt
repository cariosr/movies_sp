
                      Muy buena!. Del estilo "Tiburón" o "The Shallow" con la diferencia que toda la historia transcurre en las profundidades del océano dentro de una jaula. Súper frenética de principio a fin y con un desenlace excelente e inesperado. Logra su cometido con creces y va a dejar a mas de uno contracturado de los nervios. La recomiendo sin dudar.
        
            