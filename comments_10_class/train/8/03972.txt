
                      La nueva película de Disney, dirigida por los veteranos del estudio Ron Clements y John Musker (directores de maravillas en animación tradicional como "La Sirenita" o "Aladino") a partir de un guión de Jared Bush, Pamela Ribon, Taika Waititi y los propios directores, constituye un nuevo logro de Disney a nivel visual y de animación, donde alcanza indiscutiblemente la perfección, aunque en el terreno argumental se queda por detrás de lo conseguido con "Zootrópolis" e incluso con "Frozen", el hit al que pretendía emular como película musical protagonizada por una heroína.
(...)
Lo mejor: "Vaiana" es una película espectacular a nivel visual y de animación, capaz de llevar a su máxima expresión lo mejor de las posibilidades del 3D para realizar dibujos animados.
En contra: Disney recicla su narrativa clásica en una aséptica historia en un intento algo fallido de adecuarla a los nuevos tiempos.
        
            