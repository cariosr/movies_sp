
                      Majestuosos efectos, un soundtrack excelso, envolviendo te en las emociones de los personajes y la euforia del momento, aunque Moana parece una nueva apuesta con un tema diferente. lleva los mismos esquemas de trama que otras entregas anteriores de Disney. Una misión que cumplir junto con un personaje opuesto al protagonista. que al final se vuelven súper cercanos y cumplen el objetivo.  algo ya predecible, aunque no deja de ser buena y en algunos momentos creativa. Se esperaba un poco más de este film. recomendada en el top familiar.
        
            