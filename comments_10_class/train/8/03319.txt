
                      Aunque a priori, se puede pensar que la película, es para el lucimiento de Mario Casas, reúne muchas características que hace que merezca la pena echarle un vistazo.
Se basa en acción policial por las calles de Sevilla en la época anterior a la Expo 92, con escenas brillantes y muy bien llevadas.
Una película española de calidad, rápida y dinámica. Con el mundo de las drogas de por medio. Va generando y aumentando una historia, mezclada con la brutalidad y la tensión policial, a la par que la corrupción y los chanchullos.

Quizás es como demasiado brusca, no muy clara, confusa. A veces difícil debido al acento de los actores, lo que en cierto punto, la hace más realista.
La actuación no me termina de convencer, a pesar de las diversas nominaciones a los Goya, excepto el secundario Antonio de la Torre, y la del yonki Julián Villagran.

Mi reflexión es que merece la pena verla, ya que es mucho más convincente que las películas americanas, se ve como más cercano, más real, menos tópico, nos sentimos más identificados, con una realidad que nos suena.
        
            