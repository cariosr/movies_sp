
                      Como la mayoría de las películas musicales no hay que buscar espacios de crítica. Solo dejarse llevar por las imágenes (llenas de colorido) y la música (con alma de jazz) para disfrutarla a tope. Ryan Gosling no es Fred Astaire bailando, pero lo supera actuando, y Emma Stone es capaz de emocionar a un canto (lo lleva en el nombre).
        
            