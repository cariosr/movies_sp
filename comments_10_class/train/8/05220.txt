
                      La película es una historia de amor entre Rebecca y Dylan. Separados por medio país, cada uno tiene una vida completamente diferente: mientras que Rebecca se encuentra casada con un famoso y rico doctor; Dylan está soltero, pobre y desempleado.
Pero entre esa enorme diferencia comparten una cosa: su soledad, pues por diversas razones, que luego nos irán diciendo, ambos se sienten solos y abandonados.
La película aborda esa creencia ya muy popular que dice que en algún lugar del mundo se encuentra nuestra alma gemela, conectada de alguna manera inexplicable a nosotros. Esto se da a entender cuando nos damos cuenta de que Rebecca y Dylan están unidos desde su nacimiento por un extraño “poder” que los hace ver y escuchar a voluntad todo lo que el otro está viviendo en esos momentos. 
Este es el pretexto que usan aquí para que ambos se vayan enamorando, cada vez más y más, con el pasar de los días. 
Es una película muy bonita y que vale la pena ver, pues considero que logra lo que se propone: hacer que nos sintamos romanticotes por un instante. 
La única parte negativa que se podría decir sería su final, que la verdad me pareció un poquito forzado 