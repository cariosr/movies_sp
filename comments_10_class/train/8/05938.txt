
                      Se usa como alabanza y reconocimiento hacia algunos de nuestros directores de cine decir que es “el más internacional”. Pues bien, de Isabel Coixet, se puede decir que es “la menos española de nuestros directores”.
¿Y eso es bueno o malo? A mi entender es buenísimo.
Partiendo de que nunca se deja de ser de dónde eres, se te escapa por las entretelas, por cualquiera, que se sea capaz de sobreponerse a ello y enzarzarse con la creatividad desde ese punto, tomando el mundo y todo lo que en él existe como propio y cercano, no puede ser otra cosa que muy estimulante y atractivo.
Esta película de Coixet la podía haber dirigido cualquiera de esos excelentes directores británicos de film de época. Por ejemplo James Ivory. No se hubiera notado, porque como tantas películas de ese ¿genero? es perfecta.
Isabel Coixet no lleva camino de descubrir un nuevo lenguaje cinematográfico ni de abrir nuevos senderos creativos en el cine, pero el nivel de calidad de sus películas es excelente.
Esta historia de sueños, resignaciones, pérdidas, aprendizaje y maldad es una delicia. Su dirección de actores es prodigiosa. Dirigir a los actores de manera que parezca que se escribe con ellos no es algo al alcance de todos. Muchos directores con el fin de encubrir esa carencia dicen aquello de “yo les dejo libertad para que hagan suyo el personaje”. Que no es que esté mal, es que entonces la película no es del director, solamente. Y las películas de Coixet son suyas. De principio a final.
Y casi todas ellas tienen una intención moralizadora, una tesis, una idea, lo que sea o como se quiera llamar, constructiva, humana, de defensora de los valores que nos hacen un poco soportables.
Cine sin alharacas, técnicamente insuperable, que cuenta historias bien guionizadas, interpretadas, fotografiadas, nada más. Y nada menos.
Hacía tiempo que no veía una película, en la que al aparecer los créditos la gente no saliera de estampida. Allí nos quedamos, todos sentados, unos segundos. Como si paladeáramos algo. Por algo será. Lo justo para ver la dedicatoria a John Berger, entre todos los créditos, con humildad y sencillez, como a él le hubiera gustado.
A mí Isabel Coixet nunca me ha decepcionado. Sólo una vez me dejo extrañado, estupefacto.
Ah!, y una cosa más, leer te permite no sólo no sentirte solo si no vacunarte contra la vanidad. Leer a tanta gente excepcional te posibilita situarte en el mundo y ya si eso, tú ocupas tu sitio. Haciéndose muy complicado el engaño.
        
            