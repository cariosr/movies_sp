
                      Imágenes poderosas, hipnóticas y sugerentes para que nuesta conciencia se impregne de misticismo en la contemplación de todo lo vivo e inerte que nos rodea. Una obra de arte con mayúsculas !!
        
            