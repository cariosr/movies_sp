
                      Felices 140 es una película de personajes memorables, actual, y con un ritmo y unos giros que aceleran las pulsaciones del que está en la butaca. ¿Lo mejor? Que esta historia no termina con los créditos finales. El espectador sale de la sala cuestionándose muchas cosas sobre los personajes y lo más importante, sobre uno mismo. Y es que esta perturbadora historia lleva implícita una pregunta ¿Qué habrías hecho tú?
        
            