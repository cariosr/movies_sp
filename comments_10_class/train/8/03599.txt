
                      Por Francisco Arrabal

Cuatro años después del éxito de crítica y taquilla que tuvo la primera parte del reboot de Star Trek por el nuevo realizador de los sueños hollywoodienses, J.J. Abrams; llega esta segunda parte de la saga con el pan debajo del brazo de no pedir al público: ni el carné de trekkie, ni haber visto la anterior parte de la saga, para poder disfrutar de ésta en su totalidad.

Tras la positiva irrupción de J.J. Abrams en el universo de Star Trek, saga a la que consiguió resucitar y aportar un soplo de aire fresco en una primera cinta que sirvió para sentar las bases de lo que estaba  por venir, vuelve repitiendo la fórmula que tanto éxito le reportó en la primera parte; si bien, ésta vez traslada el protagonismo del chuleta capitán Kirk y del estirado Spok, al malvado superhombre Khan. Y es aquí dónde la nave hace aguas. 

Benedict Cumberbatch es uno de los actores más disputados del panorama actual, polifacético, ningún director actual se resiste a tenerlo en su reparto, ni al él ningún papel;  por lo que es normal que él fuera el elegido para dar vida a uno de los hombres más peligrosos con los que ha tenido que lidiar la tripulación del USS Enterpise. Aunque la interpretación no pase de decente, el verdadero problema tiene como origen un guión que no llega a estar a la altura de las exigencias, ni mucho menos. Todo un problema si lo conviertes en el foco principal del film. Toda una pena, ya que podría haber sido uno de los grandes villanos de la historia del cine, pero no ha sido así, y es lo que más me duele.

Por lo demás estamos ante una cinta muy entretenida, que sabe casar a la perfección el género de ciencia-ficción con la aventura y el humor -¡Ay! hombre de acero-. Espectacular en todos sus apartados técnicos, nos reportará 132 minutos de pura diversión. Si algo no se puede poner en duda, son las dotes de Abrams como director y mente maravillosa. Gustará tanto a los más fans, como a los que acaban de aterrizar en el universo.

Muy recomendable para pasar un muy buen rato de entretenimiento.
        
            