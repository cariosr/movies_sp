
                      La estética y la puesta en escena de este artista coreano es alucinante. Sus obras te ofrecen siempre algo nuevo que nunca antes has visto en la pantalla. Otra cosa es que pueda gustarte, pero el tío tiene la imaginacIon suficiente para no repetirse. En este caso se trata de un thriller con tintes eróticos tan fascinante  como alargado, que va mostrando sus cartas poco a poco. En su segundo capítulo se repite un poco y en el tercero y último se pasa de rosca truculenta. Con todo es diferente e imaginativa.
        
            