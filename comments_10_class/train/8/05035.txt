
                      Asesinato en el Orient Express (2017).
"Un grupo de absolutos desconocidos sin nada en común salvo la necesidad de ir de un lado para otro".
Kenneth Branagh vuelve a ponerse tras las cámaras... y también delante, dirigiendo y protagonizando una de las más conocidas aventuras del detective belga Hercules Poirot, quizá el mejor detective del mundo (según sus palabras...). “He llegado a una edad en la que sé lo que me gusta y lo que no. Lo que me gusta lo disfruto muchísimo. Lo que no me gusta, lo detesto”. 

La cinta, sin ser redonda, aprueba con nota gracias a su ambientación, fotografía (tanto de exteriores como de interiores), diseño de producción, vestuario (no me extrañaría que la nominasen a varios premios por ello) efectos visuales (la Estambul de 1934 se ve preciosa), y por supuesto, por la interpretación de tantos y tan buenos actores. Destacan- aparte de Branagh, su acento y su bigote- la casi sexagenaria M. Pfeiffer, Johnny Depp, los veteranos Judi Dench y Derek Jacobi (Yo, Claudio), y Willem Dafoe. Pero el reparto es totalmente coral y bien elegido (seamos benévolos con Penélope Cruz, pobrecita...).

Sus cerca de dos horas de metraje no se hacen largas en mi opinión, y si no recordáis la versión del '74 o la novela de Agatha Christie, os sorprenderá en algún que otro momento.

Raúl Cabral.
https://www.filmaffinity.com/es/film166313.html
        
            