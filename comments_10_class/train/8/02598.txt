
                      Una peli bonita, sea musical o no. Los actores están genial, y los números de baile/cante no son demasiados (por si alguien cree que le pueden molestar) y son geniales.

Pero el guión no deja de ser un poco sencillo y no está del todo bien resuelto en el final.

A mi la secuencia inicial no me gustó nada. Está totalmente desconectada del resto de la peli y ofrece lo peor de los musicales: gente que se pone a bailar de repente sin ton ni son. Por suerte el resto de la peli y numeritos musicales mejoran bastante (no son tan horteras, vaya)
        
            