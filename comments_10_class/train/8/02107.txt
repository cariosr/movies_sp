
                      Una gran película que retrata la crudeza del holocausto y la vida de los judíos de Cracovia, pero a la par, una cinta que nos muestra que no todo es blanco y negro, no se reduce las cosas a buenos y a malos y, finalmente, una cinta que nos enseña el valor de la vida humana y de las pequeñas acciones que aunque nos nos lleguemos a dar cuenta, hacen la diferencia. 👍👍👍
        
            