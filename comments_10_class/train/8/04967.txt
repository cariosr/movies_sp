
                      Ya en sus inicios Nolan transmitió, quizá de forma inconsciente, las pautas que lo distinguirían en su posterior filmografía, algo que el paso del tiempo nos ha ayudado a confirmar. 'Memento' no es más que otra muestra -la primera, en realidad- de las habilidades del director, de los juegos temporales, del uso dinámico del espacio y los lugares, de los detalles imperceptibles. Bien por la historia de su hermano Jonatan o por la sublime actuación de Guy Pearce, una película sobre la amnesia se convierte en amnésica y, paradójicamente, inolvidable.
        
            