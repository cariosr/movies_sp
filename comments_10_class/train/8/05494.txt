
                      Dudé mucho antes de ir a verla. Tenía miedo que Wenders nos haya servido algo complaciente, llorón (una gran genia nos ha dejado, sniff!) Bueno, resulta que no, en absoluto. Muchos fragmentos de coreografías de la señora montadas en desorden, en forma sutil, y algunos testimonios de sus bailarines. El ritmo aumenta progresivamente, para terminar en una especie de apoteosis, dónde se mezclan todos los espectáculos. Es un gran éxito. Hermosa película no acerca de la danza, sino DE danza.
        
            