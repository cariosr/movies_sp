
                      Es una buena película sin duda. Es entretenida y es el comienzo de una de las mejores historias jamás contadas. La recomiendo al 100%. Pese a las dificultades técnicas de los años en los que se hizo esta pieza, se han conseguido unos buenos efectos especiales.
        
            