
                      Película inglesa basada en experiencias propias del director, ambientada en 1982 que nos cuenta la historia de Shaun, un niño de 12 años que acaba de perder a su padre en la guerra de Malvinas. Recién mudado y con problemas para hacerse amigos Shaun termina formando parte de un grupo de Skinheads mas grandes que el.
“This is england” es una gran película que se encarga de mostrar distintas perspectivas de la situación que atravesaba Inglaterra en la década del 80. Por un lado, nos ofrece una mirada inglesa de la Guerra de Malvinas, nos da la oportunidad de ponernos del otro lado de la guerra y pensar lo absurda que resulto para ambas partes. Por otro nos muestra las divisiones de los skinheads y el nacimiento de un nuevo grupo nacionalista y racista. 
La idea de que un niño se encuentre en medio de todo esto atrae por si sola y la gran actuación de Thomas Turgoose hace que la hora y cuarenta que dura el film nos deje con ganas de mas.
        
            