
                      Jim Carrey vuelve con su talento habitual en esta peli un tanto delirante pero que no por ello es menos simpática. Las tribulaciones de estos pequeños pingüinos (bueno, son más bien "pájaros bobos") son realmente desternillantes y, a la vez, conmovedoras, aun si debemos descartar el aspecto "cliché" del filme.
        
            