
                      Buen largometraje, entretenido y bien llevado, que nos mantiene atentos a la pantalla, con una historia que rebosa humanidad, otra muestra de buen cine argentino, con su dosis de humor, en la que se reflexiona que papel juega la justicia, y como se puede manipular. Recomendable para seguidores del cine argentino.
        
            