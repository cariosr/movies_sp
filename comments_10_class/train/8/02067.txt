
                      Me ha gustado mucho. No es nada del otro mundo pero creo que recupera lo lógico en este género. No tiene efectos especiales ni alta tecnología debido en parte a que se ambienta en 1985 y eso realmente se agradece ya que las tramas hi-tech y con explosiones se tornan a mi parecer insoportables.
Por otro lado cabe destacar que carece de un héroe y menos del tipo cara bonita o crack sabelotodo...lo cual es mejor puesto que le da un toque mas real al personaje principal.
También me ha gustado que no se abuse de la violencia y sangre, sin perder un gramo de suspenso.
Entretiene,  la recomiendo.
        
            