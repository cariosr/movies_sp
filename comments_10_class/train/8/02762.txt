
                      Realmente, está muy bien hecho, Spielberg ha hecho muy buen trabajo. Los gráficos están muy logrados y se puede apreciar hasta el más mínimo detalle. Hay varias escenas geniales, la primera la del mercadillo, también, la batalla naval y la persecución en Marruecos. A lo mejor, a la historia le falta un poco de profundidad y, sobre todo, humor aunque ¡esta adaptación de Tintín no me ha decepcionado!
        
            