
                      Nada más comenzar la película se puede apreciar que se trata de un cuento. Una abuela le está explicando a su nieta, que está en cama, de dónde sale la nieve (ya que en el lugar donde viven nunca nieva). Y así comenzó la historia. Eduardo es un ser diferente que por una serie de casualidades, sale de esa mansión en la que lleva encerrado solo y se va a vivir a la civilización, al barrio que se ve desde las ventanas de su casa. Allí vivirá en familia y se relacionará con los seres humanos, y ahí empezará el lío. De una forma muy tierna, muy emotiva, a través del entrañable personaje de Eduardo Manostijeras.

Tim Burton nos muestra las bajezas del ser humano, cómo podemos pasar de encumbrar a alguien para un segundo después repudiarlo o aborrecerlo, cómo nos asusta lo diferente y por esa misma razón tratamos de eliminarlo de nuestra vida, cómo si las cosas no salen como queremos tomamos represalias, cómo abusamos de la buena voluntad de las personas… Y cómo a pesar de todo esto, siempre hay buenas personas que pretenden hacer lo que creen correcto.

Gran parte del mérito de la película, además de ser una bonita y tierna historia, es el personaje de Eduardo y la interpretación de Johnny Depp. Si no hubiera sabido que era él, no le hubiera reconocido actuando. Me ha llamado la atención que la primera película de este actor fuera una en la que el protagonista tenía cuchillas en sus manos y esta vez ha sido él el que llevaba algo similar.
En definitiva, una película mágica.
        
            