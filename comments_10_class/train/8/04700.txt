
                      Una buena peli de miedo, llena de sustos y algún que otro tópico de las pelis de miedo.

La peli empieza en el año 1947 en una casa con una sesión de espiritismo que resulta ser un fraude, conocemos a las protagonistas que son una madre y sus dos hijas y vemos como hacen sus vidas. La hija mayor se fuga de casa para ir a una fiesta y como no allí se encuentra una tabla de Ouija a la que juegan pero no pasa nada. Parece q en el 47 estaba de moda lo de la Ouija porque la madre va a comprar unas velas y se encuentra que venden tablas de Ouija y como no se lleva una para casa. La prueba para su nuevo numero de espiritismo y aunque no pase nada si que pasa pero en la niña pequeña la cual parece tener linea directa con los espíritus. La parte de miedo empieza ahora pues la niña se ve curiosa y empieza a contactar con los espíritus los cuales le dicen cosas solo a ella. Cuando el espíritu posee a la niña pequeña es una caña de escena y apartir de ahí solo ahí sustos y un demonio de armas tomar hasta el final de la peli, la cual termina bastante bien.

Si eres fan de las películas de miedo esta no te decepcionara porque tiene sus momentos de miedo y tensión, aunque si has visto muchas pelis de terror puede que no te asustes con facilidad (como me pasa a mi). Muy recomendable
        
            