
                      El cine sigue empeñado en convertirse en la mano de la madre naturaleza. No les basta con “2012“, “El día de mañana” o “En el ojo de la tormenta“. Vuelve una vez más a demostrar que solo Hollywood es capaz de destruir lo que muchos creen indestructible. Esto es “San Andrés“.

The Rock como maestro de ceremonias para una película que consigue lo que se propone. Mucha catástrofe, mucho caos, mucha lucha por la supervivencia y muchas casualidades. Pero este cine es lo que tiene: la chica buena siempre encuentra al chico bueno, la familia que debe permanecer unida luchará hasta que lo consiga y por muy grande que sea la ciudad sumida en el caos, siempre habrá un barco, tráiler o avioneta a tu disposición para salir adelante. Con un escaso (0) número de villanos por metro cuadrado, la película solo quiere hacer las mil maravillas en cuanto a destrozar rascacielos se refiere. No quiere nada más que colapse ésto. Ni siquiera una historia. Aquí venimos a disfrutar viendo cómo California se hunde en terremotos y es lo que veremos. No hay más.

Como siempre, unos secundarios sacados de un tío que hizo el casting haciendo zapping. Véase: Carla Gucino (“Wayward Pines“), Ioan Gruffudd (“Forever“, “Ringer“) y Colton Haynes (“Arrow“).

Lo mejor: obviamente, las escenas que llevan a la hecatombe.
Lo peor: que no luche por tener mas carisma.
        
            