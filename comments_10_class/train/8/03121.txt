
                      En la parte narrativa esta es una historia sencilla, la protagonista tiene que ir del punto A al punto B, el mérito de Alfonso Cuarón y su hijo Jonás, con quien elaboró el guion, es hacer interesante ese viaje y lo consiguen dotando a la película de una tensión constante que no deja al espectador un minuto de respiro. Cada escena se percibe de una manera tan realista que nos hace implicarnos en todas y cada una de las experiencias de la doctora Stone, sintiéndolas como propias. Por si fuera poco la hace trabajar contrarreloj, tiene 90 minutos antes de que vuelva a cruzarse con la basura espacial.
Y al servicio de todo esto un despliegue visual y sonoro cuidado al milímetro, el 3D es extraordinario, pero no es lo mejor de la película, los efectos visuales, esa sensación de ingravidez, los plano secuencia, que no haya un solo plano oscuro, los contrastes del silencio más absoluto con el sonido, lo increíblemente realistas que resultan las vistas de La Tierra desde el espacio, la aurora boreal, el reflejo del sol en el océano… es todo esto lo que convierte a Gravity en un espectáculo sensorial asombroso. Una película de aventuras en el espacio, literalmente, ya que la mayor parte transcurre en el exterior de las naves, algo poco visto hasta ahora y lo más cerca del espacio exterior que puede llegar a estar el común de los mortales.
        
            