
                      Eastwood es un genio. Sólo un director de su altura puede hacer una película con esta temática sin caer en el morbo, la cursilería o el ridículo.
        
            