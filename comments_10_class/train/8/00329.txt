
                      Fui a ver esta pelicula hace tiempo pero todavía tengo un tierno recuerdo de la misma.
Un padre tiene que afrontar en solitario la educación de su hijo menor a la vez que recibe a su cargo después de muchos años a su hijo mayor al que también tendra que educar.
La mezcla de imágenes muy bellas y una buena banda sonora, la interpretacion de Clive Owen y los ambientes creados durante la pelicula son las tres mejores cosas del film.
La recomiendo para pasar una tarde viendo una pelicula sin demasiadas pretensiones ya que es verdad que es un poco sentimentaloide e irritará a los alérgicos a la moralina, pero si no tienes demasiados prejuicios te aseguro que no está tan mal.
        
            