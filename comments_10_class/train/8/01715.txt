
                      Su pegadiza forma de reinventar las canciones. Su irreverente humor. Pero sobre todo, porque son unas auténticas divas. “Dando la nota” se convirtió en un fenómeno de masas allá por 2012 en todo el mundo. La taquilla respondió como nadie se lo esperaba, su banda sonora estuvo semanas en lo más alto de las listas. Su parecido a (por aquel entonces exitosa) “Glee” y a musical adolescente junto con unos personajes de lo más encantadores, hicieron que “Dando la nota” pidiera a gritos una segunda entrega.

Tres años después, las Bellas de Bardem se enfrentan al campeonato mundial de grupos Aca-pella. Un nuevo reto para sacudir al mundo con su música, mientras deben hacer frente a la realidad que las espera cuando acaben la universidad: la vida real más allá de los micrófonos.

Anna Kendrick vuelve a liderar (con una maravillosa Rebel Wilson pisándole los tacones) a todo el reparto de la primera entrega (algunos con mayor o menor protagonismo) en una secuela que eleva el número 2 a la máxima potencia. El doble de humor descarado, el doble de música (¡carne de iPod!) y sobre todo, el doble de buenos momentos. “Dando la nota aún mas alto” no es una continuación de historias al uso. Es la evolución de unos personajes que deben enfrentarse al mundo real, de un grupo de amigas que comparten voces, la pasión por la música y la lucha por hacer realidad sus sueños. Una segunda parte que no tiene que perder el tiempo en mostrarnos lo que han cambiado sus personajes, sino lo que las une y las hace únicas: su música.

Un par de personajes nuevos, cameos de lujo y una atronadora banda sonora lista para colarse entre los éxitos de iTunes. Así que olvidate de todo lo que has visto en comedias musicales, las chicas que nos enamoraron hace unos años están de vuelta. Con más momentos tronchantes, duelos musicales y actuaciones impecables. Y lo bueno que tiene este tipo secuelas, es que siempre dejan la puerta abierta para que vuelvan una tercera vez (o al menos su legión de fans lo deseamos). Y si lo hacen de nuevo con Elizabeth Banks tras las cámaras, mejor que mejor. Pues estas historias pueden alargarse en el tiempo lo que ellas quieran, viendo como las Bellas continúan triunfando y ¡¡dando la nota!!

Lo mejor: la escena de los créditos finales.
Lo peor: la “excusa” por la cuál uno de sus personajes principales queda relegado a unos 10 minutos de metraje.

NOTA: 8/10
        
            