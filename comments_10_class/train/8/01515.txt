
                      Cine español, sin duda una grata experiencia. "El Bar" una pelicula de suspenso y comedia, no decepciona en ningún instante, desde que inicia a fin te mantiene activo y con la mirada en la pantalla, los nervios no tan al límite, pero sí alertas. Desde el punto de vista actroral, no cabe duda que las sopresas son muchas, pero en lo que se queda corto, es en el trama final.
Sin duda, Alex de la iglesia, el Director, trata de plasmar un guión que estaría en un proceso de mejora, pues hay fracmentos que no concuerdan y no terminan de encajar con lo planteado. Por ello, la trama y el surrealismo de la pelicula, ayudan a cubrir ciertos fallos, desde mi punto de vista.
En conclusión, para una tarde entretenieda y en familia, "El bar" es una pelicula que de cinco estrellas y, siendo muy amable, le daría cuatro.
RECOMENDABLE 4/5
César Risso
        
            