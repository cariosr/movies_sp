
                      La trama central muestra como casi siempre en esta temática, el lado más humano dentro de una de las tragedias más negras en la historia de la humanidad. Aquí desde luego la forma como está presentada nos acerca en cierto modo, a las vivencias de cientos de personas que tuvieron que vivir en esa sociedad en guerra. No reporta novedad, pero si mantiene un componente diferente dentro de las múltiples cintas con este drama.

La ambientación jugó un papel importante para lograr el objetivo de impresionar a pesar de cómo he dicho, no representar ninguna novedad. Jessica Chastain no estuvo brillante, aunque su actuación es de lo más reseñable de la película. Supo imprimir seriedad, sencillez y drama en su personaje. Además estuvo acompañada por un elenco interesante que si bien algunos me parecieron insustanciales, no desentonaron dentro del film.

La pega que le encontré, es la duración de la misma. Hace que por momentos pierda intensidad sin llegar a ser mala ni mucho menos. El exceso de minutos de rodaje dilataron mucho la trama y eso es una película con este contenido, debe cuidarse para no correr el riesgo de resultar tediosa. 

En definitiva, una cinta interesante, con buen fondo no sólo histórico sino dramático que resulta agradable.
        
            