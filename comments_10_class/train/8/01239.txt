
                      Un entretenimiento colosal. Puede que sea unidimensional e incluso torpe en historia y caracterización, pero definitivamente triunfa en lo que cuenta: emoción, suspense y una estupenda recreación de los reptiles gigantescos. Si quieres grandes dinosaurios, tendrás grandes dinosaurios.
        
            