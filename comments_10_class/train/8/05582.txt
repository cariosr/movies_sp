
                      Realmente es una película deliciosa. Me encantó prácticamente de cabo a rabo, su fotografía que me parece soberbia, los personales, la historia. Sin duda te engancha desde un principio y te lleva por todo un recorrido de sensaciones, una montaña rusa de emociones con situaciones a la par de divertidas o kafkianas. Todo esto, en una batidora, da como resultado 'El gran hotel Budapest', una película para ver y disfrutar desde el primer instante.
Sin duda, de lo que más me ha divertido últimamente en el cine que mira que es dificil que yo no soy de risa facilona.
        
            