
                      A pesar de que el argumento de la película no esta bien detallado en la página (yo solo he visto una vez la peli, y no recuerdo si mencionan Cabo Verde pero diría que para en las Islas Canarias, donde por cierto Alex Pella hace un cameo, y ahí solo tiene el riesgo de perder la carrera, no de ser descalificado, dado que parar para reparar el barco no tiene penalización siempre que no sea en puerto y/o se recurra a ayuda ajena), creo que es una de las mejores películas náuticas que he visto, y que no se asuste el público, porque si no tienes idea de barcos también cuenta una historia muy bonita. 
Se hace amena, y pasa rápido, y las escenas son espectaculares. Muy buena interpretación y muy realista, nada que ver con superhombres de Hollywood ni ningún otro cliché. Y la verdad es que solo por el mérito de grabar en esas condiciones, merece más de un premio.
Totalmente recomendable!
        
            