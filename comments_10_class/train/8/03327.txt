
                      Muy buen pelicula para todos los publicos y espectacularmente rodada en 3D. Divertida, con partes en las que prima la acción y otras en las que se da más importancia al amor y los buenos sentimientos, el conjunto es muy equilibrado y disfrutable. Lo tiene todo para triunfar entre grandes y pequeños.
la animación está muy bien hecha y en especial los dragones estan muy logrados y las escenas de vuelos y peleas estan increiblemente realizadas, uno tiene pocas veces en el cine sensaciones tan reales de acompañar a los personajes en sus aventuras.
La recomiendo encarecidamente a todo tipo de públicos pues, aunque en principio parezca sólo dirgida a los niños, es sorprendente y por lo tanto también puede ir destinada a un público más exigente.
        
            