
                      Película candidata a varios Oscar 2016, entre ellas, a la de Mejor Película. Y, así, cabe destacar que es bien merecida pues cuenta una historia con unos perfiles psicológicos muy bien estudiados. Magnífica labor.
        
            