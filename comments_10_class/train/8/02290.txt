
                      Es una excelente trama que logra el realismo impecablemente sin perder el hecho de tener una problemática interesante; los guiones, los personajes y sus actuaciones son excelentes por donde las mires; Si no te gusta esta película es porque estas acostumbrado a historias que nunca pasarían y a finales predecibles y cómodos y/o porque te falta sentido de profundidad o reflexión.
        
            