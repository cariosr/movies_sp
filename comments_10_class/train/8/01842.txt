
                      Paul Greengrass nos trae este documental-drama de acción basado en un hecho real como ya hiciera con United 93, pero en esta ocasión cambiamos de medio de transporte, ahora la acción transcurre en alta mar, en un barco carguero cuyo capitán da nombre al film: Capitán Phillips.

Como estrella principal tenemos al impresionante Tom Hanks que está espléndido en su interpretación y demuestra que sigue en lo más alto y es capaz de hacerse con cualquier encargo cinematográfico. Lejos está el joven Tom Hnaks de aquellos años en los que solo encarnaba personajes básicamente cómicos como en Esta casa es una ruina, Big, Socios y sabuesos...

Dar la enhorabuena al otro "capitán" somalí, Barkhad Abdi quien también está espléndido en su papel.

Paul Greengrass ha querido darle un toque muy peculiar a esta película y lo más destacable son los efectos de cámara. Parece que se olvidó el estabilizador de imagen y la cara da constantes saltos, temblores según se va desplazando, casi pareciendo la cámara de un videoaficionado. Utiliza también encuadres que llegan a resultar incómodos o molestos, con objetos delante del personaje que no te lo dejan ver bien, actores fuera de encuadre, perdiendo el enfoque en algunos momentos... y primeros planos, primerísimos primeros planos de la cara de los actores.

No me equivocaría si dijera que el 75% de la película sale el encuadre a toda pantalla de la cara de algún actor. Escasos planos abiertos para tratarse de una historia que transcurre en alta mar. Muchos planos cortos, que te permiten ver el sudor, las arrugas, las ojeras, los dientes torcidos de los personajes. Todo ello para conseguir un efecto muy realista de la película pero que carga totalmente el peso del film sobre sus protagonistas, sobre las interpretaciones y la expresividad de los actores ya que también los diálogos son cortos y escasos.

Todo esto que en principio parecen defectos consiguen a la larga hacer algo extraordinario y meterte de lleno en la piel de los personajes, en su agobio, en su sufrimiento... en su incomodidad.

Como sabéis me encanta fijarme en las bandas sonoras y en este caso debo decir que está compuesta por Henry Jackman y que no me di cuenta de que la peli tuviera BSO hasta el final y fue porque la canción me recordó vagamente a la que compuso Hans Zimmer para Origen (time).

Capitán Phillips es una gran película, absorbente y bien hecha que merece ser disfrutada con tranquilidad.
        
            