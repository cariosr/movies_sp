
                      Se adentra en la realidad virtual de un futuro cercano y distópico donde la gente utiliza este medio para evadirse de la miseria imperante. Argumento sumamente novedoso con animación 3D durante la mayor parte del film, abundantes situaciones y referencias retro de los '70 y '80 (películas, música, personajes, video juegos...) y excelentes efectos especiales como lo mas destacado. Muy recomendable.
        
            