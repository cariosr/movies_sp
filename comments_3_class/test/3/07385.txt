
                      Extravagancia es lo único que define a esta obra de la cinematografía. Con un guion enriquecido y una puesta de escena bien lograda. La producción alcanzó la perfección en las escenas de acción, todo un espectáculo donde en ningún momento se siente coreografiadas como muchas otras.
Todos los personajes destacan con interpretaciones sublimes. Y el director demuestra que nunca es tarde para revivir una saga a los grande. Sin duda una experiencia única
        
            