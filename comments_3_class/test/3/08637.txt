
                      De toda la trilogía, considero que la segunda, "El caballero oscuro" es la mejor -y por mucho por cierto-. Hoy en día hay pocas películas así.

En esta ocasión tenemos un argumento diferente. Un Batman algo más oscuro y menos "comercial". No es la clásica en la que un villano decide destruir una ciudad o el mundo entero simplemente "porque sí" o "porque es malo". Tenemos a un villano muy bien argumentado y formidable. Y es que más que Batman o dos-caras es el Joker lo que le da un buen puntazo a esta película; sin él esta cosa no sería lo mismo.

La única razón por la que no le doy las cinco estrellas, es porque la primera hora puede ser algo pesada para algunas personas. Pero créanme que eso se compensa muy bien la otra hora y media restante.
En fin, nada más que decir sin enrollarnos. Excelentes giros de guión y muy buena trama. Un film que no sólo vale la pena ver, si no que es obligatorio para cualquier fan de Batman y de las películas de buena calidad.
        
            