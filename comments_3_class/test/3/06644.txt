
                      Tercer año que presenciamos una purga anual. Tercer año y seguimos sin quierer que sea el último, ni mucho menos. Tercer año que una saga cinematográfica te deja clavado a la butaca y con ganas de más. Tercer año que damos las gracias a los nuevos padres fundadores de los Estados Unidos de América por esta tradición tan maravillosa.

Ya lo dijeron sus productores cuando se estrenó “Anarquía“, la segunda entrega: que tendríamos purga para rato. Y los fans saltamos de alegría, como es obvio. Y es que “La purga” puede que sea de las pocas sagas, si no la única, que se supera secuela tras secuela. Es de esas películas que queremos que nos lleguen más y más entregas, pues la calidad, al contrario de disminuir, aumenta.

Aunque no quede ni rastro de esa “home invasion” que fue la primera entrega, “Election” se centra en el año de elecciones de la purga. Detractores contra defensores de una tradición tan mortal como acojonante. Desde luego la trama de esta película da solo para una entrega pero la aprovecha el máximo. Seguimos teniendo tensión a raudales, seguimos teniendo misterio, personajes que luchan por la supervivencia contra los que disfrutan de su derecho de asesinar (que grande la pequeña colegiala de color) y seguimos teniendo sustos, agresiones y sangre. Vamos, un festín para los fans.

Puede pecar de ser más de lo mismo, pero la situación de esta trama es así: cada vez hay más peligro, cada vez hay más locura y cada vez la purga es más necesaria. Como cada secuela de esta saga.

Lo mejor: que siga indagando en una tradición tan fascinante como es la purga.
Lo mejor: que se introduzca en un tema tan peliagudo (e innecesario) como es la política.
        
            