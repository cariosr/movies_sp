
                      Potente artística y emocionalmente.Obra Maestra.

The Master es una pelicula que llevaba mucho tiempo esperando y no me ha decepcionado en absoluto.Una película que desprende potencia artísticamente,desprende emociones y reflexiones en un contexto indefinido,problamente díficil de captar.Paul Thomas Anderson realiza una película muy diferente que trata la relación del lider de una "secta" y Freddie,un psicópata adicto al sexo que busca un camino.Más que contar la historia sobre "La Cienciología" se basa en la progresión y en la vida de un loco ,interpretado por un Joaquin Phoenix que esta espléndido, y Lancaster Dodd,el lider de la fundación en un viaje violento,intenso y sobre todo extraño dotado de carácter psicológico, y por supuesto de mucho dogma. Un filme cargado de sorpresas y de escenas muy potentes.Muy Recomendable.

¿Lo mejor? Escenas que te dejan de piedra
¿Lo peor? Quizás sea de difícil entendimiento,no hay ni buenos ni malos,es una película diferente y eso  hará que mucha gente salgan fríos de la sala.
        
            