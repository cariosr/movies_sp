
                      Para mi es una obra maestra. La he visto 6 ó 7 veces. 
En primer lugar os recomiendo la canción fundamental de la película, que suena asociada a jugar al baloncesto. Es el mix entre Over the rainbow y What a wonderful world cantada por el malogrado IZ, una de mis canciones favoritas desde entonces. 
En segundo lugar, que parezca fácil, no es sinónimo de sencilla. Sean Connery lo hace muy bien, y llena con su personaje toda la película, como en muchas otras de maduro. Un genio.
Pero lo más importante es el tema de la película, que no va de literatura y ya está, sino de por qué la literatura y su relación con las personas. Soberbia.
        
            