
                      Si, por algún motivo, queréis ver esta película, NO OS INFORMÉIS DE NADA, es mejor verla totalmente perdido. Es lo que yo hice, y terminé alucinado. 
    Es una película, documental, o como se quiera llamarlo, que te comienza vendiendo algo que después no resulta ser lo que esperabas. ¿Quieres buenas escenas? Las tienes ¿Buenos escenarios? Ahí están ¿Las características de todas las películas de terror? Las tienes ¿Sangre? Hay de sobra. Y es que, la verdad, yo creo que debería ser una película de culto, porque es que.. Es que es una pasada. El simple hecho de "explicarte", de alguna forma, el "por qué" de las historias de terror, y el "cómo" suceden, es ya un grandísimo mérito. Pero si encima añades una historia de fondo, con sus cuestiones, personajes secundarios y todo tan planeado, es que te llevas lo mejor. ¿La guinda del pastel? 
    Ese final... Me dejó sobrecogido. Pero, la aparición de esa estrella... Sigourney Weaver se merecía más. Es buenísima, y sólo ha aparecido cinco minutos de la peli. 
    Lo bueno: Todo
    Lo malo: Sigourney Weaver, no sabía que era efímera.
        
            