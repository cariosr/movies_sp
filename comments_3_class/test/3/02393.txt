
                      Tarantino siempre es garantía de diversión y de sorpresas. Esta película no fue la excepción y la recomiendo ampliamente.
  La escenografía, la historia y la mayoría delas actuaciones son sorprendentes y el desenlace de la película también (cómo debe de ser todo buen film).
  Lástima por un par de recursos de los que tiró para unir la trama del film "forzando"un poco con narrativa  el enlace de la historia. No se siente fluido y, aunque breve, corta el ritmo del espectador -la parte del envenenamiento-. Eso sí, aclaremos, es pecata minuta para lo completo y lo que al final logra hacer con este filme. 
Un aplauso.
        
            