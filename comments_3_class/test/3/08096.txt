
                      Ser el niño medio es una perra. Lo mismo ocurre con los libros y las películas. Según esa norma, The Hunger Games: Catching Fire, la segunda película extraída de la trilogía más vendida de Suzanne Collins, debería ser un marcador de posición, atrapado entre el niño mayor que recibe toda la gloria y el bebé que recibe todo el amor.
        
            