
                      Peliculon, en algunos momentos argumentalmente caotica y donde si pestañeas o no prestas atencion, hace falta un segundo visionado. Suspense, drama emocional donde nada es lo que parece y cualquier sentido con la realidad es contradictorio u ambiguo. Es uno de esos pocos Films donde el climax final, no dura 5 minutos... si no 50. Nolan y Dicaprio (ultimamente sublime en sus interpretaciones), simplemente magicos.
        
            