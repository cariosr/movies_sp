
                      Si la película El Padrino fue un autentico peliculón, esta segunda parte es igual o incluso mejor, el director Francis Ford Coppola se supera y nos trae un film soberbio, con unos personajes muy bien definidos, tanto Al Pacino como Robert de Niro, que se doctoran en esta obra maestra, como Robert Duvall, John Cazale, Diane Keaton, etc. La banda sonora vuelve a ser impresionante y si el guión de la primera era bueno en esta es de diez, con un gran trabajo de Mario Puzo y Francis Ford Coppola.
        
            