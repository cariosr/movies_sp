
                      Gran secuela de Buscando a Nemo, divertida y tierna. Pixar ha vuelto a realizar otra película de animación sobresaliente,  evidentemente la primera parte siempre es mas original...pero aun asi muy recomendable para todas las edades.
        
            