
                      Para mi es la mejor pelicula de fantasmas de la historia del cine. Aqui nada de fantasmas con sabana blanca y cadenas, pero gente muerta con sus heridas. Lo que hace que esta pelicula sea muy real, que creas o no en fantasmas, y es terrorifico. El guion es excelente, sobre todo el final que cambia toda la pelicula, y los actores son muy buenos y autenticos. Una pelicula grande.
        
            