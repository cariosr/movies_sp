
                      La crítica completa la tengo en mi blog en la dirección que figura al final de la presente. 
Aquí simplemente voy a señalar los aspectos más propiamente cinematográficos. Si sentís curiosidad por mi opi nión al completo no tenéis más que pasaros por mi blog.

Aspectos cinematográficos
Fotografía. Impresionante. Sin lugar a dudas lo mejor. La idea que cierra la película sobre el sol y la luz es central en ella.  Más que otra cosa el director, Mike Leigh, y su director de fotografía, Dick Pope, crean una serie de pinturas filmadas que hablan por sí mismas del trabajo creativo de Turner.

Los actores. Fundamentalmente la interpretación del actor que da vida a Joseph Mallord William Turner, Timothy Spall. Su Turner es bastante creíble aunque a mí en algunos momentos de la cinta me pareció algo forzado la composición del gesto de egoísta y enfadado con que da vida al pintor. Pero por lo demás Spall es el alma de esta película, calificada por algunos de biopic o documental. 
También el personaje del padre de Turner, representado por Paul Jesson, es digno de mención por ser quizás uno de los momentos biográficos más claros en el film: la expresión del inmenso amor entre padre e hijo, la ayuda del primero y el estado depresivo en que cayó el pintor tras el fallecimiento del padre. 
Marion Balley (la casera con la que vivirá maritalmente en Chelsea) es la alegría de la huerta. Gracias a ella se logra soportar la morosidad y excesiva extensión de la cinta. Por su parte el personaje de la sufrida y maltratada sirvienta que compone Dorothy Atkinson es representativo del machismo del artista y de la época.
En cuanto a la familia del crítico Ruskin, los aristócratas que pululan por el film, así como la variada representación de pintores del momento, exitosos o no, son personalidades muy convincentes. Desde luego esta cinta es sobre todo una película de época muy bien actuada, una de las características que la salvan.

La música. las composiciones que incorpora Gary Yershon sirven para reforzar las imágenes. Es una música descriptiva muy apropiada a la pintura romántica cuya esencia se muestra en el film. La música que acompaña a la escena de la preparación del cuadro de la tormenta de nieve en el mar con un Turner atado al mástil cual si de Ulises ante el canto de las sirenas se tratase viene es magnífica.

Duración. Excesiva. Las dos horas y media de duración, paradójicamente, sólo consiguen transmitir al espectador la sensación de que no se le ha contado todo. En mi opinión es el mayor fallo de la película. Desde luego le sobra al menos media hora.
        
            