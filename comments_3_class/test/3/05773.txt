
                      Ron Howard se vuelve a poner detrás de las cámaras para traernos Rush, un biopic sobre el duelo  del alemán Niki Lauda (Daniel Brühl) y el inglés James Hund (Chris Hemsworth), dos corredores que empezaron siendo rivales en Formula 3 y acabaron disputándose el mundial de Formula 1.

Ron Howard vuelve a estar a hacer un excelente trabajo en la dirección y nos trae un drama personal envuelto en carreras de coches y con el enfrentamiento a diferentes niveles entre dos rivales muy distintos.

Como es costumbre en Ron H. nunca va directo al grano y se toma su tiempo en contar el inicio de la historia, pero consigue hacerlo sin que decaiga el interés, dándonos a conocer ambos protagonistas con sus diferentes modos de vida y sus diferentes personalidades, todo ello con una buena ambientación que nos mete de lleno en los años 70. 

Rush es una historia con el trasfondo de las carreras de coches de Fórmula 1 pero no esperéis una película "de carreras de coches". Para que os quede claro, no esperéis algo al estilo de A todo gas ( The fast and the furious), porque es una película más profunda y mucho más trabajada.
Los personajes están muy bien trabajados y con unos parecidos extraordinarios con los de la realidad. El trabajo de Chris H. es bueno pero si hay que destacar una interpretación esa es la de Daniel Brühl que hace un excelente trabajo dando vida al campeón de F1.

Como actores de reparto podemos destacar a Olivia Wilde,  Alexandra Maria Lara, Natalie Dormer y Pierfrancesco Favino.

Las carreras (he dicho que no es una peli de carreras de coches, pero evidentemente hay carreras de coches, jejeje) están rodadas y montadas con extrema precisión, con encuadres muy estudiados, muy cuidados y muy logrados, dando sensación de autentico vértigo, con visión de cámaras dentro y fuera de los cascos, de los coches, de las pistas,... ningún plano se les escapa.

La nota musical la pone el prolífico Hans Zimmer, quien con su habitual maestría nos sabe enternecer cuando quiere o ponernos el corazón a mil con música contundente.

Por todos estos motivos Rush suena en todas la quinielas para optar a la deseada estatuilla de los Oscar como Mejor película, aunque aun es muy pronto para saberlo con certeza. ¡Hagan sus apuestas!

Así pues os aconsejo que la visionéis en el cine, porque los efectos de sonido también son espectaculares (no me extrañaría que también optase a esta categoría de los Oscars).
        
            