
                      Alerta de Spoiler: ¡Los Vengadores están listos para matar ... el uno al otro! Demonios, está todo en el título: Capitán América: Guerra Civil. Steve Rogers, alias el Capitán América, nuevamente interpretado por el más excelente Chris Evans, encabeza una facción. Tony Stark, alias Iron Man, en la persona del estimable Robert Downey, Jr, lidera al otro. "Me gustaría golpearte con esos dientes perfectos", dice Iron Man to the Cap. Ay.

El problema para estos modelos de la marca Marvel es el daño colateral. Mira el desorden que dejaron estas armas de destrucción masiva al final de Avengers: Age of Ultron. Edificios derribados. Propiedad destruida Víctimas civiles. El Secretario de Estado (William Hurt, irradiando bravuconadas como Trump) declara que esto no se mantendrá. Las Naciones Unidas exige responsabilidad. Sorprendentemente, Iron Man está de acuerdo. El Cap, siempre el individualista resistente, no lo hace. Que empiecen los juegos.
        
            