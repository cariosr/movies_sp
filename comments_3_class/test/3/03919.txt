
                      Preciosa historia que consigue transmitir al espectador todo el cariño y respeto que  el equipo ha puesto durante su realización. La película va avanzando de un modo muy natural, sin forzar nada, poniendo en valor las tradiciones y costumbres del mundo Valenciano para contar sentimientos, emociones e inquietudes universales. 
Muy recomendable ir a verla para apreciar buen cine, y el inicio del que será uno de los grandes directores del cine español.
        
            