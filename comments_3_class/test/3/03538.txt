
                      Alamar es una buena película. Un niño llama Natan, deja Roma, Italia para quedarse con su padre. Su padre vive en una isla llamada Banco Chinchorro. El director, Pedro González-Rubio, ha hecho muchas películas en el pasado como Canícula, Antígona, y Inori. La película tiene una cinematografía asombrosa. La cinematografía fue realizada por Pedro González-Rubio, Alexis Zabe, y David Torres. Una cosa que la película podría haber hecho mejor fue mostrar más experiencias de Natan.
        
            