
                      ¡Me gustó este thriller bien fuerte bien violento y bien oprimente! Sale de esta película una atmósfera lluviosa y sombría (muchas escenas nocturnas). Jason Statham se iguala a sí mismo (es decir, muy bueno). Me gusta mucho este actor, aun si todos sus papeles se parecen... Linda prestación de Pady Cosidine (que no conocía para nada) en el segundo rol. El escenario es aceptable y a mi parecer el final está  bien logrado. Hay que verla. 8 de 10.
        
            