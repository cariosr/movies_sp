
                      No hay necesidad de un gran espacio.
A pesar que la mayor parte de la película se desarrolla en una habitación, la interacción y actuación de los actores es tan autentica que expresa muy bien la forma en la que uno se podría llegar a sentir en esa situación.
Las sensaciones que despierta son muchas, angustia, ansiedad, desespero, entre otras.
La única critica negativa es el desenlace de la misma, ya que en algún momento el espectador puede llegar a esperar otro tipo de final sin que esto suceda.
        
            