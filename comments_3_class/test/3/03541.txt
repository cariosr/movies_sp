
                      En general es una buena película, con detalles del libro que se agradecen, como por ejemplo, las canciones.  A todo esto añadimos música y fotografía excepcionales y tenemos el film, al final, el problema principal de la sesión es la duración de la misma, exagerada desde cualquier punto de vista, lo que termina por ser exasperante.
        
            