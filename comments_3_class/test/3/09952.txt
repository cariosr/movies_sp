
                      Cuando el cine elimina los efectos especiales, que desvían nuestra atención, uno solo puede creer en el guión de una película, y cuando esto sucede, solamente Pulp Fiction puede hacerte pensar: QUE GRANDE ES EL CINE!!!
        
            