
                      Creo que el gran problema de Bird es que voló muy alto para los estándares a los que las películas comerciales nos tienen acostumbrados. La historia es original, sin entrar en detalles de que el mensaje no lo sea, y está muy bien contada y llevada.

Y aunque entiendo por que mucha gente se confundió o decepciono por no ver la trama suscitándose en Tomorrowland, no creo que exista un minuto en toda la película que no tenga un momento de acción, diversión o de estimulación mental; además de que claramente la intención de Bird no es acerca de ir al futuro o a Tomorrowland, sino mas bien llevarnos a través de una magnifica aventura y una búsqueda más profunda hacia un mejor futuro (algunas veces los viajes, y no el destino, son los que nos dejan los recuerdos más divertidos y entrañables).

Por otra parte, las actuaciones de las chicas son espectaculares, en especial Britt Robertson, quien logra imprimir en su personaje un carisma y vitalidad completamente apropiados para la historia. Lo anterior es muy claro cuando uno ha visto a esta chica en otras películas y entiende que ese leve histrionismo inyectado a su personaje es incluso para nuestro deleite.

Al final, se podría decir que Tomorrowland es una bella pieza del séptimo arte que, lamentable, no pudo ser comprendida y apreciada.
        
            