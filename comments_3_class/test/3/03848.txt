
                      Es bastante buena, quizás lo de menos sea la historia que cuenta que para mi queda en segundo plano. Pienso que los personajes principales son los paisajes y la utilización magistral de la posición de la cámara para meterte en la acción. También se pueden destacar las actuaciones y las caracterizaciones de los actores. Recomendable verla.
        
            