
                      Sinceramente no me la esperaba tan...buena. Es cierto que tiene algunos defectos pero en general el resultado es estupendo. Lo más positivo es que muestra una personalidad propia y un tono y "crudeza" que (afortunadamente) se aleja de lo visto en las trilogías. Además, sirve como estupendo prólogo del episodio IV.

Si alguien quiere, puede leer mis razones para decir todo esto en el link que adjunto.

En resumen: una película bastante recomendable.
        
            