
                      A grandes rasgos, y sin revelar nada más allá de la propia sinopsis, Resident Evil: El Capítulo Final sigue la historia donde lo dejó Resident Evil: Venganza, pero con Alice como único hilo conductor entre la quinta y sexta entrega, además del propio estado postapocalíptico del mundo, cuya humanidad está al borde de la extinción.

La única solución posible pasa por regresar a La Colmena en Raccoon City para intentar desesperadamente frenar la epidemia mortal liberada por Umbrella. En cierto sentido, es un acierto que la sexta entrega cierre el círculo de una manera casi literal, regresando a los escenarios de la original, pero varios años después. Es una solución mucho más acertada que ir recorriendo el mundo sin ningún tipo de lógica, solo por ver los efectos del apocalipsis del Virus-T a lo largo y ancho del globo, o incluso por medio de simulaciones de Realidad Virtual como en Resident Evil: Venganza.
        
            