
                      Fantástico cuarteto interpretativo para un drama neorrealista con tintes de cine negro y policiaco cuya trama te envuelve desde el principio. La simple presencia de Silvana Mangano ya la haría icónica, pero es mucho más. Cruda, desgarradora e impactante para su época. Una imprescindible del cine europeo
        
            