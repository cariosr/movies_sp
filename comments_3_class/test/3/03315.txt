
                      Dos horas y media de película pero para nada se hace larga. No me gustan ni el estilo western ni los tiros ni la sangre, pero es que esta muy bien hecha. Estupendo guión, con caídas graciosas, estupendas actuaciones de los actores principales, muy bien planteada la trama que te mantiene enganchado. Aunque coincido con una de las críticas en que los últimos 20 minutos son un tanto extraños, digamos forzados. Al estilo del director, eso si, no nos libramos de la sangre por doquier, los trozos de carne volando y tiros y más tiros. Recomendada!
        
            