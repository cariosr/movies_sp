
                      Sarah (Jennifer Connelly), una adolescente solitaria y fantasiosa, se adentra en un mundo extraño y mágico donde nada es lo que parece y nada se puede dar por sentado para recuperar a su hermanastro de las garras del malvado rey de los goblins (David Bowie). Para recuperar al niño, Sara tendrá que atravesar El Laberinto, un lugar lleno de encrucijadas y acertijos y poblado de los más extraños personajes. 
La historia es sencilla y dinámica, llena de fantasía y relatada con gran sentido del humor, entretenida tanto para adultos como para niños. Junto con Cristal Oscuro, se trata de uno de los dos principales largometrajes dirigidos por Jim Henson (Creador de series infantiles como "The Muppet Show" o "Fraggle Rock") a lo largo de su vida. 
Para los que estamos un poco saturados de los actuales efectos digitales esta película es un refugio al que volver siempre que deseamos estimular la imaginación y vivir un mundo de fantasía.
El DVD viene acompañado de un documental sobre el proceso de elaboración de la película que recomiendo encarecidamente.
        
            