
                      Un broche de oro que pone fin a una trilogía perfecta de una manera brillante. Banda sonora a destacar de nuevo y actores que cumplen a la perfección su papel. Había dudas sobre si Bane (Tom Hardy) alcanzaría el nivel del Joker en la segunda, y la verdad es que ejerce una presencia brutal durante toda la película con tan solo la expresión de sus ojos. 5/5
        
            