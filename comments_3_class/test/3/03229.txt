
                      "Me ahoga tu amor", le dice Nicolás a Emilia en uno de los momentos más profundos de la película. Esa misma sensación, ahogo, es lo que inunda las 2 horas de cinta. Un despliegue de pasiones desbordantes me dejó noqueado en alguna parte de la película hasta el punto que sentí pena por esa mujer que no cesa en su esfuerzo de cambiar la vida pacífica al lado de su marido y su hija por dejarse la vida de forma tragicómica al asimilar que su sino es el de ser desdichada.
Banda sonora reducida al saxofón del amante de Emilia; planos escogidos con gusto y precisión, los cuales describen tan bien qué sienten los personajes, que no te acuerdas que el 90% de la película se desarrolla en un piso de clase media-baja.
Desgarradora, sobreactuada en alguna parte con diálogos más propios del teatro clásico, pero real, sobre todo creíble.
        
            