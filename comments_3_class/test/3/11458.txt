
                      Esta película ha sido comparada con ''Los Juegos Del Hambre'', ''Divergente'' y ''La Huésped''. Pero a diferencia de estas tres, tiene acción desde el primer momento en que empieza la película, es de las mejores en su género, si te estas creyendo las críticas negativas, permíteme decirte que eres un conformista, si no ves esta película en el cine, te arrepentirás tanto, quizás algunas personas no entiendan el mensaje que esta película da, y se van por la tangente sacando cosas como ''solo los sexys quedaron vivos'', dime tu ¿acaso en otras películas no utilizan este tipo de enganche?
Así que te recomiendo que la veas, es una gran película.
        
            