
                      Un grito en el desierto

Una gacela huye despavorida entre las dunas del desierto, sus enormes saltos zigzagueantes parecen concebidos para evitar las balas de los fusiles de asalto que desde un todoterreno le lanzan un grupo de yihadistas. De esta forma tan impactante y brutal, comienza la película del director mauritano Abderrahmane Sissako. La metáfora visual es tan explícita que no puede conducirnos a erróneas interpretaciones: “Timbuktu” es un grito contra la sinrazón de los fanatismos y la crueldad de las guerras, es una llamada desesperada de denuncia ante todos los totalitarismos, encarnados en este caso por el Yihadismo islámico, que como una tormenta en el desierto, va cubriendo de arena el norte del continente africano.

   Es muy importante que el autor de esta película sea musulmán, mauritano de nacimiento y maliense de adopción.  Estas particularidades le confieren plena autoridad para ilustrarnos sobre lo que está ocurriendo en los países del Magreb y el Sahara Occidental.  La decisión de rodar “Timbuktu” la tomó después de ver en internet, la imagen de la lapidación de una pareja por cometer el “terrible crimen” de estar viviendo juntos sin estar casados.  -¿Qué puede hacer un artista en el momento en que la humanidad está siendo acosada por hordas filisteas y destructivas?- Reflexiona Sissako, que ya ha visto como su película ha sido retirada de varias salas comerciales en Bélgica, después de los terribles asesinatos de los trabajadores de la revista satírica francesa Charlie Hebdo. Los exhibidores cinematográficos han tenido miedo que “Timbuktu” pudiera desencadenar la ira de radicales islamistas. 

   El músico y actor Ibrahim Ahmed, es uno de los actores principales de la película. En una entrevista reciente para TVE, declaraba: "Cuando acepté mi papel en esta película era consciente de lo que hacía. Si empiezas a pensar que pueden matarte no puedes vivir. Lo acepté porque creí que era mi deber, mostrar este país, el Sahel, las gentes entre las que nací y mostrar que mi personaje, Kidane, está al margen de la política". 

Leer todo la crítica en el siguiente enlace:
        
            