
                      Una película profunda, con una visión de la vida y el más allá que cuanto menos invita a la reflexión. Un hermoso cuadro que a cada minuto se completa con las pinceladas del director sobre el lienzo de nuestra psique
        
            