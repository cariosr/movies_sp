
                      Hacía mucho tiempo que no había visto una comedia romántica tan buena. Tom Hanks y Julia Roberts están inmensos! Una película que te hace reflexionar, un poco cliché, pero muy convincente… Además: una banda sonora original extraordinaria.
        
            