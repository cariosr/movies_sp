
                      Recomiendo a todo el mundo acompañar a los tres personajes en esta escapada hacia adelante. Un viaje con banda sonora de los Beatles. Un road trip en el que como en las geniales  Pequeña Miss Sunshine o Una historia verdadera, lo de menos es el destino, en el que no habrá carcajadas, pero mantendremos la sonrisa durante todo el trayecto.
        
            