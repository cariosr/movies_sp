
                      "El color de la montaña" es la historia de un conflicto visto a través de los ojos de un niño: Manuel. Película muy, muy bella. Manuel, muy entrañable, ve la guerra todos los días. Él sólo quiere recuperar su pelota, es lo único que tiene en la mente. Es una experiencia muy conmovedora, abrumadora y una fuerte crítica a la guerra en general que, además de quitar vidas, rompe otras.
        
            