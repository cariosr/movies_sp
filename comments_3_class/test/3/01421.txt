
                      Esta primera aproximación del famoso libro de Tolkien es una buena película pero no desde el primer minuto, lo bueno se hace esperar; la acción llega a la mitad del largo filme (casi 3 horas de película) y no desaparece hasta el final. Habrá una segunda y una tercera. Aquí habrá un espectador dispuesto a pagar la entrada. Vista en 3D y 48fps, no recomiendo ese extra esfuerzo económico. Un buena película para descubrir los límites de nuestra imaginación al leer el libro.
        
            