
                      Buen ritmo y buena atribución de protagonismo a piezas clave en la trama. Se entiende demasiado bien y si vuelves a ver otra ves la película te fijas en detalles en el guión que son interesantes. En contra, hay una escena que de repente es de noche y al poco es de día. A favor, tiene un poco de todo; intriga, terror, drama, acción...
        
            