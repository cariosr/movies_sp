
                      Nos encontramos ante una película muy expresiva, con más sentimiento (que no sentimentalismo) que la mayoría de producciones de los últimos años. A esto ayuda enormemente el estilo visual de la película, sin ninguna duda el más adecuado para el tipo de historia que se quería contar. Los autores juegan con el color (la primera vez que aparece el color en la película, la cual se desarrolla en su mayor parte en blanco y negro, es cuando la protagonista está esperando en el aeropuerto para volar a Occidente), con las sombras (esos malos siempre en penumbra), las luces, la perspectiva, la profundidad... De esta manera magistral enriquecen la película y complementan y llenan la historia de detalles de calidad, así como de simbolismos muy sugerentes y críticos.

Estamos ante una de las sorpresas más agradables de los últimos años. Una película que consigue relatar una historia difícil y emotiva, sin caer en sentimentalismos ni partidismos exagerados, y aprovechando al máximo todas las oportunidades que ofrece el formato elegido.

Crítica completa en "Sayonara, baby":
        
            