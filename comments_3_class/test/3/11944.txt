
                      Lo Imposible es una película increíble, y me alegro de que el cine español haya hecho algo semejante. Es una película de cine, y sé que es algo complicado ir hoy en día por los precios que hay, pero de veras que merece la pena. No digo nada más por no estropear nada. Una de las películas con las que más he disfrutado en el cine. 100% recomendable. Ah, y papelón de todo el reparto, me quito el sombrero con la increíble interpretación de todos los actores.
        
            