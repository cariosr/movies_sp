
                      'Del revés' logra a través de una animación impecable defender la importancia de la tristeza, emocionar en su recorrido por la mente de Riley y hacer tanto reír como llorar. Una obra preciosa, pero que -debido a su complejidad- disfrutarán más los adultos que los niños.
        
            