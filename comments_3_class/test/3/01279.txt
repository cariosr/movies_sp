
                      Para gustos pintan colores. Una cosa es que no guste la película y otra que digan que es un bodrio, una basura o una escoria. Se agradecen todos los comentarios incluso cuando para algunos son más que afirmaciones, poseedores de la verdad. 
La película, miren por donde la miren, es un derroche de montaje, acción, de impacto, de efectos sublimes. No recuerdo una acción tan trepidante y tan bien hecha desde nunca. 
Y repito, puede que guste o no guste pero para los que no, que dejen de decir bodrios y basuras. A mi no me gusta mucha gente y no por ello son bodrios.
        
            