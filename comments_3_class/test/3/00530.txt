
                      Una buena película que trata un caso de secuestro desde la perspectiva de un niño nacido dentro del mismo. Bien interpretada y bien realizada, con el dramatismo justo. Sin grandes escenarios ni efectos ni nada que distraiga de la historia. Recomendable.
        
            