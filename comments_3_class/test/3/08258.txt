
                      Como cada puente de la Purísima-Constitución, tocaba la peli del momento de Disney. 
Volver a emocionarte con dibujos animados. Saber que, pese a tener 42 años, el niño sigue vivo dentro. En fin, un acto de reconciliación con uno mismo. Si además, acudes con la mejor compañía, qué más se puede pedir.
Aceptando los momentos canción propios de Disney, la metáfora con la vida y la actitud del hombre con la tierra es preciosa.
A nivel técnico, la animación es espectacular. La banda sonora magnífica.
Muy bonita.
        
            