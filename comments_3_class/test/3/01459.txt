
                      Un film impactante y dramático, cubierto de escenas crudas, únicas e intensas. Posee una historia penetrante y delicada que nos invitar a recorrer los sentimientos e ideas de cada personaje. Genera un oscuro y devastador escenario, lleno de suspenso y tensión.
        
            