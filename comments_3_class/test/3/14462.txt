
                      Una obra maestra dirigida por Ridlay Scott con una historia y un reparto de lujo. 
Russell Crowe hace uno de los mejores papeles y más expresivos de su vida como el héroe que debe sobrevivir y vengar la muerte de su familia después de ser traicionado por el nuevo Cesar de Roma, interpretado por Joaquín Phoenix que logra convertirse en uno de los mejores villanos de la historia del cine.
La historia es conmovedora y apasionante, llena de momentos intensos, y también escenas tristes y duras (Las cuales pueden sacar unas lagrimas incluso a los menos emotivos)
Es sencillamente una de las películas imprescindibles que hay que ver al menos una vez en la vida, y la banda sonora es de lo mejor que se ha hecho musicalmente hasta el día de hoy, un auténtico espectáculo tanto visual como musical.
        
            