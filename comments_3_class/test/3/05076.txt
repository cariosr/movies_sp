
                      Es cierto una vez más: Allen repite su modelo, con claros síntomas de agotamiento, pero sigue siendo capaz de hacer pasar un rato divertido, la diferencia es que ahora sabemos que el final será indefectiblemente positivo, no cargado de tristeza y de nostalgia, como en su época dorada. Puede que a muchos --incluso antiguos incondicionales de Allen-- no les guste ni esta ni ninguna otra de sus comedias-clon, pero a mí me compensa porque, al contrario que la religión, su cine sigue sin exigir el sacrificio de la inteligencia.
        
            