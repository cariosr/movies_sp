
                      Una vez más los hermanos Cohen nos demuestran su dominio del séptimo arte. No es una película para todos los públicos, ya que la personas que no tengan unas nociones sobre este noble arte, pueden llegar a notarla lenta e incluso aburrida. Sin embargo los hermanos nos narran de forma magistral la dificultad con la que se encuentra un músico en encarrilar su carrera para poder ganarse la vida con ello. No esperes algo del estilo a otras obras de los hermanos, pero si que se nota su sello.
        
            