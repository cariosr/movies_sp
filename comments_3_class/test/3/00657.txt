
                      A nivel técnico la película es sobresaliente: el movimiento, las texturas, la expresividad de los muñecos es inmejorable. En cuanto a la historia, tiene un poco de todo, humor, aventuras, emotividad y guiños constantes tanto al público adulto como al infantil.
Además de todo esto (que no es poco) el cortometraje "Día y noche" que se proyecta inmediatamente antes de la película es de los mejores que he podido ver en los últimos tiempos. Estupenda sesión de cine.
        
            