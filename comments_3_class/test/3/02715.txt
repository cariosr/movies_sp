
                      Remake de la película del mismo nombre, protagonizada por el oscarizado John Wayne. 
El largometraje en general está muy bien, destacando principalmente las actualizaciones de Jeff Bridges y Hailee Steinfeld que inundan la pantalla cada vez que intervienen.  La de Matt Damon sin embargo, y bajo mi punto de vista, está un poco por debajo de estas dos interpretaciones. 
La película está bien trazada y elaborada, pero creo que le habría venido bien unos retoques más al desarrollo de la trama, ya que en determinados momentos, se hace un poco lenta. La fotografía y el color son de lo mejor, destacando los paisajes nocturnos. Los diálogos de los personajes son soberbios, haciendo que permanezcamos atentos a ellos de principio a fin.
He de decir también,  que la primera vez que la ví, el final me dejó con ganas de más, llegando a pensar era un poco flojo, pero al verla de nuevo, y con la perspectiva del tiempo, me dí cuenta de que contenía un mensaje un poco más profundo y que no era tan débil como me pareció. 
Película recomendada para los amantes del western y para aquellos que quieran pasar un buen rato frente a la pantalla viendo cine de calidad. 
Firma y sello de los hermanos Cohen. 
Buena película.
        
            