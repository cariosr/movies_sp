
                      Había leído el libro, el cual me había gustado bastante. La película también me ha gustado bastante y es de las pocas pelis que respetan fielmente los libros en los que se basan. Tampoco puede considerarse una gran película porque la novela tampoco puede que sea muy cinéfila al punto de impresionar. La película ha quedado bastante bien, los paisajes preciosos aunque creo que le sobra algo de lluvia. Para mí bastante recomendable tanto como la novela.
        
            