
                      A ver si les recuerda algo este argumento. Un extranjero llega a un lugar, donde hay bandas rivales enfrentadas, a cambio de poco consigue trabajar para ambas bandas, consiguiendo el enfrentamiento entre ellas y su desmantelamiento, para al final, él salir airoso y triunfante. Han acertado, se trata del western de Sergio Leone y protagonizado por Clint Eastwood titulado Por un puñado de Dólares. Si este argumento lo trasladamos a una cárcel francesa, en los tiempos actuales, con problemas actuales como el cáncer, la droga y la marginación étnica, obtendremos Un profeta. Incluso en la presentación de los diversos personajes se parece a la de El bueno, el feo y el malo.

No es que en el apartado técnico o artístico, este mal realizado. Pero tampoco es original. Quizás algo más cruda y desasosegante que Celda 211. En resumen:
Como diría Groucho Marx, un personaje que salio de la nada para llegar a las más altas cotas de la miseria. (Trafico de drogas y crímenes varios).
        
            