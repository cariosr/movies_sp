
                      Parte final de la mejor trilogía de la historia y mayor proyecto cinematográfico que haya habido. Ganadora de 11 premios Óscar que demuestran la calidad de la misma, la cual cuenta con un reparto, director, guión, maquillaje, vestuario, fotografía y efectos visuales (entre otros) increíbles. La mejor película épica, surgida del mejor proyecto, que perdurará en la historia y marcará una era.
        
            