
                      Con una incesante calma, 'Spotlight' desentraña una realidad tan cruda como cierta. Centrándose en el cómo y no en el quienes, la película habla de periodismo con mayúsculas, de la necesidad de contar la verdad y de luchar para que se haga justicia.
        
            