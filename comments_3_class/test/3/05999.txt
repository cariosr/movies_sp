
                      Película diferente sobre invasiones de extraterrestres, te hace pensar sobre hechos que damos por establecidos y que las cosas no tienen porque ser lineales, no se hace para nada pesada, las dos horas pasan sin darte cuenta y en este tipo de películas, es una tarea muy difícil, hay un momento hacia el final que puedes quedar desorientado, pero sólo se queda en eso, porque enseguida la misma trama que te despista, te vuelve a situar. .
        
            