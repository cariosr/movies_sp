
                      La mejor película de DC de lo que va al 2017? Si, puede que sea una exageración, pero ni Batman vs Superman ni Suicide Squad (ni mucho menos Wonder Woman) pueden superar éste hermoso film.
(Obviamente sabemos que el Batman de Christopher Nolan entra en la lista de películas de DC, pero son dos mundos apartes y las dos se mantienen allá en lo alto).
Watchmen tiene un buen surtido de actores (Jeffrey Dean Morgan, Patrick Wilson, Carla Gugino y el gran Jackie Earle Haley que se luce completamente aquí -como siempre-) y una introducción en la que vemos grandes imagenes como la de Kennedy, la Guerra de Vietnam, y otras fotografías iconicas).
Watchmen trata muchos temas en sus 3 horas y media (al menos su versión Director's Cut).
Pero si algo se aprecia demasiado, es su increible banda sonora... te lleva a los años '70-'80.
Bien por Zack Snyder, su mejor film junto a '300'... los demas, bueno.
        
            