
                      Obra maestra con un reparto impresionante. Frenética. Te deja un buen sabor de boca. Mezcla humor, acción y lo absurdo. Crítica social muy inteligente. Me encantó.
        
            