
                      Cuando vi por primera vez Interstellar me sentí infinito y pequeño. Es una obra maestra traída por Christopher Nolan y Hans Zimmer. Nos hace pensar que nos espera en el futuro como seres humanos y reflexionar sobre la supervivencia de nuestra raza humana en la tierra. La producción es magnifica con la exactitud científica de los astros y en el viaje que hacemos con nuestros pasajeros al planeta Miller. Simplemente ¡BRILLANTE!
        
            