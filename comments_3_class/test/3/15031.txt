
                      'Cinderella Man' es de los mejores filmes que he visto del género sin duda.
Es la historia real de James J. Braddock. Situada en la época de la gran depresión. Cinderella Man logra alimentar el alma de quien admira la cinta. Con un admirable sentido de lucha ante una situación tan terrible, los protagonistas nos conmueven por la manera en cómo encaran sus carencias y temores, para al final mostrar el gran poder que tiene el seguir un sueño y no rendirse pese a la gravedad de las circunstancias.

La cruda realidad está magistralmente narrada y junto con la interpretación de Russell Crowe consiguen emocionar a todos los espectadores, y en más de una ocasión tener algún pañuelo cerca nunca está de más. 
La historia es cuanto menos sobrecogedora, aunque algunas veces intente jugar con nuestras emociones, siempre está justificada y eso es algo que es muy difícil de conseguir.
Las escenas de boxeo son una delicia, la ambientación es brutal, la fotografía exquisita. Una de las mejores puestas en escenas que he visto.

Las actuaciones son maravillosas, Russel Crowe está sublime, me atrevo a decir su mejor actuación después de 'Gladiator' y 'Una Mente Maravillosa'. Renée Zellweger hace un maravilloso trabajo, diría que posiblemente sea su papel más serio. Paul Giammati como siempre un gran actor secundario, poco valorado, está fenomenal.
En definitiva una cinta que me llegó a tocar el corazón. Obra maestra.
        
            