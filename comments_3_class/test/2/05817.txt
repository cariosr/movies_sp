
                      El primero en años en no protagonizar al difunto Paul Walker, Fast and Furious 8 continúa la tendencia de la franquicia de sobresalir cuando se trata de una inverosímil acción vehicular, junto con algunas peleas de puños bastante sólidas en el camino.

Todo lo que te gusta de la serie está aquí: autos rápidos, referencias a "familia", Tyrese Gibson gritando. Sin embargo, la película olvida sus puntos fuertes ya que se ve atrapada en intentos equivocados de introspección con su nuevo villano Cipher, un ciber terrorista, interpretado por Charlize Theron.

Como está casi encerrada en una sala de control durante toda la película, y considerando su trabajo en Fury Road, parece una oportunidad perdida. En lugar de acción, supuestamente amenazamos con analogías sobre cómo ella es el cocodrilo en el abrevadero, o algo así.

F. Gary Gray, director de Straight Outta Compton y recién llegado a la serie, tiene problemas al ritmo de la acción, a menudo sacando el pie del acelerador en lugares equivocados. A mitad de la película, lo que podría haber sido una pieza extraña y tecnológicamente profética, en la que una flota de autos no tripulados está controlada por Cipher, se cae debido a este error de juicio.

Todo esto no quiere decir que la película no tenga sus momentos: después de un prolongado declive en el segundo acto de la película, hay destellos de esa clásica locura de Fast and Furious: un submarino persiguiendo una flota de autos de lujo, The Rock redirigiendo un torpedo con sus manos desnudas, y una secuencia de acción con Jason Statham incluso cunas directamente de John Woo Hard Boiled. Pero no es suficiente para esta entrada mediana en la franquicia.
        
            