
                      ¿Son de confiar todas las personas a quienes les hemos dado nuestra confianza? La respuesta es claramente, no y este largometraje lo deja bastante claro.
Hay personas que nacen con estrella y otras que nacen estrellados, luego hay personas que su grado de felicidad es proporcional a la infelicidad de quienes le rodean, como es el caso de Cesar, el protagonista del filme. Un personaje gris oscuro, frío, solitario e infeliz, interpretado por un cada vez más creíble Luis Tosar (También la lluvia, Celda 211, Te doy mis ojos, Los lunes al sol).
A estas alturas del año, el cine español está ofreciendo productos bastantes aceptables, basándose en protagonistas bastantes retorcidos, y de personalidad poderosa (No habrá paz para los malvados, La piel que habito y Blackthorn, sin destino).
En resumen, película entretenida sin muchos sobresaltos, ni artificios, sin abusar del susto fácil ni de la hemoglobina (a excepción de una escena, la más macabra que no la más cruel del todo el metraje), y con un final desolador y nada complaciente.
        
            