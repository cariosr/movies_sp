
                      Deja ver un relato gris entre ambos protagonistas en un pulso contra el miedo para encontrar la verdad y las pruebas. Trepidante a ratos y oscura a otros. Sugestiona al espectador para convertirlo en un figurante más. Pierde fuerza al avanzar la película pecando de ser predecible y mal acabar argumentos prometedores tirando de lo estipulado.
        
            