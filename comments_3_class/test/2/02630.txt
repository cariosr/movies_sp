
                      Una película regular. Empieza muy fuerte, con un Statham bastante enérgico, pero a medida que la película avanza, el espectador se pierde en intrigas inútiles, demasiado largas por momentos. Y un asesino de dos al cuarto que no asustaría ni a una mosca, sea cómo sea, el policía es incapaz de detener a un holgazán. El guión es malo, pero queda el consuelo de un Statham tan bueno como de costumbre.
        
            