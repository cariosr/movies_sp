
                      Repetitivas, tediosas y casi sin alegría, "Piratas del Caribe: En mareas misteriosas" se esfuerza por reiniciar lo que hasta ahora era una empresa cinematográfica generalmente divertida y siempre rentable. Es un juego de acción competente y enérgica, jugado con tanto vigor como Johnny Depp puede ofrecer, con un nuevo escupitajo (Penélope Cruz) como su florete femenino.

Pero el romping ha perdido gran parte de su rebote. El nuevo director Rob Marshall ("Memorias de una geisha") ofrece una sucesión interminable de escapes inventivos ocasionales que el capitán Jack Sparrow y su equipo de especialistas deben gestionar. Cuanto más dura esta larga película, más cansados ​​que emocionantes se vuelven.

Y "Mareas extrañas" es oscuro, literalmente. Por favor, señor Rob Marshall, señor, ¿puede ahorrar unos cuantos pedales de luz?

El Capitán Jack se encuentra en Londres, lo que impide que se escape de su fiel primer compañero (Kevin McNally), cuando oye que un barco se dirige a la cantera favorita de Jack: la Fuente de la Juventud. Un par de escapes, una persecución por el techo de un carruaje por las calles y una pelea de espadas o dos más tarde, él está a bordo de la nave de Edward Teach, "el pirata a quien todos los piratas temen": Blackbeard.
        
            