
                      Aunque pueda tener trazas de comedia al uso, "De vuelta a casa" tiene una química entre personajes y algunas situaciones que, si bien en cierto modo puedan ser irreales, le dan gracia a la película. Reese Witherspoon se mueve como pez en el agua en este tipo de situaciones y la película resulta bastante simpática
        
            