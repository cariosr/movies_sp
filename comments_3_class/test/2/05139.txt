
                      Un cortometraje-documental de tan solo 25 minutos.
El día a día de un hospital, de la zona de UCI, donde la vida y la muerte batallan a pulso.
Una doctora de cuidados intensivos equilibrando la balanza de la vida.

Tan real como la vida misma. Muestran dos casos de familias que van a ser intervenidos por ventilación artificial.
        
            