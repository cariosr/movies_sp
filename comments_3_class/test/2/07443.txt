
                      Tomb Raider (2018).
Otra mujer de armas tomar en la gran pantalla. La féminas están de enhorabuena en los últimos meses.

El director noruego de La Ola dirige, en esta nueva revisión cinematográfica del ya clásico vídeojuego, a la bellísima actriz sueca Alicia Vikander, quien, tras 7 meses de duro entrenamiento y sacrificada dieta, encara el personaje magníficamente. Y aprueba con nota.

Nos encontramos ante lo que muchos creerán que es una película de aventuras, y sin embargo, es un drama de acción y violencia dignamente facturado, donde nuestra protagonista sufre, y mucho en su viaje al encuentro de su padre desaparecido, encarnado por Dominic West. 

El villano, en esta ocasión, es Walton Goggins (también villano en la serie Justified, y "odioso" en el último western de Tarantino). Como secundarios en papeles muy breves encontramos a la gran Kristin Scott Thomas y al cómico Nick Frost.

Muy entretenida a pesar de sus dos horas de metraje (no os perdáis la fuga de nuestra oscarizada heroína ya en la isla). Por cierto, habrá más Lara Croft...

Raúl Cabral.
https://www.filmaffinity.com/es/film213699.html
        
            