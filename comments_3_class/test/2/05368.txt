
                      Si no fuera por la magnífica interpretación de José Sacristán y por cuatro o cinco puntos de humor, aunque algo manidos, la película estaría bajo suspenso. No convence en la historia que plantea y los límites de “topicazos” se sobrepasan de forma continua. Contemporánea en la trama pero añeja en las ocurrencias.
        
            