
                      Pues ahí, ahí. Es lo suficiente buena para verla sin sufrimiento pero no para destacarla. No es película de carcajada. Más bien es costumbrista. Es mucho más sería de lo que esperaba. Lo que más choca es gente que se ríe de los protagonistas por feos cuando ese personaje lo es todavía más.
        
            