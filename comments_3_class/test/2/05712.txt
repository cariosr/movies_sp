
                      Muy buen film policial. El director crea una buena atmosfera de tensión y suspense. Joel Edgerton y Tom Wilkinson hacen muy bien su trabajo, y Jai Courtney (actor revelación por allá en el 2012 luego de aparecer junto a Tom Cruise en Jack Reacher) tiene toda la facha de un detective serio y decidido.
        
            