
                      El Corredor del Laberinto se queda en una mera (pero imprescindible) introducción al mundo post-apocalíptico escrito por James Dashner. No le falta emoción y le sobran misterios. La acción es entretenida sin llegar a extasiar aunque el objetivo final (escapar) acaba siendo agónico. Muchas cosas (¿demasiadas?) resultan familiares a otras sagas, y como buena (o no) adaptación de una trilogía: todo queda absolutamente abierto… Y aunque no sepas por qué, seguramente querrás ver como sigue. Lee la crítica completa en el siguiente enlace:
        
            