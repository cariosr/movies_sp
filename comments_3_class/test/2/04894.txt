
                      Animal Nocturnos, nos propone la visión de un escritor y su tragedia. Ficción sobre ficción. El personaje de Susan (Ammy Adams) nos lleva con su lectura, lapsos de impresiones y nos da énfasis para entender los puntos por lo que decidió no estar con Tony - Edward Sheffield (Jake Gyllenhaal). La interpretación de su personaje por momentos es sorpresivo, y no dejo de ver los mismos gestos que vi en Arrival (la llegada). No hay mucho trabajo con su actuación, se mantiene en la misma posición sugestiva. No me atrapó de nuevo con su estilo. 

Con el personaje del escritor tengo un punto muy claro con Jake Gyllenhaal. Su estilo polvorín que últimamente lleva en sus películas no me quitó de la mente ese hombre pedante y explosivo. Como lo muestra en Nightcrawler (Primicia Mortal), Prisoners (La sospecha), Southpaw (Revancha), Enemy (El Hombre duplicado). Considero que de Enemy, el personaje salió a personificar el papel del novio-escritor de este nuevo filme. 

Si presentará esta historia en una obra de teatro con estos actores, estaría muy lineal y por momentos cansaría a las personas. La historia es una acto cruel de salvajismo. Como también el salvaje director de estirar las historia hasta que las emociones del personaje del escritor, cumpla el significado de la película. 

Con Bobby Andes (Michael Shannon) y su personaje, lo lleva como cualquier otro oficial de la actualidad. Supo esconder expresiones y dar de golpe el significado de su papel. 

Un punto por aplaudir de esta película, es el flashback que por momentos nos muestra a estos dos personajes Susan y Tedy con un aspecto juvenil. Un logro que da realismo y credibilidad a esta historia.  El drama de Animales Nocturnos, nos muestra una especulación por querer ordenar nuestras emociones. Reivindicar el pasado y cerrar ciclos.
        
            