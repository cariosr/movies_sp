
                      El "típico producto MARVEL" para el cine. Este film dirigido por Kenneth Branagh, es una adaptación del cómic "Thor, rey del trueno" y nos presenta una historia fantástica de la mitología nórdica. No es nada del otro mundo, pero entretiene aprovechando las ventajas del cine 3D, La película se apoya principalmente en los efectos especiales que permiten un despliegue visual único al representar los Reinos desconocidos y los elementos sobrenaturales. Cine "de palomitas en estado puro".
        
            