
                      Tengo que reconocer que al principio, como la primera media hora del film, la cinta me pareció un poco aburrida. Era todo demasiado correcto y quizá incluso lento. A partir de esa media hora todo cambia. Las diferentes situaciones decadentes en las que se ve envuelto Dorian, el protagonista, la descomposición del cuadro, los asesinatos...
Me encanta el trabajo del actor Colin Firth. Diferente a todo lo que ha hecho hasta ahora, bien ubicado en su papel de gentleman inglés, tan elegante y distinguido...
Sin embargo, el trabajo del resto del reparto cojea a su lado, incluyendo el del protagonista. Su interpretación me pareció similar a la de muchos jóvenes actores adolescentes americanos, del tipo "Crepúsculo", algo vacía de emociones a pesar del argumento.
Aún así el trabajo de ambientación del Londres del siglo XVIII y XX es un trabajo verdaderamente logrado: vestuario, calles, decorados, caserones y los primeros Fiat.
En general, una buena película.
        
            