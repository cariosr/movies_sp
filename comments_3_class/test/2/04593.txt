
                      Una más.  Esa es la sensación con la que sali del cine. 
Era consciente de que iba a ver una película épica con un desarrollo y un final ya conocido antes de sentarme en la butaca. Albergaba la esperanza de que estuviese contada o filmada de una forma distinta que te enganchase durante 150 minutos y que al salir del cine tuvieses ganas de más,  ganas de volver a verla. Ni los efectos especiales tienen algo novedoso que merezca la pena recordar ni la trama engancha.
En cuanto a las interpretaciones más de lo mismo. Mas allá de los esclavos y las megaconstrucciones,  ni los egipcios ni Ramses II me trasmiten el gran imperio que fueron.

Con el paso del tiempo me doy cuenta que Ridley Scott se quedó en Gladiator. Desde entonces cree que con grandes decorados lo tiene todo hecho pero al final lo que hace que una película sea buena es el guión,  no el presupuesto en cartón piedra.
        
            