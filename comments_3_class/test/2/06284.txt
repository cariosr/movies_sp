
                      Con un corte muy de cine independiente y una violencia muy explícita, esta curiosa "Outlaws and Angels" protagonizada por la mismísima hija de Clint Eastwood es un western extraño, atípico, más cercano a los films del género de Tarantino o a "Bone Tomahawk" que a una película del oeste al uso. Los personajes, los planos, el desarrollo de las escenas de interiores, etc nos aleja del western clásico y el retrato de las relaciones humanas lo hace aún más extraño y "malrrollero".
        
            