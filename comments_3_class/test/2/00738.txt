
                      Una comedia delirante sobre un tema particular como el terrorismo. El humor burlesco británico, las escenas que alternan lo bueno y lo no tan bueno. En general, muy irregular, aún y cuando el escenario traza la ruta. Pero se arrepentirán de un final que acaba en grande. Los personajes definitivamente son carismáticos, pero esta película no quedará en las memorias.
        
            