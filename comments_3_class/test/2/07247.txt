
                      Feliz Día de tu Muerte (2017).
Nueva vuelta de tuerca al célebre Día de la Marmota, ahora en plan thriller, tras haber probado la Comedia (recientemente Desnudo, con Marlon Wayans, y por supuesto, la inigualable Atrapado en el Tiempo, con Bill Murray ) y la Ciencia Ficción (Al Filo del Mañana con Tom Cruise).

Nuestra joven protagonista evoluciona y se transforma a lo largo de los tres actos de la historia empujada por los acontecimientos. Quizá sea lo más digno de señalar, pues en cuanto al resto de la cinta, aunque entretenido, no hace más que completar la clásica peli tipo Scream para disfrute del público juvenil (en EE.UU. ha sido de lo más taquillera).

Raúl Cabral.
        
            