
                      Comedia romántica que se deja ver con facilidad pero que no destaca en conjunto. Historias entrelazadas de amor que intentan dar algo de luz a las relaciones entre hombres y mujeres. Lo que no llego a entender porque las historias se entrecruzan sin que realmente lo hagan. Es decir, la gracia, a mi parecer de los cruces es que al final esto tenga algún sentido sin embargo si se hubiera hecho en episodios el resultado hubiera sido el mismo pues ninguna historia afecta realmente a otra.
        
            