
                      Regresión (2015).
Nuestro mejor director vuelve con esta coproducción hispano-canadiense con el infalible Ethan Hawke y la elegante Emma Watson en los papeles principales. Disfrutando de una factura impecable, como nos tiene acostumbrados, una fotografía magnífica y unos planos sobrios como si del mismo Eastwood se tratara, Amenábar peca, en mi opinión, de lentitud en el transcurso de la historia, aunque nos mantiene enganchados a ella gracias, entre otras cosas, al elenco de actores secundarios que acompañan a los protagonistas (David Thewlis entre ellos). 

Se trata de un thriller basado en hechos reales y ambientados en 1990, cosa que hace aún más difícil su realización (siempre es complicado plasmar fielmente una época reciente pero que tiene multitud de matices distintos a la actual; desde los coches o las ropas hasta ciertos edificios o mobiliarios). Recomendable aun no siendo su mejor trabajo.

Raúl Cabral.
http://www.filmaffinity.com/es/film392409.html
        
            