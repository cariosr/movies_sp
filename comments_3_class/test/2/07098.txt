
                      Primera película como director de Luke Scott, conocido hasta ahora por ser el director de la segunda unidad en películas de su padre, Ridley Scott, como Exodus: Dioses y Reyes o Marte. Está vez Ridley Scott tan sólo ejerce de productor ejecutivo en este thriller de ciencia ficción que ha sido recibido con tibieza por parte de la crítica que la considera demasiado parecida a Ex Machina.
[...] Asistí a un Q&A con Luke Scott y la primera pregunta que contestó estaba relacionada con el parecido de su película con Ex Machina, el director nos explicó que a una semana de empezar el rodaje se estrenó la película de Alicia Vikander y que cuando fue a verla sólo pudo decir fu**. Es cierto que las dos películas se parecen, pero Morgan tiene algunas ideas interesantes como esa conexión emocional que tiene Morgan con los científicos que siempre han estado a su lado, una conexión recíproca que hace que ellos cierren filas cuando quieren eliminarla. El guion nos depara varios giros inesperados y una sorpresa final que no es tal si estáis atentos a las pistas y sabéis leer entre líneas.
Conforman el elenco un puñado de grandes figuras, que en general se sienten desaprovechados, las protagonistas son Kate Mara (Cuatro Fantásticos) y Anya Taylor-Joy (La bruja) que nos dejan interesantes escenas de acción. Toby Jones (Wayward Pines) y Michelle Yeoh (Tigre y dragón) son los científicos principales del proyecto y Rose Leslie (Ygritte en Juego de tronos) la investigadora a la que Morgan tiene más cariño. También podemos ver a Jennifer Jason Leigh (Los odiosos ocho), Paul Giamatti (El ilusionista) y Brian Cox (El caso Bourne).
Inquietante thriller de acción que parte de una premisa inteligente y que consigue crear una creciente sensación de amenaza. Buen debut de Luke Scott.
        
            