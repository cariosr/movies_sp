
                      Basada en un hecho real, aborda la problemática del matrimonio interracial en EEUU y su judicialización contra el estado de Virginia sentando jurisprudencia en todo el territorio. Muy bien desarrollada e interpretada aportando conocimiento acerca de prohibiciones retrógradas que hoy en día resultarían inconcebibles. Algo lenta pero interesante. La recomiendo.
        
            