
                      Cuando se produce un hecho trágico, sembrado de dolor, sufrimiento y muerte, y de ese hecho se da poca información, escasean las noticias, todo lo que sea acercarse a él, bien con información periodística escrita, bien reportajes televisivos o documentales cinematográficos, es bienvenido.
Esclarecer los hechos y analizar las causas siempre es bueno porque aumenta el conocimiento para poder evitar posibles repeticiones.
Pero ante un hecho de estas características, hoy en día, es poco probable que no haya información a raudales, muchas veces desbocada, en demasía y en zafarrancho.
Es lo que pasó en su momento con los sucesos que esta película rememora. Nos enteramos de ellos casi en directo. Hubo ríos de tinta informando, analizando y sacando conclusiones de lo que había sucedido mientras sucedía y los meses posteriores, con el juicio incluido al asesino múltiple y protagonista indiscutible del suceso.
El que no se enteró es porque no quiso.
Sucedidas así las cosas era esperable que si alguien se aventura a hacer una película sobre los hechos, lo haga con la intención de añadir algo nuevo. En el plano creativo, quizás una historia con una perspectiva nueva, esclarecedora, original. Quizás un enfoque técnico novedoso, unos personajes llevados al límite. Quizás un tratamiento del asesino que nos llevase a reflexionar sobre la naturaleza humana y el momento político que vive Europa y por ende la sociedad occidental, con los extremismos de derechas creciendo.
En el plano técnico, quizás una forma de rodar sorprendente, en consonancia con cómo sucedieron los hechos.
Era esperable algo que justificase el rodaje de esta película sobre unos sucesos  harto conocidos.
Y esperando nos hemos quedado.
No he encontrado ni una sola razón, ni una escena, que justifique que está película se haya llevado a cabo.
Es un gris documento cinematográfico sin ninguna razón de ser.
Un rodaje correcto en el que se pueden aceptar como débiles aciertos el traqueteo constante de los disparos, la ausencia del asesino, pero ya está.
¡Con tanto para reflexionar que originan hechos como éste!
Apunto uno: Un individuo se agencia unas armas, pone unos explosivos en Oslo, viaja a una isla, asesina a más de setenta personas y deja traumatizadas de por vida a más de trescientas, es detenido, es juzgado y condenado a 21 años de cárcel, lo máximo en Noruega, con el probable desenlace de que dentro de 10 años esté en la calle, libre. O sea que para el 2022 posiblemente sea un ciudadano con todos sus derechos intactos. Bien comido, bien bebido y si se ha aplicado con alguna formación adquirida. Y con su ideología nazi bien arraigada en su cerebro como ha estado dando señales estos años, con denuncias incluidas al Gobierno noruego por diferentes razones. Denuncias escuchadas, atendidas y tratadas por los tribunales de Justicia de ese país.
¿Qué clase de sociedad tenemos?
Para esos e podía haber aprovechado esta película, por ejemplo.
Para que no resulte tan intrascendente e irrelevante como resulta desde todos los puntos de vista, incluido el cinematográfico, que es el que nos ha traído aquí.
        
            