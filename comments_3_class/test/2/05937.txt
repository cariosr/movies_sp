
                      Los ingredientes de esta película son unas buenas interpretaciones de los actores protagonistas (a excepción de Sofía Vergara que resulta sosa, quizás por lo insípido de su personaje), una historia familiar entrañable, buena música con aires de la Habana, el arma de doble filo que pueden ser las redes sociales y la reflexión sobre qué esperamos de la vida y qué podemos hacer para conseguirlo. El resultado, un plato sabroso que aunque no se convertirá en nuestro favorito sí nos dejará un buen sabor de boca.
        
            