
                      Centrada exclusivamente en el naufragio y supervivencia de la pareja en el bote + flashbacks constantes del pasado donde te muestran el inicio de la relación, su historia de amor y como decidieron emprender dicho caótico viaje. Resulta atrapante, triste y con un gran giro en el final. Está basada en un hecho real el cual recomiendo no googlear para que no se pierda el factor sorpresa. Interesante historia.
        
            