
                      Película muy sobrevalorada (en realidad dudo que esté tan valorada).

A priori todo pinta bien. Película mágica, con buena producción, una historia lacrimógena detrás, un niño que interpreta muy bien...

Pero en realidad la historia es muy floja, no tiene punch, y la magia está muy trillada. Es una magia sin magia.

El guión es digno de un telefilme, de lágrima fácil.

Aun así, es entretenida y está muy bien llevada.
        
            