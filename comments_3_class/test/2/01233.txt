
                      La he visto en Donostia, no soy vasca, pero me alegra que los vascos comiencen a reírse de sí mismos; excepto los que se ven retratados. Determinadas personas, cuando les ponen un espejo delante, se molestan por lo que ven reflejado. Comedia simple, para echarte una risas sin más, lo novedoso es el tema, que refleja una parte de realidad.
        
            