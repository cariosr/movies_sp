
                      Dos Buenos Tipos (2016).
Las pelis de parejas de amigos/compañeros suelen funcionar. Aquí nos encontramos ante otra más que trata de explotar esta antigua fórmula: por un lado el veterano Russell Crowe, orondo como nunca, y por otro, Ryan Gosling en un registro algo distinto al que nos tiene acostumbrado. Los acompaña la incombustible Kim Basinger, algo "retocada", en un papel secundario.

La cinta mezcla acción, violencia (no excesiva) y humor con un guión que no la hace redonda pero sí interesante. Tiene sus momentos; algunos de ellos recuerdan a realizadores irreverentes del tipo Hermanos Farrelly o incluso Tarantino, pero sin llegar a la calidad de estos. 

Otro de los protagonistas del filme es la ambientación: la acción se sitúa en 1978, tiempo de fiestas, drogas, amor libre y, cómo no, de buena música de grupos como Earth, Wind & Fire...
Entretenida y recomendable para echar un buen rato.

Raúl Cabral.
        
            