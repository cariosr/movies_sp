
                      Election: La Noche de las Bestias (2016).
Estamos ante la segunda secuela de La Purga. Por tercera vez vuelven a estar el mismo director de las otras dos tras las cámaras, y Frank Grillo, como el protagonista de las últimas dos partes.

Curiosamente esta tercera película es más entretenida y crítica de lo que fue la segunda, y eso que es una cinta de acción, tensión y violencia sin mucho guión con el que complicarse la existencia.

El título hace referencia a las elecciones a la presidencia de EE.UU., donde los intereses de los poderosos tendrán mucho que decir en esta historia. Una vez más, el cine estadounidense sirve como denuncia al sistema del país de las barras y estrellas, aunque sea a través de una distopía donde una noche al año se da carta blanca a la ciudadanía para que se desahogue con la idea de que no "peque" el resto del año... desde luego, salen perdiendo los de siempre; esto es, los más desfavorecidos de la sociedad. 

A nuestro rudo protagonista lo acompaña la bella Elizabeth Mitchell (series Revolution y Perdidos). Entretenida como antes apuntaba.

Raúl Cabral.
        
            