
                      Robin Hood (2018).
“De día serás Robin de Locksley. Te codearás con los tipos más repugnantes que hay: los ricos."
Nueva vuelta de tuerca al héroe inglés del siglo XIII en plan anarquista gamberro, y es que; en unos tiempos (los actuales) en los que en Francia el pueblo la arma a tope cuando les suben los impuestos, y en Andalucía irrumpen fuerzas políticas al más puro estilo hitleriano, nos encontramos con una nueva versión del célebre personaje, solo que muy alejado de las interpretaciones que hicieran Errol Flynn o Kevin Costner en su momento. 

“En las minas hay un gas que se llama grisú. Solo una chispa puede prender todo el aire: tú eres esa chispa."
El joven y apuesto Taron Egerton deja su traje de agente especial Kingsman para enfundarse el de 'Robín de los Bosques'... aunque aquí aparezcan pocos bosques...

“Algunos dicen que robo a los ricos para dárselo a los pobres, pero si los ricos os roban a vosotros... ¿quién es el ladrón?"
La cinta parece pretender reivindicar la 'redistribución de la riqueza' entre todos (y de hecho se nombra), convirtiéndose en una versión, no solo más actualizada, sino también más moderna gracias a los procronismos (anacronismos de épocas pasadas con ciertos toques modernos) en los que vemos un vestuario, una banda sonora e incluso unas armas más avanzadas de lo que les corresponde a ese siglo. Este fenómeno ya se ha visto en filmes del tipo “Destino de Caballero” o “Wild Wild West”.

“El miedo es el arma más poderosa del arsenal de dios (sí, con minúscula, por supuesto). Por eso la Iglesia creó el infierno”.
A Egerton le acompaña el oscarizado Jamie Foxx en un personaje mezcla entre Little John y el árabe que encarnara Morgan Freeman en la versión de 1991. Como villanos destacan el prolífico Ben Mendelshon (en la última década lleva una treintena de largometrajes incluyendo "Roge One”) como el odioso Sheriff de Nottingham, y un rescatado F. Murray Abraham como el cardenal de turno...

Buenos efectos especiales, bonitas localizaciones (Dubrovnik) y entretenidas escenas de acción, aunque sin llegar a la calidad de la versión en la que Kevin Reynolds dirigió a su tocayo Costner.

Raúl Cabral.
https://www.filmaffinity.com/es/film838413.html
        
            