
                      Decepción. Me esperaba muchísimo de esta película, quizás por ello no me gustó como me imaginaba. La trama no es nada del otro mundo, se supone que te lo tienen que desvelar al final y yo me di cuenta casi al comienzo de la película. En algunas ocasiones se hace incluso hasta pesada, pero a pesar de ello tiene algunas escenas que son bastantes buenas, al igual que otras, para mi, son totalmente prescindibles.
5.5/10
        
            