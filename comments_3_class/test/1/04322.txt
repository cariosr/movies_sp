
                      El Danzón
La película El Danzón tiene elementos de suspenso, ya que alguien se desaparece. Julia, el personaje principal, tiene que arriesgar mucho para poder encontrar a esa persona. En el camino a esta búsqueda Julia experimenta cosas nuevas que le ayudan a encontrarse a sí misma. 

Esta película titulada ¨el Danzón¨ fue dirigida por María Novaro y fue estrenada en septiembre 25, 1992. El personaje clave es Julia, ya que es la que va en la búsqueda y la película solo se enfoca en ella. Tiene más personajes pero no son tan importantes como Julia. La directora tiene más trabajos que ha hecho, uno de ellos es la película ¨Tesoros¨. 

Julia, la actriz principal se queda sin su pareja de baile, Carmelo. Va a Veracruz en búsqueda de él.   En el camino conoce a gente que la quiere ayudar. Como su amiga susy le ayuda a encontrar a Carmelo. Dona ti es la encargada del hotel donde se queda Julia durante la búsqueda de Carmelo. Carmelo es el que está perdido. 

Si les gusta el tipo de baile que es el Danzón, no les recomiendo esta película. Casi no bailan en la película. Se la pasan solamente hablando y hay muchas escenas aburridas. Le falta enseñar más sobre el baile. El título se llama el danzón y casi ni enseñan el baile. 
Kevin Fernandez
        
            