
                      LA GUERRA DEL PLANETA DE LOS SIMIOS...Y LA GUERRA SIN LLEGAR

Pues sí, si la segunda entrega de este despropósito de saga/precuela/refrito me resultó ya no mala, sino insultante, ¡qué os voy a contar de ésta, que vociferan por sus feudos propagandísticos que es la conclusión final!
¡¿Qué coño final de qué?¡ si no pasa nada de nada en 142 minutos!
Que sí, que ya sé que voy a contracorriente, que a la crítica (ese ente incorpóreo) le ha encantado...o se ha vendido. Quizás sí que soy más beodo de lo que pensaba o quizás me equivoqué de sala y vi otra cosa, que todo puede ser.
Os voy a enumerar lo que a mi parecer tenemos y no tenemos en esta "monada":
- Tenemos a un mono-mesías, (César), totalmente deprimido, deprimente y sobreactuado, que se pasa más de media película interpretando "La Dama de Las Camelias" como si fuese la mismísima Nuria Espert. El animalico no sabe muy bien si va o si viene, porque estoy seguro que aún no tenía un guión definido cuando estaba acabando de rodar la película.

- Tenemos ese guión del que hablo, pero que aún no sé si llegó a escribirse, o que se iba improvisando según el ánimo que tuviera ese día el director, o el productor, o la mona chita...o el chico de los recados. Pura incongruencia. Más flecos y agujeros que un chaleco de ganchillo de una comuna hippie. Podría jurar que Mark Bomback lo firmó bajo amenaza de muerte, porque si no, no lo entiendo.

- Tenemos a GOLLUM en versión mono, que no sé qué narices pinta salvo aportar una nota cómica, que se queda más bien en mera payasada.

- Tenemos otros simios que tampoco sabemos qué narices pintan. Porque, no, no hay empatía ninguna con los personajes. Importa entre nada y un carajo que vayan muriendo uno a uno o de ocho en ocho, eso sí, con mucho aspaviento y muuuuucho tiempo para morirse por ver si consiguen arrancarnos una lagrimilla o algo.

- Tenemos, eso sí, otro virus, que ya sabemos que los virus en cine sacan de muchos aprietos, y en este caso les resuelve la papeleta con tanta lógica como una orden de ejecución firmada por Hitler..

- Tenemos a Woody Harrelson esforzándose, como si estuviera estreñido, en parecer malo malísimo, (como la tiña), y en inventarse escenas "grandiosas", en plan Peter Ustinov quemando roma en Quo Vadis...pero sin serlo. Y luego vuelve a intentarlo. Y luego otra vez, pero nada oye.

- Tenemos una fotografía monocroma y gris, que no oscura, que no aporta nada salvo tedio y sopor. 

- Tenemos una partitura molesta y machacona, que estorba más que acompañar, de tan reiterativa y tan evidente. Se empeña en subrayar cada movimiento y en acompañar sentimientos que no acaban nunca de aflorar. Se crece en varios climax de melodrama, de esos de lágrima y tal, sólo que esos melodramas resutan tan, tan, tan evidentes y artificiales que casi dan un poco de risa.

- Tenemos un montaje que recuerda a esos grandiosos Spaguetti Westerns: primerísimos planos, tempos pausados, movimientos lentos e interminables, duelos intuidos en enfrentamientos de miradas.Y también por la venganza como motor de la historia y motivación del protagonista, justiciero y solitario. Tenerlo sí que lo tenemos, pero funcionar...me parece que no funciona.

- Tenemos también una referencia Coppola y su "Apocalypse Now" (ape-pocalypse now), y hay quien dice que toda la película es un tributo a su cine. Pobre Coppola.

Resulta todo tan artificicioso, tan poco (o nada) creíble, tan recargado, tan pomposo, tan falto de ritmo, de coherencia, de continuidad...y no sigo, que me enciendo.

- Lo que no tenemos es la tan cacareada guerra, que nunca llega, o más bien que no existe; Se sacan de la manga una absurda batalla final, pésimamente filmada. 
Y no, la imagen de esa batalla, que aparece en uno de los carteles promocionales de la película, no es más que eso, un reclamo publicitario y que, a no ser que me durmiera (que os aseguro que no), no aparece en pantalla en ningún momento. 

La batalla es para detenerse en ella: os haría un espoiler pero de verdad, da tanta pena como se pasan por el forro cualquier mínima muestra de "verisimilitud" (eso que explicaba en la crítica de Wonder Woman) que empiezas a plantearte ponerte el gorro de "tonto de capirote". 

Para mayor pitorreo del respetable, resuelven la "no batalla" de un plumazo tan gratuito, que te deja con la boca abierta de estupor; estupor del chungo, del que se te queda cuando te das cuenta de que sí, que es el momento de ponerte el tal gorro. 
- Vamos, que lo que no tenemos es vergüenza. Que nada viene a cuento de nada, que no hay nada que contar y que las dos horas y mucho de monotonía, de monocromía, y de monodrama, son sólo un pretexto para dejarnos "presentados" a Cornelius y a Nova, (que quienes hayáis visto EL PLANETA DE LOS SIMIOS ya sabéis quiénes son) y que desgraciadamente hacen temer un remake/refrito de la susodicha, que de parecerse en algo a ésto que he visto hoy, me hará sangrar los ojos y llorar lágrimas de petas zetas.
        
            