
                      No veáis el tráiler si la queréis ver, porque parece que sea una historia realmente distinta y el resultado final decepciona.

Passengers es una película con una imagen visual bastante buena e impecable y unos actores de diez, pero la trama fulmina y envenena toda la cinta. Empieza muy bien con una trama de ciencia ficción algo diferente pero a mitad de la película llega siendo una película romántica, como cualquier otra y de esa manera acaba con cualquier halo de luz de la trama del inicio.
        
            