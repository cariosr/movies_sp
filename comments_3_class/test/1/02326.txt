
                      De primero, entrantes de “Hannibal“. De segundo, delicatessens de telefilme con guarnición de “La casa de la pradera“. Y de postre, sorbete de dormidina. Con estos ingredientes tenemos la receta de la culinaria y caníbal película “Somos lo que somos“.

Desde los primeros minutos donde descubrimos a una familia totalmente disfuncional y desestabilizada, ya nos adelantan que lo vamos a ver no piensa estar mucho a la altura de sus ingredientes. Pues el conjunto en si, con una escenografía bastante lograda y con una trama que promete, lo tira todo por la borda del aburrimiento y la somnolencia.

Y es que, si hay algo que odiamos en el cine (aparte de a “50 sombras de Grey“) es que tengan en bandeja algo que puedan convertir en una gran película, y no lo hagan. Una familia con unos hábitos muy poco saludables y un guión pobre, lamentable y cutre. A eso se limitan. Solamente nos quedamos con un buen sabor de boca por una escena final desgarradora e intensa que apenas dura 6 minutos. El resto, una sucesión de planos sin sentido que deja con hambre de más. En todos sus amplios (y malos) sentidos.

Lo mejor: las hijas protagonistas con su destacable interpretación de una vida robada.
Lo peor: el resto del conjunto.
        
            