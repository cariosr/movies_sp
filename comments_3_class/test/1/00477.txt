
                      Quizás deberíamos mirarlo desde un punto de vista diferente. ¿Es peor que "Halloween III", anteriormente famosa por ser la peor secuela en la historia del horror, con la posible excepción de "Texas Chainsaw Massacre 3"?

Sí, me temo que lo es. Al menos esos dos fracasos intentaban abrir nuevos caminos. A pesar de tener a Jamie Lee Curtis en una secuencia de apertura como Laurie Strode, encarcelada en un manicomio donde finge ser catatónica y planea matar al mismo Michael Myers que creíamos que había matado al final de "H20", esta película parece ser realizado en algún tipo de distorsión psicológica del tiempo, como si todas las películas "Scream" que se burlaban de las convenciones de "Halloween" no existieran.

Sí, una vez más los sacan a trote. Las parejas que tienen relaciones sexuales deben morir. La más virginal y práctica de todas las mujeres debe ser la única sobreviviente. (Engañan un poco a eso.) Y ni siquiera se molestan en averiguar dónde ha estado Michael Myers o cómo se soltó de nuevo, aunque aparentemente esperan que creamos que ha estado viviendo en el sótano secreto de La casa de matanza original en Haddonfield, Illinois, todos estos años. Whoa

Esta es la película que hace la pregunta: "¿Qué pasaría si Busta Rhymes y Tyra Banks tuvieran una idea para un reality show en Internet en el que seis personas son elegidas para vivir en la casa donde ocurrieron los asesinatos de Michael Myers?" (Caramba, pregúntate qué pasa).

Al principio, crees que podría tratarse de una especie de videollamada de cámara movible con cámara múltiple súper genial, como "Blair Witch", pero no pueden decidir si usar las levas de lápiz labial o perderlas. En cualquier caso, no proporcionan nuevas perspectivas ni "muertes" visualmente impresionantes. En cambio, tenemos nueve personas vagando por una casa que no es muy interesante, y una de ellas es el Michael Myers con cara de tiza.
        
            