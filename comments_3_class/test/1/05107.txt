
                      Este es el tipo de película que te deja con la impresión de que se ha puesto más atención en frases y servicios para fanáticos que en una trama convincente, caracterizaciones reflexivas o coreografías de acción imaginativa.
        
            