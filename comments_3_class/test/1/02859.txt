
                      Hay tantas cosas equivocadas acerca de The Dark Tower, la versión cinematográfica achaparrada de la maravillosamente densa y deslumbrante serie de ocho novelas de Stephen King, que es difícil saber por dónde empezar un cálculo crítico. El quid del problema es que la obra maestra del autor superventas merece un tratamiento de miniserie abierta, similar a lo que HBO ha hecho con Game of Thrones o el tratamiento de Peter Jackson de la magnífica trilogía cinematográfica Lord of the Rings.

En cambio, obtenemos una película de 95 minutos que juega como una mezcla de temas míticos de King sin tejido conectivo. Es como si el director Nikolaj Arcel y los coguionistas Akiva Goldsman, Jeff Pinker y Anders Thomas Jensen sacaran una novela corta "The Gunslinger" y metieran todo lo que pudieran recordar en la gran pantalla. Llámalo The Dark Tower for Dummies.
        
            