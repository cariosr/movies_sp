
                      Mis dudas se despejaron y pude ver como si a al señor Diesel le sacas de coches y tiros no da la talla. Aburrida desde el minuto 10, la película carece de lo que su nombre indica: brujas. Escenas de relleno y sinsentido, un Elijah Wood ausente, un Vin Diesel deficiente y una Rosie Leslie cuyo papel se queda bajo mínimos comparado con otros trabajos.

Final predecible y alargadísimo a base de flashbacks que nada nos enseñan. Lo peor de todo, que parece que habrá segunda parte.
        
            