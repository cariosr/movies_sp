
                      Insulto a la inteligencia cultural.

¿Quién ha caído más bajo de los dos?, ¿cuál de ellos necesita antes la jubilación anticipada?, ambos fueron grandes en su tiempo, época dorada de grato recuerdo, ahora perdidos en una patética cinta de manuscrito ridículo que pretende ofrecer, entre tanta banalidad y tontería, algo sólido y decente que sacar a la luz.
Hablo de Bruce Willis, como relleno breve, de Bill Murray, como caza talentos musical que acapara todas las escenas y de la hermosa Kate Hudson, que únicamente luce cara y cuerpo, de acompañamiento ocasional para endulzar la imagen, dado el derrotismo hablado que se percibe en cada intercambio de frases.
Porque no se que es peor, el guión en sí o verlos a todos ellos, recitar cada estafa y nulidad de palabra, en este montaje absurdo del cuento soñado de cenicienta de dejar sus trapos y cantarle al mundo con pasión y entusiasmo.
Sin refuerzos, con enormes vacíos, sin pilar de base, con grietas por todas partes, sin gracia ni tragedia, con mucho dolor por la ofensiva parodia o lo que quiera que se pretenda en este artístico show, donde cojea la estrella en la atractiva arena del bello desierto, donde el resto le van a la zaga en esa superficialidad, sin talento ni ingenio, que pretende un drama válido de valentía, superación y encuentro de la honestidad y decencia de cada uno arropado con comedia ligera y amena, pero únicamente resulta una bufonada sin gancho, falible y equivoca,  que engaña y agravia a cada paso que avanza en esa consecución del premio que se esconde tras el álamo Afganistan.
Humanidad y colaboración, buena voluntad y unión, superación de las diferencias y posibilidad de aceptación del cambio, cordialidad bonachona adornada con humor jocoso, de hilaridad y gusto puesto en duda, que no es divertido, ni tenso, ni lloroso, y que con una fotografía comarcal fantástica y una música deliciosa no emite más que frivolidad chistosa, de resultado vano, en su intento de hermandad y negociación de las nuevas reglas y condiciones.
“¿Qué pasó con la sonrisa que tenías? Sonrisa de ganador, la llamabas”, que como cuando Olé Olé fue a animar a las tropas, se trató de mucho ruido y pocas, escasas nueces, únicamente meneos de circense trivial para amenizar cansadamente haciendo gansadas. Otra cosa fue Marylin en sus tiempos, ya sea por glamour, espectáculo o visión de conjunto; aquí, la categoría de tributo a la primera mujer cantante en televisión cae en saco hecho polvo, sin rédito, compromiso ni atención, únicamente pasar el rato con memeces e idioteces y, esporádicamente aportar algo decente por error.
“El tren de la paz” pasea, coge velocidad, toma el país, sigue adelante pero, es un cuenta-mañanas sin agudeza, interés o inteligencia, sólo beatitud final por quien se rinde homenaje pero, para entonces, ya te has tragado toda la payasada.
Rock the Kasbah, si al menos hubiese sido alegre, bailable y ¡entretenido! Insustancial en su valoración final.

Lo mejor; el motivo homenaje en el que se honra a la mencionada.
Lo peor; todo lo demás empezando por la base de un manuscrito banal, roto y de gran desatino.
Nota 4,4
        
            