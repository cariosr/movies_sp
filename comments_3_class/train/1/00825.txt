
                      Me parece realmente deprimente, que, hoy en día, las niñ@s piensen que esta película va sobre una historia de amor, cuando en realidad lo que han podido leer o ver en la película es una historia ciertamente triste y enervante, en la que se ve como el protagonista masculino es excusado de su falta de autocontrol y manipulación psicológica, justificándolo con un alcoholismo, y haciendo que situaciones como estas parezcan normales.
Lo que pretendo comunicar con estas previas palabras, es que situaciones como las que se observan en el libro/ película no deben ser toleradas por nadie, ni vistas como una muestra de amor pasional, ya, que alguien que te quiere, jamás te haría las cosas que él le hace a ella.
        
            