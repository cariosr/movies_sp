
                      Está claro que todos los involucrados ponen mucho corazón y esfuerzo en todo lo que vemos, pero también está claro que el corazón y el esfuerzo solo te llevarán tan lejos cuando las cuerdas del bolso se hayan tensado.
        
            