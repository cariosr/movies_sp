
                      Otra americanada de las que nos llegan a montones cada mes, intentando rebajar nuestro nivel neuronal, y muchas veces consiguiéndolo. 
Tiene su toque divertido pero no deja de resultar tan previsible como otras de su misma procedencia, a pesar de que nos la han intentado vender como algo diferente. 
Aunque ver una película no tiene porque ser un ejercicio de inteligencia ni necesariamente el tema debe de ser trascendental, al menos debería despertar algún sentimiento que no sea la risa facilona.
Ya desde el título podemos percibir lo que nos espera, aún así estos americanos son inagotables al desaliento y seguirán intentando colárnosla una y otra vez. Y, mientras pasemos por caja, les dará igual que sus películas nos gusten o no.
        
            