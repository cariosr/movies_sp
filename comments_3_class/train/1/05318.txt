
                      Y esta peli esta seleccionada para los Goya? Asi va el cine español!. 
El principal defecto de la pelicula es el guión. Un desastre. No se sabe muy bien cuál es el conflicto principal y sin conflicto no hay drama. Â¿Quiere Lope triunfar en el teatro? No parece que tenga grandes problemas para ello. Â¿Es Lope un tipo acomplejado por su bajo estatus social que pretende aparentar lo que no es? Ése parece que va a ser el tema central al principio, cuando entierra a su madre por todo lo alto, y se pone un traje de un noble, pero luego parece que a los guionistas se les olvida por completo ese tema. Â¿Ama al personaje de Pilar López de Ayala pero no puede irse con ella porque no pertenece a su clase social y está casada? Pues no parece que el tema le desespere. Es más, en una secuencia en la que Lope lloriquea un poco, ella le dice que su padre va por fin a estrenar una obra suya, y Lope se queda tan contento. Sin un objetivo para el protagonista la película no va a ningún lado y ofrece la sensación de ser demasiado premiosa.

La pobre Watling interpreta las secuencias más ridículas de la película; en una no puede dar de comer a un enfermo Â¡porque le tiembla el pulso en presencia de Lope! La otra no tiene desperdicio:

-Te seguiré a dónde sea, Lope, si te embarcas, me embarcaré contigo.

-No lo entiendes, las mujeres que se embarcan sólo son las putas.

-Pues seré tu puta -la frase es tan ridícula (parece de peli porno) que el pobre montador ha tenido que poner un plano en el que la Watling está muriéndose de la risa, o sea que es el único material que tenía o los otros planos eran aún peores.

Para ejemplo de guión cutre, la secuencia en la que Lope improvisa sonetos, copiada -también- de Cyrano de Bergerac y su "al finalizar os hiero", pero en fin, las comparaciones son odiosas:

-Un soneto me manda hacer Violante...

-Espera, espera -le diría un guionista al otro-. Queda muy raro eso aquí. En esta película no hay nadie que tenga el apodo "
        
            