
                      Casi nunca pongo una mala nota, ya que muchas veces trato de ser objetivo con mis calificaciones cinematográficas; a una persona le puede encantar y a otra no, el mundo está lleno de gustos y preferencias. 

Sin embargo, tengo una excepción. Si una película se vende con una etiqueta de, por ejemplo, "terror", tú pagas por que te asusten, para entretenerte a base del miedo, no para pasar el rato "sintiendo" otra emoción. Esto ocurre con esta película, y lo que alguna gente, en mi opinión, no entendió a la hora de criticar el film, es exactamente eso. "Pues está entretenida" dicen algunos, sí, puede ser verdad, pero Poltergeist se vende al publico como "De terror" no "Comedia-terror"... y como no asusta, ergo merece una mala nota. 

->Lo bueno: -El comienzo es bueno; empieza los primeros 15 minutos sacándote uno que otro susto, no lo niego.
-Los efectos especiales son buenos.

->Lo malo: -Todo lo demás; llega el momento en el cual no les crees nada a los personajes. Es cómo si a media película el presupuesto se hubiera terminado y alguien le hubiera comentado a los actores: "Chavos, ya casi no tenemos plata con que pagarles", y ellos hubieran respondido "Ha Ok no hay problema, mira si quieres actuamos mal y cobramos menos". Y es que 