
                      Colossal (2016).
El desgraciado realizador responsable de la fallida Open Windows, que tras ganar en su momento un Oscar por un corto de ficción alguno lo creerá digno de entrar en Hollywood por la puerta grande, nos trae este experimento surrealista de 2016 en plan Cómo Ser John Malkovich venido a menos, como si del mismísimo Spike Jonze absolutamente drogado se tratara.

Para la sipnosis ruego pinchéis en el enlace de abajo porque no merece la pena extenderse aquí, pero Anne Hathaway, quien hace labores de producción ejecutiva en la cinta, se presta a esta extraña historia de la manera más tonta.

Imagino que Nacho Vigalondo, se aburre muy mucho cuando escribe y dirige estas cosas... porque no encuentro otra razón para ello...

Una vez más, la opinión de la crítica (buena en general) es inversamente proporcional a la calidad de la obra (malita, malita)... y para más Inri, se alarga tremendamente.

NO recomendable (ni en plataformas o tv siquiera).

Raúl Cabral.
        
            