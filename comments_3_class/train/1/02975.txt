
                      Desde luego no es que tuviera buenas expectativas en esta cinta, la catalogaría de palomitera, pero después de verla sinceramente creo que entra mas en la definición de "Panfleto publicitario" que otra cosa.

Dejando a un lado los 150 millones de dolares que se han dejado para limpiar un poco su imagen (ya sabéis, lo de ser los mayores genocidas de la historia... que queda feo...), desde el minuto uno ya hueles el tufo a americanada cansina, llena de estereotipos, chistes para críos de 8 años y unos personajes tan vacíos como previsibles.

A destacar quizás, sus efectos especiales, (los pocos que tiene...) pero desde luego no recomiendo esta cinta comercial post-publicitaria, parece un anuncio de dos horas del por que los "Yankis" son tan buenos y el resto tan malos, lo mejor de todo es que ellos mismos se burlan de la imagen que dan al exterior, pero todo se estropea cuando trata de forma ambigua y casi chulesca el echo de ser el país mas corrupto del mundo.

Resumen: Un montón de basura publicitaria, la niña da ganas de que se pierda... pero de verdad y el padre.... en fin, otro tarado patriótico que no tiene ni la egb.
        
            