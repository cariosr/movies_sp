
                      Esta película me parece un insulto y una ofensa para aquellos que creemos en Jesús y tenemos un largo recorrido de fe.
La cena me parece surrealista, la oración en el huerto también, no hay juicio y sale en alguna escena como azotan a Jesús y le golpean, el camino a la cruz, la cruz la lleva agarrada y no con una madera atada a los brazos. Los clavos son en la mano y no en la muñeca, no hay traspaso en el costado.  
No tiene ni pies ni cabeza, no hay por donde cogerla, no hay hilo conductor y hay escenas que no salen en la biblia y hay escenas que se cortan.
Si el director tuviera un poquito de dignidad retiraría la película de la cartelera y de los cines
        
            