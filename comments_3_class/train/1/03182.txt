
                      Menuda película... eso sí con unos actorazos increíbles; lucimiento excesivo del protagonista,, que invade la cámara... falta de encuadre del papel de Carmen Machi como mujer con bebé  y niño pequeño y tópicos fáciles y creíbles pero no verosímiles en la personalidad que representa. Todo muy de telefilm... NO NO Y NO. Sí lo sabemos hacer mejor... y Carmen Machi no tiene que garantizar un éxito cinematográfico.
        
            