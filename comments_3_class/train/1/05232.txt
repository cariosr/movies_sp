
                      Cerca del comienzo de "El mecánico", un hombre se sumerge en su piscina cubierta y comienza a nadar. Guardias armados rodean la casa del hombre. Incluso hay personas que trabajan cerca de la piscina que ven al hombre nadar. No importa. Después de detectar algo bajo el agua, el nadador es atacado por un asesino contratado que se había infiltrado en la fortaleza del hombre. El asesino claramente sabe cómo hacer el trabajo. Ha tenido mucha práctica aprendiendo las habilidades que lo han convertido en un "mecánico" solicitado.
        
            