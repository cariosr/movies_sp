
                      Habiendo visto todas las películas de la saga y la serie animada. Puedo decir sin miedo a equivocarme que es la peor película que he visto en mi vida. Inconexa, repetitiva, interminable, con mezcla de historias sin sentido, personajes planos que no aportan nada, diálogos pobres y muy poco protagonismo de los robots que son las verdaderas estrellas de la franquicia. Una pena. Par mi fin absoluto de la saga. Ni una más.
        
            