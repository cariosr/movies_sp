
                      Siento discrepar, pero para mi ha sido toda una decepción. Es una pena desaprovechar tantísimo talento en actores, maquillaje, coreografias y decorado, que esta todo de 10 para una historia pobre, carente de la frescura, dinamismo y alegría de la primera, mal hilada y falta de imaginación, con un guión pobre, que además añade fallos y diacrepancias con la primera. Un melodrama más cercano a una telenovela con recursos emocionales para hacer llorar desde que empiwza hasta que acaba la película, con algún momento alegre para intentar emular a la primera.
Una pena, por que si la primera fue de 11 sobre 10, esta no llega al 1.
        
            