
                      El primer "Daddy's Home" se filmó en Nueva Orleans, y la brillante secuela titulada "Daddy's Home 2" se filmó en Massachusetts, pero estos personajes unidimensionales, profundamente neuróticos y, en algunos casos, gravemente perturbados también podrían estar en Asgard. Toda su conexión con cualquier cosa que se parezca al mundo en que vivimos.

Sí. Sé que esto es una farsa de slapstick, con aspiraciones a seguir en la amplia tradición de comedia de las "Vacaciones de Navidad de National Lampoon" o incluso el primer "Solo en casa".
        
            