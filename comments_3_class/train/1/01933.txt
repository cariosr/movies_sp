
                      La trama te deja asqueado.

Tener a nuestra propia sombra llena de envidia por cómo vivimos, materializada en una figura exactamente igual a uno en un subsótano del sótano (cuartos de máquinas) de un parque de atracciones americano, creo que es lo más absurdo que me podido encontrar en una película. 

De paso, estas sombras deciden salir a la superficie un día de celebración nacional, dejando atrás dormitorios al estilo militar,  pasillos y salones llenos de conejos de los cuales se alimentaban.

Todo este despropósito me hace concluir que ésta película puede estar fácilmente en un top 5 de las peores que cualquier persona haya podido presenciar.

Al menos cuenta con la buena actuación - doble - de Lupita, maquillando el desastre de guión, pero me deja dudas de su criterio para seleccionar proyectos cinematográficos.

A pasar página.
        
            