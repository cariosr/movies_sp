
                      Mala a la vez que cara, ridícula, pretensiosa a la vez que simplona, previsible, larga, horrorosa. Los rusos son malos, su gobierno es despótico y utiliza técnicas delictivas, las piernas de las bailarinas se rompen en el escenario con un "crunch" y de aquí todo el mal argumento de una pobre chica que tiene que ser muy dura para cuidar a su madre enferma. "La guerra fría no ha terminado", qué horror por dios. En rusia los malos siguen fumando en espacios cerrados, los rusos se siguen llamando Dimitri, y por supuesto, los tíos Vánia.
        
            