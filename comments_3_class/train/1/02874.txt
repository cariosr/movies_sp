
                      Muchos actores, directores y guionistas se avergüenzan al cabo de los años de algunos de sus papeles y trabajos. Rooney Mara se tapa los oídos cada vez que la recuerdan su papel en “Pesadilla en Elm Street: el origen“. George Clooney reniega de “Batman y Robin“. Sam Raimi admitió haberla cagado con “Spiderman 3“. Joss Wheedon no quiere ni oír hablar del guión de “Alien: Resurrection“. Pues bien, creo que algo así le va a pasar dentro de un tiempo a Maria Bello por interpretar “Demonic” y a James Wan por producirla.

Cinco cazadores de fantasmas amateurs investigan una casa en busca de pruebas de los asesinatos que sucedieron allí años atrás. El hijo de la única superviviente de aquella masacre, relatará a la policial todo que allí les aconteció a sus amigos y a él.

Una película absolutamente sin sentido, donde se mezclan las escenas mockumentary que graban los jóvenes, con la investigación policial la noche siguiente. Sustos más que forzados y sin ningún mal rollo, se entremezclan con escenas absurdas de infrarrojos que no aportan nada. Una historia tan aburrida como el propio escenario que no genera el más mínimo suspense.

Algo tan grande como llevar el nombre de James Wan produciendo la cinta, junto con la interpretación de la gran Maria Bello, se ven tirados por la borda. Entonces es cuando te das cuenta de que hasta las mayores estrellas de Hollywood también se equivocan en sus elecciones.

Lo mejor: María Bello en un papel que ella misma querrá olvidar.
Lo mejor: sentir tu hype disminuir desde el segundo uno.
        
            