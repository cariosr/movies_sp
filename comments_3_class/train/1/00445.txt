
                      Vamos a ver. Si es película de autor, novela negra, ochentera y todo eso pues los entendidos cinéfilos dirán que perfecta, de culto... ¡Pero es que el trailer y los anuncios la 
presentan como de acción! Y acción cero. Dos derrapes en toda la peli y media docena de escenas explícita e innecesariamente sangrientas para un protagonista que solo sonríe (no, no habla y no actúa) y una película aburrida y lenta. Lo siento pero la campaña publicitaria lleva a engaño y cabreo del espectador. Eso si, si buscas cine negro, lento y acompasado.. Toda tuya.
        
            