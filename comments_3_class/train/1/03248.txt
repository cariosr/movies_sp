
                      Solo un tonto diría que Kevin Hart y Tiffany Haddish no son graciosos. Pero solo un imbécil diría que su nueva comedia, Night School, no es el peor tipo de idiotas flojas, carentes de risa y de pago. ¿Qué pasa con las estrellas que piensan que su responsabilidad para con el público se detiene en aparecer?

El director Malcolm D. Lee, quien mostró a Haddish en todo su esplendor en Girls Trip, la deja en la estacada con esta pelota de béisbol PG-13 que "no es tan desagradable como debe ser". ¿Nadie ha visto lo que hacen estas dos tiras cómicas en el escenario? Se necesitaron seis escritores para crear una trama y un diálogo que saben que las dos estrellas podrían haber improvisado mejor en el acto. Espera, el propio Hart es uno de los seis escritores ... así que será mejor que lo retiremos.
        
            