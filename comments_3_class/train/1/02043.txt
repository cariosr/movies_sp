
                      Película basada en el título homónimo realizado en 1977 por Wes Craven, y  que respeta y “copia” totalmente su argumento
Sin duda esta adaptación sigue poseyendo la fealdad visual de la película anterior.
Todo podría resumirse en; mala película que hace en cierto modo sufrir al espectador y con escenas que lo violentan.
Si se pone uno en el lugar de los personajes… duele y no es una gran sensación.
No posee brillantes interpretaciones ni escenarios; puntualizar que si su música si es buena para una producción de este genero.
Adaptación mediocre y de poca calidad si se compara a su antecesora.
Es una historia que no engancha y en ocasiones incomoda.
        
            