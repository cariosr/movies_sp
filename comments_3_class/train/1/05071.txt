
                      Saw V es una combinación terrible: espeluznante y tediosa. Vamos a llamarlo malditamente aburrido.

La última entrega de la franquicia de pornografía torturada es lenta, pesada e incluso intrincada.

Sentarse a través de él es una tortura del peor tipo cinematográfico. Ninguna cantidad de distracción en forma de rociamiento de sangre o miembros amputados puede disminuir el trauma de fuerza contundente de todas las actuaciones terribles.

Es casi como si estuviera escrito en los contratos para actuar lo peor posible, para no eclipsar la sangre o las cabezas decapitadas.

Scott Patterson, tan agradable en TV Gilmore Girls, desperdicia su talento volviendo como agente del FBI Strahm. Costas Mandylor interpreta al detective Hoffman de madera. Los dos simplemente no podrían ser menos interesantes.

Y el cerebro frío y calculador Jigsaw (Tobin Bell) dispensa su sabiduría helada mezclada con clichés. Tal vez se haya ablandado un poco, permitiendo que "matar sea desagradable. Si un sujeto sobrevive a mi método, él o ella se rehabilita instantáneamente".

Es una película de terror, ¿por qué la exposición debe comunicarse de forma perezosa a través de primeros planos de archivos, recortes de periódicos y lecturas en teléfonos celulares? Presumiblemente, esto es para ayudar a los que se perdieron las últimas cuatro películas a unir las piezas de los asesinatos de Jigsaw. No importa. Todavía no será bueno.
        
            