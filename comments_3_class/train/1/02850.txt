
                      De Plantilla. Sin dejar de reconocer el esfuerzo interpretativo de Melissa McCarthy, lo único que se puede decir de esta película es que trata noblemente de saltar los estereotipos culturales que condenan a las personas gordas y feas para tocar el corazón del espectador, sin conseguirlo. Por lo demás, Bateman, su familia y su entorno disney es un cliché insustancial visto un millón de veces. Y de forma notable, la guapísima Amanda Peet está aquí poco favorecida.
        
            