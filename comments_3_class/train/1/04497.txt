
                      A pesar de tener un buen arranque, el guion va despedazándose poco a poco junto a sus personajes, bien porque Justin Kurzel no dirige bien a su reconocido elenco -entre ellos Fassbender y Cotillard- o porque la propia historia no merece la pena. Si hay algo que se puede rescatar de tan innecesaria como comercial película es su ambientación, dotada de una atmósfera y clima notables y un logrado trasfondo medieval. Amantes del videojuego: no perdáis el tiempo.
        
            