
                      Una dosis de somnolencia dada a cuenta gotas.

Es una de las mejores filme del director.

Ha arriesgado en querer tener la intriga del espectador desde el primer momento, pero ha caído en la absoluta esencia del aburrimiento.
El espectador que en un comienzo empieza a interesarse por los sucesos, llega a ser una dosis de inaguantable lentitud a cual el espectador pierde el interés y se deja llevar a un final que no lleva a ninguna conclusión. 
El único interés que demuestra es poder dejar la butaca.
        
            