
                      Aunque mantiene la tensión, es bastante previsible. Daniel Craig está bien en su doble papel. Las protagonistas femeninas, ambas espléndidas, tienen papeles limitados. La película incluye todos los ingredientes, casa encantada, familia feliz, locura, relaciones sobrenaturales y un criminal por descubrir, pero todo sobre un trasfondo banal que te deja la sensación de no saber qué querían contarte.
        
            