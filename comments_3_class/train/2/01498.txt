
                      ¡Saludos Gente!

Acabo de ver la película y me siento como la madre  de Mason cuando él se marcha a la universidad. "Simplemente esperaba mas."

Creo que arma principal de la película sin duda alguna es el proceso de filme extenso por tantos años. ¿ Y me pregunto si realmente valió la pena? 

En ningún momento los personajes se comprometen, incluso mismo Mason, (Un muchacho en crecimiento, sumamente introvertido, para mi sinceramente pasa desapercibido)  Rodeado de secundarios que al final nadie  toma protagonismo real de la película.   Sin rumbo fijo, sin objetividad, Tocando  temas  de manera ligera y exprés, como Políticos, guerra, el 911, discriminación, simplemente como el titulo lo menciona: "Momentos de la vida"      Hay escenas que copiaron de Toy Story 3.

¿Y qué nos deja ver la película?  Dura casi 3 horas, incluso por momentos aburrida,  Solo vemos  el lapso de su infancia con una historia de amor juvenil. Sin frases memorables, escenas emotivas. Disculpen pero en lo personal esperaba más. No merece el Oscar.
        
            