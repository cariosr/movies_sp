
                      El Rey León (2019).
"Hakuna Matata. Vive y sé feliz."
Disney nos trae su último filme en imagen real basado íntegramente en su célebre largometraje de animación, como ya hiciera con la reciente "Aladdin". En esta ocasión es Jon Favreau quien dirige la cinta de manera similar a como lo hiciera con "El Libro de la Selva”, esto es, creando una auténtica sabana con todos sus animales incluidos. 

La película, a la cual la crítica especializada pone por los suelos -razón de más para verla- es un espectáculo visual gracias a su maravillosa fotografía, a los efectos visuales que nos hacen disfrutar de unos animales increíblemente reales, y a una música orquestal compuesta, como ya hiciera con la de animación hace un cuarto de siglo, por el gran Hans Zimmer (una de las canciones postcréditos es de Elton John). 

“-Supongo que hasta los reyes sienten miedo. 
 -Más de lo que te imaginas."
La sinopsis es calcada a su homónima de 1994, solo que es algo más larga, quizá demasiado. De los 85 minutos que duraba aquélla pasa a casi dos horas para contar la misma historia: un rey “campechano” y su hijo “preparado” para sucederle, mientras el hermano del monarca intenta “ocuparse” de éste... Recuerda un poco a cierta familia real originaria de Francia... 😉

En definitiva, una película entretenida y visualmente muy hermosa, aunque tengamos que aguantar algunas canciones dobladas al castellano para que nuestros pequeños no tengan que leer subtítulo alguno.

Raúl Cabral.
https://www.filmaffinity.com/es/film316246.html
        
            