
                      Una vez más queda patente la ignorancia de algunos guionistas de cine que creen que viendo Percy Jackson y los viajes de Hércules pueden documentarse correctamente a la hora de montar un trabajo.
En la eterna lucha entre dioses y hombres, Perseo, mitad dios mitad hombre, incapaz de defender a su familia de Hades, se echa la manta a la cabeza para detener el fin del mundo.
Esta película, cuyo padre es el mismo de Transporter 2, nos brinda efectos especiales bastante chulos aunque poco constantes, el Kraken es una especie de cruce mutante entre Doom y un pulpo, cuando en la mitología nórdica y posteriormente mediterránea es, normalmente un calamar gigante.
Zeus se le ve tan flojo para ser dios de dioses que parece que necesita un actimel y Perseo más o menos logra una caracterización creíble.
Si no atendemos a su "veracidad histórica", es un producto para pasar una tarde amena y divertida sin muchas pretensiones, pero creo firmemente que podrían haber hecho algo mucho mejor, el nacimiento de Perseo, infancia, etc... Algunos personajes les ha faltado un poco de argumento, están en plan Pasaba por aquí y creo que tenían una historia muy buena que se ha quedado por el camino.
Es agradable de ver, no hay mucha sangre, las peleas están bien conseguidas y en general, no es un producto de tirar por la espalda al olvido, pero definitivamente le falta chicha.
Agradable, amena, mantita y bowl de palomitas.
        
            