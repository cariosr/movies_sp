
                      El cine tiene un verdadero filón en el método de grabación found footage. No solo lo esta explotando (si no lo ha hecho ya) en el cine de terror. También les vale para hacer cintas de bajo presupuesto con cualquier premisa para que rompa en taquilla. Aunque “Project Almanac” fue creada para eso, se tuvo que conformar con unos datos bastantes normalitos.

El chico raro del instituto y su grupo de amigos freakys inventan una maquina con la que poder saltar en el tiempo y mejorar sus errores. Pero también lo usaran para beneficio propio y pegarse algunos juergas. Claro está, sin límites que les llevaran a cometer errores que ya no será tan fácil controlar.

Durante toda la película está más que presente un continuo deja vú con dos joyitas adolescentes: “Project X” y “Chronicle“. Bastante más de la última, las tres tienen muchos parecidos: adolescentes de instituto grabando cámara en mano todos sus excesos y sus idas de olla. Michael Bay produce una historia absolutamente desaprovechada (en realidad para aprovecharla bien deberían hacer mínimo una serie de televisión) y que solo se salva por sus efectos especiales. Si en “Chronicle” disfrutábamos al máximo del descubrimiento que hacen sus protagonistas llevándolo a increíbles dimensiones, éstos solo lo usan para contar una facilona historia de amor y sus consecuencias. Una trama algo absurda que podría enganchar desde el principio y exprimir todo su jugo para acabarla con un festín de escenas alocadas. Se lo achacaremos a que Michael Bay estaba por aquel entonces preparando su remake de “Las tortugas ninja” y se reservaba todas sus buena intención para los reptiles mutantes.

Lo mejor: los viajes en el tiempo, por aburridos que resulten.
Lo peor: quedarte con la sensación de que tu la contarías mejor.
        
            