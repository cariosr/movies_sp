
                      Interesante drama con tintes de intriga ambientado en el pasado reciente del País Vasco en el que Flor Torrente está estupenda en el papel protagonista de una niña a la búsqueda de la verdad sobre su padre. desaparecido 30 años antes. La ambientación es buena y la trama consigue interesar bastante, haciendo que el primer largo de Igor Legarreta sea para tener en cuenta a este director de cara al futuro.
        
            