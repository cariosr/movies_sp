
                      A estas alturas, imagino que los seguidores fieles a la filmografía de Woody Allen, ya habrán ido a verla, y los que no les gusta su cine, ya la habrán descartado.

Yo antes de empezar a contar un chiste, siempre digo que es para inteligentes, para conseguir que se ría la gente. Con las películas de Woody Allen, debe ocurrir algo parecido, sean buenas o no, la gente va a verlas y opina que son grandes películas, aunque si las hubiera realizado otro director, igual las calificarían mas bajas.

Con el elenco de actores con que suele contar, es difícil realizar un mal trabajo, pues con buena cuchara bien se come. 

Este largometraje ralla la normalidad, exceptuando algunos gags en los momentos en los que interviene Helena (que así se llama la madre de la protagonista).
        
            