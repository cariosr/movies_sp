
                      Retrato de una chica de más de treinta que aun vive con su padre viudo y que ve como todo cambia cuando éste anuncia que se casa. Protagonizada por Bárbara Lennie, por momentos da la sensación de estar pelín sobreactuada y no resulta tan convincente como en otros trabajos, aunque la historia está bien llevada y esta dramedia (no es un drama pero tampoco comedia, juega ahí en medio) es entretenida y se sostiene bien.
        
            