
                      Una desaparición misteriosa, suspenso, terror por momentos y efectivas escenas de sobresaltos con apariciones bruscas y una música que acompaña. Lo único objetable fue el desenlace, medio agarrado de los pelos que combina situaciones de "Mama" con algo de "Insidious". Cumple su objetivo si querés pegarte unos buenos sustos.
        
            