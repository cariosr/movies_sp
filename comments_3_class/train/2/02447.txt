
                      Basada en un personaje muy popular en Australia, protagonista de los cuentos que Dorothy Wall empezó a ver publicados en 1930, "Blinky Bill el Koala" (2015) traslada al público infantil a los paisajes de las antípodas en una aventura con humor y acción que resulta más lograda en la primera mitad del metraje que en la segunda.
(...)
Lo mejor: es una película entretenida para el público exclusivamente infantil que les trasladará a los paisajes y la fauna de un país tan lejano y peculiar como Australia.
En contra: "Blinky Bill el Koala" no aprovecha la potencialidad de los personajes y conflictos que traza a su inicio, y se queda en algo bastante aséptico.
        
            