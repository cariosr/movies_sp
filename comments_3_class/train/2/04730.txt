
                      Después de ver el trailer me leí todos los libros y la verdad que no se parece en nada a otras sagas juveniles, salvo que sus protagonistas son adolescentes. Habrá que ver si la película es fiel y sigue el entramado correcto, aunque a veces no es posible del todo. Tiene muy buena pinta y espero que triunfe para que saquen las otras dos películas que le seguirían. El futuro en el que esta ambientado será curioso de ver en la gran pantalla.
        
            