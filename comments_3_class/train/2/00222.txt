
                      Es un buen entretenimiento. El escenario es un poco escaso, la historia pasa rápido y no hay mucha intriga, pero cuando se va a ver este tipo de película, ya sabemos qué esperar. El 3D es un poco demasiado pasivo, es una lástima porque precisamente en este tipo de películas es donde esperamos un 3D más activo.
        
            