
                      Suspenso bien logrado. Del estilo "Atracción fatal" pero en este caso una ex esposa, despechada y media psycho que, tras ser dejada, le hace la vida imposible a la nueva prometida de su marido. Momentos de tensión, una gran actuación de Katherine Heigl en su rol de villana y un argumento bien desarrollado y fácil de seguir redondean una buena peli. La recomiendo.
        
            