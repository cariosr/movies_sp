
                      Haneke envuelve al espectador en una primera media hora magistral, creando una atmósfera turbulenta y una repugnancia constante. Ahora bien, la vena provocadora del realizador se pierde en un festival de planos eternos, lentos y ocasionalmente aburridos. Cabe destacar que las interpretaciones son buenas, pero no encuentro en ellas suficiente credibilidad ante una situación supuestamente extrema. La locura del film está ahí, pero no todos se quedarán locos con ella.
        
            