
                      Película de espías del gran Guy Ritchie, es la película más pop del director pero también es la película con más calidad, tiene unas escenas de acción impecables, una estética que enamora, actores muy potentes, fotografía y edición con la marca del gran director. Si te gustan las películas de Guy Ritchie la tienes que ver, no es la mejor, pero si la que tiene una mejor calidad.
        
            