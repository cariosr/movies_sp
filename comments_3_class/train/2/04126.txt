
                      Sí, como decía Miguel Varona, sí recuerda a la magnífica "La Niebla", aunque sólo en su primer "acto"...y siempre salvando las distancias. Aunque también podría ser "La Cosa", o "Alien", o "Los 10 negritos"....y muchas otras

No es una gran película, posiblemente ni siquiera sea buena, pero  tampoco es mala: es irregular, fallida en muchos de sus tramos, brillante a ratos. Todo suena a ya visto, pero tiene el toque y la personalidad que le faltan a muchas otras que como este "Bar" han sido.

Se agradece que Alex de la Iglesia deje fluir su saber hacer como director, que quiera darnos una película nacida de la vocación y no del frío mercantilismo, que alguien en este país se empeñe en hacer cine nacido de las entrañas, salgo o no salga bien.
Se agradecen interpretaciones como la de Blanca Suárez (quizás nuestro "producto" más exportable), Terele Pavez (aquí sí), Carmen Machi (a ratos), incluso Mario Casas...no tanto otras excesivas y chirriantes que no voy a nombrar.
Le falta, para mi gusto, fuerza, decisión y convicción para meternos en ese bar, en ese sótano, en esa cloaca. No, no nos arrastra dentro, seguimos siendo meros espectadores. Pero consigue sobrecogernos cuando se deja llevar por la locura.

Sí, locura es lo que le falta. Se empeña con demasiada insistencia en explicarnos lo que está pasando y pierde la oportunidad de mostrarnos lo que en esencia muestran este tipo de películas deliberadamente claustofóbicas: la naturaleza humana.

Le falta claustofobia para ser claustofóbica.

Y le falta dureza, y crudeza. Nunca llega explotar el potencial de ninguna de las situaciones, ni de las motivaciones. Sólo araña tímidamente la podredumbre de la naturaleza humana, dejando a sus personajes apenas esbozados y sin entidad ni carne suficiente.

Quizás si se hubiera centrado  más explotar el salvajismo de la situación, si hubiera dejado fluir esa rebeldía que la madurez hace que perdamos, ese desprecio por los convencionalismos, habría sido una cinta brillante.

Aún así, con todos sus vaivenes, sus saltos de ritmo, sus momentos de tedio, sus personajes huecos, y todos sus peros, parece que hemos recuperado la confianza en que Alex de la Iglesia sigue ahí, y que aún hay autores en la industria cinematográfica de este país que tienen intención de hacer algo más que meros productos.

Sólo por eso, ya merece la pena este Bar.
        
            