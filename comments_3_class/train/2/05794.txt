
                      Entre las razones por las que aconsejaría este largometraje, la principal y una de las más importantes es porque trata como personas adultas e inteligentes a los niños, la siguiente razón seria porque habla sin sensacionalismo de un tema tan delicado como es la muerte violenta, otra podría ser porque refleja problemas actuales en la enseñanza, como hasta donde está el límite del maestro al alumno y viceversa, también porque no busca el sentimentalismo fácil exceptuando la escena final.
El sentimiento de culpa y el remordimiento nos lo muestra a través de un niño de doce años, y temas como la necesidad de salir de tu país y la pérdida de los seres queridos a través de la figura del profesor.
Quizás al querer abarcar tantas cosas en tan poco tiempo, no se llegue a profundizar en la personalidad y las motivaciones de los personajes. Pues es poco lo que se nos revela de los dos niños protagonistas.
La película cumple su función de distraer la mente durante la hora y media que dura.
        
            