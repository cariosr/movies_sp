
                      Objetivo: Londres (2016).
De nuevo el incombustible Gerard Butler vuelve para resolver la papeleta al presidente de los sagrados EE.UU. tras un multiatentado terrorista en la capital británica, donde varios de los líderes mundiales occidentales son masacrados para alegría de esa parte del tercer mundo esquilmado por Europa y Norteamérica.

Como de costumbre, la propaganda del país más genocida del Globo- junto a la Alemania Nazi- está muy presente, pero indudablemente la acción desmedida, los efectos especiales (aunque algún fallito hay...), los actores y actrices secundarios de lujo (desde Freeman, A. Bassett, Radha Mitchell, Robert Forster a J. Earle Haley, el último Freddy Kruger), y sobre todo nuestros protagonistas Aaron Eckhart y Butler, que lo mismo valen para un roto que para un descosido (llámese comedia, drama o thriller de acción...), hacen que el producto merezca la pena ser consumido.

Recomendable para desconectar, y para ver lo buenísimos que son los guiris y lo malísimos que son los árabes esos que venden armas y matan a occidentales inocentes...

Raúl Cabral.
        
            