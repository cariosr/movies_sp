
                      Una vuelta de tuerca en la búsqueda de los limites de la mente humana concentrados en el personaje principal : maldad, perversión, inocencia, locura, bestialidad; todo en un constante incontrolable metabolismo mental. 

La película, sin ser ni mucho menos perfecta, consigue entretener, manteniendo el suspense hasta el final. Y dejo una par de preguntas para cuando se salgáis del cine ¿Quien os ha parecido finalmente el personaje mas vil?¿Y quien el que sufre más?
        
            