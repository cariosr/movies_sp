
                      Magnifica recreación de la Sevilla de los 80 y sus suburbios, sus gentes y sus "roles" policiales. No esperaba ver nada magnifico del sobre valorado "Mario Casas" sin embargo esta vez con un papel mas maduro y atormentado parece que ha llegado a los mas críticos, igualmente mejorable, sobran ocasiones donde el "adolescente perpetuo" sale a la luz y mancha el resto de la cinta, igualmente... un aprobado.
Respecto a la ambientación y fotografía, de 10, quizás algún plano perdido que podría haberse realizado mejor pero sin muchos contras... correcto.
También comentar la actuación de "Antonio de la Torre", magnifico como siempre y de su atormentada relación con ese "amor eventual", muy acertado.
En definitiva, un Film policíaco con tintes de thriller y oscura moralidad, entretenido y fácil de ver pero donde en muchas ocasiones resulta inevitable pensar que el papel protagonista no era para "Mario Casas" y si para un actor mas serio y polifacético.
        
            