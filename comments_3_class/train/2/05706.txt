
                      Solo logro entretener. Un guion sencillos con más fallas que acierto. En la producción se puede notar el contraste entre Snyder y Whedon, más que todo en la fotografía, y según especulaciones Whedon arruinó la cinta recortando el metraje, algo que nunca podremos asegurar sin tener el corte pleno de Snyder.
Al final lo único que acierta son en los personajes donde al terminar olvidarás la película y te quedarás con la ganas de ver más a Wonder Woman, Batman, Supermán, Aquaman y a Flash.
        
            