
                      Es una película dificilmente calificable: surrealista? de humor absurdo? de denuncia social?... No creo que sea tan mala como he visto en algunas opiniones ni tampoco muy buena. Cuesta entenderla (si es que tiene explicación), tampoco es para reirse a carcajadas aunque tiene algunos golpes buenos. En fin, recomendable solo para gente de mente abierta y fans del director.
        
            