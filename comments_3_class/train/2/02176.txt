
                      NO SOBRAN MOUSTROS,  SOBRAN HUMANOS. 

Fui a esta película con un ánimo especial, la anterior no me pareció mala y había oído buenas cosas de esta,  así que entre al cine con espectativas muy altas. Gran error. 

Salí del cine aburrido y decepcionado aunque después de pensarlo que  película no era tan mala. 

Los personajes humanos se la sudan a todo el mundo,  no están bien construidos y aunque la pelicula se empeñe,  acaban sin personalidad y más planos que una mesa. En resumen,  la familia principal tiene más tiempo en pantalla que el que se merece. 

Los humanos solo están ahí para que veamos  a los moustros dándose de hostias,  y por suerte eso la película lo hace bien. 
La banda sonora,  los diseños brutales,  y la cámara que es clara con planos convencionales y con planos inestables y más largos en escenas de batalla hace un buen trabajo. 

La fotografía, junto a planos levemente contra picados , la iluminación hermosa, y la increíble banda sonora hace parecer a los kaijus dioses, y eso es un deleite para los ojos. 

Me da pereza escribir más así que en resumen,  es una película con un guión pésimo,  pero que es servible para pasar 2 horas en el cine.
        
            