
                      Un film para los más chicos, para los que todavía creen o conservan algún recuerdo de lo que era la navidad y la ansiada espera de la llegada de Papa Noel. 
La pregunta para los jóvenes obsesivos que esperaban los regalos era:¿Cómo hacía Papa Noel para repartir todos esos regalos? ¿Cuánto tiempo le llevaba?

La respuesta esta en este film, que hace notar la diferencia entre el delivery artesanal y el trabajo en serie de la tecnología. Este Papa Noel cuenta con toda la tecnología de punta al servicio de optimizar la entrega y dejar satisfecho al cliente, a diferencia de lo que sucedía en el siglo pasado que lo hacía surcando los cielos en un trineo de madera con polvo mágico para los renos y un mapa de papel.
El “pero” en este film comienza con el error, con en el equívoco que hace que a un niño no se le pueda entregar su regalo. La respuesta de Steve ante este incidente se reduce a estadísticas, a números,  minimizando el asunto y tomando como un éxito la operación, pues que “falte” uno no empaña el resultado. En tanto que Arthur el hijo menor de Santa Claus considera que si al “menos Uno” no recibe el regalo, el trabajo no esta terminado porque lo que cuenta no son los números sino el niño.  

El film muestra el recorrido que dibuja la importancia de que la cosa funcione y no tanto en quien ocupa el lugar, los narcisismos personales, que por cierto no escasean en la película, sino de quien se encuentra a la altura de sostener la función, que el lugar haga funcionar a la “creencia” entregando los regalos.
        
            