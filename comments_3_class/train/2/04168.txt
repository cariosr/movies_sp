
                      Brave (Indomable) era una de mis películas Disney "pendientes" aún y hoy, por fin, he podido echarle el guante. He de reconocer que Disney evoluciona en los guiones de sus historias (y que cumpla muchos más) cuando te das cuenta del protagonismo de la mujer desde otro punto de vista: "El ya no quiero seguir siendo princesa. Ahora soy guerrera, soy dura, soy...LIBRE!"- Y ahí lo dejo.

El diseño de los paisajes y personajes es fantástico (no es de menos en la fábrica Disney) y he de destacar su banda sonora como espléndida, satisfacción por los cuatro costados cuando escuchas esas gaitas, instrumento fundamental de las composiciones célticas, deleitando una sensación de viaje al pasado (imprescindible usar cascos

Muy recomendable.
        
            