
                      Largometraje de difícil visión, que seria conveniente revisitar para apreciar la buena calificación que le ha otorgado la critica especializada. Yo desde mi posición de amante del cine, y quizás no cinéfilo, desde mi punto de vista de espectador mediocre, lo único que pude percibir es una buena fotografía en blanco y negro, con una buena factura técnica, pues lo que es el guión insinúa más que muestra, teniendo que llegar a nuestras propias conclusiones. Para muy cinéfilos.
        
            