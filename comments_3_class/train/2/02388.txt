
                      El acierto de esta comedia que ha arrasado en las taquillas italianas es que contrapone la naturaleza cálida mediterránea (en la que los españoles podemos reconocernos fácilmente) con la gélida idiosincrasia nórdica; de este choque de culturas surge lo mejor de la película, el resto lo pone un actor cuyo físico personifica a la perfección el icono medio del funcionario y su “vida celestial”, a la que no está dispuesto a renunciar por nada del mundo (o casi, ya que el final no encaja en el tono).
        
            