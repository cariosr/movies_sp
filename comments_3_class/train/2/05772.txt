
                      Kong: la isla Calavera nos ofrece una nueva revisión, con carácter de precuela, anticipando la búsqueda y encuentro con el simio más grande del planeta Tierra. En su búsqueda hay un grupo variado de personajes con intenciones diversas. Así la historia, que podrá gustar más a unos que a otros, en mi caso, me pareció entretenida, pues resulta evidente que ya sabemos qué pasará. En general aceptable. Está cargada de música.
        
            