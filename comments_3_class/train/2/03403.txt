
                      Por primera vez, puntúo una película muy por encima de los medios...Me parece poco original (Ratatouille) pero el casting es magnífico, los actores desarrollan su papel mejor que bien y el conjunto, entretiene y deja un buen sabor de boca. Es como aquellos vinos que de entrada no son gran cosa...y luego los recuerdas porque tenían algo. Ese algo lo tiene esta peli.
        
            