
                      Lo mejor de la película son los quince minutos en blanco y negro. Después, cuando James Franco llega al reino fabuloso, la historia se pierde entre decorados y flores musicales, y la presentación de los personajes, todos del gusto infantil. Las brujas y el tercer acto, el retorno de Franco a los tiempos del tramposo embaucador, recuperan algo la energía de la película que mostró al principio. Es un filñm Disney, correcto, hecho con molde, pero fácilmente olvidable.
        
            