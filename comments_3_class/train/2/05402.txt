
                      Quien le iba a decir a Mark Wahlberg que su carrera después de pasar varios años en la cárcel (buscad en  Wikipedia) no estaría ligada a la música basura, si no mas bien al cine, y es que no nos olvidemos, que aunque su debut fue "humilde" su escala ha sido meteórica y ha rozado la mayoría de géneros excepto la que el mismo denomina "mariconada" como la romántica, un tío duro llamemosle.

Eso de ir de tipo duro le va como anillo al dedo (Infiltrados, Shooter, 4 hermanos, Max Payne....) y en esta su registro no es muy diferente.

Film político, donde se muestra que toda la basura no esta en las calles, si no en los despachos, y toda afirmación es una contradicción en si misma. Aunque decae a medio Film, sigue intrigando y entreteniendo, no deja de ser un Thriller mas, pero bien ejecutado y sin florituras.

Resumen: Si te gusta el suspense, la traición y sobre todo un final con buen sabor de boca, con "Ciudad Rota" (La trama) pasaras una muy buena tarde.
        
            