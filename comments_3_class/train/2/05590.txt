
                      Pues nada, vista la peli de la temporada. La verdad es que es bonita y entretenida, aunque como musical, salvo honradas excepciones (como la escena de la foto), es un poco flojo. También me ha disgustado el final a lo comedia romántica moderna (estilo 500 días juntos). Será que soy un clásico pero, si es una película, que termine como debe terminar una película (para ver realismo me despierto todos los días). Reparto poco coral pero actuaciones notables, sobre todo Emma Stone que está espectacular.

¿Recomendable? sin duda. ¿Olvidaba? Probablemente si.
        
            