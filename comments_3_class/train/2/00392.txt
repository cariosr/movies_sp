
                      Brawling, beefcake y bullets son ingredientes clave en este hotchpotch de una película, reunidos a partir de los clásicos del género criminal de Los Ángeles, pero hechos sin la sutileza, la originalidad o el cuidado de la edición.

Entre los hombres masculinos en el reparto, el alfa cultivador en jefe masculino es Butler, que interpreta a Nick, un antihéroe que rompe las reglas al estilo de Michael Chiklis 'Vic de la serie de televisión The Shield (2002 a 2008). ).

También es un hombre con una vida familiar problemática (por supuesto) y un amante de los atajos de investigación y de irse a casa con strippers.

La película comienza con una larga escena de un robo de un vehículo blindado que salió mal, un poco más que un eco de la misma apertura en Heat (1995).
        
            