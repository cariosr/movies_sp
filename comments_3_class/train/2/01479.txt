
                      Winchester: La Casa que Construyeron los Espíritus (2018).
"¿Sabe quién es el monstruo más aterrador?
El que invitas a tu propia casa."

Los australianos hermanos Spierig, responsables de la octava secuela de Saw, nos traen esta cinta de terror al uso que, aunque cuenta con una original (y real) premisa, no consigue enganchar al espectador... Fantasmas malignos, clásicos sustos con subidas del sonido, y caritas de pocos amigos entre el servicio de la dueña de la mansión. 

Quitando todo eso, lo que realmente (casi) salva el largometraje es la presencia de sus tres protagonistas: la veterana Helen Mirren, en el papel de la atormentada viuda del creador del rifle de repetición Winchester; Jason Clarke, quien deja por ahora a los célebres 'simios' y a 'Terminator' para interpretar al doctor que viene a prestarle ayuda a la acaudalada señora para que logre superar a sus demonios y, desde luego la casa, cuyo diseño de producción es una pequeña maravilla gracias a la magia del cine... y a un buen número de carpinteros, sin lugar a dudas.

La fotografía también ayuda bastante a mejorar la calidad de la cinta, pero sin conseguir que la crítica le dé un aprobado.
Entretenida, sin más.

Raúl Cabral.
https://www.filmaffinity.com/es/film689565.html
        
            