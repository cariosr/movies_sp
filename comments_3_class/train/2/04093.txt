
                      Christopher Robin (2018).
"No hacer nada a menudo conduce al mejor de los algos."
Como ya hiciera en "Descubriendo Nunca Jamás”, el director de cintas tan dispares como “Quantum of Solace” o “Guerra Mundial Z”, Marc Foster, nos introduce en una fábula infantil vivida por su protagonista (humano) ya de adulto.

“-Soy un oso con muy poca cerebro. 
  -No, yo creo que eres un oso con un gran corazón."
Ewan McGregor es Christopher Robin, el inseparable amigo de Winnie the Pooh y su pandilla de peluches, a los que ayudaba y con los que jugaba de niño (fue el hijo del creador de estos personajes en la vida real); pero ahora, convertido en un estresadísimo adulto, apenas recuerda... hasta que aparecen necesitados de auxilio; aunque en realidad los que los necesita es él a ellos.

La cinta, nominada al Oscar por los efectos visuales, está ambientada magníficamente en el Londres de los años 40, y cuenta en el reparto con Hayley Atwell, a quien muchos recordaréis por encarnar a la pareja (también en esa década) del célebre Capitán América.

"Los sueños no son gratis. Nada es a cambio de nada."
La película, aún estando dirigida a todos los públicos, no deja de regalarnos mensajes continuamente de la mano de esos entrañables peluches. Y es que, a veces, la felicidad se encuentra en los detalles más simples y cercanos.

Raúl Cabral.
https://www.filmaffinity.com/es/film759500.html
        
            