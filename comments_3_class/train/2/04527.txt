
                      El cineasta que dirigió  a Russell Crowe en Gladiator, Ridley Scott, con "un buen año "nos ofrece una película totalmente diferente , nada mas  y nada menos que una comedia con toques románticos que recuerda a las antiguas / clásicas películas del cine francés . Poco se nos mostró esta faceta divertida de Crowe en la cual sinceramente se le ve realmente bien . No es una película altamente recomendable en pantalla grande pero , para una tarde divertida en casa es un entretenido y chispeante filme.
        
            