
                      Antes que nada decir que no he leído ningún tebeo de este superhéroe sacado de la mitología escandinava, por lo tanto, me limitaré a comentar el largometraje por sí solo.

Los largometrajes de superhéroes al igual que los denominados de acción se basan en una fórmula sencilla: cuatro o cinco escenas de acción enlazadas mediante una trama más o menos interesante, dependiendo de lo conseguida que sea la trama, obtendrá la aprobación o desaprobación de la critica, y dependiendo de lo conseguida que sean las escenas de acción, obtendrá mayor o menor éxito de taquilla.

A mí, personalmente siempre me han gustado los largometrajes de superhéroes, me distraen, me entretienen e incluso hay momentos en que me emocionan, y esta película cumple esas funciones, entretiene, distrae, incluso hay momentos que divierte y aporta un punto pequeño de emoción.

Las escenas de acción se alternan en dos universos, el universo de Thor, y el nuestro, curioso que esta vez no hayan designado la ciudad de Nueva York, y hayan escogido Nuevo México, lo que proporciona un destacado contraste entre el colorido y futurista mundo de Thor, y el desierto de Nuevo México.

En cuanto a los protagonistas, Chris Hemsworth luce cuerpo de gimnasio, Natalie Portman aporta naturalidad y sencillez,  y  Anthony Hopkins pone el carisma.

Respecto al guión poca cosa, pero que más da, es un largometraje de héroes y villanos.
        
            