
                      Rio
La historia gira en torno a Blu, un guacamayo domesticado y en peligro de extinción.
Lo último en 3D.
Sin lugar a dudas es puro entretenimiento y belleza (visual y musical).
De los creadores de Ice Age no decepciona.
En resumidas cuentas Rio es sin duda una película amena, vistosa y animada. Recomendada para una entretenida tarde de cine con toda la familia.
        
            