
                      Película número 50 de la película Disney. De momento será la última en que veamos a una princesa Disney. Espero que no sea también la última musical. Entretenida, divertida y con los personajes bien definidos. Tiene unas imágenes fabulosas llegando al clímax junto con la canción candidata a los Oscar de Alan Menken. Como toda película Disney tiene en contra que la historia se ha contado más veces, aún así vale la pena verla en 3D. De una nota del 1 al 5 pondría un 3.
        
            