
                      Si bien desde el punto de vista de la animación sigue siendo brillante y muy bonita, esta segunda parte carece de la originalidad, la frescura y el encanto de su antecesora. Tampoco el villano ni la historia son tan originales como en la primera, por lo que estamos ante una película netamente inferior. Aún así, es entretenida
        
            