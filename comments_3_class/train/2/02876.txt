
                      Ingenioso, original y novedoso argumento que llama sin duda mucho la atención del espectador, sin embargo el transcurrir del mismo deja ciertos momentos de aburrimiento, además del excesivo final feliz que hace la película algo previsible. De todos modos,  recomendada la visualización por su originalidad.
        
            