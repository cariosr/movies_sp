
                      La admirable y provocadora puesta en escena de Verhoeven y la inigualable -y un tanto psicópata- actuación de Isabelle Huppert se combinan (de forma satírica y rara) en 'Elle', una mirada al cine morboso. No encuentro entretenimiento en la película, llega a cansarme con una sucesión de planos cotidianos y en ocasiones anhelo su desenlace. Sin embargo, está demasiado bien realizada como para negar su merecido éxito.
        
            