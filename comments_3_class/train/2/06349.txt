
                      Una película tan bien dirigida e interpretada como otras recientes películas italianas de más fama y más pretenciosidad, y con la misma artificiosidad, de un preciosismo que aburre, y mostrando una descara explotación del legado histórico-cultural y geográfico del país que cansa.
Si le quitamos eso a la historia y el hecho, que podía ser particular en los años setenta del pasado siglo, de que el argumento verse sobre una relación gay, nos queda una película más de amores de verano y aprendizaje existencial y emocional.
Sólo salvaría de la película el largo y emotivo parlamento del padre del joven al final de la proyección.
Lo demás se queda en una aparente y perfecta narración cinematográfica, sin sustancia, nada original y con montones de recursos manidos.
Todo muy “dejá vu”
        
            