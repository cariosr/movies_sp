
                      ¨Ghost Ship¨ no es sino una película que ya por la sinopsis podemos adivinar de qué va, es decir, es algo predecible pero a la par, buena y sin caer en el aburrimiento.

Esta cinta de terror en un buque fantasma, cumple muy bien ofreciéndonos unos primeros minutos muy buenos, siguiendo como un slasher y terminando felizmente (aspecto visto hasta la saciedad y para nada original).
La historia del navío y su maldición del oro aparejado con el ¨recolector de almas¨ tengo que admitir que le gana cierta originalidad, sumada a unos impresionantes efectos especiales para la época y a unas actuaciones destacables.

Mas hay detalles negativos como por ejemplo, los absurdos créditos iniciales que recuerdan más a una película de Disney, el mediocre apartado musical (quitando la ópera) y unas muertes (también exceptuando la colectiva) simples y conocidas.

Aún así es una película que está entretenida para pasar el rato, pero si queréis más suspense mezclado con paranoia recomiendo ¨Triangle¨.

un 6
        
            