
                      Es una película estéticamente buena, el argumento es un buen punto de partida, los actores lo hacen bien... pero le falta ritmo, más giros, más ambición en las reflexiones, y, sobre todo, un final mejor.
        
            