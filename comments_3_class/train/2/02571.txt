
                      Transformers: El Último Caballero (2017).
"Cualquier tecnología suficientemente avanzada es indistinguible de la magia" (Arthur C. Clarke).
Michael Bay dirige la quinta entrega de su saga de Autobots contando de nuevo con Mark Wahlberg, quien luce palmito junto a su bella compi de reparto (Laura Haddock). Le acompañan los ya habituales en la saga Josh Duhamel y John Turturro; y los veteranos Anthony Hopkins y Stanley Tucci, que se reserva el curioso papel del célebre mago Merlin (cambiando de alter ego esta vez).

Como imaginaréis, la cinta viene cargada de acción, efectos especiales (ILM), ciencia ficción y mucho sexo... (no, sexo, no. Eso era broma...).

Los muñecos de Hasbro (empresa que coproduce la película) siguen dando de sí. De hecho, es probable que veamos una sexta entrega, y es que "la pela es la pela..."

En esta ocasión, y por rizar el rizo, mezclan la historia de los transformables con la de los caballeros del rey Arturo nada menos. Todo sea por perpetuar la costeada serie...

Entretenida para el que guste de este tipo de cine, y no le canse ver dos horas y media de robots rompiéndose la cara.
Una curiosidad: S. Spielberg hace las veces de productor ejecutivo.

Raúl Cabral.
        
            