
                      Parece que empiezan a volver las películas originales, lo malo es que la gran parte tienden a ser independientes ya que las grandes compañías no apuestan en ellos porque no generan ingresos suficientes, por este motivo hay que valorar mucho el cine independiente. Orígenes es una película distinta y muy bien pensada, si queréis ver cine original y de calidad os la recomiendo, si queréis ver películas comerciales y facilonas no la veáis porque no creo que os guste.
        
            