
                      Ficarra y Picone, un reputadísimo dúo cómico en Italia aunque poco conocido fuera de su país, se ponen delante y detrás de las cámaras para hacer reflexionar políticamente a sus conciudadanos, e indirectamente a los de medio mundo, a través de su último trabajo, La hora del cambio.
	Quizás en La hora del cambio haya localismos y un tipo de humor que no terminen de llegarnos del todo al público español,  sin embargo, hay otra parte del largometraje que consigue traspasar las fronteras y convertirse en universal, “porque en todos los puntos del mundo existen ciudadanos que exigen honestidad y legalidad, pero en el momento que tienen que renunciar a sus propios intereses, a sus ventajas, se lamentan” y acusan de malos políticos a aquellos que quieran recortar sus privilegios. 
	La hora del cambio se propone que reflexionemos sobre nuestra conducta como ciudadanos y, sin duda, lo consigue.
        
            