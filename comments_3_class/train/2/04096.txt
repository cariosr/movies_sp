
                      Alpha (2018). 
El director de El Libro de Eli nos lleva al Paleolítico; a unos 20000 años atrás, en una época en la que nuestro planeta era casi tan hostil con nuestra especie como lo somos nosotros con él ahora...

La historia propone una probable forma de cómo podría haber sido el encuentro del hombre con el pariente del perro, el lobo, y para ello contaremos con un joven pero experimentado actor en el cine fantástico y de acción (ver últimas secuelas de X-Men). 

Rodada en Canadá, con una maravillosa fotografía y una factura digna, aunque sin llegar a la calidad de En Busca del Fuego (favorita de mi buen amigo Charly), nos recuerda a la más reciente “10000” (también por esas cejas milimétricamente depiladas, moda en esas glaciales épocas...). 😉

Lo mejor, la relación hombre-lobo que se forma. Lo peor, que se hayan empleado vidas de bisontes para el rodaje de la cinta pudiendo usar ordenadores para estos menesteres. 

Raúl Cabral.
        
            