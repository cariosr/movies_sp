
                      Muy excesiva, muy espectacular y muy sobrada. Hay ordenador por todas partes en el montaje de ésta película y habrá quien lo agradezca y habrá quien lo repudie. Si te gustan las películas con mucha acción y con maniobras imposibles te va a gustar, sin lugar a dudas. Si no eres para nada de lo anterior vas a mal gastar tu dinero.
        
            