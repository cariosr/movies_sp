
                      Ver a esa Kate Winslet algo ruda, esa historia de amor y sexo rara, y todo ello mezclado hace que nos mantengamos muy atentos a la película a la espera de saber hacia dónde camina. Quizá sea lo mejor de la película: su estructura, poco habitual, hace que no nos podamos acomodar mientras la vemos porque no sabemos cuales serán sus ritmos.
        
            