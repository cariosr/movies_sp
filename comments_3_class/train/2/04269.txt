
                      Ayer tuve el gran placer de compartir con más amigos de sensacine el pase de Burlesque. Pasamos una noche estupenda, como ocurre con cualquier evento donde participe en la organización sensacine.

En cuanto a la peli, os animo a ir a verla a ...partir del viernes, especialmente a los amantes de los musicales como es mi caso.
Para mí se podrian dar 2 criticas/notas sobre la pelicula. Por un lado puntuaria la parte musical, a la que le daria un sobresaliente.
Cada número musical es un espectaculo visual y sonoro estupendo, no puedes apartar los ojos de la pantalla especialmente con C. Aguilera que cada vez que pisa el escenario y sale de su boca ese bozarroón se te pone el vello de punta, pero es que además es capaz de bailar a la vez de manera espectacular. (como actriz no llegaria tan lejos como obviamente va a llegar como cantante)
Lo que esta claro es que si a través de la pantalla te quedas "enganchado" y no puedes apartar la mirada cada vez que se levanta el telón de los espectáculos, imagino ir al club Burlesque y vivirlo en vivo y en directo y debe ser para morirse de gusto...
Cher tampoco me defrauda, me gusta como cantante pero tb como actriz (se notan sus tablas), y me gusta el carácter y humor que le dan a su personaje.

El resto de la peli, ya no es tan sobresaliente.... pues me parece simplemente enlaces entre espectaculo y espectaculo...con historias trilladas: problemas económicos, historias amorosas, busqueda del éxito, superación personal....etc.

Pero en resumen, si disfrutais viendo videos musicales, gente guapa (tanto de genero femenino como masculino), coreografias sensuales y provocadoras y soys fans de C. Aguilera o Cher..no dejeis de ir al verla al cine..¡¡¡
        
            