
                      Película interesante, bien compuesta y con una historia bien estructurada y relatada, realista pero no apta para menores de 21 años por ciertas moralejas, escenas, entre otros puntos. 

Le quita puntos el mensaje que da la historia y su final con una moraleja sin moral. 

Apta para ver en pareja, amigos pero no en familia.
        
            