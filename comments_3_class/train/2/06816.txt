
                      Pelicula simpática, agradable de ver y no tan transgresora como el tema puede sugerir. De hecho, salvo por la temática, resulta bastante convencional, lo que no impide que sea entretenida. Recomendable como comedia romántica ligera con un toque gamberro.
        
            