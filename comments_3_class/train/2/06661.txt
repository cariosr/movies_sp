
                      Malick consigue que odies o ames sus pelis, o eso dicen, a mi ni uno ni lo otro. Me parece que está bien pero que creo que pretende mostrar que va mucho más allá pero ese más allá está muy vacío, o al menos él no sabe rellenarlo. La fotografía es bonita pero en momentos agobia.
        
            