
                      Humor y parodia son los puntos clave que consiguen otorgarle éxito a esta película. Chistes continuos que, aunque en ocasiones algo sexistas, se introducen sin disimulo y causan en el espectador una sonrisa instantánea que, por otro lado, casi nunca llega a carcajada. Lo mejor: Ryan Reynolds en su papel de anti héroe, se ríe de sí mismo y está sublime actuando. Lo peor: el exceso de peleas y las imágenes al detalle de “piezas de carnicería” que revuelven el estómago.
        
            