
                      Cuando te sientas frente a la pantalla a ver un producto como Megalodón, no buscas más que un rato de divertimento sin más pretensiones ni artificios. Y creo que eso lo consigue: El fondo abisal está visualmente muy atractivo, los efectos son muy decentes y la ambientación de la tensión cuando aún no ha explotado la acción está bien contenida. Y cuando ésta llega, quitando los típicos e inverosímiles momentos del héroe (tan frecuentes en los blockbusters), es entretenida y el tiburón es lo bastante impresionante para pasar un buen rato.

