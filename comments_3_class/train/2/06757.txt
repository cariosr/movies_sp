
                      Lo mejor de la película es la historia que cuenta. Lo que cuenta lo cuenta bien, pero en algunos momentos del principio se hace lenta y se entretiene en contar detalles que no aportan nada. Sin embargo no cuenta casi nada de los años intermedios. Creo que es mejorable. De todas maneras recomendable verla.
        
            