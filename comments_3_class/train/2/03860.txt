
                      Operación: Huracán (2018).
“El coraje es la resistencia al miedo, el dominio del miedo, no la ausencia de miedo.” Mark Twain.
El director de la primera parte de 'A Todo Gas' nos trae este “mix” entre el 'Hard Rain' del pasado siglo (recordemos a Freeman y a Slater peleando por aquel furgón de pasta) y ‘Twister'.

La bella Maggie Grace ya ha crecido y madurado desde que el que hiciera de su progenitor, Liam Neeson, en ‘Venganza' se encargara de protegerla a muerte. Y aquí sabe defenderse pero que muy bien, aunque con la ayuda de Toby Kebbell (de la nueva Ben-Hur). Destaca entre los villanos el veterano Ben Cross.

Los efectos visuales junto con el vehículo que luce Kebbell son los otros dos protagonistas de la cinta. Una película rodada a caballo de EE.UU., Reino Unido y sobre todo, Bulgaria, y que nos muestra- de manera ‘algo’ exagerada, todo hay que decirlo- la violencia de un huracán como nunca antes lo hemos visto... ya nos anuncian a principios del largometraje de 100 minutos que es lo que nos queda con el dichoso cambio climático...

Entretenida, sin duda, para los amantes de las pelis de catástrofes, en este caso aderezado con un puñado de malos dispuestos a todo por dinero...

Raúl Cabral. 
https://www.filmaffinity.com/es/film962472.html
        
            