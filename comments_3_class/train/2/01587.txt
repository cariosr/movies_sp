
                      CUERPOS ESPECIALES
 Opinar es fácil, otra cosa es que estemos de acuerdo.
 En el blog, definimos la película como la imagen que ofrecen en la película Sandra Bullock y Melissa McCarthy, ambas protagonistas de Cuerpos especiales, es artificial, falsa, inexacta, inconexa e irreal. Parecen, lo que no son y, la vista rechina en unas interpretaciones muy dignas, pero no en el momento adecuado. 

 Paul Feig, el director, ha hecho un buen trabajo, una comedia que te ríes, las actrices interpretan con calidad, pero existen piezas que no están en su lugar oportuno. Es un puzle con piezas equivocadas.
        
            