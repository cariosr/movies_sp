
                      Esta disección de una separación resulta bastante interesante sobre todo desde el punto de vista cinematográfico, puesto que la forma en que el director la retrata, con extensos planos secuencia o esos largos silencios, le dan mucho realismo. Los actores están muy bien, quizás las niñas no tanto, pero lo que más chirría es alguna que otra situación, que puede resultar poco verosímil.
        
            