
                      'Mascotas' comparte diversión y gracia con otras películas de animación como 'Bolt' o 'Zootrópolis', pero no acaba de ofrecer la frescura necesaria para incidir, de nuevo, en historias de perros y gatos. El principio alberga elementos originales, gags bien desarrollados y un exquisito diseño neoyorquino. De ahí en adelante, se pierde en lo rutinario, decae, y solo asciende cuando sus escenas las protagoniza el personaje de Snowball.
        
            