
                      No voy a negar que cuenta con estratégicos puntos de humor y que las risas están aseguradas. Sin embargo, se trata de tópicos manidos en exceso y cargados de obviedad. Aun así, seguramente por razones culturales que me unen a España, mi lugar de origen, me resulta mucho más amena y entretenida que su homóloga rival americana 'Resacón en las Vegas'.
        
            