
                      Amityville: El Despertar (2017).
Nueva vuelta de tuerca al caso de la famosa casa donde ocurrieron los conocidos hechos a principios de los 70's, pero 40 años después de los asesinatos originales, y de la cual se han hecho ya bastantes largometrajes (en la película vemos una secuencia de la de 1974 con Brolin padre a la cabeza del reparto).
Jennifer Jason Leigh es quien protagoniza este thriller de terror de menos de hora y media de metraje, que nos entretendrá si no somos demasiado exigentes. 

De factura correcta, con una música que cumple su misión aunque con quizá cierto abuso de las subidas de volumen, y una historia que no está mal. Tiene, de hecho, alguna originalidad en ciertos momentos, pero sin descubrir nada nuevo en realidad.

Raúl Cabral.
https://www.filmaffinity.com/es/film483435.html
        
            