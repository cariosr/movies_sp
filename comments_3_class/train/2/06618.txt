
                      El racismo y Francia lleva unos años de rabiosa actualidad, así que, ¿qué mejor que plasmar esa realidad en una comedia para buscarle ese otro punto de vista?
El argumento se centra en una adinerada familia de la clase media-alta francesa formada por los padres y cuatro hijas, las cuales, año tras año se van casando... con extranjeros o chicos 'no católicos', así pues, los yernos de los afligidos protagonistas son un musulmán, un chino, un judío y por total sorpresa de todos, la pequeña de la familia se casa con un negro católico.
Ya os podéis imaginar el trauma y el montón de situaciones cómicas que se pueden dar. 
La verdad es que se trata de una comedia fresca, divertidas aunque eso sí, algo desaprovechada (con muchos paralelismos con 'Ocho apellidos vascos'.
        
            