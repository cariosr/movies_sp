
                      Muchos falsos arranques, escenas de bienvenida pronto emplomadas por torpes demostraciones de lágrimas. El ritmo es continuamente interrumpido por charlas, donde la ausencia de elipse compite con la vacuidad de algunos diálogos. ¿Y qué? Por cierto nos reímos y varias escenas logran conmovernos. Los actores luchan con talento para hacernos compartir sus delirios. La imagen logra darnos esta sensación de nostalgia del "era mejor antes". ¡ Pero el cine italiano ya nos ha hecho despegar tanto por el pasado, que lamentamos aquí quedar a tan baja altura!
        
            