
                      La película se quedo muy corta en comparación con la serie.
Creo que lo he dicho ya en otra critica, esta es esa clase de películas que disfrutan únicamente los niños, nada más, es por eso que, a pesar de querer calificarla como "mala", le asigne la nota de "pasable".
Si nunca en mi vida hubiera visto su serie, seria muy probable que incluso le pusiera tres estrellas al film, pues al fin y al cavo la tomaría como una película pensada para los niños.
Pero la realidad es que "los pingüinos de Madagascar" nos tenían acostumbrados, tanto a chicos y a grandes, a una buena dosis de humor. En su serie son capaces de encantar y divertir incluso a muchos de los mayores. 
La verdad al ver "su película" en la cartelera, uno entró al cine con altas expectativas y salió decepcionado. 
En conclusión: No puedo darle dos estrellas o menos, ya que la película tiene como objetivo llamar la atención de los pequeños, pero no puedo darle tres estrellas o más, porque seria estar olvidando el pasado protagonismo que, tanto en las películas de Madagascar como en su propia serie, nos regalaron en el pasado nuestros cuatro amigos.
        
            