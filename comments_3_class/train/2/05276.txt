
                      El visionario director Gore Verbinski compone un interesante thriller psicológico que traslada el lenguaje del terror gótico a los balnearios de salud, insuflando nuevo aire al género con una película visual y ambientalmente impresionante a lomos de secuencias poderosamente oníricas y enigmáticas.
        
            