
                      El planteamiento está bastante bien,  con una introducción que produce atmósfera agobiante y una sensación de estar desprotegido junto a su protagonista en esa morgue que parece más jaula con el león que morgue.  
El problema es que se pasa casi de la introducción al desenlace,  el nudo es muy corto y no desarrolla bien la historia.  Te llegas a creer que han pegado tijeretazo x motivos desconocidos (producción?) pero ahí a mi juicio se cargan la película,  porque precipita el desenlace y rompe la tensión tan insidiosa de la 1a parte de la película. 
Se debería cocer a fuego lento pero el cine de ahora parce que necesita olla express..

Lo mejor: la ambientación agobiante y la mezcla de morgue y exorcismo
Lo peor: el ritmo del desenlace
        
            