
                      Esta ha sido una película algo decepcionante para mi ya que la primera ha sido excelente y una de mis favoritas.
Me han gustado los personajes nuevos y me ha encantado ver a los anteriores personajes (aunque no haya aparecido el personaje de Will Smith), la historia es bastante interesante y los efectos especiales son buenos pero para mi ha carecido de emoción, las escenas a veces eran raras y algo absurdas en ciertos momentos y en cuanto a las actuaciones de algunos actores ha dejado mucho que desear. Para mi también ha fallado la banda sonora porque no lo sentía bien en los momentos más tensos o de acción.
Me ha parecido una continuación interesante pero que no ha colmado mis expectativas, aún así no sé si seguirán con la siguiente película pero si lo hacen la veré, aunque con pocas expectativas.
        
            