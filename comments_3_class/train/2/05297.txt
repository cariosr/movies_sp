
                      En muchos aspectos, con esta tercera entrega de Alien, se desdibuja la idea original del film de Ridley Scott, cuyo giro de Cameron con Aliens El regreso resultó de lo más digno aunque saliendo del terror y adentrándose en la acción. Esta tercera no se sabe bien si bebe de una o de la otra, quedando como un ejercicio bastante torpe en cuanto al guión, aunque deja ver desde el punto de vista cinematográfico el potencial de David Fincher como uno de los directores contemporáneos más personales e interesantes
        
            