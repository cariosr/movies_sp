
                      Es una película muy esperada y muy entretenida. Los efectos especiales superan evidentemente los de Jurassic Park pero no son ni mucho menos lo que me esperaba.  El casting bien elegido, hay guiños a la primera entrega de la saga. Me ha gustado pero creo que va dirigida más bien a un público infantil-juvenil.
        
            