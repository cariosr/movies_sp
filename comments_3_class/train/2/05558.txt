
                      Película entretenida que cumple con el estilo francés. La peli va de menos a mas. El principio puede resultar en algunos momentos un tanto anodino pero conforme va avanzando la trama esta se va enredando, los personajes se van desmelenando y la comedia va apareciendo. Destacar el papel de Patrick Bruel que hace de su personaje el mas gracioso sin duda. Peli recomendable para una tarde de domingo. (por cierto le da un repaso a Roman Polanski de como hacer una comedia con cuatro personajes en un salón).
        
            