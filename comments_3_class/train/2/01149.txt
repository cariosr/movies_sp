
                      El trailer de “La fiesta de las salchichas” fue tan brutal, tan tronchante, tan divertido, que parece que hacia presagiar una de las comedias más gamberras del año. A mí, personalmente, me supuso un soplo de aire fresco en las películas de animación para adultos y elevó mi hype a límites insospechados. Cuál es mi sorpresa cuando, tras ver la película, me encuentro con que “La fiesta de las salchichas” no tiene ni chicha ni limoná.

Para empezar, la trama que se deja entrever en el trailer, es mucho más atrayente que la que luego se nos muestra. La trama que tiene en realidad la película es completamente insulsa, carece de sentido y no engancha ni atrae por ningún sitio. El problema principal, de hecho, puede que sea ese. Necesita un hilo más profundo que mantenga atento al espectador. Y es que hacia la primera media hora, dejas de poner interés en lo que estás viendo y te limitas a dejarte divertir por una sucesión de chistes zafios y burdos más o menos graciosos que te hagan ameno lo que queda de película.

La película quiere ser tan graciosa, quiere superar tanto los límites de lo zafio, quiere ser tan descarada y tan alocada que se hace hasta cargante. De hecho, cuando llegan los últimos 5 minutos (unos 5 minutos que dan vergüenza ajena), te sientes tan pesado de chistes sobre sexo y drogas que lo último que deseas es que no tenga una escena post-créditos que te hagan quedarte más tiempo en la butaca. Sí la tiene. Aunque puede que sea lo mejor de toda la película.

Aún así, como herencia de la serie (y la película) de “South Park“, puede ser algo para pasar la tarde entre amigos. Pero nunca como un entretenimiento cinéfilo, y ni mucho menos como una película a tener en cuenta más allá de cuatro carcajadas simples.

Lo mejor: sus primeros 10 minutos.
Lo peor: que el trailer sea mejor que una hora y media de película.
        
            