
                      Matar o Morir (2018).
Primero Charles Bronson mutó en Liam Neeson, y ahora Liam Neeson lo hace en la persona de la bella Jennifer Garner. Vuelve el tema recurrente de la venganza de la familia por parte del familiar superviviente; en este caso, el alter ego de Garner. Del 'Paul Kersey' de Bronson, pasando por Gerard Butler representando su particular versión del “ciudadano ejemplar”, hasta el caso que nos ocupa, siempre ha dado buenos resultados en taquilla el tema de los justicieros...
Nuestra heroína, preparada para la ocasión, (se ha puesto tan fuerte como lo estuviera Hilary Swank en "Million Dollar Baby"), deja sus japoneses Sai de “Elektra”, y se hace con un buen arsenal de armas automáticas para meterse en la piel de una atormentada madre que, tras los hechos acaecidos 5 años antes, clama venganza contra el cártel culpable de su desgracia.
El realizador responsable de “Venganza” nos trae, una década después, otra cinta de acción con el mismo hilo argumental y mucha violencia de principio a fin, pero cambiando de sexo al protagonista. No olvidemos que la mujer está de moda... 💪
Una cinta entretenida para todo amante de este subgénero, y con más posibilidades de gustar si nos fijamos en las opiniones de los críticos (la ponen de mala, con lo que nos invitan a verla...). 😉
Raúl Cabral.
        
            