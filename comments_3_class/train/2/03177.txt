
                      Como en toda la filmografía de Howard, el intento de blockbuster se mantiene, solo que en 1995 el término  no estaba tan expandido como ahora. Desgraciadamente, la película sobre un acontecimiento tan impactante como el del Apollo 13 resulta rutinaria y fugaz. Se alternan dos únicos sets de rodaje -la sala de control de la misión y la nave- y, gracias al carisma de Ed Harris, su duración queda exenta de justificación.
        
            