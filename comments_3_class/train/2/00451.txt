
                      A pesar de lo que Robert Redford ha dirigido, ha salido mal, porque como es historica, no ha podido plasmar los hechos de una buena forma. Lo que me ha sorprendido es el reparto, y lo hacen sobradamente bien. Pero eso, Robert Redford, a pesar de hacerlo bien, lo interpreta muy mal.
        
            