
                      Film de abrumadora belleza. Un drama de lento desarrollo, lamentablemente MUY previsible su guion y sublime interpretación de su estrella "Liam Neeson".

Realmente no vayas a buscar en "Infierno Blanco" una película de acción, por que no lo es, es un Film reflexivo y de exquisita lentitud y donde cada fotograma transmite angustia y paz a partes iguales.
De fondo Alaska, donde en un par de ocasiones se toma la licencia y nos muestra que no es tan terrible morir, si lo que ves es mas bello que tu vida diaria.
Quizás se merezca 3 estrellas, pero a mi parecer su simple argumento y extremadamente vació guion le resta muchos enteros, dicho esto, también debo decir que haberle dado mas protagonismo al Guion, no nos habría dejado ver la pureza del resto de la cinta...

Si te gustan los documentales, la belleza visual de los entornos naturales y sobre todo el desarrollo lento y sin sorpresas... esta es tu película.
        
            