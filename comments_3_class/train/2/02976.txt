
                      Película bastante divertida y entretenida, que tratando un tema tan típico en la época navideña como el de Santa Claus, le da un toque realmente innovador y aporta una visión actualizada a los tiempos que corren, sin perder para nada la esencia del espíritu navideño y el papel del legendario personaje.
Los efectos del 3D creo que están bastante bien, no como en otros títulos y el ritmo de la peli es bastante trepidante a lo largo de casi toda ella.
Se trata de una buena opción para llevar a los peques al cine en estas fechas, aunque para los mayores pueda faltarle algo en algún momento, siempre en mi modesta opinión.
        
            