
                      En mi opinión no considero que "El bar" sea una pérdida de tiempo total. 

La película empieza genial: los personajes se caracterizan por sus propias y marcadas personalidades, con frases divertidas e ingeniosas. Todo bien, risas y expectación hasta que la trama se empieza a desmadrar. Entiendo el intento que se realiza en la película a la hora de mostrar lo que el ser humano es capaz de hacer cuando tiene miedo o quiere obtener algo, pero desde mi punto de vista se centran tantísimo en ello que la trama pasa a segundo (o tercer) plano y quedan varias preguntas sin resolver de la historia que, una vez acabada la película te quedas a ver los créditos esperando (deseando) aunque sea una simple imagen que te aclare algo de la trama que acabas de ver tras soportar 1 hora y 40 minutos de la película, de los cuales los últimos 40 has sufrido la angustia y claustrofobia que causa ésta.

Para concluir diré que en mi opinión es una película que si, causa sensaciones en el espectador y si, lo mantienen enganchado, pero que al final es una película más cuya trama queda inconclusa y sin sentido. No es mala, pero si como a mí te gusta entender algo sobre por qué suceden X cosas en la trama te vas a quedar con las ganas.
        
            