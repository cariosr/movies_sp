
                      No esperaba este enfoque para una película destinada a los más peques, ya que por los trailers e información parecía tener una trama muy infantil y sin apenas profundidad. El aspecto musical es asonbroso, y la historia se hace muy corta pero intensa. Una obra magna más firmada por Disney.
        
            