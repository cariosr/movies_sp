
                      Trepidante final para una saga que estaba pidiendo acabar hace años y que han alargado demasiado, pero para todo fan de Resident Evil son unas horas más de entretenimiento. Ha sido un final con muchísima acción en el que Alice no descansa ni un segundo con tantas patadas, disparos y explosiones, también nos revelan cosas muy importantes que sirven para cerrar esta saga sin dejarse cabos sueltos.

Resumen: Muchísima acción, entretenida y muchas respuestas para los fans, pero nada del otro mundo.
        
            