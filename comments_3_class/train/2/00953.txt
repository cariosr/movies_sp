
                      Un rato divertido reforzado por una música que mete al espectador en la búsqueda de amor de los personajes. Estos, hacen muestra de una fuerza de carácter y la vez de una indiferencia que crean las situaciones de tensión (familiar o entre amigos) o de gravedad (la muerte, búsqueda de su sitio en la vida).
        
            