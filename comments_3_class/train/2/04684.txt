
                      He estado columbrando varias perspectivas desde las que hacer la reseña de esta película, incluso como si no fuera secuela de otra, como si su predecesora no hubiera existido.
Pero al final todas las perspectivas me llevaban al origen.
Porque el origen de este film y la razón de su existencia es quizás la mejor película de ciencia-ficción que se haya rodado nunca. Y eso no se puede soslayar y eso pesa.
Digámoslo de una vez. Esta peli no tiene más razón de ser que hacer caja a cuenta de su precedente. Lo que no es criticable si el resultado hubiese merecido la pena. Pero no lo merece.
Le ha faltado una historia de fuste, con tanta carga simbólica como su predecesora, y quizás había un camino para ello. Viendo los hologramas quizás por ahí había camino para desarrollar una trama en varios sentidos: Irrealidad, identidad… pero no.
Los guionistas se fueron a lo más manido… que los replicantes pudiesen dar un paso más hacia su necesidad de sentirse humanos… que tampoco hubiera estado mal si se hubiese trabajado más el asunto, pero se quedaron en la superficie.
No hay una sola frase en todo el metraje que tenga algo de mensaje, de ingenio, de reflexión. Guión pobre, plano, consabido. Con ecos de Terminator, Mad Max y por supuesto, no ecos, si no “corta y pega” de su predecesora.
¿Y la música? La de veces que habré escuchado la BSO de Vangelis, tan sugerente, tan envolvente, parte imprescindible en el anterior film. La de esta historia es gris, gris, por muchos crescendos que tenga. Que al final suenen notas de la música de Vangelis sólo es la prueba de lo que a lo largo de la proyección se notaba… un querer y no poder.
Los niveles interpretativos son correctos, aunque no hay y se echa a faltar un Rutger Hauer que más allá de lo icónico le metiese teatro a las interpretaciones. Tampoco hay una Rachel, ni un Tyrell. Su sustituto con mucha parafernalia y poca chicha es poco más que un cromo.
Toda la película se desliza al lado de su predecesora como una rémora tras un tiburón o como un aficionado tras un profesional.
Alguien debería decirles a los que se dedican al cine y ponen a actores por encima de los sesenta años a actuar, que cuando a un hombre de esa edad le das un puñetazo bien dado es más que posible que no necesite más. Por lo de la verosimilitud. Son pequeños detalles que estropean un proyecto. A menos que la cosa vaya de guasa, que no es el caso.
No hubría necesitado ver toda la película para darme cuenta de que no era más que un triste y fracasado remedo de la primera si hubiera visto el final antes. Por un momento me puse en lo peor y estaba ya oyendo  “Yo he visto cosas que vosotros no creeríais. Atacar naves en llamas más allá de Orión…”
He hecho una pausa para buscar la escena en Youtube y ponérmela. No sé cuantas veces la habré visto. Rutger Hauer  soltando su reflexión, su sonrisa de resignación, la cara de Harrisón Ford, la lluvia, la música y la paloma volando. ¡Qué escena! Poesía y emoción pura. ARTE.
Pero no, la cosa no pasa de una pobre muestra de falta de imaginación y ambición. De todos los finales posibles, si yo hubiera sido el de la pasta, este se habría rodado por encima de mi cadáver.
¡Ay, esta industria que no puede resistirse al cash y está tan poco por el arte!
Al Blade Runner original poco daño le puede hacer cualquier intento de secuela y para muchos será un reto, pero indudablemente si hay más, el recuerdo que de ella tenemos no puede por menos que verse mezclado con los intentos, si fracasan, una pena, y si triunfan, no me lo quiero imaginar. A ver.
        
            