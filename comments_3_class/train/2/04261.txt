
                      A mi personalmente los largometrajes de Woody Allen  ni me desagradan, ni me fascinan, este no llegar a ser uno de sus mejores trabajos, aunque me gusta el modo como arremete contra el mas rancio conservadurismo que perdura en nuestros días, donde quizás se encuentren los mejores gags. A considerar el mensaje de optimismo, que proyecta a través de un personaje tan pesimista. Entretenida.
        
            