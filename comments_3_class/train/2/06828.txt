
                      Dentro del género de la comedia se encuentran subgé
neros, pequeñas variantes que distinguen los diferentes elementos que componen la totalidad de este género. Si se tiene claro este concepto y se conoce no detalladamente pero si superficialmente todo posible individuo que tiene posibilidades de participar en la película por pertenecer a la farándula puedes llegar a pasar un muy buen rato, en el que el surrealismo se apodera del guión y en el que las menciones y parodias a distintas películas interpretadas por los protagonistas despertaran la hilariedad del espectador.
        
            