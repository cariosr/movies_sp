
                      Sentimental y dulce, la película de Komandarev convence más cuando deja de lado sus estereotipos  de fábula humanista: en el registro del realismo, con el retrato de la vida cotidiana en una fábrica bajo el régimen comunista;  en escenas cruciales, cuando los jugadores de backgammon ponen su alma sobre la mesa. Por estos pequeños momentos de gracia, demasiado escasos, le será perdonado mucho  a "El mundo es grande".
        
            