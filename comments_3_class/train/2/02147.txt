
                      La Torre Oscura (2017).
"Yo no mato con la mano... mato con el corazón."
Nos encontramos ante la adaptación a la gran pantalla de una obra menor de Stephen King en plan Western fantástico (demasiado diría yo) donde ni el imponente Idris Elba logra salvarla del todo. El director parece ser que debuta en este terreno en EE.UU. después de rodar varios largometrajes en su Dinamarca natal.

Al pistolero bueno (Elba) lo acompaña en el reparto Matthew McConaughey en plan villano malo, malo. Tan malo que deja de ser creíble. Recuerda al Lucifer que encarnara Gabriel Byrne cuando se enfrentó a Schwarzenegger en El Fin de los Días, pero venido a menos.

Definitivamente, la cinta no pasará a ser la película del año o del verano, pero como toda producción de estas características está costeada y bien facturada.

Raúl Cabral.
        
            