
                      El Otro Guardaespaldas (2017).
Comedia y acción, pero no a partes iguales. Bastante más acción y violencia de la esperada. No apta para menores.

Ryan Reynolds y Samuel Leroy Jackson, (al filo de los 70 años, aunque más en forma que nunca), vuelven al terreno del humor cinematográfico con esta cinta cuyo título ya sabemos a qué nos recuerda, pero nada tiene que ver.

Casi dos horas de "leña"- como diría mi amigo Marcos- de tiros y de persecuciones. No pasará a ser la peli de acción del año, pero entretiene.

Acompañan a nuestros protagonistas Salma Hayek, en un papel de mujer fatal... por ser comedidos... Gary Oldman, como el villano despiadado y Joaquim de Almeida.

Raúl Cabral.
        
            