
                      Men in Black International (2019).
El director de la octava entrega de "A Todo Gas” nos trae la continuación de la exitosa saga que iniciaran Will Smith y Tommy Lee Jones ante las cámaras, y Barry Sonnenfeld tras ellas (quien aquí solo hace labores de producción ejecutiva).

Aunque el imponente Chris Hemsworth siempre es garantía de taquilla, cierto es que no consigue elevar el listón como para igualar al simpático 'príncipe de Bêt Lehem-Air', de quien toma el relevo. Al actor australiano le acompaña la actriz Tessa Thompson, tras haber trabajado con él en la última película del dios del Trueno. En cuanto al plantel de secundarios, podremos disfrutar de la presencia de Liam Neeson, Rebeca Ferguson ("El Gran Showman”) y Emma Thompson, quien repite papel tras aparecer en la tercera parte de “MIB".

En definitiva: más alienígenas a cual más feo y original, más acción y efectos visuales, un poco de humor y un buen número de localizaciones haciendo gala del subtítulo de la cinta (París, Londres, NY, Marruecos...), pero sin llegar a la calidad de la anterior trilogía.

Raúl Cabral.
https://www.filmaffinity.com/es/film204633.html
        
            