
                      Belén Rueda acapara todo, la gran pantalla, Ismael, y la pequeña con B&B, lleva buena racha de trabajo, y aquí vuelve a demostrar sus dotes de misterio y preocupación al lado de un Ricardo Darin, que aquí le falta un poquito de empuje. La recomiendo, porque aunque no te mantenga en vilo, si es entretenida y con sorpresas incluidas.
        
            