
                      La película es impresionante y tiene efectos muy buenos. Es muy buena respecto al efecto visual que nos ofrece y no tiene malos diálogos. La historia surreal, subjetivamente, me gusta mucho. Tenía que añadir la excepción, la cual, se carga la película: no deberían haber añadido la música de fondo todo el tiempo porque se hace muy pesado y le quita, a veces, la emoción que proporciona momentáneamente.
        
            