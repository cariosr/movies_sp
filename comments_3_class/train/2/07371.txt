
                      Se puede hacer mucho con poco. 
En esta película encontraras una buena trama, no una impresionante; pero realmente entretenida. 
Pocos personajes, buenos diálogos, un toque de tensión en los últimos minutos, son elementos que conforman este filme que, sin lugar a dudas te puede hacer pasar un buen domingo.
        
            