
                      Un monstruo viene a verme es una bonita película, pero no es para tanto como dicen los medios. Es una buena película con un buen ritmo, una historia bonita, un mundo de fantasía, buenas actuaciones, tremendos CGI y momentos duros que te llegan. Pero para nada es una película que te va a hacer llorar como anuncian en algunos sitios, es una película diferente que llama la atención, por eso se ha formado tanto revuelo. Os la recomiendo pero no vayáis con las expectativas muy altas, si no saldréis muy defraudados.
        
            