
                      Crítica de "Angry Birds"
Película que no me llamaba nada y mira que yo he jugado mucho a Angry Birds y decía… seguro que es un rollo de animación para sacar pasta a los padres de los niños y no tenía en mente verla hasta que ví una noticia que decían que iban a hacer Angry Birds 2 y me dije… si van a hacer otra no tendrá que ser tan mala y me dispuse a verla.
Es una película muy simple no tiene ningún misterio pero es entretenida, graciosa, tiene buenos personajes y si has jugado al juego, ves cosas del juego en la película, como el tirachinas, con un doblaje muy español y gracioso como son José Mota y Santiago Segura. En resumen es una película entretenida que de vez en cuando te saca una sonrisa y que no sientes que pierdes el tiempo.
        
            