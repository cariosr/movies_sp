
                      Basada en el famoso experimento del Dr Philip Zimbardo realizado en 1971 el cual consistía en recrear el ambiente carcelario para saber que repercusiones tenía en la psiquis de los prisioneros. Fueron escogidos 24 jóvenes y su papel de prisionero o guardia se asignó al azar, la única regla para participar era comprometerse totalmente con el personaje. 

La película acaba por enseñarnos el rostro del poder y como este acaba progresivamente con la humanidad. 

Dato: El experimento tenía una duración inicial de 2 semanas pero se salió de control y duró 5 días.
        
            