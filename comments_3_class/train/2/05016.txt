
                      Bajo unas apariencias burguesas, JW es en realidad un estudiante sin dinero con muchas ganas de éxito. Para financiar su ambición, frecuenta a traficantes de droga. En ello nada muy original, ya lo hemos visto un montón de veces. La ambición y la arrogancia que se queman las alas. Pero hay que reconocer que la película está dotada de una temible eficacia. Un ejercicio de estilo bien realizado, trepidante y sin demasiadas cursilerías.
        
            