
                      Una vez anunciado el estreno de esta película, y echándole un vistazo a sus protagonistas, y al argumento, pensé que no me la podía perder. Únicamente duró 1 semana en cartelera en mi ciudad, por lo que esto se preveía como un mal augurio.

Empieza la historia con diálogo inicial muy a lo Tarantino, Pulp Fiction o Django, para introducir desde unos actores desconocidos a los famosos: Colin Farrell, Woody Harrelson, Christopher Walken…

He de decir que conserva muchos toques graciosos, chocantes y de un humor negro imprevisible, y que la idea de la creación mental de los psicópatas para realizar una película, me parece original, y con mucho potencial.
Más, si lo juntamos con el absurdo de un secuestrador de perros, y un mafioso cabreado porque le han secuestrado el suyo.

Sin embargo, todo ese compendio de medidas y elementos, hacen mantener una cierta intriga, pero que no termina de enganchar, flojeando mucho más de lo esperado.
Como la historia, no se sostiene, no se consigue, se producen muchos sin sentidos, con enlaces forzados y metidos con calzador.

Total que al final se convierte en: "Cojo un poquito de Terapia Peligrosa, Pulp Fiction, Reservoir Dogs, American History X, y lo meto en la lavadora de comedia sangrienta".

Por tanto, sólo lo puedo resumir en una palabra: Decepción
        
            