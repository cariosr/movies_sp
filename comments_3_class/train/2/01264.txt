
                      La puesta en escena, al menos, en la primera parte de esta película; es decir, la explicación de como surge la propuesta para iniciar La Purga está muy bien (e incluso da en el clavo, para mi gusto, en que se desarrolle en zonas marginales y conflictivas

La segunda parte de la película no cumple a la perfección en cuanto a gore y sustos (admitamoslo al ver este tipo de películas) se refiere. Y bueno, la figura de antihéroe de Dimitri que pasa de ser un mafioso y delincuente a el "salvador" de la Purga no me convence. Y lo veo hasta innecesario.

Está entretenida en general, incluyo el vestuario y el maquillaje (no decepciona siendo habitual en la saga) que para mí, es una estrella de las 2 y media que he dado.

Un saludo.
        
            