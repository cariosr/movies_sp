
                      Hablar de Woody Allen sin levantar ampollas es complicado, más que nada porque el hombre no tiene término medio, o te encanta o le odias. Nosotros no somos fans de su cine, aunque algunas películas suyas nos han encantado ("Blue Jasmine" por ejemplo).

"Aprendiz de Gigoló" tiene una buena historia, pero unos personajes principales cansinos (ni Allen ni Turturro enganchan) y unos secundarios que gustan, aunque tiene poco peso (sobretodo las chicas).

La recomendamos, aunque sea quizá, solo para incondicionales de Allen
        
            