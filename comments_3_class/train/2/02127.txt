
                      Una historia de venganza con una inquietante sensación de preguntarte cuando llegara tal venganza, por momentos resulta exasperante. Crea una sensación hermética como otros filmes tales como enterrado (Buried) o 127 horas sin tener nada que ver con estas dos últimas.
Buenos escenarios, buena fotografía y una pelea, hombre contra oso bastante enervante, las actuaciones de Di Caprio y Hardy pueden ser lo mejor del film sin duda. 2,7/5
        
            