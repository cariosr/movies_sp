
                      Serena es un drama dirigido por Susanne Bier, directora de películas como Amor es todo lo que necesitas y Cosas que perdimos en el fuego. La película está basada en la novela de Ron Rash.

George Pemberton ( Bradley Cooper) es un empresario maderero que está pasando apuros con su empresa. Todo parece ir mejor cuando Serena Shaw (Jennifer Lawrence) aparece en su vida. Ella se convierte en su esposa y en su socia, ya que Serena es una mujer fuerte e independiente capaz de todo.
La película está ambientada a finales de los años veinte, durante la Gran Depresión, lo cual queda bien reflejado en los escenarios, los personajes y el clima en general.

Al ser George y Serena un matrimonio muy bien compenetrado, en principio se espera que gran parte de la película sea romántica, pero no es así.
Las escenas de la vida personal de la pareja son limitadas.

Los personajes no destacan por su simpatía, ni los principales ni los secundarios. Durante el transcurso de la trama todos sufren una transformación importante. La historia, junto con ellos, se va oscureciendo.

El final de este drama es bastante inesperado y algo brusco. Todo ocurre en unos pocos minutos.
        
            