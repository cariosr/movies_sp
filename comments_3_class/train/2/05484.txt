
                      Como ejercicio de encargo, a esta versión de Alicia en el País de las Maravillas se le ven las costuras "made in Disney", tan lejos del espíritu del universo de Tim Burton cuando trabaja o produce con completa libertad. El "buenismo" está presente, lejos de la oscuridad gótica del afamado director. Lo que sí es cierto es que la recreación de personajes, colorido, y esos ángulos imposibles herencia del expresionismo alemán están presentes en la película. Pero está lejos de los momentos cumbre de su trayectoria. Entretenida y poco más.
        
            