
                      Con un comienzo atractivo me fui aburriendo a medida que avanzaba la película. En el argumentario de las razones pondré en primer lugar al guión. Quizás la historia es demasiado simple y evidente. El amor se cruza en una pareja entre un músico de jazz frustrado y una aspirante a actriz. Los sueños de cada uno entran en conflicto con la relación de pareja. Y con esto acabo de explicar la película. Para mi gusto, demasiado simple. Quizás, lo que la pudiera haber hecho más atractiva, es la forma de contar esa trillada historia. Hacer un musical con ella, quizás, sea lo más acertado que podría haber pasado. Los musicales siempre funcionarán. 

     Uno de los principales problemas que vi son los puntos de giro. No son contundentes. No hay grandes sobresaltos y la película fluye como agua mansa. Pero a este joven director le seguiré el camino. Me parece inquietante lo que ha logrado en tan poco tiempo y estará muy presente en la noche de los Oscars (aunque puede ser una de las grandes derrotadas). 

      La banda sonora es de lo mejor de la película, sin llegar a destacar. Pero la canción "city of stars" es una de las verdaderas candidatas al Óscar, porque se te queda pegada y porque pocas veces una canción es parte tan importante de la trama de la historia. Lo que si me pareció muy destacable es la interpretación de los actores. Un Ryan Gosling brilla con un papel que le pega como anillo al dedo por su belleza natural. Otra cosa es Emma Stone. Me volvió a enamorar. Su recital de gestos y miradas le valió la prestigiosa Copa Volpi en el Festival de Venecia y un huequito en mi corazón. 

TWITTER: @luisalserrano
​
        
            