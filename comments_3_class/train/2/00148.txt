
                      Viudas de atracadores que planean un golpe y no mucho más. Un giro previsible en el desenlace dentro un argumento lineal, lento y sin demasiadas emociones. Solo un buen elenco para una historia que no logra hacer la diferencia. Regular.
        
            