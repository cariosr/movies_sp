
                      Una comedia amorosa con altos y bajos, tres sketches de aproximadamente 35 minutos cada uno de un humor ligero y a menudo fácil casi inocente... Gira alrededor del amor, (juventud, madurez, etc...) esta historia no vuela nunca muy alto y hace recordar el cine de los años 70... A notar la aparición de Deniro y Monica Belluci en la tercera parte (35 minutos)... Para los tres sketches las historias son simples y es la caida lo que los hace resaltar... Es italiano, es ligero, (mas bien un espumoso que un Champagne)... A usted de juzgar...
        
            