
                      Aborda la formación en el espionaje de la protagonista, sus armas de seducción para infiltrarse en los servicios de inteligencia yankee, la traición y una clásica bajada de línea en favor de Estados Unidos mostrándolo como un país garantista en contraposición a los "perversos" y "opresores" rusos. La película mantiene ritmo, sensualidad, sadismo y giros argumentales. Ideal para pasar el rato pese a la gastada propaganda pro imperialista.
        
            