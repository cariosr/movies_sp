
                      Quien pretenda encontrar en esta película una adaptación fiel del juego homónimo, que se olvide, ya que no pretende ser nada de eso.
"Prince of Persia: Las arenas del tiempo" es una película de aventuras que toma prestada la apariencia y estilo del videojuego para contar una historia completamente diferente, repleta de guiños a las espectaculares acrobacias del protagonista, y fiel cumplidora de la tradición Disney según la cual el protagonista debe ser huérfano y verse enredado en un montón de problemas que acabarán... Bueno, tendrás que verla para descubrirlo. 
Al final el producto resulta entretenido, al estilo de Piratas del Caribe y otras recientes cintas de Disney (al margen de las de animación) sin aportar ninguna novedad que le haga destacar por encima de otras producciones similares. Eso sí, hará las delicias de los aficionados al parkour/free running, con las demostraciones del príncipe Dastan durante buena parte de la película.
        
            