
                      Un día, la plataforma petrolífera donde trabaja Dawson, explota y él cae al mar a una altura de 30 metros. El accidente que debería haber acabado con el, motiva a Dawson para encontrar el verdadero propósito de su vida. Dirigida por Michael Hoffman y producida y  basada en la novela de Nicholas Sparks, hoy os traemos "Lo mejor de mí".

Durante el último año de instituto el amor surge entre Amanda Collier (Liana Liberato) y Dawson Cole (Luke Bracey) en una pequeña ciudad de Calorina del Norte. A pesar de las diferencias entre ambos, el amor les golpeará tan fuerte que creerán que son capaces de hacer cualquier cosa. Pero, una serie de acontecimientos hará que sus caminos se separen y no vuelvan a saber nada el uno del otro, por lo menos hasta pasados veintiún años.

La muerte de Tuck Holstler (Gerald McRaney), un amigo que tienen en común será la causa que les volverá a reunir, pero parece que sus vidas no son lo que una vez imaginaron. Durante el breve tiempo que pasarán juntos realizando los últimos deseos de Tuck, Amanda (Michelle Monaghan) y Dawson (James Marsden) se darán cuenta que ninguno de los dos ha llevado la vida que habían planeado. ¿Puede el amor reescribir el pasado?.

Durante las dos horas de la película iremos conociendo el pasado de los personajes a través de flashback. Conoceremos cómo fue su historia de amor, que relación tenían ambos con Tuck y que les llevó a tomar caminos separados.

A pesar de ser muy fan de las "pasteladas" románticas, Nicholas Sparks siempre es un autor que me da flojera comenzar, por lo que solo he leído un libro suyo. Aun así, nunca viene mal una película que te saque las lágrimas y eso está asegurado con el autor. La película tiene los ingredientes típicos del autor: Una pareja joven que rememora su amor, un accidente o muertos y cartas narradas con voz en off.  Y aunque tenga todos estos ingredientes que parecen funcionar perfectamente al autor, a mi ya se me hacen repetitivos, pero como buena romanticona sigo disfrutando.

La ambientación del pueblo, el vestuario de los jóvenes actores y los coches hacen que vayas directamente a 1992. La fotografía es realmente bonita. Tanto la puesta de sol como las escenas de las estrellas son preciosas, y al igual pasa con las del jardín de la casa de Tuck.

Algo que me llamó la atención y que normalmente se suele hacer es la selección de dos actores diferentes para un mismo personaje en dos edades. Y digo que me llamó la atención porque mitad de la película vemos a Liana Liberato —de películas como Stuck in love o If I stay—, como Amanda y a Luke Bracey como Dawson; y otra mitad a Michelle Monaghan —de Made of honor— y a James Marsden —de Hairspray quien sustituyó a Paul Walker— como protagonistas. A pesar de los intentos se notaba muchísimo las diferencias entre ambos actores lo que me hizo más difícil empatizar con ellos. Y creo que este y el final es el gran talón de Aquiles de la película.

El final de la película me dejó bastante fría e indiferente, es más, al entrar al cine el taquillero nos dijo "chicas no lloréis mucho que no merece la pena", añadiendo eso a el nombre de Sparks era asegurar una llorera, pero no fue así. El final es simple, predecible y para nada cuidado. Pero esto es solo en España, donde han decidido censurar el final (algo que no entiendo) y cambiarlo. Así que si evalúo el final americano, me parece completamente acorde y muy pedido por la historia. 

No la puedo juzgar como adaptación, pero dentro de las películas basadas en el escritor sin duda no llegué a conectar tanto como me pasó con El diario de Noa o Un camino para recordar. Creo que si hubiera visto desde un principio la cinta americana la puntuación hubiera llegado un poco más alto.

En definitiva Lo mejor de mí ha sido una pequeña decepción. La película es una bonita historia de amor, pero la diferencia entre los actores de ambas edades y el final que han cambiado en los cines españoles (GRACIAS CENSURA), hacen que la película cojee un poco.
        
            