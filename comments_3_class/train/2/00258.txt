
                      Ni es documental al completa ni lo deja de ser. Muestra las cosas bajo un punta de vista muy concreto y se mantiene sincera a esa idea. Tira más de la película el tema y lo que pasó en las ciudades europeas que la cinta en sí.
        
            