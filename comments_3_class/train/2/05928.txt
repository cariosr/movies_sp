
                      Entrañable película, amena e interesante, tanto del punto de vista cinematográfico como del meramente visual.
Bajo una historia aparentemente infantil y de típicas batallas entre bandas de amigos de diferentes barrios o en este caso pueblos, se desarrolla toda una historia basada en temas reales de las diferentes contiendas ocurridas durante la 2ª Guerra Mundial, a nivel de pueblos más pequeños, no las típicas ciudades siempre nombradas.
A lo largo de la trama se muestran claramente los diferentes valores que empezamos a desarrollar en la infancia y que extendemos posteriormente a la edad adulta, convirtiéndolos eso sí, de juegos a verdaderas infamias discriminatorias por rasgos sin importancia y no elegidos normalmente, contrarestados por otros más nobles de solidaridad y lucha por la libertad en los momentos en que toda pequeña diferencia sin importancia se olvida por el bien de un semejante.
En lo referente al aspecto puramente cinematográfico, se parte de una buena dirección, realzada por la labor de los actores en su casi totalidad, transmitiendo verdaderamente los sentimientos que te tocan la fibra y hacen que te integres de lleno en la historia y acompañados por una estupenda fotografía y un vestuario bien adpatado a la época narrada. 
Claramente recomendada para pasar un rato ameno y no falto de fondo.
        
            