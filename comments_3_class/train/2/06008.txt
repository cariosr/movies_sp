
                      Acción en exceso y abuso de escenas de pugna que parecen absorber la trama con el único objetivo de rellenar el tiempo. Activa en las imágenes pero muy lenta en el argumento, hasta el punto de que apenas aporta novedad a la historia y uno acaba un tanto agotado y con ansias de concluir. Aun así, se encuentra en el engranaje típico de los amantes de la saga y, con ello, no defrauda.
        
            