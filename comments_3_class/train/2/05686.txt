
                      ¡Qué viva México!

“El libro de la vida” comienza con unos niños norteamericanos que acuden con pocas ganas a un museo, donde una azafata les recibe para enseñarles la sala dedicada a México. Allí la azafata les contará la disputa por el amor de una bella joven entre un joven torero que aspira a ser cantante y un héroe local cuyas medallas dan buena prueba de su valentía, mientras La Muerte y Xibalba se apuestan entre sí sus respectivos reinos, la Tierra de los Recordados y la Tierra de los Olvidados, en función de quien consiga casarse con la chica. 

Con esta breve sinopsis, que prácticamente se nos desvela durante los diez primeros minutos de la cinta, el debutante Jorge R. Gutiérrez viene a resumir a su vez el espíritu del que supone su puesta de largo en la animación. Su debut no pretende más que acercar la cultura y folclore mexicanos del sur al norte en particular, y en ese sentido se convierte en un escaparate que consigue estrechar lazos entre culturas tan cercanas geográficamente, pero alejadas en su idiosincrasia. 

Su propuesta acierta no sólo en contarnos las tradiciones de todo un pueblo y todo lo relativo a una celebración como el Día de los Muertos, sino que a la vez da en el clavo al reírse de todos los tópicos de los gringos hacia los mexicanos, así como al poner en tela de juicio, que no condenándolas, algunas de las características fundamentales de algunos de estos acervos, como la tauromaquia. Sin embargo, comete el gran error de confundir el acercar la cultura a Estados Unidos con americanizarla, utilizando gags más propios de una propuesta que pretende abrirse un hueco entre audiencias más amplias y se olvida de que es más importante contentar a unas más modestas. Una americanización que también se evidencia en su propia banda sonora, donde encontramos versiones de éxitos del rock en anglosajón de Elvis Presley o Radiohead al más puro estilo pop, algunas cantadas por sus propios personajes.

Lo que acusa esta película es pura falta de identidad personal. Podía funcionar perfectamente gracias a su trama, sus personajes y la lección que da sobre el peso del legado, de crear nuestra propia historia y no vivir siempre a la sombra de nuestro apellido, sin necesidad de todos esos intentos de empaquetarla y venderla a un público adicto a los productos basura. Y es algo curioso considerando que su productor es Guillermo del Toro, un cineasta que ha sabido confeccionar un cine fronterizo, encontrando el perfecto equilibrio en su filmografía entre sus raíces culturales y el cine mainstream. 

Sin embargo, la propuesta de Gutiérrez compensa este grave defecto con una puesta en escena espectacular, un sentido del ritmo que la hace tremendamente entretenida y una animación de primer nivel. La historia protagonizada por estos muñecos de madera animados digitalmente divertirá a espectadores de todas las edades, pero es menos abanderada de la cultura mexicana de lo que ella misma cree ser.

A favor: el nivel de su animación, y que entretiene
En contra: el empeño por acercar la cultura mexicana al norte a base de americanizarla
        
            