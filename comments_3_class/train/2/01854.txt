
                      Gracias al cielo por Cher, ya que Streep apenas está en él y las canciones de Abba son de su colección B-side, leftovers. Al igual que los votantes de Trump, los fanáticos de esta franquicia de jukebox-musical solo ven lo bueno, a pesar de la evidencia irrefutable de lo contrario.
        
            