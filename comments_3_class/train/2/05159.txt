
                      'Election' no es muy diferente a lo visto anteriormente, pero James DeMonaco tiene la habilidad para transmitir una sensación de frescura y renovar la saga película tras película. Quizá por esa y otras muchas razones -como la presencia de Frank Grillo- uno puede sentir que se trata de la mejor entrega de 'The Purge'. El juego macabro se desarrolla a partir de una premisa muy predecible, personajes típicos y muertes tan brutales como divertidas. Acertado final de trilogía.
        
            