
                      La película es una fábula social disfrazada de farsa hilarante, el fútbol es sólo un pretexto para hacer una caricatura anticapitalista. Está llena de tópicos, tanto en los personajes como en las situaciones, su principal baza es el reparto de estrellas del cine francés, que para nosotros se reduce a Omar Sy, el resto nos pueden sonar, pero no son excesivamente conocidos. Pero la película no resulta aburrida, es simple y sencilla y eso la hace agradable, sin más pretensiones.
        
            