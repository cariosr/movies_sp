
                      Tanto esperar el estreno de esta película en España para luego llevarnos una desilusión, y eso que la vimos en una sala 3D y todo, aunque este truco tampoco es que eleve demasiados enteros la calidad de una palícula. El argumento a veces pierde conexión y al final acaba  resultando un pelín larga.
Al menos pudimos deleitarnos con el siempre maravilloso Jonnhy Deep, aunque no lucía precisamente su mejor look, pero, como siempre, su actuación es tremendamente divertida. Yo la calificaría de delirante. 
La actriz protagonista era extrañísima, sin expresión en el rostro, ni guapa, ni fea, ni nada, me desconcertó bastante.
Lo más bonito es la parte en la que cae en un jardín y el colorido de la pantalla es realmente magnífico.
A pesar de todo, los niños salieron encantados, por lo que imagino que gustará más a los pequeños de la casa.
        
            