
                      Por cuarta vez en ocho años, Johnny Depp se sumerge en su cofre del tesoro privado, un activo profesional de alto rendimiento conocido como el parque temático cinematográfico Piratas del Caribe. Depp se gana la vida como capitán Jack Sparrow, el papel que lo llevó a ser el primero de los tres nominados al Oscar. Se necesita un actor para fingir que hay algo nuevo que descubrir acerca de este rey pirata en sombras de ojos y rastas.
        
            