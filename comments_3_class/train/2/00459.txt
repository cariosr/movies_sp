
                      LOS TIMOCRÍMENES por Videns


A lo largo de la historia del cine hemos asistido a regresiones en el tiempo que invitaban a patologías de todo tipo bajo la forma de cualquier género cinematográfico que se precie. En Los Cronocrímenes, la simulación y el engaño en busca de emotividad esconden obsesiones obscuras de un director que se cayó del Nido del Cuco y perdió la noción del tiempo.

Por otro lado, la dirección actoral no convence ni al voyeur más exigente. Karra Elejalde caricaturiza a La momia de Karl Freund jugando al escondite con una eyaculada actuación de NachoVigalondo que necesita de una viagra actancial.

En lo narrativo, hay que reconocerle que el punto de partida resulta atractivo aunque tras los primeros quince minutos termine invitandote al Viaje a Ninguna Parte. Los giros en el guión son vertiginosos y absurdos desarrollando la figura de un nuevo espectador suicida emergente de una crisis de expectativas (y económica teniendo en cuenta el precio de cualquier entrada de cine).
En definitiva, estamos ante una comedia de pseudociencia-ficción inscrita en el cine de serie C homenajeando a los nostálgicos programas dobles.
        
            