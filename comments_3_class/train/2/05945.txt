
                      El acierto de la película está en el tono en el que se nos cuenta la historia. Y es que la pequeña Maisie aparece en casi todos los planos de la cinta focalizando la atención en su  punto de vista de toda esta historia. Los cuatro protagonistas adultos del film son o demasiado malos o demasiado buenos. Y es la tierna mirada de esta niña la que hace real esta historia. Su inocencia es la que por una parte nos hace compadecernos de ella y por otra, la que pone la distancia necesaria para que la película no caiga en el melodrama.
        
            