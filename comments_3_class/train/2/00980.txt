
                      Bryan Cranston y poco (casi nada) más.

Al igual que Trumbo y All the Way (producida para la TV por HBO), esta película tiene, según mi opinión, una excesiva e innecesaria duración producto de la falta de ingenio de los guionistas pero cuenta al igual que las 2 primeras mencionadas con una extraordinaria actuación de Bryan Cranston. El trabajo de este hombre justifica por sí solo ver este filme lento y carente de nervio.

 Parece ser que Breaking Bad fue la única obra en que el producto estaba a la altura de las dotes interpretativas de Cranston. Basado en hechos reales, personalmente esperaba un thriller intenso, con suspenso y tensión pero aquí el resultado es algo así como un telefilme para pasar el rato en un día de lluvia, siempre y cuando uno no tenga nada mejor que hacer.

 Insisto, vale la pena verla para disfrutar del histrionismo de un actor que casi, casi... se acerca a la altura de Jake Gyllenhaal o a Matthew McConaughey. Pero de estos mencionados el Sr. Cranston debería tomar ejemplo para elegir mejor los proyectos para los que es contratado.
        
            