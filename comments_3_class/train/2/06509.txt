
                      Tras el éxito reconocido de la serie y la primera entrega de ésta en cines, he de reconocer que al menos esta segunda parte resulta "entretenida" y logra extraer mas de una risotada y alguna que otra emoción al espectardor, pero no aporta nada nuevo a la saga ni como película es realmente destacable. 
En mi opinión, Sarah Jessica Parker nunca resultó brillante en interpretación y una vez más, aqui nos lo muestra. Se lucen muchos más personajes como el de Samantha (Kim Cattrall), o el propio"Mr Big", que en esta parte cobra más protagonismo.
Aunque también debo señalar que en ocasiones el film resulta algo pesado.
Resalta lo bien escogido de su música, sobre todo en ciertos momentos.
Resumiendo resulta cuanto menos pasable, de esas películas de las que afirmarías "se puede ver" pero no mucho más.
        
            