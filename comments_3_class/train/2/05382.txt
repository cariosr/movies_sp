
                      Realmente no comprendo las malas críticas que recibe. Claro, si la comparás con "El Conjuro 1" puede que casi ninguna esté a la altura de aquella. Pero verdaderamente logra transmitir tensión en casi todo momento y está bien desarrollada. Fiel al estilo James Wan verás intentos de posesión, sobresaltos varios, una música tétrica y una llorona que genera cierto escalofrío. Lugares y situaciones comunes que siguen dando resultado. Claramente no es la mejor de la saga pero SI está a la altura. Cumple con su cometido.
        
            