
                      Me he llevado una grata sorpresa al ver esta película, que me ha gustado bastante. Tiene un trabajo de cámara fabuloso, que no todos los directores tan aclamados fuera de nuestro país saben utilizar, una puesta en escena fabulosa y la localización de la casa es bestial, ¡da un miedo! Los actores están también muy bien, aunque la película está inacabada ¡todo para seguir con una segunda parte, que ya se ha anunciado! Me muero de ganas de verla.
        
            