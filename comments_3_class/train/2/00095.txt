
                      Un western que no pasa de correcto por tener numerosas lagunas, como algún actor poco creíble y errores de guión que no ayudan. No es de los trabajos más memorables de Hathaway y en mi opinión se queda en simplemente entretenida, lejos del gancho de los grandes filmes del Oeste.
        
            