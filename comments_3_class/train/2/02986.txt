
                      Después de verla, solo quiero la versión original de Zack Snyder. Los reshoots hacen que la película se vuelva mala en su edición y con escenas claramente innecesarias. Podríamos quejarnos del bigote de Superman, de los cromas  que usó Whedon para las regravaciones, de chistes sin gracia ninguna,... Errores, muchos errores.
Que sin ninguna duda, son momentos que se salvan gracias a la química entre los héroes, las escenas de acción y escenas de puro Snyder. 
-Lo bueno: La liga de la Justicia y ver a Sergi Constance como Zeus
-Lo no tan bueno: los claros reshoots y la duración que a mi parecer es breve
        
            