
                      Me ha defraudado la última película de Alex de la Iglesia, uno de los cineastas más capacitados de la actual generación de directores españoles. Parte de un buen planteamiento para ir descubriendo las psicosis sociales que nos toca asumir a cada uno, pero el desarrollo va perdiendo fuelle, y sin saber por qué no te acabas de creer a los personajes ni sus historias, así que todo resulta demasiado impostado, como forzado. Me ha dejado frío.
        
            