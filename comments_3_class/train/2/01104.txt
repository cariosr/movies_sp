
                      Quizás, después de leer las críticas de obra maestra, me esperaba más.
Desde el principio sabemos lo que pasa. Un enfermo terminal ( Ricardo Darín ) recibe la visita de un viejo amigo desde Canadá ( Javier Cámara ) que viene a despedirse para 4 días. A partir de ahì, vemos a dos buenos actores haciendo bien su trabajo.
Pero a mí ni me emocionó, ni me hizo reir, ni llorar, y además se me hizo lenta. 
Eso sí, el mensaje "guay" de la peli reflejado en las dos "familias" candidatas para quedarse con Truman, el perro del enfermo, ahí queda. La familia "buena" son dos lesbianas encantadoras de la muerte, con un hijo adoptado ruso. Ideal. Y la "mala" es una señora con pasta que le echa la bronca a un camarero que no se entera, que parece que lo han sacado del altiplano boliviano. O sea, una racista asquerosa.
Es lo que tienen las películas subvencionadas por el Gobierno de España, TVE, Generalitat de Catalunya, TV3, ICO, etc.
Pero vamos, que no está mal. Se deja ver.
        
            