
                      La sensación es la siguiente: McDonagh cumple como guionista pero tropieza como director. La historia de 'Siete psicópatas' es muy original e imaginativa, dotada de frescura, pero sufre un estancamiento técnico en su desarrollo, notable en alguna exagerada interpretación. El resultado es una película más que decente y un final sorprendente, aunque podría aspirar a más.
        
            