
                      Mucho bajó el listón con esta segunda secuela, aquí hay demasiada gente, sobra mucha gente. Las interpretaciones no están a la altura de la primera y salvo los efectos especiales que, sí que se aprecia un buen avance, lo demás pasa desapercibido. Sólo hay momentos puntuales en la película que merecen mención, la escena del acantilado, el cara a cara con los velociraptores y la aparición del Rex al final de la película.
        
            