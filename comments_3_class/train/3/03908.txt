
                      Esta es de esas películas que uno puede ver una tarde. Definitivamente la palabra para describirla es: Subjetiva. Todo la película trata sobre la vida de Marjane Satrapi, y tiene una buena evolución desde su perspectiva como niña hasta la adultez.
 Rebelde, alocada, y muy cruda en el aspecto bélico. Recomendable.
        
            