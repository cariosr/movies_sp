
                      Esta comedia dramática me dejó maravillada. Rebosante de carisma y simpatía, debido, en gran medida, al maravilloso trabajo de Sally Field; ya que la actriz aporta al filme su gran experiencia para sostener ella sola en sus expertas manos todo el peso de la película y regalándonos escenas realmente genuinas y cómicas.
        
            