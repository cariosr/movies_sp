
                      Probablemente la mejor película de la historia de Disney cuando las películas aun eran de dibujos animados. Una película que debería ver todo el mundo. Preciosa, emotiva y con una banda sonora de lujo.
        
            