
                      El origen de todas la pelis de terror que tenemos hoy en día. Respeto total. Lo que hace inmortal a 'Psycho', cuando muchas otras películas las hemos casi olvidado tras salir del cine, es que conecta directamente con nuestros miedos.
        
            