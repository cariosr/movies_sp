
                      Con una serie de películas en sus espaldas, "The Hunger Games: Mockingjay – Part 2" muestra la cara por toda la serie y lo hace de la mejor forma. "Mockingjay – Part 2" rompe las barreras del prodigioso Bestseller, "Harry Potter" lo hizo , "Twilight" lo hizo, ¿Por qué no usar como cebo a Katniss para atormentar a los fans locos una vez más? ¿Por qué no aumentar el capital de Lions Gate con Panem, una vez más?

Por desgracia, la emocionante saga llega a su fin, y la sabia decisión de dividir la última parte en dos películas exalta su calidad, tremendamente. Lawrence es un ser indetectable, su pequeña expresividad ayuda en su papel a la perfección, no sabes lo que va a hacer hasta que lo hace. Su ascensión es grande y gloriosa, desde que se ofrece como tributo hasta convertirse en un ídolo de la rebelión, de verdad Lawrence es Katniss de pies a cabeza.

Gracias a Gary Ross, Suzanne Collins y Francis Lawrence, la valiente historia sobre rebelión,  venganza, amor y orden social (distritos) entre Katniss Everdeen y Coriolanus Snow, quedará grabado en el alma, el corazón y la mente de cada uno de los fans. No puedes negar que gracias al auge que ha causado "The Hunger Games" diferentes sagas se han creado, muy similares en la forma de narrativa, historia y la mayoría vinculados a un orden social que se implementa luego de algún acontecimiento trágico para la humanidad. Se produjo "The Maze Runner", "The Giver"," Divergente ".

La primera "The Hunger Games" decepciono en su desarrollo con respecto al libro, por muertes rampantes pero como la saga se levantó su calidad aumento y termino en la hermosa "Mockingjay – Part 2". Sabiamente , Lawrence ha hecho guerra tanto en el Capitol como con sus demás interpretaciones, como prueba de esto, apuntó con su arco a los prestigiosos premios Oscars, Golden Globes y mas. Jennifer debe estar contenta con el resultado, ha  hecho demasiado dinero, fama y éxito, pero también hizo camino a varias nominaciones.

Las locaciones también fueron un punto clave para la conexión audiencia - película, ya que "Mockingjay" le quería dar un toque real haciendo grabaciones en Alemania como zonas de guerra o en Paris para un ambiente mas futurista.

Algunas estrellas de la película se muestran brevemente, dándonos un poco de todo el deleite de cada personaje, La presencia abreviada de Hoffman, que murió el 2 de Febrero de 2014 por una sobredosis accidental, es desgarrador. Tucci aparece demasiado breve en su papel de locutor de televisión untuosa como Caesar Flickerman. Elizabeth Banks aparece en un par de escenas con sus trajes típicamente indignantes como estilista y arribista Effie Trinket, y eso es todo.

Katniss va en serio esta vez, y de una vez por todas se toma su papel como nunca, mezclando su extrema valentía y vulnerabilidad a su peor enemigo y poner fin de una vez por todas todos a los sufrimientos de los distritos.

Las relaciones de la "Chica en Llamas", tener que elegir con cuál de los dos pretendientes pasaría el resto se su vida y demostrarse a sí misma que el amor es "verdadero o falso" fue un verdadero deleite.

Esta versión cuenta con las mejores secuencias de acción de toda la serie, nos trae una gran cantidad de aceite a un campo de minas con millones de trampas con un solo propósito "Matar a Katniss" El camino desde el distrito hasta la mansión de Snow es un viaje perfecto y peligroso que ira matando lentamente a cada uno de los personajes, hiriendo  nuestros corazones cada vez más (el viaje a través de las alcantarillas).

La parte dramática de la película hace derramar lagrimas a todos, muertes, traiciones , asesinatos, monstruos , minas , fuego, destrucción , son las causas.

Un verdadero fin  para la fantástica trilogía de Suzanne y un cuento de hadas para el real final de la película de Francis Lawrence . ¿Sabes por qué esta es la película perfecta? Cuenta con cada uno de los géneros del mundo cinematográfico drama, acción, aventura , ciencia ficción, terror, suspenso  y hasta un poco de comedia, pero lo más importante, la historia, que se desarrolla a la perfección dejando el nombre de "The Hunger Games" en la cima.

"Definitivamente mejor fin, nunca podría tenerlo".

Desde las primeras palabras de la película: "Puedes decir tu nombre, Katniss", hasta las últimas palabras de la película: "Hay juegos mucho peores" Es hermoso y totalmente entretenido.

La película que todo el mundo quiere ha llegado y se llama :
"The Hunger Games: Mockingjay - Part 2 "
        
            