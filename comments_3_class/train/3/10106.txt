
                      La mejor película que he visto y la obra que elevó a la categoría de excelente director a Christopher Nolan. La trama es fantástica, un poco compleja quizás, que implica que pongamos nuestra máxima atención en todos los detalles que van sucediendo, debido a contar dos historias paralelas con el mismo protagonista que se juntan en la parte final de la película. Guy Pearce realiza una actuación memorable, aunque ni siquiera fuese nominado al Oscar.
        
            