
                      Divertida, llena de energía y acción,  conmovedora finalmente, me gusta sobretodo la inocencia de Diana enfrentada a la raza humana con sus miserias y ambiciones que someten a la mayoría sin poder de defensa alguna, lo que la lleva a ver la realidad en su crudeza y aceptarla por sus profundas convicciones, se trata de luchar por lo que crees : excelente mensaje para grandes y chicos. La fotografía de la película es bastante buena. La recomiendo !
        
            