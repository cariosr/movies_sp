
                      "DOS DE NUESTROS TERRORES MILENARIOS integran la esencia del nuevo opus de Christopher Nolan: 1. Carecemos de control sobre nuestras vidas, y 2. La realidad tal y como la conocemos no existe. Conciliando los puzles brillantemente diseñados de Memento y El truco final y el caos y la anarquía orquestados por el Joker en El caballero oscuro -el propio Nolan es un Joker, siempre urdiendo planes para jugar con nosotros..."
        
            