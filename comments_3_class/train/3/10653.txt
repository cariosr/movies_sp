
                      Con esta película he vivido momentos de la crisis de mi país, refleja la situación crítica de muchos hogares, de personas corrientes que están cerca de nosotros. Las emociones como ingrediente llevadas a las últimas consecuencias. Sorprendente los actores y actrices españoles. Increíble música que te sujeta a la butaca esperando el desenlace. Buen cine de calidad.
        
            