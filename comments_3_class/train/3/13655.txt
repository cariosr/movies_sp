
                      Esta película te hace sentir la angustia y el dolor de los protagonistas en esos duros momentos. Sales del cine en shock y valorando lo realmente importante en la vida. Enhorabuena a esa familia tan valiente.
        
            