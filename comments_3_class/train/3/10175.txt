
                      Sorprendente, intrigante, dramática y romantica, esta película es uno de los mejores dramas románticos que he visto últimamente.
Posee gran fuerza tanto por su gran reparto como por la interpretacion de este, que hace que unos personajes que podrían haber quedado desdibujados de haber sido encarnados por otros actores, tengan una fuerza inusual y consigan transmitírsela al espectador.
Grandes clásicos y nuevos actores que no decepcionan y llegan a emocionarte inmensamente.
Con un tema que sin duda es una de las grandes lacras de nuestro tiempo, sorprende con un original final que en todo momento ves, pero no lo asimilas al llegar.
Crea intimidad y silencio, complicidad y angustia, en ocasiones llega tanto que duele.
Un film dramatico para ver en mas de una ocasion.
        
            