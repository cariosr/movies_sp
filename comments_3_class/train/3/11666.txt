
                      Película muy divertida para tus hijos, sobre todo para los menores de 7 años por el lenguaje sencillo y adecuado para esas edades, haciendo que no se pierdan y estén embobados toda la película, no como Del revés que la mayoría de los niños no se enteran bien de qué ha ido la peli. Claro está que tú como padre la notarás muy infantil, pero para quién es la película cuando vas al cine con tus hijos, para ti o para ellos? pues esta es tu peli, intensa, amena, muy colorida, con muchos trucos y animales ideal para los peques
        
            