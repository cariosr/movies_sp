
                      Esta es una película seminal, basada en clásicos más antiguos como Metropolis o Things to Come, pero que establece una visión generalizada del futuro que ha influido en las películas de ciencia ficción desde entonces.
        
            