
                      James Wan, gracias por esta joya paranormal. Una historia basada en hechos reales que te tendrá espectante y con todos los esfínteres apretados. La pareja que forman Patrick Wilson (Insidious) como Ed Warren y Vera Farmiga (El Juez) como Loraine Warren transmite una gran conexión dentro de la película. ¿MI DESCUBRIMIENTO? Pues ha sido Josh Brotherton como el agente Brad, personaje más bien secundario pero desde mi punto de vista enorme.

En realidad el argumento no es del todo fiel a la historia real pero eso no hace que The Conjuring deje de ser una de las mejores películas de terror paranormal de estos últimos años.
        
            