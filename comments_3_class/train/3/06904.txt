
                      Domingo por la tarde, primera sesión, en la sala habíamos unas diez personas.
Un largometraje que sobresale de la norma, atrevido, valiente. Cine de genero, saliendo de la corriente del cine español de hacer largometrajes sobre la guerra civil.
Sin abusos de los efectos especiales, ni decorados artificiales, ni vestuarios futuristas, y quizás sea eso lo que le dé credibilidad.
Sorprende como se puede crear el cerebro de un robot, y divierte la mascota felina del protagonista.
Interesante ver a Lluis Homar, transformado en un robot que igual te limpia la casa, que te prepara la cena.
Una película bonita y triste a la vez, con unos humanos robotizados y unos robots humanizados, donde el paisaje consigue llegar a ser otro protagonista más de la historia.
La niña Claudia Vega, se muestra como el nuevo descubrimiento del cine español.
Buena, emotiva, un cuento de ciencia ficción, te mantiene atento a la pantalla. El único fallo, que quizá sea demasiado predecible.
        
            