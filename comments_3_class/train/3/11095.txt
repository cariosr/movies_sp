
                      "Deadpool" tiene una boca muy grande y una mente muy corrompida"

Visceralmente genial, la comedia sangrienta de Marvel clasificada R ha refrescado el género de superhéroes, superando a su competidora más fuerte "Guardians of the Galaxy".

Los créditos iniciales dicen la absoluta verdad, Reynolds está catalogado como el "Idiota perfecto de Dios"; el director Tim Miller es descrito como "un instrumento pagado" y los escritores son detallados como "los verdaderos héroes aquí".

Evidentemente, la aventura está llena de sangre, bromas y temas obscenos, no desperdiciando ningún segundo para realizar uno de esos chistes. Wade genera una experiencia más íntima con el público rompiendo la cuarta pared cientos de veces. Fue genial!. Sin duda esta película lleva la etiqueta de "La primera y superior película de Marvel clasificada R" con honor, sudor, amor, calidad y muchas palabrotas.

Rhett y Paul Wernick Rheese (los verdaderos héroes) son los culpables de la creación de un historia llena de momentos humorísticos que generan una irresistible carcajada seguido para el fabuloso sarcasmo de Ryan Reynolds.

Si te ha gustado la fórmula Marvel evidenciada en "G. G.", "Deadpool" les dará una agradable sorpresa, mejorando la fórmula inminentemente con sarcasmos, mensajes indirectos y directos para sí mismo, para otros superhéroes e incluso para su propia productora.

Principalmente, el proyecto alberga dos elementos innovadores elegidos por sus escritores de forma delicada y precisa, el primero fue cómo el mismo anti - héroe expone sus orígenes y el segundo fue la ruptura constante de las cuatro paredes en la película. Pero éstos eran sólo las bases para solidificar la película.

Basado en el cómic de 1991 por Rob Liefeld y Fabian Nicieza, "Dead Pool" cuenta la historia de Wade Wilson, un terrible hombre quien recibe un pésimo diagnostico de salud, esto lo lleva a tomar decisiones aparentemente perjudiciales, haciéndole literalmente un monstruo pero dándole capacidades sobrehumanas y una inimaginable habilidad para curar ("Todos los dinosaurios le temían al T - Rex"- Wade Wilson).

El filme utiliza diferentes y constantes momentos no fundamentales para el avance de la trama, pero necesarios para mantener la estrecha relación Wade - audiencia, que incluyen a dos participantes de los "X-MEN" o a una anciana ciega la cual quiere ensamblar una repisa.

Sin embargo, hay algo que no hace muy feliz al público, la película es todo innovación y cosas nuevas pero la parcela romántica central tiende a romperse por periodos de tiempo debilitando el rápido desarrollo, pudieron elegir un tema menos cliché, ¿Una historia de amor para el día de San Valentín? ¿estrategia? ¿más dinero?.

Hace varios años, los cómicos escritores vieron la pequeña y complicada "X-MEN Origins" (2009), en donde Ryan Reynolds obtuvo una corta y vergonzosa participación, sin embargo esos pocos segundos en pantalla bastaron para inmediatamente comunicarle al joven actor que si en algún momento de sus vidas tenían la oportunidad de hacer un filme propio para el anti-héroe, el papel principal era inmediatamente para el chico. Los escritores tomaron la elección perfecta. Su sarcástico entusiasmo es lo que hace a Deadpool "Deadpool", él exuda todas las características del personaje, incluso logro plasmar la pansexualidad y homoerótica actitud del super. Este chico es elocuente, divertido, profesional, él es el ser perfecto para interpretar el anti-héroe más poco convencional del universo.

El director Tim Miller y el resto del equipo son los magníficos compañeros para adornar el trabajo del actor principal, admiro el trabajo realizado por Miller para dirigir un proyecto de tan enorme magnitud, es uno de los directores del momento y lo seguirá siendo por un tiempo más.

Es una coreografía de acción, entremezclada con una narrativa fresca y profana vertiendo los mejores comentarios del mercenario en las mas inesperadas situaciones. Esto era lo que las película de superhéroes necesitaban. Por fin, la obra maestra de Marvel.

Según la visión del espectador, el filme puede convertirse en una película de acción, en una película de terror, en una película de drama, en una película romántica, en una película de thriller, es verdaderamente polifacética. teniendo el majestuoso poder de superar su premisa y la de muchos mas.

Lamentablemente o agradablemente, no podemos decir que "Deadpool" será la ultima invención de Marvel para mayores de edad ( "Wolverine 3" al ver el éxito de su amiga, decidió cerrar la franquicia del hombre con garras de acero con una clasificación R), lo que si se puede decir es que el 'Merc con una boca' lo ha hecho excelente y en palabras textuales del super, esto se convertirá en un fu-king franquicia. Una película que desfila perfectamente entre la delgada y peligrosa línea de la comedia, los superhéroes y el fantástico mal vocabulario, defiende y da un aire fresco al género tan congestionado de los héroes.
        
            