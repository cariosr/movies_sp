
                      Neil Burger director que en su día nos sorprendiera gratamente  con “El ilusionista”, nos ofrece através de ” Sin  Límites”,un muy diferente y original Thriller  fantástico basado en la novela de Alan Glynn .
Puntualizar la buena interpretación ofrecida por el actor Bradley Cooper durante toda la cinta.
Destacar también de este filme su buena banda  sonora y perfecta puesta en escena.
Captarán nuestra atención varias escenas y detalles. Fotogramas que pasan ante nuestros ojos a alta velocidad…etc.
Sin duda el mensaje que esconde “Sin lìmites” nos atrapara y hará pensar.
Una  película  sin excesos ni altas pretensiones que, cumple sobradamente con esa función que toda medianamente buena producción ha de poseer: entretiene y os hará pasar una buena tarde de cine.
        
            