
                      Es, sin duda, la mejor película que ha visto entre este año y el pasado, junto a En Tierra Hostil y El Caballero Oscuro (me retrasé un poco porque la vi en abril 2009). Tiene escenas graciosas para todas las edades (para mayores, p.ej.: 
lo de Kenn). De hecho, me atrevería a decir que es para gente de 10 años para arriba, dado que ciertas escenas menores de esa edad no las comprenden. Una vez más, DISNEY Y PIXAR HAN MOSTRADO SU POTENCIAL. DE LUJO
        
            