
                      Inside Out, es una película que tiene algo más. No solo por la bonita forma que tiene de explicar y reflejar elcerebro humano; si no la carga psicológica que acarrea. Es una película totalmente recomendable en la que la importancia de la infancia se hace presente. Debemos concenciarnos de ello, ya que como bien explica la película, nuestras experiencias hacen de nosotros las personas que seremos a partir de ese momento. ¡Disfrutad con alegria, asco, tristeza, ira y miedo!
        
            