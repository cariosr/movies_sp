
                      La trama es simple pero eficaz y entretenida. Para los amantes del género de ciencia ficción y especialmente, de las películas apocalípticas, esta obra es una referencia. Muy buenos efectos especiales y ritmo narrativo constante y entretenido. Único fallo es que la trama es muy 'cliché' pero en mi opinión en esta película le va genial.
        
            