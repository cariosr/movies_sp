
                      Actuación 8
Dirección 10
Guión 5
Efectos visuales 10
Efectos sonoros 10

Calificación 8.4

Me parece una película muy bonita y bien editada, actuada y dirigida, pero con un guión muy aburrido y predecible. Casi me duermo durante el filme pero aún así creo que los efectos visuales y sonoros la salvan y la hacen una de las mejores películas del año.
        
            