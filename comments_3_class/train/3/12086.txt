
                      Una de las mejores películas animadas de la década. Esta película trae consigo un sinnúmero de emociones y sentimientos que pueden llegar a tocar emocionalmente al espectador. Esta película es apta para todas las edades, pero tiene un trasfondo tan general, que es especialmente dirigida a los adultos. Me encanta, buena trama, buen argumento, buenas animaciones, buena variedad de personajes, buena banda sonora, etc. Definitivamente Disney y Pixar se lucieron con esta gran producción.
        
            