
                      James McAvoy, quien interpreta al personaje central de la trama, hace un papel increíble. Sin duda ha hechos grandes papeles, pero este es uno de los que más ha demostrado que es ser un buen actor a mi parecer.

Si todavía no lo habéis hecho y os han gustado otras películas de este director o de este género, os recomiendo que la veáis.

Tierra Cero Blog
        
            