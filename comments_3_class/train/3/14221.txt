
                      Película buenísima, con mucho sentido del humor y con unas ardillas que no puedes dejar de reir cada vez que las escuchas hablar o cada vez que hacen algo. Unos personajes de los más simpáticos que nos ha dado el cine últimamente. Me gustó mucho lo cachondas que son y cuando cantan también te lo pasas realmente muy bien.
Yo le saqué una pequeño sentido a la película, no sé si era su intención transmitir esta idea, que es que, si te haces famoso con una persona humilde que te ha ayudado y te a subido a lo mas grande, no tienes que dejarla por otra que te promete el oro y el moro y que sólo quiere llenarse los bolsillos a tu costa.
        
            