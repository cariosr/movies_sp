
                      Muy buena película, sobre los abusos cometidos por curas a niños en Boston, con el encubrimiento de las fuerzas vivas de la ciudad. El director nuevo llegado al Boston Globe les encarga investigarlo, y poco a poco con muchos palos en las ruedas y muchas presiones van avanzando hasta publicarlo.
Refleja lo que de verdad debería ser el buen periodismo, y no el que hay ahora en la mayoría de los casos. 
Y el tema de los abusos lo tratan con corrección, sin entrar en demasiado detalle, cosa que agradecemos los espectadores. Pendiente de ver El Renacido, me parece la película mas redonda de cara al Oscar.
        
            