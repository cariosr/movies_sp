
                      Absolutamente todo es memorable. Cada escena, cada línea de diálogo, cada plano, cada pelea, cada disparo... 'Pulp Fiction' es una sucesión de historias que revelan cómo han de hacerse las cosas para pasar a la historia. Contiene una magia y una crudeza dignas de inspirar a más realizadores -como logró, de hecho- y transforma la estructura narrativa en un personaje más del que disfrutar. Tarantino se alía, además, con la maestría de Travolta, Jackson y Thurman.
        
            