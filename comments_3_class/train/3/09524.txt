
                      Peliculón! Una de mis 10 películas favoritas. Una película subestima y poco conocida. Injusto, ya que es una gran película y se me hace inevitable darle las 5 estrellas. La película en versión española está descatalogada, pero la he conseguido en dvd en versión española para mí colección. Muy difícil de encontrar, cuando la conseguí aún habían solo tres personas que vendían la versión española en dvd, ahora no lo sé, pero yo tengo la mía y estoy tranquilo de tenerla en dvd original versión española en mi colección. Sin duda una joya de película.
        
            