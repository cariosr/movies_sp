
                      Si bien la trilogía me emocionó de una manera que ninguna otra (a pesar que muchos dicen que es similar a los juegos del hambre, y dicen que son "fans" de la saga, déjenme decirles que no lo son porque un fan NUNCA debe comparar una obra literaria con otra y mucho menos una película), la película cumplió con mis expectativas, aunque debo decir que hay algunas cosas que cambiaron al momento de adaptar el guión (así como lo han hecho en muchos guiones de libros que pasan a ser películas) le da un toque extra y no lo hace para nada fuera de lugar. Ademas de contar con una muy buena banda sonora o soudtrack durante la película.

Lo único que le falto a esta peli para que estuviese completa es un poco mas de fuerza en la actuación y con eso me refiero a más gestos corporales a la hora de realizar una escena de acción o de amor.

Por lo demás, me encanto... ademas de estar esperando con muchas ansias la siguiente entrega de la saga y obviamente esperando que sea mejor que esta entrega y con mucha mas acción, romance y drama.
        
            