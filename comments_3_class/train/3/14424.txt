
                      Pues si, gracias a Schindler miles de generaciones judías sobrevivieron a la barbarie nazi.
En el 83 Spielberg ya tenía el encargo de llevarla al cine pero no se sentía maduro del todo para dirigirla, por lo que el proyecto pasó por las manos de muchos cineastas conocidos, Roman Polanski, Sydney Pollack y Martin Scorsese, entre otros. 10 años después Steven Spielberg decidió no buscar más y dirigir el mismo la historia más dolorosa de su pueblo, la caída del Muro de Berlín fue determinante para su decisión. Así después de terminar su Parque Jurásico, se puso a trabajar en la película que tuvo atravesada en su alma, como el mismo aseguro, porque se lo debía al mundo.

Me golpeó en la cara mi vida personal. Mi educación. Mi judaísmo. Las historias que mis abuelos me contaron sobre la Shoá. Y la vida judía fue volviendo a mi corazón. Lloraba todo el tiempo.
Steven Spielberg sobre sus emociones durante el rodaje... Grabado en blanco y negro para que ser elevado a nivel de documental atemporal como si fuera un reportaje de la época. Y porque según el director, el color embellecería la historia.
Desde su concepción nunca se imagino el alcance se su creación cinematográfica porque hasta ese entonces las historias del Holocaustos Judíos no eran rentable. Y menos mal que su empeño pudo más que cualquier avariciosa productora.

La filmación fue dura para todos según se, desde los cámaras a los extras, varias mujeres entraron en pánico y ansiedad en la grabación del momento ducha. Para mi no hay, en este caso, diferencias de trabajo actoral entre los tres principales, Neeson se transforma en el mejor vendedor, mejor adulador para salvar a cualquiera, con expresiones dulces, amables siempre educado y sobre todo galante.
Fiennes impecable de apariencia, hay veces tierno, más veces cruel, inhumano, sádico. Y Ben Kingsley es como el pepe grillo de Schindler, es su conciencia, astuto, inteligente que se hace indispensable para así poder proteger a su pueblo.

La lista de Schindler, es la historia de la humanidad, de nuestra humanidad, de como nos destruimos unos a otros pero siempre abra uno que nos de esa esperanza, en la película esa esperanza es la figura de aquella niña que en medio del gris (blanco y negro) destaca por el rojo de su vestimenta.
La lista de Schindler es un documental del salvajismo que imperó con el nacismo. Es duro de ver y de digerir porque tiene que recordarnos que ese holocausto no debe volver a pasar nunca más, ni en nuestro mundo, ni en ningún otro. Ya la puedes ver 30 veces y no esperar para la 31 porque fue real y lo real no cansa.
Si derramé alguna lágrima al verla? pues sí, al igual que apreté mis puños en momentos de impotencia, al igual que me lleve las manos a la cabeza por el impacto y asombro de las barbaries. No a la intolerancia.
En definitiva, Obra maestra.
        
            