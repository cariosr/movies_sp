
                      la verdad es que iba un poco un poco escéptica a ver esta película, pero me sorprendió favorablemente. Los efectos especiales son espectaculares y aunque resulta un tanto infantil, en general es una historia bastante entretenida. Merece la pena verla en el cine.
        
            