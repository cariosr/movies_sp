
                      El Renacido (2015). 
Una vez más América hace gala de dos de sus grandes maravillas: los hermosos paisajes (Argentina, Montana...) y sus grandes directores (Iñárritu vuelve a sorprendernos con esta pequeña obra maestra, aunque siempre con su estilo, sus primerísimos planos, su realismo, sus planos secuencia y su sobriedad). 

En esta ocasión, el realizador mejicano revisa el clásico que ya protagonizara Richard Harris espléndidamente hace décadas, y que ahora es Leo DiCaprio quien hace sus méritos para alcanzar su querido Oscar (la cinta está nominada a una docena y ha ganado 3 Globos de Oro entre otros premios). El nivel interpretativo que Dicaprio alcanza en este filme iguala a su sufrimiento en el mismo, incluyendo la hipotermia que parece ser una costumbre en ciertos dramas protagonizados por él (recordemos Titanic). 

Los efectos visuales de esta película son asombrosos, la fotografía, bellísima y sus secundarios, ideales- Tom "Camaleón" Hardy está pletórico. 
Muy recomendable, a pesar de sus dos horas y media de metraje. 

Raúl Cabral.
        
            