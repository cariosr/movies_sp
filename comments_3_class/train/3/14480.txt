
                      Casi me quedo sin verla. La habían quitado de todos los cines. Y de repente veo que la tienen en un pequeño cine de Madrid, y voy.
Y me alegro porque me ha parecido una experiencia muy diferente. El cine asiático tiene un tono distinto al occidental, y el erotismo japonés es  tan atrayente como perturbador 