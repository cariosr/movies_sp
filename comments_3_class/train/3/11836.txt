
                      Nadie como Tarantino para mostrar la violencia en el cine.

Historias cruzadas copadas de grandes estrellas. Un Bruce Willis implacable, un John Travolta en pleno auge y un Samuel Jackson predicando la palabra de Dios.

Pulp Fiction lo tiene todo para ser considerada una de las mejores películas de la historia y por ende del siglo XX.
        
            