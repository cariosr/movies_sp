
                      Este film definitivamente pasara a la historia, con un montaje muy bien cuidado, hasta los detalles mas insignificantes, un argumento increíble, junto a un muy buen guion, que quizá no se va por el lado de explicarnos cada detalle, pero ni siquiera es necesario ya que la película se explica por si sola y debo decir que encontraron el punto exacto entre cada una de sus partes, un equilibrio tan enriquecedor, que si cambiáramos la balanza de uno de ellos fácilmente se arruinaría el film.
        
            