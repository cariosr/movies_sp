
                      Una película puedes ser tan buena como esta enganche en nuestro gusto y en nuestra formación. Una película se puede distinguir por múltiples variables y ser calificada como mala, buena o extraordinaria; todos sabemos que por eso "La Academia" a través del Oscar premia actuación, fotografía, sonido, etc. Para mi gusto esta película nos entrega un tema fundamentado en las relaciones humanas, el relacionamiento, la psicología genética, "montado" en historias que sin duda alguna podrían ser de la vida real. 

Bien señala Jean Francois Vezina, en su libro "Las coincidencias necesarias", ediciones Obelisco, un libro recomendado para personas que amen saber del pasado y que deseen entender su presente y poder armar, como dice Vezania, "La novela de nuestra vida". Todos nos hemos encontrado -por casualidad- con personas que han modificado ostensiblemente nuestras vidas. "Los encuentros determinantes que señala nuestra novela personal no se crean sólo con gente real. Se producen también con ideas, con símbolos que forman parte de nuestra cultura"

"Todos nosotros hemos descubierto un libro, una música una PELÍCULA que ha marcado y en casos turbado nuestra existencia. Estos encuentros suceden en momentos cruciales y su llegada puede repercutir en los problemas personales.

Dan Folegman, escritor y director de "La Vida Misma" logra en 118 minutos "enlatar" una historia que nos muestra la conectividad "acausal". Bien nos dice  Eric Emanuel Schmitt “La lógica no es la única herramienta para abordar la realidad, que ésta puede ser a veces misteriosamente hermosa e irracional” en su libro "El visitante".

Seguramente como reza el refrán "en gustos se rompen géneros" y este genero fílmico me apasiona.

Cordialmente 

Broker_DTalento

"Nada es verdad ni mentira, todo es según la película y cómo se mira"
        
            