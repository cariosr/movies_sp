
                      Cómica con un trasfondo muy humano y a la vez "triste". Increíble la forma de dar la vuelta a un tema como la tetraplegia, y a la necesidad de alguien con ese problema de necesitar como mano derecha a alguien que no hace un drama de una situación complicada. Genial!
        
            