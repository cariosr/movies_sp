
                      Es una de esas películas que sabes como va a acabar, que sabes lo que va a pasar en cada conversación, pero aun así te emociona y te encanta, la recomiendo encarecidamente, es una película que te deja muy buen sabor de boca a salir del cine.
        
            