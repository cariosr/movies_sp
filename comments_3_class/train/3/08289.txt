
                      Para mí sentarse en el sofá y ver esta película es una auténtica gozada. John Sturges dirige este film de una manera magistral, en gran parte gracias al gran elenco de actores y sus respectivas actuaciones, destacando al gran Steve McQueen, a Donald Pleasence, James Garner o Charles Bronson entre otros. La película esta acompañada de forma admirable por la gran banda sonora de Elmer Bernstein. Y lo mejor de todo es que muestra parte de la historia mundial.
        
            