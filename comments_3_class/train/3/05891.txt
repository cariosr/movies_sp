
                      Unas atmósferas que te envuelven desde el principio gracias a un vestuario, elección de decorados y fotografía adecuados e impecables. La interpretación es sobresaliente y el guión se sostiene ante lo complicado de adaptar un clásico. Para disfrutar de lo romántico victoriano.
        
            