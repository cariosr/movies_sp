
                      Regreso exitoso. Muy distinta a la anterior en cuanto a lo argumental pero no por ello menos entretenida. El juego de por si es netamente en una jungla, en contraposición a su predecesora donde había que ir avanzando casilleros hasta alcanzar la meta. Me resultó muy divertida de principio a fin, destacadas actuaciones y grandes efectos. Ideal para verla en familia. La recomiendo.
        
            