
                      Esta película es la continuación de Kill Bill: volumen 1. Continua como la primera entrega, dirigida brillantemente por Quentin Tarantino y las interpretaciones siguen al mismo nivel que en el volumen 1. Muy buen guión, al igual que la banda sonora. Al igual que la primera parte tiene acción sin límites, tampoco podía faltar su dosis de humor negro y mucha, mucha sangre. Tarantino en estado puro.
        
            