
                      No creo que ni uno de los críticos que han opinado a la ligera sobre esta película se haya parado a hacer ciertas reflexiones sobre eventos que, como en la original, sólo aparecen cuando uno repasa con calma lo visto. Han ido a hacer la crítica de una secuela comparándola con la primera y sin fijarse en qué es lo realmente novedoso.

Ved la película, vedla con mucha mucha calma. Hay muchas cosas que no te cuenta la trama y que han de inferirse de una relevancia notable.
        
            