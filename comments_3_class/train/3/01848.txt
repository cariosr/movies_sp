
                      Un simple despiporre de principio a fin. De los mejores ratos que he pasado en un cine con una película española. Imprescindible de ver si eres de Sevilla, recomendable si eres del resto del planeta. Un 8.
        
            