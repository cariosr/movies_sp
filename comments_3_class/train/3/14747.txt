
                      Darren Aronofsky demuestra aquí ser un indudable genio.
Es desgarrante (desgarradora), intensa , una lucha constante de la conciencia que se adentra en el espectador, lo invade y angustia inmensamente.
No es desde luego nada anormal que a día de hoy ,Cisne Negro y Natalie Portman sigan en lo más alto de la taquilla del cine en España
Resulta Hipnótica en todos sus poros por sus muchas e impactantes escenas.
Una película excepcional, una rara y peculiar Obra de arte.
Portman realiza una interpretación que eclipsa todo y a todos los que la rodean, está brillante en todas y tantas formas como ocupa en este film .Pasa de lo mas hermoso, de la mas impoluta belleza, a lo mas negro, oscuro e imperfecto del alma; con una absoluta perfección (lo que la hace si duda indiscutiblemente merecedora del Oscar de la academia).
De argumento profundo, fotogramas perfectos, magistral música y coreografías bestiales que erizan la piel.
“Black Swan” no es apta para cualquier publico ya que ,no es una producción simplona que forme parte de una vena comercial y caiga en tópicos vendibles.
Una mezcla de thriller, pasión-terror y drama que se graba a fuego en la piel del público sin tocarlo.
En pocas palabras: Un cine íntimo, único.
Te dejara sin aliento.
        
            