
                      'Unforgiven' nos trae a un Clint Eastwood redimido, un hombre que dejò atras sus andanzas de ladròn y asesino. Un tipo que formò una familia en una granja y vive alejado de la ciudad.
El film nos muestra que por màs que queramos, no podemos olvidar o dejar atras nuestro pasado, siempre nos persigue. De las mejores cintas de los 90's y del genero western, infaltable.
        
            