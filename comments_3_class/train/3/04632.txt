
                      Para mí, la película está bien realizada, como casi todos los remakes, no le gana a la original, (la de George A. Romero es excelente) pero tampoco es un bodrio, las actuaciones son creíbles y tiene sus buenos momentos, en ocaciones las tomas son muy buenas (a mí me gusta mucho la fotografía que muestra un mundo post apocalíptico o desolado). En fin, si puedes ver mejor la versión original, te lo recomiendo. 
La pelicula es muy muy entretenida desde que empieza hasta que acaba. No es una obra de arte, pero me esperaba menos de ella, me sorprendió. Me gusta ese tipo de género cinematografico, y esa peli no me disgusto.
        
            