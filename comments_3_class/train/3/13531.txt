
                      Disney vuelve a sorprendernos desde su estudio (cada vez menos "secundario" ya, o bien Pixar últimamente no está en su mejor nivel, o el trabajo de Walt Disney Animation Studios mejora cada vez más) de animación. Esta vez lo hace desde la perspectiva de una ciudad muy animal y muy humana a la vez.

Me explico. Zootrópolis es una ciudad que, si bien está habitada por animales antropomórficos, refleja en muchos aspectos nuestra sociedad. El uso de la tecnología, los medios de comunicación, la influencia del poder en los personajes, los prejuicios adquiridos, etc. Todo esto, Zootrópolis nos lo presenta como una película para un público aparentemente infantil, mientras incluye un mensaje para los adultos, aquél de "las apariencias engañan" pero bien llevado a la práctica.

Qué decir de la banda sonora, la animación (sublime, el mejor nivel en Disney) y el diseño de personajes. Destacar este último, los personajes son inolvidables y la transformación antropomórfica de los animales está muy cuidada, nunca parece forzada.

En resumen, no dejes de ver esta película por su aspecto de película infantil. Detrás de las apariencias, se esconde una de las mejores películas de la historia de Disney. Según parte de la crítica americana, la mejor desde El Rey León.
        
            