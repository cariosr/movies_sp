
                      Será dificil que los más pequeños, lo que aún no han atravesado ese proceso de cambio que conocemos como la pubertad, entienda el mensaje de esta historia. Aún así reirán y se emocionarán con sus entrañables personajes. Los que disfrutamos verdaderamente de Inside Out somos los que ya en la edad adulta conocemos ese colapso en nuestro centro de control, e identificamos esos sentimientos que son a la vez tristes y alegres.  ¿Es Inside Out la mejor película de Pixar? Quizás... Lo que es seguro, es que es la más compleja y madura hasta la fecha.
        
            