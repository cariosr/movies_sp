
                      El Puente de los Espías es un buen biopic basado en hechos complicadamente reales, protagonizado por dos grandes actores que llevarán el peso de la cinta, que poco a poco va perdiendo un nivel inicial muy alto, finalizando de manera humilde que permite soltar toda la tensión que inconscientemente hemos ido acumulando.
        
            