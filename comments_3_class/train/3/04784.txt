
                      ¿Puede el sentimiento del fumeta articular una narración cinematográfica? ¿Puede una película multiplicarse sobre sí misma y florecer como un cogollo otoñal? ¿Puede una secuencia final pletórica redimir una película de todos sus errores?
        
            