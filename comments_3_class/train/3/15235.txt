
                      Puedo decir que Seven es una de mis películas favoritas sin lugar a dudas.
El mejor film de asesinos en serie. Podria destacar muchísimas cosas, los protagonistas lo bordan en sus papeles, Morgan Freeman como policía a punto de jubilarse y asqueado por todo lo que ve a su alrededor. Brad Pitt como el nuevo detective llegado a la ciudad con ganas de acción. Los secundarios no se quedan atrás, Kevin Spacey está sublime como asesino consigue dar miedo, no por su físico sino por su mentalidad, realmente perturbadora, y Gwyneth Paltrow cumple en sus escasas escenas. 

La ambientación es tal y como debía ser, una ciudad sucia, fría, con ese ambiente a delincuencia por todos lados y esos edificios abandonados rodeados de callejones interminables. Una fotografía oscura, unas escenas de crímenes escalofriantes.
La dirección artística es genial, es la perfecta representación de un entorno en el que los siete pecados capitales están siempre presentes.

Un final que ha marcado un antes y un después en la historia del cine. Es tan impresionante que puede pasar un mes de haberlo visto y aún seguir rondando por tu cabeza. Sin duda de los finales más impactantes y espectaculares que ha dado el cine.
En definitiva, Obra maestra.
        
            