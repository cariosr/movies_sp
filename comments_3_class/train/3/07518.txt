
                      Gran película de una directora poco conocida, seguramente su mayor éxito.
En el filme he tenido una impotencia descomunal al ver como una pandilla de hombres agredían física y moralmente a unas mujeres cuyo último recurso era trabajar en la mina. Y aún la siento más cuando está historia sucedió de verdad y, actualmente, aún hay mujeres que lo tienen que sufrir.
Quiero destacar dos interpretaciones: la de Charlize Theron (su segundo mejor papel, solo por detrás de "Monster") y la de Frances McDormand.
Gracias.
        
            