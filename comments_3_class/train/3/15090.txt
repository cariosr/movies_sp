
                      Esta película es una autentica maravilla. Entre al cine sin expectativas y salí completamente maravillada. Es entretenida, inspiradora, preciosa visualmente... ¡lo tiene todo! Una auténtica obra de arte. Y madre mía, ¡Qué final!
        
            