
                      La película nos relata la desventura por la que tienen que pasar un grupo de soldados británicos prisioneros de Japón, no tiene tanta acción como El Puente Sobre El Rio Kwai pero es parecida, tiene al inigualable David Bowie como protagonista y ese es motivo suficiente para verla. El guion es bueno, el montaje y la dirección de arte también, a nivel actoral, quitando a Bowie tiene actores no conocidos que resolvieron su tarea excelentemente.
        
            