
                      De la misma manera que había que haber leído a William Carlos Williams para degustar con más intensidad “Paterson” de Jim Jarmusch, para poder apreciar esta película de Terence Davies en todos sus matices y recovecos, sobre todo para poder valorar la complicada personalidad de su protagonista, se hace necesario haber leído la poesía de Emily Dickinson.
Podemos dar una vuelta por una ciudad que nos atrae, admirar sus monumentos, disfrutar sus calles recoletas o sus calles animadas, ver como sus gentes enfrentan la vida, pero seguramente hay unas razones, hay un motivo, hay una historia detrás, hay más.
Terence Davies hace otra de sus cuidadísimas películas, cargada de intensidad, de emoción, de sentimientos, con una fotografía que convierte cada fotograma en un cuadro, muchas veces de luz mágica. Los encuadres estudiadísimos reflejan muy bien el ambiente represor, costumbrista, tradicional de una sociedad puritana y autoritaria. Las largas tomas, los personajes envarados, rígidos, casi de madera, reflejan un momento de los Estados Unidos en que la libertad era cartón piedra.
En medio de ese ambiente, constrictor como una anaconda,  sobrevivió Emily Dickinson, seguramente una de las poetisas más atormentadas e infelices de la literatura universal. Nacida inteligente y con talento, de escaso atractivo y educación religiosa su alma atravesó un calvario del que no dejó otra cosa que sus poemas. Los poemas más inexplicables, misteriosos y mágicos de la poesía de todas las épocas.
¿Cuál es la pasión de la que habla el título?
No lo sabemos, pero sí sabemos que fue el motor que puso en marcha la creatividad de Dickinson. Aquello que la vida no le daba o que le sustrajo, aquello que deseaba, que inundaba su alma de tristeza y pesadumbre, que seguramente tiene que ver con la desesperación de vivir sin saber muy bien para qué. Todo un cumulo de insatisfacciones que la actriz Cynthia Nixon interpreta maravillosamente.
El cine de Terence Davies capaz de revestir de intensidad y emoción hasta el vuelo de una mosca, en esta película tenía un tema a la altura de su talento y creo que lo ha bordado. Léanse algunos poemas de Emily Dickinson, no se preocupen si no los entienden, siéntanlos, y después vean la película, verán que Terence Davies, seguramente sin tenerlo muy claro, con el corazón del artista, ha conseguido reflejar yo creo que con mucho acierto lo que debió ser la personalidad de esta escritora enigmática e inextricable. Les dejo un fragmento de uno de sus poemas:

Me fui temprano -llevándome a mi perro-
de visita al mar.
Las sirenas del sótano
salieron a mirarme
y, en el piso alto, las fragatas
tendían manos de cáñamo,
creyéndome una rata
atrapada en la arena.
No huí, a pesar de todo. Después el flujo
me llegó a los zapatos,
al delantal, al cinturón
y luego al corpiño,
como si intentara devorarme….
¿La pasión?
El cine de Terence Davies en su plenitud. Si te gusta lo disfrutas, si no, aprendes a ver cine.
        
            