
                      Maravillosa historia de amor, lujuria, engaños y, sobre todo, música, mucha música y realmente buena, todo esto mezclado con guiños constantes a 'El retrato de Dorian Gray', de Oscar Wilde. Casi todos los actores principales son conocidos (Ewan McGregor, Toni Colette, Christian Bale, Jonathan Rhys Meyers) e interpretan personajes peculiares. Muy recomendable para amantes de la música en general, y en particular del Glam Rock.
        
            