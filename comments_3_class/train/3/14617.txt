
                      Nadie se equivocó con la puntuacion alta dada a esta pelicula. Incluso a la persona menos cinéfila del mundo puede llegar a gustarle.
Una vez más grandioso el actor Ricardo Darín. Magnífica actuacion de principio a fin.
La historia es tan buena que el director se permite el lujo de contar dos historias paralelas a la vez con total maestría.
El final tambien es muy bueno, nada esperado. A su vez, y mientras se va desarrollando la película se van descifrando las claves, poco a poco. No sé realmente cuanto duró este film pero yo tuve la sensación de que fue media hora intensa de lo rapido que se me pasó. Una verdadera hazaña, porque hay muy pocas películas de las que se puede decir lo mismo. 
Para broche de oro el tema que trata del que no daré demasiadas pistas para que os podáis llevar una sopresa al final de la pelicula.
No me extraña nada los premios que ha recibido y todo lo que ha dado que hablar esta película que pasara a la historia. Muy recomendable.
        
            