
                      De todas las historias de origen de superheroes que hemos visto en el cine, Wonder Woman es una de las mejores hasta la fecha. La película tiene una historia interesante que te atrapa de principio a fin. Las actuaciones de Gal Gadot y Chris Pine son impecables. El humor y la comedia están bien equilibrados en esta pelicula, cosa quedo no todas las películas de superheroes logran últimamente. Super recomendable a los fans del personaje y de los comics, tanto viejos como nuevos.
        
            