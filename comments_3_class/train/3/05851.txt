
                      Quizás la primera entrega de la trilogía de Nolan peca de no tener un villano con más presencia como sí lo tienen las dos siguientes, pero no por ello deja de ser una película brillante que devuelve al personaje de Batman al lugar que se merece.
        
            