
                      Y el universo Marvel se adentra en el cine cómico! Los efectos especiales espectaculares, de esperar en este tipo de películas, al igual que la acción. El desarrollo de la historia y la dirección del film es muy original, propio del director. Pero lo que la hace totalmente diferente es el humor. Innumerables gags que aunque no quieras hacen que te acabes riendo de cualquier cosa. Muy recomendable.
        
            