
                      Un Steven Spielberg inteligente, algo flojo en comparación a otras super-producciones suyas, pero soprendente como siempre.
Esta vez, ha hecho un trabajo futurista y pese a que los personajes principales son robots, es una película con mucha emoción destacando las escenas finales. Es una película reflexiva y sirve para sustentar tu propia opinión sobre temas como la muerte, la vida, las emociones, un futuro próximo... Spielberg carga con la dirección y el guión y hace, una vez más, un gran trabajo.
        
            