
                      No es una película lo que van a ver , no es un documental , no es una grabación del espacio ,  lo que veran es   ARTE. La película nos dice: nos convertimos en humanos cuando aprendimos a pensar. Nuestra mente es un instrumento para entender dónde vivimos y quiénes somos. Es hora de dar un paso más allá: entender que no vivimos en un planeta sino entre las estrellas, y que no somos carne sino inteligencia.
        
            