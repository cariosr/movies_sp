
                      Una pequeña gran obra cruda y dolorosa que con los temas ya muy sobados como son las drogas, la marginalidad, el abuso o la homosexualidad consigue vestir un discurso de una fuerza y un corazón incuestionables escapando con una magnífica habilidad del discurso empalagoso del "pena , penita, pena ". Una obra que los Óscar sin que lo parezca. Una experiencia cutánea que no se puede perder y que agranda el mundo del cine. (8,5/10)
        
            