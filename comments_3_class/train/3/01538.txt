
                      Me a encantado mucho por su fantasía y sus animales fantásticos, me a parecido un poco inexpresivo el actor principal. Muy recomendable.

Todo da comienzo en Nueva York, al parecer unos sucesos con algo del mundo mágico están interfiriendo en el mundo no mágico, mientras eso sucede un mago londinense esta entrando en Nueva York para una misión especial, pero por culpa de un animal que transportaba se ve envuelto en los sucesos que en Nueva York estaban ocurriendo. Tras investigarlo y declararle culpable de lo sucedido en Nueva York el intenta averiguar que sucede con la ayuda de dos magas y un no mago. Increíble película que muestra los beneficios de ser un gran mago y los males que se encuentran en el mundo mágico. Con un final que sorprende pues el malo no es quien parece y un gran cameo que por lo menos a mi me encanto.

Pueden hacer segunda y tercera parte y muchas mas pues el mundo de los magos esta repleto de ilusión e imaginación. Muy recomendable.
        
            