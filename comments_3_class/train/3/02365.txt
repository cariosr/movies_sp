
                      Salgo feliz del cine. ¿Se puede pedir algo más? Una película que nos muestra las miserias humanas en su vertiente más hilarante. Humor grueso que milagrosamente no se hace zafio por el tono amable de la película. No dudéis en ir a verla. Si fuera americana, estarían las salas a rebosar. El uso acompasado de música, historia y gags es un vehículo que conduce la película hacia una sonrisa de bobo que ha disfrutado como un enano
        
            