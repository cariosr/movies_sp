
                      Después de leer algunas críticas tenía mis dudas de lo que iba a ver. Por enésima vez (por suerte) bajo mi punto de vista la opinión de algunos no puede estar más lejos de la realidad.
Creo que ya va siendo hora de que entendamos que muchos de nosotros vamos al cine a ver un espectáculo y a pasar un buen rato sin cuantificar el porcentaje que pueda tener con el cómic original.
Buen ritmo, buen guión, buenos efectos y buenas interpretaciones, en definitiva se trata de una película genial.
        
            