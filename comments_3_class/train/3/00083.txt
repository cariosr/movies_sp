
                      Cada verano sucede lo mismo, florecen las cintas de supuestos 'super héroes' en nuestras carteleras, y la verdad, los del equipo cada vez estamos ya un poco de vuelta de ellas, por eso, cuando este verano vimos anunciada "#Chef" pensamos que sería una buena opción para pasar un rato en el cine, y así fue. 

No os vamos a engañar, "#Chef" no es una obra maestra, ni una gran película, pero si que es una apuesta fresca y dinámica que te hace sonreír mientras la moralina de la historia va cuajando en ti. 

Si queréis ver una película y acabar con una sonrisa en la boca y una buena sensación de bienestar, esta puede ser vuestra película.
        
            