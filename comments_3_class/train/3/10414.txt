
                      Magnifica película en la que no paran de pasar cosas sobre la trama principal, no necesitan refugiarse en tramas secundarias para mantenerte atento durante todo el film. Final abierto que hace que tengas ganas de comentarla con otros espectadores de la cinta. Gran fotografía, rodaje y gran papel de los protagonistas 100% recomendable, de lo mejor del 2014.
        
            