
                      Una crítica mordaz a la perfecta Norteamérica que nos venden los medios, un homenaje a la felicidad que puede encontrarse en el seno de una familia desestructurada y a la importancia de ser uno mismo, sin importar el qué dirán.
        
            