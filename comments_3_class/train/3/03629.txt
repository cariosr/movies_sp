
                      Danzón
Esta película por María Navaro es una historia romántica la cual trata temas femeninos al igual que la cultura de el baile, en este caso el Danzón. Comienza con Julia la protagonista la cual va en busca de su pareja de baile pero descubre varios aspectos de ella misma en el trayecto a Veracruz.
La directora María Navaro hace un gran trabajo implementando el tema feminista en la película. Ella es considerada la cineasta más exitosa con cinco otras películas y catorce otras cortas. Sus temas casi siempre contienen el feminismo y la cultura,en este caso la cultura que refleja raíces mexicanas.
Maria Rojo protagoniza en esta película junto a Carmen Salinas. Estas actrices conocidas contribuyen con un toque más cultural a la película. La película compara metafóricamente el danzón con el aspecto de la vida de la mujer en el sentido en el que habla sobre el dominio del hombre en la relación y en el danzón y el aspecto sumiso de la mujer en el baile y la relación.
Recomiendo esta película para la audiencia interesada en la cultura de el danzón y el feminismo. Esta película hace un buen trabajo en implementar ambos aspectos al conjunto con la autoexploración de identidad.
        
            