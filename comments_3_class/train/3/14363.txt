
                      Sin lugar a dudas una obra maestra. La historia te atrapa, combinado con una de las mejores actuaciones de Angelina Jolie y un final con suspenso hacen que este filme sea digno de un aplauso. Esta película está llena de reflexión,suspenso,tristeza y esperanza.
        
            