
                      La mejor de la trilogía original. Con un final abierto muy arriesgado para la época y con una de las mas famosas frases de la historia del cine. Con "yo soy tu padre" todo da un vuelco y ya nada será lo mismo para los espectadores, que quedan enganchados de por vida a la familia Skywalker.
        
            