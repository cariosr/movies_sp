
                      Cuando fui a ver Jurassic Park tenía 6 años pero, aunque no recuerdo los detalles, sé que causó en mí una sensación que tantos años después no ha desaparecido. Una vez al año, más o menos, veo esta película (es algo que suelo hacer con mis favoritas), y nunca nunca me ha decepcionado. 
Se trata de una película de ciencia-ficción, y no de una aventura en la que monstruos te persiguen, como ocurre en las posteriores. No, aquí se prepara una buena historia, surgen debates sobre la ambición del hombre por querer dominar todo, y además lucrarse con ello
Los efectos especiales son increíbles. Es una película  que envejece como ninguna otra lo ha hecho.
Sus frases: +No hemos reparado en gastos; +Estate quieto, no te verá; +hahaha, no has dicho la palabra mágica! Y unas cuántas más. Esas frases pasarán a la historia. Por supuesto, la vida se abre camino.
Escenas sublimes: cuando la T-Rex va a hacer aparición, el vaso de agua temblando, la pata de la cabra en el coche... Y todo sin una banda sonora detrás que haga aumentar la tensión. No, no se necesita. 
Por todo esto, y muchas más cosas, Jurassic Park ha sido, es y será una película imprescindible para todo buen cinéfilo
        
            