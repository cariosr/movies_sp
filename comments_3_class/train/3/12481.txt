
                      No he parado de reírme en toda la película. Una hora y pico de entretenimiento puro, un magnífico guión, y unas increíbles interpretaciones. Tiene muchas cosas buenas (Carolina Bang frotándose con una escoba, el taxista, la mitología vasca, la secuencia inicial) y muy pocas malas (el final).
        
            