
                      Puede que la crítica acierte al describirla como la versión extendida de un anuncio de Freixenet, pero he de reconocer que me lo he pasado muy bien disfrutando de éste espectáculo del que sobre todo destaco las actrices. Ésas maravillosas actrices cuya presencia hacen de ésta película un fabuloso espectáculo. Y es que desde nuestra desquiciada Penélope hasta la desconocida en el mundo del cine Fergie lo hacen de maravilla. Tampoco ha de menospreciarse el trabajo del gran Daniel Day Lewis, que como siempre, lo borda.
Una película para disfrutar de principio a fin.
        
            