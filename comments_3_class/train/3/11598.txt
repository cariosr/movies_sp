
                      Buena a rabiar. Con una calma tensa impresionante y con Ryan Gosling bordando el papel. La banda sonora es espectacular y la realización magistral. Para mi Drive es una de esas películas que con los años pasará a ser uno de esos clásicos del cine que todos recordaremos.
        
            