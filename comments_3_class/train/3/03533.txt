
                      Wonder Woman (2017).
"Si ves que algo no está bien, puedes actuar o no actuar".
Alegato seudofeminista y antibelicista producido y escrito por Zack Snyder, el responsable de la extraordinaria 300, y a cuyo estilo nos recuerda las escenas de acción de la cinta.

Una semidiosa (Gal Gadot) encarnando a otra semidiosa (Wonder Woman). A nuestra impresionante protagonista le acompañan actores de la talla de Danny Huston, como uno de los principales villanos, el veterano David Thewlis o la española Elena Anaya. Y como "detrás de cada gran mujer hay un gran hombre"... Chris Pine le da la réplica estupendamente. Quienes no los veo del todo bien elegidos son el grupo de héroes mortales que le acompañan en plan amigos del Cap. América (esos me gustaron más). 

La película nos recuerda a un mix del mismo Cap. América citado antes, (por el estilo de la historia ambientada en los albores del S:XX), Thor (por aquello de la mitología donde se cuenta el origen de nuestra heroína) y cualquiera de esos personajes que por su aislada crianza mantiene la ingenuidad e inocencia de un niño (Los Gemelos Golpean Dos Veces, Cocodrilo Dundee, etc.).

La obra está muy bien facturada, la música sinfónica es muy buena y la acción, también a tenor de la inversión de la Warner, aunque pienso que sobran rayos y algunos saltos exagerados y, desde luego, 20 o 30 minutos de metraje...

Raúl Cabral.
        
            