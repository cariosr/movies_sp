
                      Una obra de arte.
La suma de una idea para la trama magnífica, unos actores de primera fila, que se salen en esta película; unos efectos especiales acojonantes, y la batuta de Nolan, dan como resultado este peliculón de 10. Recomendadísima verla y volver a verla.
        
            