
                      En busca de la Felicidad está basada en una historia real. Trata sobre la vida de Chris Gardner, un padre que lucha junto a su hijo por sobrevivir. 
Tiene varios elementos que hacen de ella una película emotiva. En primer lugar el instinto de superación, de no creerse menos que nadie, de seguir peleando a la contra, contra el infortunio y la mala suerte, de no menospreciarse y creer en uno mismo contra viento y marea. Eso es precisamente lo que hace Chris, vendedor de escaneres que un día, decide probar fortuna como corredor de bolsa, para lo cual entra haciendo prácticas en una empresa, durante un periodo de seis meses, no retribuidas, tras los cuales, de los veinte candidatos solo seleccionarán a uno para trabajar en la firma.

La mala situación económica de Chris que convive con su mujer Linda, cansada de trabajar sin recompensa y su hijo pequeño Christopher, hace que Linda cansada de esa situación y una vez que ya no cree en la capacidad de su marido para salir del socavón en el que están, decida poner tierra por medio, dejar San Francisco e irse a vivir a Nueva York.
Chris entonces comienza un peregrinaje, junto a su hijo, por el camino del infortunio, donde nada le sale bien, continuamente ve como el poco dinero que gana vendiendo escaneres, resulta insuficiente para hacer frente a los pagos de los alquileres o de las multas de tráfico, lo que incluso les llevará a recurrir como último recurso a los albergues municipales.

Esta película está relacionada con la ética ya que trata puntos como la libertad, la actitud, la superación y la felicidad, en este caso relacionada con el empleo.
Creo que nos hace reflexionar y valorar de alguna manera lo que tenemos, y nos enseña de cerca la vida que llevan miles de personas y familias que tienen que luchar día a día para llegar a fin de mes, debido a la situación económica actual.

La extraordinaria actuación de Will Smith en la piel de Chris, la hace merecedora de un Oscar, le da a su personaje el vigor, fuerza y arrojo necesario para hacernos vibrar con su odisea y experiencia vital, en especial en las escenas finales, donde sus ojos, lo dicen todo. Es imposible no emocionarse.
En definitiva, una maravilla.
        
            