
                      Te atrapa en su estética desde el primer minuto. Dura historia sobre lo duras que son las decisiones personales y como arruinan la vida a uno mismo y a los demás.
Amy Adams y Jake Gyllenhaal tan buenos como siempre. Espero que finalmente Amy Adams tenga su sexta nominación a los Oscars y se lleve el premio de una vez. Ese repertorio de miradas en primer plano se merecen todas las alabanzas.
        
            