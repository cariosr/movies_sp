
                      Tristeza e impotencia con la muerte de Ojos Azules y la esposa de César, conmoción al ver a César ser torturado por el Coronel, mantenerte al filo de tu asiento al ver a los Simios luchar con Alpha-Omega, una película de esa categoría que te haga sentir esas sensaciones, es digna de ser llamada memorable, este es el caso de War for the Planet of the Apes.
        
            