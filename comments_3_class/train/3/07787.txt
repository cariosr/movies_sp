
                      El comienzo es muy bonito, con su similitud a Harry Potter.
La historia es entretenida aunque en varias ocasiones lenta. Si no prestas atención  en  algunas escenas te pierdes debido a su tiempo tan corto en escena y la rapidez de la historia. El final un poco rebuscado. Pero por lo demás es una película divertida y también tenebrosa con un toque de misterio que centra la historia en una época siniestra y revuelta del mundo mágico.

Lo mejor: Las escenas de Persival Grave y de los animales.

Lo peor: Esos últimos cinco minutos de película rebuscados y esas pocas escenas lentas.

   9
        
            