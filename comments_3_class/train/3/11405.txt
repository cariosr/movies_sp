
                      Una película que nos muestra lo más grotesco y contradictorio del ser humano. Al estilo de un cuento de antaño, entre fábula e ingenuidad narrativa, vemos que la maldad es relativa ante los ojos de los comunes, mas no de los niños y los diablos. 
Me agrada la fotografía, la temperatura de los colores, la selección de personajes, los efectos 3d con ese estilo a pintura flamenca, y el hecho de que sea una producción europea. Felicitaciones.
        
            