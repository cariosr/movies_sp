
                      Sin ser la mejor película de Nolan ni de lejos, la precisión suiza del film en todo su esplendor, los tres intervalos de tiempo perfectamente sincronizados al ritmo de la banda sonora de Hans Zimmer, la tensión y la angustia tan realista y su siempre notable forma de rodar, hacen de Dunkerque una imprescindible del cine bélico. Esa delicada línea que puede separar el cine blockbuster de un cine más grande, la cruza con firmeza Nolan, cogiendo lo mejor de cada lado como a él le apetece, como prácticamente casi nadie sabe hacer.
        
            