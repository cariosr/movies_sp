
                      Aunque no esta a la altura de su anterior entrega, representa un cierre perfecto de una trilogía que va para la historia de la industria cinematográfica. Nolan hizo del cine de superhéroe un género importante y con este cierre se gana el respeto de todos.
        
            