
                      "El Mundo es grande" es una película divertida y conmovedora, llevada por actores auténticos y convincentes. La banda de sonido es bellísima y perfectamente adecuada. Fue la primera película búlgara que veía, y  la recomiendo calurosamente.
        
            