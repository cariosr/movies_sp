
                      Nominada a seis Oscar entre las que se encuentran Mejor Película y Mejor Guión Original, este segundo trabajo de M. Night Shyamalan marcó lo que para mi opinión sería la tendencia que llevaría en pantalla este director que con esta cinta abrió en cierto modo una forma de ver una fusión de géneros de manera muy armónica: drama, terror, suspense… En un mismo film, lo que le valió no sólo abrirse un hueco en el panorama de la industria, sino que muchos somos los que seguimos a este director desde entonces, donde se ve la evolución de su visión de este género que necesita más de este tipo de cintas.

La trama que se plantea aparte de mostrar con maestría el drama vivido por su personaje principal Cole Sear interpretado de forma excepcional por Haley Joel Osment (le valió una nominación a los Oscar como Mejor Actor de Reparto) hace que no sólo sientas la historia con empatía. Te mete de lleno en la trama con dosis de ternura y terror. Vas viviendo con este personaje cada una de las secuencias de la película hasta llegar a ponerte en su situación casi sin darte cuenta. Fue una actuación notable que le dio a la cinta ese toque de ingenuidad pérdida y destrozada por una situación agónica que para un niño es difícil de entender.

También nombro a Bruce Willis quien en su personaje me sorprendió gratamente. Siempre le había visto en cintas de otro estilo: pegando tiros, saltando de azoteas… Y aquí mostró que cuando quiere, puede hacer un papel distinto a este. Quizá en algunos momentos le vi un poco fuera de contexto probablemente por esta falta de continuidad en el género, pero no desentonó de lo que querían ver los espectadores. Esta vez el joven niño, se come a una de las estrellas del momento.

Un plus adicional es como se desarrollan los hilos de la trama. Cuando vemos la trayectoria de las películas de este director, nos damos cuenta que no da puntada sin hilo, cada escena, secuencia, personaje; esta hecho para garantizar no solo continuidad en la historia sino para que puedas ir involucrando tu cerebro en lo que estas viendo con el ritmo adecuado, sin recurrir muchas veces a lo sencillo. 

En definitiva, una película con un componente sencillo de entender pero bien plasmado. 
Un ritmo adecuado, personajes sólidos, bien expuesta, buena película.
        
            