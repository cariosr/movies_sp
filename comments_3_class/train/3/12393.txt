
                      Cuando una película te engancha desde el minuto uno hasta los mismísimos créditos se merece las 5 estrellas, y Guardianes de la Galaxia se las merece. Chris Pratt borda el papel de Star Lord, Rocket (voz otorgada por Bradley Cooper) es un personaje imposible de olvidar y Gamora una mujer que de primeras te parece malvada y sin corazón pero con el trascurso de la película ves que es una heroína. 
Los efectos son impresionantes (en mi opinión muchisimo mejores que los de Los Vengadores).
        
            