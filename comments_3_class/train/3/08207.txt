
                      Excelente película. Atrapante desde el comienzo. Cada escena te deja con más ganas de continuar viéndola. Muestra gran cantidad de valores a lo largo del filme. Deja una gran enseñanza al final. Recomendada para trabajar en las escuelas.
        
            