
                      Visualmente es preciosa. Su protagonista se aleja de los estereotipos de los personajes femeninos más clásicos y cuenta con escenas cómicas que hacen reír a pequeños y mayores. Por último, con Vaiana, Disney ha vuelto a conseguir una música digna de sus mejores películas. 

Su único fallo es la poca originalidad de su argumento principal, pero los detalles que rodean la historia hacen imposible aburrrirse.
        
            