
                      Bajo la dirección de, en este caso, Ridley Scott, la secuela de la magnífica "El Silencio de los Corderos" es un film con una trama interesante, sigue contando con el magnetismo de Anthony Hopkins en el papel de Lecter y si bien Jodie Foster ya no encarna el papel de Clarisse Starling, Julianne Moore es también solvente. Para ser una secuela está bastante bien y la intriga está conseguida, pero está un par de puntos por debajo de la original, algo menos elegante y bastante más truculenta y explícita
        
            