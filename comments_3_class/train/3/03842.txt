
                      Me encanto esta película yo sé que muchísima gente no tendrá el grado de empatía requerido para no juzgar el comportamiento socialmente inapropiado de este  padre de familia... cada personaje tiene su verdad y la defiende muy bien. Hay mucho amor y mucho dolor, los diálogos son profundos y poéticos.
        
            