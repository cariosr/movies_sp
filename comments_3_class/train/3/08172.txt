
                      La mejor película del año. Parece que Nolan no se cansa de sorprendernos con sus creaciones. En este caso nos encontramos delante de una película que derrocha emoción, belleza e intriga. Una vez más, Cristopher Nolan obtiene mi máxima aprovación.
        
            