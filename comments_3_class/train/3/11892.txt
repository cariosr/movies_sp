
                      Para los aficionados a las películas de acción y superhéroes es todo lo que puedes esperar. Efectos digitales increíbles, acción a raudales, humor, personajes diferenciados 
y escenas épicas. Esta película es IMPRESCINDIBLE si te gusta el cine de acción.
        
            