
                      Bravo por llevar al cine la historia de este luchador.  Una película que te recuerda que   hay que luchar siempre y dar gracias por lo que tenemos en la vida. Como dice al final, es un homenaje a todas esas personas que luchan por mejorar. 
Al margen de eso, es una película que me ha hecho sentir, primero agobio como si la noticia de la enfermedad me la hubieran dado a mí, he reído también con ese fantástico suegro y también llorado. 
Una peli super completa, con un Dani Rovira sensacional en este papel dramático, la envidia española que no aguanta que nadie de aquí triunfe, lo han querido lapidar con un fracaso que ha tenido en otra peli, pero estamos ante el Ton Hanks español, que exagero ? Démoles tiempo y ya veremos, genial Dani.
        
            