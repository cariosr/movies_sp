
                      Es demasiado buena. Parte de un argumento muy bien elaborado y para nada predecible. El hecho de que esté, gran parte, rodada dentro de un submarino da un punto positivo en ella. Destaco muy por encima las actuaciones de Jude Law y Ben Mendelsohn, siempre impecables.
        
            