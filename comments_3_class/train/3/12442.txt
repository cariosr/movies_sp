
                      Un verdadero homenaje a la película original. Cabe decir que siempre ha sido mi preferida y tenía algo de respeto por ver este remake. 
Estoy gratamente sorprendida por el resultado de los avances de la tecnología aplicado a este tipo de remakes. La película es muy fiel a la original (se agradece) y transmite las mismas emociones o más que la de 1994. He disfrutado como una niña.
        
            