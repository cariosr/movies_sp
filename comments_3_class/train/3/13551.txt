
                      A mi entender la película trata sobre la elección de vivir la vida en  la realidad  o en una fantasía. La fantasía de ser un superhéroe, la fantasía de estar por encima de los demás, la fantasía de ser más importante que el resto. Al personaje le resulta más fácil refugiarse en la mascara de los personajes teatrales, porque cuando uno se atreve a salir del teatro a la realidad, esta puede ser tan descarnada, que uno tiene que arreglársela en bolas, sin el disfraz de Birdman.
        
            