
                      St.  Vincent,  sensibilidad camuflada y envuelta  en papel de regalo.
Película correctísima en su forma  y en su fondo.
Actores elegidos con muchísimo tino todos ellos. 
Bill Murray en su línea, persona acabada y en las últimas que se agarra a su manera a un pequeño hilo de vida para salir de la oscuridad, no decepciona.
Naomi Watts con un papel tan arriesgado que bien podría parecer que no le encajase,  lo hace comedido y muy bien planteado,  sin llegar a irse demasiado al lado oscuro como chica de la noche que es,  la caracterización  le potencia mucho su belleza y el acento de inmigrante que ella saca la hace encantadora.
Melissa McCarthy acostumbrado a verla en papeles un poco estridentes,  en este caso sorprende por lo bien tratado que esta su personaje de madre castigada por esta dura vida y esa sensación que da de estar a punto de derrumbarse en cualquier momento le da un encanto especial.
Y Jadeen Lieberher el niño, esta encantador, la interactuación con Bill Murray es perfecta, las escenas en las que salen juntos son las más cómicas. 
Aunque el director no es muy conocido, creo que es el quien imprime en la película esa sensibilidad, camuflada de tufillo a alcoholismo e intento de destrucción  de uno mismo que tan bien refleja Bill Murray. 
Que al final es un acercamiento entre personas que se necesitan,  que aunque  son muy dispares entre ellas lo cierto es que son almas gemelas y encajan a la perfección como el Yin y el Yang.
        
            