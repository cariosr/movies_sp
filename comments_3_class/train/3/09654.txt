
                      Me encantó. Los fotogramas son perfectos al igual que los decorados y el vestuario.
Yo leí los libros y me la imaginaba exactamente igual que la protagonista de esta película.
La escena del jacuzzi ha sido más corta de lo que esperaba pero se asemeja a la novela.
En general, es una película buena para ver una noche. Te quedas con buen sabor de boca.
        
            