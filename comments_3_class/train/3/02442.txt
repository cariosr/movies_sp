
                      De la comedias más divertidas con las que me he topado. El humor se divisa en cada escena combinado con ápices de reflexiones sobre los valores de un hombre con respecto a las mujeres y al amor. Y ya se sabe qué triunfa, finalmente, la ética resurge y la superficialidad sumerge.
        
            