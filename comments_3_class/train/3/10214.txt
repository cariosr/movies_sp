
                      Película histórica (primer Oscar del cine español) y magistral. Garci homenajea a una verdadera generación interrumpida por la Guerra Civil, hombres y mujeres que tuvieron que renunciar a sus vidas y que quedaron marcadas por el terrible conflicto. 
Gijón se muestra en todo su esplendor y una parte muy importante de la preciosa Asturias. 
Es una de esas películas que marcan y que de alguna manera te hacen sentirte orgulloso de la forma con que los españoles supimos mirar al futuro y darnos un marco constitucional donde todos cabíamos. 
La interpretación de Pepe Bódalo y Antonio Ferrandis deberían mostrarse en las escuelas de interpretación, ambos actores teatrales crean una atmósfera única. Esa escena donde le comunica a su amigo el motivo de su viaje es emocionante y uno de los mejores exponentes de la gran calidad de los actores de entonces.
José Luis Garci crea una historia creíble y inolvidable.
        
            