
                      Sin superar a la primera película, estos guardianes vuelven por todo lo alto con incorporaciones de personajes muy pintorescos y varios cameos que, al menos a mi, me sacaron una sonrisa.

Sigue siendo divertida, sigue siendo un espectáculo visual, pero ¿por que no llega a ser como la primera? Pues bien, la acción en esta secuela esta demasiado exagerada, sacándome de la película. La banda sonora no está a la altura, me quedaría únicamente con dos o tres temas. Le meten más humor forzado, sin demasiada gracia. Y me ha faltado ver algo de conexión con el universo de Marvel de cara a Infnity War.

También repite el error de buscar la gracia en el clímax del combate final, cuando mas tensa es la situación.

Lo mejor: Los créditos iniciales, los 15 minutos finales y por su puesto Baby Groot, con todo el merchandising que tendrá detrás.
Lo menos mejor: exceso de humor sin gracia, acción exagerada y el segundo acto.
        
            