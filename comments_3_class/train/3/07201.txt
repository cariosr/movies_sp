
                      Si por algo se caracteriza el trabajo de Ang Lee es por su extrema sensibilidad y delicadeza. Incluso dentro del cine de superhéroes o de artes marciales, Ang Lee siempre ha sabido buscar el drama humano y transmitirlo de la forma más natural y cercana para el espectador. 'La vida de Pi' confirma que el director taiwanés es capaz de contar cualquier género de historia bajo el prisma más íntimo y humano que el cine puede ofrecer. No es de extrañar que finalmente Lee fuese el escogido para la adaptación a la gran pantalla de la novela de Yann Martel, de la que incluso llegó a decirse que era imposible de realizar.

'La vida de Pi' es un todo un hito, un milagro cinematográfico. Una proeza visual que traspasa la pantalla y sumerge al espectador en un bello pero desgarrador viaje de supervivencia y fe. Majestuosos y poéticos planos llenos de vida, que evocan al mejor Malick de 'El árbol de la vida', se suceden a lo largo de una película en la que no sobra absolutamente nada.


Ang Lee emplea la tecnología tridimensional, no como una mera herramienta de esplendor visual, sino como una pieza esencial y necesaria para transmitir ese tono casi onírico de una historia que respira realismo mágico desde el principio hasta el final. Pocas veces el incremento del precio de la entrada de cine ha merecido tanto la pena.

'La vida de Pi' es también todo un logro narrativo. Las secuencias se suceden con un sobresaliente montaje sin que en ningún momento se resienta su extenso metraje.

El guión de la película es excelente, con un empleo medido y muy acertado de la voz en off. Por otra parte, todo el desconocido reparto está espléndido, destacando sobretodo su protagonista Suraj Sharma quien consigue llevar sobre sus hombros de manera triunfal el peso de la película. La recreación digital de los animales también resulta más que brillante y realista.


'La vida de Pi' habla tanto de la fe y la esperanza como de la amistad, de la naturaleza y del respeto por la vida, ya sea humana o animal. Aunque la temática religiosa impere en la historia, no pretende adoctrinar, tal y como su comienzo pueda hacer sospechar. Todo lo contrario, la película sugiere más que afirma y deja a la libre interpretación del espectador la valoración de los asombrosos hechos narrados.

Una indiscutible obra maestra que merece la pena contemplar en la gran pantalla. Inolvidable, hipnótica, cautivadora, emocionante y de potente mensaje.
        
            