
                      Sin duda es la película del año y de nuestro tiempo. Da por finalizada una saga perfecta y espectacular que ha cautivado al mundo, este desenlace ha dado el perfecto final para los personajes. La música es perfecta en cada escena. Las partes de accion han sido ESPECTACULARES, es un no parar hasta dejarte al borde de la butaca. MUY FIEL al libro, con respecto al oscuro ambiente y a los guiones y tal. Por mi, la mejor entrega de la saga y la mejor saga de la historia.
        
            