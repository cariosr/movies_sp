
                      El mensaje de esta historia autobiográfica es potente, Jennifer  Fox logra impactar al espectador con sus recuerdos. Recuerdos con los que juega muy bien durante el desarrollo de su película, reviviendo, distorsionando, corrigiendo sus propias rememoraciones, interpelándo a su propio preconsciente, lo que resulta en una experiencia tan real, tan humana que permite empatizar con el abanico de emociones de va viviendo la protagonista. Sin dudas, que una gran nominada a los Oscar
        
            