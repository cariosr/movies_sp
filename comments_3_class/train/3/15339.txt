
                      Maravillosa mezcla de stop motion con el universo de Tim Burton en el que la banda sonora es ya parte de la historia universal del cine. Personajes carismáticos, una aventura ciertamente original y adictiva, que gusta tanto a niñ@s como a mayores, y el inicio de un tipo de animación que marcó época. Sencillamente estupenda y parada obligatoria cada Halloween o cada Navidad.
        
            