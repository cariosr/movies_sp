
                      La vida de Adèle es una fantástica película que nos transmite realismo en estado puro. La historia refleja con perfección y gran  realismo las distintas fases y procesos de la relación entre dos personas que se gustan, desean y aman. Cada mirada, cada caricia, cada beso, cada pausa, transmiten al espectador un sentimiento, excitación, ilusión, desesperación y ansiedad.

La química entre las dos actrices es tal que realmente parece que no estén actuando. Un auténtico chorro de pasión y sensibilidad, nos ha hecho captar cada estímulo, que se palpa en cada escena, el sexo es explícito, perfecto para adentrarte mas en la historia y mantener al espectador incomodo, boquiabierto o simplemente pegado a su butaca, disfrutando.
Adéle con esa mirada perdida, esa sonrisa inocente a la vez que picarona, desprende sensualidad por los cinco costados, con una fotografía a tener en cuenta, quizás el único pero es el excesivo metraje sobre todo en la primera parte de la historia, considero que podía haberse acortado, actuaciones excepcionales para esta joya.
 
En definitiva, es una historia sencilla con un desarrollo tan perfecto, tan cuidado y unas interpretaciones soberbias que son altamente destacables, por poner un pero el primer cuarto del film transcurre demasiado lento, hasta que sin darte cuenta te encuentras totalmente hipnotizado y cautivado por el enorme magnetismo que hay entre las dos actrices.
        
            