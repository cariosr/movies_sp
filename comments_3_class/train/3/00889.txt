
                      Incencies se trata de un film en el cual el eje central de la acción son los problemas de consanguinidad, acompañado de una crítica a el absurdo de la guerra y la reivindicación de la memoria histórica. Sin embargo, la película no rehusa a la corriente principal hayada en muchos otros films, la de atrapar al espectador en una ficción de estructura convencional, que funciona, como es el giro de guión y el impacto emocional.

Denis Villeneuve nos regala un grito de dolor y de amor que se graba a fuego en el corazón del espectador gracias a su enorme intensidad narrativa. Preparense para ver un drama que lo tiene todo para hacerles emocionarse: violencia, guerra, niños, familia y una protagonista con una valentía admirable y un corazón que no le cabe en el pecho. En realidad, lo que nos cuenta Incendies es un culebrón auténtico, con el valor añadido de estar basada seguramente en miles de historias similares nacidas al amparo de cualquier guerra. 

El valor de Incendies es contar la historia de Nawal con una emoción desbordada que agarra al espectador por la garganta y no lo suelta hasta el final de la película, o incluso después, en un viaje doloroso pero lleno de belleza también.
Acierta también Villeneuve al escoger a los actores, todos maravillosos, y muy especialmente Lubna Azabal, que perfectamente podría haber estado nominada en los Oscar. 
En definitiva, muy buena película.
        
            