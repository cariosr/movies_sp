
                      Título: Gravity
Director: Alfonso Cuarón
Reparto: Sandra Bullock, George Clooney
Duración: 90 minutos
Nacionalidad: Estados Unidos
Fecha de estreno: 4/10/2013
Música: Steven Price
Fotografía: Emmanuel Lubezki
Guión: Alfonso Cuarón, Jonás Cuarón
Productora: Warner Bros/Esperanto Filmoj/Heyday Films
Género: Ciencia Ficción

Titular:
En su séptima película como director el mexicano Alfonso Cuarón sube el listón en un tour de forcé  técnico, emocional y atmosférico.

Crítica:
Siete años después de su anterior película [Hijos de los hombres. 2006] llega a las pantallas la última película de Alfonso Cuarón, una cinta catalogada como ciencia ficción, aunque no hay villano, no hay robots ni monstruos, no hay persecuciones, disparos ni giros de guión, por lo que para mí es simplemente un drama humano ambientado en el espacio. Estamos ante una historia que podría ser perfectamente real, un relato experiencial sobre la superación humana al estilo de “Lo imposible”. 
Cuando Cuarón rodó su primera película [Sólo con tu pareja. 1991] no podía imaginar hasta dónde podría llevarle su pasión y versatilidad como cineasta. Alfonso nació en México el 28 de Noviembre de 1961 y forma, junto a Guillermo del Toro y Alejandro González Iñárritu el triunvirato más prometedor de la cinematografía de su país, tridente que fundó conjuntamente la productora Cha cha cha.
Cuando Alfonso terminó la redacción del guión junto a su hijo Jonás, se dio cuenta de que no tenía la suficiente tecnología para rodar lo que había escrito, así que se puso en contacto con uno de los directores más poderosos de Hollywood, James Cameron, quien tuvo que interceder para dotar a Cuarón de los elementos necesarios para narrar su historia. Tal vez por esa colaboración técnica Cameron en declaraciones hechas a los medios haya manifestado que la película Gravity “es la mejor película jamás filmada sobre el espacio”, opinión que para nada corroboro.
Ya en sus inicios Cuarón demostró sentirse cómodo rodando en plano-secuencia, ejemplos son [Y tu mamá también. 2001],[Harry Potter y el prisionero de Azkaban. 2004] y [Paris, je t`aime. 2005] capítulo para televisión rodado íntegramente en plano-secuencia. Gravity también cuenta con esa marca de la casa y comienza con un elegante plano-secuencia de 16 minutos de duración, lo que ayuda al espectador a adquirir la dinámica y densidad de los protagonistas fluctuando lentamente ante sus ojos.
Los protagonistas de la película son actores mediáticos y carismáticos, factor del que podría haber prescindido sin duda Cuarón, ya que la historia se sujeta por una dinámica constante de acontecimientos y no por escenas de carga interpretativa. Matt Kowalsky (George Clooney) es un veterano astronauta en su último paseo estelar, y Ryan Stone (Sandra Bullock) es una brillante ingeniera en su primera misión espacial, ambos están realizando unas tareas rutinarias a 600 kilómetros de la Tierra acompañados por otro astronauta cuando sufren un fulminante barrido de basura espacial. A partir de ahí comienza un espectáculo de peligro y situaciones extremas que requiere un gran esfuerzo físico por parte de los actores. 
Los planos subjetivos de la heroína protagonista, el deslizamiento de la cámara emulando los movimientos en gravedad cero, el absoluto realismo, la consecución de la historia en tiempo real, la utilización de la banda sonora como mera ambientación y un hermético guión que no permite la entrada de ningún virus que lo desvíe de su cauce, convierten a Gravity en un espectáculo visual y visceral que se aleja de las producciones metafísicas del género, tipo [2001: una odisea espacial. 1969] -que sigue siendo la cima inalcanzable- como también de las comerciales como por ejemplo [La Guerra de las Galaxias. 1977].
El momento en que los protagonistas se separan de la nave y entre sí y quedan suspendidos a la deriva en el espacio, recuerda a la escena tan emotiva que rodó Brian De Palma en [Misión a Marte. 2000]. Como también, otro momento evocador de la cinta, cuando Bullock sueña el regreso de Clooney, es totalmente el realismo de Gabriel García Márquez en cualquiera de sus obras, como por ejemplo [Relato de un náufrago. 1970], historia que –por otra parte- tiene muchas más connotaciones con la película que su citado realismo.
Lo mejor, que no sea del todo previsible, que con los “mínimos” elementos se cree tensión, se mantenga y no aburra durante los 90 minutos.
Lo peor, que no trascienda, que la historia no termine de hacer pie, que no haya mensaje ni metáfora, sólo descripción y efectismo.
Lubezki, el director de fotografía es un habitual de Terrence Malick, un experimentado profesional que tiene la oportunidad de lucirse en ésta cinta en escenas como la de la lágrima de Bullock que gravita hasta enfocarse en primer plano, o la secuencia en la que la protagonista trata de desenganchar el paracaídas de su módulo.
A pesar de que la crítica americana encumbra unánimemente las virtudes de ésta cinta, para el que suscribe estas palabras no es redonda, no alcanza la majestuosidad de una obra maestra aunque sí cotas de buen cine. 

                                    José Antonio Olmedo López-Amor
        
            