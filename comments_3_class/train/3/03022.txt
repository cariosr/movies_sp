
                      Es una película muy buena y, sobre todo, que mide bien los tiempos. No tiene ni inicio lento ni tampoco final lento. Quizá la parte intermedia antes del final un poco más lenta pero vaya que es una película muy buena. Aunque no supera a las de la saga de Marvel, esta sí es digna de estar a su altura.
        
            