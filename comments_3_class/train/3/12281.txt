
                      Una película simplemente hermosa, una historia complicada, llena de altibajos emocionales para el espectador y rebosante de amor francés que todos amamos, Marion Cotillard y Guillaume Canet forman un duo espectacular. El guion es impredecible, nunca sabes lo que se viene a continuación, como en la escena del casamiento, esto es fantástico porque como dice el director argentino Juan Campanella, los personajes de una película no deben tener paz hasta que esta se termine. Cerca del final te deja al borde de la silla y comiéndote las uñas.
Llevando por un rato afuera la dupla Cotillard-Canet, los actores son muy buenos, interpretan casi a la perfeccion a sus personajes.
Mi visión como director: Yo tal vez hubiese alargado mas la trama mostrando mas momentos felices entre ellos haciendo énfasis en la relación, a mi personalmente me gusta ver todo el amor que se pueda.
Es una película que te va a dejar con una sonrisa en la cara y que cuando terrmines de verla te va a dejar esa sensación que solo el cine francés puede ofrecer.
        
            