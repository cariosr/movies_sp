
                      Si hubiera que haber hecho un casting para escoger director para Ready Player One, pocos habrían alcanzado la final. Así que no es de extrañar que el mismísimo Spielberg se haya puesto a los mandos del proyecto que está a punto de ver la luz.

Ready Player One es su regreso a la ciencia ficción en el cine, doce años después de La guerra de los mundos. Lo hará con una temática que está inundada de referencias a unos años 80 que Spielberg conoce bien. No en vano varias de las referencias del libro tienen a trabajos suyos como protagonistas. Un maestro del cine de los 80 para una película con esas referencias parece un seguro de vida.
        
            