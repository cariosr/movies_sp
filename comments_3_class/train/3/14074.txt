
                      Excelente película con puro realismo, extraordinaria interpretación de los actores, super bien ambientada y con un guión estupendo. De lo mejor visto últimamente, será un clásico con el paso de los años.
        
            