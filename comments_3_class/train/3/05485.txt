
                      Basada en hechos reales, es una historia tierna, divertida y con alma, con un mensaje social que cala a través de unos personajes que deben aprender a dejar sus prejuicios atrás para unir fuerzas y luchar por una causa común. A pesar de algunos clichés del género, 'Pride' logra emocionar y encandilar, en gran parte gracias al carisma de sus personajes y, por supuesto, de los actores que los encarnan.
        
            