
                      La última escena de El Padrino muestra a Kay saliendo de la habitación, después de que Michael le miente, negando cualquier participación en el asesinato de Carlo, el marido de Connie. De pie afuera, ella observa cómo el nuevo Don tiene corte. Es significativo que veamos los procedimientos en un enfoque largo y profundo desde el punto de vista de Kay, el forastero de WASPish. Cuando comienzan los tratos, la puerta está firmemente cerrada, y el último disparo es un primer plano de Kay, preguntándose ...

El Padrino II parte presenta dramas paralelos. Una parte narrativa, la secuela, se centra en la historia de 1958 de Michael Corleone (Al Pacino), la familia del crimen del Don de Corleone, que protege sus negocios familiares después de un atentado contra su vida.

La otra hebra narrativa, la precuela, cubre el viaje físico y mental de su padre, Vito Corleone (Robert De Niro), desde su angustiosa infancia escapada de Sicilia en 1901 hasta la desesperada fundación de su empresa familiar en la ciudad de Nueva York.
        
            