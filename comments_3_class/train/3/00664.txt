
                      Interesante historia ambientada en la época posterior a la II Guerra Mundial en la que se retrata el despertar sexual de un adolescente y su relación con una mujer 25 años mayor en esa Alemania luchando por volver a la normalidad. Kate Winslet está estupenda (obtuvo numerosos galardones, incluídos Globo de Oro y Óscar), y tanto David Kross en el papel protagonista adolescente, como Ralph Fiennes interpretándolo en su madurez están también estupendos. Mencionar también algunos secundarios soberbios como el desaparecido Bruno Ganz
        
            