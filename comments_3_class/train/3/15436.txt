
                      Sin lugar a dudas es la mejor película de súper Heroínas. La historia, la trama, las actuaciones y la secuencia es casi perfecta. Cabe resaltar que la actuación de Gal Gadot es magistral. Wonder Woman te hará soñar y vivir una fantasía dónde lo real y lo sobrenatural se conjuntan para dejarte un buen sabor de boca.
        
            