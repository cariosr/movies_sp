
                      Grandiosa. Empieza como la típica película bélica pastelosa y tienes miedo, miedo de otro Pearl Harbour en el que el almíbar pesa más que las grandilocuentes imágenes de los bombardeos.

Pero es este caso no es así. La historia no sufre de ser excesiva en nada. Tiene la cantidad justa de humor, de romance, de tensión y de sufrimiento para ser la película perfecta.

No soy capaz a borrar de la cabeza el muro de Okynawa por donde subían a la línea de batalla con los bombardeos detrás....
        
            