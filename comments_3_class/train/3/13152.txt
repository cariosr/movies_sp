
                      Una película preciosa, muy espiritual y que para mi sorpresa muestra una muy posible realidad que la mayoría no queremos reconocer, que la vida no acaba y que todo tiene sentido en la Tierra, aunque de primeras no se lo encontremos. :)
        
            