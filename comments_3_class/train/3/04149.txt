
                      Nostalgia. Esta es la mejor palabra para definir esta película. Al menos su primera parte ya que la segunda es una completa cinta de acción que no tiene nada que envidiar a tantas otras. El final es predecible pero es una buena película con una gran banda sonora a manos de Hans Zimmer y protagonizada por el tema principal de Terminator.
        
            