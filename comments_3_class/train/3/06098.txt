
                      El desmadre de Scorsese. Esta frase define este film, el qual nos lleva en la parte oscura de Wall Street. Un Dicaprio con una buena actuación pero que no me destacara como sus mejor actuación de todas. El inicio de la película es de lo más desenfrenado que hay en el cine. Un Jonah Hill totalmente divertido y con una actuación totalmente creible. El film más electrizante de Scorsese digna de nombrar en su filmografía.
        
            