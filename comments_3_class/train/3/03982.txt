
                      Una obra maestra de culto eterna en el mundo cinematográfico. Una historia hilarante con un gran mensaje, un espectáculo visual lleno de efectos especiales de alto nivel. Kubrick con esta cinta demostró a ser un genio.
        
            