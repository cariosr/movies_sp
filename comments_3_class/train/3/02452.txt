
                      A todos en algún momento nos han intentado estafar con las falsas empresas piramidales. Esos trabajos en los que tienes que ser autónomo, invertir muchísimo dinero y en el que te prometen que el dinero volverá a ti multiplicado por diez. Bien, pues por fin una película (¡española!) habla abiertamente de este tema parodiando estas situaciones y riéndose de ellas al máximo.

Todo mezclado con unos personajes perdedores, ansiosos de felicidad gratuita y tan ingenuos como torpes. Personajes de la calle, que son timados cómo podríamos ser muchos y que aprenden a reírse de sí mismos. Llevados con unas interpretaciones magistrales por unas actrices tan distintas como divertidas. Alexandra Jiménez, reivindicando una vez más, su puesto de (merecida) ‘reina de la comedia’. Cristina Castaño, intentando quitarse su sino de personaje popular de televisión, dando un nuevo salto al cine con un papel protagónico que borda. Y una Victoria Abril que, interpretándose a sí misma, se ríe del personaje que ella ha creado fuera de las pantallas, para explotarlo dentro de una pantalla.

Guiños y más guiños al mundo del cine y del espectáculo y un guión lleno de frases que todos hemos comentado con alguien en algún momento de nuestra vida, que se impregnan del vocablo de calle, de barrio, de Móstoles. Ese municipio madrileño que a todos nos recuerda a Martes y 13 y que en esta película es explotado hasta la carcajada. Gracias, en parte, a Las Supremas de Móstoles, que también tienen papelito en la película y en la banda sonora. Y por supuesto, a un plantel de cameos que alegran la vista.

“Nacida para ganar” es divertida, muy divertida, pero sobre todo es de esas películas que son tan agradables de ver, tan cercanas al espectador y tan fáciles de disfrutar, que no quieres que dure solo una hora y media. Es de esas películas, que te encantaría que en realidad fueran una serie de televisión. Por supuesto, con las mismas actrices que la película.

Lo mejor: lo campechana que es.
Lo peor: que no tengamos a un secundario de lujo del tipo Loles León o Rossy de Palma, pegando berridos.
        
            