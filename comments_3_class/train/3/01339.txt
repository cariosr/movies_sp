
                      Uno de esos casos en los que una gran película lo es porque está basada en una gran historia. El mérito es de Victor Hugo y de sus Miserables, si cogemos una historia así, la adaptamos al cine y 
le sumamos un elenco como el que aquí vemos, no necesita mucho más para dar un buen resultado.
        
            