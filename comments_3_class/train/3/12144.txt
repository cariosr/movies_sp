
                      Para mí la mejor película de Marvel hasta la fecha. En comparación con "El Primer Vengador" la película es mucho más entretenida, hay más escenas de acción, la relación entre Steve y Natasha es increíble y te quedas con ganas de más, de saber como será su siguiente encuentro con Bucky.
        
            