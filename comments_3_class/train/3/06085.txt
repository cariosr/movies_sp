
                      Probablemente parezca que llamar a Furious 7 una película familiar. Pero qué demonios es, la familia son las audiencias que se han quedado con esta franquicia de auto-porno, desde el primer capítulo, en 2001. Y la familia de actores y equipo que se han reunido aquí para rendir homenaje a uno de los suyos

Ese sería Paul Walker, quien interpreta a Brian O'Connor, el policía encubierto que se infiltró en una banda de proscritos de los corredores callejeros de East L.A., dirigida por Dominic Toretto (Vin Diesel), y se quedó para formar un vínculo. Walker, de 40 años, murió en un accidente automovilístico a alta velocidad el 30 de noviembre de 2013, en el asiento del pasajero de un Porsche Carrera GT 2005 que golpeó árboles y una farola de concreto.
        
            