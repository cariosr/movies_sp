
                      Llega 'Lo imposible' con impresionantes efectos especiales y una sobrecogedora historia basada en los hechos reales de una familia. Sin duda, hay que destacar la excelente interpretación de los actores y el trabajo que ha realizado su director, Bayona, para transmitir al espectador los sentimientos de una familia por luchar, sobrevivir y reencontrarse. Una verdadera lucha entre la vida y la muerte, lo humano y la naturaleza en pleno estado puro y sobre todo, un gran mensaje: La esperanza.
        
            