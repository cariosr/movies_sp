
                      El Desafío es una historia espectacular, emotiva y vertiginosa que Robert Zemeckis recupera y nos presenta en formato IMAX® y 3D para regalarnos una experiencia visual y sensorial única con la que vuelve a subir el listón de sus anteriores éxitos. Y en esta ocasión lo logra con un relato tan sencillo como real, el de un joven que descubre muy pronto su auténtica pasión, el funambulismo, y desde ese momento todas sus decisiones lo conducirán a culminar la mayor actuación jamás vista y alcanzar un sueño: pasearse por las nubes sobre un cable colgado entre las Torres Gemelas, a más de 400 metros de altura.
Lee la crítica completa en el siguiente enlace:
        
            