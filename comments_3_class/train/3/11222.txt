
                      Preciosa película, llena de alma y buenos sentimientos. Es un subidón de vitalidad y tiene algunas escenas que se van a quedar en la memoria de la gente. Mucha atención a las interpretaciones, porque son de las que se recuerdan.
        
            