
                      En un patio de París es una película que oscila entre situaciones incómodas y diálogos divertidos. La película empieza como una comedia y, poco a poco, se convierte en un drama melancólico con seres humanos angustiados patológicamente. Es una película en la cual la solidaridad aparece como el mejor humanismo y remedio para la ansiedad de los personajes.
        
            