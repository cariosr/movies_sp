
                      Este remake de la película de Janet Gaynor de 1937 es uno de los melodramas superlativos de la historia de Hollywood y contiene la mejor actuación de Judy Garland, junto con el brillante James Mason, dos estrellas en el apogeo de sus poderes.
        
            