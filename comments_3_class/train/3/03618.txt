
                      Vi el pase de la prensa. Para entender lo que pasó en España en el primer tercio de siglo, es bueno ver esta película. Biopic de san Pedro Poveda, educador, humanista, mártir de la fe. Magnífica interpretación de los actores que encarnan a Poveda, Pepita Segovia, Marina, Felipe, Manolo, Carlos y otros.
        
            