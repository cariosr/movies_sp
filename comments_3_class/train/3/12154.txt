
                      Dicen que la curiosidad mató al gato. Pero lo cierto es que la curiosidad puede llegar a ser un potente motor para la creatividad y el aprendizaje. Que se lo pregunten a Joaquín Calderón. Este actor, productor y director de cine navarro lleva toda su vida siguiendo la llamada de su instinto curioso y ávido de saber. La última parada de su viaje: la vida del gran músico Pablo Sarasate contada en 76 minutos de documental lleno de rigor, sensibilidad y talento.
        
            