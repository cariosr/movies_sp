
                      Escrito y dirigido con gran habilidad y dolor de corazón por David Ayer, Fury captura los sentimientos enterrados de los hombres en combate con una penetrante inmediatez.
        
            