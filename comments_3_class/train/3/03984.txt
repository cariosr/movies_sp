
                      Creo que es una película de producción palestino- Franco-Israelí , la directora Maysaloun Hamoud es palestina si mi memoria no me falla, las tres chicas son palestinas ( de religión musulmana) viviendo en Tel Aviv la capital de Israel. Habla de la situación de la mujer musulmana “moderna” en una sociedad israelí tradicional, es aun mucho más complicado, excelente película con escenas muy fuertes, mujeres reprimidas que expresan sus ganas de libertad! La banda sonora es genial, yo la recomiendo  de verdad súper ampliamente ! 
Asi como la de Téhéran tabú entre muchas otras que tratan sobre la situación de la mujer en la sociedad actual, si les interesan algunos títulos no duden en preguntarme y con gusto les comparto 🙂 
🌟🌟🌟🌟👌🏽
        
            