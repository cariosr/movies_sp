
                      Extraordinaria película con la que nos sorprende Michel Hazanavicius y que sin duda se va a convertir en lo más grande de su carrera, al menos hasta el momento.
Una apuesta muy arriesgada para los tiempos que corren, pues cuando prima el 3D y los efectos, va Hazanavicius y nos seduce totalmente con una película muda y en Blanco y Negro.

Con un guión perfectamente hilado y lleno de ritmo nos traslada, y nunca mejor dicho, pues también la ambientación es soberbia, a 1927 y la transición del cine mudo al sonoro, utilizando para ello una clara historia de amor con los momentos más extremos de los personajes y su devenir conforme suceden los acontecimientos de la época.

Actuaciones estelares de los protagonistas, perfectamente secundados por todo el reparto y con una especial atención al perro por lo gracioso y divertido, sin olvidar su fidelidad y apoyo, al igual que la de Clifton, mayordomo por antonomasia.

¿Qué decir de la música? , que marca claramente el ritmo y se adapta perfectamente a todas las situaciones mostradas, sin olvidarse de hacer claros guiños e insinuaciones a diversas bandas sonoras de películas archi conocidas.

Detalles muy cuidados de la puesta en escena desde el formato de la pantalla, al estilo de la época a las imágenes subliminares al estilo del cartel de un local "Lonely Star" en un punto muy concreto de la película.

En definitiva, una gran película, llamada a formar parte de las más grandes y a ser vista por todo aquel amante del buen cine y aquel que simplemente quiera entretenerse un rato.
        
            