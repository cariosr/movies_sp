
                      Obra cumbre de Julio Medem, sensible, tierna, mágica, un romance contando con una delicadeza que solo el director vasco sabe transmitir, excelentemente ambientada y rodada. Cine con mayúsculas
        
            