
                      Disney/Pixar nos ofrece una película creativa, divertida y con mensaje.

Ya desde los primeros avances sabía que ésta iba a ser no sólo una película divertida y con exquisitos enfoques psicológicos, sino que además desbordaba originalidad y brillantez por el argumento principal. Intensa-Mente es humorística, atrevida y ofrece, en ocasiones, un ritmo vertiginoso. 

El argumento es sencillo: transportarnos al complejo e intrincado mundo de nuestras emociones, ubicados en la etapa de la niñez poco antes de llegar a la adolescencia, y la personificación de cada emoción con colores para darle un matiz más específico. Pero eso no es todo, pues además del divertido tratamiento del tema (originalidad) y cómo lo aborda el director Peter Docter, uno llega a la conclusión de que hay más de por medio que sólo la presentación de la psique humana puesta de un modo sencillo en este filme. 

De esta manera conocemos a Riley, una niña que está en plena transición a la pubertad, y ese cambio se hace más patente en su comportamiento. La película arranca con los cambios que su familia experimenta, como un nuevo empleo para el papá, el cambio de ciudad para su familia y el ingreso, por defecto, a una escuela desconocida para Riley. 

Creo que uno de los mayores aciertos de la historia es contextualizarnos en situaciones creíbles, situaciones que a todos nos pueden ocurrir, ya que en el momento en que la protagonista comienza a empatizar consigo y con su entorno es que notamos y admiramos el porqué de sus decisiones y ocurrencias. 

Además de eso está el desarrollo de personificación de emociones (mencionado líneas arriba), pues en los tres actos la película juega con nuestra capacidad deductiva y nos lleva de una situación a otra, intentando adivinar cuál será el final o el mensaje de la película. Algunos diálogos y acciones presumen transparencia; es decir, ofrecen la sensación de expresar una motivación sencilla, pero que en realidad es más profunda y que da pie a divertidas consecuencias. No todo es color de rosa. Aunque la película evidentemente está dirigida a un público infantil, como sucede en este género hoy día, hay detalles para el público adulto que conviene recoger, por lo que se recomienda prestar especial atención a cada momento del filme. 

Si bien es una película muy agradable y entretenida, fueron pocas las veces en que solté una carcajada. Me parece una película que se centra más en la exploración de la mente humana desde la perspectiva de una niña que en una película que se esfuerce inútilmente en arrancar risas al por mayor. Intensa-Mente es completamente disfrutable y con una moraleja inesperada; un logro así sólo es obra de un tema trascendente.
        
            