
                      Esta historia de racismo, activismo político y lucha por los derechos de los más vulnerables se siente y se sufre a través de una cuidada fotografía y una banda sonora de excepción.  Los que se sienten en la butaca con el deseo de sentir algo que les remueva la conciencia y las tripas, verán más que cumplidas sus expectativas.
        
            