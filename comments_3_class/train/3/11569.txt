
                      Sin duda la mejor película de la trilogía y de la marca Comic DC, incluso una de las mejores del género superheroes.
Marca un antes y un después en su género y un ejemplo para las nuevas películas de los estudios. Si todas las películas venideras de Warner sobre personajes de DC Comics fueran así sería un taquillazo seguro.
Un superheroe de su calibre y unos villanos como El Joker o Dos Caras hacen de este film una mezcla increíble. Efectos además que condimentan esta fantástica mezcla hacen que sus 2 horas y media sean pocas.
La recomiendo a todo amante del cine y sobre todo de los comics; y felicito al ya difunto Heath Leger por su increíble actuación como Joker, sin duda insuperable e inolvidable.
        
            