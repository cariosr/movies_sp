
                      Cada año, por estas fechas, hay algo que esperamos con más ansia que la propia navidad. Más que los regalos, las cenas, las vacaciones. Es el anual clásico Disney por excelencia. Este año, nos trae de regreso, algo que no teníamos desde hace tres años con “Frozen“: una princesa Disney. Dadle la bienvenida a “Vaiana“.

Aunque aún es pronto para llamarla princesa como tal, pues Vaiana no viene de un reino con un amor al que buscar, un principe o un enamorado. Pero tampoco lo tenía Elsa. Mientras esperamos la ceremonia de coronación para esta nueva heroina Disney que nos lo confirme, tenemos un nuevo mundo lleno de aventuras para deleitarnos.

Como cada clásico, Vaiana nos trae canciones pegadizas que no puedes quitarte de la cabeza y que están llenas de energía y fuerza. También tenemos la habitual mascota secundaria que llena de carcajadas toda la película. Y un montón de efectos visuales llenos de color y magia Disney. Pero sobre todo, Vaiana tiene algo que es distinta a las demás: ella busca sus orígenes, busca la llamada de su naturaleza, busca respuestas a todo lo que la rodea y muestras aspectos de grandeza que nunca antes habían mostrado las princesas Disney. 

De nuevo, una obra maestra de la animación que no podía venir de otra productora que no fuera Walt Disney. De nuevo un clásico memorable que llenará de buenas vibraciones nuestras vidas para siempre y de nuevo, una película para nuevas generaciones que crecerán con la majestuosidad de una factoría que llenó nuestra infancia de felicidad.

Lo mejor: saber que estamos ante el comienzo de una nueva era en cuanto a clásicos de princesas se refiere.
Lo peor: que no tenga un villano con más fuerza.
        
            