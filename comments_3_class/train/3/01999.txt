
                      Película que no narra una historia definida con un objetivo concreto como tal, sino que narra un fragmento de una vida en la cual quedan muchos elementos sin definir permitiendo al espectador interactuar con ellos, y permitiendo disfrutar ay saborear cada encuadre de la película debido a su ritmo lento.
        
            