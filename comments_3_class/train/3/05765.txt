
                      Yo, él y Raquel es una agradable historia sobre una chica con cáncer terminal, y puede que agradable pueda sonar irónico sobre el tema que se está tratando, pero la cinta lo trata de una forma tan sumamente natural que hará que el espectador viaje tranquilo y amigablemente sobre diálogos ácidos y bromas que se tiñen totalmente de color negro carbón.
        
            