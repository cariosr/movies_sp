
                      El nombre de Jack Nicholson aparece primero en los créditos, y por una buena razón. Puede llamarse Batman, pero esta es la película de The Joker.

Es sorprendente, supongo, que el director Tim Burton, trabajando a partir de un guión de Sam Hamm y Warren Skaaren, estaría más interesado en el personaje más extraño del cómic de Bob Kane. Teniendo en cuenta la gran aventura de Pee-Wee y Beetlejuice, tiene sentido que él se sienta atraído por la mayor locura del cuento.

Y con Nicholson en el papel, ¿por qué no ir en esa dirección? No todas las elecciones creativas funcionan, no hay razón por la que el Joker deba bailar con Prince, pero no por falta de esfuerzo por parte del actor. Nicholson va por ello en Batman, maldita sea su reputación.

Su compromiso se amortiza exponencialmente, ya que nos volvemos GRANDES, sobreactuando a Jack al principio, lo que prepara el escenario para el GARGANTUAN Jack que obtenemos más tarde, ya que The Joker viaja más y más hacia el fondo. (Por cierto, me encanta la escena temprana en la que Jack Palance, como el jefe del crimen Carl Grissom, básicamente hace una personificación de Nicholson en el rostro de Nicholson. Y luego, la forma en que el mismo Nicholson más tarde responde a eso en un discurso irónicamente dramático para uno de sus secuaces. ) Nicholson no solo ofrece una parodia brillante aquí, sino que lo hace en servicio al personaje.
        
            