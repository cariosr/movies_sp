
                      Simplemente increíble, es muy difícil encontrar sagas de películas como ésta que a lo largo de sus secuelas sigan manteniendo la magia y calidad que las primeras. Excelente final para esta maravillosa saga que me ha atrapado desde la primer película.
        
            