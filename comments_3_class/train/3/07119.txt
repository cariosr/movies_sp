
                      🎞🌟🌟🌟🌟⭐️🎞

En guerre de Stéphane Brizé, 2018 🇫🇷 

Una película que trata sobre una compañía que decide cerrar dos años después de haber firmado un compromiso que hicieron con los trabajadores en el cual se comprometen a reducir sus sueldos y aumentar horas de trabajo a cambio de mantener sus puestos por lo menos durante 5 años, vemos la lucha ruda y constante de los trabajadores con el afán de lograr que los dirigentes cumplan con su parte de la promesa. 

Porque una empresa que funciona bien, que deja beneficios quiere cerrar dejando a 1100 familias sin empleo? 

Cuál es el papel del gobierno en todo esto ? 

El pueblo unido jamás será vencido ! Solo que el pueblo jamás está realmente unido, contrariamente a los empresarios que estás dispuestos a todo por proteger sus intereses.

Personalmente este tipo de películas me apasionan, deja una sensación de injusticia y mucha reflexión sobre la humanidad. ✊🏽✔️🎞
        
            