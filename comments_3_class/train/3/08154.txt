
                      Como absoluto admirador, enamorado y crítico de BLADE RUNNER, seguramente fui uno de los primeros en poner el grito en el cielo al enterarme del proyecto para su secuela.

BLADE RUNNER es para mí, una obra maestra. Una de esas películas que no necesitan más...ni menos, que cuentan todo lo que tienen que contar y el resto lo dejan para que tú lo imagines como quieras. No hay ningún interés en saber qué ocurrió antes, ni después, ni siquiera durante. ¡¿Qué necesidad hay pues de una secuela?!...ninguna, la verdad. Pero, ¿quién es uno para luchar contra la gran apisonadora de Hollywood?...nadie. Así que, como a todos, no me queda más remedio que tragar.

Mi indignación va menguando al saber que Ridley Scott (a quien considero uno de los mejores directores de cine), muy sabiamente renuncia a dirigirlo, consciente que ya aportó cuanto tuvo que aportar, y cede la batuta ni más ni menos que a Denis Villeneuve, quizás uno de los directores más prometedores y personales de la última hornada. 

A pesar de que mi escepticismo sigue ganado la batalla, llegan los primeros trailers y no me queda más remedio que darle una oportunidad...Sólo os digo que fuí (pagando) cuatro veces al cine a verla.

Sí: Blade Runner es una obra maestra, y BLADE RUNNER 2049 es una película innecesaria (como lo son el 99,9999% de las películas), pero ya de hacer una secuela innecesaria, al menos hacerla así de bien. Porque BLADE RUNNER 2049 es una magnífica película.

Curiosamente, somos los fans del Blade Runner de Ridley Scott quienes más hemos disfrutado esta entrega y quienes más la hemos apreciado. Y es que no nos ha quedado más remedio que rendirnos a la evidencia. Scott es un grande dirigiendo, pero es igual de bueno produciendo. Debimos habernos fiado de su inteligencia: no,él no iba a permitir ninguna chapuza, puesto que eso restaría valor a la que sigue siendo su mejor  película...o al menos la más influyente.

No, Blade Runner 2049 no va a cambiar la forma de hacer cine, como ocurrió con su antecesora, pero sí afianza un camino que concilia el cine comercial, el cine de género, con el cine de calidad, el cine de autor y el cine con intención de permanencia. 

Blade Runner 2049 no es una película rupturista: mantiene toda la esencia pero también aporta la enorme personalidad de su director. Tiene el punto justo de homenaje pero con una clara vocación de continuismo, y también cuenta con sus propias imágenes y planos icónicos

Digna hija de su madre, Blade Runner 2049 reproduce un futuro creíble, un futuro treinta  años más allá del que inventó Ridley Scott, con una continuidad lógica y coherente, en el que se reconocen todos sus elementos sin caer en la copia ni en la recreación. Es como si realmente hubieran trascurrido treinta años en ese universo ficticio, y hubieran ocurrido avances y acontecimientos que han dejado su huella y que intuimos. 

Un diseño de producción apabullante, hipnótico y casi tangible, que nos abstrae de nuestra realidad y nos encierra en un universo oscuro, nostálgico y húmedo, que sobrevive lleno de heridas y cicatrices y que ahoga cualquier rastro de humanidad. Una atmósfera opresiva que nuevamente se erige en un protagonista más de la película . 

Es el futuro lógico del futuro. 

Tanto Scott como Villeneuve son grandes amantes de lo físico a la hora de rodar, y sólo recurren a lo digital cuando no les queda más remedio, o realmente consideran que es realmente imprescindible. Ambos dan gran importancia al diseño de producción, y ambos manejan magníficamente las bandas sonoras.

Si bien La partitura de VANGELIS, marcó también un hito en su momento, HANS ZIMMER , el maestro, retoma la magia, junto con Benjamin Wallfisch, que sustityue a Jóhann Jóhannsson, y componen una banda sonora potente, rompedora, emocionante, en ocasiones agresiva y muchas veces evocadora, que siempre fluye en beneficio de la película.

De hecho, la que es para mi una de las secuencias de lucha más brutales y mejor rodadas que recuerde (junto con la que ya comenté en "Atómica"), que transcurre al pié de el muro del mar, lo es en gran medida por la arriesgada pieza musical, que recuerdo que incluso hacía vibrar la pantalla del cine.

Justicia también en esta "secuela" al encargar su guión a quien fuera el padre del proyecto original del Blade Runner de 1982, Hampton Fancher, y que se sintió desplazado en su momento por la reestructuración y reescritura del mismo a manos de David Peoples.

Bien es cierto que el guión de 2049 se resiente en algún momento por el excesivo "homenaje" que se rinde a Harrison Ford...o por exigencias de la estrellona como condición para participar en el proyecto.
Su aparición en escena confiere un nuevo tono a la cinta y nos sumerge en otro universo decadente y nostálgico, que si bien ralentiza la película, le confiere una nueva capa más humana. Sin embargo toda la secuencia que comparte con Jared leto y Sean Young se convierte en un pegote sin más sentido que el mero lucimiento y autocomplacencia de Ford, un tributo que todos debemos pagar aunque sea a regañadientes, un lastre que quizás en un posterior "diector´s cut" quizás nos podamos ahorrar y pasar directamente a ese largo y absolutamente brillante final.

Mención especial a Ryan Gosling, que si bien últimamente ya empezaba a no creérmele, aquí hace una recreación serena, humana y más que convincente de su personaje, y carga él solito con todo el peso de la película. Sin sus muecas y tics de "fondo de armario" ni sus eternas manos en los bolsillos. 

Mención también para Ana de Armas, en un papel secundario que ella consigue hacer crecer muy por encima de sus posiblidades iniciales. 

Mención especial al perro de Harrison Ford, un toque realmente maestro que confiere humanidad, normalidad y realidad.

Y mención especial también a Jared Leto, pero esta vez para que le borren digitalmente de la película.
Blade Runner 2049 es una película valiente que no se achanta ante una madre famosa, que hace honor a su ADN y que aporta sus propios valores a la tradición familiar. No es la prima del novio de la vecina de la amante de nadie que cobra por contar las miserias de quien sea...es otra artista más que aporta prestigio al apellido y a la tradición familiar.

Es un verso suelto...o puede que el principio de una saga que no sé lo que podría dar de sí...pero ella por sí misma, tiene vida propia, y sin ser necesaria, se convierte casi en imprescindible.
        
            