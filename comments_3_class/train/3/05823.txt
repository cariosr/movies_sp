
                      La verdad es que en un principio la película no me llamaba en absoluto la atención. Sus imágenes no me resultaban interesantes, y su trailer no parecía gran cosa. Pero dado las estupendas críticas que en general ha cosechado el film y como me considero fan del género fantástico me decidí a verla. Ahora me doy cuenta de lo equivocado que estaba.

"District 9" comienza de una forma que desconcierta al espectador, hablando de una serie de personajes y hechos de los que no tienes la menor idea. Esto logra intrigarte, consigue que te pique la curiosidad. El hecho de que la peli sea como un documental no significa que sea mala, aunque hay que reconocer que debido a ello hay momentos en los que se hace lenta, pero afortunadamente no llega al extremo de aburrir.

Las actuaciones son bastate buenas, en general los actores logran meterse de lleno en los personajes; los efectos son fantásticos, la nave, el diseño de los alienígenas... espectacular. Lo mejor de la película es que logra despertar sentimientos en el espectador, tales como rabia por ver cómo tratan a unos seres que no conocemos de nada y que, aprendiendo de nosotros, logran comportarse, en algunas ocasiones, como ellos no son por naturaleza.

La historia entre el padre y el hijo es la parte sentimental del film, así como la relación entre el protagonista y su tan anhelada esposa. La transformación, el contagio, el tema de las armas y los problemas con el gueto hacen que la película resulte aún más interesante. El final, sencillo y bastante decente.

En conclusión, de lo mejor del género, al menos en lo que llevamos de año, hay que reconocer que no es perfecta, y que probablemente mucha gente no entenderá el mensaje: Nosotros los educamos, ellos nos imitan...!
        
            