
                      Ya queda mas que claro que Ridley Scott domina este genero a la perfección!
Maravillosa visualmente, guión quizá un poco visto pero súper potente, interpretaciones buenisimas, sobresaliendo Matt Damon por encima de todo, con momentos divertidos dentro de la gravedad de la situación. Por poner un pero aunque a mi me ha gustado, quizá la BSO no sea del gusto de todos para la ocasión.
Rozando la perfección!!
        
            