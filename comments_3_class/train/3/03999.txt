
                      Basada en una novela de Steve Niles y Ben Templesmith, esta cinta cuenta con lo que se debe esperar en cuanto a la temática contada. Un ambiente siniestro donde la oscuridad se adueña de los habitantes y sale a relucir terrores donde la mayoría de los hombres se harían pedazos dentro de un pánico latente en busca de la supervivencia. En esto la cinta cumple con el objetivo previsto y siendo de una de las temáticas quizá más usadas en el cine, no deja de traer una visión dura y temible de los seres de la noche más famosos en esta industria.

La elección de los personajes me pareció acertada dentro de las limitaciones histriónicas que tienen algunos de los actores que formaron el elenco de este film. También he de reconocer que viéndola por enésima vez y viendo algunos trabajos posteriores de Josh Hartnett, me convenció de lo que estaba viendo a pesar del melodrama que me resultó luego el final de la cinta.

Lleva ritmo, la historia me pareció bien ejecutada, el ambiente correcto para elaborar una trama con esta. Fue secuencial y en los momentos donde la acción cobraba vida, satisface haciendo que las escenas fuesen lo que supongo que querrían mostrar en el desarrollo de la cinta.
En definitiva, buena película.
        
            