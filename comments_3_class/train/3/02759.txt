
                      Tati como jamás aún lo ha visto. Las películas de animación poseen este encanto que permite crear obras intemporales. Este ilusionista de Music Hall se halla en la depresión de su carrera, pero un encuentro logrará cambiar su vida. Esta película es un puro momento de poesía ,a la vez sobrio, depurado, delicado y sutil. Nos dejamos llevar por las imágenes de esta película casi muda, como hechizados por la melancolía del personaje.
        
            