
                      Han pasado más de 20 años desde su estreno y a diferencias de otras películas esta me parece no envejecer, aun así ya hemos visto crecer a Kirsten Dunst y convertirse en una gran artista de la pantalla grande, Tal vez no sea la película de terror por excelencia porque no voy a mentir miedos ni uno pasé, ni paso cada vez que veo la peli, porque la cada vez que puedo me la pongo, no sé si por su vestuario, por los ojos de los vampiros, por como Claudia a través de falsa inocencia y ternura se come a todo que intenta consolarla o por ver a los tres más guapos hombres de los 80s y 90s seduciéndose los unos a los otros con una exposición de poder absoluta.

Para la adaptación se tiro la casa por la ventana, tan solo el casting de "guapos" ya era un lujo, aunque muchos de los pensados por la propia Rice no entrarían en el puesto que River Phoenix descansaba en paz o Rutger Hauer ya estaba muy madurito para ser Louis de Pointe du Lac y mas Alan Delon de Leslat.

Una de las bases de la película es la relación entre el trío de vampiros protagonista: Lestat, Louis y Claudia. Relación que llega al espectador gracias al excelentísimo trabajo de los tres actores.
La aparición de Banderas trajo consigo escenas con enorme contenido de morbo pasión y sentimentalismo, los tres estuvieron magníficos en sus representaciones, fuerte escena de casi beso entre Brad Y Banderas que en los 90 fue escandaloso pero a la vez abrió puertas a la tolerancia.
Con Entrevista con el Vampiro, la Rise nos trajo denuevo al vampiro hambriento, malo, seductor por sangre, desgarrador porque ama pero no importa apartarse de ese amor con tal de chupar sangre sin pudor. Cruise representa al vampiro por excelencia, del malgastado Dracula de Ford Coppola al manipulador, obsesivo, poderoso Lestat.

“Entrevista con el Vampiro” es un film de gran valía pero muy Infravalorado. Uno de los mejores títulos de ficción y terror de la década de los 90, que goza de interpretaciones perfectas, una ambientación muy conseguida, la acción necesaria, música excelente y la historia de un Vampiro incomprendido dentro de su propio infierno.

Para finalizar voy hacer mención a esa espectacular banda sonora que envuelve a toda la cinta. La cuál se hizo archiconocida por la acertada canción final y que pone el broche de oro a esa escena final, en la que vemos cómo Leslat ha superado sus siglos de retiro y sacando la puntilla del puño de su camisa del siglo XIX por fuera de su nueva chaqueta de cuero, conduce el coche del acojonado reportero, mientras le dice "Voy a darte la ocasión de elegir, que yo no tuve".
En definitiva, una joya del género.
        
            