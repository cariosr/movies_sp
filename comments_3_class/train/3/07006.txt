
                      Una obra maestra del cine, el guión de esta película es espectacular, es tan genial que dependido de la persona que la vea puede sentir y ver cosas diferentes a otra, la película nos habla de nuestra asquerosa sociedad consumista, que somos esclavos del marketing. 
La película es graciosa, enserio, es increíble que una película que te haga reír tenga esa trama tan profunda y trabajada. 
Los personajes son geniales. 
La fotografía cumple, no es para tirar cohetes pero cumple. 

Si eres humano tienes que ver la película, así de simple.
        
            