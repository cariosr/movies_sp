
                      Filmar una película sobre un hecho tan reciente y tan mediático como el polémico aterrizaje del vuelo 1549 en el río Hudson en el 2009, es cuando menos arriesgado. 

A priori puede parecer una salida facilona para un director con una carrera tan abultada y ejemplar como la de Clint Eastwood; una búsqueda apresurada de un tema resultón sin más complicaciones. Primer riesgo.

Puede caerse ( y de hecho suele caerse) en el patriotismo exacerbado y la exaltación del héroe. segundo riesgo.

Se puede ( y suele hacerse) perder la objetividad, deliberada o accidentalmente, o por intromisión y control por parte del personaje de quien se relata. Tercer riesgo.

Puede que la cercanía de los hechos no dejen espacio para una perspectiva lo suficientemente amplia y madura sobre los mismos. Cuarto riesgo.

La película puede ( y suele ) acabar degenerando en un mero telefilm superficial de consumo dominical. Quinto riesgo. 

Una mala elección de actores  o  una búsqueda insistente de la espectacularidad pueden restarle verosimilitud a la historia, haciendo fracasar a la película. Sexto riesgo.

Se me ocurren muchos otros, pero me quedo con éstos, de momento.

Pues bien, el señor Eastwood, que para eso tiene la madurez que le dan los años, los huevos bien puestos y un talento que dudo que nadie se atreva a poner en entredicho, asume esos y más riesgos y sale indemne, incluso triunfante de ellos.

Partiendo de la base que una la objetividad y la imparcialidad son una quimera, y es más, dudo de que ni siquiera fuesen recomendables, Eastwood plantea su relato de los hechos situándose al lado del ¿héroe? para dejarle que nos explique sus motivos. Se sitúa a su lado, pero construye un relato milimétrico y lineal, desprovisto de valoraciones morales y dejándole al Capitán Chesley "Sully" Sullenberger (Tom Hanks) todo el espacio que necesita para aclararnos por qué decidió tomar una decisión tan arriesgada como aterrizar (no amerizar, como él mismo aclara) un avión lleno de pasajeros en mitad del río Hudson y no intentar volver al aeropuerto de La Guardia.

Nos lo cuenta tan bien, que no deja ningún cabo suelto. Convierte la película en la eterna lucha entre los que "hacen" y los que se limitan a criticar a los que "hacen". Los que arriesgan y los que juzgan a toro pasado...algo que deberíamos aplicarnos esa subespecie inmunda que somos quienes nos dedicamos a "juzgar" las películas que hacen los demás.

Un homenaje todos aquellos, que en un mundo de incompetentes y tecnócratas, se convierten en héroes por el mero hecho de hacer bien su trabajo. Y sí, hay unos segundos al final en que se cae en esa exaltación del héroe nacional, o de nacionalismo de héroes...pero ¡coño, qué son unos segundos en una película tan bien hecha!

No, no es "Million Dollar Baby", ni es "Gran Torino", ni "Mystic River" o "Sin Perdón". Pero nadie se puede pasar la vida haciendo obras maestras. No, no está a la altura de ellas, pero sí a la altura del señor Clint: directa, bien escrita, sin adornos y con un montaje limpio y narrativo y lo más importante: con la duración que necesita, ni más ni menos. Poco más de hora y media para contárnoslo todo sin dejarse nada en el tintero y sin estirarlo para convertirlo en una epopeya grandilocuente. Ese es mi Clint.

Pero por encima de que me haya parecido una buena película, de que me haya entretenido, que me haya gustado mucho (aunque no me haya emocionado)...el mayor mérito de este SULLY es que por primera vez en mucho tiempo, me he creído (incluso me ha gustado) a Tom Hanks, y eso no es poca cosa. 

Ni qué decir que Aaron Eckhart está estupendo, como siempre, y el grueso del reparto a la altura de los dos (salvo Laura Linney que por primera vez no me acaba de convencer).

Queda patente una vez más, que Mr Eastwood , a pesar de algún que otro pequeño patinazo, sigue siendo un valor seguro y un animal cinematográfico de pura casta. Que dios nos le conserve muchos años, y que siga votando a quien le de la gana, que a nosotros (al menos a mi) lo que nos importa es su cine.
        
            