
                      La película es excelente: interpretación, fotografía, puesta en escena, ambientación y decorados. Esta narrada desde el punto de vista de un soldado no creyente, rudo, y fiel al imperio romano. Es emocionante y logra captar la atención del espectador, con ritmo adecuado, ya que en cada escena, gana en interés, hasta llegar a un final inesperado.
        
            