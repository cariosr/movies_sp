
                      De lo mejor en dramas románticos. Protagonizada por galán, Ryan Gosling, que se come el objetivo y a Rachel McAdams, por supuesto. Triste y bonita trama con un desarrollo limpio y un buen ritmo. Aunque parezca una película para adolescentes con una historia intuitiva, pronto nos daremos cuenta de que tiene más alcance. No podemos encasillar esta película con el resto de su mismo género.
Un consejo, si eres soltero y conoces a una chica que no ha visto esta película, eres un tipo con suerte.
        
            