
                      Arsénico por compasión es una comedia dirigida por Frank Capra, director de ¡Qué bello es vivir!, The Miracle Woman y Rain or Shine.

Mortimer Brewster (Cary Grant) es un crítico teatral y periodista recién casado. Antes de irse de luna de miel con su mujer (Priscilla Lane), visita a sus ancianas tías y a su hermano Teddy. Durante su visita, Mortimer descubre un cadáver en el arcón bajo la ventana. Desconcertado, intenta arreglar la situación para evitar causarles problemas a sus "inocentes" y adorables tías.

De este largometraje prácticamente perfecto destaca Cary Grant, en uno de sus mejores papeles. Carismático y divertido, como siempre, logra llevarse el protagonismo.

La película es divertida de principio a fin; con una dirección excelentísima, un reparto formidable, y unos diálogos y situaciones desternillantes, es considerada una de las mejores comedias de todos los tiempos. Sin duda, una obra maestra.
        
            