
                      Alegorías, metáforas y submensajería a parte, Aronofsky mantiene su deseo de retorcer los sesos de su audiencia hasta los extremos. Son muchas las referencias que podría uno citar en madre!, como la Biblia o la naturaleza, pero sabiendo que cada fotograma requiere días de reflexión, quizá lo mejor que se pueda hacer sea disfrutar del recital de Bardem, acompañado de una gran Jennifer Lawrence. Un delirio exquisito.
        
            