
                      En mi opinión como espectador,Ant-Man,es otro gran acierto de parte de Marvel,hizo del hombre hormiga una película de los más fresca,divertida y entretenida,casi al nivel de Guardianes De La Galaxia,pero con menos héroes,logra ser una excelente incursión al U.C.M y concluye de gran manera la fase 2.La trama es de lo más llevadera y entretenida en todo sentido:Elenco,humor,sobresalen los espectaculares efectos especiales y escenas de acción,que nos presentan algo nunca antes visto,ya que predominan los cambios de cámara al constante cambio de tamaño de Ant-Man(Paul Rudd),y hacen que las escenas estén súper bien logradas y consiguen atrapar no solo visualmente sinó en la trama de la película.Los personajes son en todo momentos entretenidos,desde los papeles algo más serios que trabajan Michael Douglas como Hank Pym y Evangeline Lilly como Janet Van Dyne,hasta los divertidos papeles de Paul rudd como el protagonista Scott Lang y Michael Peña como su fiel y chistoso amigo Luis.Lo único malo es nuevamente un villano para nada entretenido y súper aburrido,Darren Cross/YellowJacket,interpretado por Corey Stoll,que a pesar de ser un buen actor,no logra consagrar a su personaje como uno de los mejores villanos de Marvel.En resumen,la película del hombre hormiga es de lo más divertido y entretenido de Marvel en todo sentido,súper recomendable para pasar un buen rato con diversión asegurada.

Puntos a favor:Trama súper divertida y ágil,Personajes entretenidos,efectos especiales y escenas de acción innovadores.

Puntos en contra:Villano muy olvidable.

Mi calificación:9/10
        
            