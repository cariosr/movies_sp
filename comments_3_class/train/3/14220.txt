
                      -¿Porqué Dios nos hace plantearnos preguntas si no va a darnos las respuestas? 
 -No me lo ha dicho.  
Un Tipo Serio, o cómo hablar de filosofía y existencialismo sin necesidad de ser pedantes. Los Coen abren el grifo de las parábolas religiosas a máxima potencia para hacernos ver que de esa forma el vaso de la vida se desborda y nunca se llena. Una obra maestra del cine. Su mejor película.
        
            