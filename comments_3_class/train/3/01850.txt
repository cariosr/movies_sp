
                      En un año de débiles funciones animadas, Frozen, basado libremente en "The Snow Queen", toma la delantera de forma predeterminada. No hay mucho talento en la historia de las hermanas princesas Anna (con la voz de Kristen Bell) y Elsa (Idina Menzel), divididas por la incontrolable habilidad de Elsa de convertir las cosas, incluso Anna, en hielo. Pero la animación es bonita, las canciones son melodiosas y Josh Gad se ríe a carcajadas como Olaf, un muñeco de nieve con un fetiche del sol. Son las vacaciones, la gente, trabaja con ello.
        
            