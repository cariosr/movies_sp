
                      Pasada de película, me e quedado embobado viéndola, me a gustado de principio a fin. No soy jugador del juego pero la peli me a encantado.

Empieza contando la historia de el credo de asesinos, para luego ir a la actualidad y ver como un assasin mata a la madre de un chaval, luego el chaval crece y se convierte en Michael Fassbender el cual esta preso con pena de muerte. Cuando parecía que había muerto resulta que la Marion Cotillard lo quería con vida para un proyecto que ella tenia. Y bueno ahí empieza la cosa Fassbender se mete en el animus y entra en la época de la inquisición española donde tendrá que hacer varias misiones para cumplir una mayor y conseguir la localización de un artefacto en la vida real. La película esta llena de acción e imágenes increíbles, movimientos de parkour y sobre todo el salto de fe (espectacular).

Muy recomendable si te gusta el genero aventura – acción. Espero que hagan una segunda parte.
        
            