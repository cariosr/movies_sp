
                      Misery es una historia limpia de sucesos impresionantes sin divagaciones de seres, ni fantasmas, ni ovnis. Solo la locura de una mujer como mucho normalita, ni guapísima, ni modelo, ni señora de alta esfera social. Kathy Bates, que bien merecido su Oscar, es Ann Wilkes, a veces cariñosa, atenta, habladora pero sobre todo mala, malísima, cruel con las palabras y luego con sus actos, retorcida, maquiavélica, inhumana total, con una capacidad de infringir dolor que es abrumador. El pobre James Cann en el papel de Paul Sheldon no es más que un títere maltratado y encogido a su más mísero ser.

Es tan alto su potencial en todos sus rubros, desde la dirección hasta el sonido, todo en conjunto como un puzzle terminado y exhibido en una pared del salón, así con grandeza. Si bien la película no es fiel al cien por cien al libro es una de las mejores adaptaciones de King al celuloide y aún así las partes cambiadas supuestamente porque eran imposibles por su salvajismo.

Lamentablemente en la película no podemos apreciar la brutalidad de Annie, ni su historial criminal completa que incluyen familiares, compañeros de la universidad, vecinos, pacientes. Aún así es magnífica.
En definitiva, muy buena película.
        
            