
                      Denis Villeneuve se confirma como de uno de los directores del momento con 'La llegada', un thriller de ciencia ficción, que está entre lo mejor del año en el género. No esperéis explosiones y efectos especiales a raudales, la película no los necesita: el realizador genera el suspense y la emoción necesaria con unos pocos elementos, en una de las cintas más originales del año.
        
            