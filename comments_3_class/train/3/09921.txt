
                      Buena película. La recomienda porque la serie te engancha desde el principio. ¡La película es una pasada y es alucinante! Encontrar a mis actrices favoritas en una sola cinta no lo creí jamás. Pero llego esta película y lo cumplió. Me encantó y estoy a la espera de la segunda parte. La trama es espectacular. Te da que pensar sobre la vida y relaciones entre amigos y familiares. ¡Algunas escenas de acción que dejan con la boca abierta! especialmente en el medio de la película. Numeros musicales muy efectivos y escenas super divertidas.
        
            