
                      No quiero contar mucho de qué va porque se puede tender a hacer algo de spoiler casi sin querer, una experta lingüista es contratada por el ejército, cuando unas naves extraterrestres, 12 concretamente, comienzan a llegar a la Tierra para intentar averiguar si los alienígenas vienen en son de paz o suponen una amenaza. 

Amy Adams realiza la interpretación de su vida. Ella es el corazón de la película, cuya actuación resulta vital para creernos lo que le ocurre a su personaje y asimilar que esto no va realmente sobre extraterrestres, sino sobre cómo su llegada afecta a la humanidad. Su interpretación es soberbia, exponiendo con acierto tanto la fragilidad de su personaje y sus dudas como su determinación y valentía.

La película es extraordinaria, es un poema sobre la vida y la muerte, una verdadera historia de amor y una delicia desde el punto de vista artístico, visual e incluso espiritual. Te engancha desde el minuto 1 y te tiene en vilo casi sin respirar las dos horas de metraje. Que manera de usar los flashbacks... Que interpretaciones... Que grandes homenajes al gran Kubrick, Spielberg y Ridley Scott. 
En definitiva, una maravilla de película.
        
            