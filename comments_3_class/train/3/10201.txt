
                      Considero, que recrea con un gusto excelente la epoca en que se plasma la historia, Mario Casas transmite perfectamente en cada momento las vivencias y sentimientos. En cada poro de su piel así como casi todos los personajes nos hacen sentir parte de dicha historia transportandonos y embullendonos en el sentir de un pueblo y de una parte de la historia. Nos sentimos capaces de llorar, perder el aliento y consumir los minutos en los brazos de la melodía. Genial¡¡¡¡¡
        
            