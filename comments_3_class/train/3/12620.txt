
                      La relación entre Capitán América (Chris Evans) y la ruda Natasha Romanoff / Black Widow (Scarlett Johansson) es divertida y sincera.

Probablemente no sea una coincidencia que a Redford se le asignara, con su turno en All The President´s Men para hacer caso omiso de la deuda de la película con las películas de conspiración de la década de 1970.

También es una película que se mueve de vez en cuando, con el Capitán retomando su llama de la década de 1940, interpretada por Hayley Atwell, que ahora es un inválido en cama.

Las lágrimas son una cosa que no esperas en una película de superhéroes, pero incluso los corazones más duros encontrarán su encuentro terriblemente conmovedor.

Una vez más, el Capitán América ofrece una sorpresa encantadora y compite con la primera película de Iron Man por el título de la mejor película de Marvel.
        
            