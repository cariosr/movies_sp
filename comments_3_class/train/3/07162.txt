
                      Excelente película, con un planteamiento novedoso. Lo único que no entiendo porqué siendo los extraterrestres tan avanzados no tiene un traductor al inglés y evitan que la protagonista se desgañite tratando de traducir su lenguaje escrito.

La película contiene mucho más de lo que se ve a simple vista, empezando por el protagonismo de una mujer, frente a un mundo de hombres. El juego con el tiempo es muy inteligente.
        
            