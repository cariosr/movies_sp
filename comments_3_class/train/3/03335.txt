
                      Desde mi punto de vista una película singular en lo que a trama se refiere, rompe viejos esquemas del "Felices por siempre" y nos lanza un film lleno de realidad (por mas dura que esta sea). La recomiendo mucho por su calidad de guión y por su fuerzo transformadora.
        
            