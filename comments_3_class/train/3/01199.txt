
                      Gran thriller de terror psicológico. Babadook es una película que se te mete por debajo de la piel, como esas cucarachas que aparecen en distintos momentos de la película, para contarte una historia sobre los más crueles trastornos psicológicos y emocionales que, en ocasiones, sí, son nuestro "hombre tenebroso en el armario" personal. Babadook-dook-dook.
        
            