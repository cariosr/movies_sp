
                      Es una comedia francesa a la que '8 apellidos vascos' le echó un ojo para adaptarla en versión española. Muy entretenida, tierna y con humor. Algunos momentos podrán despertar la risa del espectador, aunque existen varias coñas que nunca entenderemos porque no pertenecemos a su cultura. Al igual que con algunas series americanas no llegamos a entender bien sus chistes, aquí pasa algo parecido. 

Sin embargo, la historia de un hombre que debe ir al norte por motivos laborales da mucho juego. Aquí el norte es lo más parecido a Mordor, mientras que el sur es el paraíso francés. Si te gustan las comedias francesas esta será de tu agrado, aunque advierto que no es tan graciosa como otras de su género que también puedes ver como son 'Dios, pero que te hemos hecho', 'Nada que declarar', 'Salir del armario' o 'La cena de los idiotas'.

La actuación de Kad Merad y de Danny Boon son sublimes.
        
            