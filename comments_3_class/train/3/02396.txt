
                      Tremenda película, para reflexionar mucho. Se dice que la gente en los pueblos es más sana. Hasta qué punto es así? El triunfar despierta la envidia de tus supuestos amigos, paisanos? Se está a expensas de los caciques de turno? El Nobel se entrega en el ocaso de la carrera de un escritor, te convierte en estatua?
Todos los escritores son vanidosos (no es una pregunta). La cultura necesita defensa? Mal vamos entonces. "La mejor política cultural es no tener ninguna".
El actor protagonista, Oscar Martínez, treeeeeemendo!! De la gloria al infierno en su querido pueblo natal. Muy recomendable.
        
            