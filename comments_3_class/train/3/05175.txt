
                      Gran película. Merecidísima del Óscar que no obtuvo, a la mejor película de habla no inglesa.
Refleja la actitud de una clase "normal en los tiempos que corren", A LA PERFECCIÓN. Es una película sencilla pero que muestra la realidad de una clase. Durante el curso se crean conflictos pero acaban solucionados y muestra las quejas de los alumnos; y que los profesores, por mucho que inpongan, también lo pasan mal cuándo sus alumnos tienen una actitud provocativa e insolente.
Recomendable a todo el mundo, pero sobretodo a los alumnos de ESO, bachillerato etc... ésta película te hace reflexionar y si la ve un adolescente puede llegar a la conclusión de si su actitud hacia el profesor es buena o se puede corregir.
        
            