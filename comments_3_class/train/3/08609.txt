
                      De entre otras cosas que le pido a una película es que me distraiga, me haga sentir y que mientras la miro me aleje del mundo que me rodea. Y esta lo ha conseguido desde el principio haciendo que no me mueva de la butaca hasta que terminara.
Me ha evadido por completo durante las casi dos horas que dura y en ningún momento he tenido la sensación que durase tanto.
La recomiendo a los amantes de la ciencia ficción y a los que no lo son...
        
            