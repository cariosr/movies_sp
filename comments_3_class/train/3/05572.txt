
                      Una obra maestra, basada en hechos reales, la película narra a la perfección la historia ocurrida durante la Segunda Guerra Mundial. Un drama que suscita empatía con los protagonistas. Buen ritmo narrativo. Película altamente recomendable.
        
            