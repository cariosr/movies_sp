
                      Impactante “H6 diario de un asesino” como dice la Guía del Ocio de Madrid. El regreso de Martín Garrido (Barcelona 1982) con su segundo film no es menos impactante. Me ha parecido una película por encima de la media del cine que se hace en este país. Tiene momentos geniales.
        
            