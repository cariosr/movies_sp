
                      La mejor película de la saga Bond de Daniel Craig con diferencia. Recuerda mucho al estilo de Casino Royal pero con diferentes ráfagas de aire fresco. Un Bond más humano y menos perfecto pero igualmente de efectivo es el que nos muestra su director Sam Mendes. Y Bardem, perfecta interpretación de un villano malo malísimo como ambiguo, que borda su papel con creces y es capaz de poner nervioso a este duro Bond. Geniales Craig y Bardem.
        
            