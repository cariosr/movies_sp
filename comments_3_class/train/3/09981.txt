
                      Una verdadera obra de arte, no es muy fácil de comprender pero si tienes las neuronas necesarias no hay problema. Es una maravilla desde su TRAMA hasta su SOUNDTRACK pasando por los EFECTOS ESPECIALES, LAS TOMAS Y ENCUADRES, LAS HERMOSAS ACTRICES, LA EDICIÓN, LAS ACTUACIONES, etc. en resumen; una de las mejores de este año para mí.
        
            