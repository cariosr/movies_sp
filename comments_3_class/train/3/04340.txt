
                      Atrapado en el tiempo es una comedia dirigida por Harold Ramis y protagonizada por Bill Murray. Ambos, protagonistas de Los Cazafantasmas (1984).

Phil Connors es un reportero encargado de cubrir la noticia de El día de la marmota.
Al despertar la mañana siguiente se da cuenta de que está reviviendo el día de ayer. Vuelve a despertar cada mañana en el mismo día, cada día cambiando algo diferente, intentando averiguar cómo romper el ciclo.

Todos los días Phil se despierta con la canción 'I Got You Babe' de Cher y Sonny. Debido a la popularidad de la película, esta canción es asociada por el espectador con la película.

Esta comedia fue premiada por el mejor guión original.
Bill Murray, muy bien metido en su personaje realiza una actuación sensacional, muy cómica.

La desesperación inicial de Phil al ver que repite siempre el mismo día y la diversión que le provoca hacer una locura nueva cada mañana, sabiendo que no tendrá represalias, está muy bien expuesta. La evolución de una actitud a otra no es demasiado brusca, pero no llega a aburrir.

Lo malo de esta película es que la atención se centra únicamente en Phil, dejando a los demás personajes con una menor importancia escénica, haciéndolos secundarios.
        
            