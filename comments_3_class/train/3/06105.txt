
                      Que grande es Almodóvar cuando más adulto se hace!
Que emocionante película de amor-homenaje a su madre, de desamor-homenaje a sus amores!
Antonio Banderas perfecto! Nora Navas fantástica ! Penelope que cada vez se parece más a Sofía Loren! Asier buenísimo ! Y El Niño que es un amor!
        
            