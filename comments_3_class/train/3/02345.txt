
                      Al final la película urga en los peligros del nuevo sueño americano. Recalca que el sexo y el dinero son los motores actuales del mundo y nos lo presenta en pantalla con toda su crudeza, consiguiendo sin embargo hacer una película bella. Merece la pena.
        
            