
                      En términos de estado de ánimo y estilo, podría ser la película más influyente que haya aparecido desde Blue Velvet (1986).

El rumor de Pulp Fiction comenzó en mayo, cuando la película obtuvo los máximos honores en el Festival de Cine de Cannes. La emoción siguió creciendo a medida que la imagen fue elegida como la presentación de la noche de apertura del Festival de Cine de Nueva York y se volvió aún más bulliciosa después de la apertura del 23 de septiembre.

Con Pulp Fiction listo para comenzar su lanzamiento comercial nacional hoy, ese rumor se ha vuelto ensordecedor. Si de alguna manera no has oído hablar de esta fantástica producción de vanguardia, deberías volver al ciclo.

Quentin Tarantino, el hombre detrás de la película, es conocido principalmente como el creador de la famosa película de crímenes violentos de 1992, Reservoir Dogs. Pulp Fiction, también, es una película de crimen cuya violencia impactará a los que se ofenden fácilmente (y no solo a los ofendidos fácilmente).

En términos generales, sin embargo, la nueva producción es algo más fácil de usar que la imagen de debut de Tarantino. Tiene más risas, por una cosa, y más color.

Además, el elenco contiene algunas estrellas y estrellas cercanas: Bruce Willis, John Travolta, Harvey Keitel, Christopher Walken, Uma Thurman y Samuel L. Jackson, entre otros.

Sin embargo, la trama es bastante complicada. En realidad, son tres parcelas en una, y la narrativa imprudentemente da vueltas en el tiempo.

En cualquier caso, el suspenso es intenso: nunca se sabe qué sucederá a continuación, incluso si ya lo ha hecho.

En lo que he llegado a considerar como la historia principal, Travolta y Jackson interpretan a Vincent Vega y Jules Winnfield, Los Angeles golpean a los hombres con un problema. Alguien ha sido asesinado a tiros en su auto, y necesitan limpiar este desastre pronto.

En la historia No. 2, a Vincent se le asigna cuidar de la impredecible esposa de su jefe, una posible actriz de televisión llamada Mia (Thurman), mientras el jefe está fuera por negocios. El tercer relato se refiere a un luchador profesional (Willis) que cruza dos veces a ese jefe de la mafia.

Hay una gran cantidad de disparos, asesinatos y una variedad de golpes, así como una violación y una escena inolvidable en la que Vincent hunde una jeringa en el corazón de alguien.
        
            