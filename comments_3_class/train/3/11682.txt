
                      Star Wars… suspiro, Star Wars: Episodio IV - Una nueva esperanza, es la mejor película de ciencia ficción independiente de todos los tiempos. Cuenta una historia completa y puedes disfrutarla sin mirar otro segundo de Luke Skywalker, Han Solo, Darth Vader, R2-D2 y compañía. Pero al hacerlo, te privarás de la mejor película de ciencia ficción de todos los tiempos: Star Wars: Episodio V: El Imperio Contraataca, sin necesidad de clasificatorios. Esta es una obra maestra que influye en la generación, como la que rara vez hemos visto desde entonces.
        
            