
                      Al conocer muy bien el mundo de la danza, Pina es la primera película en rendir un magnifico homenaje a la danza y al teatro. La puesta en escena está muy cuidada, los cuadros y los decorados magníficos, las coreografías y la música convincente, divertidas, cómicas, emocionantes. Todo con un 3D muy logrado y por una vez necesario. Pina Bausch está muy bien presentada y las entrevistas de los bailarines son necesarias para entender mejor el universo tan teatral y poético de la coreografía. Una película maravillosa...
        
            