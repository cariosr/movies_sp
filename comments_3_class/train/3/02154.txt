
                      M. Night Shyamalan está de vuelta. Y no lo digo porque acabe de estrenar su nueva película, “The Visit” (2015), sino porque con ella ha vuelto a encontrarse a sí mismo. Imaginad lo feliz que soy.

El director Indio que nos sedujo con su particular forma de hacer cine en “The Sixth Sense” (1999), “Unbreakable” (2000), “Signs” (2002) y “The Village” (2004) se había perdido entre proyectos de encargo y presupuestos desorbitados. Tras el estrepitoso fracaso de sus dos últimos trabajos “The Last Airbender” (2010) y “After Earth” (2013) la crítica se cebó definitivamente con Shaymalan, catalogándole como vendedor de humo.

Cuando se anunció el tráiler de “The Visit” la cosa no parecía muy esperanzadora. Otra película más basada en el más que trillado subgénero del “found footage” promocionada como película de terror. Aún presente la polémica que surgió con la campaña de “The Village” (vendida como película de miedo y monstruos aunque no fuese la realidad en absoluto) el escepticismo conseguía ganarle el pulso a la expectación.

El argumento, simple, cuenta la historia de dos hermanos que se disponen a conocer por primera vez a sus abuelos maternos, y deciden grabar un documental con la esperanza de descubrir el motivo por el que su madre y sus abuelos no se hablan desde hace 15 años.

Se explica por tanto la sorpresa. Shyamalan se aleja de todo lo que había contaminado su cine para regalarnos una película sencilla, un cuento de dos hermanos que guarda claros paralelismos con “Hansel y Gretel” y que destaca, inesperadamente, por su humor. Un humor puro, sin artificios, que en ocasiones surge de manera espontánea y en otras se nutre del lado más cínico del director.

No todo son risas. La comedia es en este caso negra, y en la oscuridad son los sustos los que tienen reservado el papel protagonista, funcionando bastante bien la combinación entre los momentos de terror y suspense con el humor que destacábamos anteriormente. Es curioso como pasamos de estar en tensión a reír nerviosamente de manera bastante natural, lo que refleja el grado de empatía que logra conseguir una puesta en escena sencilla en conjunto con unas excelentes actuaciones.

Y es que los personajes de “The Visit” están cuidados con mimo, tanto, que Shyamalan se dibuja a sí mismo en la hermana mayor (Olivia DeJonge) una futura directora de cine que recuerda mucho a él tanto por su inteligencia y estilo como por su ego y pretenciosidad. Destacar también el trabajo de Deanna Dunagan, que da vida a una abuela entrañable cuando cocina galletas pero espeluznante cuando pasan de las 9:30 de la noche. Y, sobre todo, el papel de Ed Oxenbould como el divertidísimo hermano menor, que nos gana desde el primer momento con su particular forma de expresarse sin decir palabrotas.

Como viene siendo habitual, el giro de guión final marca de la casa existe, aunque no posee un alto grado de sorpresa ni creo que pretenda ser imprevisible, ya que funciona correctamente como aperitivo previo al clímax sin necesidad de muchos alardes. Ojito, eso sí, con las dos últimas escenas, donde, por un lado, se explota la carga dramática de una familia desestructurada (si en otras películas del director los mensajes que se explotaban eran el amor o la fe, aquí lo es el perdón), y por otro, se nos regalan unas últimas risas.

Difícil hablar más sobre una película de Shyamalan sin desvelar nada, lo mejor, ir a verla sabiendo que el director ha vuelto a encontrarse, demostrando que el amor que siente por el cine puede volver a dar grandes frutos en forma de buenas películas.
        
            