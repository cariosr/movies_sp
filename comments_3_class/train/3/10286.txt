
                      Simplemente maravillosa, una historia narrada desde y para el corazón. Intimista, sencilla, pero llena de grandes momentos de ternura a pesar de la crueldad de la época en la que sucede la historia.
Aún existe lugar para la esperanza.

Y para todos aquellos que no lean, comiencen a hacerlo, no se pueden llegar a imaginar el mundo que se puede abrir ante ustedes.
        
            