
                      Adaptando un cortometraje de sus inicios, Tim Burton nos regala esta maravilla animada, homenaje al Frankenstein de Whale y con todos los ingredientes de su concepción de la animación: Su estilo tan característico, la vida, la muerte, ese aire tan gótico que impregna sus trabajos, sea como director (La novia cadáver) o como creador (Pesadilla antes de Navidad). En Frankenweenie nos queda esa nostalgia de la niñez desde el universo particular de Burton que tan bien sabe retratar, tocando la fibra del espectador.
        
            