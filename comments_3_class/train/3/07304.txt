
                      Ryan Reynolds nació para ser Deadpool. Hace años que deseaba esta película y al final se hizo realidad. Excelente película de este personaje antihéroe de los cómics de Marvel. Fiel rompiendo la cuarta pared y muy entretenida. Me encanta el estilo de este personaje con espada y pistolas, uno de los motivos principales por el que es uno de mis personajes favoritos de Marvel.
        
            