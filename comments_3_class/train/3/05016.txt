
                      Al igual que Freddy contra Jason o Godzilla contra Mechagodzilla, el enfrentamiento entre Alien (cara-abrazos, boca vagina-dentata maltratada) y Predator (dreadlocked, vagina-dentata mawed) es inútil e inexplicable. No espere iluminación en el rencor entre estos dos. En vez de eso, espere un poco de comezón que sea un millón de veces mejor que el AVP de acción del 2004. ¿El dinero tirado? El vientre de una mujer embarazada explota, revelando una camada de bebés extraterrestres. Aliens vs. Predator: Requiem es una película B+ que realmente gana su B+
        
            