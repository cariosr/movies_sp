# movies_sp
Source of comments: http://www.sensacine.com/peliculas/mejores/comentarios/
Corpus on spanish. With the aim to test a sentiment analisys task.
10 levels of rating: 1,2,3,4,5,6,7,8,9,10
For different number of class

On comments_bin folder:
\\ For a homogeneus sample over classes. It is recomended use 4300 amount for train sample and 1080 for test sample. //
Splited in 2 classes. 
pos: 1,2,3  in total 5418 rewievs
neg: 8,9,10 in total 15467 rewievs

On comments_3_class folder:
\\ For a homogeneus sample over classes. It is recomended use 4300 amount for train sample and 1070 for test sample. // 
Splited in 3 classes. positive, neutral and negative
pos: 1,2,3  in total 5418 rewievs
neu: 5,6 in total 7496 rewies
neg: 8,9,10 in total 15467 rewievs

On comments_5_class folder
\\ For a homogeneus sample over classes. It is recomended use 2850 amount for train sample and 640 for test sample.  //
Splited in 5 classes. 
1: 1,2 	4000
2: 3,4	3520
3: 5,6	7496
4: 7,8	10570
5: 9,10	9424

On comments_10_class folder:
\\ For a homogeneus sample over classes. It is recomended use 1130 amount for train sample and 270 for test sample.  //
10 Class: 1,2,3,4,5,6,7,8,9,10
1: 2062
2: 1938
3: 1418
4: 2102
5: 2588
6: 4908
7: 4531
8: 6043
9: 2696
10: 6728
