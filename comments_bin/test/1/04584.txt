
                      Llevo meses esperando con ilusión la "Regresión" de Amenábar después de 6 años de ausencia y el chasco que me he llevado ha sido tremendo. Puede que tuviese demasiadas expectativas en este film pero me ha parecido insípido y totalmente previsible. Todavía estoy esperando un final que me deje con la boca abierta y pensando horas y horas en el contenido para cuadrar todas las pistas que nos iba a dejando con maestría el director en "Los Otros". Resumiendo... la ilusión se ha convertido en desilusión.
        
            