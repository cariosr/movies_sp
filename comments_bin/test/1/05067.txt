
                      Plantea el problema moral y etico de forma pesada y reiterativa. Al final no existe coincidencia entre la conclusion en off del prior y la exaltacion del modelo de vida monacal que nos muestran a lo largo de la narracion. Del contenido global de la peliculan pesan mas las imagenes y situaciones sobre la opcion espiritual de los monges que el hecho de una vida dedicada a los demas por encima de las creencias religiosas. La pelicula no refleja el drama terrible al que se ven sometidos sus protagonistas.
        
            