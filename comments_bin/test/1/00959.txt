
                      Mala, mala, hasta decir basta! 
Lenta, sin ritmo, argumento con muy poco sentido, penosa realización y lamentable interpretación...
No salí antes de terminar, para poder dar mi opinión y que quien lea esto uniré el error que yo cometí!
        
            