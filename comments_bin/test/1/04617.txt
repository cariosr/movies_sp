
                      Amélie es una comedia romántica dirigida por el francés Jean-Pierre Jeunet.

Esta película está ambientada en el 97, en París. La protagonista pasó la infancia sin salir de casa, debido a una falsa enfermedad de corazón que creó su padre. Cuando tiene la edad, Amélie se va de casa.

Ella trabaja en un modesto bar de camarera, y un día decide empezar a hacer cosas buenas por los demás.
Cómo Amélie va ayudando a las personas de su entorno, olvidándose de sí misma, es realmente inspirador.
Ella va perdiendo oportunidades en el amor por el miedo a arriesgarse, pero todas las personas a las que ha ayudado le devuelven el favor.

La personalidad de la protagonista es bastante siniestra, la forma en la que mira y cómo se comporta debido a su desafortunada infancia, quitan parte del encanto a la película.

La película fue nominada a varios premios por guión original, banda sonora y mejor diseño de producción, pero sólo ganó una pequeña parte de ellos.
Las escenas y los vestuarios son de colores llamativos, que combinan a la perfección con la extravagancia de los personajes.
        
            