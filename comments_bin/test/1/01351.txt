
                      Película mala de solemnidad. Tanto desde el aspecto de su presentación,guión, dirección e interpretación.Lo único que se salva es su realizador.Si lo observa un militar se moriría del pasmo: Un batallón frente a un Sherman blindado y vulnerable yanki. Mención aparta merecen sus palabrotas y blasfemias repetidas (perseguibles según los arts. 524 y 525 del código penal español).Ofenden a cualquier persona cristiana.Sus imágenes escabrosas sobran en un tema tan vil,cruel y cruento como es la guerra.
        
            