
                      Ana de Armas, la eterna “Carol” de “El internado“, fué una de las jóvenes promesas famosas por aparecer en series de televisión, a las que engañaron para rodar aquella bazofia llamada “Mentiras y gordas“. Si, negaré haber escrito este título en mi blog, y por supuesto, me avergonzaré siempre de haber pagado en un cine para verla, y mas aún, para convertirla en la película española más vista de 2009. De armas, huyó a su país natal Cuba, para rodar esta película a medio camino entre las películas de mujeres acosadas y el cine de suspense.

A medio camino, que es donde se queda. Pues en la película, aunque tengamos al personaje atrapado en la lavandería de un callejón, el suspense es mas bien nulo. Y es que, de las escenas que mas podrían sacar partido, son las que se quedan sin más en ese callejón. Los momentos de telefilme barato se van sucediendo de tal forma que, nos da hasta igual lo que les pase a los personajes.

Y es que no hay quien entienda absolutamente nada del comportamiento de éstos. Para empezar, la protagonista aparece en el callejón asustada ¡y es donde ella realmente vive!, el psicópata aparece y desaparece a su antojo y hasta las lavadoras de la lavandería y los móviles funcionan cuando quieren.

Esperemos que a Carol Ana de Armas, le vaya mejor en su salto a Hollywood. Aunque no lo dudamos, pues a pesar de haber estado en “Mentiras y..” (Vale, con una vez es suficiente) y en esta película, confiamos en ella para que sepa elegir mejor sus papeles. Más, sabiendo que en uno de esos papeles está involucrado el genial Eli Roth.

Lo mejor: que solo dure hora y diez.
Lo peor: que el trailer te spoilee absolutamente TODA la película.
        
            