
                      Hola a tod@s,

este primer domingo de estreno fui por la mañana a ver esta película. Mis hijos tienen por costumbre ir al cine o al teatro una vez cada fin de semana con lo costoso que eso significa y nunca se habían dormido, uno de ellos si lo hizo, de hecho a los 20 minutos me preguntaron si era muy larga...
Movimientos de cámara para niñ@s de tres años, gags sin gracia, acción para niñ@s de uno y diálogos desproporcionados en ocasiones para adolescentes en ocasiones para aburrir al más pintado. 
Como adulto y pedagogo que soy me asustan los valores que se trasmiten en el film: una vaca que no le gusta como es y se va con un grupo de pijas famosas que se operan las tetas (expresión literal que sale en la peli: me he hecho un trasplante de ubres), por tanto lo que motiva es ser una estrella, no vale con ser normal.
Sus amigos la van a buscar y la rescatan del problema en el que se mete, pero ojo hay unos soldados ardillas y un rey que valoran en una pasarela si las ovejas están buenas y son guapas; las que son feas no las quieren. Supuestamente esperaba que los valores finales de la película fueron la amistad y la bondad pero no, hasta en la última escena el rey proclama un: ¡oh, qué guapa es la vaquita!

Sinceramente, me sabe mal que buenos técnicos de animación y presupuestos como este no hayan sido asesorados por profesionales de la enseñanza, de la psicopedagogía y en general de los valores que debe aprender un@ niñ@. No vale con reproducir lo que muestra la sociedad.

Gracias.
        
            