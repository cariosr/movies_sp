
                      no se a que viene tanta aclamación por parte de los críticos. En mi humilde opinión es una peli lenta, aburrida, tediosa y carente de interés alguno. Te pasas todo el film pensando "Ahora, ahora vendrá algo interesante, ahora viene lo bueno..." y no, ese momento no llega nunca. Gosling se pasa toda la película con la misma cara de tristón haciendo un papel de lo más insípido. La banda sonora también tan aclamada... hay que admitir que va totalmente acorde con el resto del rodaje, es igual de aburrida.

