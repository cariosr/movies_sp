
                      La historia es la misma que la original. Se trata más de un remake con dos detalles nuevos que de una nueva aventura. Todo es predecible y rápido. No tiene la tensión de la original. Como punto positivo es la aportación de una nueva idea de dinosaurios híbridos creados genéticamente. Volveré a ver Jurassic Park para quitarme este mal sabor de boca.
        
            