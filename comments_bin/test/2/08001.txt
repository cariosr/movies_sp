
                      Después de los enfrentamientos vistos en Avengers 1 y 2 entre Tony y Steve, finalmente sus diferentes ideales explotan en esta entrega. 
Civil War nos muestra diferentes argumentos que dividirán en dos a los fans que apoyan los ideales de Tony y los de Steve.
Los puntos más fuertes de la película son las batallas que tienen entre los miembros de los Vengadores y sobretodo el peso que tienen los nuevos miembros en la película: Spiderman y Black Panther.
Del otro lado encontraremos un villano irregular que finalmente consigue fracturar desde dentro a los Vengadores.
Una gran película que nos dejará con un sabor agridulce al final de ella.
        
            