
                      Rain Man es un drama dirigido por Barry Levinson, director de 'Good Morning, Vietnam', 'Bugsy' y 'El Mejor'.

Charles Babbitt (Tom Cruise) es un vendedor de coches al que le va bastante bien. Al enterarse de que su padre ha muerto, acude al funeral, y a por su herencia. El abogado informa a Charles que solo se queda con el coche de su progenitor, y todo el dinero va a parar a su hermano mayor Raymond (Dustin Hoffman), que padece autismo, y del que Charlie no sabia nada.

En este filme se nos hace presentes de la maduración como actor de Tom Cruise, que se vuelve visible en sus siguientes películas.
Dustin Hoffman está sencillamente extraordinario. La postura de su cuerpo, su mirada perdida y su atropellado modo de hablar, forman el conjunto de su premiada actuación.

Este drama conmueve a cualquiera. Narra la bonita historia de dos hermanos ya mayores, que empiezan a conocerse y a pasar tanto buenos, como malos momentos juntos.

La película obtuvo veintidós nominaciones, de las cuales ganó nueve premios.
        
            