
                      Es un lugar común de distribución cinematográfica que, aunque la gente siempre dice que quiere ver más títulos antiguos en la pantalla grande, las masas permanecen obstinadamente en casa cuando se programan esas películas. Dos excepciones se oponen constantemente a la tendencia: las películas de Alfred Hitchcock y las dos primeras partes de El Padrino. Por lo tanto, no es una gran sorpresa ver a The Godfather Part II recibir una revisión digital por su 40 cumpleaños. La película todavía parece una maravilla absoluta.

Francis Ford Coppola vivió varias pesadillas durante la producción del Padrino original. El estudio, inseguro sobre su director, estaba constantemente a punto de despedirlo. ¿Quién era este chico de Al Pacino? ¿No era absurdamente oscura la cinematografía de Gordon Willis? ¿Seguro que ya nadie quiere ver a Marlon Brando?

Nota: 4/4
calificación: Fresh
        
            