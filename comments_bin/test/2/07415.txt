
                      Sin duda un esencial para los fans del Thriller psicológico.
El maquinista traspasa las barreras de lo que seria una simple película.
Posee un peculiar misterio que la hace altamente inquietante y sobrecogedora.
Es curioso que una producción tan” oscura” este dotada de tan buena fotografía.
De brillante guión y ambientación.
Destacar indudablemente la fascinante interpretación de Christian Bale que se supera a si mismo.
Se dijo que perdió la friolera de 30kg, lo que prácticamente puso en peligro su integridad física y sin duda, hizo  de” El Maquinista”, una mas que buena película.
        
            