
                      Sorprende hasta dónde puede llegar una película con un planteamiento tan limitado. Un thriller para no parpadear, y encima cargado de crítica política. Una durísima 
metáfora del atolladero de Irak. Probablemente lo mejor que hay en cartelera ahora mismo.
        
            