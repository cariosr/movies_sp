
                      El camino iniciado por "Mi hija Hildegart" y continuado por " The Village" llega aquí a su ultima estación ( por el momento). Película desasosegante que no ofrece ni explicaciones ni conclusiones. No exenta de un humor malsanamente inocente, esta cinta griega es una de las apuestas mas sorprendentes del año y desunida hasta al espectador mas versado.
        
            