
                      El film implanta el subgénero del thriller psicológico. La atmósfera es densa, opresiva, agobiante y, sobre todo, terrorífica, gracias al despliege de la extraordinaria imaginación de Hitchcock. Construye con habilidad el perfil psicológico de los personajes, en especial de Norman Bates y Marion Crane (Janet Leigh).
Toda una joya del cine.
        
            