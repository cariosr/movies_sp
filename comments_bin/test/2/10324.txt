
                      Estamos ante la que sin duda ha sido la mejor película de ciencia ficción de 2009. Ciencia ficción de la buena, de la que con una ficción sólida aprovecha para contarnos una historia que puesta en sus términos reales sería prejuzgada en lugar de debatida.
Nos situamos en Sudáfrica, concretamente en una de sus ciudades más pobladas: Johannesburgo. Un día sin previo aviso aparece una nave alienígena sobre la ciudad, permaneciendo estática. Los habitantes de la ciudad los recibieron con los brazos abiertos, pero hoy, 28 años después, la situación es diferente.
Los alienígenas viven en campos de refugiados, aislados del resto del mundo, tratados como una especie inferior, aunque el gobierno se empeña en demostrar que en realidad velan por su bienestar.
Wikus (Sharlto Copley) es un funcionario del gobierno, que un día en una inspección rutinaria es expuesto a un agente biológico. Y a partir de ahí experimenta el recelo que sufren los alienígenas por parte del resto de la población humana.
Una forma curiosa, a la par que entrentenida, de abordar temas como el apartheid, que puede lograr en el espectador una reflexión más allá de los prejuicios que pudiera tener inicialmente, recuperando una de las formas de uso tradicionales de la ciencia ficción.
        
            