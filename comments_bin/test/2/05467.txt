
                      Una comedia romántica desde un diferente punto de vista. Me encanta la actriz, Rachel Mcadams, como nos tiene acostumbrados, nos muestra su faceta más romántica. Él, sin embargo, no se queda atrás con el buen papel que interpreta en esta película. La recomiendo porque: es original, te lo pasas bien viéndola, y es de esas películas, que te gustaría volver a ver. Muy buena, no creo que sea imprescindible, en general. Para los fans de las comedias románticas, por el contrario, no se la pueden perder.
        
            