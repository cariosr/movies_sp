
                      Ustedes se están haciendo las mismas preguntas

Una pelicula muy buena,4 estrellas para ella,la he disfrutado mucho,Rodrigo Cortés hace una de las mejores peliculas españolas,si no la mejor del año,una pelicula digna de directores del calibre de Nolan,una pelicula con una dirección fantástica,unos actores de la talla de Cillian Murphy,visto en otros peliculones como el Batman de Nolan o "Inception" (También de Nolan),un gran actor,Sigourney,menos conocido por mí,ha hecho un buen papel pero Robert,Robert De Niro es el auténtico genio de la interpretación,aqui está sublime,la pelicula sin él se resintiría mucho,sin duda uno de los pilares más importanes del filme de Rodrigo,De Niro continua siendo espectacular,uno de los mejores actores de la actualidad y de la historia. Si hablamos del guión,de la historia y el argumento me ha parecido muy interesante,unos buenos pequeños sustos(muy pequeños) y es que Robert consigue dar auténtico miedo,una historia bien llevada pero con un final que me ha dejado un poco frío.

Muy recomendable,hace dudar.
        
            