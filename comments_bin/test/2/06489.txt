
                      Ya lo dijo un crítico "Dark Phoenix es más sólida que espectacular" y como saga X-men se ha elevado y caracterizado por eso. No ha sido un mero espectáculo risible ni al estilo "bang-bum", sino una saga que ha sabido entretejer sus tramas con cautela, con inteligencia y precisión. 
Otro crítico o medio dijo: "Dark Phoenix no ha ofrecido nada que previas entregas no hayan tenido"; seamos honestos, si vemos la primer trilogía y consecutivamente la segunda, las similitudes son grandes y no por eso X-Men ha fallado, todo lo opuesto, lleva su sello a lo largo de sus entregas.
Si en algún momento Dark Phoenix se ha sentido apresurada, ha sido por las circunstancias que ha tenido en contra y todos sabemos que han sido muchas. Aún así, logró lo que pocos esperábamos y a Mí, como película de acción me ha parecido un bomba, emocionante y atractiva.
        
            