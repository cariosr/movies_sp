
                      Según mi opinión, el mayor logro cinematográfico de Lars Von Trier y una de las mejores interpretaciones de la historia, por parte de Björk. Ésta fue nominada a los globos de oro por esta interpretación (sin ganar) pero no obtuvo la nominación a los Óscar, imperdonable ya que a mí me parece mejor su trabajo que el de otras nominadas de aquel mismo año como por ejemplo: Juliette Binoche y Laura Linney.
Gran drama con musical aunque la trama empieza cuando ocurre el asesinato. Antes de ello hay escenas que se pueden hacer pesadas por el ecceso de guión.
        
            