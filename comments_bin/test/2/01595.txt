
                      Apta para fans y no fans del mundo magico de J.K. Rowling

Animales fantásticos y dónde encontrarlos no tiene nada que envidiarle a sus antecesoras películas magicas. 

Eddie Redmayne se luce, interpreta un personaje muy carismatico y con un historia intrigante detras de él que nos deja con el sabor de "quiero ver mas".

Por otro lado David Yates sigue con su esencia intacta. Todo lo que nos fascinó en las anteriores entregas de Harry Potter podemos volver a verlo aquí, pero en un contexto diferente, situandonos en New York y no en Inglaterra.

Sin dudas, como dije al principio, si no eres fan de los libros de J.K Rowling no te preocupes... Animales fantásticos y dónde encontrarlos es para todos, es un nuevo comienzo.
        
            