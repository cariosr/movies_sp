
                      Es una película extraña, escenas largas y lentas, muchos silencios, primeros planos, ideada y desarrollada de una manera que te mantiene extrañamente pegado todo el tiempo, como hipnotizado. Además, no sabes como va a continuar, ni como va a acabar. Es un tanto desconcertante. La recomiendo aunque no a todo el mundo le gustará.
        
            