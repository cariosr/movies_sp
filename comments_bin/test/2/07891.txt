
                      Kingsman, una pelicula que sorprende. Comenzare diciendo que con el trailer dejaba claro que seria distinta a todas las cintas de espias, como un James Bond pero menos serio y con menos rellenos y mas diversion. Kingsman nos entrega una nueva version de las cintas de espionaje con accion de calidad, humor perfecto, no exagerado y una trama completa, todo acompañado por personajes entretenidos y entrañables y con divesion en todo momente sin rellenos. El villano, al mas puro estilo de Mark Millar (creador de los comic de Kick Ass) intrepetado perfectamente por Samuel L. Jackson, un personaje divertido, chistoso y calculador. Por el lado de los buenos el papel de Harry Hart interpretado por el extraordinario Colin Firth es un Bond pero con mas actitud y carisma, mientras que el protagonista Eggsy es interpretado por un desconocido Taron Egerton, quien sorprende con una buena actuacion que nos permite pensar que tiene un futuro prometedor. Kingsman sera sin dudas otro acierto de Millar llevado a la pantalla grande, la dirección de Matthew Vaughn claramente estuvo pensado en hacer algo diferente a las peliculas de espias dandole humor, accion y escenas de peleas al estilo de Kick Ass, un acierto en todos los sentidos. 

En resumen, Kingsman, cinta de espias al estilo Kickass.

Lo Mejor: los personajes, el guion y las peleas.
Lo peor: mmm...nada, no le doy un 5 porque no esta al nivel de obras maestras.
        
            