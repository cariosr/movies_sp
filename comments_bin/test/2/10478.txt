
                      My Fair Lady es una película basada en el musical de Alan Jay Lerner y Frederick Loewe. Está dirigida por George Cukor, director de Lo que el viento se llevó, Vivir para gozar e Historias de Filadelfia.

Henry Higgins (Rex Harrison) es un prestigioso y arrogante profesor de fonética, que cree que la clase social es definida por el habla.
Higgins intentará enseñar a Eliza (Audrey Hepburn); una deslenguada florista callejera, a hablar y comportarse como una dama.
Hugh Pickering (Wilfrid Hyde-White) lo retará, y le dará seis meses para enseñar a Eliza y llevarla al baile anual de la embajada para probarla.

El vestuario es precioso y los escenarios majestuosos. Todo encaja en una maravillosa armonía gracias a la dirección.
El reparto de actores es estupendo, destacando el trío protagonista. Hepburn está en uno de sus mejores papeles y Harrison, que ya había interpretado a Higgins en Broadway, está insuperable.

Frederick Loewe se encarga de la música. La banda sonora está compuesta por unas canciones míticas, a la par que hermosas. Esta es considerada una de las mejores bandas sonoras de la historia, y no es para menos. Audrey Hepburn fue doblada en todas las canciones por la soprano estadounidense Marni Nixon, lo cual no sentó muy bien a la actriz.

La película obtuvo veinte nominaciones a premios diferentes, de los que ganó trece; ocho eran Oscars.
        
            