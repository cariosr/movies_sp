
                      La nueva y apabullante película de Nolan es más arriesgada que las anteriores, está narrada con un realismo íntimo y profundo, y se adhiere -gracias a la fotografía de Hoytema y la música de Hans Zimmer, entre otros- a una experiencia visual fascinante. Apenas tiene diálogos e incluso le sobran, aun gozando de actores reputados (Rylance, Branagh) que han decidido dar vida a verdaderos supervivientes. Una obra maestra del cine bélico.
        
            