
                      Memento, recuerdos de un crimen.

Memento fue sin dudas una de las grandes sorpresas del comienzo del siglo XXI. Una gran obra de culto. 

Si bien "Following", fue el largometraje con el que Nolan se estrenó en la pantalla grande, fue con Memento con la que se "aseguro" su lugar en Hollywood. Y bien merecido lo tiene.

Un film totalmente original, que sorprende. Con una trama que se mueve hacia atrás en el tiempo, muy inteligente y sin tropezarse en el trayecto.
        
            