
                      Una película de gangsters, de nuevo. Pero realizada por un casting genial, y una Keira Knightley como nos gustaría verla más seguido. El escenario se entiende, pero mantiene el interés de todas formas, gracias a un Colin Farrel casi tan bueno que como en La Prueba. Super sonido original.
        
            