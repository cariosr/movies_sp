
                      Captó mi atención todo el tiempo. Si hay algo indiscutible es q la película te atrae  y te absorbe durante los 150 minutos que dura. Una dirección buena, una interpretación excelente por parte de todos, un argumento del mejor suspense, una buena fotografía, sobre todo en las lluviosas escenas nocturnas, en definitiva una gran película para los amantes del género.  La única pega es la introducción en el argumento de un personaje ( no digo cual)
 cuya única misión es complicar un poco al espectador para q las cosas no estén claras hasta el final.
        
            