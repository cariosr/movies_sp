
                      “Las divertidas aventuras de otra guerra de las galaxias”

En plena explotación cinematográfica del universo Marvel, en un momento en que decenas de superhéroes ocupan sistemáticamente nuestras pantallas, donde primeras, segundas e infinitas partes de las nuevas aventuras de “Iron man”, “Spiderman”, “Capitán América” u otros “Vengadores” atrapan nuestra atención y nos conducen al cine cual soga tirando de nosotros, nos llega una película basada en un cómic, en principio, bastante desconocido para la inmensa mayoría.
“Guardianes de la galaxia” supone un golpe de aire fresco para las ya demasiado exprimidas andanzas de viajeros espaciales, rememorando, con su viveza, el espíritu de las tres primeras partes de “Stars Wars”, y construye un camino esperanzador para un posible resurgimiento del género. 
Ésta película huye de todo encorsetamiento y nos ofrece dos horas de la más pura comedia de acción y aventuras, con unos personajes muy definidos y bien construidos, un guión y dirección excepcionales, y unos efectos especiales impresionantes. Sin olvidar las innumerables referencias al  mundo del cómic, o la aparición de algún que otro personaje de nuestra infancia cinéfila (esperad hasta el final de los títulos de crédito).
El reparto actoral cumple perfectamente con su cometido, destacando especialmente Chris Pratt como Star-Lord, el torpe líder del grupo, o Zoe Saldana como Gamora. El resto de los guardianes lo completan: el luchador de Wrestling Dave Bautista como Drax el Destructor y las animaciones Rocket y Groot, cuyas voces fueron aportadas por Bradley Cooper y Vin Diesel, respectivamente (en la versión americana, por supuesto).
En definitiva, “Guardianes de la galaxia” es una película muy recomendable para aquellos que disfruten de batallas interestelares o, simplemente, que busquen acción y comedia a partes iguales.

Lo mejor: Perter Quill/Star-Lord, Gamora, Drax el Destructor, Rocket y Groot.

Lo peor: La recreación de Thanos, líder de los villanos.
        
            