
                      Perfecta, Impresionante... Podría usar miles de sinónimos para describirla, pero siempre echaría en falta uno más. La vi de pequeño cuando daban por la tele la serie en la 1 y me la regalaron en VHS, a día de hoy me sigue pareciendo la mejor película de Batman. Gracias a la película descubrí a mi versión favorita de Batman, su universo y a mi personaje favorito: Joker. 

La recomiendo al 100% a todo el mundo, sin excepción
        
            