
                      Rara, atípica, y excéntrica cinta de humor negro de los Coen. Los hermanos construyen esta vez una secta de personajes torpes y aparentemente normales, se ríen de ellos y provocan alguna carcajada, y de telón de fondo, como es habitual en sus películas, esconden esa severa crítica a la religión judaica y a la vida cotidiana. El trabajo visual es bueno, peor aún lo es más el actoral: Stuhlbarg merece mayor reconocimiento.
        
            