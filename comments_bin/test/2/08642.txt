
                      Pixar lo volvió a conseguir, la sencillez de una premisa convertida en un mundo que se apalancará en lo más hondo de nuestros corazones a través de sus personajes, emocionándonos y divirtiéndonos a partes iguales.
        
            