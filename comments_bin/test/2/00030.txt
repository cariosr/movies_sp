
                      Ni por asomo me esperaba de estos dos actores "descafeinados" una película así. Violencia, sangre, gore... Una delicia para los amantes de las escenas violentas. Grata sorpresa y espero que haya una secuela, pues me ha dejado buen sabor de boca.
        
            