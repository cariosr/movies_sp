
                      Las dos primeras películas de la franquicia animada de Toy Story de Pixar son brillantes guiones, modelos de técnica calentados por el humor y la imaginación. Toy Story 3, que vendrá 11 años después del último, debería haber sido una decepción. Quiero decir, ¿qué tan difícil puede exprimir un ganso de oro antes de que el ave llame a un paro laboral?

Sin preocupaciones. Toy Story 3, adornado con una ingeniosa y no vistosa 3D, es un placer contemplar. Presiona todos los botones, desde la risa hasta las lágrimas, y te eleva en oleadas de deslumbramiento visual. Y no necesitas llevar a un niño para apreciarlo. Dios, gente, todos éramos niños una vez. Y la forma en que los magos de Pixar recrean el sentido de la maravilla de la infancia es casi genial.
        
            