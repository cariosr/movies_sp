
                      Para mí, la primera parte de Sinsajo me ha parecido muy bien adaptada. Muchos se quejan de que apenas tiene acción, que es aburrida. Pero, si hubieran leído el libro, entenderían que la primera mitad es más lenta, ¡y es normal! ¡No todo es acción! Esta es la parte de la historia de "recuperación" tras los dos primeros libros. Una dósis de realidad antes de la guerra final. 
Y en cuanto al reparto, Josh Hutcherson, que aunque no haya salido mucho, la clavó. Y además, aplaudo por el maquillaje y el realismo de las escenas más "intensas" de la película. Mientras leía Sinsajo, nunca me lo llegué a imaginar así. 
Resumiendo, me ha gustado muchísimo.
        
            