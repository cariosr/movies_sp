
                      OBRA MAESTRA.
Se juntaron todas las artes para alumbrar esta película. Partiendo del Gran Ridley Scott, un elenco como traído de la antigua Roma, un guión bordado como sí fuese alta costura, fotografía espectacular, y que decir de la magistral banda sonora compuesta por el todopoderoso Hans Zimmer. Una película que llegó cuando nadie la esperaba para resucitar el Peplum.
        
            