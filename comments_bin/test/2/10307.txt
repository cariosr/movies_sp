
                      Simplemente, perfecta.

Poco se puede añadir a lo esperado. Una película perfecta de principio a fin. Los efectos visuales, los sonidos (o la ausencia de ellos), la banda sonora y como no una interpretación de vértigo.
Sigourney Weaver es muy grande, ya lo sabíamos, pero ahora está aún más claro.

Para mí solo tiene un error. El trailer te cuenta la película de principio a fin. Pocas o ninguna sorpresa. 

Aún siendo tan buena tiene un problema; el tipo de gente que va a verla. Tanta publicidad hace que vaya mucha gente a buscar una película de acción, película de fantasía o lo que es peor, un árbol que habla implica llevar niños al cine (en nuestra sesión había varios) y eso repercute en los malos comentarios.
        
            