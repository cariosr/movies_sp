
                      ¡Me ha encantado! Me parece un homenaje precioso al cine mudo por un lado, y a grandes películas y grandes actores por otro, como "Cantando bajo la lluvia" y el gran Gene Kelly. Un ejercicio de estilo admirable. La pareja de actores, estupendos. Elegancia, clase y simpatía.
        
            