
                      He de decir que ya fui a ver esta pelicula con el prejuicio de ser la mejor pelicula del año pasado según bastantes criticos especialistas en los que confío y quiza por eso me esperaba mucho más.
La fotografia y el montaje son muy buenos pero el argumento deja coja la pelicula. Lo tiene casi todo, con unas buenas interpretaciones, en especial de los niños y adolescentes, y un magnífico guion pero le falta lo que a una gran pelicula hace grande: un magnífico final. Creo que el final es bastante anticlimático y, sinceramente, no sé qué pretendía el director al dejar la película en ese punto, si es que pretendía algo o sólo se le acabaron las ideas. 
Hay escenas que por sí solas están cargadas de dramatismo y que son prácticamente perfectas pero la unión de todas las escenas para hacer en conjunto el film no llega a cuajar para mi gusto.
A pesar de estos fallos, una película muy superior a la media que estamos acostumbrados a ver.
        
            