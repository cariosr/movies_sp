
                      A mi me encanta, por muchas razones. Como no quiero enrollarme resaltaré lo más básico. Esto es que demuestra que el cine comercial también puede ser un gran cine capaz de sorprender al espectador. Se han dicho tantas cosas que, como bien ha dicho alguien más abajo, lo mejor que se puede hacer es ir a verla para descubrir de qué lado estás tú. Porque lo q está claro es que esta película no deja indiferente a nadie.
        
            