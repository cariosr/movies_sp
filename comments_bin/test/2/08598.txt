
                      Gran película con grandes interpretaciones. Una vez más el cine nos muestra como hemos apartado y torturado a alguien a quien deberíamos dar las gracias cada día por tener el mundo que tenemos a día de hoy.
Benedict Cumberbatch está soberbio y se postula claramente para el Oscar; Keira Knightley no se queda atrás.
        
            