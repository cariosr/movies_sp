
                      La he vuelto a ver despues de mucho tiempo. Me parece una pelicula imprescincible para cualquiera que le guste el cine. La trama, los dialogos, los actores, todo ha sido un acierto en la pelicula. Tim Robbins y Morgan Freeman encabezan un reparto donde todos rayan a gran altura. Los detalles son importantes, debes estar atento a cada escena, por que algo tendra que ver en el futuro de la trama.
¡A disfrutar!
        
            