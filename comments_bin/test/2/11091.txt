
                      Me ha marcado. Una película que transmite tanta felicidad como tristeza. Los actores fantásticos, la banda sonora también muy adecuada. En resumen, 100% recomendada. Hacía tiempo que no veía una película romántica tan buena como ésta.
        
            