
                      Rocky es sin duda la mejor película de Boxeo de la historia, por todo lo que significa el personaje de Rocky Balboa, todo un icono en la historia del cine.

Una bellísima historia de superación y coraje, demostrando desde la humildad que para conseguir tus propositos debes enfrentarte a los obstáculos por muy difíciles que sean de superar. Un entramado reparto de distintos personajes forman parte de la vida social de un boxeador de baja categoría que vive en un barrio bajo de mala muerte.
Alli conoce al amor de su vida. Una chica tímida que tiene por hermano a un alcoholico pero entrañable y gruñón Burt Young.

El guion de Stallone era en un principio una subversión del sueño americano, pero acabó convertido en un reflejo de éste. Rocky es una mezcla entre los púgiles reales Chuck Wepner y Joe Frazier, es el típico don nadie al que se le da una oportunidad y se agarra a ella como una garrapata. Eso caló hondo en el público de todo el mundo, no solo EE.UU. 

Aqui es donde Stallone demostró y para quien todavía siga criticandole de inexpresivo y tópico encasillado en el cine de acción, que no sólo valía para enseñar músculos y disparos. Aqui demostró un talento innegable, por el cúal fué nominado al Oscar y merecidamente.

Que decir de la banda sonora... Una de las mejores melodías que encajan con las imágenes que estamos viendo, soberbio el trabajo del maestro Bill Conti. Hay alguien en el mundo que no sepa tararear el tema principal? Lo dudo.
En definitiva Obra maestra.
        
            