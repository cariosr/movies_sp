
                      es un niño de 13 años que vive en una zona rural de Malawi. Allí las condiciones económicas han empeorado debido al mal clima. Esto además le ha obligado a dejar la escuela, porque su familia no puede afrontar otro gasto que no sea la comida. Para salvar de la hambruna a su pueblo, este joven con una mente curiosa se inspirará en un libro de ciencias para construir una turbina de viento. Con ese molino su comunidad podrá regar los cultivos.
        
            