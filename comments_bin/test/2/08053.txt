
                      Una de las mejores películas de la saga es esta segunda entrega. A pesar de su larga duración, mantiene en todo momento la tensión y el misterio siendo las interpretaciones algo más maduras. La ambientación y los efectos especiales siguen a un gran nivel con algun nuevo personaje que sorprende por su calidad e interacción con la realidad. Se nos van desvelando algunos por qués preparándonos para la siguiente.
        
            