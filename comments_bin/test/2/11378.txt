
                      Sin duda una película que promete ser mucho mas que un taquillazo justificado .
Cumple sobradamente con la promesa de un buen cine lleno de grandes actores e interpretaciones, buena música y buenísimo, buenísimo guión llevado a cabo de modo elegante y directo .Un drama con mayúsculas que llega .
Una vez mas se nos muestra (vemos) un Christian Bale camaleónico y espectacular que bien merece un oscar.
        
            