
                      Tom Ford sorprende con esta adaptación de la novela 'Tres noches'. Una película que te atrapa desde el primer minuto gracias unos personajes con los que conectas y junto a los que compartes todos sus dramas y dificultades. Amy Adams merece el Oscar ya.
        
            