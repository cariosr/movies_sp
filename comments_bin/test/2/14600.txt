
                      Amenabar no defrauda, y eso es algo que con cada película que dirige queda más claro.

Ágora nos cuenta una historia de lo que pudo ocurrir en la destrucción de una de las maravillas del mundo clásico: la gran Biblioteca de Alejandría. Y para ello centra la historia en la persecución y posterior ejecución de Hipatia, una erudita que ve como el radicalismo de la sociedad que le ha tocado vivir está descarta el conocimiento si eso puede hacerse tambalear a sus fundamentos.

Lejos de una condena explícita al cristianismo, Amenabar nos ofrece una visión donde el fundamentalismo no entiende de religiones. Da igual si defienden a una deidad o a varias, porque lo único que queda claro es que ignorar los conocimientos adquiridos por la ciencia solo nos lleva a la autodestrucción.

Todo esto además en un entorno social donde la parte de los sabios, los que de verdad admiran el conocimiento, no distinguen entre hombres y mujeres, mientras que el fundamentalismo trata de crear diferencias, definiendo conceptos discriminatorios para aumentar su poder.

Los fallos, pues como todo en esta vida nada es perfecto, son para mí "ignorables", aunque no está de más mencionar que algunos planos de perspectiva forzada pueden desorientar al espectador, así como que poner en boca de Hipatia el descubrimiento de una de las leyes de Kepler --varios siglos posterior-- es quizás hilar demasiado fino.

Con eso, una película muy recomendable, que una vez más pone de manifiesto que cuando se quiere hacer buen cine, se puede.
        
            