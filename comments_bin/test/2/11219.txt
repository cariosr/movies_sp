
                      Creo que es una muy buena película.  Al margen del guión que ya sabiamos lo que iba a pasar.... me quedo con los sonidos (las gotas de agua arrollando por las hojas de los arboles), los efectos especiales espectaculares y la banda sonora. 
¿En alguna otra película os habían transmitido tan bien el aterrizaje de un avión? A nosotros no...

Naomi Watts está espectacular.
        
            