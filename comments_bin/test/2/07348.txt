
                      Una peli muy bien realizada, una historia simple pero impactante, adornada con tantos detalles de una vida en altamar, exaltados con las actuaciones de Crowe y Bettany, capitan y medico respectivamente; en una historia que lleva a la tripulación a enfrentar los misterios que llegan con la aventura y que ponen a prueba el honor y capacidad de nuestro capitán... un film que si te gustan las buenas historias en barcos no debes perderte.
        
            