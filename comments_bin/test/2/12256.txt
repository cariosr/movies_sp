
                      Estas películas me gustan, no por la violencia ni sangre (ya que ese tipo de cosas no me gustan para nada), sino por la historia.  Me cautivó el cómo puede ser posible (viendo la película de un modo realista) que alguien piense en una ley así para intentar una solución.  Me llamó la atención cómo actúa la gente ante la situación y cómo hacen para sobrevivir y pelear. Me encantó.
        
            