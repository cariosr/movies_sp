
                      Motivada por múltiples incomodidades tabú y agarrada a una línea argumental dialogada y concisa, 'Spotlight' alcanza razones de peso para ser merecedora de sus Oscars. Los personajes se construyen a través del habla, de la comunicación, de escuchar y comprender. Y el resultado impacta al espectador, no solo porque se plantea no volver a pisar una iglesia, sino porque también logra entretenerse.
        
            