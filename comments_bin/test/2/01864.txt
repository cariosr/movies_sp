
                      Hasta El Último Hombre (2016).
"En paz, los hijos entierran a sus padres. En guerra, los padres entierran a sus hijos".

El homófobo y ultracatólico Mel Gibson vuelve a ponerse tras las cámaras tras una década sin dirigir, y lo hace a lo grande.
Hay películas entretenidas, películas buenas, muy buenas... y luego está esta; posiblemente, la película del año.

En la línea de cintas antibelicistas como La Chaqueta Metálica o Forrest Gump, Hasta El Último Hombre se divide nítidamente en dos partes: la de la INSTRUCCIÓN de nuestro protagonista (un Andrew Garfield visitando un registro distinto al acostumbrado, y que borda el papel de modo que podríamos estar ante un nuevo Globo de Oro a la interpretación, o ante una nominación a esos premios de menos nivel que estos últimos, pero más glamourosos, como son los Oscar), y la de la GUERRA en sí; concretamente los hechos transcurren en la batalla de Okinawa, donde uno de los pueblos más duros, preparados y valientes de la Tierra, los japoneses, vendieron cara su derrota.

El reparto es de lujo sin duda alguna: el impresionante Vince Vaughn, un caracterizado Hugo Weaving en un papel autodestructivo pero imprescindible para conocer el origen de las convicciones del soldado Doss, su hijo, y que existió en la vida real, salvando a 75 hombres en la citada batalla, la bellísima Teresa Palmer o el varonil "avatar", Sam Worthington.

Los premios avalan los positivos comentarios que acabáis de leer,
entre otros: Globos de Oro- Nominada a mejor película-drama, director y actor (Garfield) y Festival de Venecia- Sección oficial (fuera de concurso), donde Garfield disfrutó de un aplauso de 10'.

Unos efectos tanto visuales como sonoros de escándalo. Un rodaje casi tan realista como Salvar al Soldado Ryan (quizá sea, tras esta, la mejor batalla moderna llevada a la gran pantalla). Y una música maravillosa, hacen a esta cinta merecedora de los mejores elogios.

Imprescindible. No os la perdáis.

Raúl Cabral.
        
            