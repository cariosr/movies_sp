
                      Gunn hace un gran trabajo conservando su extravagante sensibilidad indie e incorporándolas a la perfección en una película de gran éxito, una hazaña que requiere más delicadeza de lo que su currículum de Troma-boy puede hacerte creer que es capaz de hacer.
        
            