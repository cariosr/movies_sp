
                      Corazones solitarios

Dentro de la Nouvelle Horreur Vague francesa, quizá sea el belga Fabrice du Welz el cineasta más sugerente y visceral de todos los que pueblan esa corriente moderna del terror galo, con cuyos filmes su cine se hermana en esencia más allá de la mera coproducción a las que están sometidos sus trabajos. Su visceralidad se inclina, eso sí, más hacia el lado del terror psicológico cargado de simbolismo y libres interpretaciones que hacia el del género entendido como ese festín de hemoglobina al que son adictos compañeros como Alexandre Aja o Xavier Gens.

Con “Alleluia”, du Welz vuelve a las Ardenas, a ese espacio rural en el que Laurent Lucas sufriera su propio martirio en aquella joya titulada “Calvaire”, y ofrece su segunda parte de una trilogía que comenzó hace ya diez años. Y, además, con ella el director consigue su mejor obra desde entonces, después de un periodo ofreciendo unas pocas películas menores y olvidables.

Vuelve el du Welz más enfermizo, el que acerca las pulsiones humanas a límites grotescos, ofreciendo en este caso una nueva versión –muy libre, eso sí- de “Los asesinatos de la luna de miel”. Compone mediante la historia de estos dos amantes condenados por sus bajas pasiones e instintos malsanos a vivir juntos para siempre, caiga quien caiga por el camino, una radiografía del amor llevado al límite de la locura. Dos personajes que encuentran una media naranja plasmada mediante planos y contraplanos en los que cada uno queda tapado por la figura del otro, soberbiamente interpretados por el propio Lucas, actor fetiche de du Welz, y una Lola Dueñas que absorbe la pantalla con su neurótica y obsesiva interpretación.

 “Alleluia” juega a crear repulsión en el espectador mediante su cuidada estética de serie B, que su responsable consigue rodando en 16mm y que acerca al film a ese aroma putrefacto que desprenden obras maestras como “La matanza de Texas” o “Henry, retrato de un asesino”. Y a la vez también le desafía con una escena musical de un humor negro tan desconcertante  como aquel baile de los pueblerinos de “Calvaire”. Todo ello con una realización de primerísimo nivel que demuestra la madurez creativa y profesional del belga.

Por supuesto, no es una cinta para todo tipo de públicos. Porque es provocadora, desagradable e inclasificable. Sin embargo, “Alleluia” es mucho más que todo eso, incluso es más que un retrato de dos corazones luchando contra la soledad. Es, como su propio título indica, una expresión de júbilo. El grito de regocijo de un autor libre, que por fin ha ofrecido la joya que llevaba una década gestando. Si tienen que venir más obras alimenticias para que nos regale algo como esto en el futuro, bienvenidas sean.

A favor: Lola Dueñas, y recuperar al mejor du Welz
En contra: es tan malsana que no es para todo tipo de públicos
        
            