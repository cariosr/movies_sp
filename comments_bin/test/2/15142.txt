
                      Aunque el desenlace puede parecer exagerado, es un filme muy entretenido. Debido a unos vecinos ruidosos tuve que verla con auriculares puestos. Eso agregó suspenso y tensión. La banda de sonidos es estupenda, potencia el misterio. La recomiendo.
        
            