
                      Cuando se junta el genio de ese monstruo de la interpretación llamado Meryl Streep con el casi inmejorable Clint Eastwood (que suerte que dejó de interpretar a Harry el Sucio) en un buen guión, el resultado es lo que consiguieron en su momento con "Los puentes de Madison", un drama romántico espectacular que te hace vibrar cada uno de sus 135 minutos. Totalmente recomendable.
        
            