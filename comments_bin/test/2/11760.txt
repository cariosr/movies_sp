
                      Del Revés es, sin ninguna duda, LA obra maestra de la factoría Pixar que tras grandes títulos como Toy Story, Buscando a Nemo, Cars o Monstruos (y alguna que otra decepción) vuelve a posicionarse en el Olimpo de las producciones de animación. Y es que pocas veces se ha visto como una historia se adentra en temas tan extremadamente complejos como puede llegar a ser la mente humana y explicar, con absoluta simpleza y naturalidad, como funcionamos y cual es el resultado físico de nuestras emociones.

Lee la crítica completa en el siguiente enlace:
        
            