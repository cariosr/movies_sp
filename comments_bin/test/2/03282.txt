
                      Una película en la línea del director Richard Curtís, que por cierto me encanta. Logra que salgas del cine contento, optimista y lleno de buenos propósitos. Un film apto para todo tipo de público. La recomiendo.
        
            