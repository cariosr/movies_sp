
                      Uno de los aspectos más geniales de la película es que la historia no ha estado esperando que el público los obligue a ponerse al día rápidamente. El director / escritor George Lucas pone a los espectadores justo en medio de este conflicto en curso entre el malvado Imperio y la heroicamente superada Alianza Rebelde.

Luke Skywalker (Mark Hamill), un adolescente idealista desesperado por abandonar la desolada granja de Tatooine dirigida por su tío Owen (Phil Brown) y su tía Beru (Sheelagh Fraser), que tienen poco interés en ver a su sobrino convertirse en piloto de caza en el conflicto galáctico . Hamill se pasa por alto con frecuencia en comparación con sus compañeros de reparto, pero encarna al joven adulto que espera una vida mejor y desea tener un impacto positivo a la perfección.

Un par de droides C-3P0 (Anthony Daniels) y R2-D2 (Kenny Baker) pronto cambiarán para siempre el destino de Luke mientras conducen a Luke a Obi-Wan Kenobi (Alec Guinness), un solitario Caballero Jedi convocado para ayudar a la Princesa Leia (Carrie Fisher ). Leia ha sido capturada por el insidioso ejecutor del Imperio Darth Vader (David Prowse y expresado por James Earl Jones). Con su infame respiración, su imponente armazón y su imponente atuendo de color negro, Vader corta una presencia mezquina. Star-Wars-Darth-Vader-Confronts-Princess-Leia

El único agente imperial con la autoridad para comandar a Vader es el único agente imperial con la autoridad para comandar a Vader.

star-wars-grand-moff-tarkin-darth-vaderTarkin quiere usar la arma más poderosa del Imperio, La Estrella de la Muerte, un planeta que destruye la estación espacial, para eliminar a la Alianza Rebelde de una vez por todas.

Obi-Wan convence a Luke de perseguir su destino y convertirse en un Jedi como su padre, quien fue asesinado por Darth Vader cuando traicionó y ayudó a asesinar a la mayoría de los Jedi.

Después de ver toda la saga, es fascinante leer expresiones de personajes y reacciones a nombres y noticias que sugieren un significado mucho más profundo. Guinness está en su mejor momento en las escenas prefigurando eventos que eventualmente se revelarán a medida que la serie se desarrolle. Claramente, Lucas tenía algo de eso en mente, pero es genial ver lo bien que todo se une. Star-wars-chewbacca-han-solo

Con la ayuda de los pilotos Han Solo (Ford) y Chewbacca (Peter Mayhew), Luke puede rescatar a Leia ya que Obi-Wan tiene un encuentro fatídico con Vader.

En gran parte, el rendimiento de Ford es que Han se ha convertido en la plantilla para todos los personajes pícaros y mejores amigos / mercenarios en las películas de ciencia ficción y acción desde entonces. Es difícil encontrar fallas en la estrategia, ya que Han casi roba la película desde el momento en que se presenta.

Luke es un personaje heroico tradicional y Star Wars hubiera estado bien enfocándose directamente en él, pero es la adición de Han (y Chewie) la que lleva a la película a ese nivel tan especial.
        
            