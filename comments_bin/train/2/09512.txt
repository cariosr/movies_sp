
                      Los efectos visuales me han parecido excepcionales desde el primer momento. Aunque no deje de ser otra película de orgullo militar, me ha gustado la alianza compartida por los americanos con otros países y sobre todo Japón. Entretenida e interesante hasta el final. El trailer me hizo creer que era como Invasión a la Tierra, pero ha resultado ser muchísimo mejor. Le doy un 5, porque es la impresión que me ha dado.
        
            