
                      Impresionante reparto, con un gran Kevin Spacey a la cabeza, que narra las horas previas al inicio de la crisis de 2008 desde dentro de una empresa de gestión de activos. La película no es larga, unos 100 minutos, pero tiene tiempo para definir y dar vida a grandes personajes mientras se desarrolla y desenlaza la historia.
        
            