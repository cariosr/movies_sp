
                      Mike Flanagan sorprendió a todo el mundo hace un par de años con “Oculus: el espejo del mal“, una historia de terror sobrenatural con los productores de “Paranormal Activity“, que nos puso los pelos de punta. Ahora, vuelve a unirse a Blumhouse para traernos una home invasion que no trae nada nuevo, y que sin embargo no deja indiferente a nadie.

Kate Siegel, que ya estuvo en “Oculus”, interpreta -y escribe- magistralmente, a una sorda que vive apartada de la sociedad en una casa en el bosque, y que es sorprendida por un psicópata. No, no es muy novedoso, pero si tenemos en cuenta que la película tiene un guión que cabría en un poss-it y que la mayoría del metraje es una película muda que nos trasmite terror y suspense a base de oscuridad, el tema mejora. Utiliza un elemento poco común en estas películas para transmitir el mensaje: la mente de la protagonista, pues ni oye ni puede gritar, y aunque no lo parezca, da mucho juego. La lucha por la supervivencia de una mujer con muy pocos recursos se convierte en el eje central de una historia de suspense exprimida al máxima y con tantos puntazos como cabría esperar.

Mike Flanagan vuelve a dar, en “Hush“, con la clave exacta en una historia más sangrienta que la anterior y que vuelve a situarlo en el podium del género. Justo al lado, por supuesto, de Blumhouse.

Lo mejor: Utiliza para bien todo lo que tiene a su alcance.
Lo peor: que The Newton Brothers, no hayan hecho un score tan potente como el de “Oculus”.
        
            