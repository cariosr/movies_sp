
                      Buenas a todos.
Ayer por la noche acudí al cine con mucha expectación por ver la nueva entrega. Todo empezó de la mejor manera posible, había mucha gente con logrados disfraces de la saga. La tradición y el hecho de conocer la duración de la película, nos motivó a repostar palomitas, como no. Ya sentados al apagarse las luces y salir el logo de Star Wars, los nervios dieron paso a mi entrega sin reparos a la trama.
No puedo decir otra cosa que gracias al director y guionista (que son la misma persona), por dar un giro a la forma de continuar la saga. Después del episodio VII, tenía que aportarse algo nuevo, fresco, no podía caerse en lo fácil, otra precuela. 
El guión, la fotografía y colometría de la película se puede calificar de brillantes. No pierde un ápice de acción, batallas estelares, duelos de espadas láser, en cuanto a los toques de humor, son santo y seña de la franquicia.
No puedo darle un 10, porque considero que la perfección es imposible, pero un 9 si se merece. Para mí puede que estemos ante la mejor película de la saga, junto a El Imperio Contraataca.
        
            