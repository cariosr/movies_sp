
                      Comienzo a escribir escuchando una selección musical  magnífica del Youtube ligada a la película de forma inseparable. No sé qué porcentaje de su valor se le puede asignar pero es del todo seguro que es muy elevado.

Sigo escribiendo con el gozo de haber vivido una experiencia cinematográfica que seguramente estará entre las mejores del año. Tarantino sigue siendo él mismo a pesar de que abandona, acertadamente en mi opinión, esa rotundidad rabiosa y trascendente (el regusto amargo de "Los odiosos ocho" todavía me devuelve) para explorar con una contundencia propia de él una época hollywoodiense en decadencia que debía dar paso a otra igual o más brillante con un divertimento risueño, de homenaje irónico y con narración discontinua que mejora una linealidad sosa.


El argumento gira en torno a tres personajes y no dos. Un actor en decadencia y su doble que ha acabado siendo su mayordomo mantienen una amistad férrea y tienen por vecina a Sharon Tate en una zona acomodada de Los Ángeles de 1969. Quisiera destacar la mayúscula interpretación de la Margot Robbie (la recuerdo especialmente por "Tonya") que puede quedar injustamente eclipsada por la pareja de actores consagrados y de renombre que acaparan el protagonismo sin subestimar nada sus méritos.

La primera parte contemplativa parece que sea una sucesión inconexa de situaciones con un aluvión de referencias y canciones en un divertimento sublime sin que pase nada significativo a nivel argumental. Un paseo magnífico para la época con los coches arriba y abajo y con algunas escenas de una magia memorable entre las que destacaría de la Sharon Tate en el cine o la parte dedicada a  Bruce Lee. En este punto quisiera mencionar el exceso de cortes y retrospectivas que se encuentran en múltiples ocasiones y que pueden crear confusión. También se podría calificar de vigoroso dinamismo que obliga a una atención activa. Esta sería una de mis incertidumbres que no le daría más importancia.

Quentin Tarantino nos regala su lado más personal en un homenaje sincero y un viaje a su imaginario más íntimo y evocador donde combina la ironía y el drama con una propuesta ingeniosa y de un respeto memorable para un mundo que es su vida. Tarantino se fusiona en un acto de estima incuestionable, emotivo y sugerente fruto de una tarea meticulosa y gigantesca. Mientras la selección musical continúa sonando permítanme unos últimos consejos: no se la pierdan, vayan preparados por su duración y no se vayan con los títulos de créditos. (9/10)
Más comentarios en: bit.ly/2Z940AR
        
            