
                      Oren Peli vuelve a ponerse tras las cámaras después de la gloriosa “Paranormal activity“. Junto al productor Jason Blum (“The purge“, “Obsesion“, “Sinister“), nos traen otro found footage esta vez centrado en la misteriosa área del desierto de Nevada: “Área 51“.

Tres jóvenes deciden investigar, cámara en mano, los secretos que esconde el área 51, zona reservada del gobierno de los Estados Unidos famosa por los rumores acerca de sus conspiraciones extraterrestres.

Aunque la película fuera concebida tras el éxito de “Paranormal activity”, no ha sido hasta cinco años después cuando ha visto la luz “Área 51″. De hecho, la película podría haber tenido más repercusión si se hubiera estrenado antes y no ahora, con la cantidad de películas similares con las que la podríamos comparar. Aun así, la cinta goza de buena energía, no cayendo en los aburridisimos primeros treinta minutos que normalmente nos ofrece este tipo de cine. “Área 51″ cuenta desde el primer momento con escenas, ya no aterradoras, pero sí que generan una tensión en el espectador que es de agradecer. Pero la verdadera fiesta comienza a partir de la mitad de la película, cuando sus protagonistas ya son conscientes de donde se han metido y cuando realmente comienza el terror a lo desconocido. Una película que a esas alturas del género, ya no sorprende, pero que supone otro punto de vista al cine de extraterrestres hostiles, más allá de las también maravillosas “Extraterrestrial” e “Incident in Lake County“.

Lo mejor: el final.
Lo peor: que el cine de OVNIS no comercial siga tan infravalorado.
        
            