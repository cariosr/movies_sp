
                      La premisa es simple: siete astronautas que trabajan en una nave de carga maltratada, traen algo de vuelta a la nave después de aterrizar en un planeta extraño, y el alienígena comienza a aprovecharse de ellos.

Presenta un thriller de ciencia ficción con una tensión palpable y nerviosa, con Sigourney Weaver, que interpreta a la suboficial Ellen Ripley, en una forma sorprendente.

–– ADVERTISEMENT ––

La directora Ridley Scott levantó a Weaver en el instante en que ella se dirigió a su oficina para hacer una audición. "Tuve mucha suerte al obtener el papel de Ripley", dijo más tarde, "porque hizo que mi carrera tomara una ruta mucho menos convencional de lo que había pensado que quería. Había soñado con ser principalmente actor de teatro, que era el tipo de mi carrera, mi madre. Shakespeare y todo eso. Creo que lo que me ayudó fue que no me caí al revés para obtener el papel. Pensé: 'Sí, esta gran manada me perseguirá por la habitación. Jell-O '. Y no había nada sorprendente ni original en el guión. Son básicamente diez indios pequeños. Luego conocí a Ridley y él era un hombre loco de una manera maravillosa. Cuando vi el diseño del set, me di cuenta de que esto iba a ser fabuloso ".

Por pura teatralidad, la misma cosa que Weaver pensó que estaba abandonando, Alien es difícil de superar.
        
            