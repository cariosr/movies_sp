
                      Contando con que indiscutiblemente este filme dejará satisfecho a los fans comiqueros e internautas cinéfilos, también tiene el suficiente potencial para gustar al resto de peña cinéfila a base de desternillante mitología de superhéroes, mucha acción y sangre a borbotones, que no a raudales. 
Aquí a la que menos te lo esperas recibes un palizón, te pinchan, te dejan cojo, manco, o te vuelan la cabeza antes de que puedas decir pío. El director Matthew Vaughn no se ha cebado en exceso en la escenificación de los momentos 'salvajes'. Incluso ha sido capaz de meterle humor a la cosa y no desentonar, por lo que Kick-Ass se convierte en uno de los mejores filmes llegados del otro lado del charco en lo que llevamos de año.
        
            