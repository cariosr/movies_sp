
                      Summer Wars es una película de 2009 de la productora que trajo "la chica que saltaba a través del tiempo".

Nos sitúa en el presente, con una red social que engloba todas las redes sociales que nosotros podemos tener en la realidad, esta red es el mundo de O, donde cada persona tiene su mundo virtual. Este mundo es atacado por un virus que roba cuentas y puede usar sus permisos específicos para hacer cambios catastróficos en la vida real.

Nuestro protagonista tiene la gran suerte de ser invitado por la chica más guapa del instituto a pasar el verano con su familia haciéndose pasar por su novio; sin embargo, tiene la mala suerte de que su cuenta es la que el virus utilizará para hackear el mundo de Oz.

El desarrollo no podría estar tan bien enlazado y presentado. Una película que hay que ver una vez en la vida, más ahora que las redes sociales han comido tanto de nuestro mundo real.
        
            