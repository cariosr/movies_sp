
                      Muy buena película, excelentes actores. Aunque la trama ya esté un poco gastada, el director lo supo manejar muy bien. Jennifer Lawrence como siempre acierta en su actuación. Como ya se venía previsto, los efectos especiales son sorprendentes. Recomiendo la película, por las actuaciones de Lawrence y Pratt, como también por la puesta en escena.
        
            