
                      Continuando a la derecha desde donde lo dejamos, la Fraternidad que se ha reunido para proteger a los Hobbit Frodo Baggins (Elijah Wood), en su búsqueda para destruir el Anillo Único, se ha dispersado. Frodo y su hobbit jardinero Sam (Sean Astin), se pierden en las montañas hasta que la criatura babosa Gollum, que los había perseguido antes de ser vencida, se ve obligada a llevarlos a la puerta negra de Mordor.

Mientras tanto, el malvado Lord Saruman (Christopher Lee) está creando un ejército de Orcos para destruir la Tierra Media y los otros dos hobbits, Merry y Pippin, escapan de los minions de Saruman y se encuentran en un bosque de árboles parlantes. Y el trío heroico de Aragorn (Viggo Mortensen), el elfo Legolas (Orlando Bloom) y el enano Gimli (John Rhys-Davies), en su esfuerzo por encontrar a Frodo, se encuentran defendiendo al Rey de Rohan y su encantadora hija interpretada por Miranda. Otto de la embestida de las hordas de Saruman.

Mientras que algunas personas piensan que esto supera al original, yo no. Las historias están fracturadas y, a nivel geográfico y narrativo, a veces es difícil entender qué está pasando. Y hubo una emoción con la primera que de alguna manera falta con esto. El puntaje de Howard Shore supera algunas escenas innecesariamente. Son todos los aspectos negativos, pero los positivos son muchos. El alcance romántico de esta historia de referencia celta permanece, el mensaje es claro, se unen para luchar por el bien en este mundo, elfos, hobbits, hombres y bestias, incluso árboles en esta ocasión.

Las actuaciones están bien, con un sobresaliente de Andy Serkis como Gollum, que tiene dificultades para averiguar si es un buen tipo o un malvado.
        
            