
                      Hola. Soy una lectora apasionada de esta serie.

He visto la película 2 veces en el cine el día del estreno.

En cuanto a la película, en general no está mal, aunque me gustó mucho más la primera parte porque creo que transmitía mejor los sentimientos de los protagonistas y la historia tenía una mayor continuidad. En esta segunda parte, el director pasa de una escena a otra sin un hilo argumental sólido (a mi parecer).

No me ha gustado para nada el final de la película. Acaba de una forma muy brusca, parece que acabe a mitad de una frase. Deja mal sabor de boca; incluso se oyeron silbidos de desaprobación en la sala.
        
            