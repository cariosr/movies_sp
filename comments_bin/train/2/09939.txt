
                      Una obra maestra, un comedía excéntrica con unas frases memorables.
Es divertida, intuitiva; merece la pena verla. Me encanto.
        
            