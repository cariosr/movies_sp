
                      Película de género infantil, nunca mejor dicho, porque es de lo más infantil que te puedes echar a la cara. Claro que, si te gustan los animales y el cine blanco, es una película ideal para ir con los niños y desengrasarse de los problemas que nos acontecen.
El paso del perro Marmaduke de tira cómica de periódico a la gran pantalla ha sido un gran acierto, su estilismo es de lo más perruno, algo que se agradece hoy en día que nos sacan perros que parecen cualquier cosa menos uno perro real. 
Y lo que más me gusta es que rompe con el tópico del refrán llevarse como el perro y el gato, ya que nuestro protagonista tiene como mejor amigo un minino de lo más gatuno. Simpática y entretenida.
        
            