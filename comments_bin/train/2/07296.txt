
                      Me ha encantado. Una película épica, de las que siempre recordaré. Desde un principio te involucras en la historia por el cariño que desprende el protagonista. Visulamente perfecta, la banda sonora acompaña magistralmente a la historia. Es una película hecha con delicadeza y sensibilidad. Me ha emocionado, toda una lección, que al final, da lugar a la reflexión. A aquellos que os atraiga ver algo diferente, os la recomiendo. Para mí es una obra maestra, una de esas rarezas imprescindibles.
        
            