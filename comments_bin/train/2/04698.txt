
                      El universo de Jack - LA HABITACIÓN

Joy (Brie Larson) nos muestra una doble cara reflejada en el espejo de la vida. Tras ser secuestrada y tener que criar a su hijo Jack (Jacob Tremblay) dentro de la habitación, decide contarle que ese pequeño cuarto es el mundo entero. Y para Jack ¿Qué hay fuera de la habitación? EL ESPACIO. La película está claramente dividida en dos partes: al comienzo observamos a una madre apagada y hundida ante la terrible situación que le ha tocado vivir pero que a su vez, sale a flote día tras día para ofrecer a Jack una vida de lo más normal y feliz posible. Para Jack la habitación es todo un mundo, un lugar de juego, de aprendizaje y de diversión; su curiosidad pone a prueba la admirable capacidad de Joy a la hora de dar respuesta a todas sus preguntas.

La habitación nos ofrece la oportunidad de vivir una auténtica situación dramática de la mano de este pequeño que con su delicada inocencia permite que veamos la vida a través de su mirada infinita.

Más información en el blog de anguloneutro.
        
            