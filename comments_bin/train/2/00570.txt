
                      Yo pensé que seria una típica guerra entre humanos y seres de otro planeta... ¡Pero resulto ser lo contrario! 
Es una película que supero mis expectativas, debido a que es original, inteligente, no hay guerra, abarca problemas sociales como racismo, opresión, mercados y política. 
El decorado, montaje y efectos visuales son acertados en este filme. Todo quedo bien, aunque tengan escenas de mas, es una maravillosa película de ciencia ficción. 
Para resumir, es una película con buena trama, es astuta, trata problemas sociales y deja mucho que pensar. 
3.8/5
        
            