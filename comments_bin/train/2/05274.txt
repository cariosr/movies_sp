
                      Passengers (2016).
"Deje de preocuparse de algo que se escapa a su control. Saboréelo!"

La actriz mejor pagada de Hollywood (Jennifer Lawrence cobra 8.000.000$ más que su compañero) protagoniza este drama romántico ambientado en el espacio en compañía del actor de moda Chris Pratt. 
Nos encontramos con una cinta bien facturada, con un excelente diseño de producción solo superado por el atractivo de la pareja protagonista, y que nos presenta un interesante dilema: ¿serías capaz de cambiar el futuro de tu acompañante por no pasar solo el resto de tu vida?

La historia engancha a pesar de no disponer de un extenso reparto y de tener casi dos horas de metraje. Y como en cierto momento dice nuestra protagonista; "Si vives una vida corriente, solo podrás contar historias corrientes. Vive una vida maravillosa y contarás historias maravillosas", y de eso trata Passengers.

Mensaje del día: "Que no te obsesione tanto donde quieras estar que no te permita disfrutar donde realmente estás." Frase final del Alter Ego de J. Lawrence en este largometraje. (Y es que el cine enseña mucho en ocasiones...). ;

Raúl Cabral.
        
            