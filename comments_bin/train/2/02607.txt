
                      Anton Ego, el crítico de restaurantes de The Grim Eater, es una de las mejores actuaciones de la carrera de Peter O'Toole. En realidad, no vemos a los Oscar de 108 años con la esperanza en carne y hueso. Pero nos deleitamos en super voz fulminante. Y qué Herramienta se Cuando se combina con la fluencia que posée en la maravillosa caricatura Ratatouille de Brad Bird. Ego es el crítico alimentario más temido de París.
        
            