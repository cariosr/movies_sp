
                      No pueden dejar de verla. Encierra una trama que muchos desconocen y que pocos entienden pero que es cautivadora. El protagonista tiene un tipo de Autismo que lo hace ser diferente a los demás pero  esto lo hace triunfar en todo lo que hace por encima de otros (supuestos normales) y la trama se va haciendo cada vez mas emocionante y hermosa..  Aunque nunca se nombra en la película que es lo que padece el protagonista deja un mensaje de esperanza y  que a pesar de su condición, pudo hacer todo lo que se propuso sin importar  lo que dijeran los demás.
        
            