
                      Una película que te atrapa y consigue que esa llegada no te parezca irreal. De hecho, te olvidas de ellos, y te centras en ella, la protagonista.
Original, muy bien actuada, gran banda sonora, y fotografía intachable. Un 10
        
            