
                      Yo he leído el libro y como suele decirse: el libro siempre es mejor. Pero a mi la película me ha gustado, está bien hecha y bien interpretada, además tiene una ventaja, para el que no ha leído el libro se encuentra con un guión desconocido con una historia apasionante de amor y relaciones entre los seres humanos, con independencia de su raza, procedencia, religión o creencias. En fin 2 horas 45 minutos de buen cine que no se te hacen largas.
        
            