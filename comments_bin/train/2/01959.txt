
                      EXTRAORDINARIA FÁBULA SOBRE LA RESPONSABILIDAD DE SER PADRES

Se puede afirmar que el simple hecho de ser padres muchas veces nos atemoriza, por la responsabilidad que implica serlo. La palabra responsabilidad significa la habilidad de responder ante una realidad, y ésta la consideramos demasiado grande y difícil, ya que siempre hemos escuchado que "nadie nos enseñó a ser padres". Esto es una falsa creencia limitante para justificar conductas nuestras, o de nuestros hijos, que no queremos afrontar. ¡Nuestros mejores maestros hemos sido nosotros mismos! Y no hemos caído en cuenta. A partir de haber vivido una infancia feliz o infeliz, yo sé qué es bueno para un niño, qué siente un niño, la perspectiva de un niño, qué lo puede dañar y cómo me gustaba a mí o me hubiera gustado que me trataran; así que es a partir de mi vivencia de mi infancia, por la cual yo aprendí a ser un buen padre o una buena madre.

Apoyándose en estos conceptos, José F. Ortuño y Laura Alvea han cimentado la película que nos concierne: "The Extraordinary Tale"; un cuento sobre una pareja un tanto especial y particular (debido a que sufren un grave trastorno mental derivado de una infancia difícil), y que tienen que afrontar la arduosa tarea de ser padres. 

Para llevar a cabo tal historia, sus directores le han dado un toque especial, bastante original, con grandes dosis de humor negro, una estética fantástica deudora del cine realizado por Jean-Pierre Jeunet (responsable de la maravillosa "Amelie" [2001]), que la convierten en una de las más fascinantes, e ilusionantes, operas primas creadas en España en mucho, mucho tiempo.

Destaca el impresionante trabajo de su pareja protagonista, formada por el estadounidense Ken Appledorn y la actriz canaria de origen alemán Aïda Ballmann; que con sus múltiples gesticulaciones, y matices, llena la pantalla, apoderándose de ella, y que por momentos me ha recordado al cine mudo de los grandes Charles Chaplin o Buster Keaton, salvando las distancias evidentemente. La banda sonora compuesta por Héctor Pérez también me ha parecido estupenda, encajando como anillo al dedo dentro del contexto del filme.

En conclusión, "The Extraordinary Tale" se erige como una notable y atípica pequeña joya, que se desmarca del resto por su imaginativa estética, su propuesta fresca y entrañable, su premisa con calado, y el rebosante talento de sus directores y plantel de intérpretes; sin lugar a dudas, una verdadera maravilla.
        
            