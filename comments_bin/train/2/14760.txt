
                      Maravillosa en cada plano y fotograma, en cada imagen y nota musical. Chazelle construye una historia bellísima, un espectáculo visual desbordante y una magia impropia de los musicales de este siglo. La chispa entre Ryan Gosling y Emma Stone sigue tan viva como siempre, y le brindan a Hollywood una muestra de arte y espontaneidad. Oscars, allá vamos.
        
            