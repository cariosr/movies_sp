
                      Cínica, irreverente, subversiva, amoral... una película distunta que no deja indiferente. Todas las caras menos compasivas y más inclementes del ser humano son mostradas a traves de sus personajes. Perfectamente reconocibles aunque no nos guste. Imprescindible para saber algo más de nosotros y nuestros semejantes.
        
            