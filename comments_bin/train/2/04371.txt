
                      Es una de esas películas que no te deja indiferente, que te afecta y hace aflorar los sentimientos. Además sabiendo que está basada en una historia real todavía parece que te afecte más, aunque pueda estar algo exagerada. Muy recomendable verla y creo que es mejor ir sin prejuicios previos.
        
            