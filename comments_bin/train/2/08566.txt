
                      “Y vio Dios que la luz era buena, y separó a la luz de las tinieblas”. Génesis 1:
Con ésta frase del Antiguo Testamento sobreimpresa en un negro tan profundo como amenazante, comienza una de las películas más duras y perturbadoras que nos ha dejado el año 2015.   Aunque uno tiene la primera impresión de que esa casa, es una metáfora representativa del purgatorio, lo que el cineasta chileno Pablo Larraín nos propone es visitar las neblinosas y azufradas estancias del infierno. Puesto que en la actitud de los curas no existe el mínimo atisbo de arrepentimiento, de expiación, ni siquiera reconocimiento de culpa, la redención y la purificación final de los pecadores se muestra como un propósito tan estéril como inalcanzable.
Reseña completa en:
        
            