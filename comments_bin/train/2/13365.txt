
                      Me ha encantado. Creo que está con todo merecimiento entre las mejores de Pixar (y eso ya es mucho). Me parece una película con la que disfrutarán los niños y los que ya no somos tan niños. Si alguien quiere, he escrito una reseña más completa y detallada en mi blog. Pero, en definitiva, "Del revés" es una película muy, muy recomendable.
        
            