
                      Excelente película. La intriga la mantiene desde el principio. La trama tiene tantos giros inesperados que llegados a un punto no se sabe cuál es la verdad y te confunde tanto que las ansías por saber la verdad aumentan muchísimo. Las actuaciones estuvieron muy buenas y acorde con la trama de la película.
        
            