
                      Una pareja actoral brillante. Una narración técnica preciosa. Un final inesperado. Son solo algunos de los detalles que coronan a 'El sexto sentido' como un referente en el género del suspense, del que cada minuto puede disfrutarse, del que Shyamalan disfruta contando y el espectador disfruta viendo. Es una demostración de que, sin necesidad de fuegos artificiales, una película puede ser emocionante.
        
            