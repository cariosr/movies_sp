
                      Como seguidor de Woody Allen, que no falta a sus estrenos, acudí a este el primer día, llevado un poco por la buena crítica de los medios. Y evidentemente que la película me cautivó, sobre todo la protagonista y su genial interpretación, pero de ahí a decir que es lo mejor de Allen va un abismo, el genio neoyorquino tiene mucho y muy bueno en su haber y me atrevería a decir que algunas de sus obras superan a esta.
        
            