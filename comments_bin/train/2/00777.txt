
                      Esta película es la continuación de "La noche de los muertos vivientes" y el director George A. Romero nos trae otra gran obra, eso si esta en color, posiblemente una de las mejores de la década de los 70 en este género. Las interpretaciones vuelven a estar a la altura de la cinta, es decir, muy bien. El director vuelve hacer una crítica bastante inteligente a la sociedad, en especial al consumismo y capitalismo, muy bien retratada en el centro comercial.
        
            