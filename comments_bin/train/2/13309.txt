
                      Una joya rarísima de encontrar, solo apta para quienes quieren mas que simples sustos de película palomera. La única película que habla de los verdaderos monstruos, tienes que analizarla a cada momento para entender. Al final, es una poesía macabra maravillosa.
        
            