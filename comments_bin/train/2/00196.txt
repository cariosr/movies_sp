
                      De nuevo Toni Servillo, nos sorprende  con una interpretación magistral, desdoblándose para reinterpretarse. Con ello, da una esplendida visión de la inexistente cordura de los políticos.
        
            