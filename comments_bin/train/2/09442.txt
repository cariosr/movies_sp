
                      Hay que ver a los extremos que tenemos que llegar para poder ver una película. Como las productoras españolas (o quien corresponda) no me han dejado otra opción he tenido que ver Rubí descargada pirata en alemán y con subtítulos en español (porque sino no me hubiera enterado de nada porque yo de alemán nada de nada por mucho que me haya leído el libro).  

Me hubiera gustado verla en la gran pantalla con el paquete de palomitas y toda la parafernalia típica pero en Alemania la estrenaron en marzo y vamos por estas alturas de año y aquí siguen sin decirnos ni mu al respecto aunque parecía que una productora la había comprado para doblarla. Luego se quejaran de la piratería…

En fin, que me enrollo como una persiana. A la película.

Esta trilogía de libros ha sido y sigue siendo una de mis sagas favoritas de literatura juvenil desde la primera página que pase leyendo. Así que cuando me entere de que daba el salto al cine me puse contenta y temerosa a partes iguales porque ya se sabe lo que suelen hacer los guionistas cuando se ponen a adaptar un libro (ejem).

El producto final ha sido una película más que aceptable aunque hay cambios sustanciosos. Los actores que interpretan a los protagonistas están bien aunque el Gideon que me imaginaba era mucho más guapo que el chico que han escogido para que interprete el papel.

Hablando de Gideon, me da la impresión (no sé si será por los subtítulos) que no es tan acido, ni tan mordaz como en el libro. Yo lo recordaba más irónico y mucho, mucho más veleta en cuanto a su trato con Gwen. Así que le doy un aprobado raspadillo.

Gwen me ha gustado. Creo que refleja bastante bien el carácter del libro y la chica que lo interpreta lo hace aceptablemente bien.

A la que también han clavado en el papel los responsables del casting es a la prima, Charlotte. Me la imaginaba como la vecina repelente de Los protegidos pero tengo que reconocer que esta actriz es una buena Charlotte.

No sé si la información que ronda por internet acerca de la segunda película es cierta o no. Ojalá que sí porque Zafiro es el que más me gusto de los tres libros y tengo muchas ganas de verlo en película. Y espero que incluyan a cierto personaje que fue toda una revelación.

Para acabar, una petición. Por favor, que por lo menos salga la película a la venta en DVD para que lo podamos comprar los fans de la saga. (ojitos del Gato de Sherk)
        
            