
                      La Habitación nos habla de una madre y un hijo, de su relación, de sus penurias y sus alegrías, de sus sensaciones y pasiones.... Pero todo en el entorno de una habitación, en la oscuridad y claustrofobia de cuatro paredes con sólo una pequeña claraboya en el techo. Esta habitación es TODO el mundo que conoce el niño. Esta habitación es la cárcel de esa madre, encerrada, secuestrada allí durante esos últimos años, precisamente el tiempo de vida de ese niño, su hijo.

Es una delicia de película, es cómo hacer de un guión una experiencia cinematográfica de gran calado. Un elenco de actores madre mía... Qué actores esa Brie Larson haciendo de madre coraje extraordinaria, ese niño que nos transporta a un universo que quisiéramos no haber vivido nunca.

Lenny Abrahamson nos demuestra a los admiradores del séptimo arte que no es necesario grandes medios, grandes presupuestos, grandes espacios exteriores para elaborar un producto que te angustia, te agobia, te desespera, te lleva a límites a los que realmente no querías llegar, te martiriza.

Es angustiosa, pero lo es tanto entre las cuatro paredes como fuera de las mismas. Es agobiante en ambos ambientes, desesperante en muchas escenas que te gustaría parar y no vivir. 
En definitiva, la mejor película del año sin duda. Obra Maestra.
        
            