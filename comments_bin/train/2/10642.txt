
                      Perfecta! Por fin profundizan en los principios de salud mental verdaderos donde la tristeza es tan necesaria como la alegría. El problema de la trama se crea por lo mismo que nos lo creamos todos nosotros: pensar que hay que estar bien, ser feliz y estar contento evitando todo atisbo de tristeza desde nudos en la garganta ahogados hasta antidepresivos diarios....en fin, genial! Para todos los públicos! También los niños pueden aprender las cosas bien desde el principio aunque nosotros no lo hayamos hecho y tengamos que desaprender.
        
            