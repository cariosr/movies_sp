
                      Gravity es sin duda una de las grandes películas de ciencia ficción, intercala de manera magistral la acción (de una manera peculiar, complicada y sublime) y el drama y la angustia que vivimos junto a Bullock. Tanto el sonido como los efectos especiales, así como la actuación de la actriz son magnífico. Gran película. PD: No le gusta a todo el mundo, paciencia y disfrutarla!
        
            