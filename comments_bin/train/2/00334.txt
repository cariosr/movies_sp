
                      Espectaculares interpretaciones y un guión formidable. Muestra el sinsentido de la guerra y más aún cuando es religiosa. Es dura de ver en algunos momentos, pero merece la pena soportarlos para entender la propuesta.
        
            