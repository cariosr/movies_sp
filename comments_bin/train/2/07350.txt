
                      Excelente obra cinematográfica.  Pero a pesar de que sea muy buena no se la recomiendo a todos. Ya que tiene un trama muy complicada, para ser honesto la primera  vez que lo vi  me perdí algunos detalles y no la comprendí al máximo. Creo que
 solo lo deberían ver solo las personas que son verdaderos amantes del cine, porque a muchos esta película le parecerá confusa y aburrida.
Pero dejando eso esta película capta perfectamente la desesperación de una actor por la necesidad de ser relevante otra  vez, y lo mejor de esta película es que su humor negro te hace sentir incómodo en todo momento. 
Creo que esta ha sido la mejor película del 2014
        
            