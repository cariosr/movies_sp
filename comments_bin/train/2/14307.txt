
                      Una joya bélica que muestra la perspectiva más gráfica de la guerra. Con una animación y banda sonora de lujo, y una narración magnífica, solo queda decir que es 100% recomendable.
        
            