
                      Billy Elliot es un homenaje a los luchadores, a los valientes, a los soñadores, a la comprensión de que es el
corazón quien decide, aunque nuestro mundo nos intente quitar la razón.
Es el reencuentro de un niño con sus seres queridos, pero ante todo es el reencuentro consigo mismo.
        
            