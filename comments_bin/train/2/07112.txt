
                      ¡Larga vida al rey! El hechizo de la Parte 3 que plagaba la Matriz, el Padrino y la Guerra de las Galaxias original no puede contaminar al Señor de los Anillos: El Retorno del Rey. El director, productor y coautor de Nueva Zelanda, Peter Jackson, hace el autor J.R.R. Tolkien se enorgullece al convertir su tomo en una película épica por la cual se juzgarán todas las futuras películas épicas. King abre tus ojos, excita tus sentidos y te acerca tanto como un susurro para escenas de emoción sorprendente.

Muchos críticos que se resistieron a las dos películas anteriores (The Fellowship of the Ring y The Two Towers) se han subido a bordo para llamar a King, como si la serie solo hubiera aparecido. Bull. Las tres películas son iguales e indispensables para la historia que se cuenta.
        
            