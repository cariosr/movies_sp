
                      La película superó todas mis expectativas. El conjunto de actrices está en general de notable alto, inclusive la cursi Emma Stone. Más de dos horas de película y no se hizo larga, a pesar de que el conflicto de la película es siempre el mismo y los mantiene hasta el final. Jessica Chastain y Viola Davis están magníficas en sus respectivos papeles. Sólo me queda recomendarla. Una de las películas del año.
        
            