
                      Los últimos pasos de Michael Jackson, sus últimas palabras, su últimos esfuerzos en vida, su útilma demostración de su pasión de la música. Esta película no es solamente un documental de la gira, ni siquiera se expone el estado de salud del cantante, ni tampoco se ve siempre su simpatía y alegría que le acompañó en gran parte de su vida. Esto, realmente, es THIS IS IT: 

Una película que te envuelve en un concierto real desde dentro, no como una experiencia inmersiva, sino de descubrimiento y sensaciones a cualquier otro espectáculo de música. El espectador podrá ver los entresijos de una enorme gira de 50 conciertos que serían únicos en la historia, además de comprender la perfección y comprensión de Michael a través de sus más míticas coreografías. Una última visión real y sin adornos de lo que hubiese sido esta gira, pero como estar en un lugar VIP.

Gracias a esta película, se recuerda aún más el espíritu de Michael Jackson, su deseo de dejar un legado y un punto final a su carrera artística gracias a sus conciertos finales. Sobre todo para los fans, ya que todos pueden verlo sin tener que viajar a Londres o retroceder en el tiempo.
        
            