
                      En mi opinión esta película esta llena de fuerza y intensidad siendo unos de los mejores films narrando las consecuencias de los supervivientes del drama de la guerra (en este caso la de Vietnam), el director Michael Cimino esta soberbio en la dirección al igual que los actores, destacando en especial a una jovencísima Meryl Streep, que a pesar de realizar su segunda película ya mostraba que iba a ser una de las más grandes, y a los actores Robert de Niro y Christopher Walken que realizan unas interpretaciones increíbles.
        
            