
                      El comienzo de Batman.
Inicio de un nuevo universo de Batman, enfocandose en temas como la política, corrupción, valores humanos, etc. Christian Bale sabe personificar a la perfección a Bruce Wayne, caracteriza muy bien el rol de empresario, multimillonario y filantropo de Gotham City. 
La aparición de Liam Neeson como Ra's al Ghul trae esa esencia que solo Neeson puede tener, la acción. 
Cillian Murphy (uno de los mejores actores de la actualidad -y de años atras- para mí) como el principal antagonista de la serie, logra demasiado bien su rol de villano. 
Batman Begins, atrapa desde el primer momento hasta el último. Sin dudas Nolan sabe como hacer películas.
        
            