
                      Impresionante historia inspirada en casos que, terroríficamente, han tenido lugar en la vida real. Impresiona el modo en el que logran captar tu atención sin apenas cambios de escenarios durante la primera parte de la película, pues, curiosamente, cuando están fuera de la habitación la trama resulta más densa y pesada. Aún así, pocas pegas que añadir. Brillante la puesta en escenas de los actores protagonistas, con mención especial para el joven niño que da vida a Jack. Logra poner el vello de punta y conmocionar.
        
            