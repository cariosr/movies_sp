
                      Sospechosos habituales es un thriller que nos trae a la memoria ese gran género que fue el cine negro. La música de los primeros compases de la película ya nos da la pista. Pero no es eso lo que más llama la atención de esta cinta. Un guion magnífico, muy original, con algunos giros inesperados y otros no tanto, aunque no por eso menos sorprendentes, porque aunque puedas sospechar la verdad en algún momento, siempre te deja la duda de si estarás en lo cierto o no. Porque cuando ya crees que lo has descubierto, abre otras puertas para que puedas elaborar otras teorías. 

De hecho, yo pasé por varias a lo largo de la cintal, todas mis teorías se fueron al traste. Así que el final fue una gran sorpresa para mí. No serviría de mucho un buen guion si el resto de los elementos de la producción no funcionaran correctamente. Aquí lo hacen y con muy buena nota. 
Bryan Singer dirige la película magistralmente, marcando los tiempos a la perfección y creando momentos de mucha, mucha tensión. 
El reparto es muy bueno. Kevin Spacey que esta magnífico, encabeza a este magnífico elenco de actores en estado de gracia. 
En definitiva, muy buena película.
        
            