
                      No es una película bélica, si bien el argumento tiene su origen en un hecho de combate en Irak. Es un fresco sobre la sociedad americana y su visión del mundo, amenizada con desfiles coloristas, sus majorettes, sus espectáculos orquestados por cadenas de televisión y sus presentadores/empresarios estrellas.
Una mirada cruda y mordaz donde cada actor parece preso de un rol y un destino inalterable.
Ilustra perfectamente aquello de: the show must go on...
        
            