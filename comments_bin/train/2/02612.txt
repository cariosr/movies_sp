
                      "No le arruines a una novia el día de su boda". Con esa premisa saldrá el espectador que vaya a ver '[REC]3: Génesis', el tercer film de la saga de terror más exitosa de España, dirigida por Paco Plaza. El director repite al mando, ahora sin Jaume Balagueró, que trabaja en la última película de la serie.

Los incondicionales del género quizá se sienten defraudados con esta precuela, al incluirse como novedad la cámara panorámica. Atrás queda el dominio de la cámara en mano que retrató a '[REC]', incumpliendo la primera regla de su historia: todo ha de ser grabado. Ello permitía darle mayor drama y una visión subjetiva a la película. Pero eso no es motivo para no sentarse y ver una película con una historia bien contada, que hace hincapié en los tópicos de cualquier boda, desde el familiar borracho hasta el inspector de la SGAE.

Dividida en dos partes, la primera, cámara en mano, presenta a los novios, Clara (Leticia Dolera) y Koldo (Diego Martín), la ceremonia y el convite. Todo es tranquilidad, felicidad y algo de pastelada hasta que se desata la tragedia. Cuando empieza la acción y todo se transforma en horror por la infección zombie, el cambio a cámara panorámica inicia la segunda parte.

Su banda sonora puede parecer de lo más surrealista, pues entona un recuerdo de los años 70 y 80 con temas de Loquillo, Pablo Abraira ("Gavilán o paloma") y el baile nupcial al tono de "Eloise" de Tino Casal, que volverá a repetirse. Pero lejos de hacer un viaje al pasado, Paco Plaza también se ayuda en el "sí, quiero" con Coque Malla y su "No puedo vivir sin ti", así como de Iván Ferreiro, quien compuso para la película "Canción de amor y muerte". Al contrario que sus predecesoras, en esta ocasión Carlos Ann no colabora.

La buena química que tienen los actores protagonistas hace que los momentos que están juntos luzcan como uno, y cuando se ven separados por los acontecimientos hagan todo lo posible para reencontrarse el uno con el otro. Mención aparte merece Leticia Dolera, que está llamada a ser un nuevo icono de heroína, tras apostar por un personaje que, vestido de novia ensangrentada y motosierra en mano, mata a los zombies que le han fastidiado su día a ritmo de Eloise de Tino Casal.

Con un tono más gamberro y delirante que las anteriores, '[REC]3' es la lucha de dos enamorados por volver a estar juntos, pase lo que pase y hasta que la muerte los separe.
        
            