
                      La inmortal madre de las películas de terror es El Exorcista. No por que sea la primera ni la última, si no por lo que significo y significa en la historia del género y del cine en general. Evidentemente ha hecho algo que ninguna otra cinta ha podido lograr antes ni después, y que hacen que luego de 40 años resulte cada vez mas estupenda y terrorífica. 
Las palabras trascendental y obligatoria le quedan chicas a semejante titulo que es considerado por muchos como la mejor película de terror de la historia.

El director de la película puso arneses a las actrices Linda Blair y Ellen Burstyn para poder darles fuertes tirones cuando fuera necesario en alguna escena. De hecho, Ellen Burstyn sufrió una lesión espinal permanente durante la grabación de la película. También, Linda Blair se dañó la espalda cuando un trozo de aparejo se rompió mientras era lanzada contra la cama.

Esta cinta es el mejor film de terror que he visto jamás. Consigue lograr un ambiente de nervios y tensión que no te abandonará hasta que termina. Los personajes están muy bien definidos y, a pesar del paso de los años, me siguen pareciendo muy buenas las interpretaciones. Los efectos especiales fueron espectaculares en su época, ahora la tecnología ha hecho que ya no lo parezcan tanto, pero esa voz que sale de esa pobre chica me sigue poniendo la piel de gallina, esa transformación que va sufriendo el aspecto físico de Regan me produce escalofríos y todo ello acompañado por esa magnífica "Tubular bells" de Mike Olfield. Para rematar, un final perfecto que cierra la historia sin dejar cabos sueltos.
En definitiva, Obra maestra.
        
            