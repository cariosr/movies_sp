
                      Película divertida, fresca e inteligente
Esta comedia romántica de Emilio Martínez-Lázaro es sin duda divertida, fresca e inteligente.  Con un guion ágil y unos personajes cercanos, entrañables, que tienen salidas ocurrentes y creativas para solventar las dificultades que se les aparecen en la vida, son personas con las que nos podemos identificar fácilmente. La ambientación y la fotografía son muy atractivas mostrándonos un Madrid de lo más estimulante. Una película para ir a pasar una más que agradable hora y media. Muy recomendable.
        
            