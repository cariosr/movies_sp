
                      Casi una obra maestra, pero muy buena, con muy buenos efectos especiales, buenas interpretaciones del villano, y buen trabajo por parte de J. J. Abrams.
Lo malo es que cuando empieza a haber algo sorprendente, se corta por alguna manera y luego no se entiende, pero en el fondo se lo han currado muy bien.
        
            