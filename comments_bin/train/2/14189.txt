
                      Me a encantado la peli, llena de acción con una buena historia y recordando la pelis anteriores. Y por ultimo desvelando un gran secreto que no me esperaba.

La historia comienza con Alice contando como empezó todo y de como una cura para todas las enfermedades del mundo se convirtió en la peor plaga de la historia. Luego aparece Alice vagando por unas ruinas allí se topa con dos bichos y tenemos la pelea de la peli, luego se da cuenta de que una base se encuentra cerca de donde esta, al entrar se topa con la reina roja que le dice que tiene que volver a Racon city ya que allí esconden la única cura contra el virus. Así que todo acaba donde empezó.

Durante el viaje a Racon city se topara con unos cuantos soldados de Umbrela y con nuevos aliados que le ayudaran a terminar su misión. Veremos muchísima acción y como uno por uno van muriendo hasta quedar solo Alice, nos toparemos con una antigua compañera Ali Larter y veremos el verdadero propósito de Umbrela con el virus.

