
                      Es un capítulo a parte pero relacionado de la trama original. Muchas caras conocidas, fotografía espectacular, increíbles lugares, y acción trepidante en muchas ocasiones. El cruce de historias te hará recordar a las antiguas. Recomendada al 100% de un fan incondicional.
        
            