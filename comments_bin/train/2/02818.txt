
                      El Árbol de la vida: una película de categoría superior. Por desgracia, solo puedo aconsejarla (mismo si el tema es diferente) a los que han sido sensibles a la poesía lenta de "2001" y a sus imágenes de comienzos del mundo. Se logra aquí una calidad tanto la fotografía como en la  interpretación, de una inusual sutilidad  (aunque en este ámbito tenemos alegrías a menudo ), y en el registro alegórico algo invocatorio de un cine que ha decidido mostrar lo intemporal a través de la imágen. Los que no se dejarán seducir dirán: "grandilocuente y lento!"
        
            