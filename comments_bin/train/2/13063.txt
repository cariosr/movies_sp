
                      Una gran película. Hace que reflexiones acerca del modo de seguir después de un hecho traumático en la vida de los protagonistas. Me gusta el cine que me mueve por dentro y me hace pensar.
        
            