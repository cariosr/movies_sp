
                      Muy buena película.
Soy un gran amante de los comics y en concreto Civil War  (el comic) me gustó mucho. La película, en mi opinión, no lo deshonra.

Tampoco es que hubiera cientos de personajes como en el cómic... pero los que tiene los sabe aprovechar y lo hace muy bien. Además nos presentan dos nuevos personajes: Spiderman y Black Panther, junto con la nueva faceta de Antman (Giantman o Goliat).

Un buen casting, unos muy buenos efectos especiales y un argumento decente aunque algo mejorable en el ámbito del antagonista.

En resumen, una bastante buena película del generé superheroico sin olvidar que la dirigen los hermanos Russo... eso significa que no es la típica película de tios con superpoderes en mallas.
        
            