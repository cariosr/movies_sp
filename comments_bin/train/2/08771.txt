
                      Una preciosa historia, con increibles personajes
Desde el primer minuto de la película, te das cuenta de que entrar en esa sala a ver esta película ha sido uno de los mayores aciertos en la vida. Un historia preciosa de principio a fin, nunca empalaga, maravillosa, bonita, sensible,.. una historia que ilusiona! 

Todo el rato te lleva a través de la alucinante historia, una historia fantástica enarbolada por el carisma y la espectacular personalidad de cada uno de los personajes. 

Tim Burton demuestra así una vez más,su grandeza como cineasta,problamente el mejor que se haya visto con el permiso de Chaplin y Clint.Realmente no puedo decir que esta pelicula sea la mejor de Tim Burton, pienso que ha tenido mejores pero se mantiene en su altísisisisimo nivel proponiendonos una pelicula que recordaremos por su mensaje que a aspirantes a cineastas,guionistas o simplemente cuentacuentos les cuestionará ¿Realidad o Fantasía? Nosé vosotros pero la realidad suele apestar,con esta cuestión te invita a pensar y reflexionar llegando directamente al corazón con esta ¿por qué no? Obra Maestra del Cine,por lo que...Tim Burton LO HAS VUELTO HACER.Enhorabuena Big Movie

Lo Malo: El elenco de actores ha sido increible,Ewan McGregor,Jessica Lange,Steve Buscemi,Helena Boham Carter por supuesto,increible una vez más,Danny DeVito y Deep Roy pero PERO....
Johnny Depp ¿Donde estabas? Para convertir esta obra maestra en la mejor.

Aun así,impresionante filme que recomiendo a todos. Nota:9 

Lo Malo:
        
            