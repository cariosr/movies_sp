
                      Una película brillante y muy recomendable. Para aquellos algo más sensibles, os recomiendo tener un pañuelo al lado, pero no os alarméis, no es un drama para los días más tristes. Es más bien, por la ternura que te infunde, el amor que se expresa y la frustración ante situaciones injustas.

Tierra Cero Blog
        
            