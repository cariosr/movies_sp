
                      A mediados de los noventa el singular y siempre interesante Tim Burton nos encandiló con esta divertida comedia sobre el apodado "peor" director de la historia del cine. Ed es un joven lleno de entusiasmo, de talento delirante, muy poco perfeccionista y que le gusta vestirse de mujer. Sin duda un personaje ideal para Burton.. ¿no creen? Un reparto magnífico -grandísimo Landau-, una dirección exquisita y un cuidado guión para una de las mejores películas de su época. Brillante. Diferente. Genial.
        
            