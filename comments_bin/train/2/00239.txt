
                      Retrato de un fragmento de la historia de EEUU años antes de la Guerra Civil. Cruda e intensa, resulta un film que, motivos extracinematográficos aparte, no se entiende su falta de repercusión a nivel de festivales. Bien rodada y sin escatimar en rudeza, Nate Parker consigue un trabajo que en su conjunto es más que notable
        
            