
                      Sin ningún genero de dudas es "mi película favorita". Ya había visto la edición "normal" y ahora la "especial" en 3D supone un "regalo para la vista". Y no es que varíe mucho pero las "secuencias añadidas" (como las de la "caza" con los Nav'i desde el cielo) son espectaculares. La única "pega" es que se ve que no es una versión "rodada en 3D original"  y la sensación de tres dimensiones es algo "pobre" pero se le puede perdonar porque esta 
película es lo mejor que ha hecho James Cameron.
        
            