
                      Me ha parecido una película imprescindible para cualquier fan de Harry Potter. Si bien es verdad que no tiene que ver con el personaje, durante la película vemos unos cuantos guiños a la saga del niño mago que os van a emocionar sin duda alguna. J.K. Rowling ha logrado darle vida de nuevo a su mágico mundo, y parecer ser que por mucho tiempo, porque la película promete mucho, y estoy segura de que va a dar mucho de sí. 
Fantástica 😻
        
            