
                      Uno de esos thrillers a los que hay que estar atento de cabo a rabo para llenarte con la esencia de la película. Su intriga es latente a lo largo de todo el metraje, pero sigue siendo una de las pocas películas vistas cuyo final me ha dejado con un enorme debate interno.
        
            