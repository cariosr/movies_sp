
                      Esta es una película musical de mis favoritas, la he visto un millón de veces y no me canso de hacerlo. Cada vez que la veo me encanta cantar sus canciones, me encantan los personajes y la historia.
Es una película estupenda y muy recomendable.
        
            