
                      Tener claro la definición de ‘Codigo Fuente’ ayuda mucho para comprender esta película de acción, ciencia ficción y suspense:

El código fuente de un programa informático (o software) es un conjunto de líneas de texto que son las instrucciones que debe seguir la computadora para ejecutar dicho programa. Por tanto, en el código fuente de un programa está descrito por completo su funcionamiento.

Asi pues, gracias a un programa informático que controla un experimento totalmente revolucionario y futurista, vemos los supuestos 8 últimos minutos de vida de un capitán-piloto de helicóptero de guerra, que se ha despertado desconcertado y desorientado dentro de un tren a punto de explotar por una bomba. Pero a través del código fuente, vive una realidad obligada y paralela de tiempo-espacio, donde tendrá siempre justo esos pocos minutos repetidos para conseguir su cometido o misión dentro de la operación que le han asignado, que no es otra que evitar el desastre del atentado masivo y a la vez, descubrir la identidad del terrorista.

En seguida uno se encariña y compenetra con Jake Gyllenhaal, por lo que no es dificil ponerte en su piel y seguirle mientras vive y revive una y otra vez la misma situación (recordando al Dia de la Marmota), pero aprendiendo inteligentemente de cada enlace y descubriendo nuevas piezas del enigma, y consiguiendo así, salir de ese estado de sobresalto, incredulidad y desesperación con el que aparece por primera vez en la pantalla.
Además esta presente la posibilidad de una segunda oportunidad para luchar contra el destino, vivir un amor inesperado y la reconciliación con un padre con el que tiene una relación distante.

El ritmo de la pelicula, te atrapa desde el principio y te mantiene pendiente y expectante durante toda ella, mientras tu cabeza no puede parar de hacer cabalas, conjeturas, suposiciones y apuestas sin parar, sobre cual será el desenlace total.
        
            