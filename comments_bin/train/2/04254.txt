
                      Acabo de ver The Imitation game, una excelente opción navideña. Trata de como decodificaron la clave que utilizaban los alemanes para comunicarse durante la segunda guerra mundial y la de su protagonista Alan Touring, el precursor de la informática. Una historia oculta por mas de 50 años... La película cuenta con maravillosas actuaciones, un guión agil que logra un buen ritmo y una historia interesante que los mantendrá atentos durante los 114 minutos que dura la cinta!
        
            