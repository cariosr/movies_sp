
                      Estupenda peli para ir con los chabales, no se hace nada pesada, tiene un toque de sorpresa y los gráficos son espectaculares. La historia engancha, el muñeco de sencillo y bonachón te atrapa y el resto de personajes, a pesar de estar un poco estereotipados (propio de pelis de dibujos infantiles), no se hacen excesivamente empalagosos. Los pequeños detalles son los que mas han enganchado a mis chicos, consiguiendo conectar con el protagonista y disfrutando hasta el final de la peli. Me ha sorprendido gratamente.
        
            