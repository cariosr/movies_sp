
                      Épica, espectacular... Una gran historia ya conocida, de supervivencia y orgullo humano. Acción y traumas humanos a partes iguales, aderezados con los mejores efectos especiales que he visto en mucho tiempo.
        
            