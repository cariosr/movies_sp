
                      Cierto es que la dirección me parece magnífica y que decir de la interpretación. Lo de Michael Keaton me parece soberbio y lo de Naomi Watts o Edward Norton espectacular.
Me ha gustado pero quizás iba con las expectativas demasiado altas. Quizás me esperaba algo menos explorado, bastante más oscuro y a la vez cómico.
Conclusión: actuaciones para recordar y un trailer que enseña demasiado.

