
                      Película sencillamente brillante, no soy de películas de acción en coches tipo Fast And Furious o películas de tramas principalmente ilegales. Pero es que esta película roza la perfección, los personajes estan demasiado bien construidos, con dobles cambios de personalidad inteligentes, giros inesperados para el espectador, Baby es brutal tanto en la acción como fuera del volante, etc... Y ya la guinda del pastel es el despligue sonoro de todo el filme, no falta detalle.
        
            