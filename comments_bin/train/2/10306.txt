
                      Me importa un bledo lo que diga la gente yo adoro esta película, cuando la vi en el cine tenía ocho años y la disfrutaba a pesar de sus defectos y de no ser fiel al espíritu del videojuego, me encantaban los goombas, esa ciudad estilo Blade Runner, la música de Alan Silvestri, la canción de Roxette, las escenas de acción y la aparición de mi actor favorito Bob Hoskins que es clavado a Mario, menos mal que con los años ha sido reivindicada como un título de culto.
        
            