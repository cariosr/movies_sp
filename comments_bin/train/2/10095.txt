
                      ¡Qué gusto y qué milagro poder contemplar en estos tiempos una vuelta al CINE con mayúsculas! Ford, Capra, incluso el Victor Fleming de "Lo que el viento de llevó", se nos aparecen como por ensalmo, a fucilazos, a lo largo de esta joya cinematográfica. La he visto 7 veces y la vería otras 7 más, porque está hecha para la gran pantalla. La verdad, lo tiene todo, y uno, si tiene buen corazón, sale siendo mejor de como entró. Gloria a Spielberg y que Dios nos lo conserve muchos años, aunque como ésta, le van a salir pocas.
        
            