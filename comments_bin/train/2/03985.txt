
                      He visto esta película hace una semana y todavía recuerdo perfectamente lo mucho que me gustó. La verdad es que no soy muy fan de este tipo de películas, adaptaciones de videojuegos o cintas de acción un poco fantasiosa, pero me invitaron al preestreno y he de reconocer que fué algo inusual ya que supero con creces todas mis expectativas. Quizás porque tampoco fui con muchas, la verdad. 

Se merecen una mención los espectaculares efectos especiales y el desarrollo de la historia que, salvo en algun momento puntual, no deja de ser interesante e intrigante de principio a fin, haciendo la cinta entretenida durante todo su metraje, algo que es difícil lograr con este tipo de producciones, que te aburren con tanta batalla y tal.
        
            