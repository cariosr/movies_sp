
                      Pocas peliculas han logrado emocionarme, mas a mis 40 pasados, esta ha sido una de ellas, la musica, la historia y el circo, todos los ingredientes para un colosal espectaculo de luz y color. Solo viendo (a posteriori) el como se hizo y como eran los ensayos se puede entender la grandeza de sus numeros.

E X C E L E N T E.
        
            