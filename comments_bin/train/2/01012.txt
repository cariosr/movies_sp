
                      La tensión, no la sangre derramada, alimenta este apocalipsis zowie zombie. La multitud de Walking Dead puede sentirse abatida por la relativa escasez de carne podrida y pústulas con fugas. Pero World War Z sigue siendo tan inteligente, cambiante y atemorizante como un zombi hambriento listo para devorarte, cariño, tú.

¿Sorprendido? Yo también. Los rumores de nuevos retoques, reescrituras y problemas de producción han plagado la adaptación cinematográfica del director Marc Forster de Max Brooks (hijo de Mel), de casi 200 millones de dólares, desde que comenzó la filmación en 2011. Atornille el zumbido tóxico. La adictiva y emocionante World War Z viene lista para el rock. Te agarra fuerte, sin piedad.
        
            