
                      Soberbia, con un mensaje brutal, un dúo interpretativo insuperable y visualmente rompedora, es una película para verla una y otra vez y sacarle nuevas texturas, una obra maestra. El cine de Fincher en su máximo exponente, visual, sónico, ambiental y psicológico. Una obra maestra
        
            