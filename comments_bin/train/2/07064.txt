
                      Esta película es un clásico de los años 70 que perdura hasta hoy. El trabajo de Ridley Scott a la dirección es excelente, gracias al buen guión de Dan O'Bannon y a las grandes interpretaciones, en especial la de Veronica Cartwright y Sigourney Weaver como la gran teniente Ripley. Pero lo que es una autentica pasada son los efectos especiales de Brian Johnson, acompañado por Nick Allder.
        
            