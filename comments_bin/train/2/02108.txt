
                      Me parece un sano ejercicio de cine en estado puro en la era de los efectos especiales y del cine virtual. Pero considero que esta sobrevalorada. Hasta la mitad le falta contenido. Despues coge fuerza y te engancha. Por lo demas la historia tiene como moraleja que la adaptacion a nuevas circunstancias puede abrir un mundo de oportunidades y represantar un paso adelante sin que comporte renuncia alguna, salvo en este caso compartir el exito con las nuevas generaciones. Si te gusta el cine de antes no lo dudes vete a verla.
        
            