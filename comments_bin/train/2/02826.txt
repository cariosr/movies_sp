
                      ¡La comedia perfecta del año! Es la película con la que más me he reído que yo recuerde. Las seis ofrecen buenos papeles pero, sin duda, la que borda su personaje es Melissa McCarthy. Ofrece un papel que, al principio piensas que es la típica "gorda" (con perdón) que dice cuatro barbaridades sin fundamento, ¡pero no!, poco a poco vas viendo la parte humana del personaje y le llegas a tener cariño, convirtiéndose en el pilar fundamental humorístico. Deberían hacerle un hueco en las nominaciones al Óscar como secundaria. ¡Muy recomendable!
        
            