
                      Un poco más sosa que la primera, pero aún así realmente bonita... Estoy de verdad enamorado de estos dibujos y este universo. Nunca se han visto colores tan magníficos en un filme de animación en 3D, y la caracterización está, también, muy conseguida. En el aspecto escenográfico esta secuela está más orientada a la acción y el transfondo es menos visceral, si bien nos divertimos mucho con ella. Una cosa es cierta: Dreamwoks hila muy fino con sus producciones.
        
            