
                      Para mí está película es como una buena canción o un buen libro, no me canso. Cuando la ví en el cine me atrapó. La temática sobre la soledad, la desesperación, la supervivencia en un espacio limitado en medio de la galaxia, la necesidad de conpañía, el amor... con una banda sonora bien elegida que enfatiza las sensaciones de cada momento. Mostrar, decir tanto con tan pocos actores en un escenario trabajado a la perfección y unos efectos especiales impresionantes. La interpretación magnífica, muy bien elegido el elenco.
        
            