
                      A veces uno se topa con esas joyas del cine sin esperarlo. The Artist reúne humor, tensión, ingenio, pasión, romanticismo y entretenimiento. Es decir, reúne todo aquello por lo que vamos al cine. Cine mudo en pleno Siglo XXI, si, pero un reparto que borda una actuación descomunal (sobre todo Jean Dujardin), y una banda sonora de las que alegran el día más oscuro. Un 10 como una casa. Normal que arrasara en los Oscars...
        
            