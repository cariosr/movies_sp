
                      La primera película que recuerdo que me puse en el camino de convertirme en un fanático del horror de toda la vida fue It! El terror desde más allá del espacio. Se fabricó a bajo precio en 1958 y se presentó un tipo con un traje de monstruo con cremallera y espalda, pero para mis ojos de seis años era una obra maestra del terror. Básicamente, la trama involucró a un extraterrestre desagradable que sube a bordo de una nave espacial y comienza a matar a los miembros de la tripulación uno por uno.

El director Ridley Scott tomó esa idea y la implementó en 1979, el resultado fue Alien, un éxito crítico y la taquilla que llegó a recaudar $ 100 millones en todo el mundo, una cantidad enorme para esa era.

Extranjero: El Corte del Director incluye algunas imágenes adicionales por Scott, incluida una escena en el que el heroico Ripley (Sigourney Weaver) descubre los restos de dos víctimas alienígenas (Harry Dean Stanton y Tom Skerritt). También se han realizado restauraciones y mejoras digitales en la mezcla de sonido y negativo de la película original, pero lo mejor de este relanzamiento es simplemente la oportunidad de ver lo nuevo en el mundo Alien en los cines.

Nota: 4/4
calificación: Fresh
        
            