
                      Pawel Pawlikowski nos cuenta, de manera muy íntima, una historia de amor y condena, inspirada en la verdadera historia de sus padres.
Con total delicadeza en el lente, el polaco nos transporta a Varsovia, París y algunas otras locaciones de la Europa de los 50 haciendo uso de una relación de aspecto 1.37:1, una iluminación clásica y una fotografía completamente en blanco y negro; creando así una atmósfera atrapante instantánea que, sin perder el toque moderno, plantea realidades pasadas.
Esta película cuenta (como su slogan dice) un romance imposible en tiempos imposibles, en plena Guerra Fría, Wiktor y Zula se ven condenados a amarse y a encontrarse luego de cada separación sin importar la magnitud de la discusión.
Una crítica a los límites impuestos en tiempos de guerra y a los tiempos en sí mismos, con un final que más que nada deja un vacío en el espectador.

Morir solo, o vivir encadenado a alguien, el amor a veces puede doler, pero.

¿Qué seríamos sin él?
        
            