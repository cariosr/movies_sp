
                      Apoyar causas complicadas es extraordinario,  viajar 1500 km sobre una bici es extraordinario,  que el papa te reciba y oiga tu demanda es extraordinario.  Grabarlo todo y mostrar este bello documental con la delicadeza,  sencillez,  alegría,  sentimiento con el que Paola García Costa muestra esta aventura tan personal,  este maravilloso viaje a la vida de Dani Rovira junto al padre de una niña con síndrome de rett es simplemente una maravilla. 
No dejéis de verla y cultivar vuestros sueños!
        
            