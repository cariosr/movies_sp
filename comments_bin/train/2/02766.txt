
                      Es muy buena película, cierto, sin embargo, a mi entender se le ha encumbrado al Olimpo de los dioses de manera injusta. No es culpa de la película es sí, que es buena, repito, sino de la prensa y del boca a boca. Si la vemos sin el fanatismo Tarantiniano descubriremos una película entretenida y con un sello personal -algo que podría decir también Almodóvar-,  nada más. No puedo creer que películas como Interestellar, Origen o Memento no estén por encima en el ranking.
        
            