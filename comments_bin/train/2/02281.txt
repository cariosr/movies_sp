
                      El humor argentino, debido seguramente a los avatares históricos, es un cóctel de finísimo y a la vez brusco e incisivo filo  que se nutre de aromas hispanos, italianos y ese poso salvaje, que podía parecer provenir de la proximidad de los ingleses en las Malvinas pero que yo creo que es producto oriundo de las cepas inmensas y despobladas de la Pampa que al espíritu debe sentarle como la represión victoriana inglesa. Un humor que no deja pasar oportunidad de explicarnos lo ruines y cabrones que podemos ser.
Y con ese humor se ha construido esta reflexión del papel no ya del escritor laureado, como es el caso de la película, sino de cualquier personaje celebre que se escapa del rebaño y al que el rebaño vitorea, halaga, homenajea sin por ello dejar pasar la oportunidad de señalarle que el rebaño ya no está por él.
La mediocridad apesta pero es cálida. Como los pedos.
Nuestro escritor que lleva cuarenta años fuera de Argentina, que ha recibido el premio Nobel de literatura y que desde entonces no ha escrito nada decide aprovechar una invitación del alcalde de su pueblo natal, Salas, para recibir el nombramiento de “ciudadano ilustre” y participar en algunos actos que se han montado con motivo de las fiestas populares.
Evidentemente lleva la intención de recargar pilas para poder seguir escribiendo y nutrirse de nuevo de aquello que le puso a escribir y que tantos éxitos le ha dado.
Todos en el pueblo le están “esperando”.
Desde el admirador sincero al que se finge admirador para sacarle algo, pasando por el político oportunista, el terrateniente nacionalista, el antiguo amor, el amigo que se quedó para siempre en el pueblo, la joven admiradora que lo contempla como una plataforma de huida, tienen su factura preparada.
¿Qué tiene el rebaño y el redil que tanto interesa al emigrante que un día se fue y pugna por encontrar algo que nadie de los que se quedaron sabe qué es?
Aciertos de la película son esos planos de los ciudadanos estáticos que contemplan el paso del “ciudadano ilustre”, esos “móviles” que sostienen a tipos que graban las cascaruja del personaje, pues a buen seguro no han leído un libro suyo en su vida o la constante pugna del personaje por ir andando a los sitios frente al solicito vecino que lo quiere transportar y que no entiende “ese afán por andar entre casuchas, polvo y árboles esquifidos de este pueblo de mierda”
Oscar Martínez transmite de manera brillante con su rostro, mirada y sonrisa toda la perversidad, sinceridad, determinación y algún sentimiento más del personaje que según dice el mismo “un día se fue del pueblo para no volver mientras que sus personajes no pueden salir”.
Impagable, a pesar de un leve histrionismo, la interpretación de Dady Brieva en el papel del amigo que se quedó con su novia, la del escritor, de toda la vida. Una gozada.
En fin, una película que reflexiona sobre la paradoja que supone que muchos de los “ciudadanos ilustres” de muchas sociedades sean personas que huyen precisamente de esas sociedades que los encumbran y de las que abominan por su hipocresía y vulgaridad y de la que, sin embargo, no pueden prescindir.
Un amor/odio que es correspondido por la misma sociedad, que lo mismo te levanta una estatua que te tomatea.
Aquello de que nadie es profeta en su tierra. Y es que quizás para ser profeta como para el amor, lo ideal es no saberlo todo… del profeta o del objeto de nuestro amor.
Siempre hay un aporte de la imaginación y de nuestros deseos que el conocimiento hace papilla.
Un lio.
La película,  más que notable.
        
            