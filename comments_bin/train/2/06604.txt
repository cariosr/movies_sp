
                      Sin duda la película Marvel más completa de todo su haber. Humor y acción elevados al cubo como buena tercera parte que es, sin renunciar a una historia ingeniosa 
con pies y cabeza. Iron Man 3 es entretenimiento puro y duro y el contrapunto ideal -y necesario- de sus más oscuros y solemnes competidores de DC Comics.
        
            