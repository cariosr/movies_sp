
                      La película está realmente ambientada, además que nos hace sentir tensos y asustarnos con algo que no sabemos qué es. A pesar de la actuación descentralizada de Naomi Watts y de screamers medio predecibles y de momentos, abusivos, vale la pena ver esta película que creo que a los amantes del cine de terror les va a gustar.
        
            