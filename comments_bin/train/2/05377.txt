
                      Primero que nada, decir que jamás tuve intención de verla ya que pensaba que sería una película romántica sin más.. pero reconozco que me equivoqué. 
Tanto había oído hablar de que era una película perfecta si necesitabas llorar que la tuve que ver casi obligada. Me recordó un poco a mí y mi pareja, así que de mitad para adelante de película ha sido un llorar y no parar, que me he tenido que secar con la camiseta porque no tenía ya pañuelos suficientes. 
En fin, hay detalles de la película que no me gustaron, como que fuera tan predecible, etc. Pero eso queda en segundo plano, te quedas atrapado en esa historia de amor de verano, puro y verdadero y te engancha.
Muy buena.
        
            