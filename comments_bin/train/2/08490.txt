
                      Si quieres pasar un rato divertido sin más, fresco y simpático sin grandes escenarios ni conversaciones profundas, pero con mucha chispa, es una película altamente recomendada. Saldrás del cine mucho más contento y con ganas de sonreír.
        
            