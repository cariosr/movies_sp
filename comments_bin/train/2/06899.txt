
                      Esta peli está muy bien hecha! La historia respeta bastante bien, con añadidos notables, el cómic de Peyo. Cuando nos fijamos en el comienzo, y a lo largo de todo el filme, tiene un fuerte aire de "érase una vez"...de forma imaginaria y a la vez, en un mundo real!. Muy buen film, recomendable para toda la familia.
        
            