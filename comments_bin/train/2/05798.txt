
                      La película es bastante buena. Me ha recordado mucho a Sherlock Holmes. Se consigue un suspense constante y también cuenta con un final inesperado. Cuenta con un reparto inmejorable pero pienso que se le dan demasiadas vueltas. Tal vez se le podría haber sacado más jugo a la historia. Otra cosa que no me gustó fue el hecho de que como en muchas series de crímenes, solo el protagonista ve las pistas.
        
            