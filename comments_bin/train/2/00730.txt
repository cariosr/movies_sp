
                      Una historia sencilla para conocer las entrañas del caballero oscuro. Muy buena, pero no mejor que las dos que le siguen. Christian Bale va a mas sin duda alguna y cada vez tiene al personaje mas pulido. Un actor de método que no decepciona, al igual que Nolan, que no falla, Oldman, correcto, muy acertado su papel. Sin embargo, Katie Holmes cojea bastante. En fin, un "superheroe" muy para adultos, con un guión serio y maduro y bastante realista, dentro de lo que es el mundo Batman.
        
            