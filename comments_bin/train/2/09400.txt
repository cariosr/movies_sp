
                      Fantástica película. Original, entretenida y conmovedora. Es una crítica al pensamiento existencialista vigente en la sociedad posmoderna de hoy en día. El personaje principal nos plantea las tres preguntas: Quienes somos? De donde venimos? A donde vamos?... Weir demuestra una profunda sensibilidad a la hora de tratar estas cuestiones  como lleva haciendo a lo largo de toda su obra.
        
            