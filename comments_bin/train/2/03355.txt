
                      Acabo de venir del cine de ver esta película con mi primo Paco y nos ha parecido buena, entretenida desde el principio a fin. Nos hemos fijado que tiene algunos huecos vacíos, pero en general está bien. Tiene momentos muy buenos y cargos de mucha acción, la protagonista lo hace francamente bien su papel. El argumento se entiende muy bien. 
Buena película para entretener al espectador, no es un peliculón, pero se puede ver.
La recomendamos.

Nota:7,50
        
            