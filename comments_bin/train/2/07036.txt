
                      Fui a verla con pocas ganas pero por fortuna es una de esas pelis que dices: "joder como me gusta el cine". Absolutamente maravillosa, engancha desde el principio. A pesar de ser una película de diálogos 
hay algunos planos que consiguen emocionar. Una ambientación de principios y mediados de siglo magistral, Thompson y Hanks impresionantes. el resto de casting inmejorable menos Crowe, las dos historias muy bien hiladas de principio a fin y con una magnífica realización.

Que mas se puede pedir?
        
            