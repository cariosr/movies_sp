
                      Extraordinaria, abrumadora e inesperadamente significativa para el futuro del universo cinematográfico de Marvel. Explora géneros muy diversos, con secuencias de acción real muy bien dirigidas, personajes magníficamente desarrollados y un guion de lo mas cuidado.
Se sustenta a si misma no solo como otra película mas de superheroes sino como película en general.
Actuaciones interesantes y personajes en facetas desconocidas con sentimientos nunca antes vistos en este universo, 'Captain America: Civil War' es una película que provoca en el espectador emociones que ninguna otra película de superheroes produce, sin importar si este es un fanático del cine de superheroes o un amante del cine en general.
        
            