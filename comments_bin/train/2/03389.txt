
                      La La Ciudad de las Estrellas es para mi una de las mejores cintas del 2016, un musical estelar, con unas actuaciones a la altura y aunque no goce del mejor guión, si sabe jugar bien con los colores, los planos y los sentimientos del espectador, con un final tan crudo como genuino.
        
            