
                      Excelente película, es la historia de cientos de mujeres que se enfrentan a una vida dirigida y escrita por hombres, y casi siempre hombres sin escrúpulos y de tendencia misógina de aquellos tiempos. Por suerte ya han cambiado mucho las cosas.
        
            