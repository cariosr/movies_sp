
                      Sublime. Perfecta, me ha gustado todo de ella. La banda sonora, la fotografía, la forma de dirigir de Tom Ford, el perfecto Collin Firth en una historia preciosa.
        
            