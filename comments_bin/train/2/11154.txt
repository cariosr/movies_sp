
                      Magnífica película. Un guión sublime, una dirección magistral y unos actores excelentes. La película alterna momentos de drama, de suspenso, de humor y por supuesto, de amor, que es el tema central. Esas dos historias de amor que se ven truncadas y quedan inacabadas.
Es de esas películas para disfrutar y ver varias veces, aunque a partir de la segunda ya no nos sorprenda ese final inesperado.
        
            