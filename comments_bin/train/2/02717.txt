
                      Me gusta ir al cine a la primera sesión, hay menos público y se está más tranquilo. De vez en cuando comparto la sala con un simpático grupo de minusválidos.

El jueves 28 estábamos en la sala ocho minusválidos en sus sillas de ruedas, sus dos cuidadoras y yo.

Cuando terminó la película una de las cuidadoras dice: "Manolito ¿te has dormido?". Y Manuel contesta: "¡Con unas chicas tan guapas, cualquiera se duerme!"

En una frase este cinéfilo había expresado la esencia de la película.
        
            