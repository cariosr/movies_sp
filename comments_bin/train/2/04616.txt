
                      Técnica: 5,5/10
Interpretaciones: 8/10
Guión: 8/10
Con un nudo en la garganta gran parte de la película.
Alexandra Jiménez soporta el peso durante el primer tercio de película, luego pierde peso.
Karra Elejalde notable.
Rovira sorprende y se quita de un plumazo los encasillamientos.
Los planos de cámara bastante justos y muy movidos. La música ambiental desacertada en ocasiones.
Una gran dosis de motivación y emoción.
Recomendable al 85%.
        
            