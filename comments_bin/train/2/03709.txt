
                      Cuando los padres de Bruce Wayne (Christian Bale) son asesinados ante sus ojos, se dedica a combatir el crimen en todas sus formas para evitar que lo que le sucedió le suceda a alguien más. Con la ayuda de su leal mayordomo Alfred (Michael Caine) y Lucius Fox (Morgan Freeman), el jefe de su compañía, Bruce Wayne se transforma en el terrorífico luchador contra el crimen Batman.

Recolectada a partir de fragmentos de varias décadas de la tradición de Batman, algunas de las raíces pulposas de los años 30, el renacimiento de los años 70 donde se presentó Ra's Al Ghul, el trabajo definitivo de Frank Miller sobre Batman de los años 80, la producción los llevó a cabo Con algo nuevo e inmensamente entretenido.

El aspecto más interesante de esta nueva película de Batman es lo aterrador que es, algo que siempre se ha atribuido a Batman pero que nunca se mostró en la pantalla. Muchas de las primeras escenas de Batman de Batman Begins se rodaron como una película de terror, con la amenaza de Batman invisible, que se escabullan a través de los efectos de sonido. Y The Scarecrow (Cillian Murphy) es realmente aterrador cuando su gas de miedo está en efecto.
        
            