
                      Película distopica con un guión firme y bien resuelto.  No es la típica de tiros sin razón, sino que lleva una trama que puede resultar lenta para los que buscan aliens y disparos, y es el tiempo necesario para que todas las piezas vayan encajando en el puzzle.  Buena película de ciencia ficcion 
menos enfocada a lo comercial que nos tienen acostumbrados.  Un acierto.
        
            