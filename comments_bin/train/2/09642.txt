
                      Peter nos vuelve a regalar un maravilloso paseo por la tierra media, la película está a la altura de lo que nos tiene acostumbrado y la forma en la que esta adaptando el libro es magistral, cierto que se está tomando libertades que para algunos son hasta sacrílegas pero a mi juicio le a dado el punto necesario para atraer tanto a cinefilos como a tolkienmaniacos, en definitiva una película completa y que podemos disfrutar como "ENANOS" y que nos queda un año de mordernos las uñas esperando la conclusión de esta trilogía
        
            