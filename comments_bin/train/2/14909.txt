
                      Asombrosa. Una obra maestra que te hace sufrir por la escoria humana, te hace reír y hasta emocionarte y llorar.
Creo que lo tiene todo. 
Brie Larson con un merecidisimo Óscar y Jacob Tremblay, la joya de la película, que debería haber arrasado en todos los premios porque con lo pequeño que es nos regala una interpretación única.

Es una pena que a lo largo del año no podamos disfrutar de más películas como Carol o Room.
        
            