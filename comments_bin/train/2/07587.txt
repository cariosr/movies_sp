
                      Tal vez nunca hayas oído hablar de Guardians of the Galaxy, la franquicia de cómics de Marvel que se marchita en las sombras mientras Spider-Man, X-Men y Avengers reciben todo el amor. Tal vez pienses que una gran película sobre los íconos de Marvel de Wanna-be no vale la pena.

Animarse. Los guardianes de la galaxia hacen lo imposible. A través del deslumbramiento y la muda suerte, convierte los clichés de las películas de cómics en sus cabezas de idiotas y te golpea como una explosión emocionante de diversión-diversión-diversión. Es una locura, descaradamente tonta, solo una razón para amarla.

Otro es el elenco para arriba-para-nada. Chris Pratt está felizmente bien como Peter Quill, quien se llama a sí mismo el Señor de las Estrellas, sobre todo porque nadie más lo hará. Desde la edad de nueve años, cuando fue trasladado de la Tierra al espacio después de la muerte de su madre, Peter ha estado cazando recompensas por todo el cosmos en el trabajo corrupto de Yondu (Michael Rooker) de piel azul.
        
            