
                      Si tu saltas... Obra Maestra.


Al principio del metraje vemos los restos del increíble trasatlántico y nos topamos con una viejecita adorable. Tras ello se escuchan frases tan profundas como: "el Titanic era el buque de los sueños" o "todavía olía a recién pintado, la vajilla nunca había sido usada, nadie había dormido entre aquellas sábanas" y se nos presentan a los personajes principales y sus situaciones, además de el primer encuentro entre Jack y Rose.

Después se nos cuenta la historia de un amor apasionado, increíble y entrañable, con momentos estelares como ese: "Jack estoy volando", "si tu saltas yo salto", entre otras cosas.

Tras esto chocamos con un enorme iceberg (y lo digo en 3ª persona, porque es como si lo vivieras), que hará naufragar al majestuoso barco y aqui es donde empieza la desesperada carrera por sobrevivir de los enamorados, mientras observamos el impresionante y dramático naufragio, con unos efectos especiales apabullantes. Una vez hundido el barco, hasta el final de la cinta, la lágrima está asegurada.

En definitiva una película inolvidable, con buenos diálogos, buenos actores (mención especial a Kate Winslet), buen director, buenos efectos visuales, la historia de un naufragio que nadie olvidará y uno de los más sonados romances cinematográficos hacen de "Titanic" una cinta, simplemente, imprescindible. No se la pierdan.
        
            