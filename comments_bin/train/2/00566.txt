
                      Aburrida, pretenciosa, confusa, a veces ridícula...pero extrañamente, tengo un recuerdo precioso de ella. Transmite muchísimo, y es auténtica comunicación cinematográfica. Verás planos, enfoques, iluminaciones, secuencias, etc imposibles en otra producción. La vida según Malick
        
            