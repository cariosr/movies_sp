
                      Personalmente, decir que “Los odiosos ocho” es la mayor obra maestra de Quentin Tarantino, quizás sería excederse. Y es que para mi gusto, “Kill Bill” es la joya de la corona del universo del director. Pero entiendo que para muchos lo sea. Entiendo que muchos la consideren una obra maestra, puesto que si no estuviera (para mí) “Kill Bill”, ésta lo sería.

La película goza de una peculiaridad exquisita: ocho personajes atrapados en la nieve dan para mucho. Y si encima los guía Tarantino, aquí podríamos perfectamente tener una trilogía de películas, pero que para no agotarnos, no tenemos. Pero que podría, perfectamente. En un camino entre las historias de misterio de Agatha Cristie y los westerns setenteros, se encuentra un bar de montaña donde se dan cita ocho mercenarios, caza recompensas, embusteros y ladrones de poca y mucha monta. Cada uno con un objetivo, cada uno con un secreto, pero todos con la misma ruda apariencia. Y por supuesto, sin dejarse amedrentar por nadie, y que ninguna piedra les obstaculice en su camino. Las peleas, las (largas) conversaciones de poder y las escenas más grotescas, están servidas.

Tarantino vuelve a ofrecernos una historia que él hace más larga de lo debido, pero que con su especial manera de rodar buen cine, nos entretiene, nos alegra la vista, nos revuelve las tripas, nos hace reír, pasarlo bien y sobre todo disfrutar. Disfrutamos del blanco paisaje, de unos personajes llenos de carisma, de una historia que parece simple pero que  es retorcida y de unos planos llenos de vida. Tarantino es tan cuadriculado que aunque no seas un seguidor extremo del director, disfrutarás de sus cameos y autohomenajes. Pero sobre todo, disfrutarás de los que has ido a ver: “Los odiosos ocho”.

Lo mejor: Jennifer Jason Leight, digna candidata al Oscar.
Lo peor: su duración. Siempre una película que pasa de las dos horas y media es excesiva. Por muy buena que sea.
        
            