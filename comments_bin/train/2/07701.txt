
                      Disney/Marvel han vuelto al ataque este verano y a lo grande, con una nueva apuesta, una adaptación de unos comics que empezaron en 1969 pero que no son de los más conocidos de la casa de las ideas (marvel), pero con un reparto joven, con Zoe Saldana entre ellos, un trailer espectacular y una música muy pegadiza (Hooked on a feeling) consiguieron despertar el interés de todo el mundo. Pero no tenía que quedarse en esto, la película tenía que ser buena. Hartos estamos de avances y trailers espectaculares y que luego la película se quede en nada. Pero no es el caso.

Guardianes de la Galaxia ha arrasado en las taquillas de todo el mundo y ha sido la película sensación del verano y lo ha hecho con méritos propios.

Dirigida por James Gunn, quien contaba con solo 4 películas en su currículum como director y son o cómicas o de terror, y protagonizada por Chris Pratt, Zoe Saldana (quien ya ha demostrado que al natural, verde o azul siempre sale preciosa y desaeable) y Dave Bautista en lo papeles principales, y con las voces en V.O. de Vin Diesel como Groot, Bradley Cooper como Rocket y Josh Brolin como Thanos, esta película se ha convertido sin duda en la mejor del verano

Guardianes de la Galaxia es una película fresca, espectacular, divertida, trepidante, llena de acción, de gags de humor y con una selección de canciones de los 80 espectacular. Una de las mejores de Marvel hasta el momento que se disputa el puesto con Los Vengadores y muchos son los que dicen que la supera.

Cuenta la historia de un grupo de, hasta el momento, desconocidos vividores y/o delincuentes que por cosas del destino tienen que juntarse para salvar todo un planeta. ya se sabe, los enemigos de mi enemigo son mis amigos.

A los jóvenes les va a encantar por la trama y la acción pero este grupo variado y variopinto harán las delicias del espectador que se haya criado en los 80, porque si una cosa tiene esta película son referencias constantes a esa década y es muy friki para quien sepa reconocer algunos gags y personajes (reales y de comics/películas) que aparecen a lo largo de la trama.

James Gunn ya se ha asegurado una secuela que está prevista para el 2017 y se habla de un posible crossover con Los Vengadores. No olvidemos que pertenecen al mismo universo Marvel y son contemporáneos.

Así pues si aun no la habéis visto no dudéis en hacerlo y desde aquí deseo que las secuelas mantengan este nivel.
        
            