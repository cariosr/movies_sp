
                      Es una maravillosa película. De esas que de vez en cuando aparecen. Una obra maestra.
Conecta dos mundos y llega al fondo de los sentimientos humanos con gran sencillez y precisión.
        
            