
                      O Brother! es una comedia dirigida, producida y escrita por los hermanos Coen; Ethan y Joel, directores de películas como Crueldad Intolerable, No es país para viejos y El hombre que nunca estuvo allí.

Los reclusos Everett (George Clooney), Pete (John Turturro) y Delmar (Tim Blake Nelson) escapan de prisión con el fin de conseguir 1,2 millones de dólares que Everett robó y enterró antes de que le detuvieran. Deben darse prisa, ya que en cuatro días una presa inundará el lugar donde se encuentra el dinero.

Candidatos a dos premios al mejor guion original, los hermanos Coen no decepcionan con esta comedia.
Una crítica de aquella sociedad con numerosos toques de humor, algo típico de los directores.

La película cuenta con un reparto de actores más que espectacular, con George Clooney y John Turturro a la cabeza. 
La banda sonora, compuesta por canciones country muy pegadizas, ganó un Grammy. Acompaña a la historia a la perfección e incluso los protagonistas se atreven a cantar en un par de ocasiones.
        
            