
                      La Guerra del Planeta de los Simios (2017).
"Esta guerra no la empecé yo, pero la acabaré yo".
Matt Reeves repite en esta tercera entrega (segunda que dirige y primera que coescribe) con broche de oro. Más dramática, más intimista, más ecológica y desde luego, más crítica, esta última parte se centra totalmente en el personaje de Andy Serkis (César), quien ha evolucionado a lo largo de la saga de manera exponencial.

Su némesis es el alter ego de Woody Harrelson, un despiadado coronel que odia a todo lo que huela a simio. Tanto él como Andy Serkis y el resto de monos están extraordinariamente bien interpretados (incluyendo al pequeño chimpancé que da el contrapunto humorístico, Steve Zahn), pero es el responsable de Golum y King Kong el que nos sorprende y emociona con su forma de moverse por la pantalla y su sobriedad. La mirada de nuestro protagonista es poco menos que perfecta. De una fuerza majestuosa. 

Para completar la factura de la película tenemos una música estupenda y, sobre todo, una fotografía de primera. El maquillaje y los efectos especiales hacen que dudemos cuándo nos encontramos ante un simio artificial y cuándo ante un mono auténtico. No echéis cuenta a la crítica (la pone bien), porque esta vez, aunque hablen bien de la cinta... en realidad es buena...

Si queremos ponerle un pero hablaríamos de su duración; creo que la historia se puede contar en media hora menos de metraje, pero por lo demás, la intriga e interpretación que posee nos hace olvidarnos del reloj y de los minutos sin acción.

Raúl Cabral.
        
            