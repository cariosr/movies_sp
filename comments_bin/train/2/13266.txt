
                      El Titanic se deslizó en el Atlántico Norte hace un siglo, y parece que hace mucho tiempo que se lanzó la gigantesca película de James Cameron. Verlo de nuevo, con el filip agregado de efectos 3-D, es una experiencia interesante.

'Titanic en 3-D'
Jugando ahora

La película en sí es como la recordamos, una telenovela romántica y entretenida que de repente se convierte en una lucha por la supervivencia. En la primera mitad, nos proyectamos en ella, en la segunda intentamos imaginarnos tratando de salir de ella. Lo mejor del cine es la forma cautivadora en que "Titanic" nos succiona en lo que parece, en la superficie, ser un romance magnético, mientras que debajo, hay corrientes peligrosas de conciencia de clase y cardúmenes de arrogancia. (¿Ya agotó las metáforas? No es muy probable).

Y luego, de repente, ese iceberg arranca la cubierta segura y estable de debajo de los personajes. No te metas con la naturaleza.

Hay tantas capas de significado asociadas a la tragedia del Titanic que no es de extrañar que se haya convertido en una piedra de toque de la mitología del siglo XX. Cameron lo entendió y, sin embargo, no lo utiliza. La película fue objeto de burla por su diálogo simplista; Me sorprende su economía de superposición expositiva. La maldita cosa tiene más de tres horas de duración y, sin embargo, no quieres dejar tu asiento por un segundo.
        
            