
                      Potente, preciosa, emotiva, tierna, dura, muy humana y de un colorido onírico espectacular. Bayona se sale, metiendo el dedo en la llaga, dando donde más duele, en una película que es un baño de realidad a través de una fantasía infantil para adultos.
        
            