
                      Increíble película sobre el drama humano en una persona enferma e impedida en toda su movilidad, no, no es una película lacrimógena, esta llena de sensibilidad, humanidad, esperanza y un gran sentido del humor. Pude presenciar el estreno de esta joya el pasado Zinemaldia, a la salida del mismo con uno de sus actores y director presente y 
no pudimos por menos que romper en una ovación que emociono a los protagonistas. Gran interpretación de los dos actores principales en sus memorables papeles. No se la pierdan, entretenimiento asegurado pero con algo mas.......
        
            