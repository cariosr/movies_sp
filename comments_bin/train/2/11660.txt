
                      La película nos presenta la vida (el amor) en estado puro. Es emocionante ver el trabajo magistral de un W. Allen renovado. La interpretación es excelente y la película me trae reminiscencias almodovarianas.
        
            