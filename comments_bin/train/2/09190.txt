
                      Esta es una película que no te habla solamente de la red social que revoluciono al mundo, si no que también sobre la ambición, el triunfo, la amistad y la traición, mientras  te relata como es posible que camino al éxito ganes amigos pero también los pierdas.

La historia es como un sueño americano pero llevado a los tiempos modernos puesto que solo basto una computadora y una idea para así crear la red social mas importante en la actualidad.

Lo que me agrado bastante es como te presentan a mar  Mark Zuckerberg (Jesse Eisenberg) ya que en pocos minutos sabes como sera la personalidad y la forma de ser de este.

David Fincher es el director y hace un trabajo excelente con el guion de aaron sorkin , en cuanto a las actuaciones  de Jesse Eisenberg y Andrew Garfield son brillantes. Si mezclas un buen director, un buen guion, una banda sonora increíble y unos actores talentosos el resultado final es "The social network"
        
            