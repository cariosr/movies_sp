
                      Un flim que te mantiene al tanto de lo que pasa y admiras cada uno de sus detalles. Uno de  los mejores films de David Fincher. Y el final es de lo mejor de este director. 
 Ernest Hemingway once wrote, "The world is a fine place and worth fighting for." I agree with the second part.
        
            