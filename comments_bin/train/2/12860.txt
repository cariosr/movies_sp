
                      Star Wars: Una Nueva Esperanza es una 'space opera' dirigida por George Lucas, conocido por producir las películas de Indiana Jones y por encargarse de los efectos especiales de Jurassic Park.

La princesa Leia robó los planos de La Estrella de la Muerte, una estación espacial liderada por Darth Vader, con el fin de que los rebeldes pudieran destruirla.
La princesa es secuestrada por el Imperio antes de que los planos logren llegar a la alianza rebelde, pero consigue transmitir la información a un pequeño robot llamado R2-D2, que junto a su compañero androide C-3PO, será enviado a Tatooine para transmitir dicha información.

Star Wars ha llegado a ser una saga de películas muy influyente, ya que otras películas se han inspirado en estas, y en muchos filmes y series de televisión hacen referencia a ellas.

John Williams es el encargado de la banda sonora de la película, considerada una de las mejores bandas sonoras de toda la historia. Cada canción es totalmente diferente a las demás, son originales y pegadizas. Auténticas obras maestras.

La película fue nominada a treinta y cuatro premios diferentes, en distintas categorías, de los cuales ganó veintitrés.
        
            