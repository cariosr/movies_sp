
                      Supera ligeramente a la anterior de Jurassic World (2015) aunque todavía le queda un poco para llegar a las dos más grandes: The Lost World (1997) y Jurassic Park (2001).

Lo Mejor:
-Los efectos visuales de los dinosaurios, la combinación entre animatrónicos y CGI es perfecta, sin duda es la película de la saga dónde mejor están realizados los dinosaurios (a diferencia de la de Jurassic World, donde había veces que dolía ver como se notaba tanto como estaban hechos por ordenador).
-La fotografía, espectacular.
-Que sea más aterradora que la anterior, recordando más a las dos primeras de Jurassic Park.
-La primera secuencia es brutal.
-