
                      Una genialidad de ida de pinza de Martin McDonagh. No soy nada fan de Collin Farrell y me ha encantado en ésta película, como nunca se le había visto antes. No hay manera de describirla más que ir a verla y deleitarse con grandes actores como Cristopher Walken y Woody Harrelson.
        
            