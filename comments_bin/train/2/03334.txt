
                      Me ha gustado mucho, una película muy buena, con una tensión muy bien llevada que te mantiene hasta el final con la intriga de cómo va a acabar. Todo ello aderezado con algunos toques gore muy puntuales que se compensan con sus equivalentes románticos. El director utiliza un lenguaje de cámara muy particular tanto, que a veces, se idependiza del propio guión, diciéndonos cosas importantes sin que los actores hablen.
        
            