
                      Única e irrepetible.  Sólo critico su excesivo metraje. Me parece excesivamente larga pero no sabría que eliminar. Todo es importante porque no se trata de una trama puntual aderezada con relleno, son el cúmulo de detalles de una vida normal. El crecimiento real de unas personas delante de una cámara.
        
            