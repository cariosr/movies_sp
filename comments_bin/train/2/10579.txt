
                      Empezaré diciendo que para mí, es una de las mejores películas de Stallone, junto a Rocky y Acorralado. Una de las menos citada de Stallone y que tiene un drama que lamentablemente en muchas cárceles del mundo se vive a diario unas veces por sobre población, otras por algunas deficiencias en los régimen carcelarios; pero entro a valorar la película no las situaciones ajenas a ella que eso daría para muchas charlas que no tienen que ver con cine.

En cuanto al desarrollo de la cinta el ritmo es acorde con la temática haciendo que los espectadores se metan en una situación dura para su personaje principal que fue llevado magníficamente por Stallone, quién en este tipo de tramas, tiene una forma de atraernos hacía lo que en un principio se puede antojar sencilla de analizar desde el punto de vista de derechos humanos y de relaciones personales. También el estar rodeado por buenos actores como Sutherland, Sizemore o Amos, le dio solidez a la historia. Sutherland está sublime, hasta el punto que llegas a odiarlo.

El ambiente desconozco más que por lo visto en pantalla, me pareció duro y carente de sentimientos donde una institución que se supone que está hecha para reinsertar, va matando y sacando de sus internos, lo peor de cada uno aunque a veces hasta en el peor de los escenarios, puedes encontrar palabras como el honor, la amistad… Allí la película tiene algo más que lo evidente. 
En definitiva, una de las mejores películas carcelerias.
        
            