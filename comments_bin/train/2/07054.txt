
                      Bien, la película sin duda una de las mejores en Stop Motion. Pero es selecto.
Vayamos por partes,

Trama: FANTASIA. Con un trama mas abarcado en la psicologia infantil. Comparando con el libro original, la película llega a ser en cierto punto mas interesante y oscura. 

Personajes: En mi opinion, los que se emplean no son de la calidad que uno esperaba, pero en sí, se camufla perfectamente con los ambientes. 

Diálogos; Este es en donde mas me gusta. Las voces de doblaje fueron bien escogidas, dando casi real personalidad. 

Bueno, eso por hoy. Me parece una buena película para verla en familia, escuché que algunos niños se suicidaron despues de verla, hay que tomar en cuenta que el mensaje que lleva escondido es relativamente siniestro, pero con discreción y prudencia se puede dejar de lado.
        
            