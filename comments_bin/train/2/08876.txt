
                      Fantástica película de suspense con giros esperados pero muy bien guiados. Si fuera americana tendría críticas de lujo y si fuera de Almodóvar nuestra candidata a los Oscars, una pena que no haya más películas así y mejores espectadores en los cines.
        
            