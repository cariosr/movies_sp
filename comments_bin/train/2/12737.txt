
                      Película muy recomendable. Te hace pensar, reír, llorar, enfurecer,...en definitiva SENTIR como cada uno de sus personajes.
Hacia tiempo que no disfrutaba de una película y aunque a priori se prejuzga con que las películas de animació son para niños esta no cumple la regla. A mi modo de ver los peques podrán verla y disfrutar de las aventuras de sus personajes pero los adultos veremos más allá y nos quedaremos con el fondo de cada anécdota, vivencia y situaciones.
En fin, ME HA ENCANTADO 😍😍😬😬
        
            