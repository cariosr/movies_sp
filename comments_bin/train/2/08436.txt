
                      Un filme hecho para fans.
El público promedio puede comprenderla pero no en su totalidad.

Hace manejo de un humor tolerable. El trasfondo de cada personaje es soberbio, resaltando las apariciones de Batman y Wonder Woman. 

Esta película te pide como requisito verla con ojos distintos a como miras los productos de Marvel.
        
            