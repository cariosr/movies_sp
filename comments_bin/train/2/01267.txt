
                      Rodrigo Sorogoyen demuestra cada vez más el buen hacer como director. En esta ocasión nos presenta la trama de corrupción dentro de un partido político español con un enorme Antonio de la Torre. Sobria, bien construída y con escenas soberbias que consiguen con auténtica maestría meterte en la tensión de la historia, para mí es la mejor película de su autor
        
            