
                      Espectacular de principio a fin. Las interpretaciones son excepcionales y el guión mantiene la tensión dutante toda la película. 
Si algo hay que destacar es la fotografía.  Los planos aéreos son un espectáculo visual.

Los Goyas de este año deberían llevar el nombre de esta película en varias categorías.
        
            