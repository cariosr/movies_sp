
                      El comienzo en un caos en la ciudad de Philadelphia, que me recuerda a la escena de Washington de Independence day, introduce un desarrollo muy del tipo videojuego, con mucha acción, muy rápida y dinámica, estresante.

Todo se sostiene sobre el personaje de Brad Pitt, ya que en realidad todo y todos los demás importan poco. En la actuación, como siempre soberbio, pocos actores hay tan versátiles y capaces de papeles tan diferentes y tan bien realizados. 

Enseguida se inclina hacia el prototipo de película de catástrofes/zombies/virus, bebiendo de otras como La guerra de los mundos y 28 semanas/días. Pero enganchando desde un primer momento.
Gracias a esa tensión e intriga, acompañados por una música y efectos sonoros totalmente acordes y de calidad.

La parte técnica, también está muy lograda.

Llama la atención, que apenas se vislumbran escenas explícitas, sin sangre ni violencia en pantalla, quedando casi siempre como intuido. Excepto en las partes en las que los militares emplean las ametralladoras y helicópteros de nuevo a lo videojuego, pero todo lejano.

El guión es flojo, con demasiadas copias y tragos de otras y un desarrollo llevado a veces a trompicones.
Hasta en  el concepto y acción militar tanta veces visto, el honor y sacrificio por la patria y los demás.

Pero en conclusión es muy dinámica, entretenida, vistosa con una gran sensación de agobio y terror psicológico.
A  mi parecer merece la pena.
        
            