
                      10 primeros minutos en los que la película se pierde en diálogos sin ningún interés, a la puesta en escena le cuesta asentarse, después poco a poco el elenco va tomando forma, muy buen juego de actores de los dos protagonistas, una pareja convincente y las réplicas son excelentes. Escenario todo planeado, ninguna sorpresa por esta parte, pero está bien pensado. Escenas graciosas mezcladas con situaciones ya vistas que hacen que esta comedia romántica sea divertida e interesante!
        
            