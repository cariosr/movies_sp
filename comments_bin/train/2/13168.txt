
                      Puede que ésta se la película definitiva de la Historia del Cine, aunque no necesariamente la mejor. Pocas veces se ha logrado semejante ejercicio de espectacularidad, sentido de la narración y emoción como en la superproducción de James Cameron, que vista hoy no pierde ni un ápice de efectividad y de habilidad para ponerte la piel de gallina. La banda sonora de James Horner y una gran pareja protagonista (en la que sí, Kate Winslet se come con patatas a Leonardo DiCaprio) redundan en los logros de un filme irrepetible.
        
            