
                      Claro, puedes tomar algunos disparos críticos. Llegaremos a eso. Sin embargo, en primer lugar le damos crédito a Jurassic Park por ser un entretenimiento colosal: el emocionante recorrido del verano y, probablemente, el año. Después de pelearse con Hook, el director Steven Spielberg filma las obras y convierte la ciencia popular en una aventura épica.

El bestseller especulativo de Michael Crichton en 1990 le da a Spielberg una jugosa premisa doble: ¿Qué pasa si el ADN de dinosaurio extraído de los mosquitos atrapados en el ámbar permite clonar especies extintas para un parque temático? ¿Y qué pasa si a estos gigantes reconstituidos no les gusta ser explotados detrás de cercas electrificadas?
        
            