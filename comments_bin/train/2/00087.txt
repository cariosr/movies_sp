
                      Vaiana es la última apuesta de Disney de la “no-princesa” Moana, que se alza como nueva heroína de sus películas de animación.

Es una película musical de aventuras y fantasías protagonizadas por la adolescente Vaiana, a quien vemos crecer feliz en una isla de la Polinesia, predestinada a liderar a su pueblo y elegida por el océano para cumplir una profecía, que restaurará a la Madre Naturaleza  el corazón robado una vez por un semidiós llamado Maui. Y en donde se muestra a una chica decidida, enérgica y capaz de valerse por sí misma, contra toda adversidad; muy lejos de los roles de las antiguas princesas Disney y más parecido con la últimas protagonistas de las últimas animaciones como Enredados, Brave o Frozen.

El nombre de Vaiana, ha sido rectificado de la original “Moana”, porque dicho nombre es una marca registrada de perfumería en Europa, y buscaron un parecido que encajase también en su significado, ya que Moana, en maorí, significa “gran extensión de agua, océano” y Vaiana, es una palabra que en Tahití se traduce como “agua procedente de la cueva”, por lo que se podía atribuir perfectamente en el contexto de la historia.

La animación está completamente hecha por técnicas digitales, salvo los tatuajes de Maui, que han sido creados manualmente y adquieren vida de forma ocasional. Los paisajes, la isla, las montañas, el mar, están llenos de color, naturaleza y efectos especiales que la hacen muy grata de visualizar y llenarnos de imágenes bonitas. A destacar la escena de “los Kakamora”, claramente identificados con la estética de Mad Max o Waterworld, y una de las más divertidas de la película.

En el doblaje español, las canciones de la adolescente Vaiana, han corrido a cargo de la que fue ganadora de la Voz Kids 2014: María Parrado, igual que lo hizo en Annie (2015), de las que destaca la canción “Que hay más allá” (How far I’ll go), tema principal de esta película compuesto por Lin-Manuel Miranda, protagonista absoluto del musical con 11 Premios Tony de Broadway: Hamilton.

Los directores de Vaiana, también lo fueron de otras películas clásicas de Disney como: Aladdin, Tiana y el Sapo o La Sirenita, aunque no guarda ningún parecido con esta última, incluso teniendo un vínculo marino. Lo que sí han conseguido es una buena historia, con unos personajes que transmiten mucho y además cuenta con buenas canciones, por lo que será un nuevo clásico de Disney en muy poco tiempo
.
Calificación: 🎥🎥🎥🎥🎥🎥🎥🎥🎥  (9 sobre 10)
        
            