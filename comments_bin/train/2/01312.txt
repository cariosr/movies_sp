
                      Una película de argumento sencillo y romántico, en que  debes tomar nota de los detalles. Destaco la fotografía y los increíbles paisajes que recorre el gran soñador de Walter Mitty, Groenlandia, Islandia e incluso las Montañas de Afganistán. complementados con breves momentos divertidos cuando Walter  llevado por su imaginación realiza actos temerarios que jamás habría realizado. Un filme muy bonito.
        
            