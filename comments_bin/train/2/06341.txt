
                      Obra maestra del thriller de este siglo, desde la elegantísima dirección de Ben Affleck hasta la cruda e insensible interpretación de su hermano Casey (que es, de lejos, mejor actor que su hermano). La historia de Lehanne es buena como todas las que escribe, por lo que ahí no hay lugar para sorpresas. La guinda del pastel, de hecho, reside en Morgan Freeman, en Ed Harris, en la violencia, en la suciedad y en los memorables diálogos de la película.
        
            