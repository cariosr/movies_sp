
                      Funny Games es un film que busca mostrar el lado más oscuro de la influencia de la violencia en la sociedad y sus repercusiones, en eso me resultó una cinta compacta haciendo de la violencia un arte más allá de lo explícita de las imágenes que algunas de ellas han sido bastante duras de digerir teniendo en cuenta la cabalidad de los espectadores que siempre quedamos de alguna manera u otra, horrorizados y sorprendidos de lo que es capaz el ser humano.

El ambiente también ayudó a crear esa atmósfera necesaria para desarrollar la trama. Resulta simple, abierto, poco novedoso pero efectivo… Con unos planos bien conseguidos y armonizados con las secuencias y eso se agradece porque a pesar de algunos fallos supo mantener la tensión en casi toda la duración de la película y eso es complicado y más cuando la temática en sí misma, no representa demasiados misterios. Aquí también influyó la elección de los personajes que dieron ese toque personal y realista a la cinta. Cada uno jugó un papel dentro del desarrollo y parecía que no estaban siendo usados al azar. 

Es una cinta de esas que se deben ver de forma analítica más que por visualizaciones estériles. Tiene muchos componentes psicológicos que le hacen una buena película si sois de los que os gusta analizar lo que veis.
En definitiva, buena película.
        
            