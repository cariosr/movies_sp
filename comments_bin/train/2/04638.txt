
                      Es leal a la franquicia sin ser nunca servil, y dobla la historia establecida en el ángulo correcto, lo que le da al conflicto central de la saga un nuevo tipo de vida.
        
            