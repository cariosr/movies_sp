
                      J.K. Rowling lo ha vuelto a hacer. Lo más sobresaliente de esta precuela de 'Harry Potter' es que no se parece en nada a la saga original. Es mucho más adulta y el personaje de Newt Scamander, interpretado de la manera más 'adorkable' posible por Eddie Redmayne, te llega a lo más hondo del corazón. Tampoco hay que perder de vista a Dan Fogler (Jacob Kowalski), la versión 'muggle' de Ron Weasley. Gustará tanto a los fanáticos de El Niño Que Sobrevivió como a los recién llegados. La 'era de Hufflepuff' comienza.
        
            