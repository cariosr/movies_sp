
                      Brutal película. Ya no sólo por cómo dignifica el trabajo de periodista sino por cómo trata un tema escabroso como el de los abusos sexuales por parte de curas a niños durante décadas. Sin guardarse nada y sin quitar un ápice o un matiz para suavizar lo ocurrido, tampoco se recrea ni se muestra más crítica de lo necesario. Cuenta lo que pasó, de la forma más objetiva posible. Periodismo 100%.

Gran música, gran guión y grandes interpretaciones. Ruffalo es incomprensible que vaya nominado como actor secundario porque es sobre él sobre el que recaen los momentos de más intensidad argumentativa y emocional de la película.

Sólo pondría como puntos negativos que puede ser un poco lineal en cuanto a sorpresas o emociones, demasiado fría. Puede que por ese no dejarse caer en el sensacionalismo. Y algo más personal, me dio a entender a mí y mi acompañante que se estaban fraguando un par de sorpresas y de giros de guión cuando al final no fue así. Pero la recomiendo sin dudas.
        
            