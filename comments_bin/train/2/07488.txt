
                      La recomiendo pero...
Cien por cien Nolan, incluyendo  todos sus elementos que le caracterizan: Película de una gran duración donde se divulga una idea y sobre ella gira toda la trama de la película no del todo razonada, aderezada con una banda sonora tan repetitiva como su argumento. 
Además como en la antepenúltima película del director se necesita varias revisiones para exprimir todo su jugo, máxime si la jerga científica no llegas a dominar.
Pero entonces la famosa fórmula para qué sirve?
Cien por cien Nolan.
        
            