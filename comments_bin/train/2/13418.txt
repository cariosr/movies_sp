
                      LA PELÍCULA. Combina emoción, comedia, historia... Te deja con una sonrisa en la cara y con ganas de aplaudir. Merece la pena verla en una buena sala y disfrutar de la música, del paisaje y de actuaciones magistrales. Premios bien merecidos.
        
            