
                      Para ver esta película hace falta ir con la mente muy abierta, personalmente salí encantada de la sala de cine ya que me remontó muchos años atrás a cuando era una infante.
Aprecié mucho el echo de que la película conservará muy bien la esencia de lo que fueron los Power Rangers años atrás, la dinámica de la película es la misma que siempre tuvieron los programas sobre estos seres, me encantó el final, me encantaron los personajes, me encantó todo.
No vayan con expectativas, vayan abiertos a lo que sea, con una mentalidad como si fueran aún esos niños a los que les encantaba el programa y les prometo que les fascinará.
        
            