
                      Se trata de un film con un argumento un tanto desenfado en comparación a lo que las tramas de James Bond, el galante por excelencia, nos tienen acostumbrados. Es un punto de inflexión dentro de la saga. Efectos especiales muy logrados y acción a raudales. Con un ritmo vibrante es una película 100% recomendable.
        
            