
                      Esta, es una buena adaptación, me gusta que Steven Spielberg haya decidido tomar tantos riesgos, gracias a estos cambios, la película se ve y se siente más tranquila, no se siente presionada para contar muchos sucesos.

 Me parece que lograron retratar excelentemente la esencia de la historia, trama y sobretodo los personajes.

 Excelente adaptación, por esto mismo, mi calificación tan cercana al 5.
        
            