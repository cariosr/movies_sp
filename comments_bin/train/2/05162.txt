
                      Es una pelicula con una factura de cámara muy sencilla en donde el montaje queda en un segundo plano dada la fuerza del guión, en este caso la novela de Jane Eyre. La película preciosa. Película intimista, la protagonista  desarrolla su papel de forma espléndida.
        
            