
                      Hoy cine para llorar: LA LADRONA DE LIBROS
Preparados para llorar con La ladrona de libros, una historia que jamás debía de haber sucedido, que recuerda por su dureza a La vida es bella, que habla de la Segunda Guerra Mundial, que podemos ver como reciente estreno cinematográfico y, que es brillante en muchos aspectos el filme de Brian Percival, responsable máximo de La ladrona de libros, una película muy recomendable.
        
            