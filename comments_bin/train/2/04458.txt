
                      En general 'Zipi y Zape y la isla del Capitán' es una buena película de entretenimiento para todos los públicos que hará las delicias de niños y mayores. Bien hecha, bien llevada y con unas buenas actuaciones, nos encontramos con un producto con el que divertirnos y pasar un buen rato. La recomiendo y más con este calor.
        
            