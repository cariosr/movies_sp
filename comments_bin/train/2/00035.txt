
                      Pelicula complicada e inteligente, poco comercial, desde luego, pero que interesará a todos aquellos que quieran ver algo realmente diferente. Fue, opino, merecidamente premiada en el festival de Cannes. Una de esas peliculas que sorprenden por lo diferente y peculiar.
Buen guión y escenografia. En más de una ocasion se ve uno reflejado en las situaciones que se narran. Eso sí, no se trata de una película para no pensar y/o analizar, no resulta fácil y a veces es desconcertante.
Está bien que de vez en cuando se estrenen en nuestro país cintas de cinematografías tan desconocidas por estos lares como la cinematografía griega, de la que yo, por ejemplo, nunca había visto una película. 
En resumidas cuentas, otra de las mil y una formas de hacernos llegar buen cine.
        
            