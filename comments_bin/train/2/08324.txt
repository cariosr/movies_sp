
                      Ejercicio impecable de Shyamalan, volviendo a la senda de la excelencia, con un James McAvoy inmenso y una historia oscura, terrorífica y tensa a partes iguales. Todo al servicio de un guión que te vuelve a enganchar y te sumerge en el universo de su director, con todos sus matices de cámara, música y colores. Definitivamente, estamos ante un autor esencial del cine contemporáneo
        
            