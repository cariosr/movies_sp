
                      Me gustó mucho. La verdad es que fui al cine sin esperar una gran maravilla, ya que apenas había leído sobre ella, y ni tan solo me molesté en ver el trailer, pero el hecho de que saliera Dicaprio, me hizo ver la peli, ya que sus actuaciones en películas como Shutter Island o en origen me gustaron mucho y no pude evitar verla. Y su actuación no me decepcionó ni un pelo, es impecable. En cuanto la de Jonah Hill... me dejó sin palabras, mas que buena.

En cuanto a la película, como he dicho, no esperaba nada de ella y me gustó muchísimo, hay que tener en cuanta que dura 3 horas! Y en estas horas hay momentos de todo, hay alguno en que se puede hacer un poco larga, ya que hay mucho dialogo y se repite un poco, pero aun así no le quita interés. Y las escenas cómicas... son un puntazo, reí muchísimo con la película, no pensaba que tuviese tantas escenas de comedia, y son irreprochables, quieres mas y mas con cada una de ellas.

Muy pero que muy recomendable la película.
        
            