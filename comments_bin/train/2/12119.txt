
                      Si vistes las ediciones 1 y 2 esta te parecerá diferente. Los juguetes han desarrollado un "alma humana" y reaccionan con una ternura que hará que se te salten las lagrimas. Es una película preciosa, y técnicamente lo mejor que he visto en cine de animación. El 3D es fluido y suave y te sumerge plenamente en la acción. Va a ser difícil superar una obra maestra como esta. Nadie debería perdersela!..
        
            