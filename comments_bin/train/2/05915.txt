
                      Curiosamente a pesar del tema que se aborda, se pasa un buen rato. Del humor digno de la corona, los personajes carismáticos y la reproducción salen del fondo del alma. Los terroristas en proceso, torpes y caricaturescos, hacen que prácticamente nos olvidemos que es una película de actualidad sobre una realidad preocupante y angustiosa. Gran controversia = gran diversión. Eso es lo bueno. No hay que olvidar que se puede reír de todo, pero no podemos reírnos con todo el mundo. Besos.
        
            