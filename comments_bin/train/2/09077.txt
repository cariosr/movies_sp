
                      Relanzado 40 años después, y pulido digitalmente, la sorprendente y ambiciosa secuela de Francis Coppola a su primera película de El Padrino es tan apasionante como siempre. Es incluso mejor que la primera película, y tiene la mejor escena final en la historia de Hollywood, un verdadero golpe de cine.

Michael Corleone (Pacino), su rostro ahora una máscara arrugada de odio implacable, ha cobrado venganza a todos sus enemigos, y luego, de repente, recortamos 20 años al joven Joe College, aún capaz de mostrar una sonrisa infantil, sorprendiendo a su Hermanos en la mesa de la cena anunciando que se ha unido al ejército y que defienden firmemente su patriotismo. Es un florecimiento narrativo impresionante: misterioso y conmovedor.
        
            