
                      Es una película con excelentes actuaciones y se nota que tienen una química increible los actores, es muy entretenida y no, no solo por el humor sino por las épicas batallas y lo bien que están dirigidas, La historia es muy buena y opino que aunque tiene muchos personajes Joss Wheddon y los guionistas se esforzaron por hacer que no nos aburrieramos con tantas subtramas, Ultrón es un excelente villano y me encantó que no fuera ese típico villano de siempre sino un villano carismatico, el Soundtrack es bellisimo y épico.
Lo malo:
Tiene algunos personajes desaprovechados que en los comics son muy importantes y que según Capitán América Y el Soldado del Invierno tendrian mucho que ver con esta película.
        
            