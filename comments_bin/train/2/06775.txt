
                      Es una buena película. Diferente y con un mensaje positivo sobre la vida, el amor y el esfuerzo como fuente de felicidad. A pesar de ser algo comercial y ligeramente superficial se sale de los clichés tan manidos en un gran numero de producciones americanas. Hace falta más cine como este que cuente historias de personas y menos cine vacío para pasar el tiempo. Esta película consigue dejarnos buen sabor de boca.
        
            