
                      Esta película trata de un osezno huérfano que encuentra en su camino a un macho ya adulto herido en el hombro. Perseguidos por cazadores, deberán encontrar una salida.
Lo curioso de esta cinta de culto, que gustó mucho en Europa, no fue así en Estados Unidos, es que los protagonistas son animales de carne y hueso, casi sin humanos, y poco diálogo, traza una historia conmovedora en medio del sobrecogedor paisaje de Alaska. 
Así, la caza aparece desde el otro punto de vista, el animal perseguido, para hacer una denuncia pública de hasta dónde llega el ser humano en su afán destructivo.
El guión está basado en el libro del escritor conservacionista, James Oliver Curwood, que, lejos de exponer un producto cansino, ofrece una historia preciosa sobre amistad y supervivencia.
En este caso, es una película interesante de ver, por los paisajes de infinita belleza, de la ternura e ingenuidad del osezno y la impresionante presencia del oso adulto.
Los humanos, desgraciadamente no salimos bien parados como conjunto, pero los tres personajes que se atreven a darle réplica a los osos, Tcheky Karyo, Jack Wallace y André Lacombe, bordan su papel, hasta el punto de creerte de verdad que su verdadera profesión sea la de cazadores.
Es una película que no insulta al mundo de la caza, sólo expone una batida con presa y cazador en una lucha por ambas partes por sobrevivir en un entorno hostil, como es, en realidad la naturaleza salvaje.
        
            