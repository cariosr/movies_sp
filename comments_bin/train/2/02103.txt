
                      Batman Begins quiere que la audiencia entienda a Bruce Wayne incluso antes de que se ponga el disfraz, y como Bruce Wayne y Batman, Christian Bale es extraordinario. La timidez y la alegría ya no son el nombre del juego. Bale personifica el miedo, la fuerza y ​​el ingenio de Bruce Wayne de una manera que ningún otro actor tiene. Es una caracterización que no solo es efectiva, sino que también agrega una tercera dimensión a una serie tradicionalmente bidimensional.

Nos presentan a Wayne durante sus viajes alrededor del mundo. Aparentemente, él está buscando una manera de combatir el mal que se llevó la vida de sus padres. En Asia, conoce a un mentor llamado Ducard (Liam Neeson), que entrena rigurosamente a Wayne para que sea un luchador que se aproveche de los temores de sus enemigos. En la tradición de las películas de acción de Hong Kong, las secuencias de entrenamiento preparan muy bien a la audiencia para la transformación de Wayne en Batman. 

Después de esta preparación mental y física, Wayne regresa a Gotham City y comienza a planificar la mejor manera de combatir el mal y la corrupción que ha invadido la ciudad. Con cuidado, crea una persona diseñada para llevar la esperanza a los inocentes y el miedo a los villanos.

Como Batman, Wayne se enfrenta a los mafiosos que controlan la ciudad. Las cosas se calientan cuando Batman descubre a un psiquiatra pícaro que usa alucinógenos para asustar a sus víctimas, lo que lo lleva a descubrir un plan siniestro y mortal que tiene horribles ramificaciones para toda la ciudad.
        
            