
                      De las mejores películas de toda la historia. Una historia biográfica magnífica y con excelencias actores. De esas películas que hay que verlas si o si antes de morir. La vi por primera vez principios del 2000 cuando era pequeño y jamás me olvido de esta excelente película de Tom Hanks, actor que hacía excelentes películas en su época. Y con una de las mejores frases de todos los tiempos del cine. 

Mamá dice que la vida es como una caja de bombones, nunca sabes lo que te va a tocar.
        
            