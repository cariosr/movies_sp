
                      Un contenido muy pero que muy potente, sobre un tema muy pero que muy duro, visto desde un punto de vista periodístico progresivo, y aunque cuente con un reparto de altos vuelos, le falta una pizca de chispa que no aparecerá hasta bien entrados en materia.
        
            