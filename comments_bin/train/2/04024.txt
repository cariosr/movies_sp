
                      Reconociéndome seguidora adepta de la saga, intentaré ser lo mas objetiva posible en mi opinion personal/critica.
Buena pelicula si nos referimos a su fiel adaptacion del libro.
Muy grandes interpretaciones de sus protagonistas, fieles a los personajes acompañados de grandes efectos y de un guión algo más maduro, de más cultivados diálogos, ya que todos nuestros personajes crecen al son de la saga.
Aun reconociendo que no puedo "criticarla" de corazón de un modo negativo, añadiría que...en ocasiones resulta lenta y el espectador se desengancha de la trama.
Aún mejor en esta pelicula la caracterizacion de los personajes y sumar que la interpretación de un personaje de la fuerza de Victoria en este film, digamos que brilla por su ausencia no llega al espectador. Y eso que han cambiado a la actriz y todo.
        
            