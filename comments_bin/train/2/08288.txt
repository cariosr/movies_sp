
                      La película Alamar, dirigido por Pedro González-Rubio,  expuesta la vida de un niño y su hijo. El nino, Natan Machado Palombini, viaja a su padre en un isla en México y pasa tiempo hacer muchas actividades. Los actores en la película no estan actores. Ellos son personajes reales, y los telespectadores reciban una experiencia personal con un padre y sus hijo. Esta es muy conmovedor y con el paisaje, crea una atmósfera de serenidad. Alamar es una película magnífica para es paisaje y experiencia personal de un niño y su padre.
        
            