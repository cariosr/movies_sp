
                      Me ha encantado ver esta película. Es de las que van directas al corazón. Una preciosa historia real sobre la realización de la universal Mary Poppins. Es un juego constante con tus sentimientos hasta que consigue que te entregues absolutamente y sin condiciones. Excepcional interpretación de Emma Thompson. Muy recomendable.
        
            