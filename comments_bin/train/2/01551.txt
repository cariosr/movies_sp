
                      La cinta nos regala un viaje personal repleto de un imaginario particular y completado con unas imágenes y una música fascinantes y evocadoras. Una estética que se basta con el blanco y negro con la intención de destacar la profundidad y la intensidad del desconsuelo interior del protagonista.
Una magnífica odisea, una especie de quimera personal que nos transporta hasta lo más recóndito del alma humana en un viaje por el río de la vida y que consigue mostrar una visión crítica y nada complaciente de nuestra relación con el mundo indígena. (7,5/10)
        
            