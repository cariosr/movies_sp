
                      Es una de las mejores obras americanas contemporáneas, la película es una adaptación maravillosa, y definitivamente el reparto es inmejorable. Un peliculón, esa es mi opinión, se la recomiendo a todo el mundo. 
Y por cierto, quien se haya encargado de escribir la sinopsis... No ha visto la película... ¿De dónde se saca lo de "vacaciones familiares"?
        
            