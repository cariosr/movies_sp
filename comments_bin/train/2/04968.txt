
                      Biopic de Stan Laurel y Oliver Hardy centrado en los últimos años de la carrera de los afamados cómicos. Es un retrato de una ternura y una delicadeza conmovedora. El trabajo de Reilly y Coogan es absolutamente fantástico, y la fotografía te traslada a otra época de una forma totalmente convincente. Ver a "El Gordo y el Flaco" retratados así es un homenaje al cine y a otra época que inevitablemente se agotaba.
        
            