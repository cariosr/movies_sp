
                      Siempre había oído hablar de esta película, que si es muy bonita, que buena es, es diferente a las demás… y la verdad es que así es.
Por fin me decidí a verla, obviamente por DVD, aunque la echaran hace poco por la televisión (no me gusta ver películas ni series ya por el simple hecho de meterte tandas de anuncios de 7 minutos entre medias).
La primera sorpresa fue descubrir que Noa es el chico, siempre pensé que sería la chica :P. La segunda sorpresa fue el planteamiento de contar la historia y como poco a poco vas cogiendo cariño a los personajes y sintiéndote uno de ellos. 
No quiero destripar mucho de la película y como la sinopsis se puede conseguir en cualquier búsqueda por la red no diré nada. Pero todos hemos tenido un primer amor. Un amor que aunque pase el tiempo y estemos con una buena pareja actualmente, pienses que habrá sido de ese primer amor y que habría ocurrido si hubieras luchado mas por mantenerlo. Y de eso va la película en luchar por ese amor puro, esa persona que estarías dispuesto a conquistarla cada día para verla de nuevo sonreír otra vez.
Así que si quieres candilar a una chica, mi recomendación es proponerla ver “El diario de Noa” y hazte el duro, porque hay escenas que es imposible no soltar alguna lagrimita.
        
            