
                      Gran película de finales de los años 30.
Sorprendente para su época, ya que su argumento en rebeldía era propio de muuuchos años más tarde, seria una trama propia de los años 60 o incluso los 70.
No es una simple comedia, ya que posee un inteligente sentido del humor.
Cumple sobradamente con el cometido de entretener aunque opino sinceramente que va mucho más allá, roza lo magistral.
Buen cine catalogado, a día de hoy, como un clásico que cuenta, entre otras cualidades, con actores de la talla de Cary Grant (el cuál nos sorprende con unas increíbles cabriolas) y katherine Hepburn, sin duda, dos de los grandes de hoy y siempre.
El film nos traslada una moraleja más que interesante: se feliz sin mirar lo que posees, lo material. Eres lo que sientes y sueñas, no el cuanto posees y te pertenece. 
Una de esas películas que no podéis dejar de ver.
        
            