
                      Película exquisita, tanto en el trato de las imágenes, como en los sonidos y diálogos. Invita a reflexionar y cautiva en cada escena por la creatividad tan espectacular que muestran. Digna de cualquier espectador que se tercie, con independencia de encauzarse en el género infantil. Sorprende.
        
            