
                      Sin duda alguna la mejor película del mayor artista del cine. Gran película con mucho sentimiento de fondo, acción, humor y todo lo que te puedas imaginar. Una obra maestra del gran Clint Eastwood, en mi opinión es su mejor película.
        
            