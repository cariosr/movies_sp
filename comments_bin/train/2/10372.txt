
                      Absolutamente soberbia. Te deja quieto en la butaca durante dos horas y no te das cuenta. Planos increíbles, actores extraordinarios, efectos especiales impresionantes y música inolvidable, y todo ello para contar una historia tremendamente real. Me encantó y la recomiendo.
        
            