
                      Sweeney Todd es un musical de comedia negra dirigido por Tim Burton, director de Big Eyes, Mars Attacks!, Bitelchús y Sombras Tenebrosas.

Benjamin Barker (Johnny Depp) es un experto barbero al que les son arrebatadas su preciosa mujer y su hija por el malvado juez Turpin (Alan Rickman). Barker es desterrado, y tras quince años en el extranjero trabajando, vuelve a Londres para vengarse, con el sobrenombre de Sweeney Todd.

La banda sonora de Stephen Sondheim es una genialidad. Las canciones son lúgubres, a juego con los personajes y los escenarios. Además están interpretadas por voces tan portentosas como las de Johnny Depp, Helena Bonham Carter, Jamie Campbell Bower, e incluso Alan Rickman.

La historia está llena de suspense, y cuenta con uno de los finales más increíbles que se pueden ver. Completamente inesperado, deja al espectador sin aliento.
El maquillaje y el vestuario también son elementos dignos de destacar, al igual que la ambientación victoria que posee el filme.
En general, no hay nada del largometraje que se pueda destacar negativamente. Es una de las mejores obras de Tim Burton.
        
            