
                      Posee todos los ingredientes que la hacen más que merecedora de una buena tarde de cine con palomitas, bien vale y merece la entrada.
Entretenida, de fascinante montaje y efectos especiales que no te dejan parar en la butaca.
Bien montada y de actores estupendos que nos ofrecen buenas interpretaciones.
Grandes dosis de magia, aventuras y efectos visuales bien propios de Jerry Bruckheimer, hacen de ella una buena película que recuerda a los antiguos libros de aventuras que te mantenían en otro lugar, en otros parajes, durante el tiempo que duraba tu lectura.
Pelicula para no pensar y disfrutar. Siendo esto lo que pretende y consiguiéndolo, por eso le doy una puntuación tan alta.
        
            