
                      Como todas las de Gianni Amelio, una obra maestra de sensibilidad y agudeza. Indispensable en esta época en que el cine se basa en la comercialidad, en la falta de reflexión o en en la necesidad que sean films americanos.
        
            