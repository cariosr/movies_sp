
                      Increíble. Esa es la palabra que mejor define, a mi parecer, la primera película de Neill Blomkamp. No es sólo una película más sobre alienígenas. Es una película que representa la forma en la que trataríamos a los extraterrestres si alguna vez habitaran nuestro planeta. Es decir, con un desprecio total, y como si fueran apestados. Definitivamente, Neill es un genio, y si sigue en esta línea conseguirá hacerse un hueco en el mundo de Hollywood.
        
            