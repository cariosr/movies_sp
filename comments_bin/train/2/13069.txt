
                      La mejor adaptación de un super héroe en la gran pantalla. Ritmo trepidante, personajes nuevos que encajan a la perfección, argumento que enlaza de manera magistral con las dos entregas anteriores y un villano que cumple 
aunque joker es insuperable. Peliculón que nadie se debería perder.
        
            