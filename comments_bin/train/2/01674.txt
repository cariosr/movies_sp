
                      Una segunda parte, en el regreso de John Wick, forzado a tomar un oficio imposible de dejar, en una jubiliación imposible. Y con una película con acción, elegancia, normas y un Keanu Reeves en buena forma nos permite entretener y pasar un buen rato dentro de este género cinematográfico. Si hay una película con segunda parte válida para ver, una de ellas, es la presente, la cual tiene por malo tener que esperar para ver la tercera entrega.
        
            