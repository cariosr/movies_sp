
                      Otra obra maestra de Tarantino, venganza, sangre, visceras, katanas y escenas que ya quedarán para la eternidad del cine, regadas con la mejor banda sonora posible, otro toque de autor de el bueno de Tarantino. Uma Thurman esta impresionante, haciendo de su papel un icono para siempre.
        
            