
                      Una película de las imprescindibles de ver si eres español. Muestra, aunque de forma ficticia, a la perfección la situación que se vivió en Guinea Ecuatorial cuando los colonizaban los españoles. Tiene una trama muy bien definida, las elipsis temporales pasan desapercibidas, y la historia de amor te engancha. Recomendada 100%, y aunque Mario Casas no sea muy bueno, en Palmeras en la Nieve se lleva la palma. Muy bien.
        
            