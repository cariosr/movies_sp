
                      Impresionante relato de supervivencia y forma de ver la vida. Quizá se tendría que haber detenido un poco más en la "tercera parte" de la novela para entender bien toda la forma de ser del personaje. 
Película ideal para ver en pantalla grande y, a ser posible, en 3D. Merece la pena.
        
            