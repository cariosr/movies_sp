
                      Gracias ha sensacine, pude disfrutar del preestreno de Spiderman.

La película es acción pura, incluso cuando hablan se te hace pesado por el ritmo de la película.

Es una gran introducción a la saga que viene por delante (2 películas más).

El actor es fantástico, pega perfectamente en el papel de Spiderman, no está demasiado "cuadrado", y es inteligente (a veces demasiado, que resulta absurdo) y si sabes un poco de ciencias, te das cuenta de detalles absurdos en el contexto en el que se realizan.
        
            