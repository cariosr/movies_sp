
                      En las antípodas de su imagen de galán, un convincente Mario Casas da vida a este hombre tosco, seco, asilvestrado. Oímos con claridad su jadeante respirar cuando camina por la nieve arrastrando un trineo lleno de pieles: escuchamos el ruido de la carne que le sirve de comida arrancada  con sus dientes; su masticar ansioso. Especialmente destacable su interpretación en la parte final de la película, cuando afloran unos sentimientos inesperados para él, chocando violentamente contra su naturaleza salvaje.
Leer reseña completa en:
elcinepormontera
        
            