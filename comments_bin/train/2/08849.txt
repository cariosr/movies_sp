
                      Muy divertida, cumple a la perfección su objetivo que no es otro que hacerte pasar un buen rato. 
Buena comedia puesto que te hace reir desde el minuto uno.
No cabe comparación con Ocho Apellidos Vascos puesto que son completamente diferentes, aunque su esencia es la misma: los cuatro protagonistas principales (que  lo vuelven a bordar). En este caso suman además tres nuevos personajes que también hacen un buen trabajo, sobretodo Belen Cuesta como Judith. 
Desde mi punto de vista ha conseguido como mínimo igualar a su predecesora, lo cual era un gran reto.
Los actores de 10.
Por último,  el final parece muy alocado al inicio de su escena, pero es de lo mejor de la película, prueba de ello es que consiguió que absolutamente toda la sala se riera a carcajadas, y considero que no puede haber mejor broche final para una comedia.
        
            