
                      Formato: IMAX 3D

En una palabra: ÉPICA

En mi experiencia: La mejor película de superhéroes, tanto que no soy fan y me quedaron ganas de entrarle a todo!!

Lo que no me gusto: El capitán América pierde su esencia así como protagismo 
                                  El momento ridículo cuando llora Spider Man, de por si ese personaje no lo trago...
                                  La pelea en Wakanka estuvo un poco ilógica
                                  Faltó explotar el carisma de black widow 
                                  
Lo que no entiendo: Desconozco la historia de Vision pero me parece un estorbo
                                 Mencionan a Ant Man pero nunca aparece, me imagino que los están guardando   
                                 A dónde se fue Thanos cuando desapareció a la mitad de la vida en el universo?
                                 Como fueron seleccionadas las almas que sobrevivieron
                       
Lo que me gusto: La psicología de Thanos 
                             La trama / historia
                             La pelea de Thanos VS Iron Man
                             Ironman es genial
                             Thor es literal "de otro mundo"
                             El guión es muy bueno
                             Cuando forjan el hacha es un buen momento aun así siento que lo resuelven muy sacado de la manga
                             Efectos visuales increíbles

Opinión general: Es una película llena de acción, con un sentido del humor bastante cuidado y bien colocado, en general cada personaje muestra sus cualidades en los momentos indicados llenando cada espacio de la trama dándole al guión una secuencia bien articulada, los efectos visuales junto con la música que de pronto remontaba a escenas de Star Wars resaltan emociones, la sensación que percibí, es de que no quieres que nadie gane pues la cadencia es demasiado buena y disfrutable, sin embargo hay un punto donde la película cansa  y es ahí donde provoca un desinterés, pero justo es cuando inicia un desenlace que vuela la mente y hace pensar y reflexionar un poco a aquellos que se atreven, el final deja satisfecho al mismo tiempo que provoca la expectativa de la segunda parte. En general es una película bien construida que demuestra lo lejos que puede llegar Hollywood y la tecnología en el ámbito del cine comercial, nada mal para una película de $ 300 000 de dólares
        
            