
                      Una de las mejores películas de terror que vi, produce en el espectador un viaje a lo más macabro, perverso y oscuro de la mente humana, con escenas de auténtica violencia. El director Tobe Hooper crea una obra esencial para todos los amantes del cine de terror. Las interpretaciones son una pasada, ya que para la mayoría era su primera película. La banda sonora esta a la altura del film.
        
            