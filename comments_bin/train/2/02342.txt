
                      Acertada decisión de Gustavo Taretto al filmar este largo a partir del corto del mismo nombre que realizara en 2004, pues el tema y la puesta en escena bien lo merecía.
Desde la narrativa inicial acompañando a una especie de pase de diapositivas sobre la singularidad y disparidad de los edificios en Buenos Aires se nos lleva a una historia de vidas paralelas y soledades muy presentes en la sociedad actual por el ritmo de vida y la dependencia de las tecnologías a las que nos vemos ligados, olvidando en muchos casos salir a tomar el aire o hasta sentir temor ante esa sensación.
Historia perfectamente desarrollada y enlazada hasta llevarla a su punto final, que si bien previsible para nada deja de sorprendernos.
Interpretaciones bastante interesantes y situaciones divertidas con elementos curiosos que dejan un buen sabor de boca, amén de reflexionar sobre el mundo al que nos empuja el comportamiento actual.
        
            