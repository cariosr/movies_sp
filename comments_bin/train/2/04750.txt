
                      Película de las que hacen a uno reflexionar. También muestra que aún el los días más terribles del siglo XX hubo héroes mudos, humanidad, ternura... etc. Totalmente recomendable. Yo que no había leído el libro, me parece una genialidad de la narración en "voz en off" de la Muerte. Por cierto, no me extrañaría unas cuantas nominaciones a los Oscar.
        
            