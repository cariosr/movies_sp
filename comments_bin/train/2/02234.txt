
                      Cine con el sello de Clint Eastwood, con un Tom Hanks que siempre que se le saca partido está de lo más convincente. Entretenido retrato del accidente aéreo de Enero de 2009 en el Río Hudson (Nueva York), en el que el peso recae sobre el comandante que amerizó el Airbus con 155 tripulantes. Entretenida, creíble, contenida, muy al estilo de su director
        
            