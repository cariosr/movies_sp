
                      Tras 'En tierra hostil' (The Hurt Locker, 2008), Kathryn Bigelow parece haberle cogido el gustillo, y la medida, a los thrillers bélicos ambientados en Oriente Medio y vuelve con 'La noche más oscura' (Zero Dark Thirty, 2012), un film que abarca más de una década y en la que nos narra la historia de la búsqueda y captura de Bin Laden a través de Maya, la agente de la CIA interpretada por Jessica Chastain (Globo de Oro a Mejor Actriz de drama y nominada al Oscar por este papel) que consigue dar con el paradero del hombre más buscado del Mundo.

Sin duda Bigelow lo ha vuelto a conseguir, aunque se hayan olvidado de ella y de su película en los Oscars, estamos ante un thriller muy bien hecho y muy bien llevado, que sabe manejar a la perfección la tensión y las tramas. Seguramente le ha podido perjudicar, la que prácticamente ha sido la película del año, 'Argo', ya que guarda con ella varios puntos de conexión: es un thriller, ambientado en Oriente Medio, basado en hechos reales, en los que más o menos conocemos los hechos, o los podemos llegar a conocer, y que a pesar de ello saben manejar la tensión de una forma impecable con una muy buena factura. Aunque no nos dejemos engañar, si bien 'Argo' le ha podido hacer sombra, es una de las grandes películas del año.

Hay cuatro puntos que quiero destacar del film:

- La mano de Kathryn Bigelow: Sabe a la perfección lo que tiene y sabe darle el punto adecuado a todo el metraje, siendo excepcional la parte final en la cual se realiza la preparación y captura de Bin Laden (todo aficionado a CoD, MoH o Battlefield quedará totalmente alucinado y verá sus sueños hechos realidad). No puedo entender como no ha estado nominada a mejor dirección en los Oscars. Muy bien Bigelow.

- El guión firmado por Mark Boal: guionista también de 'En tierra hostil', lleva a cabo un guión muy bien documentado que nos acerca con bastante verosimilitud a los hechos reales, sabe hacer a la perfección el recorrido de la búsqueda de Bin Laden a lo largo de esos más de diez años, que como podréis imaginar no tiene que ser fácil de condensar por mucho que sean 3 horas de metraje, resulta ser muy consistente y no pierde al espectador entre las tramas ni por la historia.

- El montaje: es una auténtica maravilla, junto al de 'Argo' (ganador del Oscar a Mejor Montaje), los dos mejores del año. ¿Alguien lo duda?.

- Jessica Chastain: Está perfecta interpretando a Maya, muy sólida sirviendo de vínculo emocional, reflejando la cosmogonía y evolución del personaje, su obsesión por encontrar a Bin Laden. No es de extrañar que fuera la gran favorita para el Oscar a Mejor Actriz.

Para terminar, no quiero entrar en las polémicas que ha generado a nivel político ésta película, pero con respecto a la de la justificación de las torturas quiero decir dos cosas:

1º La película en ningún momento se posiciona, ni justifica la tortura, al menos desde mi punto de vista.

2º No seamos ingenuos, cualquier centro de inteligencia o ejercito de cualquier país del Mundo, lleva a cabo este tipo de prácticas, otra cosa es la opinión que tengamos sobre ellas.

Muy recomendable para TODO el mundo, es una gran película, hay que verla.
        
            