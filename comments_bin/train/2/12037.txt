
                      Las cualidades de esta película son muchas, entre ellas el hecho de presentar los orígenes de la estrategia del choque, empezando por Chile, el papel de los Chicago Boys, que yo no ni siquiera conocía, etc. Esta película atacó a Reagan, Thatcher, Bush (obviamente) ... : Sólo por eso, diría que vale la pena verla! Sin duda una verdadera oda a la resistencia!
        
            