
                      La quintaesencia de la vida
Decía uno de los más célebres fotógrafos y periodistas de la revista Life, Alfred Eisenstaedt, que lo importante de una fotografía no es la cámara, sino el ojo. En un momento de “La vida secreta de Walter Mitty”, el fotógrafo interpretado por Sean Penn –que, casualidad, trabaja para esa misma revista-, acecha con su objetivo a un guepardo de las nieves en lo alto del Himalaya. Aguarda, sigiloso, pero nunca llega a tomar una fotografía. No es necesario, pues ese momento, ese instante que no muchos pueden decir que tienen la suerte de haber vivido, estará vivo en su memoria.

No es casualidad que en su nuevo trabajo Ben Stiller haya escogido la desaparición de un negativo como pretexto para lanzar a ese soñador despierto llamado Walter a la mayor aventura de su vida. La vida de todos nosotros puede ser contada a través de una secuencia de instantáneas, de recuerdos sostenidos en la memoria. Pero la vida de Walter es tan monótona que su álbum está vacío, y eso le obliga a echar mano de su imaginación para verse a sí mismo como una persona aventurera y creativa.

Podría decirse que “La vida secreta de Walter Mitty” es como la “Forrest Gump” de nuestros días, aunque sin el carisma y simpatía innatos de su protagonista ni esa magia que desprende el cine de Robert Zemeckis. En el fondo, en las dos subyace una idea mucho más potente que la de la superación y la definición personales: la del amor como chispa de la vida, como el detonante de los mayores sacrificios.

Pero aunque esto no sea “Forrest Gump”, no se puede negar que sea una película repleta de alma. Stiller pone todo de su parte en la realización, tanto en la parte real –el esplendor con que fotografía los parajes de Islandia y Groenlandia es de ensueño; la banda sonora con temas de José González, David Bowie, Of Monsters and Men y Arcade Fire, entre otros, invita a volar con su protagonista; las apariciones de Kristen Wiig, Penn y Shirley MacLaine son bienvenidas- como en la onírica –aquí hay momentos para el recuerdo, como la súper pelea por el muñeco elástico-, y lo que resulta es un canto a la vida, a la lucha por los sueños personales, a no limitarse simplemente a soñarlos, sino a vivirlos. Desprende humanidad en cada fotograma, algo muy alejado de sus trabajos más famosos.

Un film extremadamente positivo que tiene ese puntillo de inverosimilitud que también desprendía la cinta de Zemeckis en su momento. Pero mientras en aquella la casi imposible vida de su personaje transcurría con la simpleza y naturalidad de quien ve lo que le rodea con ojos inocentes, en la que nos ocupa es bastante más complicado entrar en su juego una vez Walter decide abandonarlo todo y lanzarse a su particular odisea. Superado este -para algunos insalvable- obstáculo, lo que queda es la búsqueda de esa instantánea que resuma la quintaesencia de la vida.

A favor: la música, la fotografía, y que invita a soñar y a vivir los sueños

En contra: cierto puntillo de inverosimilitud que puede hacer que más de uno desconecte
        
            