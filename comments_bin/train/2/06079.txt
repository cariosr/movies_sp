
                      El cómic "Mujer Maravilla" fue creado por William Moulton Marston en 1941, un año improbable para una superheroína. La Segunda Guerra Mundial estaba en pleno apogeo, y en el mundo de DC Comics, Batman y Superman reinaban supremas, y luego ella vino, peleando con "amor, en lugar de odio", según Moulton Marston.

Imagine qué tan nuevo y refrescante debe haber parecido todo el concepto a las mujeres estadounidenses. Tomaría otros 20 años para que el movimiento feminista comenzara, y otros 35 antes de ver a la actriz Lynda Carter interpretar a Wonder Woman en la televisión.

Más de 75 años después, "Wonder Woman", la película ha llegado a nuestras costas. Aunque ha habido muchas adaptaciones, esta, protagonizada por Gal Gadot y dirigida por Patty Jenkins, captura fielmente el espíritu del cómic original, no necesariamente porque ella pelea "con amor", sino porque no necesita una excusa para ella. propia magnificencia. A diferencia de sus compañeros camaradas varones en los rankings de DC Comics, Wonder Woman no tiene un chip masivo en su hombro o un pasado traumático. Ella hace lo que hace por un feroz sentido de la justicia. Como dice la canción, ella nació de esta manera.
        
            