
                      Teniendo en cuenta que la gente de mi generación se perdió ABBA... es un gran descubrimiento. Las letras de las canciones asi como el reparto y la escenografía son sublimes, podemos ver a una Meryl Streep moviendose más que una jovencita y la película logra hacer reir y llorar, al menos a una servidora. La recomiendo.
        
            