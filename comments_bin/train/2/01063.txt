
                      A pesar de la mala fama, me ha gustado. Es una película que va a gustar mucho fuera de España porque juega de nuevo con estereotipo español: transgresor, salvaje, sexual y un poco desfazado. Es cierto que, en este caso, la historia es muy pobre pero técnicamente es la primera película de Almodóvar que es completa y que no le sobran historias ni gags paralelos incongruentes (a pesar de lo mucho que nos gustan algunos y lo que nos hacen reír). El problema radica sólo en que es una historia que no daba mucho de sí (a pesar de su simbolismo de nuestra realidad), porque sinceramente a nivel técnico puro es su mejor película hasta ahora... En resumen, vale la pena verla y e
speremos que para la siguiente tenga una mejor historia que contar...
        
            