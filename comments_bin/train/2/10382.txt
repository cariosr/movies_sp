
                      La he visto y puedo decir que no es una obra maestra del cine, con poco presupuesto es dificil...pero la fotografia es brillante y el mensaje da un poco de luz en este mundo en el que cada dia somos todos mas egoistas y poco humanos. No impone ninguna moral, pero hace ver la vida desde otra perspectiva...No es cuestion de religion o moral es cuestion de humanidad y esta pelicula lo muestra!!
        
            