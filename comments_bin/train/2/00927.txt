
                      Es fantástica. Esa trama de romance y ciencia ficción, junto con algo de misterio y suspense al mismo tiempo, le da un toque... mágico. 
Es algo original, que nunca antes había visto.
Los actores magníficos, hacen su papel muy bien, y eso hace que la película tenga, como es obvio, más sentido, digamos. 
