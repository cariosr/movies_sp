
                      A día de hoy, sigue siendo la película más destacada de la saga, y la película que marcó mi infancia (y creo que la de otros muchos). Sus efectos especiales, sus personajes, la ambientación, la música, es perfecta en todo. Si pudiera la olvidaría y la vería de nuevo para repetir la experiencia del primer impacto otra vez. Creo que, después de tantos años que han pasado, no merece la pena a estas alturas comentar la trama porque es bien sabida por todos.

Lo Mejor: Todo, o casi, es la película perfecta de Jurassic Park

Lo Peor: No ser fiel al 100%, paleontológicamente hablando, al aspecto original de dos especies.
