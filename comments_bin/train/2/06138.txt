
                      The Amazing Spider-Man 2 es una película excelente que adapta correctamente la historia de los personajes involucrados en ésta.

Las películas que son adaptadas de los comics, no tienen que ser fieles al 100% a la obra original; son diferentes expresiones artísticas con diferentes reglas. Por eso la modernización de los personajes de el Hombre Araña con esta nueva entrega a cargo de Webb, supera a su predecesora y se pone a la par junto a Spider-Man 2 de Sam Raimi.

Peter Parker tiene que afrontar la realidad y madurar, y encaja el camino de Disney con sus películas para todo público; en la lucha contra el mal, el heroe logra encontrar cierto equilibrio pese a las perdidas, y a quien le gusten las películas con valores, es lo que Siderman representa en más de 2 horas entretenidas.

Aunque es un poco contradictorio, ya que Parker asumirá que su lucha es quijotezca, ya que tiene que enfrentarse a los dominios del poder de la gran corporación llamada Oscorp, en beneficio de otra megacorporación llamada Sony.

En fin, si tienes un criterio abierto, y tienes la capacidad de digerir golpes y efectos especiales en lenguaje infanto-juvenil, podras pasar un buen momento viendo ésta película.

Y si te dejas llevar podras llenarte de la esperanza que transmite el hombre araña, en un mundo complicado como el nuestro.
        
            