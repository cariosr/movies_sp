
                      La crítica a una película debe hacerse con miras al género al que pertenece o al objetivo al que aspira, y en este sentido Crudo resulta ser una ejecución ejemplar. Máxima expresión de lo siniestro, la película emplea muy cuidadosamente la técnica cinematográfica para intoxicar al espectador con el desasosiego. Montaje audiovisual, banda sonora, fotografía, maquillaje y puesta en escena son articuladas entre sí para transmitir con absoluta eficacia las sensaciones que encarnan perfectamente sus actrices. Con ello se abandona al espectador en algún lugar entre la angustia y el horror, donde la exquisita configuración de los elementos sublima en un conflicto: «no puedo seguir mirando, pero no quiero dejar de hacerlo».

No soy fan del cine de horror ni del gore, pero creo que para admirar esta película basta con tener un ojo y un oído. No cabe duda: hablo de la auténtica SEDUCCIÓN SATÁNICA.
        
            