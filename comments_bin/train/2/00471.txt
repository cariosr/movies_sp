
                      Hay películas que son sencillamente un retrato. Una historia que acontece ante nuestros ojos. La cámara suspende el juicio de un modo tal que logra evitar tanto la subjetividad como el distanciamiento. Consigue acercarse a las contradicciones estos personajes sin llegar a explicarlos más de la cuenta. El punto justo.
        
            