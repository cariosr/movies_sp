
                      La comedia francesa está al alza en el mundo cinematográfico. A las ya conocidas, 'Salir del armario', 'La cena de los idiotas' o 'Bienvenidos al norte', se une esta película que, en mi opinión, supera a las mencionadas anteriormente. Estamos ante una película que gusta de verse, divertida, alegre, tierna y que realza el valor de la amistad. Además mezcla la ternura con dosis de humor inteligente, y algún gag no tan inteligente, en lo que sin duda será la comedia francesa del año. 

Si lo que buscas de una película es que te enganche a la historia y se te haga amena con su humor y algunos momentos de gran vitalidad, al estilo 'Pequeña Miss Sunshine', esta es sin duda tu película.
        
            