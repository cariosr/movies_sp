
                      Creo que no me equivoco si digo que es la mejor película de extraterrestres que he visto.
Al menos se trata el tema desde un punto de vista adulto y maduro, nada de guerras nucleares para demostrar cuánto podemos devastar y que somos mejores que ellos.

Un punto de vista tan humano que te hace plantearte muchas cosas.

Amy Adams genial como siempre. La película creo que está muy bien montada. Los sonidos y la música perfectos.
        
            