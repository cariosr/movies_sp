
                      Gamberra, ácida, negra, divertida, ingeniosa, explosiva, con un punto tróspido y unas cuantas cosas más...

Película llena de detalles de alguien que disfruta, vive y respeta el cine.

Recomendable y a ratos tronchante. Me corté a la hora de aplaudir al final, pero no sería por ganas. Ojo. A ver si alguien se va a creer que es la mayor Obra Maestra de todos los tiempos. No es eso. No quiero levantar expectativas tan altas como el Monte Olimpo de Marte y que luego te aplanes hasta llegar al fondo de la fosa de las Marianas.

Personalmente, hacía tiempo que no veía una película donde no tienes ni repajolera idea de lo que sucederá en la escena siguiente. Vigalondo es un crack.

El director te lleva por donde quiere. Juega al despiste como un viejo tahúr y no sabes si la carta que estás viendo es el as, un rey o el loco e impredecible comodín.
Francamente, no me considero una persona con un cociente intelectual elevado, pero no he visto una sola fisura en el guión. Cada uno de los personajes tiene un trasfondo rico y profundo. Se podría hacer una película con cada uno de ellos (incluida la regidora del estudio de TV).

Me encantaron dos planos (personalmente me resultaron simpático e inquietante respectivamente). Las varillas suecas sacaojos y su inutilidad (y de paso tumbamos la estantería). Nacho se ríe del icono cotidiano de una generación de españoles y les hace un guiño con los productos de una multinacional sueca cuyo logo ovoide (¡coño, un platillo volante) combina amarillo y azul.

El segundo plano es un picado ligero del rostro de Villagrán iluminado con un corte de luz justo en la mitad de la cara, en medio de una noche cruda, peligrosa y paranoica. Detrás de él, en el negro y profundo cielo, una enorme nave espacial de otro mundo nos observa. La cara de Villagrán le dice al espectador «Yo soy el alienígena ¿lo ves?».

Le he puesto un Ocho. Comedia con punto tróspido. Funciona y parece que es sencilla y facilona, pero cualquiera que tenga un poco de criterio, cultura y espíritu crítico sabe que no lo es.

Atentos a los diálogos. Algunos cortan como cuchillas. Otros juegan con los malentendidos y los sobreentendidos. Conversaciones cruzadas que tenemos todos los días. Diálogos automáticos que mantenemos con quien no nos interesa hablar, errores fatales de interpretación que cometemos por no prestar atención…

Una película genial. La ví ayer. Me sigo acordando de ella. Definitivamente Extraterrestre de Nacho Vigalondo no es de esa clase de películas. Esa cuya categoría reza: pagar, ver y al salir del cine, olvidar.
        
            