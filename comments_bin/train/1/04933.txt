
                      La quinta entrega de la franquicia Saw no desperdicia el tiempo para llegar al aspecto de tortura porno por el que se conoce a la serie. La escena inicial ve a un hombre musculoso y con el torso desnudo, un asesino despiadado liberado de la prisión por un tecnicismo, que encuentra justicia a través de la vieja rutina de pajaros y péndulos.

La atracción añadida aquí es que se ofrece una salida al convicto imparable. todo lo que tiene que hacer es colocar sus dos manoplas asesinas en dos visas cercanas y aplastarlas para pegarlas. Desafortunadamente para este tipo, y afortunadamente para el contingente de gorehound, el que duda es un brindis.

Las manos humanas se ofrecen para la mutilación de nuevo más adelante en una secuencia de tortura que compite con el factor de retorcimiento en tu asiento de la infame escena del globo del ojo al ojo del albergue, ¡y esta vez el dolor es autoinfligido!

Entre estos horripilantes juegos de piezas se ejecuta una trama complicada que utiliza flashbacks revisionistas para mostrar cómo los sádicos juegos de trampas de la muerte del genio malvado Jigsaw (Tobin Bell, cuyo personaje murió hace dos secuelas) son llevados a cabo por el detective Hoffman (Costas Mandylor) mientras persigue por otro sobreviviente de Saw IV, el agente del FBI Strahm (Scott Patterson, cuyo mejor desempeño consiste en perforar su propia tráquea).
        
            