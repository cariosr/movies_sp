
                      Hace casi treinta años, que Steven Spielberg rodó "E.T. El extraterrestre" con un enorme éxito comercial, y una "moraleja" con la que muy  pocos se quedaron. A saber, alienígena con poderes extrasensoriales dejado por otros de los suyos que "queda atrapado en nuestro planeta"  sin poder comunicarse, y que establece una especie de conexión con un chico.

En esta película, la historia se repite y a pesar de que Abrams logra crear un clima de excelente suspense e intriga, la resolución de la historia es más que pobre,y a años luz de ET,  ya que la criatura no sorprende ni asusta y se la ve poco y mal, posiblemente porque el realizado habrá creído  que con eso aumentaría el misterio. Los mensajes moralistas también están a la orden del día y resultan monótonos y aburridos, ya que mientras en ET se lograba "creer en la esperanza",  en Super 8 se nos intenta convencer con prepotencia y malos diálogos de que hay que seguir adelante y perdonar todo sin importar cuan grave sea la falta cometida.

O sea que no me ha gustado nada y es que "las comparaciones son odiosas".
        
            