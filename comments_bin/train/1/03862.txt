
                      Malísima. Cuando vas al cine sabiendo que una película es mala, te parece hasta menos mala. La decepción llega cuando vas a ver una película con unas expectativas muy altas por toda la promoción que hay detrás: dos buenos actores, un buen director y encima te la venden con los premios ya casi ganados. Te esperas una película fantástica, pagas sin problemas porque sabes que vas a ver una de las películas más maravillosas de de este año y te encuentras con...: un argumento habitual de las películas americanas, una película sin movimiento ni emoción y lo peor, una película en la que ya sabes incluso lo que va a salir por la boca de los personajes. 

Aburrida, predecible y decepcionante. Elegiría pagar por otra película sin lugar a dudas.
        
            