
                      Stardust cuenta cómo Tristán (Charlie Cox), un joven tan atractivo como tosco, promete a su indiferente amada Victoria (Sienna Miller) traerle una estrella caída del cielo. Para ello Tristán se atreverá a abandonar su asfixiante pueblecito, Muro, y a emprender un viaje iniciático a un universo paralelo al nuestro, Stormhold, en el que la estrella resultará ser una chica preciosa, Yvaine (Claire Danes), cuyo corazón es codiciado, debido a sus enormes poderes, por varios príncipes herederos, unas brujas y un mercader.
Tengo que decir que entré bastante escéptica a la película y salí encantada con el resultado final.
Es una cinta que, sin ser una obra maestra, es divertida, amena y tiene puntos de humor muy graciosos. Recomiendo, para el que pueda, verla en versión original, ya que desgraciadamente, el doblaje no le hace justicia.
Es una película que pueden ir a ver los niños, con brujas, reyes curiosos, estrellas fugaces, piratas y un mundo paralelo lleno de personajes interesantes.
Desarrollada en un tiempo sin especificar, tanto la ambientación como el vestuario son excelentes, el guión se sucede con rapidez, efectivamente, es un cuento de hadas con una vuelta de tuerca fascinante, Michelle Pfeiffer junto a Robert de Niro o Matthew Vaughn hacen sus papeles de atrezzo acompañando magistralmente a los personajes principales.
Efectos especiales bien conseguidos y trama muy bien hilada, la historia engancha desde el principio.
Aunque la actuación de Tristan durante los primeros minutos es un poco sobrecargada, a medida que se desarrolla la acción va encontrando su puesto en esta historia, me gustó especialmente Sienna Miller, en su papel de niña caprichosa que juega con unos y otros, pero la naturaleza es sabia y al final cada uno desvela su propia cara...
En definitiva, divertida, diferente, amena, sin importunar a nadie, sin ser una película al uso, bien hecha y sobre todo, que no importa ver varias veces.  
Disfruté como una niña con este cuento con mala baba inglesa, brujas malas malas, reyes regicidas por un reino, caballeros y humor a raudales.
 Se ve que De Niro se lo pasa en grande interpretando a un rudo pirata con un secretillo...y el prólogo con el guarda del muro (y su evolución) es divertidísimo y nada, insisto, nada, hace chirriar la bisagra perfectamente engrasada de esta grandiosa epopeya.
        
            