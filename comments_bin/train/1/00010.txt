
                      Una secuela de el Tiempo entre Costuras con una historia similar que como miniserie de televisión puede que funcione, pero en el cine no.
La sensación desde el principio es de no estar viendo cine sino una telenovela de sobremesa, imagínense mezclar actores, actuaciones y sobreactuaciones de Al Salir de Clase y de La Señora, todo eso durante tres horas y en pantalla extra large. 

La historia no es mala, el problema es que es imposible meterse, una vez más el lenguaje televisivo con unos decorados demasiado obvios -por ejemplo el ambiente africano del supuesto puerto de Santa Isabel cuando llegan por primera vez- te impiden creértelo. 
Por otra parte que siempre -sin excepción- nieve en el pirineo oscense también es un poco forzado; sí el mensaje se entiende, que Kilian viene del Pirineo, pero es que las imágenes son más bien del círculo polar ártico, un poquito de contención..

La guapísima Berta Vázquez de origen etíope es de lo más salvable de la peli, aunque una vez más te lleva a pasar de puntillas por el extraño acento no guineano de los habitantes de esa Guinea Ecuatorial de cartón piedra.. -la película está rodada en el caribe colombiano y los figurantes son caribeños-. 

En fin, perfecta para cuando la pongan en Antena 3 como telefilm de sobremesa con mantita y vermut.
        
            