
                      Me esperaba otra cosa con tanta actriz de reconocido recorrido. No es una película de humor como la venden, sino una historia normal con alguna gracia.

Todo empieza con una madre hasta arriba de tareas con un mojón de marido que no la ayuda en nada, la madre (Mila Kunis) llega al limite de su paciencia después de averiguar que su marido le pone los cuernos por Internet durante 10 meses. Decide pasar el grupo de mujeres de la APA y se da el gustado de tener tiempo para ella misma, se le unen 2 mujeres (Kristen Bell y Kathryn Hahn) e intiman hasta que se hacen amigas. Luego se unen para destronar al grupo de mujeres de la APA y así pasa toda la película, de fiesta con algún gag gracioso e intentando Mila kunis el salvar su matrimonio mientras se divierte con sus amigas.

Es de las películas que e visto porque decía que era comedia y las actrices que salen las conozco de otras películas o series y me agradan, pero sinceramente la película me a parecido mala y aburrida.
        
            