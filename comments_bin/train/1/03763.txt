
                      Nieve, frío, violencia, muerte y mala gente desde el comienzo hasta el final, que pareciera no llegar nunca. Muy modestamente, no entiendo qué es lo que premian en esta película. Lo único magnífico que le encontré es la lucha con el oso. Increíble la técnica. Nada más.
        
            