
                      Que aberración, que despropósito, que mal gusto en todas sus formas. Una película dirigida a personas que estén hasta arriba de estupefacientes, porque otro sentido no la encuentro. Ni para niños ni para adultos. Un cumulo de frases mal sonantes que quizá alguna te pueda llegar a sacar una ligera sonrisa. No la recomiendo!
        
            