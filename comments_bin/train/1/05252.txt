
                      El último clavo en el ataúd es una grotesquerie completa, una película que es tan divertida como una estadía de 20 años en una prisión turca. Arnold Schwarzenegger (Mr. Freeze) y Uma Thurman (Poison Ivy) son los villanos olvidables, George Clooney es tan aburrido como Batman / Bruce Wayne, y Alicia Silverstone es una presencia molesta como Batgirl. El guión es incoherente e intrascendente, y he visto rayos X colónicos más atractivos visualmente que lo que Schumacher arroja en la pantalla.
        
            