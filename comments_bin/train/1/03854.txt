
                      Una palabra: Pésima.
Como fan de la saga de Assassin's Creed he de decir que esta película es puro humo.
Si bien es cierto que los últimos juegos no tenían una gran historia, por no decir ninguna, esta película es aún peor.
No hay narrativa alguna, te sueltan los personajes y la finalidad sin contar un origen ni un porqué.
La historia sobre el fruto del edén esta erróneamente contada a como es en los juegos, se han sacado cosas de la manga sobre el credo de los asesinos que no tiene ni pies ni cabeza, te dicen que hay una guerra entre asesinos y templarios pero no te dicen porqué nuevamente, simplemente luchan, la historia del antepasado es literalmente una pelea tras otra sin más y así un largo etc.
Realmente no pensaba que fuese tan decepcionante.
        
            