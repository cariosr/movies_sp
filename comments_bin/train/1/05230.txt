
                      Las estrellas se ven aburridas de sus mentes cuando el cuarto episodio de la franquicia se detiene entre las secuencias de carreras, que es demasiado a menudo en una película en la que 106 minutos pasan en lo que se siente al menos cuatro horas.
        
            