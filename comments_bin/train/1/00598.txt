
                      Que despropósito, que sinsentido, que locura, más allá de la complejidad y rozando la aberración sin pies ni cabeza. Después de la película te puedes hacer cientos de preguntas con o sin coherencia. He de reconocer que las interpretaciones son muy buenas en éste entramado absurdo. Al salir puedes llegar a pensar si su director tiene algún problema mental.
        
            