
                      Esta película es el rey de los clichés. Desde los primeros diez minutos te das cuenta que la película va de mal a peor. 
Tiene muy poco humor y la química entre los personajes protagonizados por Ryan Reynold y Tara Reid es prácticamente ficticia, por su falta de buen dialogo y acciones que acompañen a estos. 
Prefiero leer un libro o jugar en el computador que volverla a ver. 
1.7/5
        
            