
                      Una de las películas más lentas y aburridas que he visto. Incomprensibles las 14 nominaciones a los Óscars.

Recién salido del cine de sufrir esta película. He aguantado hasta el final por el dinero que me había costado la entrada y por confirmar que el final no iba a poder arreglar una película tremendamente aburrida y de una lentitud que te lleva a la exasperación y al deseo de abandonar la sala.

Comparar esta película (con unos surrealistas 8.6 puntos en IMDb) con películas como Ben Hur (8.1), Titanic (7.7) o Ciudadano Kane (8.4) es como comparar los castillos de arena que hace mi hijo en la playa con el Empire State.

Ni es comedia (no he oído ni una sola risa en toda la sala a lo largo de toda la película), ni creo que se le pueda llamar musical, ya que eso sería insultar al resto de musicales. Y desde luego no es una película romántica. He visto a suegras que miran a sus nueras con ojos más amorosos que los que mostraban los protagonistas de esta película al mirarse.

TEDIOSA en grado extremo. Totalmente desaconsejable
        
            