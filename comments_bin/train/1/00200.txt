
                      No es una adaptación, a menos que por ello se entienda saltarse todos y cada uno de los aspectos narrados en el libro. No es una película donde el fan del libro encuentre la materialización de aquella narrativa: la historia de Ender no se desarrolla desde los 6 a los 16, la interacción y aparición de los hermanos apenas suma 2minutos y para nada se ve su operación Locke-Demóstenes, no se ve reflexionar a Ender como en el libro, etc. hasta llegar, para mayor decepción, a un final inventado. En fin, que quiero que me devuelvan el dinero.
        
            