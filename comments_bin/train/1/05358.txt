
                      Es asqueroso como el infierno. Un médico (Cary Elwes) y un exaltado (Leigh Whannell) están encadenados en un sótano, y solo una sierra puede sacarlos. El director James Wan pulsa el botón espeluznante sin vergüenza.
        
            