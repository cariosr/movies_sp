
                      Es de lejos de las peores. Luke Skywalker es un héroe de la saga clásica y aquí lo han vuelto un inútil y patético quejica que termina sus dias como una versión mala de Shaka de Virgo de Caballeros del Zodíaco. Y Leia, si le hubieran eliminado al explotar la nave habría tenido un final digno, pero fue deprimente verla volar como Superman por muy usuaria de la fuerza que sea. Los porgs muy útiles para vender peluches, pero nada más.
La trama de Finn y Rose no sirve para nada.
Yoda, que le dijo al propio Luke en El Imperio Contraataca "Por eso has fallado" luego de la célebre "Hazlo o no lo hagas, pero no lo intentes"... aquí se conforma con un "El Mejor Maestro el Fracaso es". Me parece tan contradictorio el ver ambos personajes antes y ahora.
Creo que Disney debería confiar la dirección y el guión a gente que conozca el universo de Star Wars, como Dave Filoni, que es capaz en las series de Clone Wars y Rebels de innovar sin matar el espíritu original que dio George Lucas.
        
            