
                      Floja floja floja floja, buenos actores, buenos efectos, buena simulación de naves (aunque hoy en día ya no tiene mucho mérito), trama típica y nada sorprendente, final previsible. Si eres un amante real de ciencia ficción, no te molestes ni en verla, puedes vivir sin ella perfectamente.
        
            