
                      Después del espectáculo de horror que fue The Fast And The Furious: Tokyo Drift, fue una gran sorpresa que Universal encargara una nueva película en la franquicia, sin importar que el director Justin Lin vuelva a participar. Pero como suele ocurrir en la industria del cine, el dinero habla. Tokyo Drift ganó casi $ 160 millones en la taquilla de todo el mundo, por lo que puedes ver por qué querían que la serie agonizante fuera otro rayo de vida.

Esta vez alrededor del reparto original regresará. Después de que rechazó la secuela porque no le ofrecieron suficiente dinero, Vin Diesel y su agitada carrera debieron de gritar de alegría cuando Fast & Furious se dejó caer en su puerta. Paul Walker, Michelle Rodriguez y Jordana Brewster completan la alineación, todos los cuales parecen haber salido del original.
        
            