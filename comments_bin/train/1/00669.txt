
                      Una vuelta de tuerca innecesaria para los cinéfilos que necesitan que les sorprendan con algo distinto, que no por novedoso ha de ser bueno. Estética de Mortadelo y Filemón, guión simplón, diálogos artificialmente recargados con 3 adjetivos por cada sustantivo, y un MacGuffin penoso de un cuadro del que no es creíble su trascendencia. Ni siquiera es perfecto el supuesto simetrismo que se le achaca al director en sus planos (aunque no acabo de ver el mérito ni la belleza si simétrica fuera)
        
            