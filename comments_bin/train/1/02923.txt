
                      Todo comenzó con The Chipmunk Song (Christmas Don't Be Late), el exitoso single de novedad escrito y grabado por Ross Bagdasarian Sr. en 1958 que vendió millones y ganó tres premios Grammy. Mientras que la canción ha durado, Alvin y The Chipmunks y el trío femenino The Chipettes (creado en 1983) han estado acumulando más "nueces" (nominaciones y premios por grabaciones, programas de televisión, especiales, etc.) y, más recientemente, masticando El escenario en tres películas en vivo de acción familiar y CGI.

Chip-Wrecked sigue las molestas características de Alvin y The Chipmunks (2007) y The Squeakquel (2009), que siguen siendo muy molestas (para los adultos) y continúan con las desventuras de los astutos roedores cantantes y danzantes con voz de helio y sus Dave Sevilla (Jason Lee), padre sustituto del gerente, cuando abordan un transatlántico de lujo para el muy necesario R&R antes de un importante evento internacional de música pop.
        
            