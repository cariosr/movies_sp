
                      Una continuación bochornosa de una primera parte bastante decente. Conflictos mal llevados, abuso de escenas que no son más que sueños, una resolución final absurda y unos zoom locos que, al menos, logran hacer reír por su sinsentido.
        
            