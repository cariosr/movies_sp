
                      Es un engendro espantoso filtrado por transparencias grises que la convierten en un tortuoso ejercicio visual. Poco importa como conoce a Chewaka o gana el Halcón, se suceden secuencias inconexas y deslabazadas en un mal estilo de buenos, malos, raros, menos malos, malísimos... Para terminar preguntaría: ¿a nadie se le ha ocurrido suprimir la desagradable y horrible escena en la que Solo mata sin piedad a Harrelson? Espantosa.
        
            