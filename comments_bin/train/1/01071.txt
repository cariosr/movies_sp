
                      No tengo palabras para describirla... no es mala...¡ES PEOR!

Me ha defraudado mucho. Esperaba llorar de risa como en la primera y he llorado, sí... pero de pena. Se nota demasiado que han aprovechado el tirón del exitazo de "Ocho apellidos vascos" (muy merecido, por cierto) pretendiendo recaudar dinero fácil. Quizás lo estén consiguiendo, no lo sé, pero como hagan una tercera parte como ésta, me parece que se les acaba la gallina de los huevos de oro.

La interpretación del reparto ha sido digna aunque, a buen seguro, habrán tenido que esmerarse muy mucho para sacar ese guión tan deplorable adelante. 

Mi enhorabuena a los actores y actrices por el esfuerzo.
        
            