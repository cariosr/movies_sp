
                      En la ausencia de cualquier cosa que se parezca a la estructura, el carácter, el punto de vista o el sentido de propósito, no hay otro lugar para que este proyecto vacío vaya.
        
            