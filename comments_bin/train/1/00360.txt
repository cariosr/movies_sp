
                      Ésta [comedia], poco novedosa e imaginativa donde las haya, parece parte de una campaña institucional de prevención de la promiscuidad, a ratos de manera explícita y en general, por el tono pacato y ñoño que marca el comportamiento de sus personajes, [...]
        
            