
                      Empecé a verla y multitud de cosas vinieron a mi mente:
¿Puse la lavadora?,¿Saqué a pasear al perro?,¿Me dejé la televisión encendida?,...
No consiguió centrar mi atención, pero sirvió de estimulante para repasar mi agenda diaria.
El dinero que me gasté en el cine me da la sensación que lo tiré por el desagüe, y eso que el precio supone 2 horas de mi trabajo.
        
            