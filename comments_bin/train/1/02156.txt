
                      Contiene muchas incongruencias y contradicciones. Intenta manejar las emociones del espectador pero no solo no lo logra, produce confusión. ¿Si los padres lloraban de alegría porque la juez lo estaba salvando porqué el chaval les dice que querían que muriese? ¿Si el chaval había averiguado por internet que un juez no le dejaría morir, porqué a su vez dice que había pensado en hacer un vídeo de despedida para colgarlo en las redes? Veo una trama muy forzada, como a martillazos. Han sido dos horas de mi vida tiradas, prefiero una buena siesta.
        
            