
                      La película empieza bien, parece una historia interesante de la que se puede sacar bastante partido. Sin embargo, a medida que avanza se vuelve aburrida y pesada. Alguna escena de miedo logra mantenerte en tensión, aunque al final defrauda. Muy poco recomendable.
        
            