
                      Extremadamente aburridísima. En mis 60 años esta es la segunda vez que tengo que salir del cine al no soportar la película, pese a tener un excelente reparto de actores de actualidad que, sin ser actores de cine, resultan muy divertidos, pero en esta película no han podido lucirse en absoluto. No la recomiendo a nadie.
        
            