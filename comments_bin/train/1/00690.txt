
                      FIlme mediocre y tedioso donde los haya. Un pastiche y un ejemplo del estado decadente en el que se halla sumido Hollywood, dominado por completo por firmas tecnológicas propietarias actualmente de los estudios, y que sólo ansían fabricar productos de consumo a los que puedan dar mucho bombo de marketing y les generen ingresos a corto plazo, descuidando todo lo demás. La historia podría ser buena, incluso brillante, si el director y los actores la hubieran encarado con cierta seriedad. No funciona de ningún modo.
        
            