
                      Pues una más del típico personajillo mediocre americano. Fracasado en la vida real, se viste de soldadito y se va a matar "terroristas" a un país lejano. Personaje sin fuste ninguno: por mucho que escarbes no se le encuentra fondo, y no se le encuentra porque no lo tiene.
Por otro lado, no he visto en el cine una historia de amor con tan poca credibilidad. Supongo que sería igual en la vida real.
        
            