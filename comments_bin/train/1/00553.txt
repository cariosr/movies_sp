
                      Yo no he visto la original asi q mi mente no esta condicionada.
Personalmente me ha parecido una pelicula innecesariamente larga y aburrida. La forma de rodarla tan lentamente en algunas escenas hace q las casi tres horas de duración se hagan interminables. La trama aunque es interesante ni te atrapa ni te envuelve pues parece estancarse cada diez minutos. Te quedas con la espina clavada al pensar que les hubiera quedado una muy buena película si la hubieran desarrollado de una manera mas entretenida y trepidante para el espectador. Decepción absoluta.
        
            