
                      El último reinicio de los Cuatro Fantásticos, el equivalente cinematográfico del malware, es peor que inútil. No solo raspa el fondo del barril de la película Marvel; golpea el suelo y succiona a la audiencia en un agujero negro de embotamiento que provoca el aplastamiento del alma y el coma. Y, adivina qué, es una historia de origen. Está bien. Se contrató a un joven y talentoso elenco (Miles Teller, Kate Mara, Jamie Bell, Michael B. Jordan) para renovar la trama, como una vieja puta que intenta pasar como jailbait. No vayas.
        
            