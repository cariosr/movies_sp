
                      Es la peor de toda la saga con diferencia. No solamente no aporta nada nuevo, sino que se limita a copiar, pegar y modernizar algunas tramas y escenas anteriores. Para colmo se cargan todo el trasfondo de la filosofía Jedi, y la película en general te deja frío. Algunos de los actores nuevos (Finn) no dan la talla y alguno de los antiguos (Princesa Leia) tampoco. El guion es limitado en exceso y la trama insulsa como ella sola.

Pues aquí uno abandona el barco, ya no tengo ganas ni de saber cómo sigue. Mastíquenla como puedan, para mí es infumable.

Y cuando sale War Craft? A ver si hay más suerte con sagas nuevas...
        
            