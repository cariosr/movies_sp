
                      ¿Es mejor película “Los lunes al Sol” que “Hable con ella”? ¿Es mejor interpretación la de Javier Bardem que la de Javier Cámara? La respuesta a las dos preguntas es, rotundamente, un ¡NO!.

Me interesé por esta película por el reparto y por los numerosos premios cinematográficos ganados (goya a la mejor película, al mejor director, mejor interpretación masculina protagonista...); pero después de ver la película pienso que dichos premios no eran merecidos, ya que las demás competidoras la superaban.

Lo mejor de la película la interpretación de Bardem, porque el guión es flojo y la película, pese a que es de corta duración, se te hace muy extensa.
        
            