
                      La demografía ha cambiado radicalmente en el pequeño e idílico Haddonfield, Illinois, el suburbio totalmente estadounidense donde se desarrolló el clásico de terror de 1978 de John Carpenter, '' Halloween '', junto con varias de sus secuelas decididamente no clásicas.

Ahora es el centro del universo punto-com, donde un astuto empresario afroamericano, Freddie Harris (Busta Rhymes), y su novia tecnológica (Tyra Banks) se han instalado como "www.dangertainment.com". concepto: conectar media docena de adolescentes con pequeñas cámaras de video y enviarlos a pasar la noche de Halloween en la casa donde Michael Myers (Brad Loree), el imparable y psicótico asesino que es el único personaje perdurable de la serie, pasó su infeliz infancia y afirmó Su hermana mayor como primera víctima. Los internautas de todo el mundo podrán sintonizar y seguir la acción, eligiendo a qué adolescente desean seguir a través del menú interactivo del sitio.

Después de la entrada inicial, el Sr. Carpenter, quien se retiró sabiamente de la serie, los productores recurrieron a Rick Rosenthal, quien dirigió '' Halloween II '' en 1981, para filmar esta octava entrega de la aparentemente interminable saga. Él hace lo que puede, pero no hay muchas oportunidades para inventar en la fórmula eliminada.
        
            