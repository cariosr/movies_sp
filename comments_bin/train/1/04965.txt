
                      15.17 Tren a París (2018).
Hay que saber retirarse, o como dice mi buen amigo Pablo; saber jubilarse. Y es que no creo que el bueno de Clint Eastwood, a sus casi 88 años, haya realizado un largometraje más aburrido y malo que este...

La gracia de la película está en que los héroes de la historia, los que evitaron un atentado terrorista en un moderno tren con destino a París, son los que la protagonizan encarnándose a sí mismos. Lo malo es que para rellenar el guión nos cuenta el paseo por Europa que se pegaron y algo de su infancia, sin más interés que el que sus progenitores podrían tener por ella. 
Como secundarios muy secundarios me ha llamado la atención reconocer a un Steve Urkel venido a menos y sin preguntar en ningún momento "¿He sido yo?", y al actor que interpretara en los 80´s al barón de Münchausen en la cinta más cara hasta aquel momento.

Lo dicho: aburrida como cualquier peli en plan documental sin otro mejor momento que los pocos minutos de acción del desenlace del filme. Para ver en casa, sin duda...

Raúl Cabral.
https://www.filmaffinity.com/es/film236004.html

Pd. Por una vez coincido con la crítica en no darle ni el aprobado.
        
            