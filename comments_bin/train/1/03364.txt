
                      Sí, la primera película europea en 3D... pero resulta tan deficiente que el que vaya a verla será a cuenta y riesgo suyo. Fuerte campaña promocional. Dentro de poco sólo será recordada por "ser la primera película europea en 3D". Tal vez para los más pequeños les guste porque a la mayoría de los adultos que sé que la han visto no.
        
            