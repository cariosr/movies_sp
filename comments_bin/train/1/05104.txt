
                      Si bien hay buena química entre el elenco protagonista y se producen algunas situaciones algo divertidas, la verdad es que el producto como un todo es tosco, previsible y cargado de humor "grueso". Si a eso le añadimos giros argumentales y desenlaces inverosímiles, estamos ante un producto de lo más olvidable y visto mil veces
        
            