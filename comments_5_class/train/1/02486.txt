
                      A Almodóvar no lo queda bien el negro. Lo suyo es el rosa chinón o el amarillo jengibre. Alejado totalmente de la realidad vive en el mundo del cine, un territorio idealizado en el que se pierde hasta no poder encontrarse. Mal, Pedro, Mal.
        
            