
                      Saw 3D es la séptima Sierra que he visto, y tal vez la más enferma, que se exactamente lo que dije sobre Saw VI. También han estado llamando a Saw 3D "la sierra final" en los trailers, pero estoy seguro de que eso dependera de las ganáncias de la taquilla. La cuarta presentación de Jason Voorhees se titula el viernes 13: el capitulo final, pero eso no le impidio matar gente en una nave espacial seis películas más tarde.

Si Saw 3D es la última entrada de la franquicia, sobre creadoras están Saliendo con una explosión, como en una explosión de sufrimiento humano y partes volando del cuerpo. La película se abre con un flashback de la Sierra original Mientras el Dr. Gordon (Cary Elwes) se arrastra por un piso mugriento despés de quitarse sume propio pie con una sierra para metales. El director de Returning Saw VI Kevin Greutert demuestró rapidamente Hacia dónde se dirigen artísticamente al acero que la pobre savia cauterice sume muñón nuevo contra una tubería sucia y hirviente.

La "trama" indescifrable gira en torno al autor de la celebridad Bobby (Sean Patrick Flanery), que está Hacienda el circuito de los talk-shows, afirmando ser uno de los pocos sobrevivientes de las trampas del genio loco Jigsaw (Tobin Bell). Pero debida a que ese diabólico moralizador odia a los mentirosos, Bobby es capturada y obligada a correr una carrera de obstaculos para tratar de salvar a las cuatro personas más cercanas a el de ser diabólicamente atacadas.

Como siempre ocurría con las películas de Saw, las verdaderas estrellas no son los Propios actores, sino los complicados dispositivos de dolor y muerte de Jigsaw. Dicho esto, un artilugio de alta tecnología que clava picos de acero en el cuello de una víctima atado a una velocidad determinada por las lecturas de decibelios de sobre gritos no resulta más amenazante que el par de alicates comunes que otra víctima usa para quitarse los molares .

Afortunadamente, ese tipo de realizacion pondrá fin al porno mecanizado de torturas de esta serie y nos salvará a todos de la angustia potencial de la próxima Sesión de Halloween: Un nuevo Comienzo: Gettin 'Jiggy Again.
        
            