
                      Tan pero tan mala que parece una película cómica. Que pena, al ser basada en hechos reales tenía mucho potencial, para ver gente maquillada y disfrazada ya es suficiente con Walking Dead. Cero clima, cero suspenso, cero imprevisibilidad sobre el desenlace. Me extraña que Helen Mirren, una actriz de renombre, haya aceptado actuar en semejante bodrio.
        
            