
                      La verdad: de las peores películas que he visto junto a Another Year. Será que no tengo ni idea de cine porque me asombra las notas positivas que tiene. La película se desarrolla el 98% en un salón de una casa con una discusión entre 2 parejas que degenera hasta límites insospechados. Y el hecho de que el escenario sea un salón, no es nada negativo. De hecho, la película "El Nombre" también se desarrolla en un salón y me resultó infinitamente más graciosa que este bodrio (¡a poder ser, vedla en su versión original!)
Me dormí a mitad de la película y tuve que retomarla al día siguiente. Y la comedia estará hilada muy muy finamente porque no recuerdo un solo momento gracioso.
-> ¡Os recomiendo que no perdáis el tiempo con esta película!
        
            