
                      Si hubiera que definir "malo", sería esta pelicula. Al lado de la definición de esta palabra en el diccionario van a plantar una foto de La venganza de Ira Vamp. Existen varios films de humor españoles de gran éxito y casi podría asegurar que ni por asomo esta pelicula se encontrara entre ellos.
Curioso Josema Yuste y Florentino Fernández, funcionan de un modo increíble en la pequeña pantalla y hacen reir al personal pero en cine... No llegan a conseguirlo, no hay química, no generan magia ni misterio, ni tan siquiera lo que "es lo suyo": risa/humor.
No vale la entrada ni el tiempo que se emplea en verla y... Podría emplear lineas y lineas que argumentarían siempre finalmente que es mala, malisima. Pasad olímpicamente.
        
            