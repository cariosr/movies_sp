
                      Creo que es hora de desmistificar a Bolivar y los demás "libertadores", que no hicieron otra cosa que desligarnos de nuestros orígenes hispanos para lanzarnos a las garras anglosajonas. El personaje Bolivar no tiene nada que ver con la realidad. Su figura se ha deformado de tal manera que parece una figurita de Hollywood, comenzando con la forma amanerada de hablar y no el hombre que vivió y luchó con sus virtudes y defectos,
Seguiremos esperando la sinceración del cine venezolano
        
            