
                      Tras leer el libro (un libro para niños sin demasiadas pretensiones), la opinión sobre la película es básicamente la misma que saqué tras finalizar mi lectura.
Un film muy, quizá demasiado para niños por aquello de los cuentos y sus personajes con dialogos/guión que, por momentos no llega a comprenderse bien debido a que es tal vez infantil en exceso. Existen peliculas en clave de documental-imagen real, que nos llegaron mucho más que esta producción, que no logra emocionar en ningún momento. Hay que resaltar el mal doblaje de los actores, excluyendo alguno de ellos, los menos.
Por desgracia para esta producción española nuestra, no llegó a hacernos volar con la imaginación como supongo se pretendia.
        
            