
                      Como personaje popular de DC Comics que ha pasado por varios tonos de la mitología de cómics desde que fue presentado en 1940, el Lantern original fue un ingeniero ferroviario que descubrió una lámpara mágica que le otorgaba un brillo verde y un arsenal de superpoderes.

Ahora, 70 años más tarde, Hal Jordan (Reynolds), piloto de pruebas especiales, es renuentemente nombrado como un escuadrón de élite de fuerzas de paz intergalácticas, y transformado en el primer miembro de la Tierra de los Faroles Verdes.
La película cuenta con los considerables encantos de Reynolds, su principal protagonista, quien ha dado muchas oportunidades para exhibir su físico buff dentro y fuera de su traje de color jade, personalizado.

Pero Reynolds, tan efectivo en otros papeles de la película, jugando a la guapísima belleza, sino a chicos más o menos ordinarios, no parece estar a la altura del superhéroe. No es tan convincente como un protector del cosmos teñido de verde con algunos de los poderes más asombrosos del universo en sus manos.

Hay otros problemas tambien. La historia recorre, abarrota y combina plantillas de otras franquicias de películas mejores, a menudo emblemáticas, como Star Wars, Superman y Top Gun.
        
            