
                      Solo para fans incondicionales y con una curiosidad desmedida. Para el resto será una perdida de tiempo y dinero. Si odiáis a Adrien Brody esta es vuestra película ya que es una colección de argumentos en su contra. Lo único bueno de la película es que, como los pistachos amargos, te obliga a disfrutar de nuevo de la original para quitarte el mal sabor de boca.
        
            