
                      No es mi intención ser despiadado con esta película porque creo que toda película conlleva esfuerzo económico, esfuerzo de trabajo en equipo y esfuerzo de voluntad. Y ya el mero hecho de hacer una película es hazaña.
Dicho esto, en mi opinión la premisa era buena y es un tema de actualidad. Y el uso de los tópicos les salió redondo en OCHO APELLIDOS VASCOS, así que, ¿por qué no seguir la fórmula?
Salí del cine algo enfadado. La palabra es decepcionante y algo sorprendido porque las interpretaciones me parecieron tan exageradas que lo hacían inverosímil. Salvo José Sacristán, los demás, no sé si por órdenes del director o aportación propia, eran caricaturas de personajes. No me los podía creer.

Fui con mi prima, y ella se rió en algún momento, así que quizás yo esté siendo excesivamente crítico.
        
            