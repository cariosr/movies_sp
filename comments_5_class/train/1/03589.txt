
                      Menudo tranque!. Mejor hubiera puesto gasoil al coche con el precio de las entradas. Un argumento inexistente, desarrollo lento y previsible. Demasiado tiempo para tener el mismo chicle en la boca. Lo único que evitó que algunas señoras no se durmieran en el cine fue pensar en que Jack enseñara algo mas de si mismo. Eso fue ayer y todavía se ríe al recordar un titulo tan pomposo "L'americano"...
        
            