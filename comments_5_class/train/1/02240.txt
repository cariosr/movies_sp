
                      En Saw III, el genio asesino conocido como Jigsaw finalmente obtuvo su merecido. Era un ser humano muerto, sin posibilidad de una resurrección al estilo de Michael Myers o Jason Voorhees. Pero, ¿desde cuándo una pequeña cosa como la muerte impidió que una franquicia de terror cobrara?

Jigsaw está de regreso, o al menos su cadáver, y, chico, sus entrañas se roban el espectáculo. Cuando se abre la película, vemos los restos fríos de Jigsaw (Tobin Bell) desnudos en una losa mortuoria, que pronto serán objeto de una autopsia en toda regla. Los asistentes a la Morgue empiezan por pelar la cara del Jigster, aserrarle abren el cráneo, separándolo y extrayendo su cerebro bulboso, todo de manera terriblemente realista.
        
            