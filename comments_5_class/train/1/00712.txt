
                      Media hora para ver la magnificencia de los decorados y de la puesta en escena. Si hablamos del guión, llevo 30 años viendo cine y es de las pocas películas que no he conseguido terminar de ver.
        
            