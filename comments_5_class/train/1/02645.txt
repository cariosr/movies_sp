
                      Los Croods es una película que solo divierte a los chicos, y eso por su estupidez.
El argumento, eje de toda película, es predecible y estúpido. Los personajes son planos, unifaceticos. De hecho, casi todos son usados únicamente para situaciones cómicas y unos pocos dialogos. La película sigue la fatidíca tendencia del "para ser gracioso, todo debe salir mal", por poner un ejemplo, en una escena un personaje se para sobre una roca y esta rueda, chocando contra un árbol. Despues de eso, el arbol cae sobre el personaje. El único "guiño" que la película presenta hacia los adultos es la terriblemente estereotipante desesperación que tiene el padre por que se le muera la suegra. Llegando a un punto casi insultante.
La música se divide en dos tipos. En el primer tipo, la música no coincide con la época propuesta (Podrías estar enfrente de unos mariachis). En el segundo tipo nos encontramos frente a una especie de remasterización de la banda sonora de "La Era del Hielo" Original (Les juro, es igual nomas que con distintos instrumentos).
La animación es lo único que le merece su estrella. Los paisajes parecen reales y los personajes están cuidadosamente hechos.

En resumen, es una película malísima. Bien que es para chicos pero, como Disney no se cansa de demostrar, los chicos merecen ver cosas mas complejas y emocionantes que esta película. Y, dada la fama que tiene Dreamworks de arruinar sus películas con secuelas, la idea de una segunda parte parecería salida de, valga la redundancia, un cerebro prehistorico.
        
            