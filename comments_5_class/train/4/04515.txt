
                      El problema de llevar las expectativas tan altas es que, una vez se sienta uno en la sala de cine, la pequeña decepción comienza a cocerse. Wright sigue pareciéndome el director de comedia más innovador y original, y es este hecho el que me ha dejado a medias con 'Baby Driver'. Tiene todos los elementos que lo consolidan como realizador (excepto la grata sorpresa de Elgort) y, guste o no, posee una espléndida combinación de coches y música.
        
            