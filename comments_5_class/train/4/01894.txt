
                      En 2013, a Hollywood le entró de repente unas ganas locas por destrozar la Casa Blanca (otra vez) y al presidente de los EEUU (otra vez). Y es que nos llegó “Asalto al poder“, de Roland Emmerich y “Objetivo: La Casa Blanca“. De la última, debe ser que tenían más historias que contar, pues ahora nos llega una secuela, cambiando de localización.

Mezclando las americanas de “Misión: Imposible“, las conspiraciones de James Bond y la seguridad del gobierno para el presidente de “Air Force One“. La película goza de entretenimiento puro y duro, y es que hace honor a su lugar de procedencia. Éste es un buen producto Hollywoodiense, ni más ni menos. Cine para disfrutar viendo cómo explotan las ciudades más conocidas del mundo, y como las persecuciones más imposibles se suceden una tras otra en calles reconocibles por todos. “Objetivo: Londres“, es una película más para disfrutar una tarde de sábado con el bol de palomitas más grande que encuentres y te prepares para que tus oídos no escuchen otra cosa más que bombas, tiros, lanzallamas y granadas.

Eso sí, piensa que aquí los buenos pueden ser cualquier bueno, perseguidos por malos, que pueden ser cualquier malo. Con esto me refiero a que no intentes explicarte y buscarle lógica a que el presidente de los Estados Unidos pueda correr solo por las calles de una solitaria y desolada Londres perseguido por unos pakistaníes. Y es que sí, eso es solo cosa de estos blockbuster de Hollywood.

Lo mejor: el plantel de actorazos que llenan la pantalla.
Lo peor: que la tercera entrega “Objetivo: El mundo”, sea por ahora, solo producto de mi imaginación.
        
            