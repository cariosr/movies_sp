
                      Guardianes de la Galaxia es una película de ciencia ficción dirigida por James Gunn, basada en el cómic homónimo de Marvel, creado por Dan Abnett.

Ambientada en el año 88, la película empieza con la muerte de la madre de Peter Quill. Al enterarse de su fallecimiento sale corriendo al bosque, donde es secuestrado por una nave.
Veintiséis años después, vemos que Quill se ha convertido en el recadero de los que lo secuestraron.
Le encargan encontrar el orbe, un objeto muy valioso, pero tan peligroso que podría acabar con la galaxia. La misión se le complica cuando ve que no es el único que quiere el orbe.

Hasta la fecha, esta es la película de Marvel con más comedia, que radica, sobre todo, en Rocket y Groot.
Es una película tan cuidada en detalles, que hasta los personajes menos importantes consiguen hacerse destacar.

La banda sonora está compuesta esencialmente por canciones de los 60 y 70 que el protagonista tiene grabadas en el cassette de su madre.

Como siempre, Marvel tiene un patrón de tres años entre las primeras partes y las segundas, así que habrá que esperar hasta 2017 para ver Guardianes de la Galaxia 2.
        
            