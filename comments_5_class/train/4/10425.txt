
                      Aunque no es de las mejores películas de Leonardo Dicaprio, la historia es envolvente y mantiene al espectador conectado. Los efectos son muy creíbles, la ambientación es atrayente y la fotografía es excelente.
        
            