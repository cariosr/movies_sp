
                      "Predator" comienza como "Rambo" y termina como "Alien", y en el Hollywood de hoy, eso es creatividad. La mayoría de las películas están inspiradas en un solo éxito de taquilla anterior.

La película está protagonizada por Arnold Schwarzenegger como líder de un equipo de comando del Ejército de los Estados Unidos que se adentra en la jungla sudamericana en una misión política y termina en un duelo con un asesino del espacio exterior. Este es el tipo de idea que se produce al final de una sesión de lluvia de ideas de 10 segundos, pero si se hace bien, ¿a quién le importa?

"Predator" está muy bien filmado. Es una imagen de acción resbaladiza y de alta energía que toma gran parte de su fuerza de sus lugares llenos de vapor en México. Los héroes pasan la mayor parte del tiempo rodeados por una jungla impenetrable, una pared verde de vistas majestuosas pobladas por todo tipo de depredadores naturales, además del alienígena. Rara vez he visto una jungla más bella, o más convincente; el efecto de ubicación está a la par con "Fitzcarraldo" y "El Bosque Esmeralda".

Nota: 4/4
calificación: Fresh
        
            