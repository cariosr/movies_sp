
                      Esta película muestra una posible realidad aunque bastante surrealista en su desarrollo pero que acierta. Es imposible no divertirse con Tom Hanks y a pesar de lo "sosa" que pueda parecer la película en un principio, empiezan a suceder acontecimientos, muchos muy divertidos y algunos bonitos y emotivos que nos aseguran el entretenimiento. Las largas esperas en los aeropuertos internacionales es una realidad que todos conocemos, que afecta de manera notable en la vida de las personas y es algo a tener en cuenta.
        
            