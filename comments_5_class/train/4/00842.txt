
                      Muy recomendable. A ver, una película entretenida (no te aburres en ningún momento), graciosa (muchas situaciones de humor) y con valores (amistad, aceptar a los demás, etc.). No se puede pedir mas con los bodrios que reinan por los cines. Y además llevando el protagonismo un secundario de lujo, que no tiene nada que envidiar a Rayo.
        
            