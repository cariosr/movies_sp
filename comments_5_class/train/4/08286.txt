
                      A pesar de lo que dicen algunos críticos, a mi la película me lleno por completo, la acción es incansable, y la emoción de ver a la trinidad en pantalla es increíble. Tiene algunos altibajos, pero el líneas generales la película es sensacional y me deja con las ganas de ver La Liga de la Justicia y Wonder Woman en acción.
        
            