
                      Jon Watts nos trae a un Peter Parker más joven, divertido y renovado, que amplía el universo del superhéroe y se aleja de la oscuridad en la que nos sumergió Sam Raimi. Puede que inevitablemente los giros de guion se asemejen a otras películas de Marvel -incluso el plan de Keaton, que por cierto, me encanta-, pero la simpatía y el carisma de Holland se comen la pantalla.
        
            