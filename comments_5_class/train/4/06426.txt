
                      Por fin nos llega el final de la segunda trilogía precuela “X-Men”, tras “Primera generación” y “Días del futuro pasado“. Aunque aún tenemos varias preguntas sin contestar, ya estamos más cerca de la historia que comenzó Bryan Singer con el primer “X-Men” alla por el año 2000.

Aunque viendo trailers, pósters e incluso leyendo la sinopsis, pudiéramos pensar que se trata un capítulo más en la saga, lo cierto es que “Apocalipsis” va mucho más allá de ser una misión cualquiera para los mutantes. Está llena de acción, de momentos memorables, de energía por todos lados y de un ritmo tan trepidante que las dos horas y cuarto que dura no se te hacen absolutamente nada largas.

Quizás la más parecida a la primera trilogía en cuanto a enfoque, la cinta posee más adicción que “Días del futuro pasado” y mucha más diversión que “Primera generación”. La multitud de personajes ya conocidos por todos, y los orígenes de sus aventuras sigue siendo el aliciente más importante de la película y eso deja un muy buen sabor de boca. Puro cine hollywoodiese, palomitero, de fácil visionado y para toda la familia. Con un villano que recuerda mucho al Iván Ooze de “Power Rangers: La película“, y que, definitivamente, es el mejor villano de toda la saga (con permiso, Magneto).

Lo mejor: QuickSilver y sus números de velocidad.
Lo peor: que la comparen con “Vengadores: La era de Ultrón“.
        
            