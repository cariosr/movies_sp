
                      A esta crítica se le podría titular de varias maneras: “Me gustan que los planes salgan bien”, “¿Qué opinan los espías de jubilarse a los 67?” o ¿Qué pasaría si juntásemos a cuatro actores veteranos para hacer una película de acción?

Pues lo que aquí nos encontremos es un grupo de agentes de tipo “El equipo A” que se supone que están retirados, pero cuando se trata de hombres duros, de la talla de Bruce Willis, o de zumbados como John Malkovich,  veteranos como Morgan Freeman o mujeres de armas tomar como Helen Mirren, la cosa cambia. La cara amable la pone la “chica joven” de la película: Mary-Louise Parker.

Acción, efectos especiales, ritmo trepidante por momentos, sentido del humor, una realización estiló video-clip, unos actores interpretándose a sí mismos, entretenimiento y diversión, es lo que nos encontramos en este largometraje.

He de decir que desconozco el cómic en que esta basada la película, por lo tanto, no puedo dar mi opinión sobre si es fiel al mismo, o se trata de una libre adaptación para ajustarse a los protagonistas.

En resumen: Cine entretenido y recomendable.
        
            