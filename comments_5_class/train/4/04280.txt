
                      El guión de Allen vuelve a recurrir en temas ya conocidos en su filmografía como la religión y el amor, con la chispa habitual y algún giro sorprendente. Con una bella fotografía y un diseño de vestuario y dirección artística exquisita, Magia en la luz de la luna resulta ser una delicia en la gran pantalla. Recomiendo dejarse llevar por la innegable magia de la que para muchos será una  "comedia menor".
        
            