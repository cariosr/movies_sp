
                      Deepwather Horizon es una fiel representante del genero suspenso y drama.

Las actuaciones de cada estrella no resaltan una sobre la otra, pero aun así se logra crear una atmosfera atrapante en la que nos deja pegados a la pantalla sin pestañar. 

Una clara representación de los riesgos que conlleva trabajar en cualquier planta petrolifera, y como sus obreros se juegan la vida en esto.
        
            