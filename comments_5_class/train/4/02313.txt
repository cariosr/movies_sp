
                      Malos Tiempos en El Royale (2018).
“A base de que te la peguen acabas aprendiendo.”
El thriller de hoy se me antoja una mezcla de películas como “Identidad” (que a principios de siglo protagonizara, entre otros, John Cusack), “Four Rooms” (de Robert Rodriguez, Tarantino y otros dos realizadores menos célebres), “Barton Fink” (de los Coen) y “Pulp Fiction” (huelga comentar nada sobre esta obra maestra). 
Es posible que “Malos Tiempos en El Royale” se convierta en una cinta de culto por su estética y estilo. Un estilo que nos recuerda a los citados hermanos Coen por su sobriedad a la hora de rodar, su particular fotografía, la profundidad y complejidad de los personajes que conforman la historia y ciertos toques surrealistas en el desarrollo de la misma. 

Dirige el responsable de “Daredevil” y de “La Cabaña en el Bosque” -Drew Goddard- donde contó, como hace aquí, con el espectacular Chris Hemsworth (su mera presencia en el motel con camisa abierta distraerá a damas y caballeros, sin lugar a dudas...). Goddard suele llevar a cabo labores de guionista, pero en esta ocasión nos lleva a la frontera de California con Nevada para introducirnos en la década de los 70, donde nadie es lo que parece...

“Lo que me cansa y me aburre son hombres como tú. Un ‘hombrecito’ frágil que utiliza a los débiles... ¡es mucho mejor escuchar la lluvia!”
El reparto es totalmente coral: por un lado, varios actores multipremiados como Jeff Bridges, John Ham, el mayor de los hermanos Hemsworth, y la pequeña gran artista que es Cynthia Erivo (este mes, también de estreno con “Viudas”). Erivo, con su poco más de metro y medio de estatura llena la pantalla con sus silencios, su contenida interpretación y, sobre todo, su inmensa voz (canta varios temas: la mayoría a capella). Por otra parte, completan el conjunto principal de personajes los vástagos de conocidos actores como Bill Pullman -Lewis Pullman- (quien sorprenderá a más de uno con su interesante papel) y de Don Johnson -Dakota- (que deja el mediocre y comercial cine erótico-festivo para encarnar a una mujer mucho más dura e imprevisible que la amante de Mr. Grey). 

En resumen, cine serio y de calidad aunque con un ritmo, a mi entender, demasiado lento al principio (su metraje es de dos horas y veinte nada menos). Atención al excelente doblaje en castellano neutro y a la banda sonora. 

Raúl Cabral. 
https://www.filmaffinity.com/es/film903138.html
Nota: para que se entienda la enorme complejidad que supone la realización de un filme como éste, esta producción generó 16000 puestos de trabajo directos e indirectos.
        
            