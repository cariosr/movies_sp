
                      Deadpool (2016). 
Llega la comedia de humor negro sobre el superhéroe más gamberro de Marvel. Ryan Reynolds protagoniza esta irreverente historia acompañado de diversos héroes (X-Men) y villanos donde se presenta por vez primera en pantalla grande a este nuevo elemento de la factoría de Stan Lee. 

Con, probablemente, más posibilidades que su anterior Linterna Verde, Reynolds promete volver con su borde y potente personaje. 

Aparte de su sentido del humor (atentos a su nada eufemista ni delicado amigo), es digna de apreciar la calidad de sus escenas de acción a cargo de la Weta, y su banda sonora para el que le guste la música muy americana...

Raúl Cabral.
        
            