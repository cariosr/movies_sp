
                      Con su estilo y desparpajo habitual, Moore indaga en la raíz del tema presentando pruebas en forma de cifras, documentos y sobre todo testimonios reales que harán que se nos ponga la carne de gallina ante lo que nos parece ciencia ficción.
En cualquier caso, todos los vicios del mundo en los que el amado y odiado Michael Moore pueda caer en Sicko no son ni de lejos suficientes para empañar el valor intrínseco de este documental imprescindible y terrorífico fruto de una realidad enferma.
        
            