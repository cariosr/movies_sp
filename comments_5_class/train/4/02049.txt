
                      Película inteligente pero muy difícil de entender. Debo confesar que tuve que buscar por google la explicación del final ya que no lo pude descifrar en el 1er intento. Está plagada de alegorías y hay que ser muy deductivo para interpretar el libro escrito por la ex pareja de la protagonista. 
En lo personal me gustó, está bien pensada y presenta un argumento muy original.
        
            