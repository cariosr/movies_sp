
                      Cuando se trata de películas animadas por computadora, descripciones como "reconfortante" y "clásico" han sido durante mucho tiempo el dominio de Pixar. Casi nadie pensaría en aplicarlos a DreamWorks, el estudio que brindó al público los cuentos de hadas de la franquicia de Shrek, junto con Kung Fu Panda y Madagascar.

Pero con su último esfuerzo animado por computadora, The Croods (PG calificado para acción ligeramente aterradora), DreamWorks puede finalmente estar listo para probar algo más que el ritmo frenético, las referencias de la cultura pop y los guiños del humor adulto que construyeron la marca del estudio. . La historia de una familia de personas de la cueva que intentan superar el fin del mundo es sorprendentemente sincera y encantadora, sin un doble sentido a la vista.
        
            