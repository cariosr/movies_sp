
                      A pesar de tener una puesta en escena demasiado convencional y monótona, la película logra captar la atención del espectador a través del razonamiento de Hannah Arendt y su lucha por defender el concepto de banalidad del mal, tan aterrador como fascinante.
        
            