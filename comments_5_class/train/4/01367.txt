
                      "The Bourne Identity" nos introduce en el universo de Jason Bourne con un buen filme de acción e intriga, que pudo haber sido ser mejor. Matt Damon, el ritmo rápido y las muy bien logradas escenas de acción, es de lo más sobresaliente de la película. Sin embargo la película falla al conectar completamente con la audiencia ante la falta de desarrollo de sus personajes secundarios, haciendo que la trama en sí, sea sólo un trasfondo para el desarrollo de esta vertiginosa película de acción.
A favor: La acción (mención aparte para esa excelente persecución, por las estrechas calles de París)  y el ritmo vertiginoso con el que avanza la película.
En contra: La falta de desarrollo en los personajes secundarios, nos limita a conectar sólo con Bourne.
        
            