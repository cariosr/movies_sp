
                      Amelie es una película que te hace sentir enamorado. Te transporta a una época diferente, y la personaje principal tiene mucho carisma quizás por que a ratos tiene la inocencia de una niña que desea atormentar con travesuras al que se pasa de grosero. 

Me agrada que toque temas serios como la muerte o padres emocionalmente distantes de forma llevadera, no como algo «telenovelesco» o «trágico» 

La fotografía y la paleta de colores es una belleza que da gusto verla y más allá de lo técnico, me gusta que «Amelie» tenga la capacidad de hacerte pensar que puedes cambiar la vida de la gente sólo mostrando interés y tratando de ser solidario o amigable.
        
            