
                      Idea muy original, sin pelos en la lengua y sin clichés establecidos de otras películas del género. Esto último debido a que nadie muere cuando se supone que lo debe de hacer, ni como te lo imaginas, y el mensaje de la película tampoco es el esperado.
Una grata sorpresa.
        
            