
                      Night Shyamalan es un director que tiene una trayectoria brillante en el cine, incluso a pesar de no ser éxitos todo lo que hace. Digamos que tiene una forma de crear única, que la hace totalmente suya, pero no siempre contacta igual el resultado con el gran público.

Yo me declaro abiertamente fan de él, y destaco de su gran filmografía los mejores títulos que son muchos: “El sexto sentido”, “El protegido”, “Señales”, “El bosque”, “El incidente”, “After Earth” el resto de su trabajo no lo destacaría, y eso a veces se une con resultados de taquilla y quizás es de lo que últimamente venía padeciendo en su trayectoria.

Pero Múltiple le ha vuelto a situar en todo el mundo como uno de los grandes éxitos de taquilla, y además se le acompaña de buenas críticas, todo ello por esta película que consigue “conectar” con un brillante James McAvoy, que encarna a un protagonista que padece un trastorno de identidad múltiple; bajo el cual 23 personalidades distintas conviven en su interior, y una vigesimocuarta con tintes más crueles, incluso fantasiosa, que quiere predominar sobre el resto para quedarse como la dominante.

Esta película, si no se hubiera hecho bien, hubiera podido parecer incluso ridícula, ya que no es nada fácil, cambiar de registros continuamente, conviviendo en escena, no importa en cuanta veces se grabaran las mismas; pero en todo caso deben parecer reales las personalidades que afloran y McAvoy en un gran ejercicio de interpretación, lo consigue, sin necesidad de llegar a ver todas ellas, ya que solamente nos muestran algunas de ellas.

En algún momento, incluso me recuerda a otro film de personalidades múltiples “Identidad” de James Mangold (2003), que para mí sigue siendo el primero en esta referencia, por tocar fría e inteligentemente este tema, mejor incluso que Múltiple.

También me parecen muy necesarias para complementar la historia las actuaciones de una joven Anya Taylor-Joy, que en poco tiempo se está haciendo un hueco después de “La Bruja” y “Morgan”; y una magnífica Betty Buckley, que ya trabajo con el director en “El incidente” en el papel de la doctora Karen Fletcher, que distingue siempre a que personalidad tiene delante, incluso hasta cuando la quieren engañar.

Es una película que sube de nuevo a primera línea a Shyamalan, y también lo hace por su final…

