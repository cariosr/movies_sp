
                      La tensión, no la sangre, alimenta este apocalipsis zombie zowie. La multitud de The Walking Dead puede verse afectada por la relativa escasez de carne podrida y pústulas con fugas. Pero la Guerra Mundial Z sigue siendo tan inteligente, astuta y aterradora como un zombie hambriento listo para arruinarte, bebé, tú.

¿Sorprendido? Yo también. Los rumores de nuevos trabajos, reescrituras y problemas de producción han plagado a la adaptación cinematográfica de la novela de Max Brooks (hijo de Mel) del director Marc Forster desde el comienzo de la filmación en 2011. El diablo comenzó la película. El adictivamente emocionante World War Z viene listo para rockear. Te agarra duro - no hay piedad.
        
            