
                      Si aún no entiendes por qué y cómo hemos llegado hasta esta crisis económica, esta es la película que deberías ver! No esperes acción ni grandes escenarios. Lo mejor es el reparto y la manera como mantienen la tensión de la trama. Si logras meterte en la película, saldrás satisfecho.
        
            