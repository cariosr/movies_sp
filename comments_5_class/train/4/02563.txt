
                      James Hunt y Niki Lauda son el sueño de un cineasta.

A mediados de la década de 1970, compartieron una rivalidad épica en un deporte, la Fórmula Uno, con mucha acción y un drama que desafía a la muerte. Fuera de la pista, los dos hombres no podrían haber sido más diferentes, ofreciendo al director Ron Howard y al guionista Peter Morgan (The Queen, Frost / Nixon) la oportunidad de explorar algo mucho más interesante que la típica película de deportes donde hay un solo héroe. Un camino a la gloria inevitable.

El problema con las películas de carreras de autos, sin embargo, es que son películas de carreras de autos. ¿Algún director ha encontrado una manera de ahorrarle al público el tedio eventual de ver a los automóviles dar vueltas alrededor de una pista y, en cambio, capturar la emoción del deporte?

Howard, un artesano confiable que no es conocido por sus secuencias de acción, hace lo mejor que puede al poner cámaras en casi todos los lugares que pueda imaginar: abajo, a los pies de un conductor, dentro de la cabina, detrás de las ruedas giratorias, arriba de los árboles que esconden los automóviles a toda velocidad y al costado de la cabina. Rastrear como pasan los coches.
        
            