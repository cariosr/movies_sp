
                      Del estilo "Deadpool" donde abunda mas la comedia que la acción, se contrapone a los viejos superhéroes serios y justicieros. Se aprecia a un protagonista payasesco, torpe por momentos pero efectivo en las peleas principales. Buenos efectos propios del género y entretenimiento garantizado.
        
            