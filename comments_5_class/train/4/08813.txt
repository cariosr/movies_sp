
                      Buena película sin duda. Contiene todos los ingredientes de un buen cóctel vital sin resultar excesiva en ninguno de sus componentes. Una historia tan real que cualquiera puede ver reflejados en ella acontecimientos de su propia vida con la intensidad adecuada y no desmesurada como ocurre en cine tantas veces. George Clooney impecable, y las niñas perfectas.
        
            