
                      La historia es todo un viaje para el espectador, prácticamente no da un respiro. Notable como se va desarrollando y por las diferentes emociones en las que nos pasea. No la dejes pasar, te va a sacar de donde estés y te va a meter en la historia.
        
            