
                      Al visualizar este largometraje, seguramente nos vendrá a la memoria, muchos largometrajes vistos anteriormente, pues contiene muchos puntos comunes, la novedad del largometraje será la variante que tiene con respecto a esos largometrajes.

Puntos comunes, se sitúa en el extrarradio de la ciudad, el hermano mayor está vinculado a las mafias locales. Variante, es la salida o escape que tiene el hermano pequeño, si en los otros largometrajes, era un concurso televisivo o la fotografía, aquí se trata del deporte rey, el balompié a nivel profesional.

Este largometraje, contiene el ritmo y la emoción de un partido de balompié, consigue atraer la atención del espectador sin llegar a aburrirlo, los personajes consiguen la empatía de los espectadores.

Marcel Rasquin nos sumerge en una Venezuela llena de calles empinadas, interminables escalinatas y chabolas, que forman parte del paisaje urbanístico y que comparte con las canchas de futbol, el escenario donde sucede la acción.
        
            