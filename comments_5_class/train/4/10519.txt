
                      Muy entretenida y chistosa. Maribel Verdú borda el papel de mujer al borde de una ataque de nervios. 
La elección de la Pedroche ha sido muy acertada para encarnar ese papel de niñata repelente, cargante y estúpida a la que te cargarías sin pensarlo dos veces (vaya, como a la Pedroche en la vida real). Y el resto de actores más que correctos en su papeles.
Sin grandes pretensiones, una españolada muy divertida.
        
            