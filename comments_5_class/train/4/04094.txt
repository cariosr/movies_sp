
                      Aunque al principio resulta un tanto complejo captar con agudeza las situaciones que entrelazan dos épocas distintas, se consigue transmitir la emoción que ambos actores reflejan en cada escena y es inevitable entregarte a la película y ausentarse de la realidad durante la totalidad del film. Argumento original y distintos episodios que no se esperaban convierten esta secuencia en recomendable.
        
            