
                      ¡Qué gusto cuando vas al cine a ver una película que tiene buena pinta y te mejora esas ya buenas expectativas! Muy recomendable, y cuanto más grande sea la pantalla, mejor. Argumento original –nada que ver con lo hasta ahora contado–, buen ritmo, buenas interpretaciones, magníficos vestuarios, espectaculares localizaciones (sorprendentemente, en Irlanda), extraordinaria fotografía. Y unos efectos especiales dignos de un Óscar.
        
            