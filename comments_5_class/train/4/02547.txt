
                      Película agradable, a pesar de lo que  se espera tras leer su sinopsis. Múltiples tramas llevadas con mayor o menor acierto, y una contención tal que se convierte en su principal virtud. Buenas actuaciones. Sacrilegio verla doblada.
        
            