
                      Habrá quien tache esta historia de culebrón. Pero las grandes interpretaciones, los diálogos que lo mismo nos estremecen que nos sacan alguna carcajadas y la tensión constante son razones suficientes para no perdersela.
        
            