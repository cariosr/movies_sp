
                      Es una película magnífica, pero a mi juicio fuerza un poco los sacos lagrimales del espectador, y se nota. Lo mejor, la actuaciòn de los niños, especiamente el mayor de la familia. Bayona y su equipo han hecho un trabajo excelente. El cine español, por fortuna, cuenta con directores de calidad y esa consiguiendo el lugar que merece.
        
            