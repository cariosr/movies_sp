
                      Cuando uno se preguntaba si ver o no esta película, siempre aparece la misma pregunta: "¿será demasiado zafia al sobre explotar los tópicos?" Ya os decimos que la respuesta es: NO.

Se nota y mucho la mano de los guionistas de "Vaya Semanita" (EiTB), y eso sin lugar a dudas, ya es una garantía.

Argumentalmente, aunque no es ninguna joya si que tiene la suficiente coherencia como para hacernos disfrutar de principio a fin (que no reír a carcajadas como dicen algunos) y hacernos pasar un rato genial.
        
            