
                      Sí, Atómica es puro músculo, pura fibra muscular eficiente, no anabolizada ni clembuterada. Es física, es palpable y en ocasiones hasta duele.

Atómica es la puesta en imagen real de la novela gráfica "La ciudad más fría", de Antony Johnston y Sam Hart. Novela ambientada en los días previos a la caída del muro de Berlín y que narra una compleja trama de espionaje. El guionista Kurt Johnstad y el director David Leitch han hecho una magnífica adaptación, reduciendo la trama al mínimo, casi a una anécdota, y centrándose en las escenas de acción, que apenas aparecen en la novela.

Han sabido conservar la naturaleza de su origen y recrear ese aire de cómic sucio, sin renunciar a lo que es: puro cine.

Atómica es cine de espionaje y es cine de acción. Lleno de referencias y homenajes, es quizás la única película actual de este género capaz de darle la réplica a Bourne y aportar algo nuevo desde una perspectiva radicalmente distinta.

Insisto en que apenas nos importa lo que nos cuenta, y nos atrapa con su forma de hacerlo. La complicidad guionista/director enrevesa deliberadamente la trama para desligarnos de ella y centrar nuestra atención en la impresionante, física y entretenidísima puesta en escena.

Sorprende Atómica por el intrincado y sin embargo eficiente manejo de la cámara. Mucho más allá del mero exhibicionismo estético. Con escenas rodadas en ángulos imposibles, llevadas al extremo, y con una fuerza brutalmente hermosa. 

Todas y cada una de sus muchas escenas de acción son ejemplares, y contiene la pelea mejor rodada que ahora mismo alcanzo a recordar. Escena que parece no acabar nunca, en que cada golpe duele, la sangre te salpica y te deja agotado físicamente. El trabajo de cámara es aquí especialmente memorable colocándonos no como meros espectadores, sino como partícipes involuntarios: nos empuja, nos tira por las escaleras, nos golpea contra la pared. Rodada sin adornos y tan bien orquestada que resulta real en su imposibilidad.
Sólo dicha escena (secuencia) ya justifica la existencia de Atómica.

Pero no sólo la cámara. El uso de la música nos lleva a pensar en Tarantino, en Boyle, incluso en Ritchie. La música es protagonista. Una impresionante selección de temazos de los ochenta (y alguna magnífica reinterpretación de los mismos), mucho más allá del mero cliché ochentero, que acompañan la acción como si estuvieran compuestos para esta película, que suenan en el momento justo, con el tono justo y manteniendo el estado de ánimo justo.

Pero si algo es atómica es Charlize Theron. Mujer de bandera, y actriz como la copa de un pino, es de las pocas estrellas de Hollywood que siguen teniendo la capacidad de eclipsar todo y a todos cuantos le rodean, de llenar la pantalla, transmitir humanidad y de hacer grandes los papeles pequeños. Dota al personaje de ese aire enigmático, sofisticado, peligroso e inalcanzable que requiere, y lo hace tan suyo que no sabemos dónde empieza Lorraine y donde acaba Charlize. 
Además, reparte ostias como nadie.

No, dudo que sin Charlize Theron, Atómica hubiera sido lo mismo...o ni tan siquiera hubiera sido. Porque la película necesita una protagonista  a su altura...y no se me ocurre ahora mismo otra opción.

James McAvoy sí, está realmente brillante, al igual que Sofia Boutella o John Goodman, y todos resultan memorables, a pesar del torbellino Theron. y la química (y la física) entre Boutella y ella es arrolladora, y su relación sirve no sólo para elevar la temperatura de la sala de cine, sino para desarrollar el aspecto humano, sensible y tierno de la destructiva rubia, y mostrar otra capa de la historia donde sí hay cabida para las emociones.

En definitiva, una de esas películas que suma, en vez de restar ,y que, aunque puede que no cree escuela, puede presumir de una gran personalidad y singularidad. Y por si fuera poco, al contrario de lo que suele suceder con este tipo de cine, gustará por igual a hombres y mujeres...y sacará ese macarra que todos llevamos dentro.
        
            