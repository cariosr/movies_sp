
                      Superhero Movie es una comedia escrita y dirigida por Craig Mazin, guionista de Scary Movie 3 y Resacón 2.

Es una parodia de la película Spiderman, en la que además aparecen otros superhéroes de cómics, o referencias a ellos; como X-Men, Los 4 Fantásticos, Batman...
Rick Riker (Drake Bell) es un adolescente marginado que un día, en una excursión a un laboratorio, es mordido por una libélula genéticamente modificada. Gracias a la mordedura, adquiere poderes sobrenaturales.

Para aquellos que hayan visto dichas películas de superhéroes les resultará fácil apreciar los guiños que hacen a las diferentes historias.

Esta es una comedia muy divertida, que tiene un golpe cómico tras otro; diálogos rápidos, gags visuales, y un humor absurdo que no llega a desagradar. Bastante recomendable para pasar un buen rato.
Además cuenta con actores de la talla de Leslie Nielsen (¡Aterriza como puedas!) y Kevin Hart (Epic Movie).
        
            