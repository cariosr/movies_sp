
                      No es una obra maestra, pero reúne suficiente interés. El ritmo narrativo pausado propio de la cinematografía japonesa, eso sí, y sus afectos sin nada de efusión ni contacto físico. Todo lleno de matices y sugerencias, cada detalle tiene su importancia. Los sentimientos, pasiones, engaños y miserias del ser humano son iguales en cualquier punto de la Tierra.
        
            