
                      Ilustradora película de la ambición vivida en Wall Street que muestra, como en anteriores films se ha hecho, las tramas y fraudes llevados a cabo por dinero y adicción. Una gran interpretación y un guión excepcional. Quizás un poco larga, pero merece la pena.
        
            