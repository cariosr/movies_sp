
                      El ilusionista forma parte de las películas, que según yo, le hacen diferente, le hacen reflexionar, descubrir "el infierno del decorado" tal como es en esa película. No me esperaba a una película tan triste, que muestra realmente la destrucción de un mundo y de sus actores y el comienzo de uno nuevo...
        
            