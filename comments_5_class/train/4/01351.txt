
                      Terminó Harry Potter, y al visionar esta última entrega de las aventuras de nuestro mago favorito, dos pensamientos me vinieron a la cabeza. En primer lugar un poco de tristeza, ya que será extraño no poder ver más a la banda de aprendices brujos. Luego, una sensación de alivio, porque honestamente, comenzaba a ser aburridor y no muy bueno (en comparación con el libro). Dicho esto, la película es honesta, y el espíritu mágico sigue presente.
        
            