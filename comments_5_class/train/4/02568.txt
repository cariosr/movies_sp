
                      Baby Driver (2017).
Beats, muchos Beats.

El británico Edgar Wright, habituado al cine de acción mezclado con humor (especialmente negro), nos trae esta tragicomedia de atracos con protas monos, villanos conocidos (Jamie Foxx, Kevin Spacey...), coches rápidos y música, mucha música, y de la buena (rock, pop...).
Nuestro chico, el joven conductor que da título a la cinta, sufre de acúfenos desde la niñez y todo lo hace con sus auriculares puestos. Un portento al volante a quien persiguen los problemas, pero él tiene un plan...

Como al inicio decía, la película está repleta de Beats, y es que sus responsables han sido muy originales al rodar los tiroteos al ritmo de la percusión de la banda sonora (quizá la compre; es genial). Sin duda, el filme es recomendable para los amantes de la acción, las buenas persecuciones de bólidos y las canciones con mucho ritmo.

Raúl Cabral.
        
            