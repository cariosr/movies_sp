
                      Típica película brillante pero hueca, aunque está presa de su propia artificialidad. Aunque 
la música, las actuaciones, la brillante estética y esos salvajes estallidos de violencia hacen que este film sea difícil de olvidar.
        
            