
                      Película de una trama aparentemente sencilla de, grandes pausas y argumento .Sorprende sin duda por su desconcertante desenlace .De reparto impecable como la interpretación de los personajes que se nos ofrece .música mística a veces casi sacra , increíblemente bien adaptada a sus escenas y gracias ala cual aun logra mas nuestra emotividad sobre todo en su final ,tal vez no se adapta a la gran pantalla por su falta de paisajes o gran fotografía pero ,es una película que sinceramente merece ser vista .Un film altamente interesante.
        
            