
                      Hasta ahora, el maravilloso mundo de Jay Ward, cuna de mil sábados mágicos de baby boom de crujientes animaciones, personajes extraños y juegos de palabras maravillosamente agrietados, ha sido una tierra obstinadamente extranjera para los exploradores de Hollywood.

¿Recuerdas las versiones en pantalla grande de "Boris y Natasha", con Sally Kellerman y Dave Thomas? ¿Brendan Fraser como "Dudley Do-Right" y "George of the Jungle"? ¿Robert DeNiro como líder intrépido en "Rocky & Bullwinkle"?

Sí, lo sé. También he estado tratando de olvidar.

Es por eso que "Mr. Peabody & Sherman", basado en la caricatura deliciosamente tonta de Ward sobre un perro parlante muy intelectual, su chico geek y sus viajes a través de la historia por la máquina del tiempo, es una sorpresa agradable, en su mayoría fiel.
        
            