
                      La película está llena de buenas intenciones y los más pequeños disfrutaran con ella, pero no será una película que acabe teniendo un poso en nuestras memorias. Acaba siendo una película más infantil de los que nos prometían y eso acaba pesando a los adultos que todavía disfrutamos de cuentos de niños.
        
            