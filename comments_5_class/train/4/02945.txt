
                      A estas alturas, la verdad contundente sobre la secuela del campeón mundial de taquilla (Parque Jurásico) es clara: The Lost World es una verdadera perversión. Además de la proeza técnica de los mejores dinosaurios debido a los avances en modelos y computadoras, la película épica de Steven Spielberg, de $ 73 millones, es descuidada, sádica, frenética, sin encanto, sin trama, derivada y carente de sorpresa y personajes que importan un bledo.

¿La inferioridad del original de 1993 impedirá que la película supere los récords de taquilla del verano y el año? Diablos, no. El bombo ha penetrado en nuestros cráneos empapados de calor: ¡tengo que ver esos dinosaurios!
        
            