
                      MASTODÓNTICA Y BIZARRA AVENTURA VISUAL.

A finales de los años setenta, el realizador australiano George Miller (junto con un joven y desconocido Mel Gibson), confeccionó un infernal retrato de un país australiano  estéril y dominado por las bandas, pasándolo por el filtro de la ciencia ficción apocalíptica, y que con el tiempo se convirtió en todo un clásico de culto.

Esta nueva entrega, titulada "Furia en la carretera", nos llega veinte años después de la esperpéntica tercera parte (de hecho lo único rescatable es la canción de Tina Turner), y nos trae a un solitario Mad Max perseguido por su turbulento pasado; no obstante, se ve arrastrado a formar parte de un grupo que huye a través del desierto en un camión de guerra conducido por una Emperatriz de élite: Furiosa. Escapan de una Ciudadela tiranizada por Immortan Joe, a quien han arrebatado algo irreemplazable. Enfurecido, el Señor de la Guerra moviliza a todas sus bandas y persigue implacablemente a los rebeldes en una "guerra de la carretera" de altas revoluciones...

Miller ha resucitado la saga que él mismo creó y le puso en el panorama cinematográfico, actualizándola a los cánones que rigen en la actualidad, pero manteniendo la esencia de sus dos primeras entregas: el espíritu rebelde y macarra. 'Fury Road' nos hace sentir en nuestras carnes el árido polvo de sus anaranjados desiertos, la tensión de las adrenalíticas persecuciones, explosiones, y la acción pura metida en vena... todo en ella es mastodóntico y bestial; sin embargo, no se queda solo en su espectáculo visual, ya que posee corazón y sentimientos, con un trasfondo que subyaga el tema sobre el maltrato a las mujeres.

Cabe destacar el alto nivel de las interpretaciones, sobre todo de su dúo protagonista, que a pesar de la sonada ausencia del carismático Mel Gibson, el rudo Tom Hardy le sustituye de forma notable, encarnando de maravilla al antihéroe Max Rockatansky; aunque es Charlize Theron la que se lleva el gato al agua, estando realmente inmensa y genial como la Emperatriz Furiosa, siendo está una de sus mejores interpretaciones en su vasta carrera.

También destaca una factura técnica alucinante, una fotografía colorista que dota de vida cada fotograma, una banda sonora potente (y a ratos hasta divertida) ya que los tamborileros blancos y el delirante guitarra escupe fuego (que personalmente me ha recordado a esa desenfadada banda del club de la 'Teta Enroscada' de la cinta 'Tarantiana', "Abierto hasta el amanecer") son parte del desquiciado grupo del señor de la guerra.

Por ponerle alguna traba (eso sí, pequeña), quizás sea que el guión es un tanto sencillo (aunque solvente), pero siendo una cinta de evasión y espectáculo, tampoco hay que marear tanto la perdiz, y por consiguiente, esta nimiedad no consigue empañar la sensacional experiencia de disfrutar de esta gran película.

En conclusión, "Mad Max: Furia en la carretera" es una película impresionante, que revitaliza una saga que se había reducido a su estatus de culto, dándola a conocer a las nuevas generaciones, y que personalmente espero que sea la primera de una nueva, estimulante, bizarra, y espectacular saga post-apocalíptica.
        
            