
                      Infiltrado (2016).
El gran Bryan Cranston protagoniza este thriller de narcos ahora que tan de moda está el tema (que hasta en la publicidad de nuestras ciudades nos felicitan las fiestas). Nuestro inteligente y arriesgado topo contará con la ayuda de compañeros encarnados por el colombiano John Leguizamo y la alemana Diane Kruger.

La película está basada en hechos reales (aparece hasta el famoso Pablo Escobar) y cuenta los hechos acaecidos en los años 80's sobre esta importante operación antidroga.

La historia está bien y cuenta cómo se detuvo desde narcos al uso hasta importantes banqueros blanqueadores de dinero, pero a veces se pierde uno un poco en los entresijos de dicha historia.
No obstante, donde esté Bryan Cranston hay calidad asegurada...

Raúl Cabral.
        
            