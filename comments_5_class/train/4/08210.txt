
                      La vida de un joven matrimonio se ve totalmente alterada después de que un conocido del pasado del marido entre en ella y comience a dejarles misteriosos regalos y se revele un horrible secreto de la infancia.

Con esta base de thriller y dando saltos de guión continuos y muy acertados, Joel nos lleva desde la misteriosa vida actual del conocido y de la pareja a la pasada, volviendo al presente, indicando cómo los errores del pasado vuelven sistemáticamente a repetirse sin duda hoy y son parte de nuestra vida si no les ponemos coto y cómo no es oro todo lo que reluce.

Me parece una ópera prima de alto calado, con unas interpretaciones muy por encima de lo esperado, con un guión que te lleva y te trae con giros inesperados y de gran tensión, una dirección que desde luego indica que además de gran actor, hemos descubierto en Edgerton un proyecto de extraordinario director.
En definitiva, muy buena película.
        
            