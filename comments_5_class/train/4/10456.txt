
                      Esta bonita película de animación francesa es una auténtica maravilla! A través de un bonito grafismo y con gran elegancia, esta maravillosa película posee un innegable talento educativo para llegar a un público multigeneracional. Los niños, es de esperar, verán en ella una buena motivación para descubrir el mundo de la lectura. Y los adultos estarán encantados con su toque nostálgico. Los personajes, sencillos, muy bien diseñados e interpretados con precisión por los actores, se mueven con gracia por una historia simple, pero mágica.
        
            