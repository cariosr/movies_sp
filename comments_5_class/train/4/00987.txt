
                      La sonrisa de la Mona Lisa es un drama dirigido por Mike Newell, director de 'La máscara de hierro', 'Cuatro bodas y un funeral' y 'Harry Potter y el Cáliz de Fuego'.

Katherine Watson (Julia Roberts) llega como profesora a la prestigiosa universidad de Wellesley, en Nueva Inglaterra, para enseñar historia del arte. Al contar con las mejores estudiantes del país, Katherine espera que aprovechen sus estudios para tener una buena carrera, y un futuro brillante. Nada más llegar, se da cuenta de lo equivocada que estaba, y de que la universidad educa a la jóvenes únicamente para ser unas buenas esposas y amas de casa.

La historia sigue la misma linea que la de 'El club de los poetas muertos'; un profesor con una forma innovadora de enseñar, que consigue conquistar el interés y el cariño de sus alumnos.

Basada en los años 50, la película refleja la mentalidad que se tenía sobre el papel de la mujer en la sociedad. Es una clara representación del machismo existente en aquellos años, ya no sólo por parte de los hombres, sino también de las mujeres.
El reparto está encabezado por Julia Roberts, Maggie Gyllenhaal, Julia Stiles, Kirsten Dunst; en la que puede que sea una de sus mejores actuaciones, y Ginnifer Goodwin.
Los personajes de Roberts y Stiles destacan especialmente, a pesar de ser muy diferentes, ambas siguen el camino que han elegido ellas mismas, lo que es muy inspirador.
        
            