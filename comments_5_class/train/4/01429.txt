
                      Una comedia animada refrescante, las historias del panda fan de kung fu regresan, más sombrías, puede ser que menos tontas pero con más acción. Por supuesto la película continúa siendo cómica y las bromas logran su objetivo. ¡Humor escolar con la voz de Manu Payet que queda a la perfección! He aquí un buen dibujo animado con ritmo suficiente. ¡Excelente película!
        
            