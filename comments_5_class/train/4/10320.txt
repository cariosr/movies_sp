
                      Para la mayor parte de las actrices que han alcanzado la cima en su juventud, el tiempo es un tren de alta velocidad que no se detiene: el paisaje se contempla en su verdadera esencia solo en un fracción de segundo, todo lo demás es recuerdo, memoria fragmentada: tal vez un sueño, una ficción orquestada en un papel por una brillante mente ajena. Un día se miran al espejo y su mirada se tropieza penosamente con un rostro envejecido, una pésima caracterización de alguna torpe aprendiz de maquillaje. Ese estatus de diosa terrenal se desvanece como polvo lanzado al viento. Entonces reaparece su verdadera naturaleza terrenal, su verdadero yo: la inseguridad, el desasosiego, los temidos miedos…
   La naturaleza juega un papel destacado en la película, no sólo por la belleza del paisaje, sino como un elemento catalizador y simbólico: la nube de Maloja representa la teoría del Eterno Retorno de Nietzsche. María deberá reencontrarse nuevamente con la obra que la llevó a la fama, pero ahora no será la joven seductora, será su víctima.

Leer crítica completa en:
        
            