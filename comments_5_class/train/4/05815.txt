
                      ¡Increíble película! A pesar de ser una película sencilla sin mucho hilo argumentativo, James Franco borda el papel de Aron Ralston haciendo que la película sea completamente creíble. Quiero destacar dos elementos mas: el guión, me parece muy bueno porque es muy real, es decir, las conversaciones parecen espontáneas más que guionizadas y también me gustaría destacar el trabajo de filmación porque es alucinante. Sin duda, un gran trabajo de Danny Boyle.
        
            