
                      La segunda entrega de Guardianes de la Galaxia mantiene todo el frescor, ironía, efectos especiales y acción en igual armonía que lo tuvo en la primera. Marvel se ha expandido de una forma tremenda en el cine y se ha situado en la primera marca de súper héroes con grandes puesta en escena en la gran pantalla.

En esta ocasión, y como se adelantó en los tráiler, ya se ve una cercanía total a los comics, siendo esta la última formación de los Guardianes, de llevar misiones a cabo de lo más variopintas, con una inclusión en las vidas personales Peter Quill-Star Lord (Chris Pratt) al descubrir a su padre Ego (Kurt Russell) y más de su extraordinario origen; y por otro lado de Gamora (Zoe Saldana) y su relación con su hermana  Nébula (Karen Guillan

Guardianes no defrauda a nivel de historia, humor, espacio, y mantiene una acción trepidante desde el primer momento hasta el último. La gran inclusión en este grupo ha sido descubrir a Bebe Groot, que hace las delicias de todo el público por su inocencia y gracias a la hora de hacer cualquier cosa, y en el que sigue sorprendiendo ver en esta faceta de doblaje original a Vin Diesel, que ha querido seguir vinculado así a los Guardianes, aunque sea solamente diciendo de mil forma “Yo soy Groot”.

También la incorporación de un personaje de los comics “Stakar Ogord” que corre a cargo de Sylvester Stallone, y que deja abierta cualquier posibilidad (según las escenas de los títulos de crédito) de otras participaciones, o series paralelas; en cualquier caso su personaje “Halcón Estelar” fue incluso uno de los primeros miembros en los comics de Guardianes de la Galaxia.

En cuanto a las historias que tiene abiertas Marvel y conexionadas en su universo, no desvela nada de las Gemas del Infinito, tendremos que esperar a saber algo más a la próxima entrega de “Thor Ragnarok”; y  sobre todo en la siguiente participación de Guardianes de la Galaxia que será en 2018 en “Vengadores: Infinity War” en la gran batalla de superhéroes contra Thanos, un gran villano que pretende controlar el universo adueñándose de las Infinitas Gemas; donde participarán “Guardianes” junto “Los Vengadores” y otros grandes héroes como Doctor Strange, Pantera Negra y Spiderman, entre otros.

Mucho, más y mejor… Larga vida al Universo Marvel!!

Calificación: 🎥🎥🎥🎥🎥🎥🎥🎥 (8 sobre 10)
        
            