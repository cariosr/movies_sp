
                      Es difícil encontrar una película de Pixar (aunque algún borrón hay) que no sea un prodigio de la animación, con un poso argumental bajo la superficie y personajes con el carisma suficiente para ser iconos de este tipo de cine. Siguiendo esa senda ganadora, "Los increíbles" es una película divertida, trepidante, con una estupenda animación y una ambientación entre héroe de cómics o espionaje tipo James Bond que resulta de lo más entretenido.
        
            