
                      No es la fiesta de queso cínica y de cash-in que temiste. OK, Jurassic World es un poco de eso. Pero este dino épico de vanguardia también es más que una explosión de diversión estruendosa, rugiente, "¿ya viste eso?". Tiene una veta perversa de actitud subversiva que se conoce con el nombre de Colin Trevorrow. Es el director y coguionista cuya única característica anterior de crédito, un indie ingenioso de 2012 llamado Safety Not Guaranteed, costó $ 750,000, cambio de zoquete en un producto de estudio como este, que costó, espere, $ 150 millones.
        
            