
                      El guión mantiene un buen ritmo durante toda la película, lleno de gags que aunque no arrancan carcajadas si hacen que mantengas una sonrisa en la cara durante todo el film, mientras contemplamos el presente y pasado de Jasmine a la vez que nos preguntamos que ocurrirá con su futuro. 
En definitiva, "Blue Jasmine" es uno de los títulos del año y una comedia que no puedes dejar escapar.
        
            