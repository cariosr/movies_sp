
                      Coproducción austriaco germana.
No es del todo brillante pero es distinta .Habla de un tema altamente explotado de un modo muy, muy diferente .En apariencia narra la historia de unos judíos del Holocausto encerrados en un campo de concentración .Es sin duda una película osada y valiente. 
Puntualizar su impecable dirección de actores y buena fotografía.
En este film se refleja de un modo muy peculiar ,la lucha del ser humano para sobrevivir ,hacen de salvar la vida todo un ”arte” .Opino que es una merecida ganadora en los Oscar del 2007 por varios de sus puntos aunque en ciertos momentos resulte algo lenta y no trasmita toda la emoción esperada.
Si caemos en la cuenta de que “los falsificadores” esta basada en hechos reales y el bajo presupuesto con el que contaron para su realizaciòn , diría que con sus pros y contras es ,sin lugar a dudas,una buena película de habla no inglesa ,con claros objetivos e ideas directas que finalmente la hacen muy recomendable.
        
            