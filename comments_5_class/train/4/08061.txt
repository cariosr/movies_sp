
                      Desde el sofá de casa.
Cuando llega una película como esta, el año anterior a su estreno, meses y días antes crea mucho revuelo y expectativa. Como sucesora de una gran saga, el listón esta alto. 
Desde luego la película a mi entender es una película algo larga. La historia es la misma que la de primera. Pero con mejores efectos, mejores animaciones, guión parecido, niños y algo que no tiene la primera, la chispa de interacción entre una guapísima actriz y actor. 
Divertida, entretenida, muy buenos efectos, y banda sonora, junto a un dinosaurio que hace de  malo, inteligente y con todas las armas de la biotecnología, armas muy letales, el Indominus-rex. 
Y claro esta al final un guiño a la primera de la saga, el T-rex.
Altamente recomendable.
        
            