
                      Popurrí de géneros, (drama, terror, ciencia ficción, acción), bien resuelta. En ocasiones se puede llegar a pensar que es compleja pero para nada, se entiende perfectamente. Con un final potente y con imágenes muy buenas pasaras algo menos de 1h30m entretenidos y con dudas sobre los sentimientos del ser humano. Buen comienzo el de su director.
(Pena me ha dado no haberle podido hacerle ninguna pregunta en el preestreno, eso es lo que tienen las prisas..., lástima).
        
            