
                      Simplemente genial. Una muy digna continuación de la saga, con momentos buenísimos, donde toda la trama está centrada en un viaje en el tiempo al pasado, y lo que ello significa en una peli. Un auténtico follón temporal que habrá que enderezar.

Si les gustó las anteriores, esta es imprescindible.
        
            