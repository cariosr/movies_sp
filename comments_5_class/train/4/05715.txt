
                      Todo va creciendo con naturalidad hasta el viaje iniciático de la pareja donde el afecto se expresa hasta su culminación carnal. Todo está envuelto de una sobriedad creíble y sin ningún tipo de subterfugio ni artificio. Muy cercano a poder considerarse naif o tediosa pero sin traspasar los límites en ningún momento.
En "Carol" encontraremos una previsible historia de amor que consigue fascinar desde el primer minuto hasta el último, sin engaños, con una solidez basada en las dos protagonistas y, además, en un guión sin fisuras, una ambientación perfecta y una fotografía que completa un tono emotivo que conmueve y trasciende. (7,5 / 10)
        
            