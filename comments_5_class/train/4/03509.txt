
                      Hacía tiempo que no nos llegaba por semana santa una nueva historia bíblica y este año ha llegado en forma de superproducción Noé.

Dirigida por Darren Aronofsky y con un reparto de lujo formado por Russel Crowe en el papel de Noé, Jennifer Connelly, Emma Whatson, Anthon Hopkins, y Logan Lerman entre otros. Noé cuenta la historia ya conocida del diluvio universal y del arca encargada por El Creador a Noé donde debe salvaguardar una pareja da cada especie de animal que viva sobre la faz de la tierra dejando fuera de ella a todo ser humano pecador.

Pero la película va más allá. Personalmente conocía poca cosa más de lo que acabo de decir pero en el film nos cuentan más, como quienes son los guardianes, unos ángeles caídos a la tierra y trasformados en roca, nos cuenta el drama personal y familiar que vive Noé y las dudas que le provocan las duras y difíciles decisiones que debe tomar sin tener muy claros según que mensajes, y también nos muestra lo mal que se tomaron la noticia del diluvio los seres humanos condenados a morir y decididos a luchar con uñas y dientes para conseguir un pasaje en esa arca.

Russel Crowe vuelve a estar soberbio en su papel de Noé, Emma Whatson está más que bien y está demostrando que puede tener mucho futuro como actriz, y Jennifer Connelly tiene tablas más que de sobra para este papel y no decepciona. Logan Lerman y Emma Whatson vuelven a encontrarse en esta película tras Las ventajas de ser un marginado, lástima que aquí sus personajes no tengan tanta interacción y puedan demostrar la buena química que mostraron en esa otra película.

Película cargada de valores humanos y cristianos, donde realmente te hace dudar si nos deberíamos extinguir o no, ya que visto las atrocidades que cometieron los descendientes de Caín, los actuales seres humanos civilizados vamos mucho peor que ellos y no hemos variado el rumbo demasiado. Ya sabéis que la historia se repite siempre pero cambiando las herramientas que utilizamos. 

Toda la historia está aderezada con una excelente banda sonora original a manos de Clint Mansell que aumentan la emotividad de cada momento épico.

Quizás la pega que le puedo sacar es que es más fantasiosa de lo que me esperaba. No os esperéis sutilezas. 