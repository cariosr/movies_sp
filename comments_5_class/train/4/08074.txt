
                      Genialidad del género que lanzó al reconocimiento al talentoso Director James Wan. Una gran demostración que como realizar una entrega de terror con poco presupuesto y en tan solo 18 días. Tiene una narrativa impecable que te mantiene en vilo de comienzo a fin, y dejando de lado algunos clichés en los guiones, es una película que le puede dar batalla a grandes títulos en el rubro. Eso sí, aclaro que no es para estómagos sensibles, tal como lo especifica la descripción del sitio.
        
            