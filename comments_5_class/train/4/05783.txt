
                      Acción inmersa en un contexto ficticio pero repleta de mensajes y problemas reales  a la "orden del día ". La rabia y desesperación interpretada por Charlize Theron traspasa la pantalla. Sin pasar por alto la increíble producción, los impresionantes efectos y el espectacular montaje que trasladan al espectacular a ese supuesto futuro destrozado donde la lucha de la mujer adopta un papel protagonista . Sin duda " la mejor mad max " hasta el momento.
        
            