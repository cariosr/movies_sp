
                      Acudí a verla porque me gusta el cine de intriga y aunque en la primera hora me defraudó un poco, poco a poco fue a más, creando la tensión y la atención del espectador sobre la trama asesina. El resultado final fue satisfactorio.
        
            