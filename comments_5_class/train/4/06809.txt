
                      Una interesante e inquietante historia muy bien contada. La fotografía es excelente, y no es de extrañar, ya que Ceylan es fotógrafo, o al menos eso tengo entendido. Las atmósferas que genera y el ritmo con el que se desarrollan están perfectamente casados. Muy recomendada.
        
            