
                      La muerte os sienta tan bien, es una comedia fantástica dirigida y producida por Robert Zemeckis, director y productor también de The Polar Express y director y guionista de la trilogía Regreso al Futuro.

La película narra la historia de dos mujeres que se pelean por el mismo hombre. En esa lucha, descubren el elixir de la juventud, que no solo las mantiene jóvenes y bellas, sino que las hace inmortales.

Esta comedia protagonizada por Bruce Willis, Meryl Streep y Goldie Hawn, fue una crítica del director hacia la cirugía plástica, que estaba en su momento de eclosión.

Lo más destacable podría ser la actuación de Bruce Willis, acostumbrado a ser el 'tipo duro', esta vez encarna a un cirujano plástico acabado, controlado y humillado constantemente por su actual mujer.
Meryl Streep y Goldie Hawn, en sus papeles de mujeres vengativas e interesadas, crean prácticamente solas la comedia.

Los efectos especiales son asombrosos, muy bien conseguidos y utilizados.
El desenlace que, sin perder el humor, mantiene al espectador en tensión, pone la guinda perfecta a la película.
        
            