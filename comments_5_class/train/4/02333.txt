
                      Sin lugar a dudas es un trabajo gráfico digno de Pixar, sus contraluces, sombras el manejo de los elementos, viento, lluvia y en general la calidad y realismo de sus paisajes son alucinantes. Sin embargo el tema es demasiado Disney, absolutamente predecible y llenos de clichés. Ha perdido la magia de Toy Story, Monster Inn, Los Increíbles o Ratatouille.
        
            