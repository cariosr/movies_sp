
                      Parece que a la gente que ha leído los libros le gusta. En ocasiones el lenguaje usado debido a la naturaleza de la trama dan un poco de estupor. Pero dentro de lo difícil de una película así, te absorbe durante dos horas enteritas.
        
            