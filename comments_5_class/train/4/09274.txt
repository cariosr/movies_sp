
                      Película homenaje de Wim Wenders a Pina Bausch, coreógrafa prematuramente desaparecida en 2009, esta obra muy original en 3D está bailada por la compañía del Tanztheater Wuppertal, y nos permite descubrir el arte singular de esta gran dama.
        
            