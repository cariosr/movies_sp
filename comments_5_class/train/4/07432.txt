
                      Humor negro!
No es un filme para "cerebritos" ni para quemarse las neuronas, ya que la historia muestra una situación de adversidad llevada con buen humor.
La actuación de Matt Damon es destacada y su personificación es muy buena, pero por partes provoca algo de dispersión por lo que en mi opinión debió haber sido un poco más corta.
        
            