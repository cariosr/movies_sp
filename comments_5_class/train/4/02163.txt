
                      Película romántica sin caer en el melodrama que gustará a los amantes del género. Los actores bien en sus papeles pero sin ser actuaciones sobresalientes. Lo mejor guión y dirección. Un estudio de los recovecos del alma humana bastante bien conseguido y convincente.
        
            