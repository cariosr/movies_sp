
                      Domingo por la tarde, primera sesión, en la sala habíamos unas diez personas.
Una película española que no habla de la Guerra Civil, lo que ya es un mérito por sí. Una película atrevida, valiente, tratándose del cine de género. Sin abusos de los efectos especiales, ni decorados artificiales, ni vestuarios futuristas, quizá sea eso lo que le dé credibilidad.
Sorprende la lógica interna que convierte a la memoria de los robots (o lo que sea) en hielo ingrávido, divierte la autonomía felina de la mascota del protagonista y resulta divertido ver a Lluís Homar transformado en una 'thermomix' que ríe. El universo de 'Eva' funciona como un reloj que funciona.
Una película bonita y triste en la que te afecta tanto el desasosiego y la incertidumbre de los humanos como la de los robots. Donde el paisaje, llega a ser un protagonista más de la historia. La niña Claudia Vega, un descubrimiento en el cine español.
Buena, emotiva, un cuento de ciencia ficción, te mantiene atento a la pantalla. El único fallo, que quizá sea demasiado predecible.
        
            