
                      Enredados, la nueva película de disney
Es, sin duda, la más divertida historia que ocupa nuestras carteleras. Pasa un inolvidable fin de semana con familia o amigos.

.Opinión breve sobre la película:

Walt Disney , siempre es sinónimo de magia, fantasía, luz, color y sueños .Aquí está su nuevo film, el cual, por sus ingredientes, promete convertirse en su clásico numero cincuenta.
Una gran producción para todos aquellos seguidores del género de animación.
Una vez más, con ‘Enredados”, esta gran factoría de ficción cumple con todo lo esperado.

.Rapunzel, nuestra protagonista:

Es una intrépida princesa distinta a cuantas existieron, más valiente y mucho menos “damisela en apuros” de a lo que nos tienen acostumbrados, con una hermosa melena de cien mil pelos individuales, los cuales usa como defensa y solución en sus muchas hazañas.

Otros personajes:
El reparto de personajes de la cinta es de lo más peculiar. Os presento a algunos de ellos:
Flynn Rider : un encantador y guapísimo ladrón que encandilará a más de una.
Pascal : un divertidísimo camaleón de ojos saltones y un carácter tremendo para tan pequeño tamaño.
Maximus : un curioso caballo sabueso de gran envergadura y alzada.
Estos son, entre otros muchos, los amigos llenos de vida que conoceremos.

. Sinopsis de enredados
Erasé una vez…Un encantador y atractivo bandido del reino, llamado Flynn Rider, huyendo, va a parar por casualidad a una misteriosa torre, donde es secuestrado por Rapunzel, preciosa, alocada, vital y desmelenada adolescente con una cabellera dorada de 21 metros de largo, que está encerrada en ella. Nuestra hermosa amiguita, buscando el modo de salir de donde ha estado recluida durante tantos años, llega a un curioso trato con el guapo ladrón, que les hará vivir mil y una aventuras sin igual.

.crítica :
Una comedia romántica y musical.
 “Enredados”,  embelesa y enamora hasta la médula. Durante esas escasas dos horas de duración, logran hacernos olvidar que afuera hay un mundo, un tanto más teñido de grises y terracotas, que el ofrecido en la gran pantalla a todo color y, en tan impactantes dimensiones. “Rapunzel” llega, tanto a mayores como a los más pequeños de la casa, logrando la carcajada natural, facil y auténtica. Para toda la familia.

.Sobre la b.s.o:
La banda sonora está, ni más ni menos, que compuesta por el compositor Alan Menken, ganador de 8 oscars que pueden convertirse en 9 en unos días, ya que una de las canciones de la película está nominada en la gala de este año. Gracias a su trabajo en Disney de la talla de La Sirenita, Aladdin, Pocahontas o La bella y la bestia, se ha convertido en todo un mito. 

.“Tangled” Enredados', la película de animación de más éxito, en su 
estreno, en la taquilla española: 
Con una recaudación de cuatro millones de euros en su primer fin de semana, en las pantallas de nuestro país, fue la gran vencedora. Sigue arrasando, colocándose como la segunda película más vista en la taquilla internacional. Ha doblado las expectativas que los productores esperaban.
        
            