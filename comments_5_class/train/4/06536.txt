
                      Belleza sublime en una oda a la poesía de los textos de Lorca. 'La novia' nos sumerge en un mundo casi onírico, de paisajes crudos y viejas disputas, con una Inma Cuesta espectacular que nos hace olvidar el poco acierto del film como adaptación de 'Bodas de Sangre' (poco arriesgada en comparación con el resto de la película). Y, aunque a veces se recree en exceso en su propia estética, embruja.
        
            