
                      Marte (2015).
El superviviente de los hermanos Scott vuelve a sorprender con esta pequeña joya espacial mezcla entre Planeta Rojo y Náufrago (con Tom Hanks pasando desdicha tras desdicha). 

En esta ocasión es Matt Damon quien lo pasa mal en el planeta vecino por culpa de una fatídica tormenta (imposible en Marte, aunque creo que el bueno de Ridley merece esa licencia). En cuanto al resto de hechos y eventos narrados en la película son todos posibles según la colaboración de la NASA, de hecho están magníficamente elaborados, y aunque se trata de una cinta de menos acción que otras de este extraordinario realizador, la historia nos entretiene a lo largo de sus más de dos horas de metraje. Por cierto, los amantes de la música de los 80's están de enhorabuena si van a verla. ;

El reparto de artistas secundarios es magnífico; desde la omnipresente (últimamente no para en Hollywood), Jessica Chastain, pasando por los veteranos Jeff Daniels o Sean Bean (a ver si descubrís el guiño que le hacen a éste hacia mitad de la película ), hasta Chiwetel Ejiofor (Plan Oculto). 

Muy recomendable para el que gusta disfrutar de buenos guiones, y para, como no, aficionados y profesionales de la ingeniería como mis amigos Javi, Armando o mi sobrino Jaime. ;)
Raúl Cabral.
http://www.filmaffinity.com/es/film846099.html
        
            