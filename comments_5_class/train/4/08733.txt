
                      ¿Mejor drama del año? Por lo menos,candidata. Alexander Payne demuestra cómo sin excentricidades,desnudando los sentimientos de una familia que se puede considerar típica,se puede conmover. Muestra de forma excepcional,mérito que debe compartir con Clooney (genial),el absurdo y crueldad que a veces encierran las relaciones personales. Sensatez y pasión,dos caras de la misma moneda. Muy recomendable
.
        
            