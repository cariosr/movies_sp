
                      Es otra de las obras en las cuales Elia Kazan a través de Marlon Brando, intenta justificar la acusación contra antiguos compañeros suyos del partido comunista. Desde el odio que le tengo al director tengo que reconocer que es una de esas películas imprescindibles para cualquier aficionado del cine, aunque reitero que le falta imparcialidad...
        
            