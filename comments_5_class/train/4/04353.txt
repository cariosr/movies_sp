
                      Fotografía de la vida en la campiña de la Borgoña. Una familia entre viñedos donde se reúnen tres hermanos alrededor de las tierras familiares y que sirve de vehículo para ver cómo es la campaña de la vendimia, las costumbres de la zona y, sobre todo, el paisaje y la vida. Retrato lleno de nostalgia y sensibilidad donde la química entre los hermanos es parte crucial del film. Con este toque elegante que solo los franceses saben dar, esta dramedia resulta entretenida, amable y bonita.
        
            