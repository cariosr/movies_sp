
                      Spielberg nunca defrauda, así ha sido y así sigue siendo. Se agradece que aún se hagan películas que ayuden a entender y descubrir momentos de la historia que sin el cine jamás sabríamos. Película de hechos reales que en algunos casos puede llegar a ser un poco lenta pero merece la pena verla. Yo disfruté con ella
        
            