
                      Tres metros sobre el cielo, taquillera producción española basada en el libro  del autor Federico Moccia.
De gran fotografía ambientación y música.
Apasionante y emocionante película que te hace sentir.
Su argumento atrapa al espectador inmensamente con aquellos recuerdos de adolescencia, el primer amor... etc. Mario Casas está francamente bien en las escenas de acción, sublime, logra caracterizar al personaje del libro a la perfección, al igual que Maria Valverde que esta genial en la película. Felicidades por tan gran trabajo, altamente recomendable muy sinceramente en gran pantalla.
        
            