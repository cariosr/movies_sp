
                      A Django desencadenado le sale Tarantino por todos sus poros. A Foxx, el papel, le viene como anillo al dedo. Christoph Waltz, para mi gusto un gran descubrimiento, ya me encantó en Malditos Bastardos. 
Acción, venganza, sangre, ironía y sarcasmo. Si te gusta Tarantino, te gustará Django.
        
            