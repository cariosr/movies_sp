
                      ¡¡Muy divertida!! Es una película que se disfruta y no sólo para niños. El guión es ingenioso y con muchas bromas. El apartado técnico, como siempre, muy bueno (DreamWorks). El doblaje de Antonio Banderas le va como anillo al dedo al gato, sacándole un partido inmejorable. Merece la pena verla en el cine!!
        
            