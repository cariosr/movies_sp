
                      Es una de las mejores comedias románticas de este año. Es divertida, enternecedora, y cuenta con un súper casting. Olvídense de las comedias románticas ñoñas que vemos el 90 % del tiempo. Crazy Stupid Love está un paso adelante, y eso hace toda la diferencia.
        
            