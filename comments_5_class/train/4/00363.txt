
                      Dreamworks dio en el clavo con el carismático ogro de la ciénaga y su aventura para salvar a una princesa presa en un castillo a cambio de volver a recuperar su tranquilidad en dicha ciénaga. Divertida, con numerosos guiños y parodias de los cuentos clásicos y aunque la animación con los años se ha quedado algo desdibujada (especialmente con los seres humanos), sigue siendo muy entretenida y disfrutable
        
            