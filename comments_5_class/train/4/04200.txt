
                      Una película esperanzadora y dura a la vez. Personalemente me enfado mucho con las injusticias y las cosas que considero que no son como deberían ser, y la constante lucha de la madre y  la hija en la película hace que le pille mucha manía a todos los que se lo quieren poner difícil. El contexto de la película y sobre todo de su producción son todavía más impresionantes que el film en sí.
        
            