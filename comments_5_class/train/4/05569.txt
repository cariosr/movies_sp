
                      Danny Boyle, en su anterior largometraje Slumdog Millionaire, ya consiguió sorprenderme y agradarme, y ahora con 127 horas lo ha vuelto hacer.

Apoyado o reforzado en una banda sonora impactante y directa, consigue llevarnos por donde él quiere, cuando él quiere. Con un estilo o montaje poco convencional, como nos tiene acostumbrados, con momentos de inmenso ritmo, y otros de gran impacto, que a algunos espectadores les hará apartar la vista de la pantalla. También hay momentos que nos invitan a reflexionar sobre nuestros hechos, los vividos, los que estamos viviendo y los que viviremos. 

Aquí está claro, que el protagonismo no se lo lleva James Franco, el protagonista absoluto es el cañón, pues el director nos lo muestra desde todos los puntos de vista posibles, desde el interior hasta desde la vista de un pájaro, con todas las inclemencias meteorológicas. 

Una obra de autor, sobre una historia increíble.
        
            