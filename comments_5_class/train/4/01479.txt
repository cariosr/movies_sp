
                      Corazones de Acero (2015). 
"Los ideales son pacíficos, la Historia es violenta". 
David Ayer, habitual del cine bélico, nos trae esta pequeña obra maestra antibelicista que nada tiene que envidiar a La Chaqueta Metálica o Platoon. 
Brad Pitt, quien lleva seduciendo admiradoras desde que apareciera hace un cuarto de siglo en Thelma & Louis, borda su papel de sargento como si del insuperable De Niro se tratara. Le acompañan un Shia LaBeouf en un registro alejado de lo que nos tiene acostumbrados. El reparto de secundarios es perfecto. Y el coprotagonista, Logan Lerman, un joven músico y actor (22 años) que aquí le da la réplica a Pitt, podría ser oscarizado sin ningún problema. La factura de la cinta es impecable como duro fue el rodaje y crudas muchas de las escenas que nos ofrece. 
Muy recomendable, aunque absténganse aprensivos.

Raúl Cabral.  
http://www.sensacine.com/peliculas/pelicula-218759/
        
            