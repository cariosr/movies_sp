
                      Aterriza con una perfecta introducción en voz en off para ubicar al espectador, una historia bien llevada, que engancha desde un principio, proponiendo un concepto de ciencia ficción hasta creíble, y resaltando unos efectos especiales de mucha calidad.

Según la veo, y habiendo visionado antes After Earth, veo demasiadas similitudes que me hacen sospechar, que fue antes, "el huevo o la gallina". Lo de los Flashbacks ya me termina de rematar.

La frase principal de la película, "ganamos la guerra y ahora tenemos q irnos", me acerca a la pantalla y hacia una trama, que concibo original, no así en detalles, ni mucho menos en actuación.

Tom Cruise desarrolla básicamente su papel, el de siempre, con un guiño a Mission Imposible en una escena con una moto, y esa cara de no haber roto un plato en su vida.

Después de un pequeño desarrollo, la noto quizás un poco lenta, romántica  y poética a momentos, y me pregunto, "no le pega mucho, ¿no?".
Asumiendo que es la manera de generar la historia, de avanzarla.
Destaco, por encima de todo, la música, es simplemente perfecta.

En la trama y el guión vislumbro misterio que conjuga perfectamente los tempos, acciones y personajes, construyendo una historia interesante y completa.

La aparición de Morgan Freeman da un giro a la trama, para hacer que el espectador se enganche completamente. Y entonces llega el impás, hacia la hora de película, donde carga el guión y la interpretación, el tema flojea, y se ven las carencias, pero ahí dejo al espectador que evalúe por si mismo.
        
            