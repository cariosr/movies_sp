
                      Una de la pocas películas que me han puesto nervioso. El niño sabe hacerte odiarlo y tener pena por el al mismo tiempo. Es totalmente diferente a esas películas donde los actores quedan atrapados toda la película en un sitio y termina cuando mueren o cuando logran salir.
        
            