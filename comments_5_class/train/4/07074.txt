
                      Ensoñadora poesía de lo cotidiano, naturalista y reveladora, Yara es una película tan sencilla y luminosa, como el hermoso valle de Qadisha (norte del Líbano) donde viven una joven huérfana de guerra y su abuela. La mayoría de los vecinos se han mudado a la ciudad, y los más viejos al descanso eterno del cementerio. Las casas abandonadas sucumben ante el empuje inexorable de la naturaleza, y el pueblo parece abocado a ser pronto, testigo mudo de un tiempo perdido en el olvido.
Los días transcurren con idéntica monotonía para la joven Yara: atender a los pocos animales que les sirven de sustento, lavar la ropa, tenderla al sol… No muy lejos de ella, sentada a la sombra de los árboles, su anciana abuela pierde su vista y sus pensamientos en la profundidad del valle: idílica postal repetida desde el principio de sus días.
Leer crítica completa en: 
https://elcinepormontera.com/yara-libano-2018-abbas-fadhel/
        
            