
                      Gringo (2018).
"Cuando quieres algo, tienes que ir a por ello. ¡Lo haces realidad!"
El actor, doble y realizador Nash Edgerton dirige a su propio hermano, Joel (estos días compartiendo cartel con J. Lawrence en Gorrión Rojo), en este thriller de acción con tintes de comedia, protagonizado en el papel principal por el actor negro David Oyelowo (Jack Reacher), por la belleza sudafricana Charlize Theron, y por el también sudafricano de raza blanca Sharlto Copley (lo vimos en Distrito 9) que encarna a un exsicario de lo más contradictorio y paradójico.

El filme se me antoja un 'mix' entre The Mexican (con Brad Pitt y Julia Roberts) y El Fuego de la Venganza (con Denzel Washington). La mezcla entre thriller, comedia negra y cinta de acción está tan bien llevada que parece estar dirigida por los mismísimos hermanos Coen, solo que es un cineasta australiano quien se encarga de esta labor.

Oyelowo, quien quizá aún no ha protagonizado ninguna obra de primera categoría, parece cambiar su tendencia esta vez, pues el carácter histérico (de lo más gracioso) que imprime a su personaje, consigue sacarnos varias carcajadas a lo largo de los 110 minutos de metraje, sin dejar el dramatismo y la acción que impera en la película. Especial mención a esta última, pues aunque son momentos contados, están muy bien rodados (atentos a los atropellos y accidentes de coche).

Como ya hemos citado, hay varios actores y actrices de reparto de auténtico lujo. Aparte de los ya nombrados, podemos disfrutar de las interpretaciones de Amanda Seyfried y Thandie Newton.

Resumiendo: muy interesante y entretenida, y por supuesto, muy recomendable gracias a una factura casi impecable.

Raúl Cabral.
https://www.filmaffinity.com/es/film594458.html
        
            