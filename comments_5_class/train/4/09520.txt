
                      Impresionante Meryl Streep, le deberían haber dado el Óscar, también magnífica Julia Roberts, así como el resto de secundarios. Impresionante fotografía que recuerdan a los angustiosos paisajes de Hopper. Muy recomendable película con diálogos que provocan una sonrisa amarga
        
            