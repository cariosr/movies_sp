
                      El cuento de la princesa Kaguya

,hoy les quiero recomendar esta hermosa película japonesa de animación del director Isao Takahata.
Los dibujos son hermosos, la música te envuelve y el cuento es muy interesante.

Una pareja de campesinos crían una niña que nació de un bambú, este bebé crece rapidísimo y es súper inteligente, el papá campesino está seguro que es una princesa que la naturaleza le envió y que debe de tratarla y vivir como tal y no como una campesina, decide llevarla a la ciudad y construir un palacio, educarla y obvio casarla con alguien de la alta. 

Yo la vi con mis hijos de 8 y 10, les encanto, muchas preguntas sobre la cultura y mucha reflexión sobre porque los padres tomaron esas decisiones y sobre la felicidad etc etc.

Pienso que niños un poco más chicos la pueden ver simplemente por la belleza del diseño y la música, la primera parte es muy simple y agradable, la segunda interesante y la última un poco más complicada. 

Espero les guste
        
            