
                      Esta película me ha cautivado, Mariano Cohn y Gastón Duprat vuelven a acertar!. Los diálogos, el resentimiento como hilo conductor, la burlesca hacen de este film imperdible. 
"Como escritor no abandono la lucha, sigan así, sigan igual, que aquí nunca cambie nada. Sigan siendo una sociedad hipócrita, ignorante y orgullosa de su propia brutalidad. Siento haber interrumpido sus apacibles vidas, continúen haciendo de salas esta cuidad entrañable".
        
            