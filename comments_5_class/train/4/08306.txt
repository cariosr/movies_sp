
                      Aunque a la película le falta más suspense e intriga, y el ritmo puede ser un tanto lento, el reparto es muy bueno, y los escenarios son inmejorables. Si se entiende el idioma, es mucho mejor verla en gallego, aunque en realidad las versiInés disponibles serían castellano o bilingüe, lo que se adapta perfectamente a la realidad de Galicia. A mi me ha parecido una película muy buena.
        
            