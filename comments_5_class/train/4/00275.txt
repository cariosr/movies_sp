
                      Existen personas multimillonarias que creen que el dinero lo es todo.
Algunas llegan a ser despreciables puesto que creen que, gracias a su dinero, pueden dominar cualquier situación. Y claro, si tuvieses un familiar con este perfil, seguro que te plantearías: ¿qué le puedo regalar en su cumpleaños? 
La trama la escriben John D. Brancato y Michael Ferris y, aunque en un primer momento nos sorprende, es demasiado lineal y tiene que recurrir, en todo momento, a giros y maniobras para poder mantener el interés durante la cinta.

Fincher cuenta con un despreciable multimillonario como eje central de la película, que le sirve, entre otras cosas, para hacer una crítica a esta clase social. Para interpretarlo escogió a Michael Douglas, para mí, uno de los mejores intérpretes que ha dado la industria cinematográfica. 
Está perfecto en su concepción de millonario totalmente forrado que tiene que recurrir a una empresa para que le invente una vida apasionante y llena de aventuras. 
Por si fuera poco, también cuenta con Sean Penn, otro monstruo de la interpretación, aunque lamentablemente no tiene demasiada participación en la cinta. Eso sí, lo poco que sale lo borda. Son dos actores a los que nunca les he visto una mala actuación, han podido elegir con mejor o peor criterio sus proyectos, pero siempre han dado la talla sobradamente. 
Como coprotagonista femenina la elegida fue la canadiense Deborah Kara. Tiene en su rostro y en su actuación la dosis de misterio e intriga perfectas para esta película.

The Game es un film que consigue entretenernos, pero no convencernos del todo. Particularmente el final del film es lo que menos me convence y terminas pensando que este tipo de historias solo pasan en las películas. Es decir, no te lo llegas a creer. Lo que sí me gusta, y mucho, del planteamiento del director es que desde el primer momento estamos avisados de que todo es un juego, no hay más que fijarse en el título de la cinta, y ahí es donde pienso que acierta de pleno, juega con el protagonista, pero también con el espectador. Sabemos o mejor dicho, pensamos que todo es un juego, pero según avanza el metraje empezamos a dudar de que sea así. Consigue sorprendernos en muchos momentos y va generando una tensión muy efectiva. 
Tenemos una película con maniobras, trucos y efectos que, a veces, resultan de creer pero muy entretenida, con una interpretación muy convincente de su protagonista y con una cierta destreza visual.
En definitiva un buena película que pudo llegar hacer una joya.
        
            