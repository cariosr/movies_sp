
                      Siempre he comparado a Alejandro Amenábar con M. Night Shyamalan. Sus carreras siempre me han parecido muy similares: los dos comenzaron en el género de suspense y del terror psicológico (“El sexto sentido“, “Señales“/”Tesis“, “Los otros“). Los dos decidieron girar sus carreras hacia algo más comercial y para otros públicos (“Airbender“/”Ágora“). Y los dos, después de mucho tiempo reclamando su regreso a sus orígenes vuelven por la puerta grande (“La visita“/”Regresión“).

“Regresión” cuenta la historia de un detective que investiga el caso de una joven que fué violada por su padre. Con la ayuda de un psicólogo, y de las confesiones de la propia víctima, intentará esclarecer el caso mientras se ve involucrado más de lo que quisiera.

Un regreso a los orígenes del director con un thriller de suspense de los de toda la vida. Investigaciones policiales teñidas del misterio más escalofriante, secretos en todos los personajes y en los que ninguno es inocente. Ethan Hawke se come todo el pastel con un personaje que recuerda mucho al de Richard Gere en “Mothman“: debe decidir si creer en la evidencia o confiar en la pruebas. Su oscuridad, su secretismo (incluso todo el que ha rodeado la película desde que se comenzó a hablar de ella), la tensión que se respira en las calles del acogedor pueblo de Minessota y un granero tétrico y acojonante. Todo con una banda sonora excitante, muy característica de estas películas, inigualable a la hora de crear angustia al espectador, y que haga que no nos queramos despegar ni un segundo de la pantalla. Las sectas satánicas y los rituales al demonio inspiran el caso de una de las películas más esperadas del verano.

Lo mejor: su oscura ambientación.
Lo peor: que juntando todas las escenas de Emma Watson no superen los 30 minutos.
        
            