
                      Película con una visión distinta al inicio de toda esa serie de leyendas de Drácula. Buena actuación de Luke Evans, que va especializándose en ese tipo de sagas épicas, ya que se presume que continuaran sacando provecho de este personaje. Los actores secundarios no logran destacar mucho, salvo la bella esposa de Vlad. Sin ser espectacular, tiene buena realización y un ligero ambiente de sombras. Aunque me parece que al monstruo del filme le falto ser mas contundente en su historia y pedidos al protagonista.
        
            