
                      Kong: La Isla Calavera (2017).
Nueva vuelta de tuerca al mítico King Kong, en esta ocasión, más grande que nunca y con excelentes efectos especiales. 

En esta versión nos encontramos unos humanos más militarizados, unos parajes tan hermosos como hostiles (la localización se torna en un personaje más de la historia), mucha acción y unos "lugareños" más "civilizados" que los de otras películas del gorila gigante.

El reparto es poco menos que estupendo; como protagonistas, un atractivo Ton Hiddleston saliéndose de su querido Locki (Thor), una linda chica (menos) objeto de deseo de Kong que otras veces, y unos secundarios veteranos como se espera de una gran producción (Jackson, Goodman, John C. Reilly...). 

Esta vez la acción se centra en la isla que le da nombre al título, y la ambientación en la recién acabada guerra de Vietnam (una de las localizaciones de la cinta). Como antes apuntábamos, los paisajes son preciosos (Hawaii, Australia), la fotografía no desmerece en absoluto, la música es de los 70's (época en la que nos encontramos esta vez), y para los románticos avisarles que nuestro simio es mucho más práctico que sus antecesores y menos "pasteloso" que un servidor, con lo que no bebe los vientos por Brie Larson, la chica que antes señalaba. ;

Por último decir que si os esperáis a que acaben los créditos (cosa que en mi sesión solo hice yo) podréis ver una última escena que nos arroja cierta luz para venideras secuelas. La película está muy entretenida sin lugar a dudas, pero aún siendo puro cine espectáculo no le alcanza a la obra maestra de Peter Jackson.

Raúl Cabral.
        
            