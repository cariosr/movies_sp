
                      La última película de M.Night Shyamalan creo que es buena; algo que, vistos (o no) sus últimos truños, hace de ella una excelente noticia y muy agradable sorpresa para quienes nos temimos que la carrera de este cineasta estuviera ya tocada y hundida bodrio tras bodrio.

Creo que es una película más "modesta" pero infinitamente mejor que las últimas suyas y (casi) a la altura de sus excelentes inicios.

Para quien esté interesado, he escrito una reseña-opinión más completa en mi blog.

Buena semana a todos. :)
        
            