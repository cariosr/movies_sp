
                      Good Morning, Vietnam es una comedia dramática de carácter bélico dirigida por Barry Levinson, director de 'El Secreto de la Pirámide (Young Sherlock Holmes)', 'El Hombre del año' y 'Toys'; estas dos últimas protagonizadas por Robin Williams.

Adrian Cronauer es un locutor de radio que es trasladado a Saigón para encargarse del programa de radio de la cuidad, durante la Guerra de Vietnam.
Los soldados lo adoran, ya que es el locutor más divertido e ingenioso que han tenido hasta el momento. Todo parece ir bien para Adrian, hasta que sus jefes notan que sus comentarios y chistes no son políticamente correctos.
En su tiempo libre, se hace profesor de inglés para los nativos para poder estar más cerca de la chica que le gusta.

El personaje de Williams es muy completo, porque podemos ver su faceta más divertida, al igual que su lado dramático y sensible.
Además de Adrian y la buena historia que narra la película, se puede destacar a Forest Whitaker, que realiza una actuación espectacular de apoyo, y se le da a su personaje la importancia que este merece.

La banda sonora esta compuesta por canciones de grupos famosos de la época, como The Beach Boys, James Brown y Lawrence Welk entre otros.
Para los oyentes, la música que elige Adrian es un soplo de aire fresco en comparación con la música clásica o las polkas que ponía el anterior locutor.

La película recibió cuatro premios, uno de ellos fue el Globo de Oro al mejor actor de comedia, para Robin Williams.
        
            