
                      Muy buen thriller policíaco con tintes a ¨Saw¨ y con el trasfondo de ¨I Spit on your Grave¨.

Quitando una pésima fotografía, la cuál es una mezcla de calidad normal con telefilm barato de sobremesa, la película es muy buena. 
Arrancando con un simple caso policial, la cinta se da la vuelta para mostrarnos una historia dentro de otra historia, algo terrible que ocurrió hace mucho tiempo y que ahora, busca venganza.

Desvelando la corrupción policial ligada a los trapos sucios en una violación, ¨WAZ¨ adquiere tintes psicológicos muy fuertes mezclado con varias escenas de tortura que provocan arcadas.

La asimilación de todo lo anterior con la simbología de la Ecuación hace que este metraje consiga otro plus, saliendo de una posible ¨versión de policías de I Spit On Your Grave¨.

Las actuaciones no se quedan atrás. Me parecieron muy buenas, pero sobretodo encarnar a ¨Jean¨ es digno de un premio.

8/10
        
            