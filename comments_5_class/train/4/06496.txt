
                      The Favourite es una película difícil de clasificar, pero lo más fácil sería decir que es una "tragi-comedia", una historia en la que pasan cosas malas, cosas horribles, cosas tristes, pero todo eso no es razón para no reir.
Y Yorgos Lanthimos no pierde la oportunidad de sacarnos una que otra risa, mientras nos muestra como una Reina en mal estado de salud pierde conexión con su confidente de toda la vida, y tiene un combate con sus propios sentimientos cuando una sirvienta se infiltra en su relación para realizar unos "cambios". Olivia Colman, Emma Stone y Rachel Weisz nos regalan un triángulo de "confianza" y tres actuaciones para el recuerdo.

¿A quién elegir? ¿Qué será mejor para el futuro del reino?

¿Qué será mejor para el futuro de tus emociones?
        
            