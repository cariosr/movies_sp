
                      Marty (Colin Farrell) es un guionista tópico de Hollywood: tirao, en crisis creativa, y viviendo bajo el sol californiano lo mejor que puede. Tiene un guión en mente y en proceso, que se llama Siete psicópatas y, además del título, tiene a uno de los psicópatas ya perfilado. Pero no se le ocurren muchas más cosas, por lo que su buen amigo Billy (Sam Rockwell) le ofrece ayuda. Billy es amigo también de Hans (Christopher Walken), y se ganan la vida secuestrando perritos para luego cobrar la recompensa de sus adinerados y entristecidos dueños. Es un negocio un poco arriesgado, aunque les proporciona la liquidez que necesitan para sus vicios y problemas. Pero, por casualidades de la vida, acaban secuestrando a un chucho que es el ojito derecho de Charlie (Woody Harrelson) un mafioso bastante desquiciado que hará lo que haga falta para recuperar a su mascota.

Marty sigue buscando su inspiración para una historia que le va pasando delante de los ojos, metiéndose inconscientemente en problemas que jamás habría imaginado ni para escribir sobre ellos, pero a los que sus compañeros de aventuras parecen estar un poco mejor preparados. No es que se deje llevar, es que la historia es tan potente que lo arrastra, así como arrastra a los que estamos disfrutando de ella desde una posición más cómoda delante de la pantalla.

Farrell encaja bien los golpes que le dan a su personaje, mientras Walken y Rockwell, cada uno a su manera, intentan dirigir los acontecimientos a su manera, que no es otra que una vis cachonda muy acertada para el tono de la película. Qué decir de Harrelson, cuya cara parece estar hecha a medida para un tipo como Charlie. No hace falta que nos intentemos creer que alguien puede ser así. Seguramente Woody Harrelson sea alguien igualmente excéntrico y exagerado, aunque espero que dentro de la legalidad.

La aparición de distintos psicópatas, variopintos y con genialidades, como el hombre del sombrero encarnado por Harry Dean Stanton, sólo podría funcionar con una narración tan bien llevada como la de esta película, en la que el director y guionista, Martin McDonagh, mantiene en buen nivel el listón que ya puso anteriormente con Escondidos en Brujas.

Tal vez no sea la típica película para el gran público palomitero, pero el reclamo de su gran reparto y la original premisa sobre la que parte, hacen que merezca la pena, cuando menos, dejarse enganchar por ella.

sígueme en twitter: @jgripweed
        
            