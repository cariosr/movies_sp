
                      Pues que me gusta a mí La Coixet, oye. 
Que está donde está porque se lo ha ganado a pulso. Porque tiene las gónadas bien puestas. Porque dirige como poca gente en este país. Porque es una cachonda mental.  Porque no es nada correcta políticamente hablando. Porque es capaz de decir en una gala que su madre está más delgada que la de una competidora...Y además porque es de las pocas que ha sabido sacar tanto provecho a un actor tan desaprovechado como Bill Nighy...y tan grande.

La coixet es muy de drama, de depresión y de intensidad de esa que puede llevar a cortarte las venas. Es muy de angustias vitales ella, pero luego te la llevarías de copas para poder hablar sin pelos en la lengua de todo y de todos como ya no se hace desde los ochenta.

La Coixet es un poco "british", pero con raza. Y por eso LA LIBRERÍA es un poco (un mucho) "british"...pero con raza.

"gustadome ha La Librería", como diría el maestro Yoda,"pero no entusiasmado". Pero es que, por definición, "british" y "entusiasmo" son bastante antagónicos.
La Coixet llega en un ferry, monta una librería. Intenta culturizar a un puñado de garrulos. Se atreve a pensar por sí misma y la destrozan quienes mantienen agarrulados a los garrulos. Bueno, no la Coixet, sino Florence, que es a quien interpreta, y de manera no muy convincente por cierto, Emily Mortimer...pero como si fuese La Coixet...o cualquiera en este mundo que  tenga los huevos de ser uno mismo.
La Coixet nos trae un relato sencillo, sin sobresaltos y lleno de veneno, que se deja ver con agrado. Una farsa, un engaño que se urde a espaldas del espectador y que, como en la vida misma, te deja sumido en la mala ostia que producen la impotencia y el abuso.
Y es que las películas, y más los libros, en el fondo son como la vida misma. Al menos cuando son buenos. Y una película que se atreve a recomendar uno de mis libros favoritos CRÓNICAS MARCIANAS, del gran Bradbury (tan denostado por su falta de convencionalismos) y que cuenta una historia con tantas similitudes a ésta, ya tiene ganado mi respeto.
La Coixet ya tiene ganado mi respeto.
Bill Nighy ya tenía gando mi respeto, pero a partir de ahora, mi admiración. ¡Que papelón ,señoras y señores! Con qué elegancia, con que aplomo, con qué dominio...con qué economía de expresiones se desayuna, se come, se merienda y se cena LA LIBRERÍA. Que momentazos nos regala él y nos regala Patricia Clarkson destrozándose mutuamente en pantalla, como felinos sin apenas tener que pestañear. Qué grandes actores ambos y qué grandes actuaciones.

Qué grandes todos, en realidad, salvo Emily Mortimer, que igual peca de ser demasiado "british"...pero de verdad.

Sí, me gusta esta LIBRERÍA, sin triunfalismos y en su justa medida: como una buena película que merece una segunda lectura. Como obra madura de su autora, a la espera de obras mejores. Como un libro con una buena historia y con una buena escritura. Porque esas dos cosas sí las tiene, a pesar de otros aspectos técnicos (sonido, vestuario, montaje, fotografía) que a mí, personalmente, me parece que no están a la altura.

Y me gusta sobre todo, porque me reafirma en eso que siempre he dicho: que la gente en muy mala, pero mucho muyo, oye. Si no, no existiría "Sálvame"
        
            