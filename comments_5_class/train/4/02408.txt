
                      Ha vuelto M. Night Shyamalan a ver si vuelve a recuperar la confianza de su público, y lo hace con La visita, una historia en inicio simple y sin ningún contratiempo previsible, dos menores que van a visitar a sus abuelos maternos que por cuestiones familiares no tienen relación y aun no los conocen. 

Con muy bajo presupuesto y con una puesta en escena simple, Shyamalan vuelve a optar por el terror y la inquietud psicológica pervirtiendo una figura en principio entrañable como es la de unos abuelos. Con la excusa que de que la menor quiere grabar un documental de la visita a los abuelos, la película nos es contada y presentada con las imágenes grabadas cámara en mano por ambos hermanos en plan documental, algo que con El proyecto de la bruja de Blair impactó mucho, volvió con fuerza gracias a REC pero empieza a estar muy usado hoy en día y ya no sorprende. Esto no quita que la sencillez y el agobio que causa una cámara en mano nos cause malestar e inquietud al espectador, cosa que es lo que pretende el director.

Hay que admitir que tras el fiasco de sus dos últimos proyectos, Airbender y Afterhearth, Shyamanlan tenía que recuperar su estilo inicial y algo lo ha conseguido, ya que La visita, sin ser una gran película ni ser una de sus mejores historias, consigue al menos inquietarte durante toda la película y lograr un final que deja buen sabor de boca. Si bien es verdad que los recursos que se utilizan para ello son los típicos sustos de películas de terror de los últimos tiempos, sin aportar nada nuevo salvo la figura terrorífica. 

Pero no os preocupéis que algunas sorpresas están aseguradas y con Shyamalan nada es lo que parece y no sabes bien que está pasando.

Lo mejor es la actuación de los desconocidos niños ( Olivia DeJonge y Ed Oxenbould ) y los abuelos (Deanna Dunagan y Peter McRobbie), destacando ambas féminas y sobre todo la abuela que pasa de ser tremendamente tierna a ponerte los pelos de punta y darte un mal rollo increíble. Horribles son también los raps del chico protagonista con aspiraciones de ser un gran youtuber, pero creo que esto se debe en gran parte a la traducción en castellano de las canciones, pero por el resto tiene algunos momentos geniales que harán soltar risas a todos los espectadores.

Quizá la película peca de ser un poco lenta en su nudo ya que parece estancarse en cierto momento, pero consigue recuperarse, ya que el planteamiento inicial promete y el desenlace es más que aceptable. También intercala momentos o situaciones de humor que rebajan un poco la tensión y restan intesidad a la trama, pero es cierto que le dan un toque de realismo a la historia y te la hace en todo momento creíble.

Sigue preocupándome el tema de los trailers ya que en esta ocasión algunos de los mejores momentos que más deberían sorprender o aterrar te los cuentan en el trailer y eso disgusta a la hora de ver la película. Por esto mismo no os enlazaré el trailer aquí y si no lo habéis visto pues seguid sin verlo hasta haber visionado la película.

Resumiendo podemos decir que estamos ante una buena y decente película de Shyamalan que te inquietará y te hará estar pendiente de la evolución de los abuelos hasta....

Esperemos sea solo una parte del camino que nos devuelva lo mejor de este director.
        
            