
                      Rebelión en la granja

En un momento de “La oveja Shaun. La película”, su protagonista mira por la ventana de la casa del granjero y ve a tres cerdos sentados al sofá y viendo la tele, comportándose como humanos. Con esta referencia tan orwelliana, la factoría Aardman vuelve a humanizar a sus personajes, como ya hicieran en la prodigiosa y divertidísima “Chicken Run”, pero ahora adaptando a formato largo las aventuras de la oveja que desde que naciese en 2007 ya ha protagonizado más de un centenar de cortos.

Y lo hace sin perder un ápice de frescura, y sin que el producto apeste a alargado hasta lo imposible. Porque en su salto a la pantalla grande, el dúo formado por el guionista formado en la propia compañía Mark Burton y el recién llegado Richard Starzak se ha preocupado de que la cinta siga la tónica general de la serie, ausencia de diálogos incluida, y que no aburra a pesar de su arriesgada condición de cine mudo, que puede hacer huir de las salas a más de un espectador.

No hay nada que temer, “La oveja Shaun. La película” es una delicia de apenas 80 minutos de metraje que desborda ritmo e inteligencia, que ni se hace pesada ni se queda corta, que puede entusiasmar a los fans de la compañía británica de animación y a los que no son conocedores de su obra, a niños y no tan niños. Una aventura para toda la familia y todo tipo de públicos que no descuida las referencias a otras películas como “El silencio de los corderos”, “Taxi Driver”, “Cadena perpetua” o la propia “Chicken Run”, y por extensión, a “La gran evasión”. Y, cómo no, toda la propuesta es un homenaje al cine mudo, al que reverencia ya en fondo y forma en sus caseros créditos iniciales.

Estamos, casi con total seguridad, ante la mejor película de Aardman desde hace diez años, desde que Nick Park decidiera hacer saltar a la gran pantalla a los personajes más célebres de la casa en la magistral “Wallace y Gromit: La maldición de las verduras”. Sí, en medio nos han regalado entretenimientos como “¡Piratas!”, pero las intenciones de esta reaccionaria y soñadora oveja van más allá del mero entretenimiento. Es cine hecho con alma y corazón, una vuelta a los orígenes del estudio. Un regreso a casa, como el retorno a la normalidad al que sus directores y guionistas encaminan a sus protagonistas, sólo para que puedan seguir regalándonos joyas de corta duración muchos años más.

A favor: que mantiene intacto el espíritu del original, y que mantiene el ritmo y el pulso pese a su arriesgada opción muda
En contra: precisamente su condición de cine mudo, que puede hacer huir a más de un espectador
        
            