
                      Múltiple (2016).
M.Night Shyamalan vuelve con un thriller curioso e interesante a la par que algo tedioso en su desarrollo, como acostumbra últimamente.

La premisa en principio es fascinante: el secuestro de unas adolescentes, encabezadas por la joven y experimentada en pelis de terror Anya Taylor Joy, por un enfermo con trastorno de personalidad múltiple. Nada menos que 23 en una sola persona! De las cuales, 8 las encarna divinamente en la historia el escocés James McAvoy.

Dos horas de película que quizá podría haber sido algo menos larga, unas estupendas interpretaciones y un buen guión, aunque, como antes decía, un poco pesado. Pero Shyamalan es así. Qué se le va a hacer...

Atentos a los cameos, incluyendo el de un antiguo personaje de una obra del director de hace más de tres lustros.

Raúl Cabral.
        
            