
                      Sorprendentemente exquisita. Plasma el proceso de la conquista desde una perspectiva curiosa y encaminada a la reflexión continua. La interpretación de los actores protagonistas es formidable y el ápice de las escenas musicales le otorga un encanto especial a la historia, haciéndola aún más dinámica y viva. La calidad de los diálogos es innegable y pocas pegas pueden surgir, hasta que llega el final, donde uno se queda un tanto vacío y frío esperando un encentro que nunca llega.
        
            