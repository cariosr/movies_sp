
                      Una gran mezcla de cultura popular y sentido del humor no apta para todo tipo de públicos. Su punto fuerte, sin duda, la espectacular puesta en escena que imita con maestría la estética de los videojuegos, logrando fascinar y sosteniendo así la insulsa trama.
        
            