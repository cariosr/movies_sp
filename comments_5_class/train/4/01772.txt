
                      Una gran ejecución

Esta película trata la temática del cáncer,esta es la premisa,de aqui parte todo,sin sorpresas,ni giros argumentativos,nada,pero esta película tiene una ejecución ejemplar.La película crea unos personajes entrañables que causan efectos de alegría y tristeza de una manera sorprendentemente potente.La verdad es que se nota el esfuerzo de los actores,aunque cabe destacar más aún a un Joseph Gordon-Levitt fantástico.50/50 cuenta con una dirección aceptable,música la justa,y un buen guión,con las ideas bien estructuradas y con una progresión adecuada. Una película muy tierna y recomendable.

¿Lo mejor? Joseph Gordon-Levitt y una perspectiva del drama del cáncer bastante cercana.
¿Lo peor? Unos dialogos que a veces son un poquito cargantes (I´m fine,I´m fine...)
        
            