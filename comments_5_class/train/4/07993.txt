
                      La verdad es que de entrada es una película diferente. Se presentan 12 naves extraterrestres en diversas partes del mundo, y no nos podemos comunicar con ellos, por lo que el ejército forma un equipo en Estados Unidos, reclutando a una experta lingüista (Amy Adams, maravillosa!!). Ya desde el principio se cuestiona la linealidad del tiempo, y van apareciendo flash backs de ella con su hija, sin contar nada más.
La relación con los extraterrestres se va complicando, y como siempre aparece la gente que prefiere las armas, o la gente que prefiere la inteligencia. El guión está muy bien, con un giro final que lo explica todo, y uno sale del cine pensando si es bueno conocer el futuro, y si haríamos ciertas cosas muy dolorosas.
        
            