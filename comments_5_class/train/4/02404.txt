
                      Jurasic World (2015).
La obra maestra de la ciencia ficción nacida en la mente de Michael Crichton y llevada al cine por uno de los mejores directores de todos los tiempos, vuelve con esta cuarta entrega protagonizada por el actor de moda, Chris Pratt, tras encabezar el reparto de ”Guardianes de la Galaxia”, y por la atractiva hija del realizador Ron Howard, Bryce Dallas. 

La puesta en escena de la historia unida a los efectos visuales y sonoros, el diseño de producción, la música y toda la acción no tienen necesidad de comentarios. Como sus antecesoras, no tiene desperdicio. 

En esta ocasión los dinosaurios, tanto los normales como los manipulados genéticamente, vuelven a la isla Nublar, esto es, aquella en la que el personaje del fallecido Richard Attenborough "no reparara en gastos" en la primera parte de la saga. Como todos esperamos el famoso parque se torna en caos, y a partir de ahí nuestros héroes tendrán que hacer lo imposible por solventar los errores de la mano del hombre cuando juega a ser dios.

Raúl Cabral. 
http://www.filmaffinity.com/es/film843613.html
        
            