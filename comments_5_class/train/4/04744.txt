
                      Spotlight (2015).
Estamos ante uno de esos thrillers periodísticos imprescindibles para profesionales del periodismo. No por otra cosa ha sido considerada una de las mejores películas del año para diversas asociaciones de críticos. 
La historia, basada en hechos reales, está localizada en Boston, y nuestros periodistas pertenecen al Boston Globe. Encarnados por un sobresaliente Mark Ruffalo (lejos del papel del superhéroe verde) , un envejecido pero excelente Michael Keaton (también queda lejos su alter ego Hombre Murciélago), una atractiva Rachel McAdams y todos estos acompañados por actores del talento de Liev Schreiber (no todo va a ser aparecer en La Quinta Ola o de hermano malo de Lobezno), John Slattery y Stanley Tucci, como un gran profesional aunque antipático abogado.

Rodada de un modo sobrio, como lo es su banda sonora a cargo del gran Howard Shore (El Sr. de los Anillos), Spotlight trata sobre la publicación en el citado diario del escándalo de pederastia llevado a cabo por 90 sacerdotes de la archidiócesis de la ciudad más conocida de Massachusetts. 
No me desahogaré como buen ateo anticlerical vertiendo exabruptos sobre la mayor secta y más antigua empresa del mundo que es la Iglesia Católica, responsable de millones de crímenes de todo tipo desde hace siglos, pero sí diré que queda a la altura que merece en esta diría que aséptica, y pienso que también objetiva cinta dirigida por Thomas McCarthy, máximo responsable de una obra que ha obtenido los siguientes premios hasta ahora:

2015: Premios Oscar: 6 nominaciones incluyendo mejor película y director
2015: Globos de Oro: Nominada a mejor película drama, director y guión
2015: Premios BAFTA: 3 nominaciones, incluyendo Mejor película
2015: American Film Institute (AFI): Top 10 - Mejores películas del año
2015: Festival de Toronto: 2ª Finalista Mejor película
2015: Festival de Venecia: Sección oficial largometrajes (fuera de concurso)
2015: Independent Spirit Awards: Premio Robert Altman. 5 nominaciones
2015: National Board of Review (NBR): Mejores 10 películas del año
2015: Directors Guild of America (DGA): Nominada a mejor director
2015: Círculo de Críticos de Nueva York: Mejor actor (Keaton)
2015: Críticos de Los Angeles: Mejor película y guión
2015: Critics Choice Awards: Mejor película, reparto y guión original
2015: Sindicato de Actores (SAG): Mejor reparto
2015: Premios Gotham: Mejor película, reparto y guión
2015: Satellite Awards: Mejor reparto. 8 nominaciones incluyendo mejor película
2015: Asociación de Críticos de Chicago: Mejor guión. 4 nominaciones

Muy recomendable para los aficionados a este subgénero que es el drama periodístico de investigación, aunque no esperen encontrar acción, porque ni la tiene ni falta que le hace.

Raúl Cabral.
        
            