
                      En esta época de "ya lo he visto todo", lo afirmo, pero (un pero importante) esta cinta me trae recuerdos de ese cine bizarro y modesto que veía pasadas las 12 pm en televisión publica hace ya 10 años, cintas de las que salia felizmente traumado. Con un toque de humor manipulado vilmente en el mejor sentido de la palabra por Michael Parks de quien no tengo grandes referencias, pero con esta ya me sobra.
La mejor forma de resumir Tusk es lo que dijo mi madre después de verla, secándose unas lagrimas, "¿que mierda me haz hecho ver?".
        
            