
                      Esta película está muy bien hecha y los actores protagonistas (la niña y su padre) hacen un papel que te deja sorprendido. Es un film independiente, diferente y original. 
El argumento es sencillo: un padre que vive con su hija de 7 años en una tribu de una isla de Estados Unidos. Por culpa del deshielo, el agua sube de nivel e inunda todo el territorio y solo sobreviven algunos. Todos estos tienen que convivir y luchar por sus vidas, sobretodo la del padre porque padece una enfermedad y sabe que morirá pronto, pero no quiere que su hija lo sepa. 
Es una mezcla de inocencia e influencia del adulto al pequeño y se demuetran las capacidades que tiene un menor para poder sobrevivir y luchar sin rendirse.
        
            