
                      Muy bella y conmovedora. Centrada en la familia, los imprevistos de la vida y la continuidad de nuestro legado a través de nuestros hijos y nietos. Son 2 historias totalmente independientes que encuentran su punto de interacción recién en el desenlace. Ambas presentan un nudo dramatico, algunos momentos divertidos y giros constantes que van sorprendiendo. Un gran rompecabezas que concluye uniendo sus partes y logra transmitir emotividad con su mensaje final. Simple, cálida y excelentemente interpretada. La recomiendo sin dudar.
        
            