
                      Con el reparto y el director con el que cuenta la película se elevan mucho las expectativas. Sin embargo, esas expectativas no se logran satisfacer. Viudas está dentro de los estándares de lo que se considera un buen thriller, pero con los recursos humanos con los que cuenta se pudo haber sacado más provecho. La gran actuación de Viola Davis es la única ancla con la que cuenta la película para que no sea fácilmente olvidada.
        
            