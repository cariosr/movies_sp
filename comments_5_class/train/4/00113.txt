
                      La rebeldía ante el sistema impuesto por sus padres,  el amor (y los celos), el deseo de libertad y sobre todo el valor de la amistad son los temas sobre los que gira esta historia. El acierto del guiíon es no tomarse los problemas de los adolescentes a la tremenda. Y parte del mérito recae sobre el personaje del frikie del grupo, Biaggio, que nos regala citas tan absurdas como "Creo que soy gay. Se me encharcan los pulmones cuando cambia de estación."

El debut en la gran pantalla del director Jordan Vogt-Roberts se estrenó en la última edición del Festival de Sundance con una gran acogida de la crítica. Esta fábula sobre la amistad que recuerda al clásico "Cuenta conmigo" es una simpática comedia para el verano. Quizás no esté a la altura de las anteriormente citadas, pero para los que somos amantes del género es de las mejores opciones en cartelera. 

The kings of summer y las películas independientes sobre la adolescencia (en el enlace)
        
            