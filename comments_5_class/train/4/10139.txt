
                      La pelicula podra resultar mala...para aquellos que buscan algo sangriento y con cameos "sorpresa" para asustar al espectador,de una vez digo que este filme no los tiene(cosa que es satisfactoria),pero eso si,el miedo y la tension si que estaran presentes e ira aumentando conforme la pelicula vaya avanzando,es aterradora de una manera diferente,un terror psicologico que parte de lo mas simple- como el monstruo que habita en tu armario- para convertirse en algo sorprendente,inesperado y enterarte de que tal monstruo vive en ti junto con tus peores miedos,alimentandolo dia a dia y que amenaza con salir conforme crece.
Al principio no tenia muchas ganas de verla y mis expectativas no eran muchas,pero al final me dejo maravillada esta joya de pelicula,tanto asi que puedo decir que el genero del terror no esta tan perdido como creia; sin dudas es de las mejores peliculas del genero que he visto en años.

William Friedkin, director de 'El exorcista' (1976): "The Babadook es la película más aterradora que he visto jamas"
        
            