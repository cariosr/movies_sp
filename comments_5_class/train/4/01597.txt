
                      Shyamalan demuestra una vez más que su afán y ambición por el cine que lo alzó en Hollywood sigue tan viva como hace años. Desde la inigualable interpretación de McAvoy hasta la fotografía, 'Múltiple' es la película con la que el director tiene la oportunidad de consagrarse como uno de los mayores talentos del género. El inicio resulta algo agobiante, incluso pesado, pero hay algo en su avance que permite al espectador sentirse cómodo en la butaca. Por no hablar de ese final en honor a sus seguidores.
        
            