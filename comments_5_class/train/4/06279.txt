
                      En el cine actual parece difícil hacer una secuela y que esta consiga superar a la original. En esta ocasión, y bajo nuestra modesta opinión lo han conseguido, ya que, aunque carece del efecto sorpresa de la primera esta "Bridget Jones's Baby" es especialmente divertida, consiguiendo que la sonrisa se mantenga a la salida del cine. 
Lo más curioso de la cinta es que esta sea la mejor Bridget sin Bridget (Renée está tan cambiado que en muchas secuencias casi no parece ella). Por nuestra parte la recomendamos.
        
            