
                      Mad Max es la película de acción mas trepidante que recuerdo haber visto una orgía de violencia de principio a fin del metraje. Sorprendentemente aun así no se hacen largas las dos horas de duración, me sorprende que consiga mantener la tensión, pero lo consigue. Impecables los efectos especiales, sin embargo se echa en falta una mínima profundidad en los personajes. Imposible empatizar, todo se sacrifica en la vorágine de la acción. Aun así los amantes de las pelis de acción de serie b pasarán un buen rato.
        
            