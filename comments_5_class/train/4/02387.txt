
                      Técnica: 8,5/10
Interpretaciones: 8/10
Guión: 6/10

Como superproducción de acción/fantasía es una delicia técnica.
Tanto personajes como efectos hacen un trabajo de respeto y pasión por el mundo de fantasía, haciendo una película amena, con mucho ritmo.
Aunque hay un pero. El guión, siendo una buena historia, tal vez por prisa, ansia o falta de acierto, quedan muchas historias inacabadas o con un cierre demasiado precipitado.
Aún me pregunto por qué uno que caza y cuida animales fantásticos va a EEUU a liberar a uno en concreto, con un clarísimo guiño al patriotismo norteamericano.

Recomendada al 75% para no amantes del género.
        
            