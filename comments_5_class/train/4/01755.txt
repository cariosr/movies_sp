
                      Lo siento, Superman, pero Batman es el tipo duro. Christian Bale, del Batman Begins del verano pasado, no escatimó palabras ni ahorró sentimientos el 3 de junio cuando aceptó el Premio de la Película MTV al Mejor Héroe de Brandon Routh, de Superman Returns de este verano. Bale puso a prueba al público: ¿estás con el caballero oscuro o el santo dentro del hombre de acero?

En el interés de la revelación completa, estoy abatido por el esquizoide, depresivo maníaco que cuelga sexualmente y que se baja al escalar la pelota con músculos incorporados para vengarse de los malhechores por el asesinato de sus padres. Aún así, el director Bryan Singer, quien dejó pasar la tercera película en la franquicia X-Men para traer de vuelta a la pantalla a este advenedizo, hace un caso sólido para la rebanada de pan blanco pulido en medias azules.
        
            