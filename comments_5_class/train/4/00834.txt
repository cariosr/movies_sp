
                      ¿En qué piensan las mujeres? es una comedia romántica dirigida por Nancy Meyers, directora de 'The Intern', 'Tú a Londres y yo a California' y 'No es tan fácil'.

Nick (Mel Gibson) es un rico, egocéntrico y machista publicista de éxito. Cuando en su empresa contratan a Darcy (Helen Hunt) como jefa de campaña, ésta le da a sus empleados una caja llena de cosméticos para mujeres, con el fin de que piensen en cómo anunciarlos al público.
Una noche, mientras Nick prueba cada uno de los cosméticos de la caja, sufre una electrocución al caer en la bañera con el secador encendido.
Al día siguiente se despierta vivo, y con la capacidad de escuchar el pensamiento de las mujeres.

En su día, hubo opiniones muy dispares sobre la película; por un lado se creyó que era la mejor película dirigida y escrita por una mujer hasta la fecha, por otro lado, se consideró de mal gusto.
La realidad es que es una comedia romántica bastante entretenida, con muchos buenos momentos y unos actores estupendos.

De entre el reparto, Marisa Toemi consigue destacar más en los pocos momentos en los que aparece, que Helen Hunt, cuyo personaje está falto de gracia y soltura.

La película solo recibió una nominación a los Globos de Oro para Mel Gibson, al mejor actor de comedia.
        
            