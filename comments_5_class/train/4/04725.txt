
                      Me ha gustado esta película. Es un remake. No sé si será mejor o peor que la que versiona pero siendo John Wayne estarán a la par. Ambas se basan en la misma historia. Los hermanos Coen vuelven a ofrecernos una historia y unos personajes fantásticos. La otra cara es que "es larga", por lo que no a todo el mundo le gusta una historia "tan lenta". Buena película.
        
            