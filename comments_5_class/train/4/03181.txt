
                      Después de varios años, nuestros carismáticos dinosaurios regresan a la gran pantalla con esta nueva entrega.
Si bien no es para nada la film del siglo, nos regala unos momentos de entretenimiento. Los actores seleccionados, me imagino que para atraer tanto publico femenino como masculino, encajan, en mi opinión, en sus personajes.
->Lo bueno
-Tiene varios momentos divertidos que te harán reír varias veces.
-Si bien el indominus rex no es tan escalofriante, lo plantearon bien. Una parte de mí tenia miedo de que su diseño fuera a parecer algo ridículo, pero no, no te eriza la piel pero es pasable, ademas, hay que tener en cuanta que "asustar" con el indominus no era el objetivo. 
-Buenos actuaciones para este tipo de película.
->Lo malo
 Si bien en mi opinión no me molesto gran cosa, he escuchado gran cantidad de comentarios acerca de la  apariencia de los dinosaurios. Mucha gente salió del cine con el convencimiento de que debieron de usar más animatronics que efectos de computadora, y es verdad. Por más que lo intenten aún no se puede alcanzar el 100% del realismo por efectos computacionales, es un hecho que la popular serie de "The Walking dead" ha comprobado con creces.

En conclusión: No creo que trascienda tanto como la dirigida por Steven Spielberg, pero merece la pena verla. Merece sentarse esas dos horas frente a la pantalla, saldrás riéndote y de buen humor.

->Dato curioso: Bryce Dallas estuvo con tacones toda casi la mentada película!! ¿Se imaginan como debe de ser correr con eso huyendo de un T-Rex?
        
            