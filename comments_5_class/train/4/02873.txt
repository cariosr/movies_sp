
                      Dicen que para cada época existe un género. Pero hay uno que sirve para todas las temporadas: la comedia. La comedia de toda la vida. Ni romántica ni nada. Comedia llenas de gags tronchantes con una historia que no necesite muchas vueltas. Y es que si hay algo que todo espectador, ya sea hombre o mujer necesita de vez en cuando, es una buena comedia.

Ed Helms (trilogía “Resacón“), Christina Applegate (“La cosa más dulce“) y Chris Hemsworth (“Thor“) protagonizan una alocada road-movie sobre una familia que decide pegarse las vacaciones de sus vidas. Unas vacaciones que se convertirán en inolvidables, pero por la cantidad de infortunios y disparatadas situaciones que vivirán en ellas.

Una película que podríamos definir como la mezcla perfecta entre “Resacón en la Vegas”, “Somos los Miller” y obviamente, “Vaya vacaciones“. Una comedia que no necesita de una gran historia ni un esquema rebuscado. Las situaciones disparatadas se suceden una tras otra (con mayor o menor sentido) haciendo que el guión resalte poco entre carcajada y carcajada. Sin embargo, Helms y Applegate, al contrario que muchos actores cómicos que deciden que su mera presencia en la película ya la hace cómica, se lucen esforzándose toda la cinta en resaltar, hacerse grandes plano a plano y dejando bien claro que para hacer reír al espectador hay que currárselo. Y ellos lo consiguen. Ambos, con un sinfín de momentos tronchantes y reconocibles cameos, nos hacen pasar un muy buen rato en una comedia que no pasará a los anales de la historia, pero si viene estupendamente para las tardes de verano entre tanto mega-blockbuster.

Lo mejor: Christina Applegate en su visita a la fraternidad.
Lo peor: que Regina Hall no permanezca más tiempo en la historia.
        
            