
                      Danzon 

Danzón, una película llena de sentido sobre el amor, la pérdida y el descubrimiento de uno mismo. Escrito y producido por María Novaro esta historia te lleva a través de un viaje de lo que realmente es el feminismo y el descubrimiento de sí mismo. Contada a través de un viaje de una mujer llamada Julia, un personaje que la actriz María Rojo desempeña maravillosamente. La película es sobre una mujer que realmente no se conoce si misma o lo que es amor real. Una noche, ella va a bailar con su pareja de baile, Carmelo, y no lo encuentra. Al principio confundida y luego obsesionada, se vay en un viaje que no sólo resuelve el misterio de su desaparición, sino que también la lleva por un camino de descubrimiento y renovación personal. 

Algunas personas importantes en este película son Doña Ti, Susy, La colorada, Sivia, y Carmelo. Esos personajes ayuda Julia durante su viaje. Carmelo, el compañero de baile de Julia. Muy misterioso y el hombre "ideal" para Julia. Desaparece y no se ve durante gran parte de la película.
Doña Ti, es dueña del hotel donde se hospeda Julia en Veracruz. Se hacen buenos amigos y ella representa una figura materna. Rubén, Él es el interés romántico de Julia en Veracruz. Rubén es mucho más joven que Julia y representa los tiempos modernos que Julia tan fuertemente está tratando de evitar. Susy, travesti que se hace amiga de Julia en Veracruz. Susy ayuda a Julia a tratar de localizar a Carmelo. 
 
La directora, Primera actriz, ha desarrollado una carrera fructífera que supera los 50 años. Ha participado en varias películas, novelas, series de televisión y obras teatrales. Es conocida por proyectos como "el albergue" "mujeres Assassins" "el infierno" "las contigo" "la dictadura perfecta" de cacaro Gumaro "ella es Ramona" "sincronía" en 2017 no fue nominada al premio "las diosas de plata" por la película "jardín de niebla".
        
            