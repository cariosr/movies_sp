
                      Más que una comedia para reír es una película de humor ácido para reflexionar, que muestra las paradojas de la vida en un ambiente donde todos exhibimos nuestras inseguridades y complejos. Siempre es posible mostrarse vulnerable para recibir la solidaridad humana. Buena.
        
            