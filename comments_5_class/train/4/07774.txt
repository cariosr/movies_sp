
                      Deben verla en Cine ya que es una experiencia única. Es para muchos de nosotros lo más cerca de estar en el espacio y "sentir" este ambiente único. Esta película se trata de esta experiencia y del logro técnico de poderla realizar.
        
            