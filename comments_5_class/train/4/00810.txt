
                      Buena película, buena producción española, gran actuación de Bale y buena banda sonora la de Roque Baños. La fotografía es muy llamativa y el entorno que envuelve a la mente de Reznik está bastante logrado,. Aún así, la historia es predecible y no resulta tan potente como la de películas como El Club de La Lucha, a pesar de que la situación final es original a su forma. Recomendable e interesante, pero tampoco imprescindible creo yo.
        
            