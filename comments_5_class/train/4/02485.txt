
                      La mirada íntima y cargada de matices del director Andrew Haigh, que recordamos por la entrañable Weekend, transita por una realidad poliédrica, donde la certeza y la robustez de las relaciones personales penden de un hilo en permanente estado de debilidad.

Del todo recomendable por las memorables interpretaciones y para dejarse llevar por un retrato detallado, pausado y entrañable con sorpresas escondidas en los rincones más profundos de la memoria de una pareja de padrinos aparentemente instalados en la placidez. (7/10)
        
            