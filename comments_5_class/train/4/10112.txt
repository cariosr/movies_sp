
                      Gran película. No dejas de mirar a la pantalla ni un solo instante. Te atrapa por su magistral rodaje, su excelente guión y la magnífica actuación de sus actores. Destaca la jovencísima Hailee Steinfeld. No se la pierdan.
        
            