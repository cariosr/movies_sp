
                      Meritxell Colell Aparicio retrata con tanto respeto y admiración a la naturaleza, que esta le devuelve agradecida sus excelencias. “Con el viento” es un soplo de aire puro, un oasis necesario entre tanta polución visual. Cocinada a fuego lento, al calor de esa leña que Pilar, la madre de Mónica, corta a la puerta de la casa: la cotidianidad de la vida rural está presente en cada plano: la naturaleza, los trabajos del campo, las casas centenarias con sus muros de piedra, la partida de brisca al atardecer. La utilización de elementos propios del documental resultan del todo convincentes y están tan bien integrados en la ficción, que en todo momentos nos creemos como real, lo que la joven directora catalana nos cuenta.
Leer reseña completa en:
https://elcinepormontera.com/con-el-viento-espana-2018-meritxell-colell-aparicio/
        
            