
                      Una película decepciona cuando esperas más de ella, pero cuando esperas poco o simplemente justo lo que esperas, la peli acaba gustando. 
Eliot Ness ya reclutó un grupo antimafia hace algunos años, vuelve a pasar de nuevo. Lo de aquel entoces es insuperable, pero no creo que esta peli pretenda hacerlo. Con todo esto quiero decir que la película esta bastante bien, que consigue su objetivo, pero es algo que ya hemos visto.
        
            