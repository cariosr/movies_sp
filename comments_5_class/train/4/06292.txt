
                      Muy buena película, fresca y diferente.

No busca ser una Vengadores (I y II) o Iron Man (I, II y III) o Capitan America (I y II) es un toque nuevo, que marcara un nuevo rumbo en Marvel, a la hora de hacer películas, de personajes no tan conocidos como los integrantes de los Vengadores.

Las actuaciones son correctas y con alguna que me sorprendio gratamente, como Michael Peña, Paul Rudd y Evangeline Lilly, se complementa perfectamente, esperando que vuelvan a salir en próximas películas ya sean como un Ant-Man 2 o en otras de la saga. Michael Douglas, veterano en la vida real y veterano como Hank Pym, el cambio de registro de protagonista a personaje secundario pero con relevancia le sienta muy bien.

lo unico que me ha fallado es el villano que lo pondría al nivel de "El mandarin", "Whiplash" o "Ultron", sencillamente Marvel debería replantearse o los villanos o los actores, porque desde Loki (Tom Hiddleston) no ha habido un villano mejor en TODA la saga de Marvel.

Efectos FX, buenos sobre todo, como no, los momentos en que se empequeñece el protagonista y los momentos con las hormigas muy bien llevados, si eres fan de los comics pillaras muchas referencias (easter eggs

Valoracion 4/5
        
            