
                      SOMBRÍO FILME DE ESPÍAS CON SEYMOUR HOFFMAN

Basada en una de las novelas más recientes del novelista británico especializado en relatos de suspense y espionaje, John Le Carré, y con una de las últimas interpretaciones del malogrado gran actor Phillip Semour Hoffman, se presenta el filme "El hombre más buscado".

La trama se centra en un joven ruso, sin papeles, que consigue llegar a Hamburgo llevando encima una extraordinaria cantidad de dinero, declarándose musulmán devoto y con signos de haber sido torturado. Por otro lado, los espías de tres naciones, dedicados a la lucha antiterrorista, están convencidos de que han localizado a un importante terrorista islamista. Una abogada idealista, defensora de los derechos humanos, y un poderoso banquero son los únicos que conocen la verdadera identidad del joven.

El director holandés Anton Corbijn, curtido en el mundo de los videoclips musicales, le imprime una atmósfera sombría realmente espléndida, con reminiscencias al mejor cine de espías realizado en la década de los setenta; asimismo, demuestra cierto rigor narrativo disponiendo de un buen entramado, en el que todas las piezas encajan perfectamente; aunque huelga decir que en algunos momentos, el desarrollo pierda ritmo... pero sin desmerecer el resultado final del filme, que es notable.

Aparte de la solidez argumental y ambiental, destaca el gran trabajo del elenco actoral, encabezado por el impresionante Phillip Seymour Hoffman que se mete en la piel del astuto espía Günther Bachmann. Willem Dafoe, Rachel McAdams y Robin Wright (en un rol parecido al que realiza en la serie "House of Cards") también están formidables, dándole empaque a la película.

En conclusión, "El hombre más buscado" es un brillante thriller de espías, con una premisa que incide en la desconfianza hacia el Oriente Medio tras el 11-S y que mantiene el suspense hasta su estupendo final; y que además, cuenta con un deslumbrante Seymour Hoffman que demuestra todo el talento que atesoraba, y dejando patente la gran pérdida que ha supuesto para el mundo cinematográfico.
        
            