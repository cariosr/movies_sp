
                      Muy buena. Una linda y positiva historia de amor basada en el apoyo mutuo por alcanzar las metas propuestas, atravesando los miedos de fracasar en el intento. Muy bien actuada por ambos, buenos musicales y un final agridulce pero real, como la vida misma. La recomiendo.
        
            