
                      Una película que por su trama enrevesada y sus saltos en el tiempo, hay que ir a verla con la mente totalmente despierta para no perder así, ápice de lo que sucede. Destacaría sus varios remake de ciertas escenas de clásicos, que dotan a la película de una cultura cinematrográgica que ya pocas tienen hoy en día. Junto a ello, su buena ambientación en los años 70, su huída de la actual calidad de imagen en HD utilizando una imagen brumosa, su BSO (Alberto Iglesias), sus tomas, fotografía..., que le dotan de una gran calidad sobre 
la ya típica cartelera comercial a la que estamos acostumbrados.
        
            