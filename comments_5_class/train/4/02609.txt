
                      El intelecto social y político bajo el que los Coen esconden sus películas resulta a veces pesado y atosigante. Los diálogos en 'El gran Lebowski' gozan de un componente inteligente y rutinario, de una historia paródica y absurda. Aún así, el sentimiento es de comodidad, de tener claro que uno está viendo una película que tiene que disfrutar y que, a pesar de su complejidad, acaba siendo muy divertida.
        
            