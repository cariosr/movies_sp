
                      Deliciosa película. Sin palabras,con personajes animados y dibujados,capta las emociones de un mago ilusionista. El amor,la amistad,el éxito y el fracaso,con sentido del humor y una cuidada "interpretación",la película te sumerge inmediatamente en Londres,París,Edimburgo y otros tantos paisajes y ambientes tan bien plasmados por esta rara avis del cine.
        
            