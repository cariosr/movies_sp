
                      Es dos tercios de una gran película, pero el comienzo lento y la primera hora sin complicaciones lo frenan. Aún así, para aquellos que se comprometen con el precepto de que "las cosas buenas valen la pena esperar", The Walk, sin duda, cumple.
        
            