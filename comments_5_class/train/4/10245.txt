
                      Acabo de ver esta pelicula hoy a las 19:10 hrs. Realmente quede impresionado con esta nueva entrega de "Godzilla", pero no es del todo perfecta.
Sinceramente tenia ganas de ver esta pelicula desde que en el 2010 anunciaron el regreso de Godzilla, pero tenia mis dudas de que terminara desepcionado al salir del cine y decir: ¿en que estaba pensando?", esto ocurrio cuando en el 2011 anunciaron que el director de esta pelicula sería Gareth Edwards (Monsters) y se me bajaron un poco las ancias porque Monsters la encontre unas de las peliculas mas terribles del 2010, pero eso es un tema aparte.

A continuacion mostrare mis puntos positivos:
- La pelicula tiene una historia muy buena, explican muy bien los eventos ocurridos y los que ocurriran. Si bien es cierto es algo lenta al principio pero no es aburrida.

-Los efectos especiales son incomparables, los monstruos se ven muy reales y el ambiente tambien.

-El sonido es muy potente, y cuando dicen que Godzilla ruge y la sala se sacude, es muy cierto.

-Al principio de la pelicula no aparece Godzilla, pero los MUTO's sí, y si no fuese por ellos la pelicula hubiese sido lenta y muy aburrida.

Puntos negativos:
-Los personajes no son los ideales, Aaron-Taylor Johnson no parecia un militar, se pasaba desmallando (3 veces) y no hiso nada interesante, mas que hacer volar los huevos del MUTO.

-La pelicula no tiene 3D, incluso el mismo Gareth Edwards dijo que la pelicula la habian hecho en formato normal y luego la hicieron 3D, no fue creada para el 3D.

-Unas de las cosas que mas me dio rabia de la pelicula fue el enfrentamiento en el aereopuerto y la pelea en la ciudad despues de la escena en el puente, estaban empezando a pelear y cortan la escena mostrandote otra que no tiene ningun sentido verla.
Creo que si huiesen mostrado mas peleas la pelicula hubiese sido un exitaso del año.

Resumiendo... La pelicula es buena, entretiene todo el rato, empieza y termina bien, es totalmente recomendable. De lo que fue Monsters, aqui el director pone todo su talento y sale algo completamente opuesto.

Recomendacion: no esperen algo asi como los transformers o pacific rim, no es lo mismo.

Eso seria mi critica... Veanla!!! No se arrepentiran ;)
        
            