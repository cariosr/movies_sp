
                      Los Coen han conseguido una vez mas otra innegable obra maestra.
Posee una fantástica banda sonora y buena fotografía.
De reparto brillante que se mezcla y congenia sabiamente .Un ya  veterano Jeff Bridges con Matt Damon y la joven actriz  Hailee Steinfeld que, está mas que increíble en su interpretación, enganchan al espectador desde el primer instante.
Esta nueva “Valor de Ley” es por lo tanto un buen western  de aventuras que nos traslada y extrae una autentica sensación al mas “viejo Oeste”.
Interesante, divertida, amena, opino que perfectamente podríamos estar ante un nuevo clásico de nuestro tiempo.
'True Grit' es sin duda una historia bien tratada de diálogos inteligentes para aquellos que buscar ver buen cine.
No decepcionara a los fieles seguidores de los Cohen ni a los fans de las tantas y grandes películas de vaqueros.
        
            