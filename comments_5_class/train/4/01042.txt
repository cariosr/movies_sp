
                      El Corredor del Laberinto es una película de ciencia ficción dirigida  por Wess Ball, basada en el bestseller de James Dashner.

Cada mes aparece un chico nuevo dentro de una caja junto con provisiones, con destino al Claro. Adolescentes de entre doce y dieciocho años se las arreglan para sobrevivir en él. El Claro está rodeado por el Laberinto, del cual tienen que salir.

A pesar de que se trata de una película juvenil, El Corredor del Laberinto rompe muchos tópicos adjudicados a este género, empezando porque no hay ningún motivo amoroso. La película es todo acción, en la que todos los personajes son masculinos, menos uno.

La intriga está presente a lo largo de la película de una forma moderada, hasta llegar al final, que queda abierto, dejando al espectador a la espera de una segunda entrega. 

La película se ciñe muy poco al libro, lo que puede disgustar a los lectores de la trilogía.
Sin embargo, el reparto de actores es muy bueno, siendo fiel reflejo de los personajes. Los escenarios, las criaturas que habitan el Laberinto y demás efectos especiales, están muy bien conseguidos.
        
            