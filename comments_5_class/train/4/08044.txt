
                      Memorable. Te quedas con la sensación de haber visto una gran película. Frenética en el ritmo y en el personaje. Escenas de drama unidas a la más pura comedia. Reflexiva y con aportes de alguna que otra lección a modo de moraleja. Gran interpretación de Dicaprio que consigue la más plena atención del espectador. Realmente sorprende.
        
            