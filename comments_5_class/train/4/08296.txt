
                      'Dentro del laberinto' se ha convertido en un clásico por méritos propios. Un cuento sencillo, claramente inspirado en otros viajes a la madurez como 'El mago de Oz', pero con encanto propio, una banda sonora magnífica y la inolvidable presencia de Bowie como el Rey de los Goblins. 

Al final del recorrido por el laberinto es imposible no querer a esos peculiares personajes que acompaña a Sarah, esas criaturas a las que en algunos momentos de nuestra vida, y sin ninguna razón especial, necesitaremos.
        
            