
                      Película entretenida de ver, con muchas secuencias de acción, lo cuál no te dejaba digerir alguna escena cuando ya estabas sumergido en otra. Mi impresión, en las escenas que no salían los dinosaurios faltaba lógica y dialéctica. En cambio, las escenas que salen los dinosarios estaban bien realizadas y eran animadas. Para ir con niños, creo que mayores de de 10 años mejor. Para los amantes y meláncolicos  de Jurassic, desde luego que hay que verla. Recomendable para este veranito.
        
            