
                      "A Glass se la puede definir por las mismas características, como un cristal: brillante y frágil, excelsa en sus técnicas fílmicas empleadas pero encasillada en un estereotipo que no parece pertenecerle. Un film que será injustamente marginado por ser mucho menos espectacular que sus compañeras de género y mucho más inteligente en narrativa”
        
            