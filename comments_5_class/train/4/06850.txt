
                      Largometraje de animación tanto para niños y no tan niños. En esta tercera parte de la saga Ice Age, la ardilla Scratch y su bellota cobran mayor protagonismo. Como en todo cine infantil que se precie priman los valores de la amistad, la familia ante cualquier dificultad que se les presente a los protagonistas. En la sala que yo la vi, los niños que había se lo pasaron en grande. Aunque los efectos 3D no se aprecian tanto, como en Monstruos contra Alienígenas, se recomienda ver en 3D. Recomendable para toda la familia.
        
            