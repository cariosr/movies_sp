
                      Y el Óscar para el mejor gruñido es para..... 
Me gusta Leonardo DiCaprio y me parece un grandísimo actor que ha ido evolucionando y mejorando con cada interpretación que ha hecho. Pero a pesar del buen papel en El Renacido creo que el premio ha sido porque estos meses todo el mundo se ha convertido en expertos en cine opinando sobre la injusticia que se ha hecho con Leo.

La película está bien aunque es el trailer en versión extendida. Ni una sorpresa. Un guión plano sin gracia en el que todo se sabe antes de entrar en la sala.
Lo mejor para mí Tom Hardy y por supuesto la mano magistral de Iñarritu. Sólo con ver los primeros "paseos" de la cámara al inicio de la cinta ya sabes porque se ha llevado el Óscar.
        
            