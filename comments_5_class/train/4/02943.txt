
                      Después de 76 años como pionero de DC Comics, Wonder Woman protagoniza su primera película. ¿Qué tomó tanto tiempo? Hollywood, asustado por los fracasos de la taquilla.
de películas de cómics dirigidas por mujeres, como Catwoman y Elektra, se ha limitado esencialmente a
Dudes en spandex y bat-drag. La buena noticia es que esta salida en pantalla grande para la creación de William Moulton Marston es que deja a la serie de televisión de 1970 con Lynda Carter en el polvo y es mucho mejor que Suicide Squad, la última película de DC Extended Universe para apestar el múltiplex. Y como lo demostró en su extenso camafeo en Batman v Superman: Dawn of Justice, la dinastía israelí Gal Gadot es la dueña del papel, su cuerpo hermoso forjado con fuego feminista. Ella realmente es todo eso. ¿La película? No está ni cerca de lo que tiene que ser para darle al actor
y el carácter que tanto merecen la salida resonante.
        
            