
                      Anomalisa es la anormalidad en todos sus sentidos, es una película que no acostumbramos a ver en pantalla grande, que nos desbordará con una naturalidad extraña y mensajes potentes. Lo que tampoco es normal, es como el director es capaz de parir una idea tan genial y estropearla con un ritmo narrativo cansino.
        
            