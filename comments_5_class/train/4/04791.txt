
                      Nos encontramos ante uno de esos films clásicos, una de las obras cumbres del neorrealismo italiano, que merece la pena visionar una y otra vez para apreciar todos los matices de la historia y de las interpretaciones. 

Y para los más adictos al color, aquellos a los que les cuesta mucho ver una película en blanco y negro porque es "antigua" decir que, en este caso, el blanco y negro no sólo es una limitación de su tiempo, sino que ayuda a presentar esa atmósfera opresora y agobiante, incrementando el dramatismo de la situación, recurso que ya utilizó Spielberg con excelentes resultados (tanto artísticos como de crítica y taquilla) en la sobresaliente "La lista de Schindler".

Crítica completa en "Sayonara, baby":
        
            