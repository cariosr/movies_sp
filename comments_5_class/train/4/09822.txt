
                      La historia te engancha de principio a fin, relata un hecho histórico y real de una forma magistral, las transiciones son fabulosas, el actor principal transmite perfectamente la compleja personalidad del protagonista asi como la importancia de la historia.
La recomiendo, es una gran película.
        
            