
                      Una película ejecutada de manera sobresaliente sobre un caso real, que define muy bien tanto en guión como dirección, pero sobre todo en interpretaciones, esta curiosidad maldita (y estas jodidas ganas de patear el tablero para joder a los que creen tener el poder) que conocemos como periodismo. En todos sus ritmos y con un riguroso realismo. Una película digna de facultad de ciencias de la comunicación. Y una película cuya historia era necesario contar.
        
            