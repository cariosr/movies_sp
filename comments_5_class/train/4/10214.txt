
                      Obra maestra y clásico de Hitchcock. De las primeras películas "de miedo" con un ritmo narrativo constante y una trama entretenida. Sin duda de las mejores películas que hay del género.
        
            