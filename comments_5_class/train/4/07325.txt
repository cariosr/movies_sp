
                      La película está basada en un hecho real. La Hermana Helen Prejean fue la consejera espiritual de dos condenados a muerte (Patrick Sonnier y Robert Lee Willie). De una mezcla de ambos, surgió el personaje de Mathew Poncelet (Sean Penn), un preso sin escrúpulos que espera que pongan fecha a su ejecución y, mientras tanto, se pone en contacto con la hermana Helen (Susan Sarandon) y le pide que sea su guía espiritual. 

Curiosamente, para hacer este alegato contra la pena de muerte, Tim Robbins escoge un personaje desagradable, que no se arrepiente de sus actos y no al típico condenado arrepentido por el que el espectador podría llegar a sentir lástima. A pesar de que recurrirán a la justicia para intentar revocar la pena capital, no es ese el tema central de la cinta. El tema principal es el dolor y el sufrimiento de todas las personas que tienen que ver con este proceso: los criminales, los familiares de las víctimas, los familiares de los asesinos, los guardias involucrados, el personal sanitario que tiene que realizar la ejecución… Incluso la hermana Helen sufre las consecuencias de ayudar a un hombre así. 

Una tremenda historia que está llevada magníficamente por Tim Robbins, que le da los tiempos necesarios a cada escena y la profundidad adecuada a los personajes, con unas interpretaciones magníficas, de Oscar, de hecho Susan Sarandon se lo llevó. Una declaración de principios contra la pena de muerte. 
Quiero resaltar una frase que dice Mathew Poncelet (Sean Penn) al principio de la película que me parece una crítica brutal al sistema judicial: 
“No hay millonarios en el corredor de la muerte"
En definitiva, muy buena película.
        
            