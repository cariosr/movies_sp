
                      Una película visualmente impresionante, una obra maestra verdaderamente influyente que se ha copiado una y otra vez. Desafortunadamente, también es una experiencia cinematográfica emocionalmente vacante y, en última instancia, poco gratificante.
        
            