
                      Imprescindible verla en V.O. Ya que la historia cobra todo su sentido, las mujeres hablan entre ellas en español con sus acentos característicos y cuando hablan con los señoritos lo hacen con un francés básico y muy mal pronunciado y por supuesto las personas a las que sirven no tienen ni idea de español. Verla doblada hace que la película quede como una comedieta nada más, verla en versión original te hace ponerte en la piel de las mujeres emigrantes.
        
            