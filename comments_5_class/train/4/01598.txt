
                      No es la cínica, fiesta de quesos en efectivo que temía. OK, Jurassic World es un poco de eso. Pero esta epopeya de dinosaurios de última generación es también más que una explosión de estruendo, rugido, "¡effing viste eso!" Divertido. Tiene una mala racha de actitud subversiva que se conoce con el nombre de Colin Trevorrow. Él es el director y coautor cuyo único crédito anterior, un ingenioso indie de 2012 llamado Seguridad no garantizada, costó $ 750,000, cambio brusco en un producto de estudio como este, que costó, espérelo, $ 150 millones.
        
            