
                      Disney es muy asiduo a llenar las carteleras de nuevas adaptaciones o versiones de sus cuentos o personajes clásicos. Pero también saca provecho de las zonas de sus parques temáticos. La más famosa, la saga “Piratas del Caribe“, que ya está rodando su quinta entrega, y que está basada en la atracción del mismo nombre de la zona “Adventureland” de Disneyland. Toca el turno de “Tomorrowland“, una zona descrita por Walt Disney como “nuestro futuro”. Brad Bird, director de “Los increíbles” y “Ratatouille” es el encargado de transformar toda una zona llena de fantasía, imaginación y adrenalina es un blockbuster hollywoodiense digno de convertirse en saga.

Casey Newton y Frank Walker están destinados a unir sus fuerzas y adentrarse en un lugar llamado “Tomorrowland”, que existe en otro espacio tiempo y que solo recuerdan en su memoria. ¿O no?

Puede que estemos frente a una nueva e increíble película de aventuras futuristas solo comparable con la mismísima “Regreso al futuro“. Con una gloriosa primera hora que juega con la imaginación, con los sueños, la nostalgia, con una mezcla increíble de misterios, enigmas y sobre todo con la grandeza de transportar al espectador a un mundo paralelo donde todo es posible. Un mundo donde los efectos especiales juegan y hacen explotar a un guión que no solo te atrapa, sino que te mantiene pegado a la butaca con las ganas de querer más y más. El nivel, desgraciadamente, baja ligeramente en su última media hora, donde la trama ya no tiene tanto fuelle y se limita a recordar su maravilloso comienzo. Con un final que deja con ganas de más, queremos pensar que esto es solo el comienzo de, cual “Piratas del Caribe” y un (excepcional) George Clooney cual Johnny Depp, la zona de los parques Disneyland no tenga limites y siga exprimiendo el poder de un simple pin, que desde ahora todos deseamos tener.

Muy grandes e insuperables los guiños y easter eggs a otras películas, personajes, e incluso zonas y atracciones del mundo Disney. Pero sobre todo muy grande el homenaje que le marcan al supremo, al genio que todo esta magia: el homenaje a Walt Disney es, simplemente, SUBLIME.

Lo mejor: la rápida y veloz adicción que genera.
Lo peor: quizás excesiva en duración.
        
            