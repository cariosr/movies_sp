
                      Tres años después de la primera parte, es verano ha vuelto la magia a las carteleras de nuestro cine. No os vamos a engañar, no se trata de una Obra Maestra ni de una secuela imprescindible, pero sí que la podemos considerar una gran cinta de entretenimiento.

Y ya sabéis, para una crítica más completa (totalmente particular y sin spoilers) os la dejamos en nuestro blog.
        
            