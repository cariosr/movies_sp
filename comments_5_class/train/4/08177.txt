
                      Empezaré diciendo que esta película es la mas "atípica" de toda la saga. Nada mas comenzar con "el logo de la Warner" ya se apunta como un film de Harry Potter "diferente", con la primerísima toma del rostro del ministro de Magia Rufus Scrimgeour.

La puntuación que le pongo es mas baja por lo de "el campamento" que es una de las partes más pesadas de la película. Lo hicieron demasiado largo, y con escenas que podría haber sido eliminadas sin problemas (por ejemplo, cuando Harry y Hermione bailan).

La cinematografía está â€œmuy bienâ€. Me recordó una combinación del "Prisionero de Azkaban" y "el Misterio del Príncipe". Tiene momentos de luz y colorido tenues bien conseguidos. Me encantó la introducción a la animación tipo Pixar con la fábula de "los Tres Hermanos". Un gran paso desde luego. 

La música no me gustó mucho. No la sentí en la película. Alabo la omisión de música en ciertas partes, pero en la escena de la persecusión con Carroñeros creo que sí le iba un tema, como el de la introducción de â€œSnape to Malfoy Manorâ€, no la percusión que le pusieron. 

Y para concluir, los efectos son espectaculares (la escena de los 7 Potter me hizo reír a carcajadas) como cuando los gemelos se ecogen; y la "Persecución  Aérea" es de lo mejor.

El final es perfecto y  te deja con expectativas para la 2da Parte.

La película incluye todo: humor, suspense, terror (recordándome al "Cáliz de Fuego"
), sensualidad (la escena del beso de Harry y Hermione me dejó con la boca abierta) y romance. 

"Harry Potter y las Reliquias de la Muerte: Parte 1"  es una película que vale la pena verla, pues es el gran comienzo del final de la saga.
        
            