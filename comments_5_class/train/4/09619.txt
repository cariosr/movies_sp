
                      Dadas las maquinaciones detrás de escena que se produjeron durante la creación de Superman II (con el director original Richard Donner despedido, habiendo filmado alrededor del 70% del guión, para ser reemplazado por Richard Lester), el producto final debería haber sido apenas un lío coherente. En cambio, de alguna manera, Superman II es una de las películas de cómics más emocionantes que se hayan hecho.

Nota: 3/4
calificación: Fresh
        
            