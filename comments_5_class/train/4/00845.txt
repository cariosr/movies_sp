
                      Seguramente con esta película habrá división de opiniones. Por mi parte creo que es una buena película, los actores están muy bien y Almodovar es un maestro utilizando la cámara para sacar el interior de los personajes. Sin embargo se hace pesada, sobre todo la primera parte. Me gusta más el Almodovar más dinámico.
        
            