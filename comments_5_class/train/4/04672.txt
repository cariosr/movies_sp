
                      Hay algo que la mayoría de los fans del cine de terror adoramos por encima de los fantasmas, los espíritus y lo sobrenatural. Eso es, un buen slasher. Un tío que hacha en mano vaya sembrando el terror las noches de lluvia. Y las de luna llena, no nos vamos a poner tampoco exquisitos.

Lo creadores de la taquillera saga “[REC]” y “Mientras duermes” no trasladan de nuevo a un edificio poco acogedor de Barcelona. Una pareja que, cuáles okupas, no tienen nada mejor que hacer que colarse en el lugar para pasar la noche. Algo que no sienta demasiado bien a unos extraños y poco amigables desconocidos que necesitan, a toda costa, desalojar el lugar.

Una nueva lección de cine de terror español bien hecho. Con su sangre a borbotones, su banda sonora atronadora, sus gritos y por supuesto, el sonido de un hacha arrastrando las losetas. Vamos, un festín en condiciones para todo fanático del género. Una vez más, demostrando que aunque los personajes principales puedan ser cortos de luces, una buena interpretación y unos golpes de sonido que nos hagan saltar de la butaca son más que suficientes para hacernos pasar un rato de lo más agradable.

Aunque el guión en ocasiones parece que decaiga, el festival homenaje a “Viernes 13“, “Leyenda urbana“, “Scream” o la más reciente “Tú eres el siguiente” se palpa toda la cinta. Podemos estar ante el comienzo de una nueva saga que nos de todas las alegrías que quepan en un tiarrón 4×4 enmascarado. Y si se marca un “Hostel” o un “Saw” exprimiéndonos la historia de el/los asesinos, mejor que mejor.

Lo mejor: que no abuse de los chistes fáciles entre mal rato y mal rato. Esto es terror, no comedia.
Lo peor: que necesite urgentemente una secuela, precuela o incluso una miniserie.

NOTA: 8/10
        
            