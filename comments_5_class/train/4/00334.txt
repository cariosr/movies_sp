
                      Muy divertida y bizarra. Siguiendo con la línea de su antecesora cuenta con el clásico humor negro en la relación de amistad entre Ted y Jhon Bennett (Mark Walhberg) en su camino por ser declarado ciudadano de EEUU. Situaciones desopilantes a todo momento. Banco de esperma donde se le caen las muestras, bullying a comediantes y corredores, grandes fumatas de marihuana, roturas de vidrios, golpes y caídas graciosas son algunas de las escenas destacadas. Mantiene ritmo de principio a fin además de contar con un gran elenco de reparto. La recomiendo.
        
            