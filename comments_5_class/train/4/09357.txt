
                      Película intimista y de conversaciones que recuerdan a las películas antigüas por las conversaciones y por las paradas en los dialogos, así como su música evocando nostalgia, presión, amor, confianza, de hecho hay una canción que me recuerda a Lo que el viento se llevo, estoy segura que no es la misma pero muy parecida. Los personajes indivualmente nos muestran la forma de vivir de cada pero cuando se juntan muestran una camaradería total, mostrandonos la generosidad que puede llegar a tener el ser humano, y que por favor no perdamos en estos tiempos tan individualistas. La mirada del chico es impresionante con esos ojazos abiertos pide ayuda a más no poder, pero sin ser egoista.

Muy buena la dirección y sobre todo la fotografía con las muestras del puerto y de las calles, y sobre todo en algún plano corto del cual no abusa está genial. Pero sobre todo me transmite solidaridad y humanidad en el gesto de los personajes, sobre todo del policía.

El final me deja un poco así de aquella manera porque habiendola visto ya 2 veces, cada vez he pensado una cosa, creo que el director lo ha dejado abierto para que cada uno piense una cosa.
        
            