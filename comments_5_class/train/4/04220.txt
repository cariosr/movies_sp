
                      Actuación: 7
Dirección: 7
Guión: 7
Cinematografia: 7
Música: 7
Efectos visuales: 10
Efectos sonoros: 7
Escenario: 10
Edición: 10
Vestuario: 10

Calificación: 75/100

Muy buena película de acción, tiene mucho carácter expresivo pero pocas ganas de sobresalir. Esta muy entretenida pero es imperfecta en varios aspectos técnicos.
        
            