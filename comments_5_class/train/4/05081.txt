
                      En esta película habla la historia de hechos ocurridos en Londres, contados de una forma que, no sé sí serán fieles a la realidad, pero si están contados con alma. Una película divertida, entretenida, solidaria, reivindicativa y sin tabúes, que no hay que pasar por alto.
        
            