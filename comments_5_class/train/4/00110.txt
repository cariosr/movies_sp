
                      Comedia gamberra e irreverente donde las haya, con un humor grueso y escatológico, moderna y actual. No me reía tanto con un largometraje desde "Algo pasa con Mary", y al final ha llegado incluso a emocionarme. Un cuento adulto, una fabula cargada de influencia e iconos ochenteros (Flash Gordon, El imperio contraataca, Indiana Jones y El coche fantástico). Original, divertida y entretenida, muy en la línea de los hermanos Farrelly.
Una reflexión sobre el miedo a tomar responsabilidades, el miedo a madurar, el miedo a perder esas libertades que tenemos en nuestra niñez y posterior juventud. John Bennet y el oso de peluche Ted son lo que hoy en día serian Peter Pan y Campanilla, o Pinocho y su Pepito Grillo, lo real y lo irreal, el cuerpo y la conciencia.
En resumen: Largometraje para pasar una tarde-noche divertida, rodeado de amigos y con unas birritas.
        
            