
                      La complejidad que las relaciones entre los personajes posee así como el estilo narrativo con recursos atípicos que el director le da al filme, provocan que se mantenga la tensión de forma que el espectador no pueda despegarse de la pantalla. Es sin duda una revelación de la época que se esconde entre sus diálogos.
        
            