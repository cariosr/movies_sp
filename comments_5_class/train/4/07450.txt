
                      Quizá no sea una película cuidada en su formato, pero la historia que narra, la actuación y la sensibilidad de la directora consiguen emocionar y llevarte a una época que muchos vivimos y, quizá, el paso del tiempo nos había hecho olvidar.
        
            