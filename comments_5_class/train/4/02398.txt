
                      Una fotografía nítida, una música bastante interesante y, por encima de todo, unas interpretaciones impecables acaban configurando una admirable obra de cine negro, histórico y de espionaje en torno a toda una red de conspiraciones y secretos que determinaron una época clave para entender la evolución de un país central en la historia de Europa. Una notable crónica humana e histórica que no os dejará indiferentes. (6,5/10)
        
            