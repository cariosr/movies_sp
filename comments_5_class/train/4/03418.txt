
                      Spiderman: Lejos de Casa (2019).
“Inquieta vive la cabeza que luce la corona."
Comienza la segunda era del Universo Cinematográfico de Marvel con la nueva aventura del Hombre Araña, encarnado una vez más por el joven Tom Holland. Tras las cámaras repite como realizador Jon Watts, con una historia más juvenil y desenfadada que las últimas de los Vengadores -no olvidemos que nuestro héroe sigue siendo un adolescente de instituto desde tiempo inmemorial...

Junto a Holland podremos ver a secundarios ya habituales de Marvel, esto es; Marisa Tomei, como la tía de Peter Parker, Jon Favreau (director también, como es sabido, de las cintas de Ironman e íntimo de éste en la ficción), y los componentes de SHIELD, interpretados por Samuel L. Jackson y Cobie Smulders. En el papel del personaje 'Mysterio' tenemos a Jake Gyllenhaal. En cuanto al resto de secundarios que arropan al héroe lanza-telarañas, podríamos decir que es donde más “flojea” la cinta (demasiado grotescos).

Dos hechos a tener en cuenta: 
*Por un lado, la cantidad de países que visita la producción del filme (Italia, Austria, Inglaterra, España, Chequia...). 
*Por otro lado, el impresionante número de compañías dedicadas a los efectos especiales que trabajan en el largometraje de Disney (desde Sony hasta ILM entre otras).

La película, como no podía ser de otra manera, está dedicada a la memoria de uno de sus creadores, el gran Stan Lee. 
Recomiendo tener paciencia tras el final para poder visionar dos escenas postcréditos.

Raúl Cabral.
https://www.filmaffinity.com/es/film978970.html
        
            