
                      Son comentarios de alumnos de 1ère (15-16 años) en un instituto de Francia. Hemos estudiado el fenómeno de las maras y vimos "La Vida Loca" en el cine de Longwy.

Adrien
Pienso que es una película documental interesante para comprender la situación de algunas ciudades en el mundo, muestra la violencia en el mundo.

Quentin
Esta película es muy violenta, hay muchos muertos durante toda la película. La pobreza de los habitantes, el episodio del ojo, los llantos de la familia son chocantes. representa bien la realidad de los dos bandas, es muy triste.

Gautier
Esta película representa la vida de los salvadoreños. Es interesante ver la violencia y la vida de ese país para ver la suerte que tenemos aquí!!

Céline
Es muy triste ver niños que viven en este ambiente que es violento. ¿ Cómo pueden ser tan violentos y al mismo tiempo tener el espíritu fraternal? Para mí, es incomprensible.

Bastien
Es una película muy triste, muchas personas mueren. Hay mucha pobreza también y los jóvenes no tienen niñez.
        
            