
                      Me cuesta trabajo recomendar no perderse una película que califico con 3.5, pero esta es sin duda una de éstas. Fresca, divertida e irreverente, ya hacía falta un personaje así para los fanáticos de los filmes de superheroes.
Quizás no confiaron mucho en el resultado y le escatimaron al presupuesto y, por lo tanto, a muchas facetas importantes de un filme de este género -efectos especiales, personajes de apoyo, etc,etc,etc- y aún así salió un muy buen producto.
No me perdería la segunda película, a la cual espero le apuesten con fe y con recursos. Tiene mucho potencial el personaje.
        
            