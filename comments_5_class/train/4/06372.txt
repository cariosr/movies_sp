
                      Tarantino nos trae, una de sus mejores obras, la película que siempre quiso hacer.
Y no era una película histórica al uso, ni obedece reglas cronológicas. Es más, él manda la historia a tomar por culo y se inventa una. No estamos viendo la ocupación de los nazis, ni a Hitler o Goebbels, ni la caza de los judíos por Francia a través de un libro de historia, sino a través de los ojos y la cabeza de Tarantino. Eso sí, con grandes referencias a la época y, sobre todo, cinematográficas.
Y tengo que decir que, aunque Tarantino no sea de mis directores favoritos, esta obra suya es una maravilla.

Nos habla de un grupo de soldados de descendencia judía, apodados "Los Bastardos", que están dispuestos a joder a los nazis, al mismo tiempo que una chica, Shosanna, busca venganza por el asesinato de su familia a manos del coronel Hans Landa. Esa es la historia, pero hay que culpar lo mal que se vendió el largometraje. La parte verdaderamente importante e interesante no es la de "Los Bastardos", sino la de la joven que lleva el cine, pero la publicidad resultó muy engañosa en su momento. La verdad es que me esperaba un filme de guerra con mucha acción y esto está más cerca del Thriller de espías y el cine negro. Pero bueno, lo importante es disfrutar de ella, y sí que se disfruta. 

La gran fotografía de Bob Richardson, esa banda sonora magnífica llena de piezas de Ennio Morricone, la ambientación y todo lo que se refiere a la escenografía son aspectos tratados a la perfección. Como están recreados los bosques, poblados y sectores agrarios de la Francia durante la ocupación Nazi es algo muy destacable. Y como no sus inmensos personajes y diálogos.

Las actuaciones son enormes, destacando principalmente a Christoph Waltz, que está impresionante, como Hans Landa, con una actuación soberbia e inolvidable. 
Brad Pitt, lo veo sobreactuado y forzado en su papel, aunque no esta mal del todo. Mélanie Laurent, está fantástica, Diane Krugger, hace un papel bastante digno, y por último Michael Fassbender, está francamente bien lo poco que sale, siendo además participe de una de las secuencias mejor realizadas de la película.

En definitiva, muy buena película, de mis preferidas de Tarantino.
        
            