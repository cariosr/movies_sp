
                      Dentro de que no es una película brillante, está perfectamente ejecutada. No va de nada, no lo pretende y no lo consigue, y eso es lo bonito que tiene. Emotiva, aburrida y tensa, como todas las de espías, si buscas pistolitas, explosiones y pechos, definitivamente no la veas. Lo más destacable el personaje del espía ruso, siniestro y reservado, que te deja la piel de gallina. Muy bien contada. Recomendable.
        
            