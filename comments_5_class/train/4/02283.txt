
                      Si existe una meta-secuela, entonces este seguimiento flagrantemente tonto y autoconsciente de la 21 Jump Street de 2012 lo es. ¿Cómo es eso? Porque 22 Jump Street explota todo el concepto de la franquicia cinematográfica y luego estudia los fragmentos para descubrir por qué las audiencias siempre están jadeando para ver la misma maldita cosa una y otra vez.

Lo siento. No estoy tratando de hacer un papel de término de una farsa de verano desechable. 22 Jump Street es malditamente divertido, a veces escandalosamente. Se ríe de su propia lógica tonta y nos invita a la diversión.

Mire el título: 22 Jump Street se refiere a la nueva dirección donde los policías encubiertos Schmidt (Jonah Hill) y Jenko (Channing Tatum) aún reciben órdenes ladradas por el capitán Dickson (un cubo de hielo de valor incalculable). Pero la sede se ha movido a través de la calle. 21 Jump fue liderado por un Jesús coreano. La nueva sede de alta tecnología en 22 Jump presenta a un Jesús vietnamita afuera.
        
            