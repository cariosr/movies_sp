
                      En mi opinión como espectador,después del sensacional éxito de la innovadora Guardianes De La Galaxia,todos esperábamos maravillas de la secuela,la pregunta es¿logró James Gunn superar su obra maestra?Guardianes De La Galaxia Vol.2,al igual que Avengers:Age Of Ultron,no consigue superar completamente a la entrega original,y al igual que la anterior nombrada,sigue una gran película del U.C.M,divertida trama,divertido elenco,¿que más se puede pedir?Nuevamente,Marvel consigue capturar a su público con humor y diversión,pero la trama no termina siendo tan llevadera y fresca como en su primera entrega,igualmente,tdos los personajes se vuelven hasta más divertidos,e incluso la nueva Mantis,consigue la aprobación del público,haciéndola una más del grupo de foragidos.Algo que definitivamente supera a la primera,es el villano Ego,el padre de Star-Lord,interpretado por el gran Kurt Russel,el personaje logra imponer y hasta llegar a simpaticar lo suficiente,haciéndolo un digno villano.Los efectos especiales y las escenas de acción,como siempre,regresan con la mejor calidad del cine.Sin más que decir,la secuela de los Guardianes tal ves no supere la calidad de la original,pero cumple completamente con entretener y divertir a todo aquel que la mire,totalmente recomendable para pasar un genial rato y para los amantes de la acción y los súper héroes.

Puntos a favor:evolución de los personajes,gran villano,entretiene como pocas,efectos especiales,escenas de acción.

Puntos en contra:Trama no tan innovadora y fresca como la primera entrega.

Mi calificación:8/10
        
            