
                      Enorme film el que nos brinda el amigo Spielgberg. Capaz de dotar de sentimientos la cara inexpresiva de un caballo. 
Genio. Por poner una pega, echo de menos algo de sangre en las escenas bélicas. Un 8,5.
        
            