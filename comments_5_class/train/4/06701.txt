
                      La historia es tan devastadora que, sin más artificios que los de una gran actriz y un buen guión, consigue llenar al espectador de sentimientos, para bien o para mal.
        
            