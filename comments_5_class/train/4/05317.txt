
                      Este es el ejemplo perfecto de no juzgar un film por la apariencia. " Zombies party" es uno de los títulos mas estúpidos que he oído en mi vida. Y el trailer representaba casi el mismo nivel de estupidez de el titulo. Pero la película es hilarante. No solo eso, de algún modo la película no se separa del genero zombies a pesar de ser una comedia. Esta es la primera de la trilogía de Edgar Wright y es un buen comienzo.
        
            