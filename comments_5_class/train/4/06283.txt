
                      El Vicio del Poder (2018).
“Temed al hombre silencioso..."
Adam McKay, quien dirigiera a Steve Carell y Christian Bale en "La Gran Apuesta", es el responsable de este interesante biopic -y de su guión- protagonizado por el rey de la mutación; el mismo Bale.

“Cuando tienes el poder siempre hay alguien que quiere arrebatártelo."
El Hombre Murciélago de Nolan vuelve a hacerlo. Aumentó 18 kilos en unos meses para transformarse de manera magistral en el que llegara a ser el todopoderoso Dick Cheney, vicepresidente del gobierno autoritario de George W. Bush (aquí interpretado por Sam Rockwell) hace un par de décadas.

“Por fin esos hombres superricos y de raza blanca encontraron a alguien que los representara."
El burócrata no es presentado como un monstruo (aunque en realidad lo fuera), sino como un republicano duro e impasible. Curiosamente, y aún siendo una cinta estadounidense, la película se estrena en un momento muy oportuno en nuestro país (y región), pues el extremismo populista y ultraliberal que se respira en esta historia ambientada en la Casa Blanca comparte muchas similitudes con el tripartito que en estos días triunfa en el sur de la 'piel de toro'.

“Tú eres un hijo de puta. ¿Cómo te has vuelto tan cabrón y desalmado?”
Como suele ocurrir en el cine norteamericano, uno de los valores del filme es la interpretación, no solo del ganador del Globo de Oro por este trabajo (Bale), sino por los secundarios que lo acompañan, esto es: Amy Adams, como su astuta esposa; Steve Carell (el mentor de Cheney), Naomi Watts que solo la vemos aparecer en las pantallas televisivas de las noticias de la conservadora Fox News; y hasta una metafórica interpretación por parte de un surrealista camarero con los rasgos de Alfred Molina que deja claras las cosas...

“Si buscas que te quieran, dedícate mejor al cine."
Críticas al sistema, a la ilegal guerra de Iraq, a la violación generalizada de la Convención de Ginebra y los Derechos Humanos por parte del despiadado gabinete del componente más asesino del 'Trío de las Azores' aparte... lo que es auténticamente asombroso y espectacular es el maquillaje no solo del camaleón de su generación, Christian Bale, si no del resto de reparto que lo acompaña.

En resumen: muy interesante para los adeptos a la Historia política (especialmente, la reciente), a las manipulaciones en masa y, desde luego, al cine de actores. 
Estamos ante una triunfadora en lo que a premios se refiere... y los que quedan por ganar...

Raúl Cabral.
https://www.filmaffinity.com/es/film170500.html
        
            