
                      Alberto Rodríguez, director de La Isla Mínima ha ido creciendo a pasos agigantados película a película. Recuerdo sobre todo 7 Vírgenes y Grupo 7 como dos obras muy reconocibles en su trayectoria, pero con esta película ha rozado la perfección..

Desde el primer instante te atrapa en una nebulosa entremezclada de luz y oscuridad, en el que las marismas del Guadalquivir son la ambientación perfecta para lograr este clima de desasosiego, tenebrosidad y esperanza. 

Dos policías, dos vidas tan diferentes, dos ideologías tan radicalmente opuestas, pero, cruzadas en el devenir de la vida para tratar de resolver un caso de desaparación de dos hermanas en un pequeño pueblo de las mencionadas marismas del Guadalquivir.

La atmósfera sombría y la tensión acompaña a cada instante, el transcurrir de los días, el día a día de un pueblo español aletargado en sus costumbres, de unos personajes oscuros y sombríos.. Un thriller policiaco que engancha desde el inicio excelentemente ambientado y acompañado por una soberbia fotografía y aderezado con unas interpretaciones magistrales de su dúo protagonista, Raúl Arévalo y Javier Gutiérrez, los dos policías enviados a resolver el caso, las dos caras de dos épocas tan distintas, la cara de la vieja guardia franquista y la de aquel que rechaza todo aquel pasado, de dos métodos antagonistas pero con un delgado hilo de unión, de dos vidas tan dispares.. Existe una delicada química entre los personajes, unos gestos, tics, miradas cómplices, una unión pese a estar tan alejados uno del otro.

Resulta complicado encontrarle un pero a la película porque es un puzle perfectamente encajado sin apenas grietas. Excelentemente filmada, enigmáticos exteriores, fotografía destacada, personajes oscuros y un entresijo de pruebas y hechos aderezados con una atmósfera oscura y magnética.
        
            