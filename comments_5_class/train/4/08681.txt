
                      Recomendada para los milenials

Una película que te sumerge en un mundo futurista del videojuego basado en juegos clásicos y de la actualidad. La música nostálgica encaja de maravilla.

Lo mejor: Conocer Oasis, el videojuego. Ya que a mí entender, como sociedad, nos estamos dirigiendo hacia un mundo similar.

Lo peor: Quizás donde más patina la película es en la realidad (fuera de Oasis). Interpretaciones justitas.
        
            