
                      Al Pacino esta sublime interpretando a un policía atormentado por la conciencia y el sentido de culpabilidad, una historia de suspense a la atura de la reputación del director Nolan, que no solo consigue engáncharte si no que logra que un actor tan de comedia cómo Robin William encaje en el papel de perturvado.
        
            