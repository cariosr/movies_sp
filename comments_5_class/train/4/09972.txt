
                      Yo no soy muy fan de Torrente, pero sin duda esta entrega me ha sorprendido. La mejor de la saga! La trama tiene coherencia, un final más armado que muchas películas "buenas", y, si te gusta el humor castizo e incorrecto de Torrente,  risas aseguradas!
        
            