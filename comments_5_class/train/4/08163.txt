
                      Es una película muy humana, al estilo de su director, siguiendo la línea de 'Million Dolar Baby' o 'El gran Torino'.
 Quien espera ver un filme de espíritus se ha equivocado de elección. Tiene un principio espectacular y unos personajes muy conseguidos.
        
            