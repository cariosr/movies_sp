
                      Fui a ver esta película la cine de verano porque en su momento, cuando se estrenó, leí favorables críticas que la anunciban como el posible despegue del cine negro en español y... menuda decepción. Es verdad que la fotografía está lograda pero poco más. El argumento es típico, a cada momento sabes lo que va a pasar en los minutos siguientes y a ratos resulta hasta aburrida.
        
            