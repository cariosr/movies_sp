
                      Nueva comedia de acción, ‘American Ultra’ es la nueva víctima de ese problema, ya que la cinta protagonizada por Jesse Eisenberg y Kristen Stewart no consigue que su apuesta por convertir a Jason Bourne en un superfumado funcione. ¿El motivo? La parte cómica funciona bien, pero la acción, sin llegar a ser una catástrofe, rompe el tono casi paródico y te saca de forma constante de lo que estás viendo, hasta tal punto que hace que el final sea negativo.
        
            