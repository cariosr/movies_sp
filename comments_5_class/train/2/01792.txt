
                      Ladrones de bancos que dejan el dinero en la beneficencia. Unos pocos tiros y muchísima historia que consiguen aburrir a mas no poder.

Todo empieza con un grupo de atracadores robando un banco que casualmente el dueño de dicho banco es Bruce Willis, el FBI es el encargado de atrapar a los ladrones (en sus filas esta Dave Bautista), tras averiguar que la única huella en toda la escena del crimen es la de un muerto intentan obtener mas información de los atracadores. En uno de los atracos consiguen matar a uno de los ladrones y tras identificarle consiguen nuevas pistar para su caso. Y pista tras pista la película pasa y se acaba.

Yo la vi por Bruce Willis y Dave Bautista, pero el papel de ambos en esta película no llega ni a la altura de las pelis de acción que ambos tienen a sus espaldas. No la recomiendo, muy larga y tiros los justos.
        
            