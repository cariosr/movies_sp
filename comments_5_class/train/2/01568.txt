
                      Demasiado Disney y poco Burton, y, el poco que hace acto de presencia, trasnochado y repetitivo. Si esperas ir a ver "Alicia en el país de las maravillas", que no te engañe el título, porque desde luego la historia es otra ¡y qué otra!
El sombrerero loco da un miedo terrible y la reina de corazones es de lo más ridícula. En general, la estética feísta y carente de la habitual estitización del diseño de los personajes de Burton dan al traste con la parte visual, que podría en parte subsanar la profanación del cuento original. Poco recomendable, menos aún en 3D.
        
            