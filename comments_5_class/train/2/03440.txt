
                      Los actores representan su papel muy bien y de forma muy creíble, sobre todo Keira Nightley. Probablemente a los más entendidos en cine, que tienen esa sensibilidad para apreciar lo que al resto se nos escapa, les gustará esta película. A mi mujer y a mí en cambio la trama nos pareció tan aburrida que la acaba convirtiendo
 en un auténtico ladrillo. Se hace tremendamente pesada. Si vas a entretenerte, ni te plantees verla.
        
            