
                      El realizador judío nos muestra aquí y una vez mas un conflicto bélico; el sinsentido de una guerra.
Sin duda es un original drama de animación por el cual se nos demuestra que no es necesario un documental convencional para hacernos ver la realidad sino que ,a través de un largometraje de dibujos sentimos igual y podemos llegar a emocionarnos de igual modo.
Aun así , puntualizar una vez mas que ,es un tema tratado una y otra vez y que en muchos momentos la película resulta pesada y altamente lenta .
        
            