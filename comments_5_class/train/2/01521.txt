
                      Efectos visuales muy potentes, a nivel técnico es una pasada. Parece un documental.
Ahora bien, como película me dejó vacío. Los personajes son tan realistas que no tienen expresión facial, mientras que sus voces sí son muy expresivas, lo cual crea un contraste que quita credibilidad. Es muy raro ver cantando a los animales, sin transmitir ni una sólo emoción.  Los números musicales mucho más flojos que la original y la canción de Scar, "Be prepare", es un despropósito (cuando es de las mejores en la original).

Por otro lado, es una película que no aporta nada nuevo, siendo prácticamente un calco de la original.

Recomendable por los efectos, por el resto quedémonos con la original que es una auténtica maravilla.
        
            