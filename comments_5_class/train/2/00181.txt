
                      Futil intento por crear una nueva comedia británica que, combinando un humor más o menos blanco y el sexo como catalizador de situaciones cómicas, pretendía ganarse al público. El resultado fue un fiasco que pasó totalmente desapercibido. Y razones hubo.

La más llamativa quizás sea que la película, simple y llanamente, aburre. Así de sencillo. Ni Amanda Pays ni Mark Frankel, ese pretendido trasunto de Rob Lowe, logran convencer al espectador de la química que los guionistas idearon para sus personajes, la cual, dicho sea de paso, es nula. Los personajes no encajan el uno con el otro y no se les ve formando una buena pareja que se complemente. 

Por otro lado, el comportamiento de Pays es tan violento e irracional que es inverosímil que pueda ser una brillante mujer de ciencia, tal y como nos la quieren vender. Más bien parece una tarada con brotes agresivos que rezuma inmadurez. Ello hace imposible mantener la suspensión de la incredubilidad.

Tampoco podemos olvidar como, a lo largo de la película, se nos quiere convencer de que Pays es una mujer tan atractiva y arrebatadora que el protagonista no puede evitar obsesionarse con ella. El problema es que Pays, aunque guapa, no es ese pivonazo espectacular necesario para justificar el comportamiento de Mark. No es la actriz adecuada para ese papel, vamos. Un error de casting al que hay que añadir que tampoco es que la británica se desviva por darle vida a su personaje. Se la ve apagada y con poca vida. En consecuencia, la película no funciona.

Lo más curioso es que en el cartel de este trabajo se la promociona con una cita de terceros, según la cual se trata de lo mejorcito de la comedia británica después de "Cuatro bodas y un funeral". Hay que saber bien poco de cine o ser un mentiroso de campeonato para escupir tamaña falsedad. Porque, tal y como la historia ha demostrado, la película es un truño que no gustó a casi nadie y que ha caído en el olvido. Como muestra un botón: la película se estrenó en UK en febrero del 95 (véase IMDB), pero en España no se estrenó hasta... ¡agosto del 99! Es decir, cuatro años y medio después, en plena canícula y rodeada de subproductos de consumo rápido. A la distribuidora sólo le faltó echarle un trapo al cartel para no pasar vergüenza.

Otra anotación en la contabilidad en rojo de Amanda Pays quien, con este trabajo, puso fin a cualquier posibilidad de hacer algo potable en su filmografía. A partir de este punto fue un descenso a los infiernos a base de petardos lisérgicos en los que participaba sin el más mínimo sentido crítico. Siete años después lio los bártulos y abandonó el cine.

Puede pasar olímpicamente de este trabajo, que su vida no se verá afectada.
        
            