
                      Desde el sofá, mi crítica.
Película de espionaje basada en una obra de John le Carré, de intriga, donde diferentes departamentos del Gobierno Alemán en colaboración con la CIA nos hacen ver como controlan todo lo que pasa en Hamburgo. 

La película es lenta al principio, te hace sentir perdido, rancia y parca. No te esperas así los primeros 15 minutos. 

La actuación de Grigoriy el checheno que huye de todo, es muy buena. Perseguido y presionado es el caballo de este ajedrez. Una pieza clave que unirá a todas las partes.

Rachel Mcadams realiza una gran actuación, abogada defensora pro derechos humanos, que es parte de esta intriga de espías.

El protagonista principal Philip Seymor realiza una actuación suficiente. No brilla, aunque su papel de espía ilegal alemán, amargado y apartado por errores de,su pasado, navega entre los tiburones de otros departamentos de espionaje que no tienen compasión con el. Podía haberlo hecho mucho mejor.

Y de William Dafoe que puedo decir. Coherente actuación de un grandisimo actor del que esperaba mucho mas. Un banquero con conciencia y totalmente colaborador. Vamos cualquier otro actor podría haberlo hecho igual.

Una vez traspasado el meridiano de la película esta empieza a ponerse emocionante, donde empieza a mostrar lo que espera uno de esta película.
El director ha buscado en cuatro grandes actores que fueran la clave de esta película por separado, pero no creo que haya sacado mucho jugo de este grandísimo potencial. Un suspenso para el director. 

Tan solo una curiosidad, es que durante las escuchas hacen uso de auriculares Sennheiser, toda una coherencia en este mundo del espionaje que trascurre en la ciudad de Hamburgo - Alemania.

Todo en esta película sucede tras la pista de un hombre que ilegal y bajo la trama económica aporta dinero a terroristas.

En fin, una película de espías sosa, sin sabor, que te deja un sabor amargo, con ganas de mas. 
No he leído el libro de le carre pero seguro que esta película no le llega a los talones.

Un titulo demasiado grande para esta película y su actuación.
        
            