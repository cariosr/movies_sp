
                      Una auténtica auto-parodia es lo que es esta secuela, y así hay que tomársela.

En ningún momento la película se toma en serio así misma y eso se nota, se nota ya desde el comienzo. 
12 años más tarde del clásico film de 1974, Tobe Hooper decidió crear esta payasada (porque otro nombre no puede ser el más apropiado) y convertir a toda la familia caníbal y terrorífica de la primera entrega en una burla hacia sí misma. Desde Leatherface hasta el mismísimo abuelo todos son víctimas de esta desastrosa y cañera entrega (digo cañera, porque a veces el humor absurdo ligado a algunos momentos de motosierras son buenos).

Los protagonistas (2 por cierto) son, sino estúpidos, totalmente despreciables e irrisorios. La chica tiene un pase, pero el ¨poli¨ es de lo más patético que he visto.

Sobre el soundtrack y demás cumple de una forma normal sin muchas pretensiones.
Algo bueno a reseñar pueden ser un par de escenas tensas pero poco más.

Film para ver y automáticamente olvidar, un 3
        
            