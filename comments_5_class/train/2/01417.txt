
                      Algunos chistes buenos, pero la prometedora idea básica se queda sin explotar, y la película es bastante plana. Dejando el romanticismo a un lado, el reencuentro es bastante original y divertido, pero rápidamente se hace vacío. Para un telefilme vale, pero para el cine desde luego no!
        
            