
                      Secuela de "El Sorprendente Hombre Araña", dirigida por Mark Webb.
Fui a ver esta pelicula porque la primera no fue del todo buena, en comparacion con la primera trilogia, y queria probar si esta seria mejor y si el hombre araña se uniria a Los Vengadores.

Puntos positivos:
-La pelicula tenia mejor sonido que la anterior.

-Tenia villanos mas conocidos y no solo fue Electro.

-De todas las peliculas de Marvel, esta es la unica que eh visto con un final tan inesperado.

Puntos Negativos:
-La pelicula no es de mucha accion, mas bien parecia una historia de amor y sufrimiento, y la pelicula dura 142 minutos (2 Horas y 22 Minutos) asi que mas lo ocupaban en eso.

-Los efectos especiales no funcionaron aqui, se nota que ahora es una computadora peleando y no los actores.

-Los villanos nunca aparecen, Electro pelea 2 veces, el Duende Verde pelea solo en la escena final y Rinho aparece al final y no hace nada mas que disparar 3 misiles y termina la pelicula.

Resumiendo...la pelicula no es tan mala pero no entretiene como la anterior, se hace muy pesada y es una pelicula de amor y gracias a eso varios niños se durmieron en el cine.

Recomendacion...no se hagan la idea de que es como los otros hombres arañas, no tiene final secreto, no se une a los Vengadores.

Si quieren vayan a verla, pero no creo que sea de su gusto.
        
            