
                      Thor es una película de ciencia ficción dirigida por Kenneth Branagh. Está basada en el personaje de cómics Marvel, creado por Jack Kirby y Stan Lee.

Thor, el hijo de Odín, se prepara para heredar el trono de Asgard cuando se inicia una guerra contra los gigantes de hielo. Estos intentan recuperar un cofre que Odín les robó.
Los asgardianos ganan la batalla, pero Thor es desterrado a la Tierra por su arrogancia. Aterriza en Nuevo México, donde conocerá a la científica Jane Foster.

Esta, para mi gusto, es una de las peores películas de Marvel. El tiempo está mal distribuido a la hora de presentar la historia y al desarrollarla, y la narración es algo espesa.

Los personajes como Loki y Darcy tienen un mayor encanto que los más protagonistas, como Thor y Jane.
Chris Hemsworth mantiene la misma expresión de mal humor de principio a fin, incluso cuando la situación no lo requiere.

La película podría haber prometido mucho más, ya que la trama posee mucha sustancia; ambición, traiciones, venganzas... y mucha acción. Pero al final queda todo muy mal construido.
        
            