
                      Tantos años de Los Simpsons, han terminado cansando a mucha gente, porque, seamos sinceros, su humor ha ido empeorando con el tiempo, y por fin se decidieron a sacar una película, quizás para recuperar ese interés que la gente perdió por esta familia en cuanto le destronaron otras series como Padre de Familia, por poner un ejemplo.
Si analizamos este largometraje, se podría de ir simplemente que es como ver un capitulo muy largo de Los Simpsons, pero con muy poca gracia. 
Esta claro que esta película sólo surgió para contentar a los fans y mostrar a todos sus personajes en la gran pantalla de alguna u otra forma. 
Tiene alguna escena en la que te saca alguna sonrisa, e incluso puede resultar entretenida en otros, pero no deja de ser simplemente un capítulo largo, y aún así no estaría entre os mejores.
Hay determinadas escenas en las que el dibujo se mezcla con intentos de efectos 3D y eso es algo que ha ocurrido en otras películas como Las Nuevas de Dragon Ball, en las que es evidente que eso no queda bien, y en ciertas ocasiones hace que parezca que los personajes son de papel, en lugar de dejar que sea una película de dibujos animados que eso también gusta a la gente. Es un error que se ve mucho cada vez que vuelve a surgir una película de dibujos animados, que trata de compartir con películas con animación en 3D.
Por lo demás, no innova nada, la trama es una mezcla de parodia-homenaje a varias películas y aún así seguro que mucha gente salió del cine diciendo "Bah..."
Es un intento desesperado por recuperar fans, pero que se dió de morros con la realidad, y se quedó en un absurdo capítulo largo.
        
            