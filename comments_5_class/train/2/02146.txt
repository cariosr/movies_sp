
                      La payasa búlgara

El estornudo como elipsis tonal. Éste es el ingenioso recurso que utiliza “Espías” para mostrar la que parece ser su verdadera cara, que no es otra que la de mofarse del cine de espías de toda la vida. O quizá la que nos hubiera gustado que exhibiese durante sus abultadas dos horas de metraje. Porque en ese comienzo a lo James Bond, en esos créditos iniciales a lo 007, en ese Jude Law que se resarce del personaje al que nunca llegó a encarnar más allá de los rumores, había una buena premisa a explotar.
La había, porque es tomar las riendas del producto Melissa McCarthy y se acabó el ingenio. El reencuentro de la actriz con el director Paul Feig no es más que otro vehículo para su lucimiento personal, lo cual congratulará a sus defensores, pero irritará más que nunca a sus detractores. Lo que vemos es una sucesión de gags rematados por la verborrea de su protagonista. O lo tomamos o lo dejamos.

Una sucesión de gags que, no obstante, no siempre funciona. Da en el clavo cuando se limita a reírse de los tópicos de este tipo de propuestas, cuando expone a Law y Jason Statham al reflejo burlón de sí mismos, a la flema british caballeresca del primero y las fantasmadas de las que hace gala el segundo, cuando deja todo en manos de personajes secundarios tan potentes como los de Alison Janney o Peter Serafinowicz. Pero falla cuando trata de ofrecer el mismo tipo de humor del que hiciera gala el realizador en su propuesta más lograda, “La boda de mi mejor amiga”, con una galería de sketches de lo más irregular.

Pero aquí no está la carismática Kristen Wiig ante la cámara ni al guión. Y se nota. El libreto escrito por el propio Feig no es una maravilla y se torna previsible, a pesar de  ser capaz de arrebatar alguna que otra carcajada al espectador, o como mínimo alguna sonrisa cómplice. Su realización es efectiva en las escenas de comedia, pero totalmente caótica en las de acción, con un montaje que hace abuso del slow motion, y un metraje tan extenso que llega a hartar.

Y aún así, “Espías” acaba funcionando como comedia para matar el rato, como un entretenimiento veraniego superficial, pero de esos que se ven con comodidad y se olvidan con suma facilidad. Lo importante es disponer un escenario para que esa payasa búlgara que es McCarthy interprete su papel de siempre, con sus consabidos chistes hacia su físico y el patetismo de sus personajes. No recomendada para los que no le rían las gracias.

A favor: los momentos en los que se convierte en una parodia del cine de espías 
En contra: la duración y la irregular galería de gags
        
            