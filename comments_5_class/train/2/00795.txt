
                      Película Lentísimaaaa. A medida que va pasando la hora sólo podía pensar en qué se habían basado para darle tan buenas críticas. Si te gustan las películas con mucho entramado de espionaje, esta no es tu película. El final es inesperado pero muy malo, "tanto rollo para nada" es lo que piensas al salir. No es una película para gastarse dinero en ir a verla. El actor principal hace un muy buen papel y es lo único digno de mención de todo el film. Para mi gusto, bazofia de película.
        
            