
                      Demasiado densa. No tengo problemas con las películas largas, pero si encima es excesivamente lenta es que no hay por dónde disfrutarla. Además es larga a base de muchas escenas de relleno. La historia es difícil de seguir y los momentos de intensidad se cuentan con los dedos de una mano y son excesivamente cortos. Le salva alguna de estas escenas y algunas piruetas de la fotografía y la banda sonora pero... Hay que ver demasiada película para llegar a un final donde remonta un poco
        
            