
                      Un peñazo pseudofilosófico. Un pretendido poeta que no escribe un sólo poema completo en toda la película. Escenas callejeras con fotografía sofisticada que no salvan un hilo argumental inconexo. La banda sonora es buena pero se hace un uso abusivo de ella. Lo más salvable, la actuación de Victoria Abril. Las comparaciones son odiosas pero inevitables. El grandísimo actor Antonio Banderas no es Bergman, Woody Allen o Scorsesse
        
            