
                      Otra película de la que llegaba a esperarme muchisimo más, creía que me iba a encontrar con una comedia moralmente divertida y algo absurda aunque realmente lo es en algunas ocasiones.
Lo mejor: El montaje y los actores como Olivia Newton-John.
Lo peor: Que aunque sea una comedia pierde la gracia en algunas ocasiones.
        
            