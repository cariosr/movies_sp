
                      Para la primera mitad de la película podían haberse inventado algo mejor que la aburrida trama ganadera, y en la segunda mitad, al tiempo que los personajes se separan, la película termina siendo previsible.
Tampoco los actores contribuyen a mejorar el nivel general.Mientras que Hugh Jackman parece estar más en un anucio de una compañia de pantalones vaqueros que en una película, Nicole Kidman se mueve en la fina línea que separa lo simpático de lo insoportable.
        
            