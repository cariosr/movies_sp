
                      Una película que parece más de lo que da, la historia de amor carece de sentido, fría y chirriante... y sin más te deja con mal sabor de boca, un final que te pide a gritos no haber visto esta película. Por esta clase de películas seguimos no apostando por el cine español
        
            