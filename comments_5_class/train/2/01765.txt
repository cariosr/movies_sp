
                      Una película más que muestra que buenos son los americanos. El francotirador se me ha hecho larga por momentos. Las escenas donde está en Iraq mantenían el suspense pero lo fastidiaba cuando alternaba con las escenas domésticas. Si quieres ver una sobre la guerra mejor ve En tierra hostil ya que parecía casi una copia barata de la otra. Parece un producto sobrecalentado que alaba el patriotismo. Y todo esto siendo una peli de Eastwood deja que desear.
        
            