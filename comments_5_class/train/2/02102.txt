
                      La sexta entrega de la serie de películas más exitosa en la historia del cine mundial, llega para establecer los cimientos de las 
dos próximas cintas con las que concluirá la saga. Sin mucha emoción, pero con mucha hormona, Harry Potter se prepara con 
la historia del Príncipe mestizo para su batalla final.

En Harry Potter and the Half Blood Prince, la cotidianeidad de la escuela de magos Hogwarts pierde importancia y aunque se 
presentan algunas escenas (las mejores) de las clases de los jovencitos y sus actividades deportivas, la mayor parte de la cinta 
transcurre en los pasillos, los bosques y la oficina de Dumbledore (Michael Gambon) en donde Harry tendrá que prepararse 
para lo que el mago maestro considera “el juicio final”.

Una vez más el ganador del BAFTA y famoso director de televisión, David Yates, vuelve a recrear la vida de los adolescentes más 
famosos de la pantalla platinada. Daniel Radcliffe, Emma Watson y Rupert Grint, regresan con sus entrañables personajes de 
Harry Potter, Hermione Granger y Ron Weasley respectivamente, para sorprender más con la magia del poderío hormonal que 
con la que sale de sus varitas mágicas y pociones.
        
            