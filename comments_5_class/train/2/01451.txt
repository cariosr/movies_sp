
                      'Ouija: El origen del mal', pese a ser superiora a su predecesora, ha supuesto una decepción. Esperábamos una nueva 'Expediente Warren', pero la nueva producción de Blumhouse se ha quedado en poco más que 'Exorcismo en Connecticut'. Se agradece su estética y tono de exploitation setentera, pero también canta demasiado el ahorro de la productora en el presupuesto, con unos FX muy básicos que lastran el metraje.
        
            