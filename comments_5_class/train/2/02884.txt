
                      Tremenda decepción con lo último de Pixar. Adiós a las historias que te atrapan, al humor divertido e inteligente, a los personajes inolvidables. Esperemos que la siguiente recupere ese toque que en Brave no aparece durante las más de dos horas de película.
        
            