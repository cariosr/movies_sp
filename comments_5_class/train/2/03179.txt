
                      Ha supuesto para mí una de las grandes decepciones con el cine de Scorsese... Tanto la fotografía como la trama (que calé desde los 15 primeros minutos), me parecieron muy mejorables. Era predecible, y a medida que se desarrolla se desinfla más y más. Redunda en lo obvio, además, empeñándose en mostrarlo cuando no es en absoluto preciso.
Di Caprio, Mark Ruffalo y el siempre excepcional Max Von Sydow actúan de forma sobresaliente. También el estallido de la naturaleza en tormentas infernales está bien mostrado, pero poco más. La película pone una zanahoria frente al espectador que éste no se puede tragar de ninguna manera.
        
            