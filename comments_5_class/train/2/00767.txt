
                      Una buena historia necesita un buen guión, lo demás es accesorio. Esta película tiene excelentes efectos, grandes tomas, un escenario interesante, actuaciones decentes pero el guión es tan plano y convencional que no genera tensiones ni incertidumbre en el espectador. Tiene algunos elementos que se encuentran en "la huésped" pero que no logran explotar debidamente. Solo espero que no exista una sexta ola por el bien de los bolsillos de los cinefilos.
        
            