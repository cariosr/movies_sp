
                      Trama bastante buena y con mucha gracia pero que sufre un gran pinchazo y va deshinchándose rápidamente concluyendo con un CGI malísimo y una trama más del montón. La película cuenta con grandes actores y muy buenos cómicos, pero empieza tan bien, tan rápido que de repente te das cuenta que parece que estés viendo otra película, ya que baja el nivel, empiezan los diálogos absurdos y chistes malos. Comedia para echarse algunas risas, pero nada del otro mundo. Lo peor es el CGI del final, se ve que el presupuesto no daba más y tiraron de croma.
        
            