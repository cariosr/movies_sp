
                      Las películas de "Rocky" permiten que sus títulos sean independientes, pero aquí hay una sugerencia para un subtítulo del último "Rocky V: The Well Has Gone Dry".

Sugerir que la guionista y actriz Sylvester Stallone ha ido al pozo una vez con demasiada frecuencia quizás tenga cuatro películas tarde, pero "Rocky V" es tan estridente, tan exagerada, tan molesta que uno se pregunta por qué el director John G. Avildsen se molestó en tomar la asignación.

Tal vez, ya que ganó un Oscar por dirigir el primer "Rocky", Avildsen se sintió obligado a cerrar la serie.

Pero, y nunca pensé que diría esto, la dirección de Avildsen de "Rocky V" hace que la dirección de Stallone de las tres películas de "Rocky" se parezca a David Lean.

Decir "Rocky V" es torpe es subestimar, ya que cada actor tiene la oportunidad de parecer tonto y ninguno desarrollar ninguna dimensión.

Cuando, al principio, Rocky dice: "¡Ah, tú!" suena como auto-parodia

La pelea climática de "Rocky IV" se repite bajo los créditos, recordándonos que Rocky Balboa (Stallone) eliminó el relleno de una máquina de boxeo rusa (Dolph Lundgren) en la Unión Soviética la última vez.

"Rocky V" retoma la historia en el momento en que finaliza la pelea, ya que Rocky demuestra un daño motor y, desde luego, los médicos confirman el temor de que haya sufrido demasiados golpes en la cabeza y un daño cerebral.
        
            