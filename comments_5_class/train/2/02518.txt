
                      El personaje más hilarante de la película, sin embargo, no es interpretado por Will Ferrell o Mark Wahlberg, sino por el hombre de reparación de Hannibal Buress que se libra de la familia, interrumpiendo cada conversación en el momento más inoportuno con el diálogo más aleatorio.
        
            