
                      Si metiéramos en una coctelera a Viernes13, El Show de Truman, La Matanza de Texas y Jumanji, tendríamos algo así.
El comienzo da la sensación de una película totalmente dirigida para adolescentes, insulsa y vulgar pero aceptable, sin embargo, cuando entra en juego el experimento científico, el argumento cae en picado precipitándose en una espiral de irrealidad y surrealismo. Algunos diálogos no tienen la menor gracia, sarcásticos, carentes de sentido y fuera totalmente de tono. El final de la película es un absoluto caos, original y creativo, eso sí, siendo lo único que hace subir mínimamente el listón a este film.
        
            