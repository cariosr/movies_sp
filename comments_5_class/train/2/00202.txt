
                      Innecesaria, amarillista, violenta, no entretiene, desaprovecha el lenguaje poderoso de una industria por demás inmadura para obligar al espectador a buscar explicaciones que el director no aporta. Banaliza en una civilización que decae por exceso de vacuidad temas fundacionales, arquetipos que bien utilizados podrian enriquecer y que en este caso solo producen desazón y jartera. El director en su afán de seguir siendo un niño terrible de una industria decadente, solo muestra inmadurez existencial.
        
            