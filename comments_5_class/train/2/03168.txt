
                      ¡Tan mala que da risa! 
En serio, me he reído mucho y no me he asustado ni una sola vez y eso que me considero un cobarde.
Por lo demás, un guión típico de película de terror, una dirección casi cutre y unos personajes simples y arquetípicos que se engloban en la temática religiosa.
        
            