
                      Las películas de "Rocky" permiten que sus títulos sean independientes, pero aquí hay una sugerencia para un subtítulo del último "Rocky V: The Well Has Gone Dry".

Sugerir que la guionista y actriz Sylvester Stallone ha ido al pozo una vez con demasiada frecuencia quizás tenga cuatro películas tarde, pero "Rocky V" es tan estridente, tan exagerada, tan molesta que uno se pregunta por qué el director John G. Avildsen se molestó en tomar la asignación.

Tal vez, ya que ganó un Oscar por dirigir el primer "Rocky", Avildsen se sintió obligado a cerrar la serie.

Pero, y nunca pensé que diría esto, la dirección de Avildsen de "Rocky V" hace que la dirección de Stallone de las tres películas de "Rocky" se parezca a David Lean.

Decir "Rocky V" es una mano dura es subestimar, y cada actor tiene la oportunidad de parecer tonto y ninguno desarrollar ninguna dimensión.

Cuando, al principio, Rocky dice: "¡Ah, tú!" suena como auto-parodia

La pelea climática de "Rocky IV" se repite bajo los créditos, recordándonos que Rocky Balboa (Stallone) eliminó el relleno de una máquina de boxeo rusa (Dolph Lundgren) en la Unión Soviética la última vez.

"Rocky V" retoma la historia en el momento en que termina la pelea, ya que Rocky demuestra un daño motor y, desde luego, los médicos confirman el temor de que haya sufrido demasiados golpes en la cabeza y que tenga daño cerebral.

Otra pelea seguramente lo matará, nos dicen.

La esposa de Rocky, Adrian (Talia Shire), le grita cada vez que se le acerca un promotor de voz alta llamado Duke (Richard Gant), que es una especie de Don King sin pelo. Y se pregunta si Rocky no está tratando de revivir su vida a través de otra persona cuando él acepta manejar a otro luchador, Tommy "Machine" Gunn (boxeador profesional Tommy Morrison).

Pero Rocky, gracias a su cuñado Paulie (Burt Young), ha perdido todo su dinero. Sin embargo, en lugar de comprometerse haciendo respaldos de alto perfil, Rocky se siente obligado a trasladar a la familia a los barrios pobres del sur de Filadelfia. Y ve en Gunn la oportunidad de volver al juego y ganar algo de dinero legítimo.

Mientras tanto, Rocky Jr. (el hijo de la vida real de Stallone, Sage) está siendo golpeado en su nueva escuela y papá parece que no encuentra tiempo para ayudarlo a aprender defensa personal. Está demasiado ocupado entrenando a Gunn.

Seguro que no lleva mucho tiempo convertir a Gunn en un campeón, pero, por supuesto, Duke aparece y corrompe al joven luchador, haciéndolo contra Rocky. Y, como habrás visto en las vistas previas y en los anuncios de televisión, la pelea culminante esta vez es una pelea callejera en la víspera de Navidad.

Esa pelea final tiene algo de energía y hay un momento agradable con Mickey (Burgess Meredith), el difunto amigo de Rocky, que aparece como recuerdo, pero la mayoría de los intercambios de diálogos son rígidos e incómodos, la historia se revienta de sentimientos y es sorprendente lento, a pesar de una banda sonora llena de música rap, y las actuaciones en general son muy débiles. Incluso Stallone parece tener más sueño de lo habitual aquí.
        
            