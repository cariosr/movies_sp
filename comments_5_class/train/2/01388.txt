
                      Realmente ya había leído críticas de la película que no auguraban que me lo iba a pasar realmente bien viendo la película, como finalmente así fue. 
La película es en su globalidad, un producto hecho única y exclusivamente para recaudar dinero a través del elenco de protagonistas que por mucho nombre que tengan, no salvan un guión realmente flojo en el que tampoco pueden hacer mucho más. Decepción también en Deep, uno de mis actores favoritos que de un tiempo a esta parte está haciendo unos papeles más que indecentes para un actor con sus dotes.
No recomiendo nada verla, solo si quieres pegarte unas cabezadas después de comer.
        
            