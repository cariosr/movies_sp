
                      Woody, ¿cómo te puedes conformar con hacer esta película? Cuando alguien va a ver una película de Woody Allen es porque espera mucho más que una simple película, que es lo que has hecho esta vez, y por desgracia, últimamente tus películas me dejan indiferente o simplemente no me gustan.

“Magia a la luz de la luna” narra la historia de cómo un mago intenta desenmascarar a una falsa médium, y como este mago interpretado por Colin Firth cae rendido a los encantos de la médium, protagonizada por Emma Stone (que caiga rendido a Emma es lo único factible de la película). La historia de amor ridiculiza un poco el argumento convirtiendo la película en una banal historia de amor y dejando un poco de lado la labor de desenmascarar a la médium, que personalmente, centrándose en este apartado podría haber conseguido una historia mucho más entretenida. Sirva como ejemplo la película “Red Lights” del director español Rodrigo Cortés, aunque evidentemente hablamos de dos películas completamente diferentes.          Pero sirva la comparación para ver lo que es una buena película, de una que caerá en el olvido, porque eso es lo que hago con tus últimas películas, las olvido.

Para ser concluyente, es una película aceptable, e incluso entretenida en cierta manera, pero no para alguien como Woody Allen, quien es considerado un genio del séptimo arte.
        
            