
                      Con un comienzo donde, de una mánera rápida se desarrolla el cuento a modo de animación, y con un tono jocoso, impulsa hacia una película que no convence.
Continua durante toda su duración ese tono graciosillo, pero sin mucho éxito, siendo todo lo contrario que las otras dos versiones del cuento realizadas durante 2012.

Lo bueno que tiene, es que ésta, si que es para niños, al contrario que las otras versiones. 

Y lo malo, muy ñoña y lenta, con un guión sin jugo, sin gracia para adultos.
A parte de copiar a momentos a Alicia en el País de las Maravillas, aunque con muy poco éxito.

A destacar la actuación, dentro de ese guión que no da mucho de si, de Julia Roberts (madrasta). Y sobre todo, los grandes decorados y el vestuario (fue nominada al Oscar 2012 a Mejor Vestuario). También en ciertos tramos la fotografía.

A mi gusto, y con mi recuerdo del cuento de niño, tenían q haber realizado un mejor casting para asignar el papel de Blancanieves a una chica con más brío, un poco más guapa, más interesante.

Si me gusta, la parte del espejito, es diferente y original. Y supongo que para darle protagonismo a lo mejor del film, también agradezco la visión desde el punto de vista de la bruja/madrasta, no de Blancanieves.

Y no le saco mucho más
        
            