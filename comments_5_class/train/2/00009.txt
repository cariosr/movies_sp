
                      Con un comienzo trepidante y muy dinámico del robo de un banco, aparece un Nicolas Cage, en un papel de líder del asalto, y una perfecta planificación, muy del tipo 60 segundos pero sin Angelina Jolie.
Diez minutos muy emocionantes, le doy al Pause y me digo, igual merece una bolsa de palomitas.
Pues chico, de repente, se echa el freno de mano, todo se ralentiza, y se pasa a un tramo sentimental con la hija de por medio, lenta y típica, americanada 100%

Podemos decir que es acción de consumo rápido, pero lenta. Secuestro de la niña y típico malote, y dinero, y el padre que hace cualquier cosa...

En mi opinión, la bajada de calidad en las últimas películas de Nicolas Cage, es destacable.
En resumen, con unos primeros 15 minutos muy buenos, se desinfla hasta el punto de que no destaco nada de nada
        
            