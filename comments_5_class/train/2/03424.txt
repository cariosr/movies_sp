
                      Al igual que en otra obra maestra de la década de 1970, el Halloween de John Carpenter, a veces es difícil recordar qué gran película es el Rocky original porque su reputación se ha visto afectada por demasiadas secuelas innecesarias. Sin embargo, aún es imposible resistirse a los encantos de esta historia de perdedores, conmovedora y, a veces, muy divertida cada vez que la vuelves a visitar.

Antes de Rocky, su escritor y actriz Sylvester Stallone fue un actor de personajes poco conocido que apareció en películas de 1974 a 1975, como The Lords of Flatbush, Capone, Death Race 2000 & Farewell, My Lovely, e invitado a un episodio de 1975. de Kojak.

Al estudio le gustaba el guión de Stallone, pero quería una estrella mucho más establecida como Ryan O'Neal o James Caan para interpretar a Rocky Balboa, un boxeador que luchaba y pasaba a un paso de distancia. Pero Stallone estaba decidido a interpretar a Rocky por sí mismo y se negó a vender su guión a menos que pudiera interpretar al personaje principal. United Artists aceptó a regañadientes, y nació una nueva estrella.
        
            