
                      Ted es una comedia dirigida por Seth MacFarlane, director de Mil maneras de morder el polvo.

John Bennett (Mark Wahlberg) pidió de pequeño como regalo de Navidad un osito de peluche. Éste recibe a Ted (Seth MacFarlane), un peluche con vida que se convierte en el mejor amigo del niño. Veintisiete años después ambos siguen viviendo juntos, pero cuando John intenta afianzar su relación con Lori (Mila Kunis), comprende que es hora de desprenderse de Ted.

Como todo lo que ha hecho hasta el momento MacFarlane, esta película es irreverente y grotesca, con un humor simple, a veces casi forzado.
La idea está bien, un oso de peluche que cobra vida y acompaña al protagonista a lo largo de toda su vida, pero ahí se acaba lo bueno.
Por parte del reparto, no hay ninguna actuación que merezca la pena destacar positivamente.
Personalmente es una de esas películas que no recomendaría a nadie, y menos aún con el inminente estreno de la segunda parte, que será más de lo mismo.
        
            