
                      ¡Esta película no es buena¡ ¡Es buenísima! Original, ingeniosa, diferente a todo lo que estamos acostumbrados. Una película que parece al principio que no será gran cosa pero que alcanza cotas de maestría difíciles de lograr y llega a ellas de un modo increible. Entretiene, divierte, crea misterio, tiene un poco de drama y su buena parte de risas...
Completa en todos los sentidos y no parece excederse en medios ni abuso extremo de efectos especiales, largos dialogos...etc.
Me sorprendió muy grata y sinceramente. Disfruté en la butaca como hacía tiempo que no lo hacía.
Los actores son prácticamente desconocidos pero sus interpretaciones son brillantes. Sobra decir que el infravalorado Nicolas Cage está realmente bien en su papel de superhéroe vengador atípico.
En conclusión: buenísima. Gracias al cine por darnos estas joyas de vez en cuando.
        
            