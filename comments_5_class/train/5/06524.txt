
                      Harry Potter y la piedra filosofal es una película de fantasía dirigida por Chris Columbus, director de 'Solo en casa', 'Señora Doubtfire' y 'Percy Jackson y el ladrón del rayo'.

Harry Potter (Daniel Radcliffe) vive con sus ordinarios tíos y su insoportable primo desde que sus padres murieron en un accidente de coche, cuando Harry solo tenia un año. O por lo menos esa es la versión que le dieron sus tíos, porque cuando cumple 11 años, se entera de que es un mago, y de que tiene plaza para estudiar en el colegio de magia más prestigioso, Hogwarts.

Además de contar con un reparto de actores secundarios legendario, entre los que se encuentran Maggie Smith, Julie Walters y Alan Rickman, los pequeños protagonista no lo hacen nada mal. Dejando a un lado al trío protagonista, destacan el torpe y rechoncho Matthew Lewis y el mordaz Tom Felton.

Los efectos especiales están muy conseguidos, que junto con la banda sonora consiguen transportarnos al mundo mágico que los mismos personajes están viviendo.

Es una muy buena adaptación del libro del que nació, escrito por J.K. Rowling.

La película recibió 13 nominaciones a diferentes premios, 3 de ellos a los Oscar; mejor vestuario, banda sonora (de John Williams) y mejor dirección artística.
        
            