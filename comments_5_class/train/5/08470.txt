
                      Después de 15 años de ausencia en el cine con Lara Croft nos llega a la gran pantalla “Tomb Raider” basada en el reboot del videojuego de 2013. La pelicula cuenta los origenes del personaje basandose en su ultima reinvencion para adaptarse a los tiempos modernos con una Lara mas humana y vulnerable. La pelicula aporta a grandes rasgos una mezcla entre los dos últimos videojuegos (Tomb Raider y Rise of the Tomb Raider) extrayendo algunas ideas de ambos y mezclandolo en una pelicula para entregar al gran publico. Destaca la excelente interpretación de Alicia Vikander en el papel de Lara con una historia de aventuras al mas puro estilo clásico que tanto gustará a los amantes del genero. Una pelicula muy entretenida que con sus escenas de acción te hará sentir que desearás sabes mas detalles sobre el personaje de Lara. 
Positivo: La interprtacion de Alicia Vikander es perfecta como Lara Croft
Negativo: Se echa un poco de menos los elementos sobrenaturales, místicos y magicos que caracterizan a la saga.
Muy recomendable.
        
            