
                      O te encanta o no te gusta. A mí me encantó. Un musical que tiene un soplo de aire fresco y a la vez un guiño al cine del pasado. Emma W. debe ganar el Oscar como actriz protagonista y la película, la música y la fotografía también, por lo que se va a llevar bastantes premios porque es de esas que gusta a la academia. Tienes que verla si te gusta el cine. Si vas a que te entretengan, ve a otra película.
        
            