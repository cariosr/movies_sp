
                      Muy buena película. Las 2 horas que dura se me ha pasado en un suspiro. No es perfecta, pero me fui con muy buen sabor de boca y con ganas de más. Recomiendo que no la comparéis con las pelis de marvel (más bien Disney/marvel) que están hechas para niños/as (aunque también me gustan), porque es otro universo y mucho más fiel a los cómics.
        
            