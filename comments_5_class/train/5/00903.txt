
                      Una historia bien contada, bien actuada y bien dirigida. El tema es fuerte y el relato y las actuaciones logran emocionar tanto para abajo como para arriba. SI bien es una película que tal vez se vea con cierta sensación de angustia en el pecho (por momentos), la considero imprescindible.
        
            