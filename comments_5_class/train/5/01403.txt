
                      Gran metáfora de la egolatría. Hay quien necesita que le lean una historia línea a línea, se pierde si tiene que dejar su mente volar...
Sorprendente y poética, surrealismo en estado puro. Bien rodada, bien montada y muy bien interpretada.

¡No apta para adictos al blockbuster!
        
            