
                      El cómic "Wonder Woman" fue creado por William Moulton Marston en 1941, un año poco probable para una superhéroe femenina. La Segunda Guerra Mundial estaba en pleno apogeo, y en el mundo de DC Comics, Batman y Superman reinaron de manera suprema, y ​​luego ella vino, luchando con "amor, en lugar de odio", según Moulton Marston.

Imagínese lo nuevo y refrescante que todo el concepto debe haber parecido a las mujeres estadounidenses. El movimiento feminista tardaría otros 20 años en comenzar, y otros 35 antes de que viéramos a la actriz Lynda Carter interpretando a Wonder Woman en la televisión.
        
            