
                      Sin duda alguna, una obra de devoción para todos aquellos espectadores inquietos ante la incertidumbre de la vida. Una película que no permanece en la superficie, sino que se atreve a descubrir y a explorar nuevos límites del ser humano. 
Christopher Nolan nos presenta de este modo, una excepcional y sublime obra cinematográfica futurista que reivindica el amor por encima de cualquier otra realidad.
Obra maestra.
        
            