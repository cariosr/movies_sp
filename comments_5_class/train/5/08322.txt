
                      ESPECTACULAR. Una de las mejores películas que he visto jamás. Con unos efectos de la cámara y paisajes impresionantes, además de la increíble interpretación de DiCaprio. Nominado al Óscar y merecedor de él, completamente. Si no lo gana esta vez, me resulta casi imposible que lo consiga en otra ocasión, porque creo que es dificilísimo que se supere a el mismo en otra película como está o de cualquier otro estilo.
        
            