
                      Una película que vi con bajas expectativas, pero una vez que la termine me di cuenta del nivel de esta obra maestra, es un film que toma la dureza de la realidad que hoy en día hay en el medio oriente, como es que viven las familias con el miedo a ser separadas, ese no es el punto principal de la película, toca temas como el amor fraternal y el perdón combinando con rencor, es un film que te deja sin palabras.
        
            