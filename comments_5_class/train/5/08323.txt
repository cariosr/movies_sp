
                      Una película de culto y la mejor de Tarantino. Sin duda una de mis 10 películas favoritas de todos los tiempos. Con escenas inolvidables y actores inolvidables de la historia del cine. Me encanta el guión de la película y está muy bien dirigida por un gran director de la historia del cine. Película inolvidable.
        
            