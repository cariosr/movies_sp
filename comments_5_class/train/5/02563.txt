
                      La fuerza de la narración, su impacto emocional y la complejidad alegórica es otra de las bazas de La vida de Pi, el propio Pi nos cuenta su historia, así que es evidente que sobrevive al naufragio, pero esa falta de suspense no impide que queramos saber cómo se las ingenia para sobrevivir y que cada nueva aportación a la historia nos sorprenda. Una vez que la historia te ha atrapado, aceptas todo como verdadero, por raro que parezca, hasta el inesperado y sorprendente desenlace que nos hace replantearnos todo lo que hemos visto.
Una bonita fábula, unas imágenes impactantes y un sorprendente final hacen de esta película una maravillosa experiencia que no deberíais perderos.
        
            