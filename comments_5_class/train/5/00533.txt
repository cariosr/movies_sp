
                      Excelente película. Argumento, dirección e interpretación, especialmente el Sr. Rush, magníficos. Pronostico una nominación a los Óscar para el " profesor del Rey". Después de la película los amigos que la vimos debatimos ampliamente sobre ella. Se lo merece. Absolutamente recomendable.
        
            