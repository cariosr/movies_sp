
                      Una película buenísima donde las haya. No ha hecho falta que se recurra al gore ni a la violencia. Sin duda es una película de terror de la vieja escuela, de las que te mantienen constantemente en suspense e intrigado. Con bastantes sustos. Sencillamente increíble con un final inesperado.
        
            