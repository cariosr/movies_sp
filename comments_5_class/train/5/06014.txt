
                      Andrew Nicoll no desperdicia ni una sola coma del libro de Stephenie Meyers, la película es espléndida, tierna y fantástica. Quien no vaya a verla porque piense que es otro Crepúsculo esta equivocado, el problema no viene de sus libros que son geniales sino del mal reparto que se eligió para esa saga. En este caso el reparto es fantástico destacando el papel de Diane Kruger como la buscadora y de Soairse Ronan como la dulce y dura Melanie/ Wanda. Reconozco que me emocione pero igual me paso leyendo el libro.
        
            