
                      En mi modesta opinión, una película de ciencia ficción tan movilizante como pueden haber sido en su momento (o aún hoy) Blade Runner, Terminator, o Alien.
Por cierto no es copia de ninguna de éstas, y la propuesta sorprende de principio a fin. Básicamente porque es la única que podría representarse sin mengua de interés o calidad en un teatro, sin requerir siquiera gran despliegue de escenografías, vestuario, etc.
Vos que me estas leyendo (si, vos) yo, y un par de amigos, en la casa de cualquiera de nosotros, podríamos representar esta obra, y el resultado dependería de nuestra calidad actoral.
Un dato: cuando la vi, me pareció una obra menor. Al otro día, reflexionando sobre la trama, me di cuenta que había sido espectador de una joya del séptimo arte.
Va a ser, sin duda, una película de culto. Con lo poco que materialmente es (o tal vez por eso) tiene todo para serlo.
        
            