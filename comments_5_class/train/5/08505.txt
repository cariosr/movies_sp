
                      Peliculón de arriba a bajo. La vimos en Splau (Barcelona), en una sala experiemental con Dobly Atmos y en 3D, por lo que la sala espectacular para una película espectacular. Tiene una fotografía que te deja boquiabierto y una acción descomunal. Si eres warsie te va a gustar (salvo si eres un cerrado/a qué si no está George Lucas no te gusta nada, por cierto a Lucas le ha gustado ;) y si no eres fan, estás ante una película de acción de cine fantástico descomunal. Un gran acierto en todos los aspectos.
        
            