
                      Una de las mejores películas de este género que he visto. Russell Crowe lo hace estupendamente y la música y escenas son maravillosas. Una película para ver con la familia o solo que te hará ver cómo era la vida de los romanos y los gladiadores y cómo sobrevivir demostrando una gran valentía
        
            