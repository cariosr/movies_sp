
                      El cine francés, siempre interesante, nos vuelve a sorprender con una historia que atrapa desde el minuto uno, una película de sentimientos y superación personal en la que se mezclan las diferentes realidades de una gran ciudad francesa. Show must go on, es el mensaje que nos ofrece una cinta que, sin duda, debe estar entre tus favoritas.
        
            