
                      Desde ya un película de culto. Excelente ambientación, fotografía, banda sonora y, por supuesto, reparto. 
Película ochentera con una trama sencilla pero sublime; los momentos sin dialogo nunca me habían expresado tanto como en esta película
        
            