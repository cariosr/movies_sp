
                      Una joya del cine actual. Elegante como pocas, banda sonora espectacular, absolutamente descarnada y con algunas secuencias que pasarán a la historia del cine. Un clásico moderno.
        
            