
                      Es una película totalmente imprescindible. Es un clásico de terror de Hitchcock. Una trama genial, muy buen argumento y perturbador. Tiene todo lo que una película de horror necesita. Un clásico que hay que ver.
        
            