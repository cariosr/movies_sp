
                      Fabulosa la adaptación animada de el Principito. El cuento se introduce a través de una bonita historia de una niña, que bien podría ser la vida de cualquier niño en la realidad con una vida definida hora por hora y actividades extraescolares... Transmite unos valores imprescindibles para cualquier niño y adulto. Al igual que el libro, la película te hace reflexionar y soñar, y sobre todo no olvidar.
        
            