
                      Star Wars es una de las mejores películas de todos los tiempos. Anticipándome a la nueva película Star Wars Episodio VII: The Force Awakens, pastillas de información, volví y observé la trilogía original para recordarme por qué estas películas se mantienen muchos años después. A los pocos minutos de colocar la cinta VHS en mi combinación de TV / VCR, recordé que no era saludable.

En los primeros segundos de la función, vislumbramos la enormidad de la visión del director George Lucas para esta serie. La famosa secuencia de apertura de la película muestra al destructor estrella del Imperio persiguiendo a una pequeña nave rebelde que lleva a la princesa Leia (Carrie Fisher) a bordo. La nave es capturada rápidamente, lo que lleva a la introducción de Darth Vader, una amenaza enmascarada que tiene el poder de destruir a sus enemigos con un simple movimiento de cabeza. Vader tiene el poder de la fuerza con él, pero solo está interesado en usarlo para aterrorizar a sus enemigos y destruir a cualquiera que amenace su poder.

Luke Skywalker (Mark Hamill), mientras tanto, es un granjero respetable que sueña con escapar de su vida normal y luchar contra el Imperio. Cuando dos droides traviesos e inolvidables (R2-D2 y C-3P0) llegan a su poder, la simple vida de Luke se invierte. Es reclutado por un viejo sabio llamado Obi-Wan Kenobi (Alec Guinness) y decide unir fuerzas con la rebelión. A lo largo del viaje de su héroe, Luke y Obi-Wan se hacen amigos de un piloto pícaro llamado Han Solo (Harrison Ford) y su compañero de vuelo Chewbecca (Peter Mayhew).

La configuración de la película puede parecer simple y, en el fondo, Star Wars es una historia sencilla. Sin embargo, en lugar de vaqueros luchando en el desierto, esta película muestra héroes intergalácticos luchando en el espacio contra un imperio maligno. Al igual que las series cinematográficas de antaño, la función está llena de aventuras, emociones y sorpresas.

Cuando la película llegó a los cines, los efectos especiales no eran geniales, pero Lucas pudo ofrecer algunas de las mejores imágenes de la época. El aspecto de la película fue épico y la película finalmente ganó premios de la Academia por la mejor dirección de arte, el mejor diseño de vestuario, la mejor edición de sonido, los mejores efectos visuales y la mejor puntuación.

Nota: 4/4
calificación: Fresh
        
            