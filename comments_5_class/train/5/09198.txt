
                      La carrera de los Oscar a la Mejor Película está oficialmente encendida. Desde el primer cuadro hasta el último, Dunkirk de Christopher Nolan es un logro monumental, una épica de la Segunda Guerra Mundial de asombroso espectáculo visual (véalo en IMAX si puede) que lo golpea como un disparo en el corazón.
        
            