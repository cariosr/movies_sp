
                      El club de la lucha es una cinta con un guión donde lo más destacable es la parte de la narración. Una narración contada por el protagonista donde podemos ver con gran claridad la facilidad y la buena calidad de lo que nos están contando. Una narración delicadamente extrovertida y sin ningún tipo de tapujos a la hora de decir palabras claras y concisas.
Decoradas por imágenes de violencia. Con una visión diferente de la violencia.

Y es que, a pesar de que en El club de la lucha se ve bastante destrucción, es una violencia con significado. Con un significado dentro.
Desde el principio hasta el final del film, podemos observar una gran crítica hacía la sociedad. Mostrando un mensaje potente, eficaz y verídico cien por cien. Un mensaje que pocas películas saben mostrarlo de esta manera. De una manera tan especial. Y no solo está esa narración y ese mensaje, sino las grandes sorpresas que nos tiene preparadas este film. 

Las actuaciones son sublimes. Bonham Carter que brilla como estrella en el firmamento. Un Brad Pitt impresionante de principio a fin. Y Edward Norton que está espectacular. Los demás personajes también están francamente bien.
La banda sonora, más que acorde y propicia, junto con una dirección casi perfecta por parte de David Fincher. Un guión perfecto dotado por una narración sensacional y sublime. Con un mensaje que consigue transmitirlo perfectamente y de una manera única.
En definitiva una maravilla.
        
            