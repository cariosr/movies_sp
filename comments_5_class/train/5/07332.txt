
                      'Los Puentes de Madison' es un drama romántico dirigido por Clint Eastwood, producido por Kathleen Kennedy, y basado en la novela de Robert James Waller.

Francesca Johnson (Meryl Streep), de origen italiano, vive en el condado de Madison (Iowa) junto con sus dos hijos y su marido. Cuando los tres acuden a una feria durante cuatro días, dejando a Francesca sola en casa, aparece un exótico fotógrafo de la revista National Geographic, Robert Kincaid (Clint Eastwood), que la conquistará. Ambos vivirán un breve pero intenso amor imposible.

Meryl Streep, como siempre, realiza un trabajo exquisito. Cada mirada, cada gesto, reflejan la baja autoestima de una ama de casa poco valorada. Clint Eastwood acostumbrado a ser un macho imponente, sorprende con su sensibilidad y romanticismo.
Y aunque los actores no peguen como pareja, son tan profesionales que llegan a crear química entre ellos.

Es una historia preciosa que engancha desde el principio, y que está muy bien contada. El vestuario y los escenarios campestres añaden un efecto melancólico y romántico.
Sin duda el mejor momento de la película es la escena final entre Francesca y Robert, digno de un auténtico drama romántico, en el que es inevitable derramar alguna lágrima.

La película fue nominada en 5 ocasiones a diferentes premios, 3 de los cuales fueron para Meryl Streep.
        
            