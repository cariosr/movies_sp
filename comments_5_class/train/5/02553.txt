
                      La dirección, el cast y el arte es excelente. Hace tiempo que no veo una película con este nivel en esos criterios. Por otro lado, el guión es muy sencillo, sin embargo la escena final hace que no sea una simple película de amor y sueños. Me encantó.
        
            