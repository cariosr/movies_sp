
                      Obra maestra. Las dos horas y poco que dura la película se pasan volando, no quería que acabase.  La química que hay entre Ryan Gossling y Emma Stone, así como el clima que crean es brutal. La recomendaría a todo el mundo, incluso aquellos que por ser un musical quizás no se acaban de decidir, dadle una oportunidad porque no os dejará indiferentes. PE-LI-CU-LÓN!
        
            