
                      Ya ha llegado a nuestras pantallas uno de los estrenos más esperados del año (al menos para mi).
Quiero empezar diciendo que me encantan los comics, siempre me ha gustado Marvel y que disfruto con estas películas si están  bien hechas. Algunas películas individuales de los que forman los vengadores me han defraudado un poco (como las dos de Thor, la segunda es malísima) pero está visto que cuando se juntan triunfan. Es por ello que siempre se acude con esperanza de disfrutar pero el sello marvel y que un superhéroe sea el protagonista no siempre es suficiente para que la historia funcione. Evidentemente si no te gusta este tipo de cine, de acción y superhéores, esta no es tu película.

También quiero decir que hay que verla sin ser muy puristas de las historias Marvel. El director se toma ciertas libertades en el origen de algunos presonajes pero lo importante es que cuadran bien con toda la historia

Los Vengadores: La era de Ultrón vuelve a estar muy bien dirigida por Joss Whedon, quien ha demostrado con creces que saber rodar escenas de acción con diversos frentes y dando la dosis justa de protagonismo a cada uno de los Vengadores, con libertad de movimientos y de nuevo acierta con la fórmula para crear una de las mejores películas de acción del año.  Y como no protagonizada por unos geniales Robert Downey Jr, Chris Evans, Scarlett Johansson, Mark Ruffalo, Jeremy Renner, Chris Hemsworth,  que bordan sus papeles (como héroes de acción, claro está).

Estamos ante una excelente película de animación, más madura que su predecesora, que para superar a la primera visto que tiene que mostrar un poco más de lo mismo intenta profundizar más en el desarrollo de sus personajes y los define mejor. Los miedos y preocupaciones de cada uno de ellos están muy presentes en todo momento. Pero no olvidemos que es un producto de entretenimiento, no hay que buscar una historia de profunda reflexión. Aquí vamos a pasarnoslo bien. Y no hay duda que te lo pasas en grande en todo momento. Arranca muy fuerte, después tiene un momento de relax en su nudo central, pero solo para coger carrerilla para el esprint final.
 
Algunos momentos de lucha son realmente espectaculares, como el de Hukl y Hulkbuster, y qué decir del desarrollo final...

Con un buen guión, escenas de acción a raudales, muy bien acompasadas, las adecuadas dosis de humor con los puntos graciosos de cada uno. Unos efectos visuales a los que difícilmente podemos sacar alguna pega, y una banda sonora que eleva la el ánimo a los momentos adecuados y los hace épicos, de las manos de Brian Tyler y Danny Elfman que respetan y complementan la música de la primera entrega creada por Alan Silvestri.

Los nuevos personajes de La Bruja Escarlata y su mellizo Mercurio interpretados por Elizabeth Olsen y Aaron Taylor-Johnson dan mucho juego y aportan cosas nuevas encajando a la perfección en la trama y en el grupo. Me han gustado los dos bastante, sobre todo La Burja Escarlata. También tenemos a Visión, interpretado por Paul Bettany que ya ponía voz a Jarvis en la versión original. Visión tiene un papel crucial pero es solo un secundario al que se le reserva para la tercera entrega de Los Vengadores

En cuanto al villano tenemos a Ultrón, una máquina pero con reflexiones muy humanas lo que le da un aire muy melodramático en algunas secuencias. Un robot creado para un propósito o con una clara intención que se desvirtúa cuando este adquiere consciencia y toma una decisión. Decisión que no es nueva en las películas de ciencia ficción, la solución para la paz mundial pasa por la aniquilación de la raza humana. Mal rollo. Hay que admitir que es un gran villano a la altura de los Vengadores y resulta ser un digno oponente. Un robot con la personalidad presente en todos sus robots.

Sin duda alguna es una de las grandes películas de superhéroes y sales del cine con la adrenalina por las nubes. Una gran historia que sigue ampliando el universo cinematográfico de Marvel, resolviendo respuestas y creando nuevas preguntas, dejando de lado algunos personajes y presentando otros nuevos, siguiendo el hilo argumental de todas sus protagonistas tejiendo una gran historia, sirviendo como puente de unión para la próxima entrega, Los Vengadores, Infinity Wars que llegara en dos partes, la primera en 2018 y la segunda entrega en el 2019.

Yo salí muy contento de la sala, habiendo disfrutado de un gran espectáculo y habiendolo pasado muy bien. Así pués bien pagado está el precio de las entradas.

Si sois vengadores, no dudeis en reuniros para ir a ver esta película.
        
            