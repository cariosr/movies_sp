
                      Se hace ya muy raro de ver una película tan bien hecha, que te mantiene en tensión todo el tiempo, y eso que dura bastante, y que aún por encima no tiene nada de propaganda social justice warrior, lo cual es un gustazo, hace mucho que no veía una película simplemente hecha para entretener sin propaganda. Es muy original, generalmente en las películas o series convencionales ya sabes lo que va a pasar, no es el caso de esta película.
        
            