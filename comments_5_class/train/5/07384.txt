
                      Efectivamente, es una obra maestra en toda regla. El argumento de la película es sólido, tiene un ritmo narrativo vivo y constante y los papeles están muy bien interpretados. Sin duda una película que hay que ver sí o sí.
        
            