
                      Esta es una de las secuencias más inquietante en la historia cinematográfica: una mujer camina de puntillas por una casa hasta que, en tres disparos staccato, descubre el cadáver ensangrentado de su ocupante en el suelo del dormitorio, sus cuencas dos agujeros negros goteando sangre.

La escena es solo uno de varios momentos espeluznantes en la película de terror sin igual de Alfred Hitchcock, The Birds, realizada tres años después de su éxito abrasador con Psycho.

Comienza como una comedia absurda. Melanie Daniels (Tippi Hedren) es una playgirl ambiciosa que maneja un Aston Martin travieso. Después de que un abogado inteligente llamado Mitch Brenner (Rod Taylor) flirtea con ella en una tienda de mascotas de San Francisco, ella lo rastrea en Bodega Bay, al norte de California, donde pasa los fines de semana con su madre (Jessica Tandy). El australiano Taylor, que había ganado la aclamación por su papel en The Time Machine, murió en enero de 2014 a los 84 años.

Poco después de que ella se fuera a verlo, una gaviota le picotea la cabeza, un ominoso sustituto de la flecha de Cupido, y no pasa mucho tiempo antes de que la ciudad sea atacada por bandadas de gaviotas voladoras y cuervos. Las secuencias de ataque de pájaros son tremendamente complejas (la película contiene más de 370 tiros), y la ausencia de una puntuación hace que el horror sea más inmediato: el compositor de Hitch, Bernard Herrmann, creó una extraña banda sonora a partir de graznidos, estridentes chillidos y crujidos de alas .

Pero el verdadero genio de la película, basado en un cuento de 1952 de Daphne du Maurier, es la forma en que Hitchcock hace que los pájaros malévolos parezcan manifestaciones de la inquietud mental de sus personajes, especialmente la de la madre de Mitch y su antigua amante, Annie, ahora un maestro de escuela local.

¿Qué mujer no se sentiría amenazada si Melanie llegara a la ciudad? Con su elegante traje verde, su cabello color peróxido recortado en un inmaculado moño, y sus suaves labios moldeados en una sucesión de bobadas de minxish, Hedren hace de Melanie la máxima sofisticación de la década de 1960.

La madre de Mitch le sonríe a Melanie cuando son presentadas, su animosidad tal vez provocada por su extraña semejanza. De hecho, muchas personas confunden el famoso arte de póster de una mujer atacada por pájaros (tomada de la escena en la que los gorriones caen en cascada en la sala de estar de los Brenner) para Hedren. De hecho, la mueca le pertenece a Tandy, su cabello gris teñido de rubio para incitar la confusión.
        
            