
                      Una joyita. El director observa de un modo ácido las contradicciones y absurdos que se producen en la vida cotidiana de una familia iraní. Partiendo de una situación cotidiana: matrimonio roto,hija adolescente dividida entre papá y mamá,abuelo con alzheimer el cual necesita que alguien le atienda... se suceden una serie de disparatadas escenas y más sorprendentes consecuencias que nos recuerdan lo que es Irán,o lo que es una sociedad regida por la Ley Islámica. Sutil,profunda a la vez y moderna. A verla!
        
            