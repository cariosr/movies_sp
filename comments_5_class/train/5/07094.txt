
                      Aunque técnicamente no era su primera película, fue la carta de presentación de M. Night Shyamalan al gran público. Preciosista y aterradora, el uso de los colores, especialmente el rojo, los movimientos de cámara, un guión más que interesante y un elenco de actores y actrices muy bien seleccionado, dan como resultado esta maravilla, quedando para el recuerdo como un clásico moderno gracias a algunas escenas que ya son parte de la historia del cine
        
            