
                      Película con una historia llena de giros imprevistos donde hay amor, engaño, corrupción y con un fondo que trata sobre un problema social de actualidad. Lo que más me ha gustado de la película ha sido la historia, que te engancha desde el principio y los actores que hacen un buen papel. Las actuaciones de varios grupos musicales en algunas tramas no hacen más que potenciar el entretenimiento. Muy recomendable, les estoy diciendo a mis amigos que vayan a verla.
        
            