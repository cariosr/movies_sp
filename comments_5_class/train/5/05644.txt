
                      Una obra maestra!

A través de un diálogo directo, muestra una familia acomodada donde sus vidas muestran ser transparentes, pero hay algo más allá en el deseo humano interior, al que sólo podemos acceder nosotros mismos con nuestros deseos.
        
            