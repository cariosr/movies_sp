
                      La gran joya de Billy

Imprescindible pelicula,la mejor de Billy Wilder,una pelicula mítica con un gran argumento y con un Billy Wilder que dirige y confecciona el guión de esta tragicomedia.La historia es perfectamente narrada y con buenos personajes,el principal interpretado por Jack Lemmon es buenísimo y te mete perfectamente en la historia.Jack cocina un personaje y una interpretación excelente, aunque no es de extrañar,se pueden recordar grandes joyas interpretativas de este oscarizado actor,como por ejemplo "Irma La Dulce".Continuando con la critica tengo que aplaudir la fotografía de la pelicula y con todo esto no puedo colocarle menos de 4 estrellas y media a un apartamento que entra en la historia del cine,lo repetiré por si a alguien  no le ha quedado bien claro,esta pelicula forma parte de la historia del cine,forma parte de las peliculas que hay que ver...y disfrutar.
        
            