
                      Los fans del género ya tenemos ganas de ver esta película, en la que por los trailers y las informaciones que nos han ido llegando, parece que los detalles están muy cuidados, cosa que es de agradecer. 
Además veo bien a Ryan Reynolds en el papel de superhéroe, tanto por su calidad interpretativa, de suma probada en otros títulos, como por sus características físicas. 
Y los villanos parece que tampoco se quedan flojos y cumplirán las expectativas. 
El guión me parece apropiado: la lucha entre el bien y el mal, la aparición del mal, poderes que ayudan a salvar el mundo, una cruenta batalla. Más que correcto para tratarse de ciencia ficción.
La película llegará a muchos países en agosto, aunque en España lo hace el 29 de julio. Hasta esa fecha los fans de la película podemos convertirnos también en guardianes de las estrellas, ver los trailers, descargarnos  wallpapers y luchar para salvar el planeta y llevarnos premios en http://www.elcuerpodelosgreenlanterns.es/. Así se nos hace la espera más corta.
        
            