
                      "The Hateful Eight" cumple con lo prometido, bautizando su octavo filme como un verdadero tiro en el blanco"

Todo ser humano tiene la posibilidad de dictaminar un opinión o veredicto buena o mala con respecto a una película vista, absolutamente todos tenemos esa oportunidad. De igual manera, cada película posee un lado bueno y malo, "Star Wars" lo tiene, "Jurassic Park" lo tiene, pero en los filmes de Quentin es imposible hallar un lado malo. Relativamente, el director jamas tendrá un mal proyecto, por lo que "T.H.E." es una de sus mejores armas para deslumbrar a la audiencia.

"Un film by Quentin Tarantino" es la principal razón para asistir a un teatro y pagar una boleta, sin importar su historia, personajes, reparto, ni ninguno de tantos parámetros fantásticos, solo sabes que te sorprenderá.

Quentin se desvincula de la tradición del 2015 (remakes y una que otra cosa interesante) creando una vez mas un nuevo mundo, lleno de nieve, sangre y honor (con pequeñas pero notables referencias de "Reservoir Dogs"). Los personajes mas odiados del planeta en un mismo lugar, encerrados en una cabaña, aguardando mientras una terrible tormenta pasa, solamente tienen que pasar unos tranquilos dos días tomando te y siendo amables ¿Qué podría pasar?. Pues, Tarantino logra transformar y revolucionar este tipo de historia por una muy poco predecible e impregnada de humor negro, fuertes escenas sexuales y violentas, zafiedad y cero tabús cinematográficos. El hecho de tener 8 personas liberando todo su odio, en las manos de tan celebre director es catastrófico (en el buen sentido de la palabra).

A lo largo de la historia, personalmente, jamas había contemplado un tipo de narración como esta (Capítulos), agregando un efecto innovador mas a sus películas y al cine en general.

Tarantino es un hombre que ha estado empapado en el mundo del cine desde muy pequeño, absorbiendo cada buen elemento de las películas que podía ver y posteriormente implantándolas en sus propios proyectos (dirigiendo y escribiendo), este chico sabe lo que hace.

Mas allá de un radiante vestuario, los detalles de la época, el diseño de producción o cualquiera de sus vigorosos características, las locaciones ( el desierto de nieve, la cabaña, la carroza) es la mejor manera de empezar un western.

Y ha empezado. Paisajes majestuosos con predominancia del color blanco, una fuerte nevada, las herraduras de los corceles golpeando con el suelo, fieles sinónimos de el comienzo de una grande aventura. Es sorprendente que una película de 3 horas y 2 minutos no llegara a sentirse tensa, aburrida y hostigante, en cambio de esto, su suspicacia e intriga en ascenso, genera una experiencia disfrutable para el espectador.

Cualquiera que allá visto los 7 películas anteriores del director, sabría decir que a merito propio y como es lógico, las primeras escenas y linead e dialogo son para su mayor actor repetitivo Samuel L. Jackson, que ha colaborado con el experimentado hombre por sexta vez. Tan solo con que este negro abra la boca, ya se merece una un galardón en un prestigioso premio.

Uno de los mas frecuentes clichés y coincidencias entre el publico y los dramas modernos es fichar a un héroe quien sera el protagonista central, sin embargo ¿Es posible señalar a un héroe aquí, conociendo que nadie es lo que parece?. Él renueva dicha constante con una variación inteligente y fresca enfocándose en cada uno de los integrantes, siendo dificil señalar un protagonista. Aunque, subjetivamente elegiría a Jennifer Jason Leigh y Samuel Jackson como las estrellas del show.

La oportunidad de integrar a un mexicano en la historia de Tarantino es algo inimaginable y muy especial para el publico latino (en especial para mí), Demián Bichir (Bob) hace su trabajo a tan alto nivel como sus amigos actores.
   
Un verdadero amante de las películas de este chico, puede decir que uno de los aspectos de ambientación mas primordiales para sentir y compenetrar dentro de la historia son los insuperables y característicos scores, que en este caso, estuvo a cargo del profesional Ennio Morricone para proyectar la rigidez y comicidad fundamental en el filme.

La tormenta es larga y continua, creando una atmósfera impresionante de tensión. Luego de tanta nieve y frió, el ambiente se calienta y nace un infernal entorno en la cabaña, en donde la química explota y el filme deslumbra.

El titulo es exacto, y no por ser el octavo filme de Tarantino. Las formulas del director pueden ser confusas y difíciles de entender, pero él mismo se encarga de solucionar o explicar cada una de esas incógnitas al final del espectáculo, eso sí, no todo es fácil de deducir, estamos hablando de uno de los mejores directores de la era, la palabra fácil o mediocre no existe en nuestro lenguaje.

Creo que la película funciono muy bien en Hollywood por tantos elementos que pudo utilizar, pero me siento un poco esquivo a si funcionara este en Broadway, aunque confiando y conociendo el potencial de Quentin, todo es posible.

Luego de la polémica #OscarsSoWhite, creo que el plato predilecto de la Academia es esta cinta, teniendo uno de los grandes exponentes de la raza negra, un latino, una instrumentacion monumental, un elenco de primera, una rigurosa dirección, mucho escándalo, y cada elemento que le fascina a la audiencia. Lastimosa e ignorantemente, no tuvo la participación que debía tener, Ineptos!.

Sin lugar a dudas, la ultima invención del director y guionista se ha convertido en mi favorito dentro de toda su carrera. Exactamente fotografiada y dirigida "The Eight" aporta un éxito mas para Quentin satisfaciendo a los aficionados con un proyecto con sangre Tarantino.

Este hombre es ha convertido en uno de mis directores favoritos y no por tantos años en la materia, si no por exhibir cada una de sus ideas tal y como se las imagina, deshaciéndose de vetos, no temiendo a mostrar una desnudez, una felación y mucha sangre, por miedo a cuidar el rating de la película y no llenar sus bolsillos de dinero al no permitir la entrada de adolescentes,él hace cine de verdad (no quiero decir que los productos comerciales no sean cine vació y obsoleto, ambas clases me fascinan). En este filme la violencia es primordial siendo uno de las características mas llamativas y divertidas, creando cierto morbo e incluso un poco de gore. "The Hateful Eight" sigue inovando y esta vez no trae el majestuoso formato 70mm. 

Una de los mejores filmes del año, de Tarantino y del mundo cinematográfico, "T.H.E" es una cajita de oscuras sopresas que iluminara con todo esplendor la nueva creacion del director.
¡"Gracias Quentin Tarantino!
        
            