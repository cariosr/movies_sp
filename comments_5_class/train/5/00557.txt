
                      La narrativa de Jenkins es elegante, inteligente, femenina y poderosa. DC ve por fin un halo de luz con 'Wonder Woman', otro blockbuster super heroico pero fidedigno y leal, emocional con sus personajes y su historia. Gadot ocupa la cámara en cada escena, deslumbra, sorprende, lucha y se defiende. Y es que el humor de la película no sólo destensa las situaciones de acción, sino que se ejecuta bien.
        
            