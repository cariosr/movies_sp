
                      Yo incluso George Lucas esperaba que la primera "Guerra de las galaxias" resultase tan exitosa, así que, en el momento de estrenar el estreno de su película, el resultado de las vacaciones en Hawai con su amigo Steven Spielberg (fue ahí que desarrollarán la idea de "Indiana Jones", dicho mar de paso). Sin embargo, “La guerra de las galaxias” terminó siendo la película más popular de 1977, y como ya todos deberíamos saber, influir en millas de personas y futuros cineastas, cambiar el panorama del éxito de taquilla hollywoodense para siempre.

Afortunadamente, Lucas siempre tuvo la idea de realizar más películas de esta saga; De otra manera, no hemos obtenido una película tan grandiosa como “Star Wars: Episodio V - El Imperio Contraataca”. Escrita por Lawrence Kasdan ("Indiana Jones en Búsqueda del Arca Perdida", "La Guerra de las Galaxias: Episodio VII - El despertar de la Fuerza") y Leigh Brackett, y dirigida por Irvin Kershner ("Nunca digas nunca jamás", "Robocop 2" ), “El Imperio Contraataca” siempre es la mejor manera de suceder: la secuela que supera a su predecesora. Entretenida, madura, y sorprendentemente oscura, la cinta es todo lo que un buen éxito de la aventura y la fantasía debería ser, y más.

Nota: 4/4
calificación: Fresh
        
            