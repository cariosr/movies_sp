
                      OBRA MAESTRA.   James cameron es el rey. Ya lo demostró con Titanic y con avatar lo ha vuelto a recordar, no sólo por ser el rey de la taquilla, sino por su trayectoria donde ha ido tejiendo auténticas obras maestras. Avatar es un cuadro abstracto en movimiento, una explosión, una aventura donde tal vez no veamos nada nuevo, pero si vemos algo diferente... Larga vida al rey
        
            