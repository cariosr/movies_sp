
                      Obra maestra, que cuenta una historia épica de supervivencia y venganza a partes iguales. 
Mención especial de las actuaciones del protagonista y del secundario.  Además de una gran dirección. 
Ofrece al espectador una colección de imágenes del corazón de la naturaleza y del ser humano,  con un ritmo salvaje,  y planos que te hace vivirlo. 
Lo mejor: todo
Lo peor: quizás demasiado largo,  pero,  está bastante justificado
        
            