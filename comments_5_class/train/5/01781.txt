
                      ¿La mejor pelìcula de Ryan Gosling? Puede que asì sea aun cuando su actuaciòn no sobresale por sobre lo demas. 'Drive' es un largometraje infaltable en nuestra colecciòn de pelìculas de acciòn y suspense (con mucha adrenalina). Èste film representa muy bien el gènero y nos trae una historia 'simple' pero arrolladora... el personaje de Gosling es uno de los mejores que eh visto en los ùltimos 10 años. Arrollador.
        
            