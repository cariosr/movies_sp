
                      El Señor de los Anillos: El Retorno del Rey, es la tercera y última película de ésta trilogía dirigida por Peter Jackson, y basada en los libros de J. R. R. Tolkien.

Frodo (Elijah Wood) y su fiel amigo Sam (Sean Astin) siguen su camino hacia El Monte del Destino. Mientras tanto, las fuerzas de Sauron centran toda su atención en Gondor, dónde Aragorn (Viggo Mortensen) reclamará el trono, y liderará la batalla contra el enemigo.

Con ésta cinta, Peter Jackson pone fin a una de las trilogías más emblemáticas de la historia del cine. Un final épico en el que las emociones están a flor de piel. Por fin vemos si Frodo y Sam consiguen su objetivo, si Aragorn acepta su destino, y quién gana la inminente guerra que se cierne sobre la Tierra Media.

La batalla final es grandiosa, en cada una de sus escenas, cada imagen y cada movimiento es de una maestría innegable. Todo detalle ha sido creado con un cuidado exquisito.
En resumen, un cierre a la altura de una trilogía de películas memorables.

La película fue nominada en cuarenta ocasiones a diferentes premios, en diferentes categorías.
        
            