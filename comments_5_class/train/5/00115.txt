
                      Star Wars: La Amenaza Fantasma, es una película de ciencia ficción dirigida, producida y escrita por George Lucas.

La República está sufriendo una serie de crisis que ponen en peligro su estructura interna y fiabilidad.
El jedi Qui-Gon Jinn (Liam Neeson) y su aprendiz Obi-Wan Kenobi (Ewan McGregor), acompañan a la reina Amidala (Natalie Portman) a Coruscant, dónde se encuentra la sede del Consejo, para tratar de solucionar dichos problemas. Por el camino, su nave se estropea y han de parar en Tatooine, planeta natal del joven Anakin Skywalker (Jake Lloyd). Qui-Gon se fijará en el niño, ya que la fuerza es muy intensa en él.

La nueva trilogía que da comienzo con esta película, está ambientada años antes de Una Nueva Esperanza. Así pues, en éstas se explicará los origines de los personajes de la trilogía original.

Hubo opiniones dispares sobre la película, buenas críticas; reflejadas en las 3 nominaciones recibidas en los Oscar, y 2 en los BAFTA, y muy malas; cuya muestra está en las 9 nominaciones a los premios Razzie, incluyendo peor película y peor actor secundario.
Es cierto que no está al nivel de sus predecesoras, pero no por ello es una mala película. Tiene buenos efectos especiales, la siempre insuperable banda sonora de John Williams y bastante acción.
En lo referente a los personajes, Jar Jar Binks despertó mucho desprecio, y es cierto que al pequeño Anakin le falta soltura, pero en general hay muy buenos personajes; Amidala, Darth Maul y Qui-Gon Jinn, de los mejores.
        
            