
                      Como ya puse en la crítica de "Sinsajo (P.1)" -¿hacía falta de verdad esperar otro año?
Pues la verdad es que sí. Y creo que deberían de haber echo una 3ª parte, porque hay historia para rato.
Y aunque no supere ha "En Llamas" sinceramente me ENCANTÓ.
Es tal cual el libro.
Los efectos especiales (ver la película en 3D fue GENIAL), el guión, el vestuario, las actuaciones (Unos IMPRESIONANTES personajes), TODO.
Y aunque (No voy a SPOILEAR el suceso) sabía que iba a llegar una flecha directa al corazón, lo que me llegó fue una bomba en toda la cara que me hizo 'BOOM'. Y me quede como ¿Ya? (No lo sentí tanto como en el libro).
Estaba tan metido en la película que no quería que se acabase. Pero cuando todo se apaga y escucho a Jennifer Lawrence cantando la EMOCIONANTE pero TRISTE "Depp in the Meadow" me dije:
- Ya está, terminó.
Tuve una mezcla de EMOCIONES IMPRESIONANTE.
Lloré, me Emocioné y Temblé.
Gracias a Suzanne Collins y Lionsgate por traernos una Gran Saga Cinematográfica, que aunque muchos la odien a mí me cambió la vida.
Me despido de "Los Juegos Del Hambre", de 'La Chica "En Llamas" y del "Sinsajo".
.III.     .III.     .III.     .III.     .III.
P.D: Espero que se tomen enserio hacer los Spin-Off. Valdría mucho la pena, sobre todo para los fans, como ya hicieron con la saga "Crepúsculo".
        
            