
                      Barry Jenkins consigue captar con sencillez los instantes de una vida y relatarlos al público espléndidamente. Se enfrenta a la dificultad de hacer traspasar los sentimientos a través de las pantallas, y el resultado es una obra de arte, un frágil rayo de luna cuya luz es mágica; "Moonlight". Con una banda sonora espectacular de Nicholas Britell, la película consigue fluir de manera natural y entrar en todas nuestras vidas. Pura belleza.
        
            