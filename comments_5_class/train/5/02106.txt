
                      Tarantino no afloja la marcha y ofrece una experiencia de tres horas donde da rienda suelta a sus delirios, tanto en los dialogos como en las escenas de acción. No la recomendaria a quienes puedan tener problemas con el estilo tarantiniano, pero para sus fans es una ración triple de elixir divino.
        
            