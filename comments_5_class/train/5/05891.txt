
                      Cuando se tiene un universo tan grande como el que tiene en estos momentos Marvel, es muy fácil poder tomar diferentes caminos en sus cintas. Personalmente, me hubiera parecido bastante tedioso y predecible si la cinta siguiera la temática como todas las demás: Alien´s que vienen del espacio para acabar con el planeta tierra. 

En esta película, se puede profundizar mas en lo que en verdad pueden hacer los personajes, en especial Capitán América y la Viuda Negra. En ellos, por un momento, dejan a lado su personalidad de superheroes y se ponen el traje de personas comunes, con sentimientos comunes y problemas comunes.
 
Esta es lo mejor de la película, poder disfrutar de una trama de espías, traición y cambios de trama como los viejos cánones de las películas de acción e intriga marcaba, claro no dejando a lado la el sello que realiza marvel, muchas explosiones, golpes disparos y destrucción por todos lados,  pero con una intensidad humanitaria muy diferente a las películas que habíamos disfrutado de esta casa productiva.

Entrando un poco mas en la película, podemos encontrar una buen libreto, extraordinarios efectos especiales (como marvel nos tiene acostumbrados), buenas actuaciones, banda sonora envolvente, una dirección bastante buena; todo esto se adjunta a una gran campaña publicitaria.

A algunos no podrá gustarles las películas de Marvel o superheroes, a muchos si (se nota en taquilla) pero lo que es cierto es que Marvel domina amplia mente los super estrenos en el año, domina la taquilla y dominara los cines por algunos años mas.
        
            