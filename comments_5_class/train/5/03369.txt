
                      Una obra maestra del suspense, no decae, no es predecible y deja de manifiesto hasta donde puede llegar el poder y la impunidad que genera. Blanca Portillo como siempre brillante, impecable actuación.
        
            