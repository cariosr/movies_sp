
                      Magnífica película de principios de los 90. Un "must see" para todos aquellos a los que les guste el cine de terror, los thriller e incluso la ciencia ficción. Es una joya muy poco conocida, pero realmente merece la pena verla y espero que muchos cinéfilos puedan descubrirla y se den cuenta de lo buena que es.
        
            