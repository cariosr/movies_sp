
                      En mi opinión es una película sensacional, con una estupenda temática y con el romanticismo a todo lo que da, es una película para ver mas de una vez.  Para mi no es una película que habla solo sobre el amor entre dos mujeres, sino, que muestra que el amor es un sentimiento sin tiempo, espacio o alguna otra particularidad. Sin dejar de lado la espectacular interpretación de las protagonistas.
        
            