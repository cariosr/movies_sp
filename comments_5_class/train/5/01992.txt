
                      El impacto que Sorogoyen genera al espectador mediante un realismo desgarrador es fabuloso. Uno puede creerse en todo momento que está en las calles de Madrid, junto a Roberto Álamo y Antonio De la Torre -ambos magistrales en sus papeles- e inmerso en un buenísimo thriller de marca española. De los violentos, de los sucios, de los mejores. De los que tienen diálogos políticamente incorrectos e incluso desternillantes. De los que recordaremos.
        
            