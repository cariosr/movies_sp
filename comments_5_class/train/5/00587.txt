
                      Esta película es como era Triana: experimental, auténtica y surrealista. Por ello, para entenderla, hay que conocer muy bien a este grupo, la filosofía que tuvo ese rock progresivo y todo lo que aquello supuso. Sin duda, tanto Jesús de la Rosa como Tele estarían muy orgullosos. Un buen trabajo.
        
            