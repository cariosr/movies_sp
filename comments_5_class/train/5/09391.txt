
                      Es una obra de arte! Entiendo que muchas personas no les guste debido a que no comprenden la real intención, interpretación, es más mis acompañantes ni si quiera supieron el porqué del título, o de qué iba la película; el mensaje que manda es muy fuerte y real, y por mi parte estoy totalmente de acuerdo y fascinada con esta película. No puedo decir nada más pero me parece una interpretación sensacional, muy creativa, ingeniosa y cruda!! Simplemente perfecta.
        
            