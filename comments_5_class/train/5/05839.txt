
                      Tengo 13 razones para ver esta película: 1.- Para por fin poder llevar nuestra alma al cine, a ver una película llena de mensajes que nutren lo más esencial de nuestro ser.

2.- Para conocer ese otro cine artístico, ese que no es industrial, prefabricado, hecho con trozos de clichés y estrellas del marketing de Hollywood.

3.- Para aprender que todos nosotros podemos reeditar esos archivos de memoria del  pasado que están corruptos y envenenan nuestro presente. Alejandro reprocesa su biografía infantil embelleciéndola, dotándola de magia y sentido.

4.- Para apreciar el arte en la pantalla, a la vez sublime y atrevido, en formas y colorido. Un vestuario creación de Pascale Montandon-Jodorowsky exquisitamente adecuado en los personajes, y unos escenarios palpitantes de vida, con sus luces y sus sombras.

5.- Para regresar y revivir de forma sanadora la relación madre-hijo que todos hemos tenido. Maravillosa interpretación de Jeremías Herskovits, como Alejandro niño, y Pamela Flores, como Sara, su madre.  El primero nos acerca con maestría a nuestro niño interior herido con toda su inocencia, sus miedos, su legítima necesidad ineludible de ser visto y amado por sus padres; y la segunda nos regala su maravillosa voz musitada y sus formas de madre maga a la que amar y de la que aprender.

6.- Para sentir como la música de Adanowsky es un personaje invisible que embellece y aporta información sobre cada escena.

7.- Para acompañar a los personaje  principales en su proceso de transformación a lo largo de la trama. Es un canto a la esperanza para el espectador que se proyecta en ellos.

8.- Para salir del cine con la sensación de haber tenido un sueño reparador, lleno de simbolismos y metáforas que, aunque la mente racional no termine de entender, queda la sensación de que al inconsciente sí llegaron útiles mensajes.

9.- Para reconocer y admirar la magnífica interpretación que hace Brontis Jodorowsky en los diferentes registros emocionales de Jaime, el padre de Alejandro.

10.-Para acercarnos a la dimensión espiritual del ser humano, de la mano del Teósofo, interpretado con una mezcla de fuerza y ternura, por Cristóbal Jodorowsky.

11.-Para descubrir que Adanowsky además de brillar por su don musical lo hace también por su talento como actor.

12.-Para conocer la psicomagia y el psicochamanismo en acción y los efectos sanadores que ejercen sobre los personajes. ¡La misma película en sí ya es un acto de psicomagia!

13.- Para animar a Alejandro a que siga haciendo cine. Cada aparición del director en la película es una semilla de conciencia sembrada en el espectador . Agradecidos, maravillados y felices por la experiencia de ir a un cine transformador y útil para el mundo.

Plano fin, Carmen y Paco.

Madrid 27 de septiembre de 2014.
        
            