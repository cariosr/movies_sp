
                      Australia no es más que lo que pretende ser: un excelente entretenimiento orquestrado por un Luhrman soberbio e interpretado por unos estupendos actores entre los que destacan una Kidman criticada pero a mi gusto impecable y el joven que interpreta al aborigen.
Sin aires de grandiosidad y homenajeando al cine de los años 50, Luhrman nos ofrece una nueva historia para que la disfrutemos.
Lástima el final, que aún siendo correcto, se nota la presión ejercida sobre el director por un final más familiar.
        
            