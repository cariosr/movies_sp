
                      Mad Max: Fury Road, cuidando cada detalle estético en cada escena, con personajes muy bien desarrollados, quien se lleva todos los aplausos es Furiosa (Charlize Theron) quien juega un papel importante durante toda la película, Seguimos por Max (Tom Hardy) quien se mantuvo fiel al héroe de los años 90 que vimos, y por el Villano Inmortan Joe (Hugh Keays-Byrne) bastante destacable durante toda la película.

Es una película de las que pocas veces se ve en el cine moderno(haciendo referencia a los efectos especiales), con una trama sencilla mas no por eso trillada con personajes que aportan mucho al desarrollo de la historia por como tomaron cada rol. 

Excelente trabajo de George Miller para darnos una nueva entrega acorde a la primera trilogía, excelente relevo para Mel Gibson en el papel principal, esperemos que los nuevos proyectos para Mad Max sigan manteniendo ese tono estético y dramático que deja muy buena impresión en cada entrega
        
            