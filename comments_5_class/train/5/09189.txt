
                      Para mí ya es una película de culto. Excelente y muy recomendable. Los actores están sensacionales y los diálogos son lo que son y los justos. Bellísima banda sonora. Algunas escenas para el recuerdo: el beso del ascensor, la tensión del primer robo simplemente viendo la cara del protagonista (un descubrimiento), su mirada. En definitiva una película bellla, violenta, sentimental, lo tiene todo. Lo mejor del 2011
        
            