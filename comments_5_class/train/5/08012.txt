
                      Me parece una película magnífica en todos sus sentidos ya que cumple todos los requisitos para que no quieras que se acabe la pelicula.
Tarantino lo vuelve a lograr con este filme, debido a su buena historia, guión y reparto.
Es una película que no ves venir por ningún lado, ya que cuanto con grandes giros. 
Sin duda alguna los actores saben lo que hacen e interpretan sus papeles a la perfección, DiCaprio, Brad Pitt y Margot Robbie me parecen una gran elección de reparto.
Esperemos que esta no sea la última película de Tarantino.
        
            