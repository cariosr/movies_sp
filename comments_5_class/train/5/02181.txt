
                      "Mientras en las películas bélicas corrientes se lanzan bombas y se alza la bandera, Malick hace poesía filosófica sobre el ser humano frente a la naturaleza y los valores más humanos: el dolor y el miedo. Una película única e irrepetible"
        
            