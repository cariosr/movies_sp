
                      Muy recomendable, aunque no es para todos los gustos.   Sutil, austera, con una fotografía en blanco y negro que cuida el encuadre y matiza la luz de cada plano.   Las dos interpretaciones femeninas respiran verdad de principio a fin.
        
            