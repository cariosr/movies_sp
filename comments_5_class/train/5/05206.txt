
                      Hay que entender cuál es el mensaje del creador de la película para poder entenderla y calificarla. Si bien en el momento que la vi me pareció aberrante y retorcida, ya que con el tiempo durante la pelicula las emociones se vuelven más intensas y llegas a sentir pánico con los acontecimientos. Al comprender que representa cada uno de los personajes la interpretación se vuelve excelente. Grandes actores! La recomiendo, eso si interpretela bien después para que quedé impresionado con el mensaje!!!
        
            