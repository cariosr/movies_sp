
                      Ladrona por amor es una comedia dirigida por Ronald Neame, director de El millonario, Mujer sin pasado y Los mejores años de Miss Brodie.

Harry (Michael Caine) es un meticuloso ladrón que planea su nuevo golpe. Le pide ayuda a Caroline (Shirley MacLaine), una copia casi exacta de la difunta mujer del hombre más rico del mundo. Caroline distraerá a su victima, embelesado por el parecido a su amada, mientras Harry le roba un preciado y muy caro tesoro.

El principio puede desconcertar un poco, pero conforme avanza la cinta, va mejorando la trama. Un lío de idas y venidas que acaban teniendo sentido muy al final de la película, y que dejan al espectador en vilo durante el transcurso de la historia.
Muy bien planteada, y con actores de gran talle; hay muy pocas cosas que se le puedan reprochar a la película, por no decir que ninguna.

Ésta obtuvo tres nominaciones a los Oscar, y otras tres a los Globos de Oro.
        
            