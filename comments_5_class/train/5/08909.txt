
                      ~Opinión Personal: 10 /10
~Opinión Cinematográfica: 8 /10
●Historia: los Transformers, una raza de robots alienígenas gigantes transformables, entraron en guerra. Los bondadosos Autobots comandados por Optimus Prime, y los malvados Decepticons comandados por Megatron, luchan por la Chispa Vital durante siglos. Sin embargo, esta termina en el planeta Tierra, y por lo tanto, la lucha entre estos se traslada a la Tierra...
●Opinión: El cine de Michael Bay es entretenimiento puro y duro en el que disfrutar es la prioridad, y usar la cabeza es perder el tiempo. Y Transformers es una de sus pocas películas que además de entretener, tienen buena historia. 
La película tiene una historia simple, pero muy efectiva. Es una cinta muy simple, pero muy bien construida y bastante coherente. Sus momentos buenos no son pocos: la llegada de los Autobots a la tierra, la pelea entre Optimus y Bonecrusher, la batalla final, el inicio, cuando Sam compra a Bumblebee, cuando los Autobots se esconden en el jardín...
La película tiene una espectacular combinación entre acción, drama y comedia. A destacar sus increibles efectos visuales (parece mentira que tenga 10 años), asombrosa banda sonora, historia simple pero entretenida y el carisma de los Transformers.
¿Mal guión o malas actuaciones? Sí pero al final es una película divertida, en la que pasarlo bien es lo primero. Es un entretenimiento honesto. 
Michael Bay ha cogido a los Transformers, ha cogido a Optimus Prime, a Bumblebee, a Megatron, a Starscream... y los ha convertido en máquinas de guerra que vuelven la experiencia más que entretenida. 
Protagonizada por nuestros robots favoritos de la infancia y dirigida por el director más explosivo de HollyWood, "Transformers" es una aventura nostálgica, fresca y entretenida. Uno de los mejores placeres culpables jamás hechos.
        
            