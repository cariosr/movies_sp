
                      Deliciosa película, capaz de cautivarte desde el primer segundo. Aunque la trama es sencilla, su ambientación en el cine musical de los 30 y los 40 es lo que le da su esencia, y las canciones, planos, colores y personajes son lo que hacen que este filme sea uno de los mejores del año (y me atrevo a decir que uno de los mejores de la última década). Damien Chazelle, la joven promesa del cine, no deja de sorprendernos, capaz de soñar y plasmar su imaginación en cada fotograma de la película.
        
            