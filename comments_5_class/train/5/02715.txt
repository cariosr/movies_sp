
                      Una de las mejores películas de terror que he visto. 
Lo que, primeramente, me fascina de esta, es que no depende de los asquerosos clichés que inundan las películas de terror de la actualidad: jumpscares falsos, jumpscares innecesarios, personajes deleznables o simplemente planos, situaciones ridículas, etc, etc, etc.
Esta, por el contrario, tiene personajes que recordaré siempre, los cuales tienen una vida que, de verdad, es en si una película de terror. No tiene jumpscares innecesarios y el único que tiene es terrorífico: 