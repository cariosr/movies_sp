
                      Iron Man es una película de ciencia ficción basada en los cómics de Marvel, creados por Stan Lee y Jack Kirby. Está dirigida por Jon Favreau, director de 'Cowboys & Aliens' y 'Zathura', y productor de las dos películas de 'Los Vengadores'.

Tony Star (Robert Downey Jr.) es un rico mujeriego y genio de la informática, que heredó Industrias Stark de su padre, Howard.
Se encuentra en medio de la Guerra de Afganistán, demostrando la potencia de sus armas a los militares, cuando es atacado por el bando enemigo.
Es secuestrado, y herido por culpa de una metralla incrustada en su pecho. Los terroristas lo mantiene preso, con el fin de que Tony cree armas para ellos, pero en lugar de eso, se dedica a construir una armadura que le ayude a salir de ahí y volver a casa.

Iron Man rompe con el molde de los superhéroes que había hasta entonces. Tony Stark es desvergonzado e irresponsable, alguien imposible de admirar, sin embargo sigue teniendo todo nuestro afecto.

Downey Jr. aporta ese sarcasmo a su personaje que lo hace más completo. Sin duda, un gran acierto en la elección de casting.
Los personajes secundarios también son dignos de admirar, ya que son interpretados por celebridades como Gwyneth Paltrow y Terrence Howard.

La película fue nombrada una de las "500 mejores películas de todos los tiempos" por la revista Empire. De las cuarenta y nueve nominaciones que recibió, solo ganó siete premios.
        
            