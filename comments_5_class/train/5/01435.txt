
                      Excelente película. Agradable de ver y sobre todo de "escuchar". Podría ser una magnífica obra del mejor teatro llevada al cine. Para mi la mejor de la trilogía,  lo cual es exactamente lo contrario de lo normal. El que los actores sean los mismos que 18 o 9 años atrás y la historia sea su vida hace que entres en la pantalla y compartas sus 
problemas.   Una  película para no perdérsela.
        
            