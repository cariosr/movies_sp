
                      Es una maravilla. No solo la mejor película del DCEU, o de Superhéroe, es una cinta que muestra una identidad propia y un sentimiento mutuo. Es fabulosa, es hermosa, emoción, felicidad, amor y tristeza son una de la muchas emociones que sentirás en esta cinta donde desprende un gran trabajo y elaboración, hecha con pasión y corazón. Un gran trabajo de Jekins y su equipo. 
Chris Pine y Gal Gadot unos con sus personajes son las estrellas de la pantalla.
        
            