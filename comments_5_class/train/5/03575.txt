
                      Sólo puedo decir que es un peliculón. En particular no me gustó nada "El despertar de la fuerza", sin embargo Rogue One me ha dejado sin palabras y representa de verdad el horror de la estrella de la muerte, mejor que en una nueva esperanza. 
Banda sonora genial como siempre, personajes muy interesantes y el encanto de Star Wars que echaba tanto en falta! Muy feliz estoy.
        
            