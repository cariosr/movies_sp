
                      Técnicamente impecable: ambientación, sonido, efectos especiales... Merece la pena sólo por ver la guerra civil española narrada como nunca lo hemos visto antes: con una calidad digna de una superproducción de Hollywood y una aproximación histórica impecable, sin sesgo ideológico.

Es, ante todo, una película sobre la esencia del ser humano y cómo enfrentarse a los demonios que todos encontramos en nuestra vida. A través de sus personajes, exploramos el catálogo completo de nuestras pasiones y sus consecuencias sobre nuestro destino.


Mensaje muy positivo, subrayando el poder liberador del perdón y la necesidad del éste para superar los conflictos (al igual que ya narró el director en La Misión). 

En definitiva, muy recomendable!
        
            