
                      Así es, para mi la califico como imprescindible!. Si bien el villano al principio no me terminaba de cuajar, cierto es que finalmente cumple con su papel. El detalle que han tenido de incluir a un viejo conocido en esta precuela-secuela es muy bueno, seguramente que ya no lo veamos más en ninguna de las futuras películas de Star Trek.

En cuanto al apartado visual, puedo decir que es excelente, de lo mejor que he visto en años. Han cuidado muy bien la ambientación en la película y viendo alguna de las escenas me recuerdan a las antiguas de Star Trek (Cámara de Radiación), lo bien logrado que está el guion te hace recordar y analizar que esa escena pertenecía a otro personaje, pero si recordáis, este es un nuevo universo paralelo (Cambiado en la primer película).

Mis últimas palabras son para JJ Abrams que se ha lucido con la dirección, los efectos y la edición. Lo mejor de las secuelas es que si utilizas a los mismos actores, tienes ya casi el 70% del éxito de la película asegurado.

Ahora, a esperar a que salga la tercera parte en dos o tres años!.
        
            