
                      Una niña de 11 años lucha con las emociones retorcidas dentro de su cabeza. Suena como una sesión de terapia. En cambio, la función número 15 de Pixar es otro hito, un triunfo cinematográfico imperdible que eleva el nivel de lo que la animación puede hacer y prueba que la acción en vivo no tiene elementos en el arte cinematográfico. Oh, ¿dije que era gracioso? Es, por lo general, cuando no estás limpiando una lágrima.

Riley (con la voz de Kaitlyn Dias) está molesta de que su padre (Kyle MacLachlan) y su madre (Diane Lane) la hayan trasladado a San Francisco (de color marrón oscuro) desde el brillante y nevado Minnesota, donde le encantaba jugar al hockey con sus mejores amigos.
        
            