
                      A parte de que el bailarín, Sergei, es espectacular, la película está muy bien tratada. El ritmo, la banda sonora, las entrevistas.

Emocionante y desgarradora a la vez.
        
            