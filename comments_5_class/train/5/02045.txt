
                      La mejor manera de acercarse a la historia reciente de Irán a través del sincero relato de Marjane Satrapi. 'Persépolis' logra divertir, conmover, educar y, sobre todo, hacer reflexionar al espectador.
        
            