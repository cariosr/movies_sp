
                      "Una pastelería en Tokio" es una película imprescindible. Es lenta pero la historia, a pesar de su sencillez, transmite muchas cosas y unas de ellas son la importancia de la compañía y la magia de hacer feliz a los demás. Todo aquel que diga que es una película de lágrima fácil, realmente lo está expresando mal; es una película que cuenta una realidad, de personas que por razones de ignorancia y discriminación se han tenido que encontrar separadas de la sociedad y también obligadas a no llevar su forma de vivir deseada. Existe una razón para emocionarse: comprendes a los personajes, son reales.
Una receta en una pantalla que enseguida la hueles deliciosa; unos fotogramas  cálidos, rebosantes de vida y color, precioso; una reflexión delicada y pura... un final intenso.
No perdéis nada por ofrecer un par de horas al largometraje, de hecho, ganáis más de lo esperado.

También debemos felicitar al doblaje en castellano, ha hecho un fantástico trabajo. Sin embargo, me quedo con la versión original porque tiene un halo diferente cuando escuchas a los actores japoneses, hay algo que nos conmueve. 
Gracias a la productora española por traernos a España esta maravilla.
        
            