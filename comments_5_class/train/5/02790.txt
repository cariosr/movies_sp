
                      Es realmente una gran película. Se burla con clase de los estereotipos que la sociedad tiene fijados para nosotros, en los que nos segrega, en definitiva de los “ganadores” y de los “perdedores”. Se ríe de los modelos, de lo que “debemos ser”, de lo “ideal”.

Toca en el punto y lo hace de forma magistral, porque no solo es entretenida, sino que, en el trayecto (a la par del viaje al concurso-raod movie-) toca todo tipo de sensaciones y experiencias personales con sólidos argumentos. Un notable mix de drama y comedia.

La familia es la familia y esta vaya que tiene sus problemas… como las tienen todas, solo que lo expresa con gran fineza. 

