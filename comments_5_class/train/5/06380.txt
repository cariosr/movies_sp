
                      Graciosa, divertida, jovial, tierna, emocionante... para todos los públicos, Perfecta para ver con  los niños. Leo Harmel, no se sale de su papel humorístico y como siempre nos deja una agradable sonrisa. La historia  es fácil de seguir y se pasa muy rápido ya que no puedes aburrirte ni un momento. Perfecta para el verano. Totalmente recomendable.
        
            