
                      Una magistral película extremadamnte dura sobre el mundo de la enseñanza secundaria. A pesar de que ya se han sacado otros filmes  que tratan el mismo tema (Rebelión en las aulas) creo que El Profesor es la que mejor retrata esta sociedad y lo hace con un estilo excepcional. Y el actor Adrien Brody merece un punto aparte por su extraordinaria actuación. 
Una pena que la gente la desdeñe porque según ellos resulte pretenciosa.
        
            