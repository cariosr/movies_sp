
                      (Atención: SPOILERS) (Crítica a Kill Bill (2003) y Kill Bill Vol 2 (2004) A ver, no conozco mucho del cine de Quentin Tarantino, pero esta película carece de puntos negativos y rebalsa de buena historia, buenos arcos argumentales, excelente banda sonora, mucha creatividad y un gran giro al final de la historia en general.
Me encanta el personaje de Uma Thurman (Beatrix Kiddo) me parece que es un personaje con mucha profundidad y estilo.
Lo mejor de la película me parece el giro final en dónde la hija de Beatrix aparece con Bill, desde ese punto, hasta el final, es lo mejor de lo mejor.
La escena de anime me encanta, al igual que la de Vernita Green, y en general, todas las escenas de acción, aunque mi favorita es la de Elle Driver, es PERFECTA.
En fin, esta película, (si, digo que es una sola película ya que a mi me parece una película cortada en 2) me encanta, amo las escenas de acción y la historia.
[CALIFICACIÓN FINAL: 9.0/10]
        
            