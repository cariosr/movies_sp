
                      Una película perfecta en todos los sentidos. Una magnífica historia que no necesita grandes cosas para crear algo grande. Se trata de una película dramática que sin embargo nos permite disfrutar de momentos en los que reír. La luz natural, la música y la fotografía, sencillas pero excepcionales sumadas a la increíble actuación de sus cinco jóvenes protagonistas, que aportan frescura y nos hacen sentir conmovidas por las hermanas, adolescentes fuertes y rebeldes que se convierten en una gran inspiración, conforman una gran película.
        
            