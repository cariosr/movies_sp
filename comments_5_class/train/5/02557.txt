
                      ¿Que podía salir mal?

Esta película lo tenía todo para ser una gran película y así lo ha sido,imprescindible la oscarizada "The Departed",una historia de topos,ratas,de engaños y de mafiosos. Martin Scorsese dirige a la perfección este drama policiáco,no hay pegas,una dirección correctísima,todo perfectísimamente cuidado al detalle,con un guión muy bueno,sin entrar a valorar la originalidad de la historia,su posible plagio etc... he de decir que está perfectamente estructurado,dando información en las dosis oportunas,con escenas emocionantes,todo está preparado para que no pestañes,para que no mires el reloj,y para que pases un rato muy muy entretenido. En cuanto a historia,todo muy bien atado,y yo al menos me lo he creído. En cuanto a la música,algunas buenas canciones pero realmente no me ha entusiasmado.

Pasando al reparto y los personajes,he de decir que todos me han interesado mucho,sobre todo por supuesto el de un Leonardo DiCaprio deslumbrante y es que Scorsese no sería él sin DiCaprio,estos equipos director-actor como Tim Burton-Johnny Depp son una gozada para todo espectador,y además en este caso no solo está el loco y frenético actor y personajhe Dicaprio,también contamos con Jack Nicholson como el malo,fantástico como siempre,un Alec Baldwin al que yo tenía algo más encasillado y me ha costado creerme su papel,un Matt Damon genial que compartía protagonismo con Leonardo DiCaprio de una ¡manera más que digna,este actor sabe ponerse triste,sabe ponerse violento y sabe como actuar,un gran actor,en cuanto a la pareja Mark  Wahlberg y Martin Sheen muy buena, me han gustado estos personajes y su actitud frente a los peligros.El resto del reparto,bien,y aunque no contaré nada por supuesto,tengo que decir que no me ha parecido un mal final en absoluto,pasa lo...que tiene que pasar.

Lo mejor: La estructura y la enorme capacidad de entretener del filme
Lo peor: Pocas escenas para la historia,y es una pena.

Conclusión: Con los mejores ingredientes y un gran cocinero,esto es lo que sale.Imprescindible
        
            