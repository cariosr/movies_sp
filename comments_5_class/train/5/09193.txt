
                      Una de mis películas favoritas de Terry Gilliam. Lo tiene todo: comedia, drama, acción, emoción, crítica, reflexión...en una distopía orwelliana (aunque se abusa mucho de este adjetivo). Recomendadísima!
        
            