
                      El Juez es un drama familiar dirigido por David Dobkin, director de películas como 'Demasiado profundo' y 'De boda en boda'.

Hank Palmer es un reconocido abogado de cuidad. Un día, en medio de un juicio le llaman para comunicarle que su madre ha fallecido.
Hank vuelve a su pueblo natal para asistir al entierro de su madre, y acaba quedándose para defender a su padre por un supuesto asesinato.

Al ser David Dobkin un director ducho en comedia, es normal que al drama no le falten algunos toques de humor en momentos concretos.

Tanto Robert Downey Jr. como Robet Duvall se salen en esta película. Es más que evidente que saltan chispas entre sus personajes, pero a pesar del odio que se tienen, Hank (Downey Jr.) no deja a su padre en ningún momento, aunque él no se deje ayudar.
El drama se construye solo con esta relación paternofilial, sin embargo, hay historias paralelas que terminan de dramatizar toda esta relación y la situación personal del protagonista, que se desmorona poco a poco.
        
            