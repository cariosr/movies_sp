
                      El clásico de todos los tiempos.
Nos narra la historia de unos personajes ficticios con el trasfondo de la guerra de secesión.                       Una nación sacudida por la guerra,mientras una joven 
muchacha libra su propia batalla para sobrevivir, convirtiendose en un icono de "nunca te rindas"(reflejado en la escena "A Dios pongo por testigo de que jamás volveré a pasar hambre").                                         Impresionantes paisajes vestuario
. Esta película(una de las primeras a color) no habría sido la misma sin los cinco directores,que pasaron por la direcion,dispuestos a comerse el mundo con el best-seller de Margaret Mitchell y las deslumbrantes actuaciones de los actores(a destacar la de Vivien Leigh,inmensa como Scarlett O'Hara).                                                Sin duda la grandeza de "Lo que el viento se llevo" reside en el echo de que nos relata una historia como la vida misma,sin añadir dramas lacrimógenos,dándole un toque soberbio gracias a la mezcla de géneros(comedia,romántico,épico,histórico,aventuras y drama)
        
            