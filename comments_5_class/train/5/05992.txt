
                      Una auténtica joya este documental de creación de un director valiente y arriesgado como es el gallego Juan Pinzás. Una película muy emotiva en la que la vida y la muerte están a flor de piel, como lo está el arte en general y el cine en particular en esta singular propuesta cinematográfica. Paul Naschy, en la que sería su última película antes de su fallecimiento, Javier Gurruchaga y el propio director Juan Pinzás van en busca de la catarsis en este viaje iniciático en el que el espectador se convierte en compañero y cómplice.
        
            