
                      Resulta una pelicula que mas que miedo, crea un ambiente claustrofobico y mal rollito. Los actores han actuado de maravilla con un guion impresionante. Terror psicologico, me recuerda mucho al resplandor no llegar asustar tanto como el insiduos o sinister, pero vale la pena darle una oportunidad a esta pelicula de bajo presupuesto. Mi recomendación es que lo veais en version original, con las luces apagadas y los altavoces a tope

Es una pelicula imprescindible si te gusta el terror puro. La trama esconde lecturas que es mejor analizar despues
        
            