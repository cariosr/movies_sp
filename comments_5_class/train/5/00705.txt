
                      Alejandro Iñárritu (México, 1963) trajo en el 2014 esta tragicomedia con la que logra ganar el Óscar a mejor película y las nominaciones a mejor actor (Michael Keaton), mejor actriz de reparto (Emma Stone) y mejor actor de reparto (Edward Norton).

Es una película que ha de verse en uno de esos días o momentos en los que estemos dispuestos a sumergirnos en la pantalla, estemos en plan filosófico o simplemente queremos pararnos a reflexionar. No podemos esperar a sentarnos y a que tan sólo unos actores, guionistas y director de turno nos entretengan mientras comemos unas palomitas.
Nos tocará participar en la trama, en cierto modo estamos prácticamente ante una obra de teatro y según pasa la obra descubriremos que estamos subidos en el escenario, de un modo u otro, somos “actores” en más de una escena.
Es cine del bueno, sí, pero la primera crítica que oí de la cinta fue un “me han dicho que es lenta y aburrida”.
Imagino a esa persona que vio la película e hizo la crítica en cuestión. Me la imagino esperando ver Los Vengadores o cualquier otra película repleta de acción y efectos especiales y de repente le sueltan Birdman. 
Hay películas que te llenan de acción, suspense, ruidos de balas, terror, efectos especiales.
Birdman te vacía para con gota a gota de whisky del peleón (un Ballantine's por ejemplo) llenarte de realidad, de un punto de ella en el que quizá no te habías puesto a pensar… pensar, esa es una buena noticia, no te asustes. Pensar está bien, plantearse cosas, dudar, enfadarse con uno mismo, conocer una parte de ti que no conocías a través de actores. Eso era el teatro y eso se ha ido perdiendo. Birdman lo recupera.

Como hemos dicho en esta cinta no se trata  de esperar sentado a que  pasen cosas, has de entrar entre bambalinas de un teatro donde un actor muy conocido en el pasado por interpretar al famoso superhéroe Birdman, Riggan Thomson (Michael Keaton) nos guía por los pasillos oscuros y mugrientos, unos camerinos caóticos y nos comunica con el resto de integrantes y sus bajezas, de una obra aún por estrenar.
La cámara nos ayuda a entrar en Riggan, en ponernos en su piel, a sentir su nerviosismo antes del preestreno e incluso a hacernos partícipes de su locura.
En otros momentos seremos su hija, Sam (Emma Stone) siendo testigos en primera línea de sus inseguridades miedos y carencias tan características de la adolescencia. Una de esas carencias es la de padre… o no. Iremos descubriendo con ella cómo está la relación entre padre e hija y cómo evoluciona.
Y de repente entra en escena la nueva estrella de la obra, Mike Shiner (Edward Norton) para poner todo aún más patas arriba si cabe: la obra, la relación entre los actores e incluso la visión de sí mismo del propio Riggan.

Birdman es una crítica al cine, al teatro, a los críticos de cine y de teatro, es un viaje a la condición humana ante esas críticas. En cierto modo aunque es una película que no frena, constantemente están pasando cosas relevantes, nos hace parar a reflexionar.
Una de esas reflexiones es si la cantidad de gente que admira algo, convierte a ese algo en bueno. O qué deberíamos o no ser capaces de sacrificar por alcanzar un sueño, cuál es la verdadera naturaleza de ese sueño y sobre todo, cuál es nuestra verdadera naturaleza y cuándo somos actores?

La elección de los actores es excelente, a nadie se le escapa el paralelismo entre el Batman de Keaton y el Birdman de Riggan, también las interpretaciones son de una intensidad digna de admiración, rodadas de modo continuo, sin cortes, obligando a los actores a dar lo mejor de sí mismos. Y es que esta película “suda” teatro.

Por todo ello y bajo las condiciones señaladas de predisposición a querer reflexionar, recomiendo ver esta gran película donde cine y teatro, realidad y ficción, grandeza y mediocridad, tragedia y comedia, se dan de la mano para llevarnos de viaje a la vida y sus grandes y hermosas contradicciones.
Saludos desde la tumba.
        
            