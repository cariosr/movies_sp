
                      Algunos se preguntarán el porque de las puntuaciones tan altas. Cuando la historia es infantíl y facilona. La profundidad es casi nula y no tiene ninguna complicación mental. Pues bien, precisamente por eso, tiene esa puntuación. Porque es lo que te esperas. Vas a ver una película de Super héroes, y es lo que hay. No es la típica película que te venden el trailer y luego no es ni la mitad o es todo lo contrario. Esta es exáctamente lo que te venden, incluso más si eres fan del género. Y el sentido del humor es bestial.
        
            