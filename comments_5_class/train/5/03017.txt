
                      El Lobo de wall street es sin duda la mejor película de Martín Scorsese. Su tono cómico que podría quitarle peso a priori esconde un mensaje muy poderoso y moral que relaciona espectador y protagonistas. Al principio nos reímos con ellos y luego nos reímos de ellos.
        
            