
                      Ha sabido entender al personaje y mostrar su fuerza. Los actores han conseguido descubrir los deseos que llevan a una mujer de hace un siglo a perseguir metas inimaginables por sus coetáneas. Es un ejemplo de energía y muestra que no hay que estigmatizar a las personas por el hecho de querer ser diferentes a lo que la sociedad les exige
        
            