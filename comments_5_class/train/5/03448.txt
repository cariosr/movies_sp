
                      Sin pretensiones, estéticamente perfecta y con una historia de fondo que da miedito sólo de pensar en lo que te está contando. Te despierta muchos sentimientos: asco, vergüenza, pena... Está bien sentir en el cine y no siempre pensar.
        
            