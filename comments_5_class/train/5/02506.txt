
                      Técnicamente buena. Los actores muy bien, salvo el periodista y su padre. Una de las mejores recreaciones de guerra que he visto. Te mete dentro. Mi pega: son como dos películas en una, con dos historias que no acaban de entrelazarse bien y la garra del guión se diluye. Polémicas aparte, trata de dar un mensaje positivo. Olga está genial, como siempre.
        
            