
                      En mi opinión como espectador,Guardianes De La Galaxia es la prueba de que Marvel puede llevar a personajes e historias a la pantalla grande por más desconocidas que sean y triunfar a lo grande,casi como los vengadores,en una trama súper cómica,fresca,llena de escenarios coloridos,acción en todo momento y con unos personajes entrañables que logran simpatizar y caer bien al público,desde la letal Gamora,interpretada por Zoe Zaldana,hasta el genial Star-Lord,interpretado por Chris Pratt.Como dije anteriormente,esta es la prueba de que cuando Marvel quiere,puede superarse,convirtiendo a personajes totalmente desconocidos,a favoritos del público,que se se súper valora.La película expande el universo cinematográfico de Marvel de una forma nunca vista,y lo hace de la forma más entretenida de principio a fin.Por más absurda que sea la idea de tener a un árbol y mapache parlantes de protagonistas,Marvel convierte el resultado en un éxito total,en el que justamente lo absurdo e innovador es lo que triunfa,el único punto malo que le veo,es el villano Ronan,ya es costumbre que Marvel utilice villanos débiles y olvidables,a excepción de Loki(Thor,Los vengadores),fuera de eso,los efectos especiales no solo son espectaculares sinó también de los mejores que se han visto en las películas de Marvel.Sin mucho más que agregar,los Guardianes De La Galaxia son otra auténtica obra maestra hecha por Marvel Studios,100% recomendable para pasar un increíble y entretenido rato viendo acción y risas aseguradas.

Puntos a favor:personajes y trama súper entretenidos e innovadores,efectos especiales,escenas de acción.

Puntos en contra:Villano poderoso pero olvidable.

Mi calificación:10/10
        
            