
                      Esta película es excelente. Confieso que empecé a verla con cierto desgano y sólo por el nombre de Spike Lee. Poco a poco la magnífica actuación del protagonista, la viveza del diálogo y, claro,  el trabajo del Director me fueron atrapando y tuve que verla hasta que terminó.  Aún me parece increíble que un solo actor hubiera  mantenido mi atención por tanto tiempo pero así fue. Sumamente recomendable.
        
            