
                      RETRATO DE UN APASIONANTE Y VISCERAL PRIMER GRAN AMOR...

El amor se podría definir como un concepto universal relativo a la afinidad entre seres, un sentimiento relacionado con el afecto y el apego, resultante y productor de una serie de actitudes, emociones y experiencias. Esas emociones pueden ser extremadamente poderosas, llegando con frecuencia a ser irresistibles. El director franco-tunecino Abdellatif Kechiche desarrolla dicho concepto a través de la historia del primer gran amor de una adolescente quinceañera llamada Adèle.

La película empieza (no en vano) en una clase de instituto en el que el profesor de turno explica a sus alumnos la concepción de lo que conocemos como "flechazo", que vendría a ser ese arrebato pasional y emocional que te altera profundamente, que sientes mágico y milagroso, dándote un extraordinario bienestar y te cambia la vida de color. En este caso, a nuestra protagonista se le podría atribuir el color azul como detonante de ese sentimiento, desconocido por ella hasta ese instante.

En la novela gráfica en la que el director se basa llamada "El azul es un color cálido" ("Le bleu est une couleur chaude" en su título original en francés), su creadora Julie Maroh juega con ello de forma muy inteligente, ya que el primer color que aparece, tanto en las viñetas como en la vida de la protagonista, es el azul; algo que Kechiche ha sabido plasmar de una forma brillante.

Destaca el impresionante trabajo de las dos actrices Léa Seydoux como Emma, la joven del pelo azul; y sobretodo Adèle Exarchopoulos, protagonista absoluta del filme, que interpreta a Adèle con una convicción descomunal, llenándola de matices que van desde la forma de comer; de tocarse el pelo; de llorar; o de esas miradas. Lo dicho, impresionante... todo un descubrimiento.

Quizás el punto en contra que puede adolecer esta película es su larga duración, tres horas... ya que la premisa bien solo podría haber dado para una hora y media máximo. Además está la famosa escena de sexo (de siete minutos) rodada con genitales falsos que tanta polémica y polvo a suscitado, y a decir verdad resulta algo excesiva para una película convencional.

Pero a pesar de todo, la trama está muy bien desarrollada, cocinada a fuego lento, en el que se va desgranando con una naturalidad pasmosa y excepcional la relación amorosa de estas dos jóvenes, sin medias tintas ni artificios; de forma visceral, apasionante, y conmovedora. Kechiche desliza la cámara y nos muestra todos los detalles de la vida de esta chica, de su primer amor, con primeros planos, y otros tantos que evocan belleza poética, incidiendo en ciertos detalles, algo redundantes, que con el paso del metraje van marcando el devenir del tiempo en nuestra protagonista.

En definitiva, "La vida de Adèle", dejando de lado su polémica, y los prejuicios que pueden ocasionar la relación entre dos mujeres, que dicho sea de paso, puedo afirmar que si hubieran sido dos chicos jóvenes, o un chico y una chica, de igual manera hubiera funcionado; ya que estamos ante una obra maestra, que nos cuenta una de las historias de amor más fascinantes que ha dado el séptimo arte en toda su historia.
        
            