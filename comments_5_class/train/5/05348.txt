
                      Tarantino y el mejor guión de la historia del cine.

Estamos ante al menos,uno de los mejores guiones de la historia cinematográfica,una de las mejores peliculas del cine y ¿Por qué? Simplemente porque Tarantino hace otro cine,otro cine que no sigue las pautas oficiales,que no tiene miedo a nada,que se acerca a la gente,que tiene una narrativa perfecta y envidiada. La estructura de la historia es espectacular,la dirección es buena y el reparto genial,John Travolta,Samuel L.Jackson y Bruce Willis estan únicos,puedo decir con seguridad que Pulp Fiction ha cambiado el curso de las cosas,ha convertido la Coca-Cola en Pepsi,Tarantino sobrepasa los límites con su película mas atrevida,y es que las películas atrevidas son las obras maestras. 

Me gustaría también hablar de la música,que es simplemente genial,Pulp Fiction me descubrió Misirlou y el tema de los creditos,simplemente una de las mejores BSO del cine,en mi opinión no la mejor pero sí una de ellas.

Y para acabar digamos lo mejor de la pelicula el ¿por qué tengo que ver esta película? Yo diría que por esos dialogos tan espectaculares,esas escenas como las del baile,como las del coche,como las de las citas biblicas,en fin..un sin fin de escenas con las disfrutas muchísimo.

¿Lo mejor?  El guión
¿Lo peor?  -------------

Termino diciendo que es imprescindible,una obra maestra y una cita del filme si me lo permitís. 

Tenemos que sacar este coche de la calle, la pasma suele fijarse en los coches que circulan bañados en sangre.
        
            