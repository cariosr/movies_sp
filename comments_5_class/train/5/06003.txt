
                      Sin duda mi segunda película favorita, Donnie Darko es una de esas películas que tienes que ver si te gusta el cine de suspense y psicológico. Un joven con problemas de esquizofrenia que a través de un extraño conejo (Frank), que sólo él ve, es capaz de viajar por el tiempo y ver lo que pasará en un futuro, anunciando así el final del mundo cuando todo acabará. La visión de esa película requiere los cinco sentidos puestos en la pantalla. Muy recomendada.
        
            