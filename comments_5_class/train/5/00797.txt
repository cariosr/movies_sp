
                      Historia extraordinaria que te pone el corazón en un puño. No soy fan de Tom Hanks pero he de reconocer que esta, puede ser una de las mejores actuaciones del cine moderno.
La banda sonora es espectacular, la historia magnífica, la única pega es que se puede hacer larga en algunos tramos. Sin lugar a dudas esta película no puede faltar en casa de cualquier cinefilo.
        
            