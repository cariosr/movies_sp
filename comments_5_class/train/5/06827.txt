
                      Es una de las secuencias más perturbadoras de la historia cinematográfica: una mujer pasa de puntillas por la casa hasta que, en tres tomas de staccato, descubre el cadáver sangriento de su ocupante en el piso de la habitación, con las cuencas de los ojos dos agujeros negros llenos de sangre.

La escena es solo uno de varios momentos espeluznantes en la incomparable película de horror de Alfred Hitchcock, The Birds, realizada tres años después de su gran éxito con Psycho.

Comienza como una comedia de inflado. Melanie Daniels (Tippi Hedren) es una playgirl muy atractiva que maneja un aston Martin. Después de que un abogado inteligente llamado Mitch Brenner (Rod Taylor) coquetea con ella en una tienda de mascotas en San Francisco, ella lo rastrea hasta Bodega Bay, al norte de California, donde los fines de semana con su madre (Jessica Tandy). La australiana Taylor, que había ganado elogios por su papel en The Time Machine, murió en enero de 2014 a la edad de 84 años.

Poco después de que ella se fuera a verlo, una gaviota le pica la cabeza, un ominoso sustituto de la flecha de Cupido, y no pasa mucho tiempo antes de que el pueblo sea atacado por bandadas de gaviotas y cuervos.
        
            