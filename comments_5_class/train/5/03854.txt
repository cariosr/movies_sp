
                      Esta muy bien. Tanto para los que ya hayan visto el musical como para los que no. Los solos se hacen un poco más pesados, pero es más debido a que la cámara solo enfoca a su cara. Los actores perfectos, defienden muy bien sus papeles y para no ser cantantes lo hacen bien. La película va en crescendo. A partir del "sale el sol" y las barricadas llegan todos los mementos álgidos. Muy bien llevada la historia de Victor Hugo. 5 estrellas
        
            