
                      El Diario de Noa es un drama romántico dirigido por Nick Cassavetes, basado en la novela El Cuaderno de Noah, de Nicholas Sparks. La última canción, Cuando te encuentre y Querido John también son novelas de este autor que se han llevado a la pantalla.

La película empieza con una pareja de ancianos que vive en una residencia. Él le lee a su compañera, que padece demencia senil, todos los días la historia de amor entre Noa y Allie, que tiene lugar en 1940.

Noa, el personaje de Ryan Gosling, es un adolescente de clase baja, muy apasionado y decidido a conseguir lo que quiere.
Allie, por otro lado, es una muchacha de ciudad, refinada pero algo rebelde.
Este entrañable amor de verano queda en un segundo plano comparado con la relación que tienen los dos ancianos que comparten esta historia.

Como buen drama, este llega a tocar la fibra sensible de cualquiera, ya que el romance que narra la película es sobre un amor puro que pasa por sus dificultades, pero que siempre pervive.

Nada en esta película parece fallar. Los personajes, la época en la que está ambientada, la banda sonora que acompaña a los momentos más tiernos y demás, está organizado de manera que todo queda en perfecta armonía.
        
            