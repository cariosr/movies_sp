
                      Soy fan de la ciencia ficción y de los comics. Por ende era inevitable que vea esta pelicula, con mucho mas razón si ya habia visto las anteriores a esta basadas en los personajes de la misma.
Sin ánimo de desacreditar otras del genero, creo sin equivocarme que es la mejor en lo suyo. Los dialogos, el guion general de la pelicula, son de lo mejor, y cobran sentido si sabes la historia de cada uno de los personajes. Para ser suncero, si vas a ver esta pelicula, tienes que haber visto las anteriores de sus protagonistas. De otro modo no lograras entender bien la trama. Aun así, es muy divertida, sarcastica, tiene la acción necesaria para ser la primer parte donde los personajes se encuentran por primera vez y deben conocerse. Y tiene unos efectos especiales espectaculares.
Los que califican negativamente esta pelicula, es lisa y llanamente porque no les gusta el genero de la misma.
        
            