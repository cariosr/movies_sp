
                      Esta muy buena, no muy recomenda para menores de 15, el estilo típico del director y una trama genial con muy buenos personajes; me hubiese gustado que no se hubiesen enfocado tanto en la parte política, y que los personajes secundarios que aparecen en mayor parte en la primera mitad de la película, no los hubiesen dejado tan de lado en la segunda mitad, cabe destacar que el sountrack es muy bueno y su animación magistral
        
            