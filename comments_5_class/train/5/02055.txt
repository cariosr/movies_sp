
                      Chicago es un drama musical dirigido por Rob Marshall, director de 'Memorias de una geisha' y 'Piratas del Caribe: En mareas misteriosas'.

Roxie Hart entra en prisión tras haber matado a su amante por haberla engañado. Este le había prometido convertirla en una estrella.
En la cárcel se encuentra con su ídolo, Velma Kelly, quién mató a su marido y a su hermana.
Las dos lucharán para llamar la atención del prestigioso abogado Billy Flynn. Él las hará famosas, además de salvarlas de la horca.

Renée Zellweger, Richard Gere y Catherine Zeta-Jones protagonizan este drama basado en el musical de teatro estrenado en los años 70.
Los actores, las personajes, las canciones, vestuario, trama... Todo es una maravilla bien encajada. El resultado es una película más que recomendable y entretenida.

Danny Elfman se encarga de la música. Ha puesto banda sonora a películas como 'Epic', 'La gran estafa americana', 'Alicia en el país de las maravillas' y muchas películas más que forman parte de una larga lista.
Ha creado también el tema principal de los Simpson.

La película fue nominada a treinta y ocho categorías en diferentes premios, de los cuales, ganó catorce.
        
            