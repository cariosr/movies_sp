
                      Considero que es una película magia y que te lleva de la mano hacia los inicios del cine. Una película apasionante, diferente, conmovedora y que hace muchísimo tiempo que vi, pero aún no la he olvidado.
Una historia de un chico corriente al que le pasa algo extraordinario.
        
            