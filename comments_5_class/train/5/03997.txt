
                      Se podría decir sin lugar a dudas que esta es la mejor película que se ha hecho sobre Batman. 
Su argumento, que se centra no solo en las mafias y organizaciones criminales que controlan la ciudad de Gotham, o en el papel que juega Batman como justiciero, sino principalmente en los problemas que genera el capitalismo, el conformismo y la hipocresía social. Es un grito contra la sociedad actual que se muestra en forma de uno de los mejores villanos interpretados en la historia del cine.
En efecto, Head Ledger con su increíble interpretación de el Joker, eclipsa incluso a Cristian Bale, que hace de un Batman/Bruce Wayne más realista y creíble que los que hemos visto hasta el día de hoy en el cine.
Este Joker, que busca instaurar el caos y abrir los ojos a la sociedad, haciéndoles ver que cualquiera podría volverse loco con un poco de ayuda, nos mantiene atentos toda la película y nos hace disfrutarla en todo momento con su personalidad tan arrolladora y lunática. 
El Caballero Oscuro de Nolan nos resta la ficción de los cómics y aumenta el suspense, transformando a batman en lo que es realmente, un gran detective justiciero en un thriller con mucha acción y muchos momentos tensos y que dan lugar a muchos debates sociológicos.
El personaje de Dos Caras/Harvey Dent, interpretado por Aaron Eckhart, es la guinda del pastel , que introducen de la mejor de las maneras posibles y coordina muy bien con la trama.
En general el reparto de esta película es excepcional, compuesto por actores increíbles que bordan sus papeles y que encajan a la perfección con la historia. 
Es una película que se disfruta al verla, pero que también te hace reflexionar, y eso no es fácil de conseguir, y menos en películas de superhéroes, que suelen ser más o menos parecidas y con tramas y argumentos similares.
Desde luego esta es una de las mejores películas de la historia, no solo de superhéroes sino en general.
        
            