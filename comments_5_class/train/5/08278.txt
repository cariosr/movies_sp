
                      Considero que esta parte de la saga es magnífica! La actuación de Alan Rickman es soberbia, está hecha para él. Personalmente, me quedé con ganas de mas, pero eso le da también un sabor especial al personaje y a toda la saga. Me hizo reír y llorar, gané muchos buenos amigos con quienes nos vemos y compartimos momentos únicos.
        
            