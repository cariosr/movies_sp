
                      Sin palabras. Acabé la película con los sentimientos a flor de piel. Nadie diría que es el debut como directora de Andrea Jaurrieta. De lo mejor que he visto en el panorama español. Ojalá tenga el reconocimiento que se merece. Brillante.
        
            