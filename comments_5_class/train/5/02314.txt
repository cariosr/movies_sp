
                      "La representación de un ser tétrico, misterioso y profundamente tierno es el reflejo de una sociedad mundana y austera. Burton acompaña su sello artístico terrorífico pero siempre apasionante con un tono mucho más ameno y divertido. Danny Elfmann compone el "soundtrack" de la década"
        
            