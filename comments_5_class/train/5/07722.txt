
                      Cuando una película se une a ti para el resto de tu vida.
No he visto película en toda mi vida que llegue a este nivel, y puede que nunca la vea.
No puedo expresar con palabras lo mucho que me fascino este film.
Decir que Braveheart es un peliculón es quedarse corto. Como siempre, el veredicto depende de cada uno, de sus motivaciones y lo que en él despierte. En mí, Braveheart lo ha sido todo.

Desde aquel primer visionado en el cine cuando se estreno y hasta día de hoy. Se podría decir que Braveheart me ha ayudado a elegir una parte de mi camino, a tomar decisiones o a basarlas en ella. Hasta extremos insospechados y que analizados en frío, dan que pensar.

Braveheart es una película en la que sus detractores pueden encontrar multitud de fallos de rigor histórico, de raccord, de abuso de primeros planos del protagonista, pero sus defensores encontramos una historia además de real e histórica, muy épica, que realza valores que nos haría falta ensalzar, una historia no solo de guerra si no también de amor, del inevitable y del prohibido, con la presencia de la amistad por encima de casi todo y de la lealtad a unos valores por encima de todo...

En la mente de todos están esas escenas claves, épicas y tantas veces vistas y cacareadas, discursos que ya son parte de la historia del cine, y escenas de guerra tan pioneras como realistas.

El personaje de William Wallace es una maravilla, la actuación de Mel Gibson es sin duda la mejor de toda su vida, hasta el punto de que no me he imagino Braveheart sin Gibson.
Los demás actores están también muy decentes, destacando a "Patrick McGoohan", "David O´Hara", "Catherine McCormack" y "Angus MacFadyen" en el papel de Robert the Bruce. 

Tal vez otra palabra para definir esta película sea: Preciosa. Destacando la banda sonora y las maravillosas escenas dedicadas a los paisajes escoceses al principio del film. También destaco las escenas en las que Wallace va corriendo por las montañas con la música céltica de fondo.
Esta película es un claro ejemplo del efecto que puede causar una buena banda sonora en un film.

En definitiva, Obra maestra. Pocas películas consiguen llegarte al corazón y un poco más allá, Braveheart para mí es una de esas pocas, y seguramente, la más especial de todas.
        
            