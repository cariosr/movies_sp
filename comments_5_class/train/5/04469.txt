
                      Maravillosa. Dos veces la hemos disfrutado ya y quizás la segunda más por pararte en los detalles, que son muchos.
Toda la intro grabada en un solo plano merece todos los premios del mundo.

Para mí, lo mejor son la gran cantidad de secuencias dignas del mejor cine mudo donde no se necesitan diálogos, solo gesticular, interpretar y buena música para acompañarlo...

Sin duda la mejor película de esta temporada.
        
            