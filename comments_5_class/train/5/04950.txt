
                      A los 78 años Clint Eastwood se despide magníficamente de la actuación. Como en los últimos tiempos, ha escogido guiones precisos. Y este en particular, enfrenta las distancias generacionales y culturales de los personajes. Da la impresión que en esta despedida Eastwood hace un resumen de sus interpretaciones más saltantes: el "duro", el "entrenador", el "ajustador de cuentas", el "culposo", y también al "sensible" y mordaz". Y como escribí en mi blog personal, la película trata sobre la violencia, ya sea la silenciosa, la determinada por las diferencias, la violencia compasiva... aquellas que conviven en nosotros mismos.
El gran Torino es el "gran tratado sobre la convivencia". 
Pulgares hacia arriba.
        
            