
                      Tiene todo lo que se puede pedir a una película como ésta, acción a raudales, ciertos toques de humor bien ubicados, buena interpretación, desarrollo argumental sobresaliente. Muy entretenida desde el principio al final, casi dos horas de espectacular cine muy recomendable.
        
            