
                      -¡¡ Si la fe en la Navidad desea volver a tener, "Arthur Christmas: Operación regalo 3D" tiene que ver !!.

Con esta increíble comedia familiar, se nos revela la increíble incógnita, algo que todo niño a querido saber, se responde a la hasta hoy casi eterna pregunta, que todos los niños se plantean: "¿cómo reparte Santa todos esos regalos en una sola noche?" En "Arthur Christmas: Operación regalo" se esconde la solución.

Recién llegada de los estudios Aardman, tres de sus grandes ingredientes a resaltar son, su belleza visual, la genialidad de su argumento/personajes y el tremendo buen humor que la acompañan.

No es el típico cuento de Navidad, es mucho más.

En once palabras; "Arthur Christmas: Operación regalo " representa un renovado Espíritu de la Navidad.

Encandilara a mayores y pequeños. Un esencial para estas fechas que se acercan.
        
            