
                      Con el aire calmado de ciertas historias apocalípticas de mediados del siglo pasado, la película es lo que muchos deseábamos ver: los conflictos de una serie de personas en medio de una situación extrema.
        
            