
                      "Mundo Jurásico" es una secuela directa de la primera, ignorando las decepcionantes secuelas de Parque Jurásico.

Es una de las películas mas aclamadas de este año, por detrás de "Los Vengadores: La era de Ultron" y "Rápido y Furioso 7", y que a parte ha tenido un mixto recibimiento por parte del publico. 

Pero bueno, estoy aquí apara compartirles mi opinión sobre esta película y vamos para allá.

Las cosas que agradezco de esta película son: 

Los efectos visuales: Es algo que a comparación de las primeras películas de Parque Jurásico ha mejorado mucho. Pero en esta película, simplemente impresiona.

La música o banda sonora: Es algo que te regresa al espíritu de la primera, aunque sea muy diferente.

Las actuaciones: Esta vez cuenta con un gran elenco que hace interpretaciones decentes y aceptables hasta cierto punto. Principal agradecimiento a Bryce Dallas Howard y Chris Pratt que son de los mejores actores de la película. Los demás, lo hacen bien.

El argumento: No confundan el guion, en un momento hablare sobre ello. Aquí, el argumento es fácil y nada complejo, lo cual hace sentir la película muy ágil. Esto es algo que no impresiona tanto y no desbanca la primera película de Parque Jurásico, pero si esta bien.

Ahora voy a hablarles de las cosas que fallaron en la película:

Los personajes: Son casi los mismos que la primera, no son nada originales, salvo por el personaje de Bryce Dallas Howard, que va evolucionando conforme pasa la película. Los demás son mas de lo mismo.

El guion: Es una gran decepción la forma en que se escribió el guion y como quisieron plantear las escenas de acción. Las sientes medio simplonas las escenas, ridículas y ademas ilógicas. Y desde ahí vamos al gran fallo de la película. 

Quisieron que esta película fuera muy emocionante y cargada de intensidad, pero no logro serlo. La película, es bastante ilógica y nada tiene sentido ahí. Las cosas solo están por estar ahí, y no te dan una razón alguna. 

Terminando, esta película esta bien como entretenimiento, pero para analizar y criticar, es una gran falla cinematográfica.
        
            