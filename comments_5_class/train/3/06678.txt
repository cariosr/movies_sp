
                      Influenciado por las buenas criticas, me decidí ir a ver este largometraje, y lo que me encontré fue una mezcla de Brokeback Mountain y Ghost, llevado a un país latinoamericano. El largometraje es correcto, con unas interpretaciones bastante creíbles y una buena fotografía y localizaciones, con un guión que en ningún momento se hace pesado. Aunque he de decir que a mí no me cuadra mucho en este tipo de largometrajes, el tema de las apariciones y los fantasmas, pues creo que les quitan seriedad a la trama.
En resumen: cine para gente sin prejuicios y con un poco de sensibilidad.
        
            