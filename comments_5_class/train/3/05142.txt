
                      Una segunda parte muy entretenida y con mucho movimiento que te engancha desde el primer minuto hasta el final.

La historia parte un año después de la primera película, los jinetes intentan conseguir un trabajo a toda costa para volver a la acción, al final les designan una misión y con ella un nuevo miembro de los jinetes, la misión se ve truncada por un mago que desea descubrirlos y hundirlos. Toda la trama esta muy entretenida y dicho mago es "Harry Potter" haciendo el papel de malo gracioso. La historia esta muy bien y ahí muchas partes entretenidas, el hermano gemelo de Woody Harrelson esta muy gracioso, la aparición de Morgan Freeman y Michael Caine le da un toque interesante. Me gusto la escena de la carta que paso de uno a otro por "arte de magia". 
Merece la pena ir a verla ya que se pasa un buen rato y disfrutras de una buena comedia (a veces) y es bastante entretenida.
        
            