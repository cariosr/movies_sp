
                      Esperaba encontrarme algo tan "buahhh" y me quedé en un "psss". Es cierto que Vigalondo ha conseguido algunos puntazos, y hace fácil y cercana su particular historia de extraterrestres, pero no ha llegado a ser lo que esperaba. O bien no tengo ni idea de cine y me linchan a negativos por no saber la "maestría" del cántabro, o alguien más piensa igual que yo.

Punto aparte, y estelar, ahí sí que ha estado fino el director, son sus actores. Omitiré cualquier comentario de Jenner que no baje de un piropo jocoso que haya podido soltar alguna vez a una chica. Grande Julián Villagrán, y el tándem Areces-Cimas es la nota chistosa ("muchachada") de la película.
        
            