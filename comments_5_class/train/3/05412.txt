
                      Hace unos días descubrí “Contracted“: una peculiar película de género zombie que asombró hace unos años en el festival de Sitges y que obviamente, y para alegría de todos, tendría una secuela. Pues bien, esa secuela ha llegado y continúa de forma muy digna el terror de la primera.

La historia comienza en el mismo punto donde lo dejó la primera. Si en la fase 1 descubrimos cómo Sam se contagia de una extraña enfermedad que la degeneraría poco a poco hasta convertirla en una putrefacta muerta viviente, ahora es el turno del siguiente infectado. Riley, sufre ahora los mismos síntomas que su amiga, y está dispuesto a descubrir qué sucede con él y sobre todo, no está dispuesto a acabar como Samantha.

La cinta continúa el malrollo de su antecesora a través de escenas que remueven el estomgo y te obligan, en más de una ocasión, dejar de mirar la pantalla. De nuevo, la curiosa manera de tratar una infección zombie es la clave de la calidad de estas películas, centrándonos en esta secuela en saber más sobre el origen y procedencia de todo el mal. Muchas cosas que nos preguntábamos en la fase 1, ahora son contestadas mientras una nueva historia de infección desagradable centra toda nuestra atención durante una corta hora y veinte. Como suele ocurrir en este tipo de historias, la investigación por lo sucedido en la primera parte también centra gran parte de la trama, algo necesario para poder entender más la esencia de esta extraña, divertida y nueva “enfermedad zombie”. Y por supuesto, una excusa más para tener mas escenas gores y brutas con las que deleitarnos.

Lo mejor: la propagación de la infección.
Lo peor: que no este Najarra Townsend.
        
            