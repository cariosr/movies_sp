
                      Ante todo quiero dejar claro que soy un apasionado del género y de El Señor de los Anillos (de Jackson y de antes de Jackson).

En general me pareció un intento de ESDLA, pero infructuoso y tal vez demasiado infantil. No me bastó con que aparecieran personajes y escenarios de la anterior trilogía, con la música de Howard Shore, con la sensación de regreso a mi añorada Tierra Media. Todos esos elementos aportaban una pre dosis de entrañabilidad, pero son lo mejor de la película. Sin la subjetividad al entrar al cine, la lentitud del principio, las tropecientas caídas de los enanos, los trasgos cantarines y alfabetos, las peleas-que-desde-la-primera-son-la-definitiva, etc... No están a la altura. 
Claro que qué se puede esperar de la primera parte de una trilogía que abarcará un libro de menos de 300 páginas? Será la primera vez que se tarde, literalmente, menos tiempo en leer la versión literaria que en ver la versión cinematográfica.
        
            