
                      Es un caso claro de rendimientos decrecientes. ¿Qué dice acerca de una torpísima y muy pesada secuela de un reinicio de 2012 para el que realmente no había necesidad de llorar en primer lugar?

Dice esto: The Amazing Spider-Man 2, la quinta película de Spidey en 12 años, es demasiado larga, decepcionante, innecesaria y seguramente será una buena opción. Ahora lo entiendes. A pesar de la ley de promedios, las audiencias todavía piensan que puede haber vida en el héroe de Marvel con las cosas de la web. Y el equipo responsable de disfrazar el cadáver tiene el talento suficiente para engañarnos tal vez una vez más.
        
            