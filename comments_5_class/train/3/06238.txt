
                      El Danzon
El Danzón es una película inspirada por el baile del Danzón y es sobre una mujer soltera que está en búsqueda de su pareja del Danzón. Julia, una madre soltera tiene un pasión por el Danzón, un baile cubano creado por Miguel Failde. Cuando su pareja, Carmelo, se desaparece ella decide ir a buscarlo a Veracruz. Mientras julia esta en busqueda de Carmelo ella se encuentra todo tipo de personas y también a su identidad ella misma.
La directora, María Novaro, es considerada como la cineasta mexicana más exitosa. Novaro tiene una vista feminista y retrata la complejidad de las mujeres en sus películas incluyendo El Danzón.
Maria Rojo personifica a julia, una madre soltera, que está en búsqueda de su pareja. Daniel Rergis, Carmelo se desaparece y hace que Julia haga la decisión de ir a buscarlo. 
Recomiendo esta película porque demuestra y nos revela varias culturas como la Mexicana y la Cubana y también nos enseña que nunca debemos de rendir en algo que queremos.
        
            