
                      Los reclamos de Byzantium no son pocos: Viendo el trailer nos llama la atención el regreso de Neil Jordan al género del terror y a la dirección de una historia de vampiros después del éxito de Entrevista con el vampiro, Saorise Ronan en el papel principal y la sensualidad de Gemma Artenton a todo trapo, y por fin una historia de vampiros de verdad, de los que chupan sangre, pero una vez vista la peli esta solo va solo a medio gas.

El planteamiento es bueno, el reparto también, pero el guión es lento y carente de ritmo. La hambientación es oscura, fría, como debe ser. Hay buenos encuadres y buenas escenas ( como por ejemplo Saorise Ronan chupando un pañuelo empapado de sangre, o cuando Gemma Artenton se baña en una cascada de sangre... uf!) pero la historia tiene muchos huecos, y no llega a enganchar, a ratos parece que se va a lanzar pero se vuelve a ralentizar. El uso de flashbacks es bueno y suele funcionar, pero en este caso no los han sabido aprovechar.

Quizás esté opinando así porque tenía bastantes esperanzas puestas en esta peli y ya he dicho varias veces lo malo que son las espectativas, sobre todo cuando no se cumplen, pero no puedo decir que me haya gustado. No es mala, simplemente que no es atrevida, ni rompedora, ni escandalosa, y de haberlo sido un poco más quizás sí estaríamos hablando de una excelente historia, ya que parece que seguimos atontados por vampiros demasiado buenos y la sombra de Crepúsculo es larga.

En cierto momento me da la sensación que seria una excelente base para una serie de vampiros para continuar en tv, pero eso se debe a que la peli te deja solo a medias.

Así pues solo le doy un aprobado justo.
        
            