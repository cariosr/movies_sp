
                      Básicamente, si te gustan las sagas distópicas (sin ir más lejos, Los Juegos del Hambre), esta es tu película. No os voy a mentir, cuando comencé a verla, pensé que estaba viendo el alter ego de LJDH.
Porque es que es clavada en todos los aspectos: Sociedad pos-apocalíptica dividida en grupos, una chica del grupo más pobre que es muy valiente, el chico enigmático y el grupo más poderoso gobernante.
    Luego, hablemos de la trama.
    La verdad, la historia te atrapa, y tiene unas muy buenas escenas de acción. Habiendo leyendo el libro, es bastante fiel, aunque en algunos aspectos se desvía un poco. Pero, que decir, está bastante bien.
        
            