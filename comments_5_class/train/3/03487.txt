
                      Es difícil escribir una crítica de esta película. Empieza ya con música y créditos de los años 70, definiendo en qué época se desarrolla. Los dos protagonistas están bien, aunque no me gusta el doblaje de Ryan Gosling. Del argumento mejor no hablar mucho, con momentos absurdos que los protagonistas parecen bobos. Es más adulta e inteligente la hija de uno de ellos que los protagonistas. Tiene algunos gags buenos, en los que te ríes de verdad, pero en el global, por las críticas previas, la verdad es que esperaba más.
        
            