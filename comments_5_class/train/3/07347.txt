
                      No hace falta llevar más de 30 años casado, para imaginarse lo que puede hacer la rutina. Interesante película sobre un tema importante, muy bien resuelta. Mery Streep brillante como siempre, Tommy Lee Jones increible también.
        
            