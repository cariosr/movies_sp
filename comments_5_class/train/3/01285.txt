
                      Esto muestra a que extremos puede llegar una mujer celosa.

La película es de drama y trata de darnos algo de suspenso y miedo con las típicas escenas de terror (Rayos, gritos, música fuerte).

Sus primeros 30 minutos les hacia falta de buen dialogo y escenas con buena química, por tanto, cuando llega el giro dramático ya es tarde para enganchar al espectador.

Clara Lago (Belén) logra darle mas interés, suspenso y emoción cuando aparece. Levantado un poco mas los tediosos 30 minutos iniciales. Ella fue quien le dio vida al filme mientras que la actuación del protagonista siempre fue seria y fría, casi ni sonreía el tipo

Coclución: Es una película entretenida para verla una sola vez y luego encerrarla y olvidarla en el cuarto escondido detrás del espejo por su falta de profundización en el inicio y final de la película pero que cuenta con un buen nudo desarrollado (Giro dramático de la historia).
        
            