
                      Un abordaje a la depresión post traumática adolescente tras un accidente fatal que le cuesta la vida a uno de los 4 amigos. Excelente ambientación noventosa al estilo Strangers Things (bicicletas, suburbios, tecnología de la época...). El desarrollo es atrapante pero el final me resultó previsible e inconcluso, no estuvo a la altura.
        
            