
                      la realización esta bien, el ambiente americano que consigue darle a la película, gracias en parte al elenco, la ambientación y el vestuario de la España en la Guerra Civil también están muy logrados. Estos factores hacen que la película invite a sumergirte de buen grado en esa cruel etapa que fueron los bombardeos a ciudades por parte de los ejércitos fascistas. Además, el buen ritmo de la película, que decae en muy pocos momentos, consigue el interés del espectador.

Pero aun viéndole todos estos puntos positivos al filme, hay algo en él que no termina de funcionar; la película no consigue un trasfondo épico ni desgarrador, no hay mucha tensión, y el guión no profundiza en los conflictos de cada trama, fluye superficialmente, y por esa razón el espectador no empatiza ni con la historia ni con los protagonistas.
        
            