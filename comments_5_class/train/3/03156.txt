
                      La leyenda del Samurai. 47 Ronin es una adaptación de una famosa leyenda  japonesa que data del  siglo XVII, en el Japón feudal, la "Leyenda de los 47 ronin", y trata el código de honor samurái, el Bushido. Los 47 Ronin son 47 Samurais caídos en desgracia cuando su señor es obligado a cometer sepukku o Harakiri.

Dirigida por el director novel Carl Erik Rinsch,  y protagonizada por Keanu Reeves, Hiroyuki Sanada, Kô Shibasaki, Tadanobu Asano y Rinku Kikuchi (quien recientemente hemos visto en Pacific Rim) entre otros, esta leyenda está envuelta en magia y brujería, y con criaturas fantásticas.

La película es entretenida, pero no va más allá.
El debutante director Carl E. Rinch nos trae una película visualmente espectacular, muy colorista y con algunas escenas esplendidas pero no profundiza nada en sus personajes que resultan planos en todo momento. Keanu Reeves no tiene ninguna pega en las luchas a espada pero carece de expresión interpretativa, la única que destacan un poco en su papel es Rinku Kikuchi como la Bruja Mizuki.
 
Es una lástima que la película se quede solamente en entretenida ya que la cultura y código Samurai da para profundizar y mostrar esta fascinante cultura pero el guión simplemente pasa por los elementos espectaculares y fantásticos de la historia alguno de ellos metido con calzador ya que no termina de encajar del todo en la trama.

Mención especial a la banda sonora a manos de Ilan Eshkeri quien ya brilló con luz propia con la B.S.O. de Stardust.

En resumen, la peli se deja ver y es entretenida pero no pasará a ser ningún gran clásico ni por su historia ni por sus interpretaciones, pero su música permanecerá en mis listas de reproducción.
        
            