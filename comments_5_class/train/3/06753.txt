
                      Pero... ¿quién mató a Harry? (The Trouble with Harry) es una comedia dirigida por Alfred Hitchcock, director de Extraños en un tren, La ventana indiscreta  y Rebeca.

Una mañana de otoño, mientras un viejo capitán (Edmund Gwenn) está de caza por el bosque, se encuentra con un cadáver con heridas de bala. Éste se cree el culpable de asesinato e intenta esconder el cuerpo. Un joven pintor (John Forsythe), una madre soltera (Shirley MacLaine) y una mujer solitaria (Mildred Natwick) comprueban la inocencia del capitán, y lo ayudan a decidir qué hacer con el fallecido, Harry.

Los primeros minutos de la película son una locura; desde que encuentran al cuerpo, hasta que deciden cómo actuar, una sucesión de vecinos pasan por delante del cadáver casi sin inmutarse.

Alfred Hitchcock, el maestro del suspense, flaquea con esta comedia. La intriga y el suspense están presentes, y eso es lo que le acaba dando valor a la cinta. Lo malo es lo estrambóticos que son los personajes, pretende ser cómico, pero no lo es.

La película obtuvo dos nominaciones a los premios BAFTA; mejor película y mejor actriz (Shirley MacLaine).
        
            