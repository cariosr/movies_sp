
                      Ese es mi punto de partida de cinco películas de Terminator. No importa lo que hagamos, no importa cuántos intentos hagamos para atravesar el tiempo para detenerlo, vamos a terminar con un sistema de computadora consciente de sí mismo que está empeñado en la destrucción de la humanidad.

Después de una breve reintroducción a la serie, vemos el lanzamiento de los nukes mientras una voz en off de Kyle Reese (Jai Courtney) explica cómo las máquinas tomaron conciencia y lanzaron Judgment Day en 1997: Terminator Genisys se abre en un futuro post-apocalíptico. John Connor (Jason Clarke) y Reese están en una misión para destruir a Skynet. Es la última batalla de la guerra, dice, y el público familiarizado con la serie se dará cuenta de dónde y cuándo estamos: en el sitio del dispositivo de desplazamiento temporal momentos antes de que Kyle sea enviado a tiempo para salvar a Sarah Connor (Emilia Clarke).

Cuando Reese ingresa al dispositivo, algo sale mal. John es asaltado por uno de los soldados en la sala, un evento que interrumpe el flujo de tiempo y hace que Reese cree nuevos recuerdos, "recuerdos imposibles", como él mismo lo dice.

Y luego estamos en Los Ángeles. Es 1984. Hay un familiar modelo nude 101 Series 800 que exige ropa de un grupo de punks. Pero nada más es lo que recordamos: este recién llegado se enfrenta a una versión más vieja y vieja de la misma máquina (Arnold Schwarzenegger) que lucha junto a Sarah Connor y a quien Sarah llama cariñosamente "Pop" porque la crió después de salvarla de ella. un Terminator que asesinó a su gente en 1973. Kyle Reese ha aterrizado afuera de una tienda de ropa y se ha robado los pantalones de un vagabundo. Pero se encontró con un T-1000.
        
            