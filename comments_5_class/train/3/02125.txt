
                      Tiene buenos momentos, pero a Bollaín se le escapa el guión según avanza la trama. Y al tiempo que trata de solucionar lo inverosímil de la situación, los códigos actorales se van distanciando de la realidad. Problemas de hablar del cine dentro del cine y de la opresión por boca de los que oprimen.
        
            