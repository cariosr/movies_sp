
                      Al final de la película no sabemos demasiado de cómo es realmente Walter Mitty. La trama amorosa no logra que nos impliquemos.  El ritmo, lento. Resulta más llevadero en los primeros minutos de la cinta que en el momento en el que el protagonista decide emprender su aventura. Lo que nos queda del film es un cuidada imagen y un mensaje. Al final la película no aporta más que su trailer.
        
            