
                      Hay dos cosas que siempre me ha sido muy difícil soportar de las películas de Almodóvar: La decidida vocación egocéntrica que las alimenta, y fruto de ella, supongo, la sobreexposición de sus historias. Por eso, de sus películas, me gustan más entre menos de él tengan o menos explicitado esté. Algo que le cuesta. Supongo que tiene que ver con cómo es este director y que no puede hacer otra cosa. Algo muy respetable, por lo honesto, pero que no tiene porque agradar. Lo honesto sólo es honesto. Y el cine es mucho más.
No voy a ver todas sus películas, me aburro, pero esta me apetecía verla por un detalle: había visto a Antonio Banderas en el tráiler y lo que vi me sorprendió.
Este actor es fundamentalmente un actor de carácter, que cuando interpreta tiende a ser, o blanco o negro. El matiz no es lo suyo. Pero creo que, viendo esta interpretación, he estado equivocado todo este tiempo. Antonio Banderas sí que puede matizar pero hasta ahora no había encontrado el método.
¿Y cuál es el método, según yo?
Tener a la persona a la que va a interpretar al alcance de su mirada o conocerlo. En carne viva. Eso, o puede que con la vejez, le haya llegado como a  los buenos vinos, las cualidades que hasta ese momento no había tenido.
Sea como sea, Banderas hace una interpretación sobresaliente del “alter ego” de Almodóvar.  Y es lo único que me ha parecido reseñable en este film. Lo demás es más Almodóvar.
A mi hija pequeña le encantan los decorados saturados de  este cineasta, a mi me agobian y no me dejan espacio para entrar en la historia. Su artificiosidad, los colores puros, intensos. La teatralidad de los personajes… la escena de las mujeres lavando la ropa en el río y cantando, con el niño zascandileando por allí, es directamente de cromo. Y a mí ese cine me parece cine en todo momento. Y para eso voy al teatro.
Seguramente Antonio Banderas en este papel habrá tenido toda la libertad del mundo. Al interpretar a Pedro Almodovar y hacerlo tan bien, a este le habrá parecido cojonudo. Dos “pedroalmodóvar” en vez de uno. ¡Con lo que Bergman llegó a hablar de él en sus películas y lo poco que se le notaba!
Indudablemente Pedro Almodóvar es un director muy personal que dejará su impronta en el cine español, y con más razón en el cine en general, por lo pintoresco, pero a mí no me acaba de gustar.
¡Qué le vamos a hacer!
        
            