
                      Lo interesante de la peli son cosas paralelas, como la emoción al sentir la nieve, la inseguridad de ciertas decisiones, los paseos solitarios con el perro. El mensaje es positivo, el amor incondicional por un perro desastroso, pero que te ha acompañado durante unos cuantos años y ha visto nacer a todos tus hijos. Lo demás es bastante accesorio.y convencional.
        
            