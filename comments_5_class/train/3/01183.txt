
                      Footloose es una película musical dirigida por Herbert Ross, director de 'Sueños de un seductor' y 'California Suite' entre otras.

Ren McCormack (Kevin Bacon) es un adolescente que se ha mudado desde Chicago hasta un pequeño pueblo, cuya alcaldía ha prohibido la música y el baile por un accidente que tuvo lugar hace cinco años.
Ren despierta la rebeldía entre sus compañeros de clase y les incita a luchar para que la absurda ley que prohíbe la música, quede abolida.

A pesar de tratarse de una película emblemática de los años 80, esta no pasa de ser entretenida. La idea está bien planteada en un principio, y las canciones y los bailes son lo mejor de la cinta, pero los diálogos son insulsos y la trama no da mucho de sí.

Algo a favor del largometraje son los personajes secundarios, entre los que se encuentran Sarah Jessica Parker y John Lithgow, que consiguen condimentar ligeramente la historia.

La banda sonora tiene como tema principal la canción de Kenny Loggins; Footloose, del mismo nombre que la película.
Ésta obtuvo dos nominacines a los Oscars por las mejores canciones originales.
        
            