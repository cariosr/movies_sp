
                      Creo que esta película, y quiero decir trilogía, es menos profunda en desarrollo de personajes y con tramos realmente infantiles que no le hacen bien al universo de Tolkien. Eso si, mención aparte, cabe destacar las escenas de Gollum como futuros iconos del mundo del cine durante muchos años, simplemente genial, yo le pondría taparrabos de gala para recojer su estatuilla de los Oscar, se lo merece.
        
            