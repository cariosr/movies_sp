
                      Si bien la premisa es original, contándonos como el "negocio" de las cigüeñas se ha reciclado con el paso de los años (ya no reparten bebés, sino que se han convertido en algo como una especie de Amazon), así como nos retrata personajes curiosos como la pareja protagonista (Junior y la huerfanita), al final acaba cayendo en un argumento convencional de aventura infantil que se queda en simplemente entretenida. La animación es aceptable (sin caer en la excelencia) y el producto final resulta ameno y simpático
        
            