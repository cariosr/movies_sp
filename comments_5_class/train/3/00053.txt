
                      Film sin tema ni mensaje definido que entretiene lo justo, sin mayor tensión ni drama, que cabría mejorar; se me ocurre sin esfuerzo una tensión celotípica a resolver en un plano con dos máquinas de escribir, por ejemplo. Desconozco a este director pero parece que cualquiera la hubiera hecho mejor. Una aceptable película
 sobre la esposa de Hitch, con todo.
        
            