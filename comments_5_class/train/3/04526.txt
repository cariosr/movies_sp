
                      Esta última película de Danièle Thompson es más bien una sorpresa. No demasiado aferrada a "Fauteuils D'Orchestre", bastante convencional y demasiado romanticona a mi parecer, he encontrado este filme mucho más original particularmente en el montaje y con una historia un poco más realista. La selección de actores es más bien acertada, en especial en el género femenino con Karin Viard, Marina Foïs y sobre todo con Emmanuelle Seigner.
        
            