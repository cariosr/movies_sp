
                      Partiendo de la base de una historia excesivamente inverosímil, la película supera las expectativas que crea a priori y es cuando menos, entretenida. De hecho salí del cine bastante satisfecho para lo que habitualmente se espera de películas así. Buena
 interpretación de Zac, muy ceñido al personaje, no así de la chica, un poco bastante mejorable para mi. Un 5.
        
            