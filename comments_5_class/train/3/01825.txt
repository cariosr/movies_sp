
                      El primer "Daddy's Home" fue filmado en Nueva Orleans, y la brillante secuela titulada "Daddy's Home 2" fue filmada en Massachusetts, pero estos personajes unidimensionales, profundamente neuróticos y en algunos casos seriamente perturbados también podrían estar pensando en Asgard para toda su conexión con algo parecido al mundo en el que vivimos.

Sí. Sé que es una farsa de payasadas, con aspiraciones a seguir en la amplia tradición de comedias de "Vacaciones Navideñas de Lampoon Nacional" o incluso el primer "Solo en casa".

Diferencia clave: incluso cuando Clark Griswold crea estragos con sus luces de Navidad desenfrenadas o Kevin McCallister inventa los artilugios de Rube Goldberg para frustrar a los Wet Bandits, podemos relacionarnos con el desafortunado hombre de la familia Clark o los olvidados, solos. niño Kevin.

Con la posible excepción de un personaje femenino que queda relegado a un segundo plano cuando Boys-to-Men de mediana edad y sexagenario enciende el foco con sus payasadas cada vez más débiles e infantiles, nadie en "Daddy's Home 2" habla o se comporta de forma en que la mayoría de nosotros podemos sentir empatía (o encontrar entretenimiento) en cualquier nivel.

"Daddy's Home 2" y "Bad Moms Christmas" en realidad viajan notablemente (y deprimentemente) en trazados similares.

• En "Bad Moms Christmas", las madres de las malas mamás llegan justo a tiempo para las fiestas, portando todo tipo de equipaje familiar y causando estragos en la vida de las familias de sus hijas. Un abuelo dominante insiste en organizar una celebración navideña lujosa y exagerada. Mientras tanto, los nietos quedan atrapados en las Guerras de afecto.

Ah, y antes de que todo haya terminado, las semillas se plantan para un posible conjunto de tres quel en Las Vegas.
        
            