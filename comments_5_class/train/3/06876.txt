
                      Sin ser una de las obras cumbre de la factoría Disney, tiene el suficiente encanto para ser muy entretenida, siendo sus platos fuertes la ambientación (Nueva Orleans) y su estilo musical (Jazz), en un giro a la clásica historia del príncipe convertido en rana.
        
            