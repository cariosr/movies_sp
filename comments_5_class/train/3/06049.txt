
                      Blood Father (2016).
El responsable de la revisión del clásico Asalto en el Distrito 13 nos trae la última de acción de Mel Gibson ambientada en el sur de EE.UU. y donde vuelve a ejercer de padre protector, aunque con un pasado bastante negro.

Una cinta de acción y persecuciones dignamente rodada, entretenida y sobria, aunque sin descubrir nada nuevo. 
Gibson está correcto como de costumbre, y se hace acompañar, entre otros, del veterano y premiado actor William H. Macy (algo menos arrugado que nuestro protagonista) y de Michael Parks (actor fetiche de Robert Rodríguez junto a Cheech Marin) que se sale del ya habitual papel de shérif al que nos tiene acostumbrados.

Raúl Cabral.
        
            