
                      ¡Shazam! (2019).
“Si un superhéroe no puede salvar a su familia, ¡qué clase de héroe es!"
Shazam, el personaje “adulescente” de DC Comics de 1972, por fin tiene superproducción cinematográfica (100 millones de dólares). El director, un asiduo al cine de terror, se cambia de género para mostrarnos la primera parte de una nueva saga de superhéroes, en esta ocasión dirigida al público más joven.

Zachary Levi -actor de cine y tv, doblador, cantante y hasta realizador- encarna al personaje en cuestión. Aunque ciertamente no es muy conocido en nuestro país, algunos lo recordarán por interpretar a Fandral, uno de los personajes secundarios de la saga de Thor. Le acompañan dos veteranos del cine dramático y de acción; Mark Strong, en el rol del supervillano, y Djimon Hounsou, el mentor de Shazam (acrónimo de Salomón, Hércules, Atlas, Zeus, Aquiles y Mercurio).

La cinta está rodada a caballo entre la ciudad de Rocky (Philadelphia) y Canadá, y sus efectos especiales hacen que nos entretengamos durante las más de dos horas que dura mientras llega el gran estreno de la primavera, "Vengadores. Endgame”... por algo el presupuesto de ésta triplica al de aquélla...

Raúl Cabral.
https://www.filmaffinity.com/es/film583362.html
        
            