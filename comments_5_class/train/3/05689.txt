
                      Es un testimonio del ingenio de los realizadores detrás del Día de la Muerte Feliz que pueden hacer que incluso el deja vu parezca fresco y original. Esta es una película de terror / comedia romántica / thriller del productor de terror de bajo presupuesto Jason Blum que desgarra su premisa descaradamente del Día de la Marmota de Harold Ramis (pero al menos tiene la gracia de reconocer que lo está haciendo).

Está ingeniosamente estructurado por derecho propio, incluso si algunos de los giros y contorsiones de la trama no se acumulan por completo. El director Christopher B Landon (hijo de Michael Landon de Little House On The Prairie) combina los elementos macabros y cómicos de una manera generalmente efectiva.

Jessica Rothe interpreta a Theresa "Tree", una estudiante universitaria que se despierta en el dormitorio de un joven, Carter (Israel Broussard), que ella no reconoce. Ella tiene mucha resaca y no sabe si se acostó con él o no. Es su cumpleaños, pero ella no está feliz en absoluto.

Alejado de su padre, cuyas llamadas telefónicas ella se niega a responder, aún afligida por la muerte de su madre, se ha convertido en egoísta y malcriada. Está teniendo una aventura amorosa con un profesor universitario y es una de las miembros más desagradables de su hermandad de campus extremadamente odiosa. Efectivamente, pronto descubre que está viviendo exactamente el mismo día una y otra vez. Siempre termina cuando ella es asesinada por un hombre con una máscara grotesca.

Los cineastas inventan muchas formas diferentes de matar a "Tree" pero la mayoría involucra que sea apuñalada hasta la muerte. Al igual que Bill Murray en el Día de la Marmota, atraviesa cambios de humor extremos cuando se ve obligada a repetir las mismas experiencias una y otra vez. Ella se mueve de la furia a la resignación a la iluminación.

Finalmente se convierte en detective para poder resolver su propio asesinato. En el proceso, se vuelve cada vez más cercana a Carter, una persona de corte limpio pero insulsa a la que al principio quiere evitar. Aquí hay muchos momentos espeluznantes que podrían provenir directamente de una película slasher de los 80, pero se sientan junto a escenas que podrían pertenecer a una comedia de campus de estilo bratpack de John Hughes.
        
            