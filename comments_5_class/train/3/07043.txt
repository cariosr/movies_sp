
                      Frankenweenie es una película artística y madura, donde es fácil identificar el clásico estilo Burton, su atractivo visual, los gráficos fantasmagóricos, el barroquismo, su humor macabro y las moralejas veladas. Probablemente no guste al público más joven, aunque es menos oscura y más familiar que La novia cadáver y tiene algún susto especialmente diseñado para el 3D.
        
            