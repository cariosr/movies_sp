
                      Suburbicon (2017).
El, probablemente, actor más atractivo de todos los tiempos vuelve a ponerse tras las cámaras para llevar a la gran pantalla esta comedia negra escrita, además de por él mismo, por los hermanos Coen.

La acción del largometraje de Clooney se ambienta en el final de la década de 1950, y es una mordaz crítica a la hipócrita sociedad estadounidense, que bien podría trasladarse a nuestros días: falsas apariencias, racismo extremo, avaricia e inmoralidad.

Matt Damon encabeza el reparto en un papel alejado de su conocido Bourne. La incombustible Julianne Moore le acompaña interpretando magistralmente un doble personaje, y Oscar Isaac también cambia de registro dejando Star Wars para sorprendernos encarnando a un inspector de seguros de lo más cínico...

Aunque la música chirría un poco, supongo que el retorcido humor de los Coen podría tener algo que ver al haber contado  con un "score" tipo peli de los 50's. No obstante, mejora conforme la sangre empieza a brotar...

Creo que el adjetivo a emplear esta vez es 'interesante' al calificar esta historia.

Raúl Cabral.
https://www.filmaffinity.com/es/film424255.html
        
            