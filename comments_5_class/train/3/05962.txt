
                      La película no está mal, es entretenida y se deja ver. La actriz Alicia Vikander está bien como actriz pero creo que para este personaje era mejor la Jolie, tenía más fuerza y era más creíble. Algunas escenas están sobrepasadas de efectos especiales. También creo que falta un poco más de acción. La vi en 3D pero creo que no aporta mucho.
        
            