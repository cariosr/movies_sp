
                      Una bonita parábola sobre dejar atrás los fantasmas del pasado envuelta en temática de ciencia ficción. Pese a todo, la cinta es un homenaje en vida a la obra de Steven Spielberg, haciendo guiños a sus películas (Los Goonies, E.T., Encuentros En La Tercera Fase, Tiburón, Jurassic Park e incluso Salvar Al Soldado Ryan)
        
            