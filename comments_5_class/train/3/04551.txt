
                      The Old Man and the Gun (2018).
“Hice todo lo que me pidió porque llevaba un arma... y porque era todo un caballero."
El octogenario, filántropo e impulsor del certamen de cine "indie” Sundance, Robert Redford, nos trae su última obra antes de despedirse definitivamente del 7º Arte, al menos como actor.

“-Forrest, tiene que haber un modo más sencillo de ganarse la vida.
-No se trata de ganarse la vida... se trata de vivirla."
En esta historia basada en hechos reales, Redford encarna a un elegante atracador con mucha clase que cometió decenas de robos a mano armada -sin llegar a herir a nadie- y se fugó de la cárcel en una docena y media de ocasiones. En algunas de sus fechorías lo acompañan los veteranos Danny Glover y el cantante Tom Waits formando una banda heptagenaria similar a la de "Un Golpe con Estilo” (2017); y en el terreno sentimental, la mujer que interpretó la ‘Carrie’ original de De Palma, Sissy Spacek.

“Vestir elegante te llevará lejos. Parecerá que sabes lo que haces... aunque no lo sepas."
El agente de policía que apodó “Los Carrozas” al trío de viejos delincuentes es el hermano menor de Ben Affleck, Casey, quien le sigue los pasos lenta pero inexorablemente, mientras 'el hombre que susurrara a los caballos' hace un par de decenios ahora lo hace, pero a las sorprendidas cajeras de cada banco al que atraca, revólver en mano.

La película ambientada magnificamente  en 1981 es sobria, y algo lenta, pero su hora y media justa de metraje deja visionarla hasta el final; y es que el compañero y amigo del gran Paul Newman merece ser visto por última vez en la gran pantalla...

Raúl Cabral.
https://www.filmaffinity.com/es/film139016.html
        
            