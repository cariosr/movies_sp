
                      Entretenida película de Robert Downey Jr. y Zach Galifianakis en una nuevo film sobre un viaje en carretera (road movie). No se debería comparar con "Resacón en Las Vegas" aún cuando aparezca uno de los protagonistas: Galifianakis. Un conjunto de casuales desgracias será el inicio de la historia que llevarán a los protagonistas a desplazarse juntos desde Atlanta hasta Los Ángeles. A partir de ahí sucederán diversos hechos que les terminarán uniendo y separando. Como decía al principio sólo busca el entretenimiento y pasar un rato divertido.
        
            