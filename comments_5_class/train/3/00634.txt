
                      Película dramática sobre la dama de hierro contada de una forma atractiva, pero que no llega a empatizar con el público, no nos cuenta toda la historia completa de Margaret Thatcher y los personajes secundarios son poco creíbles, lo que es la trama me esperaba muchísimo más, pero la actuación de Meryl Streep es impecable y sin duda lo mejor de la película, un oscar bien merecido para ella. Vale la pena verla por ella, nada más.
        
            