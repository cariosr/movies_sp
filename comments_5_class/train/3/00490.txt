
                      Walking On Sunshine (2014). 
Hoy tocó otra forma de ver cine; película musical + cine de verano. 

En la "performance" de esta noche pudimos disfrutar de animaciones con cada número musical que aparecía en pantalla. Algo original y diferente. 

El filme en sí es una comedia romántica de género musical, a gran distancia de cintas como Greese, y a años luz de Moulin Rouge, pero que gracias a su banda sonora, compuesta en su totalidad por temas de la mejor época pop- los 80's- se deja ver. 

Una comedia ligera sin trascendencia para entretener las calurosas noches estivales. 

Raúl Cabral.
        
            