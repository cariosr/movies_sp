
                      Muñeco Diabólico (2019).
“Presentamos a vuestro nuevo mejor amigo..."
Tres décadas después del inicio de esta larga y longeva saga, llega la revisión del conocido y "amable” muñeco Chucky. En esta ocasión la tecnología sustituye a la maldad sobrenatural, pero el fondo sigue siendo el mismo: un muñeco sin moral y muy peligroso.

La trama sigue siendo igual de infantiloide y sangrienta. Los actores en esta versión 2019 no son apenas conocidos. Curiosamente, el que lo es más solo le pone al muñeco la voz en su versión original. Se trata de Mark Hamill (el inmortal Luke Skywalker). El niño protagonista tiene cierto bagaje en el terreno del thriller y el terror, pero a la mayoría de secundarios no les auguro un Globo de Oro próximamente...

En mi opinión se trata de una peli más de miedo que, aunque producido por los responsables de “It”, están muy lejos de la calidad de ese largometraje -cuya segunda parte se estrena a corto plazo.

Raúl Cabral.
https://www.filmaffinity.com/es/film375071.html
        
            