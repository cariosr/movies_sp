
                      X-Men: Apocalypse (2016).
Vuelve la Patrulla X con una nueva precuela. Esta vez larga de narices! Muy costeada, sin duda; con mucha (demasiada) imaginación; con un joven reparto estelar encabezada por James McAvoy como el Profesor Xavier, Fassbender como Magneto fluctuando entre el bien y el mal, la actriz mejor pagada de Hollywood, J. Lawrence como una joven Mística y el omnipresente Oscar Isaac (Star Wars VII) como el villano ultrapoderoso de turno extrañamente caracterizado.

Los efectos especiales, cada vez mejores en este cine espectáculo, aunque sin un guión acorde no es suficiente. No sabría decir si este es el caso, pero si vamos sin muchas ganas de pensar podemos pasar un buen rato gracias a la acción, a los citados efectos y a un reparto coral de primer orden.
Entretenida.

Raúl Cabral.
        
            