
                      Un film de Ciencia Ficción de Night Shyamalan, que no es precisamente santo de mi devoción pero que en este caso consigue captar mi atención los 100 minutos que dura la película. Un Will Smith en un papel en el que pone la interpretación de Jaden Smith en bandeja de plata (por qué será?). Ambos, vuelven a compartir objetivo en una historia cargada de mensajes de superación con un final que se ve venir.
Resumiendo, es una buena película agradable de ver y bastante entretenida.
        
            