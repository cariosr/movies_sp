
                      El ocaso del mercenario

En una de las imágenes más icónicas de “Sabotage”, Schwarzenegger se sienta a una mesa ataviado como un vaquero, toma un trago, enciende un puro y esboza esa media sonrisa suya tan característica que llevaba hasta entonces conteniendo, justo después de protagonizar una escena al más puro estilo del mariachi Robert Rodríguez. Es el toque de atención del astro austriaco a todos sus fans. Más que vivir el ocaso de su propia filmografía, está más en forma que nunca. Esto no es el fin, sino el punto y seguido que precede a todo lo que está por venir.

Concebida precisamente durante todo su metraje como si del retrato del ocaso del mercenario se tratase, lo nuevo de David Ayer destila sobriedad en lo que al tratamiento de sus personajes se refiere, un equipo de la DEA a cual más bárbaro, aunque las escenas de acción salvajes y no aptas para todos los públicos del realizador, un especialista en filmar como si de un continuo combate cuerpo a cuerpo se tratase, puedan transmitir justo lo contrario.

Así, “Sabotage” es una acertada radiografía del punto en el que se encuentra la carrera de su estrella protagonista. Aunque también atesora otros aciertos, como no convertir al actor en el amo de la función, sino dejar a sus compañeros de reparto, todos ellos magníficamente caracterizados y metidos en sus papeles como si llevasen años preparándose para entrar en “Sons of Anarchy”, respirar entre tanto músculo y testosterona. 

Eso sí, a costa, desgraciadamente, de una realización que en ocasiones se torna demasiado telefílmica, y de algunos pasajes exagerados y efectistas, como la persecución una vez se descubre el pastel que trae de cabeza a sus protagonistas, o esos flashbacks familiares metidos con calzador. Pero, sobre todo, lo que más se resiente en ella es su flojo guión, que acierta más en retratar a sus personajes que en inmiscuirles en una historia atractiva y de desarrollo verosímil. Su trama va perdiendo fuelle conforme avanza, yendo de un comienzo más que potable a un tramo final con no pocos lastres, que viene a remontar el vuelo gracias a una escena final de esas de quitarse el sombrero dado el tipo de producto que es.

Y en medio de tanto tiro, tanto tatuaje y tipo duro, está Schwarzenegger, que da la que podemos considerar como su mejor interpretación en años. Dentro de sus limitaciones interpretativas, claro está. Comedido, serio, cuasi crepuscular. Una estrella que está lejos de vivir su ocaso en el celuloide. Que siga así muchos años más.

A favor: un Schwarzenegger cuasi crepuscular, y la escena final
En contra: su guión, que va perdiendo fuelle minuto tras minuto
        
            