
                      Bueno, ayer me vi esta película, preparando un poco lo que será el esperado blockbuster de Los Vengadores. Era el último film individual que me quedaba de los héroes de este grupo.

No sé si ya lo he comentado por aquí en alguna ocasión. Mi relación con Los Vengadores en los comics se limita a las veces que se han mezclado con otros héroes Marvel, como la Patrulla X. Aunque, obviamente, son personajes muy conocidos y todos hemos visto algún pedazo que otro de series animadas donde aparecen. Con ello quiero dejar claro que no conozco los comics de Thor, y no estoy muy puesto en aventuras asgardianas. Por tanto, me faltan datos para analizar bien la película. Dejo caer esto para no servir de diana a los fans, que a modo de Hawkeye pueden acribillarme a flechazos xD (por cierto, de este último leí comics hace muchos años).

La película me ha parecido aceptable, correcta como pasatiempo. Aunque siempre con la sensación de que todo está desaprovechado. Quizás Kenneth Brannagh no está muy a gusto en un terreno que no es el suyo, aunque creo que la película mejora cuando adquiere tintes de drama shakespiriano, en el tema de la lucha de poder, donde el cineasta británico parece sentirse mucho más cómodo. En este aspecto sí me parece, como se ha comentado, que las partes de Asgard son mucho mejores que las de Nuevo México.

En cuanto al prota, no sé si será la cierta sosería de Chris Hemsworth, o el guión, pero me resulta difícil cogerle aprecio al héroe de esta historia. Ese cambio de personalidad que sufre me resulta tan forzado como fallidamente resuelto. Al igual que el tema del individuo que llega a un mundo extraño y desconocido. No me funciona demasiado bien (el humor me parece mucho más logrado en Iron Man). Quizás porque la anfitriona principal (Natalie Portman) no me parece que esté a la altura, y no es una crítica a ella como actriz. No sé como será la Jane de los comics y su relación con el dios nórdico, pero aquí la química entre ambos creo que brilla por su ausencia. 

Además está muy poco explotado el personaje de Sif, al que parece que se le podía haber sacado mucho más jugo. Una tercera en discordia podría venirle bien al asunto, aunque quizás no había tiempo para todo. Seguramente se verá en posteriores entregas.

Lo que es la ambientación del reino de Asgard me parece que tiene fuerza y atractivo. Para la era de los efectos digitales, de los que no soy nada entusiasta, no está mal. Visualmente bien, aunque las batallas me parecen un tanto descafeinadillas.

Una de las cosas que me pareció más desaprovechada, o decepcionante (aunque no me gusta emplear esa palabra) es el villano de la historia, Loki. Siempre he oído que éste es uno de los grandes enemigos creados por la factoría Marvel. Sin llegar a la altura del gran Doctor Doom, los entendidos me han señalado al hijo de Odín como un malo con toques megalomaniacos, y porte aristocrático (en la línea de Basil Rathbone o James Mason, por ejemplo). Si bien en la película apunta algunos detalles, se queda en menos de lo necesario, pienso yo. De hecho, su maldad es algo ambigua, e incluso al inicio, yo al menos, no puedo evitar estar de su lado al tiempo que Thor me resulta detestable.

Me queda como resultado global una película decente y correcta. Desde luego no una gran película de superhéroes.

Veremos qué tal esos Vengadores. Thor acompañado de Tony Stark y Natasha creo que será más Thor. Y veremos ese Loki con los Skrulls. A ver si hace honor a su fama de gran villano. Como decía Hitchcock, cuanto mejor es el malo, mejor es la película.
        
            