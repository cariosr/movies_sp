
                      Tras un año de espera, la segunda parte de la trilogía divergente lleva a la gran pantalla. Esta vez de manos del director Robert Schwentke.

En un mundo distópico, donde la mayor parte de la civilización ha sido arrasada, se crea una ciudad donde la sociedad está dividida por facciones para así mantener una paz duradera. Esta paz comienza a torcerse cuando abnegación (una de facciones) es atacada por los osados que estaban bajo el mandato de los eruditos. Es ahí cuando la guerra estalla y Tris (Shailene Woodley), junto a sus amigos se ven obligados a huir.

Insurgente, la segunda parte de la adaptación de la trilogía de Veronica Roth, comienza seguida de su antecesora. Tris, Cuatro (Theo James), Caleb (Ansel Elgort) y Peter (Miles Teller) buscan cobijo en cordialidad, donde podrán ocultarse durante el tiempo suficiente para planear un ataque contra Jeanine (Kate Winslet) y la facción de Erudición. Pero la aparición de eruditos en tierras cordiales hace que nuestros protagonistas tengan que huir de nuevo y enfrentarse a nuevos problemas. 

La población no conoce la verdad sobre el ataque de abnegación, el sistema de facciones cada vez es más inestable y los abandonados están dispuesto a luchar por acabar con la injusticia de Jeanine. ¿Unirán fuerzas los osados y los abandonados para detener a Jeanine?.

Desde la aparición del primer teaser ya se podrían apreciar los grandes cambios que iban a hacer con la película. El argumento de la película sigue la línea (muy en general) del libro. El gran cambio ha sido la aparición de la caja misteriosa que es encontrada en la casa de los Prior. Esta caja contiene información para la supervivencia del sistema de facciones, o eso es lo que cree Jeanine, pero solo un autentico divergente puede abrir la caja y desvelar la información. Es por ello por lo que ahora más que nunca se debe encontrar a los Divergentes.

Aún en contra de la aparición de la caja, he de reconocer que esto nos permite ver varias simulaciones, las cuales tienen grandes efectos especiales que hacen la película más atractiva visualmente. Y es que se nota el uso del 3D para Insurgente, ya que gran parte de los efectos especiales han sido creados con esta intención. A pesar de ello la película sigue un ritmo más rápido que la entrega anterior y es más corta, cosa que se agradece, ya que mi pega con Divergente fue su duración.

Respecto a los actores principales, ellos siguen en su línea. La escena más destacada es la de Shailene en su juicio en verdad, ya que por lo demás todo se reduce a acción y a una danza de golpes. Además, por fin aparecen personajes que no fueron incluidos en la adaptación de Divergente, como es el caso de Uriah, Lynn o Marlene, aunque su protagonismo sigue siendo escaso.

Una de las cosas que más me gustó de Divergente fue su espectacular soundtrack, y es algo que en esta película he echado de menos. La música se resume en un score acorde a las escenas y una o dos canciones al final.

A pesar del pésimo doblaje de Tris, los cambios de la trama original, y del final que perfectamente podría ser un final cerrado sino fuera por algo de curiosidad y la aparición de un tercer libro, la película no está nada mal.  Y entiendo los cambios para conseguir un público más allá de los lectores, pero hay momentos donde el argumento flojea y se limita a la acción. Así que como película le doy 4 fotogramas tirando a 3,5 y como adaptación apenas llega al 2, con lo que se queda con 3. 

En general una adaptación que deja bastante que desear, donde prima la acción durante las 2h de película y la intriga sobre el contenido de la caja queda reducida a los últimos 5 minutos. Aun así, la cinta como entretenimiento promete y se hace bastante corta.
        
            