
                      Como dice un personaje en un momento del largometraje: la guerra es una cadena de represalias. Un cuchillo de doble filo, en el que, en el momento te involucras en uno u otro bando es imposible salir indemne de ello, pues siempre acaba dejando secuelas tanto psicológicas como físicas.

CRITICA

Aunque quizás al comienzo del largometraje el desarrollo parezca lento, conforme se va resolviendo el rompecabezas, va cogiendo más intensidad la trama. A partir de la escena del autobús, te metes en la piel de la protagonista, pero debes intuir lo que le va sucediendo, pues el director más que mostrarlo lo insinúa. Escrita y montada de forma muy inteligente con flashbacks continuos la película no hace concesiones al espectador. La actriz belga Lubua Azabal, que hemos visto dirigida por Techiné (Lejos, 2001) o Ridley Scott (Red de mentiras, 2008) interpreta a Nawal Marwan (“la mujer que canta”), con una sobriedad y dignidad que provoca nuestra empatía y también el respeto hacia su personaje.

El espectador le encontrara ciertos paralelismos con la recién estrenada (Encontraras Dragones), pues se desarrolla en dos tiempos, uno de ellos en un conflicto bélico, los hijos van descubriendo el pasado de su madre, y al final el perdón es la salvación.
        
            