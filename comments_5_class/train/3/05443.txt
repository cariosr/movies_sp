
                      Cinematográficamente hablando, Notorious  es un producto bien hecho, que narra puramente la vida  de uno de los raperos más carismáticos de los últimos veinte años, pero al que le falta personalidad.
Gracias a este film podremos ver la vida de este hombre con gran talento que reflejaba en sus letras sus duras experiencias y que marcó a toda una generación. El 9 de marzo de 1997 Notorious falleció asesinado a tiros en su coche.
Los fans estarán encantados, sobre todo por la banda sonora.
        
            