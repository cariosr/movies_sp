
                      Linda historia de amor en tiempos de guerra independentista entre Azerbaiyán y URSS, allá por principios del siglo XIX. Romanticismo, drama y emotividad sumado a un argumento sencillo y bien narrado dan como resultado una buena película, digna de ver. También destaco como mencionan mas abajo la gran ambientación de época. Recomendable.
        
            