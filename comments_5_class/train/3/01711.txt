
                      La pregunta central en la serie Kick-Ass es si las películas son o no una crítica del exceso de género del cómic o simplemente un ejemplo del mismo.

Kick-Ass 2 sugiere que esto último es cierto.

Kick-Ass (2010) nos presentó a Kick-Ass (Aaron Taylor-Johnson), un chico normal que decide convertirse en un superhéroe. Sus torpes desventuras lo meten en problemas con los delincuentes, ganándole una paliza o tres.

Durante uno de estos golpes, Kick-Ass es rescatado por Hit Girl (Chloe Grace Moretz) y Big Daddy (Nicolas Cage), un dúo padre-hija de Batman-and-Robin que no está por encima de los criminales incapacitantes o asesinos.

Kick-Ass y Hit Girl han decidido formar un equipo cuando se abre la secuela. Ella le enseña los caminos del vigilante, lo ayuda a crecer y lo insta a vencer su miedo. Sin embargo, después de que la guardiana de Hit Girl se entera de sus chanchullos, se ve obligada a asistir a la escuela y lidiar con un enemigo mucho peor que los matones callejeros: chicas malas.
        
            