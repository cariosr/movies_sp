
                      Es posible que sea la peor película de la trilogía de Raimi: Después del éxito con Spider-man 2, la tan esperada Spider-man 3 fue una decepción. Por muchas cosas, entre ellas, el ansiado Venom se acabó convirtiendo en un actor secundario, el cool dance de Peter "Emo" Parker en medo de la calle, derivaciones del cómic, etc... Pero no es "mala" hasta decir basta. No es mala del todo, tiene sus cosas positivas.

Sus cosas positivas de cara al público del film: es posible que esta sea la entrega con el mejor Soundtrack de la trilogía,. La unión de Christopher Young y Danny Elfman dieron unos frutos bien jugosos. Varias entregas de actores, como J.J. (lo ha hecho muy bien en esta cadena de filmes) y Flintt Marko, interpretado por un Thomas Haden Church muy pegadizo al papel. Entre otras, efectos especiales o el Black Suite.
        
            