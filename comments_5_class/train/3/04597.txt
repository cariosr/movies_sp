
                      Para pasar el rato. Lo fantasioso e inverosímil son una constante en esta saga. El protagonista siempre logra reponerse a tiros, cuchillazos, caídas y atropelladas de autos inclusive, mientras que sus enemigos nunca corren la misma suerte. Pese a esto, resulta entretenida ya que hay mucha acción de principio a fin y muy buenos efectos. Aceptable.
        
            