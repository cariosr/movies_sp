
                      Nunca ha logrado Shyamalan decepcionarme, pero ha faltado poco para que 'El bosque' se convirtiera en la primera vez. La historia me causa una monotonía rimbombante, en la que resurge la sensación de que se reciclan las escenas, en la que supuestamente hay un personaje más llamado diálogo y que no acabo de encontrar. A pesar de todo, no deja de parecerme una fábula bella, con una banda sonora memorable y un diseño exquisito.
        
            