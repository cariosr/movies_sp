
                      Una película muy sobrevalorada, por aquello de ser indie y venir de Korea.

La peli no está nada mal, es hasta original en su planteamiento (todo pasa en el tren y en las estaciones), pero no dejar de ser una peli previsible sobre un brote de zombis rabiosos, sin nada a destacar.

Los personajes son un poco simples y en general es todo un poco cutre. Aunque es entretenida, eso sí.

Sólo para fans del género. Si no lo eres, puedes pasar de ella.
        
            