
                      Que un gato hable da un poco de pavor, pero si es un gato de las dimensiones y colorido de Garfield, es aún más sorprendente. Garfield es un personaje muy simpático en sus tiras cómicas y parte de su simpatía se ha logrado trasladar a la gran pantalla. No toda, porque sus películas no son realmente obras maestras pero dan para pasar un buen rato. 
Esta vez parece que los temas que suelen acontecer a este minimo son más variados, aunque sigue redundando en su voraz apetito y sus maneras poco gatunas.
No puedo decir que sea mejor que la primera parte, pero sí tiene la ventaja de las tres dimensiones, un recurso bien utilizado en este caso, dándole un tono humorístico en lugar de optar por una forzada espectacularidad. 
Para entretener una tarde a los niños es válida, para promocionarla a un Oscar de la academia se queda corta, sin duda.
        
            