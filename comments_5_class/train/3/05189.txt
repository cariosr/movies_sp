
                      LUCY TODOPODEROSA!!!

La premisa de "Lucy" se basa en la hipótesis que dice que el ser humano sólo usa un reducido porcentaje de su cerebro, un 10%. La ciencia especulativa lleva siglos imaginando lo que pasaría si se consiguiera superar ese límite. ¿De qué seríamos capaces si usásemos todo el cerebro? Si cada una de las ochenta y seis mil millones de neuronas que componen el cerebro humano funcionasen a la vez, ¿podría esa persona convertirse en un superhumano?

Tal teoría, aunque hayan algunos que crean (o han creído) en ella, tales como el mismísmo Albert Einstein, o la comunidad de la Cienciología, que la utiliza como argumento para reclutar adeptos, la hipótesis es una falacia científica; en otras palabras, una teoría totalmente falsa, debido a que si sólo utilizáramos el 10% seríamos vegetales o estaríamos directamente muertos, ya que realmente utilizamos el 100% de nuestra masa cerebral; aunque no de forma simultánea.

El inicio de "Lucy" es simplemente brillante, siendo (junto a las escenas de acción), lo mejor del filme. Me ha gustado mucho la habilidad con la que Besson presenta los personajes, y al mismo tiempo, expone la teoría del porcentaje del cerebro. De modo paralelo, los dos hilos argumentales se van desgranando en el fantástico inicio; igualmente que la comparación de la protagonista, con la otra "Lucy", la que está considerada como el primer homínido de la historia.

Por momentos, parece ser que el director francés haya recuperado ese "touch" que poseía para las secuencias de acción, aquel que tanto disfrutamos con cintas, tales como "León, El Profesional" o "El Quinto Elemento". El excelente trabajo de Scarlett Johansson también ayuda en ello, ya que tras haber descubierto esa faceta interpretativa en el universo cinematográfico de Marvel, con su interpretación de la "Viuda Negra", en esta que nos concierne, Scarlett saca a relucir todo su potencial, echándose muchas veces la película a la espalda; asimismo, podemos afirmar que los veteranos Morgan Freeman y Choi Min-sik son el contrapunto perfecto a la protagonista, dándole empaque al filme, hecho que se agradece.

Por contra, cabe decir que a medida del transcurso de los minutos, da la sensación de que a Besson se le va la película de las manos; o dicho de otro modo, es como si se le "fuera la olla", metiendo algunas incongruencias en el guión que personalmente me chirriaron bastante, ya que siendo ficción el realizador puede tomarse ciertas licencias; sin embargo, no todo vale. 

Pese a esas irregularidades, "Lucy" es una película muy entretenida, muy efectiva en algunos tramos, superando con creces a la soporífera "Transcendece" del "tito" Johnny Deep, y con una Scarlett Johansson que demuestra todo su talento como una actriz válida para la acción, como ya demostraran en el pasado otras actrices "Made in Luc Besson": Natalie Portman, o Milla Jovovich.
        
            