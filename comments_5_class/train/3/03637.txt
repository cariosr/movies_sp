
                      No es tan fácil es una comedia dirigida, escrita y producida por Nancy Meyers, directora y productora de películas como The Holiday y ¿En qué piensan las mujeres?.

Jane y Jake se divorciaron hace diez años, pero una serie de eventos los vuelven a juntar y se replantean su relación. El único inconveniente es que Jake está casado con una mujer mucho más joven que él, y Jane se ha enamorado de Adam, su arquitecto.
Los líos amorosos se van complicando y entrelazando hasta dejar a la protagonista exhausta.

La película tuvo una candidatura al mejor guion original, además de las nominaciones como mejor actriz y actor de reparto a Meryl Streep y Alec Baldwin, respectivamente.
Y no es para menos, ya que la química que desprende la pareja es más que visible.

La única pega es que Steve Martin podría haber aportado mucho más a esta comedia, ya que es un gran actor de este género, pero acaba quedando en un muy poco merecido segundo plano.
        
            