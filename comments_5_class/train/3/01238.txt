
                      Era la primera sesión del domingo, tras consultar varias webs dedicadas al cine, entre ellas esta, en la que escribo esta crítica, me decidí ir a ver este medio largometraje, medio documental. La sala era pequeña, estaba medio llena o medio vacía según se mire.

El largometraje no es que sea una  gran obra, pero trata de un tema poco tratado en el mundo cinematográfico, que nos viene a contar como un arte suburbano, revolucionario y contra cultural, acaba convirtiéndose en un mero negocio para enriquecerse a su costa. En el que nada está improvisado, y realizado al azar.

Yo calificaría a esta película de interesante, por el tema y la forma en que se trata.
Aire fresco para la cinematografía actual.
        
            