
                      Adiós Christopher Robin tiene un halo de tristeza melancólica que le da mucha belleza, tanto por la historia como la forma en que está rodada y narrada. Downhall Gleeson y Margot Robbie le dan mucha verdad, y el retrato va tomando forma e interés. La pega es que la situación que se plantea, máxime como biopic, podría haber dado más de sí en cuanto a la emoción, da la sensación de que se queda un poco aséptica y que lo que se cuenta es más triste que del modo que te lo desarrollan.
        
            