
                      Esta película tiene la garantía de hacernos pasar un buen rato, entretenimiento y acción, de la mano de dos maestros del género como son Gibson y Richet, sin los cuales, en mi opinión, este proyecto hubiese tenido dificultades para sostenerse, ya que el guión y el argumento no son muy innovadores.
        
            