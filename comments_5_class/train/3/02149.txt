
                      Francamente decepcionante, el trailer promete una película emocionante y con acción, nada parecido a lo que ocurre en realidad. Tiene dos picos de emoción, el más importante la batalla final, el resto del la película se pierde en un enlentecimiento desesperante intentando profundizar en la relación con Gwen, pero que se queda en superficial e irrelevante (te quiero, no te quiero, te quiero, no te quiero...).

Pero desde luego los personajes más desaprovechados son los villanos. 