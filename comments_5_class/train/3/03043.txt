
                      Otro domingo de frío y lluvia, otra sala medio vacía. En cuanto a la película, destacar el trabajo de los actores, la fotografía. Una película entretenida, pero que no llega a la categoría de gran película, pese a estar bien realizada. En resumen le falta algo de garra.
        
            