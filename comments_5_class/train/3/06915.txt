
                      He tenido la oportunidad de verla antes de su estreno este viernes 16 y para mi es la mejor de toda la saga. El personaje de Bella ha cambiado (y no sólo porque ya es una vampira): ella es más madura y más autónoma, muy lejos de esa niña tímida de 'Crepúsculo'. La historia es más agil y el desenlace, sin duda, una sorpresa para los espectadores. Un final que supera a las anteriores entregas de la saga y con el que los fans podrán sentirse muy satisfechos.
        
            