
                      No es una mala pelicula. Se sigue más o menos bien, entretiene, pasan cosas, te mantiene dentro de la historia y sufre los agujeros habituales, pero lo mejor, sin paliativos, es ver el cine medio lleno sin que hubiera ni un solo adolescente: gente madura que va al cine. Lo constato: existen. Están ahí. En alguna parte. Y ficciones como la de Anonymous los hacen salir. Así que... ¡
Bravo!
        
            