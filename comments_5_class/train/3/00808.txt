
                      Crítica de “Election: La noche de las bestias”

Secuela de la segunda parte de “La Purga” que continua la historia del protagonista Leo Barnes en la que tiene que proteger a una candidata a la presidencia que quiere acabar con la purga y que casualidad que ese año cambian las normas y los políticos pueden ser atacados. La película siempre llama la atención por las máscaras y la bestialidad que contiene pero en esta entrega se concentran en dos o tres escenas el momento máscaras y luego parece que sea otra película en plan salvar al presidente de los USA a parte cuándo se meten en un lío y parece que no vayan a salir “PUM” resulta que uno de ellos conoce a los que les está atacando y se libran y eso en las demás no pasaba. La ambientación, las máscaras, los momentos de acción, siguen siendo buenos pero la historia más floja, yo os la recomiendo si sois fans de la saga y tenéis curiosidad, sino no hace falta que la veáis.
        
            