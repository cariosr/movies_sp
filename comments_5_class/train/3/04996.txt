
                      Quedémonos con el entretenimiento propiciado por tan bien coordinadas y locas escenas de acción, y olvidemos la incoherencia argumental que las rodea. Esta octava entrega cae en manos de una trama frágil, diálogos estúpidos e innecesarios y Kurt Russell -entre otros- riéndose de la inutilidad de su personaje. Perdonado queda F. Gary Gray, porque el espíritu de Vin Diesel sigue vivo y porque la franquicia, al fin y al cabo, funciona.
        
            