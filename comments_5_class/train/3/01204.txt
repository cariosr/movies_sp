
                      Hay que tener muy claro que esperas obtener cuando decidas ir a ver esta película
. Si esperas ver un conjunto de coreografías, alguna de ellas bastante chula, y nada más, vas por buen camino. La trama, los personajes, todo está ya visto y es esperado, tanto que te entra la risa. La actriz principal en mi opinión, que se dedique a otra cosa porque más mala creo que es imposible.
        
            