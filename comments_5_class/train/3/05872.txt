
                      Nerve (2016).
Los realizadores de Actividad Paranormal 3 y 4 nos traen este thriller juvenil (en principio) mezcla entre Open Windows y Saw en versión light donde el peligro de las redes sociales marca la pauta de la historia. 
Rodada con cierta originalidad a base de mucho móvil, ordenadores, cámaras subjetivas y tabletas, la acción y las pruebas que nuestros protagonistas (la sobrina de Julia Roberts y el hermano de James Franco) tienen que llevar a cabo hacen que nos mantengamos atentos a la pantalla la hora y media de metraje de la cinta.

Entretenida, y mejor de lo que esperaba aunque sin ser la película del año. El desenlace no mejora la historia, aunque ésta va in crescendo conforme avanzan las pruebas.

Raúl Cabral.
        
            