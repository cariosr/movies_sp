
                      La crítica en general nunca ha sido muy justa con la saga del asgardiano. Como toda película tiene sus aciertos y sus errores y como me gusta ver siempre el lado bueno de las cosas empezemos por las cosas buenas: 
-LA MEJORA DE LOS EFECTOS-
En cuanto a efectos especiales la primera película no era una maravilla comparándola con otros trabajos semejantes de la factoría Marvel (Ironman 2, Capitán América: el primer vengador). En cambio en su secuela se puede ver perfectamente la mejoría en cuanto al paisaje de la ficticia Asgard como en las escenas transcurridas en Londres.

-"DURACIÓN" DEL FILME-
Entrecomillo la palabra duración debido a que la diferencia entre la primera película y la secuela solo es de 2 minutos, y porque en este caso la secuela es 2 minutos más larga pero te parece todo lo contrario. Debido a que en la primera parte tienen que ponerte en situación, dar a conocer a los personajes, sus personalidades y la "guerra" que existe entre unos y otros (en este caso Thor y Loki) la historia (por mucho que te guste si no eres seguidor asiduo de este tipo de filmes) te parecerá interminable llegado a un momento, pero en "El Mundo Oscuro" no ocurre. Su argumento tiene introducción, nudo y desenlace (este último como en todas las películas de Marvel) abierto.

Y los contras...:

-ARGUMENTO ENREVESADO-

El argumento de "Thor I" era simple, como el de todas las primeras películas de una saga pero es muy dificil conseguir que una secuela sea mejor. Para que una secuela triunfe deben hacer que el filme presente un soplo de aire fresco en el argumento, con una nueva aventura que no sea una trillada copia de la anterior pero si que de alguna forma tenga pequeños parecidos para que el público no se pierda. 
En el apartado de los PROS incluía la duración pero el argumento es totalmente nuevo. Si quieres enlazar el argumento de Thor 2 con otra película de Marvel sería con "Los Vengadores" por el simple hecho de que 