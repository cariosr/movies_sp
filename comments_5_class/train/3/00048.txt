
                      Hay factores que podrían haber funcionado mejor de lo que han hecho. Una historia original, además de basada en sucesos reales, es un gran comienzo para este thriller, además, el tema que trata no ha sido muy utilizado en el cine y a causa de eso podría ser un punto atractivo para el espectador. Pero en contraposición hay puntos demasiado flojos en la película que la hacen deslucir muchísimo. A pesar de los giros argumentales, Amenábar no ha dado con el punto y la película resulta en muchos momentos previsible.
        
            