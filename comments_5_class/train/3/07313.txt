
                      Babadook (2014).
Producción australiana que se me antoja mezcla de "Los Otros" con "La Morada del Miedo". Sobria en el uso de la cámara y la fotografía, y quizá correcta en su banda sonora, resulta algo infantil en ciertos aspectos (el malvado villano, por ejemplo). Entretenida, aunque no consigue satisfacer del todo su desenlace.

Raúl Cabral. 
http://www.filmaffinity.com/es/film287094.html
        
            