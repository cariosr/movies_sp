
                      Visualmente atractiva, y con elementos que la convierten en un entretenimiento asegurado para la familia. Pero eso es todo lo positivo que puedo rescatar, ya que de ninguna manera se compara con la primera entrega. Perdió frescura, originalidad, la mayor parte de los diálogos son deliberadamente infantiles y por supuesto, volvieron a abusar del humor en una historia que, necesita desesperadamente de una cuota de drama, al menos en momentos cruciales para aportar algo de seriedad.
        
            