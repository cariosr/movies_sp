
                      Domingo por la tarde, llovía y hacia frío, era la primera sesión, y en la sala el 90 % eran niños. En la pantalla un largometraje de animación, bastante bien realizado, entretenido, ameno, y con muchos guiños a otros Filmes, de diferentes géneros. Como base la paranoia sobre la posible invasión de seres de otros planetas, en los Estados Unidos de los años 50, y como mensaje final, un alegato contra el racismo o  (contra al temor a las culturas desconocidas). En resumen, cine familiar que gustara a niños y no tan niños.
        
            