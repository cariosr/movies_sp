
                      Quizás fuera por lo condicionado que fui al cine, pero no me gustó tanto como me esperaba. Creo que la película tiene dos mitades, la primera es fantástica gracias al trabajo visual que solo Nolan sabe intercalar con la narración de la historia, aunque a partir de la segunda mitad la película empieza a flojear y pierde toda esa fuerza narrativa. Insisto en que fui muy condicionado al cine, muchos amigos y familiares la pintaban como una obra maestra, algo inigualable. Sin embargo, la sensación que me dio es la falta de profundidad de algunos personajes, la trama se va perdiendo a medida que pasa el filme y pronto cae en el problema de la excesiva duración.

Repito que quizá otro problema que deba añadir sería ir al cine pensando que lo que vas a ver es lo mejor de la carrera de Christopher Nolan. No nos engañemos, no es así.
        
            