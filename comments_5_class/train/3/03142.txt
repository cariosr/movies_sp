
                      Al fin he visto Animales Fantásticos y Dónde Encontrarlos y tan solo esperaba que me transportase de nuevo al mundo de Harry Potter, lo hizo pero no como yo esperaba.

Es una película entretenida, con muchas aventuras, un ritmo brutal, tiene su parte mágica adolescente y una parte oscura, como vimos en Harry Potter, que eso me gustó mucho a parte de los grandes actores que tiene la película. Pero esperaba mucho más, veo que quiere dar mucha información y muy rápido en una película, demasiado tonta e infantil para poder captar nuevo público, me ha faltado ver a Newt cazar a algún monstruo que no tuviese, porque solo caza a los que se le escapan y ya, eso si los efectos son espectaculares y los momentos de magia dignos de Harry Potter, en fin esperaremos para ver la segunda parte con su villano, por cierto, el final del villano no me lo esperaba, ha sido una gran sorpresa.

Que no os confundan mis palabras, me ha gustado pero me esperaba más, solo eso.
        
            