
                      "Quien emerge como el héroe real aquí es el director Wes Ball. El ex supervisor de efectos visuales demuestra ser un raro cineasta que puede forzar un vigor genuino en uno de esos éxitos de taquilla modernos y banales."
        
            