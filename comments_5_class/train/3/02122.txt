
                      Las películas de verano gimen al comienzo con la respiración sibilante Wolverine, un intento transparente de exprimir a una franquicia vacilante por su última gota de jugo de taquilla. Debería funcionar durante al menos una semana, hasta que Star Trek se abra y lo saque del agua al mostrar cómo se debe hacer una historia de origen. Quien busque emociones peligrosas está fuera de suerte a menos que se arriesgue a ser procesado por la ley y descargue ilegalmente la película en la Web.
        
            