
                      Típico producto de acción que sube algunos enteros respecto a otras películas del mismo corte por el elenco interpretativo (Denzel Washington, ya ducho en estos trabajos, Pedro Pascal, siempre solvente sobre todo en el policíaco, y secundarios como Bill Pullman) y por la mano de Antoine Fuqua que es capaz de dar color a una película muy convencional con la forma de tratar la acción y algunos de los escenarios.
        
            