
                      Alpha, Crítica
La ilusión de hacer cine
Me atreví a verla y reconozco que podemos ver una película con muy buenas intenciones y mejores ilusiones. Una idea original y un desarrollo narrativo desigual. No puedo hacer una crítica negativa de Alpha porque el trabajo, en general, del equipo de Joan Culrina Perelló no lo merece. Pero tampoco puedo escribir líneas llenas elogio por el trabajo de los que forman la parte técnica y artística de Alpha no lo merecen.
        
            