
                      El cine de terror no es mi género favorito, pero reconozco que esta, sin mucho elemento extraordinario, al final resulta entretenida, porque consigue mantener la tensión. El director se excede un poco en la carga de artificio (23 personalidades en una, ¡Ni la Santísima Trinidad!). Aquí es donde la historia pierde crédito, imagino que para ganar artificio y para sorprender de esta manera al público. Me gustó más "La visita", anterior film del director, cuya cotización sube y baja como la espuma.
        
            