
                      “Francotirador, ¡cúbranse!”

Todo un clásico, eficaz, conciso, productivo e influyente; no pierde el tiempo con adornos innecesarios ni aclaraciones pertinentes -y necesarias-, sencillamente sitúa los personajes justos para dibujar y crear la trama, relaciones viscerales para que todo quede unido y el misterio de quién traiciona, la verdad de quién es culpable más, esa dolorosa honestidad de afrontar las consecuencias que dejan unas huellas, sin inocencia, que asumen ese esperado momento de confrontación directa, para saber de qué se está hecho y si se es capaz de asumirlo, sea cual sea la respuesta.
Una pertinaz caza al malvado, que no siempre es el buscado pues, diferentes clases de maldad dan lugar a asesinos de variado grado, incluso a aquel difuso que de forma retorcida se respeta ya que, funciona según honorables reglas de obedecer a su conciencia.
Es sencilla en su ejecución, rápida en sus movimientos, con un planteamiento holgado que abre cuestiones que ella misma olvida o, simplemente pasa por ellas con una mera mención y una minoritaria explicación válida.
Se busca a un coleccionista de muertos y en ello se embarca la policía para capturar al susodicho, cuando surge un competidor en acumular cadáveres, de método mucho más personal y sádico, y se asoma el dilema moral de quién tiene preferencia en caso de que haya que elegir a cuál se persigue y atrapa.
Duda y debate que no llegan lejos, al igual que muchos otros preceptos morales que se dejan caer pero en los que no se profundiza, pues es mayor su afán y obsesión por correr y derribar piezas que en pararse, regodearse, presentar a éstas como cabe y comunicar la acción con esa contundencia de contenido que la ensalza y magnífica; en su lugar, coloca tibiamente los participantes y les confiere ese mínimo para que cumplan con su papel otorgado, sin pretender llegar más lejos que cumplir con el objetivo marcado.
Y ello lo hace, su misión es realizada con éxito, con satisfacción de entretenimiento adecuado, donde se intuye su minoría de edad al no querer crecer en su libreto y provocar esa rica sugestión de atrape hipnótico.
Aunque no hay desprecio ni nada que echar en cara, es pasatiempo eficaz para las pretensiones que marca, concisa en su objetivo de ir derribando peonzas hasta encarar la decisiva, productiva en su distracción media de tiempo ligero e, influyente en su recuerdo de cintas similares que discurrieron por mismo camino, pero con más arte en sus ingredientes; un menú diario con lo necesario para alimentar el estómago y seguir trabajando.
Siempre es un placer encontrarse en la pantalla con Daniel Auteuil, aquí bajo la dirección de un italiano, Michele Placido, en una producción de triple colaboración entre Francia, Italia y Bélgica; puede que por ello se aborden demasiados puntos sin disparar al blanco en ninguno de ellos pero, aún con su tenue intención de compás en su manuscrito aprueba, se disfruta y complace lo que dura.
Liviana y versátil, de cuerpo justiciero y fondo ético, por los que transita con presura y diligencia que ¡inmediatamente viene otro muerto! y hay que hacer sitio en la cinta; visión rauda, de expeditivo efecto y ¡a otra cosa!
“Como un lobo voy detrás de ti, paso a paso, tu huella (de poco impacto e intensidad) he de seguir”

Lo mejor; su intención de andadura.
Lo peor; insinúa pero no confirma.
Nota 5,2
        
            