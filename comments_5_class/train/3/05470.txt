
                      Cuando la novela histórica viaja a la pantalla grande quizás hay que ser un poco demasiado intelectual para pasar un buen rato en el cine, porque la factura técnica de esta cinta es impecable pero me dio la sensación de que me perdía algo y me aburrí un poco, aunque en general el argumento es interesante. 
El tema tan controvertido de las herencias está bien reflejado, como el sentimiento de la avaricia, que se apodera de cualquiera llegado este punto, por muy buena persona que parezca.
Los enredos matrimoniales y el misticismo de los protagonistas están interpretados genialmente por los ya nominados a los Oscar y a los Globos de Oro: Helen Mirren en la categoría de actriz principal y Christopher Plummer en la categoría de actor secundario. Sin duda sus actuaciones merecían este reconocimiento, pues son lo mejor de la película.
        
            