
                      Sucede a veces que una buena historia no se cuenta de la forma adecuada. En esta ocasión, las formas son excelentes; pero la historia no tiene para más. Serie de grandiosas escenas unidas por un absurdo nada retórico.
        
            