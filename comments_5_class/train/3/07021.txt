
                      Godzilla: Rey de los Monstruos (2019).
"La extinción ha comenzado y nosotros somos la causa."
Nueva vuelta de tuerca al célebre monstruo de la moderna “mitología" nipona; esta vez acompañado de un buen número de “colegas” de lo más peligrosos (Mothra, Rodan, Ghidorah...). Todos creados en los años 50 y 60.

"He conocido de primera mano la naturaleza humana, y puedo decirte que no ha mejorado. Va a peor."
Aunque con mensaje ecológico y crítica al egoísmo humano, la historia, como es de esperar, no es más que la lucha del hombre contra la temida bestia... y la de ésta contra otros monstruos, haciendo valer la manida frase “el enemigo de mi enemigo es mi amigo”.

"Creo que deberíamos parar porque sigo queriendo tener hijos algún día... y a ser posible, sin aletas..."
En esta secuela de la película que se estrenó hace ahora un lustro, repiten papel solo el japonés Ken Watanabe (inolvidable 'Último Samurai' amigo del personaje de Cruise) y David Strathairn ("El Legado de Bourne”). Aparte, y a la cabeza del reparto, podemos ver a la magnífica Vera Farmiga, a Kyle Chandler (“First Man”) y a la protagonista de "Stranger Things”, Millie Bobby Brown, dando el salto a la gran pantalla.

"A veces la única manera de curar nuestras heridas es hacer las paces con nuestros demonios."
Lo mejor de esta cinta rodada en varios países y con la colaboración de diversas compañías de efectos especiales (ILM entre otras), creo que es el diseño de producción y los imaginativos 'supergadges' y armas que luce. 
Lo peor, esa fotografía oscura y lluviosa que, no solo cansa, sino que apenas deja ver los detalles de los protagonistas: los monstruos.

Tendremos más Godzilla en 2020, y midiéndose en esta ocasión con otro coloso conocido; el primate por excelencia...

Raúl Cabral.
https://www.filmaffinity.com/es/film133244.html
        
            