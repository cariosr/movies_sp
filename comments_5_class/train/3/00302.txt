
                      Entretenido largometraje, que reúne a dos estrellas en alza del momento, la todoterreno Agelina Jolie, y Johnny Depp más moderado y comedido que en sus anteriores trabajos.

Con una trama que recuerda en parte a los trabajos de Alfred Hitchcock, una estupenda banda sonora, una agradable fotografía, con algún giro del guión sorprendente y algún que otro giro ya esperado.

Quizás se eche de menos algo más de acción, pues el prologo se hace un poco largo y lento.

En resumen: entretiene, pero al final esperas algo más.
        
            