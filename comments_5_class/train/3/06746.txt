
                      La historia resulta bastante emotiva. En el límite de la sensiblería. Crea una atmósfera apropiada para desdramatizar la tragedia de los personajes.
        
            