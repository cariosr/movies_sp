
                      Desafortunadamente, ya todo estaba dicho en el tráiler de la película, y ver la película es simplemente una formalidad... Si es perfectamente martirizada desde el punto de vista visual, la intriga en sí misma y la psicología de los personajes des muy simplista y es una pena...
        
            