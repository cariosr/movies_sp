
                      La casa del reloj en la pared, si bien está catalogada como cine familiar (y de hecho la mayoría de su enfoque lo es), tiene ciertos aspectos que quizás no sean demasiado adecuados para l@s peques de la casa, quizás excesivamente gótica, excesivamente macabra por momentos. Para ser Eli Roth, está muy contenido, pero ya se sabe, "la cabra tira al monte". Destacar su ambientación gótica y la interpretación de Cate Blanchett, que siempre llena la pantalla. En definitiva, cine familiar pero no demasiado recomendable para l@s muy peques de la casa.
        
            