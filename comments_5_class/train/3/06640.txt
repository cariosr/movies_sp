
                      Película que nos recuerda a aquel astronauta de Interestelar que está solo y abandonado en otro planeta, pero no es así, no es nada parecido a Interestelar. Marte es una película para entretener, fantasear, emocionar y esperanzar, Scott ha hecho un gran trabajo a pesar de que hayan unos 20 minutos de bajón en la película, luego poco a poco se va recuperando la intensidad con la que empieza la película.
        
            