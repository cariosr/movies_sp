
                      Magia a la luz de la luna es una comedia romántica dirigida y escrita por Woody Allen, director de películas como 'Desmontando a Harry', 'La maldición del escorpión de jade' y 'Blue Jasmine'.

La película, ambientada a finales de los años 20, narra la historia de Stanley y Sophie. Stanley es experto en desenmascarar a falsos videntes, y un amigo suyo le informa de que una joven medium está trabajando para una familia rica.
Él irá hasta Francia para probar que es un fraude.

La pareja romántica que forman Stanley (Colin Firth) y Sophie (Emma Stone) no me acaba de convencer.
La diferencia de edad es más que evidente, pero además, la química que hay entre ellos es prácticamente nula.

Con respecto a la trama de la historia, no hay nada que objetar. Es original, entretenida y, divertida y emotiva a partes iguales. Podría haber sido una gran película con una pareja que desprendiera más feeling.
Por separado, los protagonistas brillan mucho más que juntos, y ambos realizan una actuación notable.

Los escenarios y el vestuario están muy bien conseguidos, haciendo que el espectador se meta en la época.
        
            