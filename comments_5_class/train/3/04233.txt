
                      “Spotlight” está basada en un caso real del Boston Globe, por el cual ganaron un premio Pulitzer al destapar una trama de abusos a menores en la Iglesia Católica, desde hace más de 30 años. Un premio que no solo les valió a los protagonistas, sino también al equipo de la película en la vida real, y es que estamos ante una de las cintas más nominadas y premiadas de este año en los Globo de Oro y en los Óscar.

Siguiendo la estela de películas sobre investigaciones periodistas como “Zodiac” o “Todos los hombres del presidente“, estamos ante dos horas no sólo de trama periodística, si no también de interpretaciones magistrales. Y es que a pesar de que sea una película densa, que en ocasiones de haga pesada y larga, el equipo artístico sabe compensarlo y ofrecernos a cambio una de las mejores interpretaciones que jamás hayamos visto. Un tema escalofriante, que revuelve a cualquiera y que es llevado de forma increible por unos actores perfectamente elegidos para sus personajes. Para bien, pero también para mal, la película ni tarda en entrar en acción, ni retrasa su desenlace, de modo que si la temática no te llama, esta no es tu película. Pero si las investigaciones periodísticas te gustan, esta sí que lo es.

Lo mejor: Stanley Tucci, genial como el abogado Mitchell Garabedian.
Lo peor: un score plano y sin sentimiento.
        
            