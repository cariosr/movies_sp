
                      Han pasado más de diez años desde que la primera película de "Johnny English" destrozó la taquilla internacional y fracasó en Estados Unidos. Pensarías que habrían hecho otro más rápido. Pero esperaron, y de alguna manera las películas de espías cambiaron desde el cambio de siglo a once años en.
En esta secuela, vemos el personaje titular de Johnny English secuestrado en una tierra lejana, que lidia con un fracaso para proteger a un líder mundial. Ha regresado al campo para descubrir un topo en la inteligencia británica y frustrar más asesinatos internacionales. Por supuesto, el inglés sigue siendo el espía torpe, jugando con los puntos fuertes de Rowan Atkinson como comediante físico.

Vi esta película en cines, y fue muy fría para mí. Al volver a visitarlo en Blu-ray, descubrí que me gustaba más, una vez que me adapté a la entrega y el sabor extremadamente británico. Todavía es obvio por qué esto fracasó con el público estadounidense, pero fue otro éxito mundial. Austin Powers, esto no es, y supongo que eso es lo que la mayoría de la gente habría estado esperando cuando lo vieron.

Al igual que su personaje de Mr. Bean, Rowan Atkinson se las arregla para ser tierno y abrasivo. Él tropieza con la película con facilidad experta. No es una comedia tan abierta como las películas de Mr. Bean o la serie "Black Adder" de Atkinson. Pero funciona, y funciona mejor para mí en mi hogar.

Puede que no me haya gustado en los cines, pero fue divertido verlo nuevamente en Blu-ray.

El Blu-ray viene incluido con un DVD y copia digital de la película. Las características de bonificación incluyen escenas borradas y ampliadas con las presentaciones del director Oliver Parker. También hay un mordaza y un comentario con el director y el guionista. Un featurette en la persecución en silla de ruedas completa las funciones de varios discos.

Las exclusivas de Blu-ray incluyen "The English Files", que son una serie de características: "The Making of Johnny English Reborn", "Working with Rowan", "Gadgets" e "English in Hong Kong". Finalmente, hay acceso a pocketBLU y BD-Live.
        
            