
                      Larga es la sombra del resplandor... Una casa, una familia, un psicópata.
Lo cierto es que me encantó la primera hora de la película. Pero el desenlace ha resultado ser insípido. Valviendo al más de lo mismo, típico y repetitivo final de las películas del género.
        
            