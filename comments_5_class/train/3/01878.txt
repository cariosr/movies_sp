
                      Divergente: Leal (2016). 
Los personajes de la anterior parte de una de las sagas distópicas de moda- junto a Los Juegos del Hambre- vuelven en esta costeada cinta dirigidos por el mismo realizador. 

Sin descubrir nada nuevo, y con un metraje algo mayor del deseable, el diseño de producción es lo más notable de la película, fruto, sin duda, de una imaginación desbordante... y de un gran presupuesto...

A los jóvenes protagonistas le acompañan conocidos secundarios como Naomi Wats, Jeff Daniels o el talentoso actor principal de Whiplash. 
Entretenida, especialmente para seguidores de la saga y para los amantes de los buenos efectos tanto visuales como de sonido. 

Raúl Cabral.
        
            