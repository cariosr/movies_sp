
                      Este filme parte western, noir y heist dirigido por tres directores difícilmente armonizables parece tener lo necesario, más no lo suficiente.
Ya desde 2016 el formidable canon de Star Wars se expandía más allá de los confines de la trilogía protagonizada por Rey y Luke Skywalker. Volviendo atrás en el tiempo Gareth Edwards se adentraba en una naciente Alianza Rebelde que conseguiría llevar a buen puerto los planos de la Estrella de la Muerte; liderada por una insuperable Felicity Jones, “Rogue One: A Star Wars Story” seria la dama que inauguraría una larga ronda de spin-offs/precuelas que prometerían desglosar el universo de un ya retirado George Lucas; por supuesto, Lucasfilm y Disney han tomado la decisión de abrirse a estas nuevas historias siempre y cuando estén enfocadas en un digno acontecimiento o benemérito personaje perteneciente a la línea argumental principal, encontrando en este renacimiento cinematográfico la mina de oro perfecta tanto para producirlas, distribuirlas y promocionarlas como para llevarse al bolsillo una jugosa tajada de ganancias ¿Negocio corporativo o incontenible amor creativo? Ya lo hemos descubierto.
Desde sus primeras señales de vida, se ha especulado bastante sobre la producción “maldita” que ha tenido que soportar la más nueva apuesta de Star Wars estelarizada por Alden Ehrenreich, que aunque si maltrató contundentemente el producto final por el impactante y súbito reemplazo de realizadores, por los incomprobables rumores alrededor de las dudas interpretativas de la joven estrella principal, unos ligeros y notables reveces en el guion y una lacerante proximidad en las fechas de estreno tanto con el polémico filme de Rian Johnson como con el del par de hits de Marvel Studios; quien sea que debía responsabilizarse del difícil trabajo de finiquitar e intentar perfilar lo mejor posible el material grabado por el dúo de directores Phil Lord y Christopher Miller— acreditados demencialmente como productores ejecutivos, —tendría que aguantar un enorme peso sobre los hombros, posiblemente uno de los más grandes que ha podido existir en el último lustro con respecto a un accidentado mega-proyecto. El oscarizado Ron Howard se ajustó los pantalones y dio un  paso adelante, aceptó coger las riendas dando todo de sí para congeniar el estilo más improvisatorio y humorístico de los anteriores directores con sus inclinaciones más dramáticas e independientes; por supuesto que ha conseguido un filme digno de ver, pero suficientes obstáculos hay en el camino para afirmar que esta no debía ser la película oficial que nos hablara del ya clásico personaje interpretado por Harrison Ford.
El guion que corre a cargo del veterano en la saga Lawrence Kasdan y su hijo Jonathan Kasdan, siempre basados en los personajes de Lucas, prefiere eludir la típica historia de orígenes, es decir, los escritores se decantan por iniciar desde un punto medio de la vida del titular, lo que subraya que no se nos está contando su vida sino más bien la historia de cómo se convirtió en un icono de la cultura popular, una trama fabricada para el personaje que no cala del todo bien. El peso argumental que se le da al personaje no está bien enfocado partiendo de un lugar predecible, se refunde entre sus posibles direcciones y solamente consigue salir triunfante en un par set-pieces y emocionales escenas dramáticas que son contadas con los dedos de una mano; no se ahonda mucho más allá de lo que ya sabemos y el hecho de introducir nuevos personajes no hace más que hacernos concluir que esta no es la historia del gran Han Solo. Las ocurrencias del guion no son los suficientemente sinvergüenzas y divertidas viniendo de Han, en realidad, quienes se roban dichos momentos son, en última instancia, Lando Calrissian, Qi'ra y L3-37, ni siquiera el Beckett del siempre fantástico Harrelson ni el Dryden Vos de Bettany son fichas que se mueven interesantemente por medio de este puzzle en varias ocasiones descafeinado. No está ese ambiente socarrón y atrayente del Solo de Ford en gran parte por un guion ciertamente ajeno a la nostalgia de los 90 y una interpretación poco carismática. Los filmes de Star Wars siempre han intercalado sus escenas de acción, desarrollo, relleno, drama y comedia con una exquisita pericia, y aunque esta no es la excepción, muchas de las susodichas secuencias híper-onerosas no agregan ni significan algo sustancial al avance de la  trama, son simple y predecible entretención que se cierra a nuevos caminos, son el típico canal para satisfacer a los espectadores menos relacionados con este universo de creaturas y batallas. Además de unos cameos imperdibles a lo largo de la cinta, esta historia de crimen espacial encaja sin negación en el canon ya que no agrega ni quita absolutamente nada a la franquicia, funciona bien como película independiente pues se mantiene en pie por sí sola, algo que el trio de directores terminaron por edificar muy bien. Hay varias escenas que debieron ser prescindidas para aligerar la carga argumental y la duración del filme, pero por supuesto que un filme de la saga jamás estará por debajo del mínimo de las dos horas, no obstante, en los filmes de Edwards o Abrams el tiempo se pasaba volando porque estabas conectado con lo que te mostraban en pantalla, acto que no ocurre aquí y que convierte la experiencia en continuas y estresantes vistas a tu reloj. Nadie pensó que un personaje tan importante y emblemático dentro de Star Wars recibiera este tipo de tratamiento y resultado en su película en solitario. Es difícil y decepcionante afirmar que de la nueva oleada de filmes provenientes de Disney, este es el menos satisfactorio y entretenido, pero hay buenas noticias, hay un pago digno por el que esperar. Sin duda, el primer, segundo y parte del tercer acto fueron formalmente planos, poco interesantes, faltos de la chispa y gracejo que uno esperaría en una gran idea como esta, sin embargo, los últimos quince minutos son la maravilla, es por lo que pagaste tu boleto, son, personalmente, lo que debió ser toda la película. La perspectiva de espejos entre las narrativas finales, el necesario factor sorpresa, ese aire engañoso y malicioso, imágenes poderosas, interpretaciones destellantes, un soundtrack apabullante y un verdadero Han Solo componen un cierre lleno de personalidad y actitud. Un “Focus” espacial, el juego de mentiras y realidades que ponen al espectador en un punto de indecisión y el auténtico ambiente timador y seductor regresan la esperanza a tu corazón y te hacen ver las cosas desde otro punto de vista, uno que duele menos, pero que arde igual.
Alden Ehrenreich no es el Han Solo que esperábamos. Es verdad que el actor de “Hail, Caesar!” no tiene mucho de done aferrarse a la hora de “crear” su personaje, en sí, en lugar de meterse bajo su piel parece ejecutar un bien intencionado pero fallido homenaje al atrayente chantajista y enredador de Harrison Ford, una representación agradable sobre un molde del personaje, aun cuando este lo supera en tamaño. No hay intriga, no hay chispa ni tampoco interés en ver qué le puede suceder a su personaje, el cual, obviamente, no sufrirá tanto como sus acompañantes, sin embargo, uno finalmente termina aceptando su pícara sonrisa y su limitado carisma. En el otro lado de la moneda,  Donald Glover lo borda sin precedentes como el tramposo apostador Lando Calrissian, fascinante la personalidad y el feeling que le brinda el rapero, actor, escritor y un largo etcétera a otra de las grandes personalidades de la saga; a diferencia de Ehrenreich, su interpretación no es un tributo o una adaptación personal sino es el mismísimo personaje, lo vemos en sus actos, en la confidencia de sus ojos y en su glamurosa actitud, no por nada ya se estaría cocinando la idea del primer filme de Star Wars protagonizado por un hombre de color, simplemente genial. El Chewbacca  de Joonas Suotamo  tampoco demuestra un desarrollo meritorio puesto que, aunque medio instaura el surgimiento de la inquebrantable amistad junto a Han y lanza uno que otro vistazo a algunos de su especie, sus actuaciones guturales no terminan ni siendo una fuente de comicidad ni tampoco de nostalgia, es un Chewie apartado; sí, en ese entonces es zagal y ha tenido que soportar una dura esclavitud por mucho tiempo— considerado como una bestia, —pero nunca se le prestan las herramientas para que brille como debería hacerlo. Todo parece apuntar a que de todo el compendio de personajes ya conocidos, el que más nostalgia y alegría levanta es el Millennium Falcon, es emocionante “rencontrarse” con la nave desde 2015 y conocer tan solo una de sus anécdotas. Aparte de los personajes ya pertenecientes a la saga de antaño, Emilia Clarke y sus tiernos hoyuelos son una poderosa adición en la serie. Qi'ra, su personaje, experimenta une súbita trasformación que la aleja de los tópicos y la posiciona en el mismo nivel de convencimiento actoral de Felicity Jones y su Jyn Erso, Lauren Dern como Vice Admiral Holdo o Rey de Daisy Ridley. Su actuación está llena de fuerza, ella es inteligente y humana,  perspicaz y estratégica, sabe que decir y cuando hacerlo con una talento envidiable y poco habitual, es capaz y grandiosa. Paul Bettany y Woody Harrelson, dos monumentales intérpretes, no pueden explotar sus habilidades a cabalidad debido a que el bosquejo y el desarrollo de sus personajes a uno no le da la suficiente profundidad y tiempo en pantalla y al otro le otorga un tratamiento vacilante e irregular; una pena y un gran desperdicio, claramente entregan interpretaciones aplaudibles pero no traspasan ese límite, prefiero recordarlos con Vision de Bettany para Marvel y el Willoughby de Harrelson para Martin McDonagh en “Three Billboards Outside Ebbing, Missouri.” Especial mención y felicitación dirigida a Phoebe Waller-Bridge por L3-37 y Erin Kellyman por Enfys Nest, quienes además de protagonizar dos de las escenas más emotivas y legendarias del filme, dan actuaciones cortas pero enérgicas e interesantes.
Exclusivos son los que tienen el poder y la experiencia para juzgar la fotografía de un avezado y no por nada nominado al Oscar Bradford Young. Es verdad que no hay muchos colores vivos en los cuadros y en ocasiones puede resultar farragoso tratar de entender qué es lo que esta pasando en pantalla debido a la paleta de colores opacos, ocres y grises, pero Young intenta incursionar en nuevos campos con nuevas técnicas para así dar una personalidad definida a una historia que narrativamente se queda corta o, más bien, enredada; según dicen, los problemas con la proyección de su trabajo se le atribuyen a la mala calibración de los proyectores, no aptos o incapaces de mostrar en pleno esplendor la iluminación exigida por el filme. Tal como sucede con el guion, las secuencias de acción y, en general, las de desarrollo del primer, segundo y gran parte del tercer acto, las imágenes no son tan agresivamente magnéticas y maravillantes como las de “The Last Jedi” o “Rogue One,” no obstante, en los últimos minutos disparan una tras otra escenas para el recuerdo, ángulos específicamente inteligentes, tonalidades aun oscuras pero contrastadas con una luz perfecta y movimientos de cámara capaces de reavivar la esperanza demuestran nuevamente lo mucho que esperábamos de la película. A pesar de esto, vale la pena deleitarse e intentar dar con el propósito del cinematógrafo de “Arrival” con su estreno más comercial. Por lo personal, mil gracias por una toma insertada a fuego en mi memoria por su hiriente belleza, protagonizada por Clarke, mirando a través de la verja del dolor e iluminada en los ojos por un amor que resplandece desde la lejanía. Tal cual ocurre con el score compuesto y adaptado por John Powell, quien deja que desear tratando de alcanzar el potencial sonoro de las situaciones, únicamente evidenciando en los últimos momentos composiciones estruendosamente sensoriales. Esta es la primera vez que el editor ganador de dos Premios de la Academia Pietro Scalia trabaja para Star Wars y aunque entiende bien como jugar y dominar las emociones del espectador con cintas como “Alien: Covenant” o “The Martian” de Ridley Scott, el metraje final, el accidentado metraje final carece de viveza, simplemente desaparece la chispa y la emoción incluso en las secuencias aparentemente más entretenidas.
“Solo: A Star Wars Story” dirigida por Ron Howard— y todos sabemos que por Phil Lord y Christopher Miller también —es la entrada más lábil, escabrosa y polémica en todos los campos de trabajo dentro del canon cinematográfico de Star Wars. Adolece de energía y dirección, adolece de una interpretación digna del papel titular, adolece de magnificencia visual y sonora, un tono adecuado y personalizado que diera a este personaje más que una sencilla historia heist. Las mejores fantasías y deseos de los Warsies se condensan en unos quince minutos que, aunque no consiguen levantar el peso muerto de más de dos horas de este irregular western espacial, inyectan esperanza para no dar por perdido el gran potencial de los siempre controversiales spin-offs de la franquicia de ciencia ficción por la que la ciencia ficción actual se conoce.
        
            