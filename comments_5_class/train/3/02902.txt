
                      Es una adaptación aceptable de una historia real. 
Mantiene un mensaje de lealtad muy bonito en todo el trayecto de la película acompañado de personajes simpáticos. 
A pesar de que todo el filme es sobre un canino, te entretienes viendo los sucesos que a este le ocurre, es como ver un "War Horse" pero con un perro (Por su gran mensaje de amistad). 
Para resumir, la trama es conmovedora pero no pasa a la cursileria. Maneja de de forma aceptables las emociones intensas que vive la mascota por semejante perdida. 
3/5
        
            