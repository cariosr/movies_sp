
                      La pareja del año es una comedia escrita y producida por Billy Crystal y dirigida por Joe Roth, director de La sonrisa de la Mona Lisa y Mientras dormías.

Eddie (John Cusack) y Gwen (Catherine Zeta-Jones) son dos estrellas de Hollywood, que además de compartir profesión, comparten su vida privada. Todo el mundo les adora, por eso, en la presentación de su última película juntos, tendrán que mantener las apariencias y evitar que todos se enteren de su reciente separación.

Julia Roberts, radiante como siempre, realiza el papel de la hermana de Gwen, que a la vez es su asistente personal. Enamorada desde hace tiempo del marido de su hermana, ella se queda atrás, debido a su falta de autoestima.
Se presenta como la antagonista de Catherine Zeta-Jones.

Los diálogos son ingeniosos, y nada faltos de humor. La película presenta una versión light, pero verdadera de Hollywood.
En resumen, es una divertida comedia, con un reparto de actores estupendo, pero con un guión un tanto escaso de contenido.
        
            