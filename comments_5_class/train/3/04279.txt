
                      Amor por la comedia española

Existe en este nuevo asalto del ex poli más casposo y políticamente incorrecto del cine español un chiste ya antológico de la franquicia que supone el inesperado punto de inflexión entre las viejas constantes de la misma y una puerta abierta hacia una comedia algo más sutil en la que esperemos reincida en el futuro. Nunca imaginó un servidor que el gag de las pajillas pudiera dar más de sí, aunque sea con una técnica sado masturbatoria con bolsa de por medio, pero Santiago Segura ha logrado darle una vuelta de tuerca necesaria para que sepa a nuevo, sin por ello renunciar a lo de siempre. 

“Torrente 5: Operación Eurovegas” es, sorprendentemente, la entrega más sesuda de toda la saga desde aquella Misión en Marbella, con la que guarda no pocas similitudes en lo que a concepción de humor se refiere. Si la cuarta recuperaba la esencia del primer y aún imbatible Torrente, esta quinta se asienta como un puente entre ésta, la segunda y lo que puede estar por venir. 

Aunque le cueste algo despegar y su primera mitad recoja todos esos puntos débiles que son ya marca de la casa, encabezados principalmente por una sucesión de gags soeces inconexos que gustarán en mayor o menor medida en función de lo que cada cual entienda por humor, es cuando comienza la preparación del atraco, y el atraco en sí mismo, cuando el conjunto remonta el vuelo y Segura tira hacia la comedia española coral de situación más en la línea de “Atraco a las tres” que de “La cuadrilla de los once” –u “Ocean’s Eleven, como prefieran las nuevas generaciones-. 

Se vale para ello del humor de siempre, pero bien dirigido hacia un bien común, el de contar la historia de unos desechos sociales que luchan por la supervivencia en la España del futuro, ésa que, con su vuelta a la peseta e independizada de Cataluña, no deja de ser menos estrafalaria y rocambolesca que la que vivimos hoy en día. Unos desechos que suponen otro de los puntos fuertes de la cinta, compuesto tanto por cómicos profesionales –ahí están Flo, Areces, Barragán o Esteso para sentar cátedra-, como por los recién llegados –bienvenidos sean Jesulín y Angy, perfectamente amoldados a sus roles-, pasando por caras habituales de la franquicia –Neus Asensi, Chus Lampreave, Cañita Brava- a los que Segura ha tenido el acierto de volver a reunir, por los viejos tiempos. Y con un invitado de lujo, Alec Baldwin, que se une a la fiesta sin prejuicios ni miedo al qué dirán.

Sin embargo, si algo desprende este nuevo Torrente, más allá de esa mirada ácida y despreocupada por ese gran chiste que es España, es amor y respeto por la comedia española de toda la vida, la de Ozores, Forqué y compañía, y por esos grandes cómicos que nos han acompañado a lo largo de nuestras vidas. Porque no hay mejor homenaje que reunir en una misma secuencia a Pajares y Esteso, o a Chiquito y Leo Harlem, y recuperar así un trocito de nuestra historia. De historia de nuestro cine, de nuestra comedia, y de nuestra historia cultural en general.

A favor: la segunda mitad, pura comedia española
En contra: la primera mitad, con el manido humor de siempre
        
            