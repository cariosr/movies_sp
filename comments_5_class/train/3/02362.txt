
                      Si estas en medio de un conflicto, por mucho que lo trates de evitar formas parte de él y debes vivir con sus consecuencias. La dificultad se agrava, cuando encima eres niño, y observas que no se trata de un juego, sino de algo más serio.

Este largometraje es para gente amante de reflexionar después del visionado. Realmente en la sala, el domingo de su estreno aquí en España habíamos poca gente, ya de por sí la sala era pequeña.

Cine minoritario, que basa su fuerza en el guión y en las localizaciones, en este caso en los montes colombianos. Protagonizada por actores naturales.

Premiada en varios festivales, entre ellos el de San Sebastián.
        
            