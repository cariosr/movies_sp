
                      Soy un imbécil para las películas de Caper en las que los estafadores increíblemente inteligentes hacen cosas increíblemente peligrosas mientras se ven increíblemente hermosas. Podía sentir a Focus tratando de ser esa alcaparra. No estoy pidiendo un nirvana, como el notorio de Hitchcock o el ajetreo americano de David O. Russell, solo un sabor del escapismo sexy. Un sabor es todo lo que obtienes en Focus, pero lo harás hasta que aparezca toda la enchilada.

Will Smith lleva todo su encanto de Slick Willie al papel de Nicky Spurgeon, un estafador prácticamente desde el vientre. Nicky y su tripulación pueden robarle el dinero a tus amigos. Su truco es desviar tu atención para que no sepas lo que te golpeó. Naturalmente, Nicky nunca deja que nadie se acerque. Luego Jess Barrett (Margot Robbie) entra en escena. Esta bomba es una novata en el juego hasta que Nicky le da su clase magistral.
        
            