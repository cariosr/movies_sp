
                      Partiendo de la base que desconocía que se trataba de una remake, puedo decirles que me pareció un rejunte de varias películas de acción con los clichés típicos del género. Un protagonista que ve morir a su hermano en una pelea clandestina en Tailandia y entrena duramente con un maestro del lugar (Van Damme) para vengar su muerte. Destaco las peleas y el dinamismo que ofrece de principio a fin, en definitiva es lo que uno quiere ver en este tipo de films. Logra su cometido.
        
            