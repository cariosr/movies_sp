
                      Pitt, en un papel en el que no estamos acostumbrados a ver, sencillo y que por supuesto, no explota ni mucho menos la capacidad interpretativa del actor.
La película es bastante entretenida, cumple con el deber de hacer pasar un buen rato. Es una película de "zombies" con un presupuesto bastante alto o al menos eso parece por sus efectos visuales, reparto y atrezzo. De "zombies" pero fuera de lo común en ese género y con una trama simple pero bien desarrollada. Recuerda a 28 días después con un acabado digital aún más elaborado y con bastante menos sangre, menos gore, por decirlo de alguna manera.
        
            