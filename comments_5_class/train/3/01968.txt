
                      Para empezar diré que yo soy bastante escéptico en estos temas de dioses y diablos, por lo tanto, me tomo este tipo de largometrajes como si viese uno de superhéroes, intento disfrutar del espectáculo a sabiendas de que lo que estoy viendo es imposible de creer. Dudo en que hechos reales está basado.

No abusa del efectismo, es pulcra en imágenes y diálogos, se deja ver y oír. No transmite ni frío ni calor y dudo que vaya a generar demasiadas pesadillas, tampoco pasará a la historia como una de las grandes en lo que a seres atormentados por las fuerzas del Mal se refiere. Quien busque el típico largometraje de terror con sobresaltos (que también tiene uno que otro) cada 2 minutos pues mejor que se ahorre la entrada. Le falta un toque de emoción al final.

Håfström sortea a la perfección la principal dificultad de este proyecto: que el tema central está trillado, y dio objeto a la magistral El exorcista, con la que las comparaciones iban a ser inevitables. Tiene a su favor la sólida interpretación de Anthony Hopkins.
        
            