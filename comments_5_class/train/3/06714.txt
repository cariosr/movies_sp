
                      La historia: Thomas (Dylan O'Brien) se despierta en el Glade, sin tener idea de quién es o cómo terminó allí. El Glade es una parcela de pastizales y bosques contenida en altos muros de concreto con una población residente de hombres jóvenes, todos los cuales llegaron uno por uno, con recuerdos borrados. A medida que Thomas se familiariza con Gladers, como Gally (Will Poulter), Newt (Thomas Brodie-Sangster) y Minho (Ki Hong Lee), descubre el laberinto que se extiende más allá de las murallas y el peligro que acecha allí.

Es algo terriblemente condescendiente de decir. Pero esta película, basada en una novela juvenil de gran éxito de ventas, funciona tan bien porque la historia se presenta sin los rasgos que los espectadores asocian con la ficción de adultos jóvenes.

Por un lado, es una pieza de conjunto. Sí, Thomas (O'Brien) es el héroe aquí, pero ese hecho no le roba a los otros personajes de orígenes interesantes, o, como mucho, un trasfondo que es posible dado que todos aquí son amnésicos. En muchas otras obras para adultos jóvenes, está el protagonista de la película, con personajes de apoyo como accesorios de un solo rasgo: el enigmático dreamboat, el matón antagonista, el audaz amigo, el amigo débil marcado para la muerte en el tercer acto ( ver Divergente de este año y otros).

Aquí, el elenco también se siente propiamente multirracial y multicultural, de acuerdo con lo que debería ser un mundo futuro globalizado. En otras distopías de adultos jóvenes, todos miran y hablan como si acabaran de salir de una escuela secundaria de Los Ángeles en un vecindario agradable.

La estructura de la historia aquí está hecha a medida para la exposición, una bonificación para aquellos que no han leído la serie de libros. Como Thomas es nuevo en Glade y no tiene recuerdos, el público aprende cómo funciona la cultura al mismo tiempo que él, un regalo para el director novel Wes Ball, y uno que no desperdicia.

Poner a un director primerizo a cargo de una película de franquicia de gran presupuesto, trabajando con un elenco de actores desconocidos, suele ser un signo de un control estricto del estudio, pero, en este caso, esa estrategia ha valido la pena.
        
            