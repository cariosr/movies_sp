
                      Hay quienes dicen que hay dos formas de entender el cine, una es como arte y la otra como entretenimiento. Probablemente muchas de las películas tienen un poco de ambas cosas, sin embargo este Doctor Strange es directamente un producto de entretenimiento sin más, o mejor dicho, un producto de entretenimiento más. Pero... ¿Destaca entre los productos de entretenimiento? Pues con total honestidad y muy a mi pesar, que llegué a creer que está película sería muy diferente a otras de la factoria, lo dudo...

Al margen de la subjetividad del cine, creo que hay ciertos elementos que nadie pero nadie nadie puede negar, y es que el argumento, a parte de simple es bastante previsible, y por último, que el humor de esta película, lejos de ser inteligente, tiende al de la comedia de situación e incluso... infantil. 

Doctor Strange es, a grandes rasgos, un producto generalizado más de una factoria cada vez más grande y cada vez más millonaria. Que ningún lector del antiguo Doctor Extraño espere ningún tipo de adaptación y/o homenaje, no tiene nada que ver, la película simplemente es... otro rollo. 

¿Recomendable para pasar la tarde, apropiada para verla con los niños y con toda la familia? Si. Pero creo que hablo por muchos cuando digo que... se esperaba más, desde luego los tráiles daban a entender mucho más de lo que realmente hay.
        
            