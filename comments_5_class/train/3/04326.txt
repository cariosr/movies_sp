
                      Era domingo, hacia mucho frío, la sala era pequeña y no habíamos más de diez personas en la sala. Largometraje minimalista, de guión pausado y esplendida fotografía. Si no te gustan los largometrajes de acción, ni de efectos especiales, ni tiros, ni explosiones, ni escenas de sexo subidas de tono. Si eres de los que te llevas el cerebro cuando vas al cine y de los que se distraen viendo cantar un pájaro, viendo volar una mosca u observando como crece una planta. Amigo esta es tu película.
        
            