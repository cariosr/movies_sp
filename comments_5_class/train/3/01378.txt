
                      Desde el sofá de casa...

Película protagonizada por Reese Witherspoon que narra una caminata de casi 2000 km a pie.

Personalmente me ha decepcionado. Cuenta como realiza esa andadura con la intención de encontrarse a si misma y enterrar a un yo anterior.
Diferente, con unos paisajes espectaculares, que intercala visiones y vivencias de su vida mientras realiza su andadura, una película que me ha dejado un sabor de boca amargo. 

Me esperaba mas. Aunque si vas a verla armate de paciencia, y si eres de los que hacen el cambio no de Santiago lo mismo te sientes identificado
        
            