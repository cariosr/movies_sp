
                      Otra pelicula anti-nazi. Si bien ésta no proviene de Hollywood (ni tampoco de Spielberg) ya hemos visto lo mismo en varias ocasiones (Operación Valkirya por ejemplo). Aun así, cumple con su cometido. Jamie Dorman demuestra una personificación más que aceptable y se nota como está madurando en el cine película tras película. Cillian Murphy por su parte, siempre impecable.
El film tiene buenas escenas y otras no tan buenas. No logra generar una atmosfera que nos atrape, pero si la trama te gusta o te llama la atención, de seguro no querras salir del sillon hasta que termine.
        
            