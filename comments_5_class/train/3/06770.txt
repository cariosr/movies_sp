
                      Nightcrawler es una película original (como las que ya no hay) con una actuación notable y una trama excelente. Es una de las pocas películas que merece la pena verla por su originalidad y por la adicción que crea.
        
            