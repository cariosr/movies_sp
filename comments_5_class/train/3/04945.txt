
                      Si bien "El juez" es un film bastante entretenido, lo que más destaca es la enorme diferencia de nivel entre los protagonistas y la historia. Vincent D´Onofrio, Billy Bob Thornton, Vera Farmiga, Robert Duvall o, sobre todo, un Robert Downey Jr. inmenso están muy por encima de la trama y las situaciones, que a veces parecen demasiado rebuscadas o poco creíbles. Aún así es un film entretenido y digno de tener en cuenta por el trabajo del elenco principal.
        
            