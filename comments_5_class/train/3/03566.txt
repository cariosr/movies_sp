
                      LA PREVISIBILIDAD DE LA HISTORIA NO SALVA LA BELLEZA DE SUS IMÁGENES

​     No es bueno ir al cine con expectativas creadas. Yo no suelo hacerlo hasta el punto que no leo nada (ni las sinopsis) de las películas que voy a ver. Me gusta dejarme sorprender como se va sorprendiendo el guionista paso a paso cuando escribe la historia. En este caso no me dejaron. Fue tanta la promoción de la película que era difícil escapar a esa especie de histeria colectiva que provocó. Vamos, poco menos que si no la ibas a ver serías un bicho raro. Sin pruebas, creo que diré que mucha gente hizo el boca a boca sin haberla visto.
​
     Iba con la esperanza de que vería un antes y un después en el cine español. Creo que eso pretenden los productores. La belleza y lo impecable de sus imágenes está dos escalones por encima de las mejores producciones de nuestro moderno cine. Pero me aburrí. Y en ciertos ratos mucho. ¿Qué es lo que falló?. Para mí, el guion.
 
     La historia es demasiado simple. La sinopsis la puedes enunciar en una sola línea de texto y habrás contado la película entera. Esto no es un falló en sí. El error es que, sabiendo que eso jugaría en su contra, no pusieran medios para solucionarlo. Si el relato es tan simple, lo que hay que hacer es dotarlo de unas tramas secundarias tan potentes que te mantengan aferrado a la pantalla y a la historia que estás contando. Aquí no se da. El resultado es una película demasiado previsible. Desde el principio te imaginas el desarrollo de los acontecimientos y, encima, te los cuentan como tú te los has imaginado. No hay sorpresas ni puntos de giro sorprendentes, así que en varias partes me vi con la atención fuera de la pantalla.
 
     Por lo demás, impresionante las imágenes. Los efectos especiales brillantes. El monstruo es humano, con sus sentimientos y sus movimientos. Te lo crees desde el principio y eso no es tan fácil. Y los actores, todos rayan a una gran altura, hasta el niño Lewis MacDougall es una gran elección de casting. No era un papel cómodo el suyo y transmite sentimiento durante toda la película. Todo el elenco que se concentra a su alrededor es de un brillantez coral que hace que no destaque ninguno por encima pero que te sumergen en los personajes: Sogourney Weaver (la abuela), Felicity Jones (la madre), Toby Kebbell (el padre) y hasta el cameo de Geraldine Chaplin son espectaculares.
​
     La fotografía corre a cargo de Óscar Faura con el que J.A. Bayona realizó sus dos trabajos anteriores y que fue responsable de las calidad de las imágenes de una peli que me encantó: "Descifrando enigma" (2014) de Morten Tyldum que le pudo haber valido su primera nominación al Óscar. Y las geniales animaciones con acuarela con la que aprovechan a contarnos algún cuento están hechas en Barcelona por la empresa Headless.
 
     El guion es de Patrick Ness, autor de la novela en la que está basada le película. Creo que aquí puede residir el principal elemento que define la, a mi gusto, errónea adaptación. La literatura y el cine se nutren de la misma esencia (contar historias) pero cada una tiene su propia semiología. Su poca experiencia le llevó a construir la propia narración sin acomodarla a un lenguaje cinematográfico comercial y fluido. Por eso, esta película, tiene la cadencia del que lee un relato lento y parsimonioso. No hay grandes giros que atrapen al espectador. A mí, de verdad, que no me atrapó
 
     Después de lo genial de J.A. Bayona con “El Orfanato” (2007) y “Lo imposible” (2012) este parece más un producto hecho por encargo y se nota. Quizás no le dejaron decidir muchas cosas sobre el guion y se limitó a hacer lo mejor que pudo: una realización impecable.
 
     Para finalizar y hablando del boca a boca, todos me decían que no me olvidara los pañuelillos porque es sabida mi facilidad para echar lágrimas exageradamente fluido. Nada más lejos. Pensé que me iba a dejar los ojos en la sala de cine y simplemente deje caer “alguna que otra” lágrima provocada por algún diálogo sensiblero… pero yo, comparado con otras películas considero que en esta: no lloré.
​​
​​
Valoración: BUENA IMAGEN CON MAL GUION

Twitter: @luisalserrano
        
            