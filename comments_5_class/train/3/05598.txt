
                      Deadpool, muy bromista en buena parte de la película. No se entiende esa situación de pasar de ser un sicario y tipo atormentado por el rostro desfigurado, para el ponerse la mascara ponerse bromista y sarcástico. Su doble personalidad, la bipolaridad no se entienden donde nace. Escenas de acción muy buenas. Actuaciones que no destacan mucho. Pero mantienen el clásico universo marvel, que dicho sea de paso se está convirtiendo en una referencia frente a productos de la competencia de los comics llevados al cine. Entretiene.
        
            