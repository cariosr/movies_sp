
                      Voy a contar un par de Spoilers, por lo tanto quien no haya visto la película, que no lea lo que escribiré a continuación.

La película tiene sus cosas buenas obviamente, los efectos gráficos y visuales son magníficos... pero tiene demasiadas malas. No negaré haberme entretenido viéndola, pero la historia está llena de agujeros y prisas innecesarias, si la película hay que hacerla 20 minutos mas larga porque en esos 20 minutos explicas mas cosas de la historia, pues se alarga.

Agujero N°1: 
Mana Uno, la grandísima organización marina que se gasta millones y millones de dolares para investigar el mar. Han creado los primeros dispositivos que soportan la increíble presión que genera la profundidad del mar, LA FOSA DE LAS MARIANAS, la zona mas profunda del mundo, hemos de suponer que la presión que hay en ese lugar haría pedazos cualquier cosa que llegue ahí. Aún seguimos sin entender como pueden haber peces que soportan esa presión... Pues cómo vamos a entender que esos dispositivos soportan la presión marina pero no soportan que un calamar gigante los apretujen? Al menos que expliquen un poquito como crearon esos dispositivos. La jaulita de tiburones si bien rápido que explican de que la hicieron para aguantar golpes, porque seria raro ver que un Megalodon no la destroze de un mordisco sin que nos expliquen el porqué...

Agujero N°2: 
La escena en la que Suyin empieza a explicar a los demas que tipo de Tiburón es el que les atacó cuando bajaron por primera vez, a mi me sobra. Porque lo único que hace es crear miedo y suspense al Tiburón con frases tipo "El Megalodon no le temía a nada" 
"Podía partir en dos a una ballena de un mordisco". Lo veo completamente innecesaria, un Tiburón de 25 metros y a saber cuantas toneladas ya impone él solito con semejante mandíbula y tantos dientes.
Pero no explican nada de cómo es posible que un Megalodon haya sobrevivido millones de años en La fosa de las Marianas, cuando se suponía que se extinguieron. 
Se supone que son investigadores, pues que hagan su trabajo.

Hablaré un poquito de los personajes ya que tengo un par de cosas que decir.
La primera y la que quiero quitarme de encima, Jason Statham es lo único salvable de todo el elenco y todo por razones obvias.
A todos los demás personajes los noto demasiado vacíos, casi no sorprenden sus roles. El negro tiene que ser el que pone el humor y los japoneses ponen el drama familiar... Eso es demasiado típico en el cine. 
y la escena en la que el médico se sacrifica para salvar a la chica, me parece un poco extraña. De por si, como llegaron tan lejos del barco tras caer por culpa del segundo Megalodon? Todos estaban si acaso a un metro del barco y ellos dos casi a 4 metros o mas... Bueno en fin. 

En mi opinión, la película creo que no se hizo para ser un peliculón de gran historia y ser recordada como obra maestra, pero es entretenida, tal como recalco al principio, tiene sus cosas buenas. Pero creo que habria sido mejor dándoles mas crudeza a las muertes y dándole mas oscuridad a la película, un poco mas de sangre y que no sean tan breves las veces que el megalodon se lleva a alguien de un bocado... Es triste ver que la parte que mas sangre hay en la película es cuando Jonas raja todo el vientre del Megalodon y acuden los demás tiburones a devorar al gran tiburón atraídos por la sangre. 
Es una película de Tiburones, hay que darle mas sangre!!
        
            