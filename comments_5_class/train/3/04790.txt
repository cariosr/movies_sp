
                      Centrada en todos los avatares y frustraciones que debe transitar una pareja cuarentañera para poder concebir un hijo. Un drama muy bien contado que no aporta efectos ni personajes antagónicos. Solo es su problemática y todo el desgaste que va sufriendo la relación a causa de su imposibilidad y los diferentes tratamientos. Cuenta con un ritmo interesante, buenas actuaciones y un final abierto. Interesante relato.
        
            