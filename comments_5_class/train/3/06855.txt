
                      Segunda parte de las crónicas de Blancanieves, esta vez sin Kristen Stewart (que eso favorece a la película) pero con estrellas como Emily Blunt y Jessica Chastain. Nos cuentan una historia que camina entre Frozen, El Hobbit, Narnia y Juego de tronos, pero quitando eso, es una película entretenida y con un aspecto visual muy bueno, lo malo que para ser tan comercial se alarga mucho y el espectador acaba perdiendo el interés.
        
            