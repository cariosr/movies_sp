
                      Quizás The Dirt, cinematográficamente hablando, tienda más a telefilm que a película de pantalla grande en ciertos aspectos, y la dirección de Tremaine deje un cierto poso gamberro que delata que estamos ante el director de numerosas películas de Jackass. Pero lo que no se le puede negar es que es entretenida, que no tiene demasiados filtros en lo que a los excesos de la banda se refiere y que, especialmente para los fans de los biopics musicales, te hace pasar un buen rato a ritmo de sexo, drogas y rock´n´roll
        
            