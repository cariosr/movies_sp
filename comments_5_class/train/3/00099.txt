
                      El cierre a la trilogía de Las Crónicas de Narnia es la película más floja de la saga. El ritmo es bastante irregular y la acción es mucho menos épica que en las anteriores, así como más intermitente. El hecho de que también falten algunos de los personajes más importantes de las dos anteriores tampoco ayuda demasiado, y esta tercera entrega se hace mucho menos dinámica y entretenida.
        
            