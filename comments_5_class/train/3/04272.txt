
                      Nos encontramos en un futuro donde las grandes potencias se han unido en la carrera espacial y tienen representación de especialistas en la gran estación internacional espacial que está en órbita alrededor del planeta Tierra. Su cotidiana vida cambia al recibir un satélite procedente de Marte con las últimas muestras recogidas de la superficie del planeta rojo. En su análisis biológico descubren una sola célula que se encuentra completamente inactiva y que no da muestras de vida en su estado actual; pero deciden estimularla y crearle un clima experimental para comprobar que existe o existió vida fuera de nuestro planeta, hasta que lo consiguen, con el consiguiente éxito y reconocimiento de toda la humanidad.

Pero toda esta euforia se convertirá en catástrofe motivada por varios acontecimientos que darán autonomía a la reproducción y desarrollo de esta célula, y que descubrirá en poco tiempo ser letal y mortal.

La película que nos vuelve a traer el terror espacial, en los límites de una nave y con el vacío a su alrededor, no deja de ser una experiencia asfixiante y que mantiene el suspense durante su desarrollo, pero tampoco nos deja de recordar el argumento, historia y evolución de “Alien”, aunque eso no sea malo, pero en la comparativa pierde.

Life, aunque quiere tener un final evidente de poder tener una continuidad, no es una gran película de terror-ciencia ficción. Los personajes no cuentan nada de sí mismos y todos tienen la tendencia de anticipar o preparar su muerte ante este ser, para salvar al resto o la humanidad; y es una predilección que se mantiene en todos, ante la misión específica de la vida alienígena de eliminarlos uno a uno.

Por otro lado, vemos un cambio estructural de dicho ser, que también quiere tener un referente a la saga “Alien”, pero no crea esa sensación de ver y esperar en que se ha reconvertido a cada paso que avanza en su evolución, quedando un resultado final más parecido a una planta carnívora que a un monstruo reconocible para nosotros. 

Tiene un buen elenco de actores de referencia, aunque ninguno destaca en su interpretación, al ser más protagonista la historia que los propios personajes.

No quiero decir que esté mal, porque es entretenida, pero no creo que se mantenga en el recuerdo como lo consiguió y sigue manteniendo Ridley Scott; y de la que esperamos con gran expectación y en breve su continuación con Alien Covenant .
        
            