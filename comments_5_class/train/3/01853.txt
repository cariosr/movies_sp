
                      Es una historia que muchos conocemos. Desarrollo lento y largo. Pierde por momentos la eficacia de la espada y solo quedan breves claros de lucidez. Un buen Bale que se queda a medio camino entre coger el tren o ser un superhéroe, solo deja una buena interpretación sin discurso; algo raro hablando de Scott.  Junto a Noe me parece un film carente frente a lo posible en la actualidad.
        
            