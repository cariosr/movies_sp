
                      Para mi este tipo de cine cargado de efectos digitales y acción sin respiro ya me interesa poco. Reconozco cierta imaginación a la hora de fabricar una atracción como si la pantalla fuese un parque de atracciones, en el fondo se trata de esto y poco más. Por algo Disney es especialista en levantar Parques Disneyworld que reciben millones de visitas.
        
            