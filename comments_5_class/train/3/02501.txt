
                      Entretenida y no mucho más. Una especie de policial de suspenso futurista donde lo más atractivo es ver a la siempre genial Jodie Foster . El resto es un intento casi Tarantinesco de manual con diálogos y personajes que no salen de lo típico de este tipo de películas. Repito,  entretiene sin ningún objetivo especial.
        
            