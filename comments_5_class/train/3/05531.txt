
                      Cuenta la gran aventura de Bodi, un perro nacido y criado por su padre en un pueblo de ovejas desempeñando la difícil tarea de protegerlas del ataque de los lobos. Pero esa obligación se ve afectada por la gran pasión de Bodi: tocar la guitarra. Esto lleva al protagonista a dejar su lugar de origen y probar suerte con la música en la gran ciudad. 
Simpática, algo divertida, ideal para verla en familia.
        
            