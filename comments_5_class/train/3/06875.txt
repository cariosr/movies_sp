
                      Drama del estilo "Capitán Fantástico" o "Kramer vs Kramer" donde se disputan judicialmente la tenencia de menores. En este caso puntual, la tutela de una niña genio es pretendida por su abuela y su tío, tras el trágico fallecimiento de su madre. Explotar su brillante potencial o llevar una vida normal como cualquier chico de 6\7 años?. Ese es el debate moral que se plantea en la película. Interesante temática y una excelente actuación de la pequeña protagonista. La recomiendo.
        
            