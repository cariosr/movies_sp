
                      Entretenido film y con puntuales momentos de ingenio, como la comparecencia de Andy Warhol, en realidad un cazaextraterrestres que disimula su oficio fotografiando objetos o filmando gente, pero en 
conjunto es tan débil como la pata de un colibrí.
        
            