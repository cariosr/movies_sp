
                      Valoración de Blancanieves de Berger: 9. Mi critica:
 Despues de la publicacion en el cine, que ademas es feisima, de "Blancanieves y la leyenda del cazador", en España, mi pais, ha hecho otra pelicula en referencia al cuento, PERO NO SOLO A LA PELICULA, SINO AL ARGUMENTO.
La pelicula es muy buena, ganadora de varios goyas, de entre ellas mejor pelicula, actriz, actriz revelacion,... 
No imita al clásico cuento, la sinopsis es: Blancanieves es Carmen (Macarena García), una bella joven con una infancia atormentada por su terrible madrastra, Encarna (Maribel Verdú). Huyendo de su pasado, Carmen, emprenderá un apasionante viaje acompañada por sus nuevos amigos: una troupe de Enanos Toreros. Con un final inesperado y sorprendente, “Blancanieves” nos descubrirá que “la vida no es como en los cuentos”, sino como en un melodrama gótico.
Lo bueno de la pelicula es que Maribel Verdu actua de maravilla, tiene muy buena fotografia, imagenes, interpretaciones, algo de comedia....
El problemita es que cuando ocurre algunas cosas en referencia al cuento de los Hnos. Grimm, va acelerado o saltando escenas, y que hay pequeñas cosillas que se pueden mejorar, ya lo verán.

En resumen, de las grandes peliculas españolas 2012, esta es de las mejores, similar a Lo Imposible, tambien superbuena. Asi que, si alguien la ve, os animo a verlo. Que Pablo Berger os acompañe, y..

"RECORDADLO SIEMPRE AMIGOS, LAS PRIMICIAS SIEMPRE SON DE CHARLIE FROST"
        
            