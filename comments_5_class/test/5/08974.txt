
                      De las mejores por no decir la mejor de las películas de la segunda guerra mundial. Impresionante actuación de Ralph Fiennes. Escenas duras, pero representativas del conflicto. En mi lista de las mejores 100 películas de la historia. Y en el top 4 de mejores películas de la segunda guerra mundial, junto a El Pianista, Cartas desde Iwo Jima y El Hundimiento.
        
            