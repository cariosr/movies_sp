
                      La mejor película de ciencia ficción que he visto hasta el momento, y lo digo porque haberlas buenas las hay, pero que sean tan rigurosas, pocas, casi le pondría la categoría de “Cierta ficción”, porque pocas veces viendo una película de este tipo he sentido que, Oh my God, esto puede ocurrir. 

Tres horas que se te pasan volando entre la cruda realidad de lo que le hacemos al planeta, el poder de supervivencia de nuestra “querida” especie, agujeros de gusanos, quintas dimensiones e imágenes espectaculares de otros mundos más que realistas y basadas en sólidos conocimientos científicos que Nolan ha sabido utilizar poniendo claro esta ese punto de ficción que es más que necesario (el espectáculo también es necesario). 

Reparto de lujo, que hace su trabajo de manera tan humilde que te olvidas de verlos encasillados en sus papeles mas memorables. 

	En definitiva una película que recomiendo al cien por cien, que hace reflexionar, emocionarse y estimular todos los sentidos, sin quitar el espectáculo que el cine tiene que tener e incluso cabida para la diversión. 

	Les dejo parte de el poema de Dylan Thomas que aparece en la película narrado por Michael Cain y que es increíble:

No entre dócilmente en esa buena noche, 
la vejez debería delirar y arder cuando se acaba el día, 
rabia, rabia, contra la luz que se esconde, 
aunque el sabio cerca del fin a la tiniebla no haga reproche,
dado que su verbo ningún rayo a confiado vigor, 
no entra dócilmente en esa buena noche, 
rabia, rabia, contra la luz que se esconde.
        
            