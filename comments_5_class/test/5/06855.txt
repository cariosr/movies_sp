
                      Comedia española 100% en la que, aunque conozcas los tópicos, te será imposible no pasar un gran rato de risas desde que empieza hasta que (lamentablemente) acaba; siempre y cuando la veas sin ser reacio por ser vasco o andaluz, en este caso. Por otra parte, lo que hace verdaderamente graciosa esta película es el derroche de talento de nuestro malagueño Dani Rovira, quien ha demostrado que no solo es capaz de hacerte reír con sus monólogos, sino que también es un "actorazo de diez".
Se agradece que el cine español dé algunas señales de vida y demuestre que sigue habiendo mucha gente con talento en este país.
        
            