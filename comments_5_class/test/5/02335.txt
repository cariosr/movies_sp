
                      ¡Sencillamente genial! Genial interpretación del protagonista en la piel de ese ser humano con un cerebro excepcional, pero de carácter huraño e insociable, al que hay que añadir su homosexualidad, perseguida y criminalizada en aquellos años y aún hoy en muchas partes del mundo. Genial todo el reparto, la fotografía, la ambientación, en fin me gustó todo.
        
            