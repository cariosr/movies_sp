
                      Esta película se merece ese medio punto extra por la sorpresa final. Un completo giro de guión que dirige excepcionalmente Oriol Paulo. Gran thriller psicológico con abundantes dosis tétricas y de completa frialdad pero consiguiendo a la vez mantener la tensión del espectador. Muy recomendable.
        
            