
                      Jurassic Park (Parque Jurásico) es una película de ciencia ficción y aventuras dirigida por Steven Spielberg, escrita por David Koepp, y con George Lucas al frente de los efectos especiales.

John Hammond (Richard Attenborough) es un anciano multimillonario que logra su sueño de clonar dinosaurios. Su intención es montar un parque temático en una isla, y exponer los dinosaurios al público. Antes de inaugurarlo, llama a dos expertos arqueólogos (Laura Dern y Sam Neil) y a un matemático (Jeff Goldblum) para que comprueben la viabilidad del proyecto. Sin embargo, los sistemas de contención fallan dejando a los peligrosos y letales dinosaurios sueltos.

A pesar del buen reparto de actores con el que cuenta la cinta, los verdaderos protagonistas son los dinosaurios; unos seres monstruosamente grandes, terroríficos y reales. Los efectos especiales, manejados por ordenador, no se han desmejorado en absoluto con el paso de los años.
Cada detalle, tan bien cuidado, consigue crear un ambiente de suspense y de miedo que cala al espectador. El sonido de las pisadas de los animales, sus rugidos, ayudan a crear esa atmósfera. Sin duda, es una obra maestra que sigue, y seguirá emocionando a pequeños y mayores, pase el tiempo que pase.

La banda sonora la creó John Williams, un compositor único e irrepetible, que nos ha dado maravillas tales como la banda sonora de Star Wars.

La película recibió tres premios Oscar por los mejores efectos visuales, efectos de sonido y mejor sonido.
        
            