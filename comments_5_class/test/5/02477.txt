
                      Es bonito volver a ver pelis de animación dirigidas al público infantil cuando eres adulto. Yo siempre creí que tras hacerte adulto las pelis de animación ya no te entretienen tanto como de pequeño pero me alegra de ver que me equivocaba. 
"Mascotas" cuenta con un realismo visual muy bueno, un montón de personajes simpáticos y un gran trabajo de producción detrás que es digno de admirar. 
Creo que no hace falta que me enrolle mucho en mi crítica... con solo decir que SE ME HIZO MUY PERO QUE MUY CORTA Y QUE QUERÍA DISFRUTAR MÁS DE ELLA Y DE LAS AVENTURAS DE MAX Y COMPAÑÍA lo digo todo.
        
            