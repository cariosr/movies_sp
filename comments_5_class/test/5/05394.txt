
                      Es una gran película que se disfruta de principio a fin. Con una buena historia y con unos personajes que te llegan al corazón. Una hora y media de un peliculón no solo para niños sino también para adultos.

La aventura comienza un año después de "Buscando a Nemo" , todo empieza cuando Dory empieza a tener flash-backs de su infancia recordando a sus padres, recuerda que los estuvo buscando hasta que topo con Marlín y se fueron a buscar a Nemo. Ahora Nemo y Marlín ayudaran a Dory a encontrar a sus padres en una aventura donde conocerás nuevos peces que les ayudaran a llegar a su destino. Tendremos a un pulpo Duro pero tierno por dentro (tiene 3 corazones), a un tiburón ballena miope y un "cabezón blanco" con eco sonar (ni idea de la raza).

Muy entretenida. La recomiendo.
        
            