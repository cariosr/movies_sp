
                      Si alguna película en la historia se merece la máxima puntuación que se le pueda dar es este retrato de una época convulsa y liberadora como la que retrata esta historia. A pesar de todo y de las renuncias a la que se tiene que adaptar Carol sin esas valentías de antes no tendríamos la sociedad de ahora.
Encima está esa atmósfera tan magnifica, esos vidrios entelados, ese universo sacado de cuadros de Hooper, esa luz... I unas magníficas actrices que lo llenan todo, que lo dicen todo, que te transportan y te emocionan...
Si tengo que quedarme con una escena, la del embotellamiento en las calles de Nueva York está rodada con una precisión y una técnica inaudita!
        
            