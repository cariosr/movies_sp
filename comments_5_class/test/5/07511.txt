
                      La mejor película romántica, si por romántica entendemos intensa, apasionada y desestabilizadora, de la década. Joaquin Phoeix esta increíble, vulnerable y deseoso de ser amado por aquella persona a quien ama, que es a su vez aquella que no es amada por los que le dicen que son quien mas por él han hecho (sus padres), es una película grandiosa.
        
            