
                      Esta película es una auténtica obra maestra y cuenta de manera sobrecogedora unos acontecimientos por lo que el propio Tony Blair, Primer Ministro Británico, en el año 2005 pidió perdón en la televisión. El director Jim Sheridan dirige la película de forma magistral al igual que adapto el guión de una manera sublime. Las interpretaciones son estupendas, en especial las de Daniel Day-Lewis y Pete Postlethwaite, que sin semejante actuación la película ya no sería lo mismo.
        
            