
                      RETRATO DE LA VIDA FAMILIAR COTIDIANA

A principios de la década de los noventa, el director estadounidense Richard Linklater escribió y dirigió la película "Danzed and Confused", que narraba el último día de clases de un grupo de alumnos de secundaria en 1976. Con ello, pretendía realizar un fiel retrato del sentimiento de los jóvenes de la época, en la que se podía ver el libre consumo de drogas y la falta de guía que desemboca en que cada quién tiene y practica sus valores de manera independiente; algo similar a lo que hizo George Lucas con su filme "American Graffity".  Por consiguiente, cogiendo tal referente se podría decir que Linklater se ha superado a si mismo con la película que nos concierne, "Boyhood (Momentos de una vida), ya que en ella relata el devenir de un chico y su familia a lo largo de poco más de una década, mostrando como van cambiando sus sentimientos e inquietudes, y al mismo tiempo, como evoluciona la sociedad. Linklater incide y reflexiona sobre la vida, planteando y respondiendo de forma sutil e inteligente cuestiones al respecto, potenciadas con lo expuesto en el transcurso del metraje.

Para llevarla a cabo, Linklater contrató al entonces niño de 7 años Ellar Coltrane para meterlo en el centro de la historia; además de su amigo y actor fetiche Ethan Hawke, Patricia Arquette, y su propia hija, Lorelei Linklater. Lo verdaderamente increíble de "Boyhood" es la convicción que ha mostrado su director a lo largo de 12 años para sobrellevar un proyecto de esta envergadura, manteniendo enchufados tanto al elenco actoral como al equipo técnico en ese largo periodo de tiempo, y una vez terminado todo, comprobar que el resultado final es más que satisfactorio... sin lugar a dudas, una labor muy meritoria y encomiable que evidencia la certera visión del director.

Al estar rodada en ese largo periodo de tiempo, huelga decir que la evolución "natural" de los personajes es impresionante; sin embargo, aparte de eso, se puede destacar la excelente banda sonora, repleta de canciones country, rock y pop, debido a que están muy bien integradas en el contexto de la historia.

En conclusión, "Boyhood" es una obra maestra, un verdadero ejercicio de estilo realizado por su director, Richard Linklater, que ha demostrado entereza, fe, y una visión insólita; a pesar de que su argumento sea muy sencillo, el cual se puede resumir en una frase... pero en este caso, no es lo que cuenta, sino como lo cuenta. 

Muy Recomendable.
        
            