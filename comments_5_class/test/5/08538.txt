
                      No se puede negar que la película es una obra maestra. Para empezar, hay un reparto y un director estelar. Creo que la película te engancha desde el principio y siempre te mantiene en tensión durante momentos. Además, Origen es nada predicible puesto que la trama te da que pensar un montón. ¡Algunas escenas de acción que dejan con la boca abierta!
        
            