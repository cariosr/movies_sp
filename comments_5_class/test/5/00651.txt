
                      La mejor peli vista en mucho tiempo y desde luego la mejor versión de Spiderman. 100% recomendable. Era reticente en ir a verla pues los trailers no me habían convencido y los remakes, cambio de actores, no me gustan, pero reconozco que he olvidado por completo a los otros dos protagonistas y me quedo con estos sin duda. Se hace quizá un poco larga tirando por el final. Un héroe más humano, más sarcástico, más tierno. Caídas graciosas y algunas escenas emotivas. Momentos tiernos crédulos. Muy bien los actores.
        
            