
                      Me parece una película fantástica, es preciosa y me reí muchísimo. Fui a verla con mi sobrina y a ella la encantó también. Es original, con un argumento muy cuidado y creo que tiene unos mensajes preciosos. La verdad es que me sorprenden mucho las críticas desfavorables hacia esta película, ya que es la mejor película de animación que he visto en mucho tiempo. Las voces de Juan y Damian (Trancas y Barrancas) Arturo Valls y José Luis Gil me encantaron, son un puntazo.
Una maravilla de película.
        
            