
                      Espectacular película, genial en todos los aspectos, no soy capaz de encontrarle un solo punto negativo. Esa dirección, ese guión, esas impresionantes actuaciones, esas brillantísimas escenas de salvaje violenta disparatada y exagerada que te ponen la carne de gallina, esa banda sonora... Yo creo que esta maravilla cinematográfica carece totalmente de fallos, al igual que su hermana histórica tarantinesca Malditos Bastardos (perfecta también). No me canso de ver este peliculón.
        
            