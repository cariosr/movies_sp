
                      Creo que es una película que aporta mucho a esta sociedad. Cargada de valores humanos, que carecen en el mundo donde vivimos. Es entretenida, emotiva y divertida. 90 minutos que te impide ver el reloj. 
No hablamos de una película con un trabajado guion, con grandes efectos especiales y con un alto presupuesto, sino más bien todo lo contrario. Pero yo pienso que una película tiene que impregnar y aportar y Todos los caminos lo hace con creces.

Recomendable.
        
            