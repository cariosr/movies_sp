
                      Basada en una historia de la vida real durante la Segunda Guerra Mundial en la batalla de Okinawa, esta cinta centra su atención en un suceso, cuanto menos interesante por el contexto en el que se desarrolla y el mensaje que lanza es una intencionalidad manifiesta de su director que en la mayor parte de sus cintas. Tiende claramente a dar una visión muy particular en las tramas en las que basa sus obras por eso es uno de los mejores en este apartado. Esta cinta sólo es una prueba de esto que menciono.

El drama vivido durante todo la cinta, es de lo mejor cuando buscas no sólo espectacularidad. Si no enlazar una historia real con una película, es complicado no entrar en errores típicos y más al tener como máximo espectador a la misma persona que originó la misma. En esto Gibson hace un trabajo fantástico en la exposición de los hechos de una forma realista pero buscando siempre la magia brutal de una guerra que en muchos sentidos, no deja lugar a personas como el personaje principal de la misma.

Las secuencias sobre todo en los momentos álgidos de la cinta cumplen de manera casi brillante lo sangriento pero al mismo tiempo emotivo de lo vivido en ese momento de la historia. En algunas de ellas recordé casi sin querer a una cinta bélica también protagonizada por Tom Hanks que tuvo algo similar en algún momento de ella pero en Hasta el Ultimo Hombre, le superan de sobremanera y es una cinta profunda con un mensaje enternecedor al mismo tiempo que crudo que muchos no podemos dejar pasar por alto.

Es un film brillante, sólo me costó ver a Garfield en este tipo de género que no se adapta al perfil de este actor pero que no lo hace nada mal. Lanza mensajes subliminales interesantes, las secuencias de acción están magníficamente ejecutadas. 
En definitiva, una magnífica película del cine bélico.
        
            