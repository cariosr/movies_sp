
                      Película lenta y para los que no le gustan el cine, aburrida. Para mi una pequeña obra de arte, con algunos pequeños errores, que no desmerecen de la gran actuación de los actores. Un tema muy visto pero de una manera diferente. Yo iría a verla.
        
            