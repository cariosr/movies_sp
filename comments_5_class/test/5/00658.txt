
                      Película sorprendente y extraordinaria por su ritmo y composición, pude verla en el pasado festival de cine de San Sebastián 2011 en su clausura y me dejo un gran sabor de boca, muy buenas interpretaciones, gran dirección, buena banda sonora, de las que están de moda en estos momentos. No dejen de verla, aunque para mi gusto tiene alguna escena de violencia demasiado explícita, que pienso no es necesaria para trasmitir la fuerza de esas escenas, pero que no empaña la brillantez de las 
mismas, decir tambien que es un filme oscuro, ya que la historia transcurre principalmente por la noche, no dejen de verla, es fabulosa. Creo que estamos ante una de las películas del año.
        
            