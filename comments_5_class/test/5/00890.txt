
                      Una de las mejores películas. Pretende mostrar cosas de la política de forma fácil, tiene ese toque de carisma y bromas inteligentes que nunca están de más, y demuestra que si el espectador es comprometido nunca se dormirá.
        
            