
                      Muchos esperábamos “Múltiple” como el renacer de un director que empezó muy fuerte y no se mantuvo en lo más alto. Soy de los que piensan que películas como “Airbender” o “After Earth” no deberían estar en su filmografía. Que tuvo que seguir la senda de maravillas como “El bosque” o “Señales“. Pues bien, creo que siendo Shyamalan consciente de esto, supo enmendarse hace un par de años con “La visita” y sabe que debe seguir haciendo lo que mejor se le da: ser el Hitchcock del siglo XXI.

De nuevo vuelve a sorprendernos con “Múltiple”, una historia aterradora sobre el secuestro a tres niñas por un hombre que tiene nada menos de 23 personalidades. Una premisa interesantísima que el director explota durante hora y media llenando la pantalla de planos aterradores. Un guión que no da puntada sin hilo, unos personajes fuertes y desgarradores y una única figura que se come la película: James McAvoy. El actor nos presenta el que puede ser el mejor papel de su carrera (¿podemos darle ya el Oscar?) y puede que uno de los más maravillosos que hayamos visto en los últimos años en el cine de suspense. Sin duda, una elección inmejorable. Acompañándole, Anya Taylor-Joy, que vuelve a demostrarnos que lo suyo es el cine de ponernos los pelos de punta. Y es que tras “Morgan” y “La bruja“, vuelve a fascinarnos dando vida a una de las secuestradas que deberá luchar por sobrevivir contra algo tan oscuro como inimaginable.

De final, ni hablamos, pero esos giros de guión, ese momento clave que te deja con la boca abierta, tan marca de la casa en el cine de Shyamalan, obviamente están presentes, y no hay más que decir. Disfruta del final y deja que los demás disfruten de él.

Lo mejor: McAvoy, arrollador.
Lo peor: que no juegue con el espectador.
        
            