
                      Muy buenos días. Impresionante película. Simplemente magistral. Da igual en qué momento sea vea: es un peliculón se mire por donde se mire. El único defecto que se le pude poner es que termina. Por lo demás, se agradecen películas como ésta, que no sólo te dejan sino que también te llevan a pensar (y mucho). Por si a alguien le interesa, mi opinión-reseña completa está en el link adjunto. Buena mañana a todos.
        
            