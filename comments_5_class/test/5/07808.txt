
                      Es una película que se disfruta desde el primer momento hasta el final, es una trama rápida que te mantiene pegado al asiento expectante de lo que esta pasando. No se hace pesada como si pudo hacerlo un poco su predecesora. Eso si, esta película es una transición entre man of steel y las siguiente películas que están por venir, asentando las bases de un universo DC que tiene muy buena pinta. Mientras la veía tenía la sensación de estar leyendo un buen cómic de DC, una sensación magnífica.
        
            