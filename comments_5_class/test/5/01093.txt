
                      Quizá el mejor trabajo de Edward Norton a la fecha.
Ideales, fascismo, discriminación... nazismo. Todo junto en éste film que pone los pelos de punta y muestra a un Norton impecable (bien merecida la nominación al Óscar).
Trata un tema muy delicado pero logra llevarlo muy bien.
        
            