
                      Además de ser una obra maestra, es un termómetro de estupidez emocional. Buena señal para aquellos que queden prendados. La inteligencia emocional es otro factor evolutivo que se abre en nuestra sociedad lentamente. Pixar aporta su granazo de arena. Felicidades.
        
            