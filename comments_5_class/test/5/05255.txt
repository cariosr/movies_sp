
                      Como siempre, todo lo bueno tiene un final, La Doncella, es ese gran final, el punto culminante de la carrera del director surcoreano Park Chan Wook.

Aunque la película dura unas dos horas, nunca te aburres, siempre hay acción, suspense y tensión (sexual). Además los sets dónde se filmó la película son un personaje más y la actuación de los actores principales ha sido exquisita.
Un filme que no es el típico filme comercial, sino que es para cinéfilos y amantes del cine un poco más refinados.
        
            