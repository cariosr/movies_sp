
                      Imprescindible cinta, de lo mejor que nos ha dado el cine europeo en los últimos años. Una historia muy dura en la que una mujer pasa lista a los hombres que han pasado por su vida. Ninguno queda indemne, y mucho menos ella. Una puesta en escena nerviosa contribuye a darle a la película un tono cotidianamente inquietante. Formidables las interpretaciones de Emmanuelle Devos y Mathieu Amalric. Viendo películas como esta no nos queda más que constatar que el cine español sigue estando a años luz del cine francés.
        
            