
                      La película en sí es como la recordamos, una telenovela romántica masivamente entretenida que de repente se convierte en una lucha por sobrevivir. La primera mitad, nos proyectamos en ella, la segunda mitad intentamos imaginarnos tratando de salir de ella. Lo mejor de la realización cinematográfica es la forma atractiva en que "Titanic" nos absorbe en lo que parece ser, en la superficie, un romance magnético, mientras que debajo hay peligrosas corrientes de conciencia de clase y caídas de arrogancia. (¿Ya he agotado las metáforas? Probablemente no sea sangrienta).

Y luego, de repente, ese iceberg arranca la cubierta segura y estable de debajo de los personajes. No te metas con la naturaleza.

Hay tantas capas de significado asociadas a la tragedia del Titanic que no es de extrañar que se haya convertido en una piedra de toque de la mitología del siglo XX. Cameron entendió esto y, sin embargo, no lo aplica. La película fue burlada por su diálogo simplista; Me asombra bastante su economía de superposición expositiva. La maldita cosa dura más de tres horas y, sin embargo, no quieres dejar tu asiento ni un segundo.

Es, simplemente, una gran película, un retroceso al cine clásico, y más que eso, una experiencia con una resonancia inusual en el mundo de hoy.
        
            