
                      Me encanta esta película desde el primer momento que la vi. Nunca antes había visto a Kit Harington y se ha convertido en mi actor preferido. 
La película cumple todo lo que yo quería. Me deja intrigada, emocionada y con una sonrisa en los labios y lágrimas en los ojos. 
Es triste, pero la historia de Pompeya lo es. Aún así, ese romance es tan especial... Me hubiera gustado que hubieran sobrevivido.
        
            