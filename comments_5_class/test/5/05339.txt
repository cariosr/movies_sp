
                      Una obra maestra! Los 4 actores están inmensos. El guión: perfecto. La cámara de Polanski: una lección de cine. Una gozada de cine en estado puro.
        
            