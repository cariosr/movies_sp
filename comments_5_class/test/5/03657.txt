
                      La denominación exacta de esta película es PER FEC TA.
El antihéroe que todos esperábamos ya está aquí. Humor negro, grandiosas dosis de acción que harán las delicias a todo el mundo y todo aderezado con toques gore. La guinda heroica del pastel.
(El chorro de aire fresco que le hacía falta a este género).
        
            