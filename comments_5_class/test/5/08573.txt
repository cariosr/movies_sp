
                      Son casi tres horas de duración. Ahí está eso. Además, Interstellar es una odisea espacial sin OVNIS, sin criaturas de piel azul de otro planeta, ningún extraterrestre estallar en el cofre de la estrella Matthew McConaughey. Revela un lado optimista del cineasta Christopher Nolan que hará enojar a los agoreros de Dark Knight. Y, oye, ¿acaso Alfonso Cuarón no acaba de ganar un Oscar por dirigir Gravity? ¿Cuánto tiempo se espera que el público se drogue con los gases de los cohetes?

Bla, bla, bla. Perra, perra, perra. Lo que los neg-heads se pierden sobre lo Interstellar es lo cautivador que es, cuán elegantemente combina lo cósmico y lo íntimo, con cuánta destreza explora el infinito en los más pequeños detalles humanos.
        
            