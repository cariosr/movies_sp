
                      BRUTAL. Hay muchas películas con la misma extorsión sea un coche, cabina de teléfono... Pero en este caso creo que todo es perfecto. 
Luis Tosar se sale. Su "hija" pega un giro a mitad de película que te deja con la boca abierta.

La acción comienza a los 5 minutos y no para hasta el final, siempre con la duda de ¿quién es peor?

Una pena que la gente siga asociando el cine español con Torrente.
        
            