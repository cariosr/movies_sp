
                      "Nunca tendremos Paris", ¡por suerte para todos!
Payasada de circo que ofende a tan estimada profesión, teatrillo barato sin habilidad para encontrar un mínimo apartado que realizar con gusto y esmero, barruntada de fotogramas que siguen el juego de la oca, de casilla a casilla y tiro porque me toca, que se da una vuelta por Paris, pues hay presupuesto de sobra, y que juega a fingir que cree en lo que hace cuando, ya se anticipa el suspenso ¡nada más empezar!
Un amigo profesor, una vez me dijo que se veía obligado a realizar publicaciones de escritos, así como cursos de refuerzo, como parte extra de su profesión, la cual no se limitaba a ir a clase y dar la misma matería una y otra vez, mantenerse y estar al día como precio para seguir siendo un buen maestro, actividades aleatorias que deberían hacerse con gusto si realmente se ama la profesión elegida como forma de vida. 
Me pregunto si, Simon Helberg, también tiene en su contrato, como miembro de la exitosa serie "The big bang theory", realizar películas aparte y entretenerse con distracciones diferentes para seguir siendo socio de tan exquisito club porque, en caso contrario, es difícil definir, justificar y encuadrar esta garrulada artificiosa, vacía, boba, simplona, sin argumento que mancillar ni historia que contar, sólo escenas aisladas de lelo niño caprichoso que no se cree nadie, que hace estupidez tras tontería dejándose arrastrar en cada fotograma por el acompañante de turno; por un lado, la ex novia a recuperar que parece más su madre adoptiva que futura esposa, la compañera de trabajo, una lunática desesperada que suplica a cada minuto ser besada por este pueril Don Juan ridículo, con amigo guayabo a lo cutre que vive del capitalismo de sus padres mientras va de colega espiritual, todo ello envuelto en escenas penosas, sin cerebro estructural, que manejan un estrangulado guión que debería suicidarse por tener la desgracia de caer en manos tan absurdas y gastarse dinero desperdiciado en tal calamitosidad, fanfarria sin crédito, observación ni nada que valga la pena excepto la consideración de qué memez presuntuosa les llevó a pensar que, un relato parecido de actuaciones tan vulgares e inverosímiles, necias de gran desatino y sandez, tenía posibilidades de ser visionado sin ser fustigado.
Por otra, también da qué pensar lo fácil que es, para algunos, hacer una película, ir de escritor, director y actor -amén de productor pero ¡ahí un respeto!, pues malgastar su propio dinero en el mismo es lo único coherente y sabio que vas a ver ya que, si no cree él mismo en su persona y facultades, pocos lo harán ¡visto lo visto!-, cuando no se tiene pajoterra idea de qué se quiere, qué hacer, dónde ir o cómo hacerlo todo con un mínimo de decencia y respeto por el público, la profesión y ellos mismos cuando, verdaderos ingeniosos de la escritura y artistas de la dirección, deben ir suplicando, rezando se les escuche, de una oportunidad para demostrar su valía, talento sobrante que aquí ni se observa, intuye ni huele pues, al acercarse al rodaje y ver el panorama, salió corriendo no fuera que ¡tan masiva incompetencia y nulidad fuera contagiosa!
Mejor apuesta por que nunca terminen las temporadas donde poder seguir siendo Howard por siempre, eternamente, bien acompañado y refugiado por tus compañeros de reparto que tapan las carencias que se observan cuando intentas dejar dicho personaje y ¡realizar algo más!
Pérdida de tiempo y estima para con uno mismo, y eso que, con sobreaviso, la había elegido para ese momento ligero, cansado donde deseas visionar algo ágil y cómodo que permita distraer sin necesidad de pensar pero, erróneamente, a causado el efecto contrario al iniciarse un cabreo insultante, que aumenta su temperatura conforme avanza la sandez, y que te lleva a pensar ¿por qué no hay un exámen básico que distinga quien aprueba o suspende a la hora de proponer, iniciar y realizar un proyecto?..., ahorraría tortazos, trompadas, fraudes y vergüenzas ajenas dada la trivialidad insípida, ultrajada y despropósita que se acaba de elegir para ocupar un leve momento de recreo.
¡Dios!, ¿cómo se puede ser tan inútil intentado relajar, hacer reír y disfrutar al público?
Salta de escena idiota a gansada superficial, a romance patético, a crisis existencial que mejor echarle un cubo de agua encima a ver si ¡se le aclaran las ideas!, y nos dejamos de hacer el tonto, de pretender que somos buenos en los que hacemos y que es suficiente con desfilar por pantalla, saberse el texto y ser conocido por la habilidad e inteligencia de quien me escribe los textos para mi principal trabajo en la tele, porque aquí, ¡estás de paso y ya vas sobrando!
Buscando mediocridad, de baja calidad, para pasar el rato me estrellé contra ¡la madre de mis deseos!, baratija banal, insutancial, lela, floja que ni regalada, ni de segunda mano ¡da el pego!
Sencillamente, mala
        
            