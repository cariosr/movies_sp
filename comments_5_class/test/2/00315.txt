
                      Esta incalificable película que trata de mezclar comedia y thriller en una historia de engaños es un despropósito de principio a fin: Las situaciones resultan inverosímiles, todo parece predecible y los momentos de humor son prácticamente insignificantes. Se hace pesada sin ser larga y no tiene gancho
        
            