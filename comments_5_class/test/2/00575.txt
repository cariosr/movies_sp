
                      Una vez mas nos presentan otra adaptación de un clásico de los cuentos infantiles. Quizás lo que la diferencia de sus antecesoras es que volvemos al cuento infantil, y por tanto excesivamente fantástico e increible.

Tiene momentos divertidos, alguna escena lograda, como la de 'la manzana' y auténticos desastres como la canción del final, cantando y bailando como si de una producción de Bollywood se tratase. 

Francamente decepcionante en general...
        
            