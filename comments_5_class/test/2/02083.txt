
                      Es muy dificil hacer hoy una critica de odisea,  tiene un merito de tratar de enseñarnos algo pero erra en la forma, interminables silencios que en esta película no indican nada, si bien en los silencios (plural porque no todos son iguales) pueden transmitirse lo que no pueden transmitir las palabras, aqui son demasiados extensos y uno lejos de entrar en un estado de comprensión termina cansandose de los mismos.
Silencios, colores tecnicamente correctos pero, pero no para una pelicula además demasiada extensa para su cometido.
Somos buenos al calificarla con un aprobado y nada más.
        
            