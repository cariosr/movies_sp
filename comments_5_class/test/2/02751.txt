
                      Sabía que esta película era distinta a sus dos fantásticas antecesoras, pero aún conociendo su género de comedia de terror gore, me ha decepcionado y me ha resultado un poco tomadura de pelo. El actor principal, Diego Martín, tiene un personaje pésimo y lo remata con una actuación robótica y nada creíble, al contrario que su compañera Leticia Dolera, que indiscutiblemente se apodera del film. Cabe resaltar un par de momentos memorables y totalmente surrealistas, pero es un film algo pobre y decepcionante.
        
            