
                      Esta película es el claro ejemplo de que al público en muchas ocasiones le llama más la atención unos buenos efectos especiales, que el guión de lo que están viendo, aunque no se enteren de que va la historia...
Las chicas modelo, junto con los héroes típicos que solo son magnificados gracias a los espectaculares efectos especiales ya mencionados antes, que son lo único bueno que tiene la película.
Por lo demás, y analizando en general la trama entera, Michael Bay consiguió que los antiguos Transformers de Hasbro que tanto nos gustaban de niños, saltarán a la gran pantalla con una adaptación aburrida en gran parte, pero con buenos efectos especiales. también tiene acción si, pero los enfoques y la cámara  llegan a marear de sobremanera y acabas por desear que termine la escena. Y ni hablemos del humor...
una película que con paciencia y un buen guión, podría haber sido muy grande, pero solo se quedó en un largometraje aburrido para ver a la hora de la siesta, y ni eso.
        
            