
                      Removeos en vuestras tumbas
El ser humano tiende a cometer los mismos errores una y otra vez. Esto, trasladado al campo cinematográfico, viene a significar que hay una tendencia clara a mirar continuamente hacia el pasado en busca de nuevos materiales que explotar. El remake, tal y como lo conocemos todos. Una palabra que es sinónimo de vilipendio para buena parte del público y la crítica, aunque el apelativo sea tremendamente injusto. Hay remakes buenos –“El cabo del miedo”-, otros que echan un digno pulso con su referente –“Amanecer de los muertos”- y otros que, directamente, superan al original –“La cosa, de John Carpenter”-.

Por tanto, es peligroso aproximarse a una nueva versión de un clásico ochentero como “Poltergeist”. Porque a pesar de los muchos dimes y diretes entre Hooper y Spielberg, -cuyas discusiones, a juzgar por el resultado final de aquella película, acabó ganando este último-, aquélla acabó convirtiéndose en una cinta de culto, una obra a referenciar de cara al cine que vino a continuación, a pesar de ser una nueva reformulación familiar del cine de casas encantadas. 

Con una traslación a nuestros días, uno podría esperar que el “Poltergeist” de Gil Kenan jugase una doble baza. Por un lado, acercar la historia a nuestros días, objetivo de todo remake. Pero por el otro, apelar a la vena nostálgica de toda esa generación que se acerca a las salas temerosa de que hayan podido prostituir el espíritu de su tan venerada obra. Y ni una cosa ni la otra consigue esta película. No es más que un calco de la original con el piloto automático puesto –hay escenas calcadas, pero carecen de la magia spielbergiana- y sin aportar nada más allá de alguna escena resultona –básicamente, la que tiene como protagonista al dron- y el año en que está ambientada.

En ese sentido, no satisfará a los que adoran la cinta en que se basa. Pero es que tampoco lo hará a quienes se aproximen por primera vez a este material. Porque estamos ante una mala película, repleta de actores que no se creen sus personajes ni lo que les rodea –se lleva la palma Sam Rockwell-, de momentos supuestamente cómicos –sólo funciona en este ámbito el personaje de Jared Harris-, de fallos de continuidad –los personajes entran en una habitación y en el cambio de plano estamos en otra- y de guión, unidos a una realización sin personalidad de la que sólo sobresale la fotografía de Javier Aguirresarobe, sin ser ni por asomo de las mejores de su filmografía.

Es, posiblemente, el peor remake al que nos hemos enfrentado en lo que llevamos de siglo, y sin lugar a dudas una de las peores películas del año. Es tan ridícula que podría pasar como parodia. Pero no lo es. Es solamente un intento de hacer que nuestros ancestros se remuevan en sus tumbas. Y de paso, nosotros en nuestras butacas. Pero no de miedo precisamente, sino de vergüenza ajena.

A favor: algún toque de ingenio y Jared Harris
En contra: prácticamente todo lo demás
        
            