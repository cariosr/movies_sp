
                      He perdido el tiempo en darme de alta para recomendar encarecidamente que nadie se gaste el dinero en semejante insulto. Fui a verla y todavía recuerdo la cara de la chica de la taquilla cuando le pedí que me devolviera mi dinero. En resumen, una patata que ni tiene gracia, ni refleja la vida de nadie, ni hace que muevas ningún músculo de tu cuerpo en toda la peli (si se la puede llamar así), vamos como una seta me quede. Eso si refleja muy bien la decadencia sociológica que atraviesa nuestro país.
        
            