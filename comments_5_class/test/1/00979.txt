
                      Tarantino ha conseguido lo que pocos cineastas modernos consiguen: crear un personaje icónico para la historia del cine. ¡A estas alturas! Pues sí, la novia, con su mono amarillo y negro y su katana es tan perfectamente reconocible para los cinéfilos como lo es la Gilda de Rita Hayworth o la Viena de Johnny Guitar. Sólo por eso, esta película ya se puede considerar una obra maestra.
        
            