
                      La peor película que he visto en mucho tiempo.
Ni metáforas, ni leches. Es increïble la violencia innecesaria, y la agresividad que muestran. Una pura aberración. Un insulto al cine metafórico. Este film es una serie de escenas sin sentido, que unen sin un fin, que lo único que te hacen sentir es que pierdes el tiempo, y que no entiendes porque sigues sentada en el asiento del cine.
Solo vale la pena verla para poder opinar sobre esta basura. Bueno, y la actuación de Michelle Pfeiffer, que te da ganas de estrangularla de verdad.
        
            