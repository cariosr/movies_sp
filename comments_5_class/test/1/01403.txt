
                      Blancanieves (Mirror, Mirror) es una comedia de fantasía dirigida por Tarsem Singh, director de 'La Celda' y 'The Fall. El Sueño de Alexandria'.

Ésta nueva versión del cuento de los Hermanos Grimm está narrada desde el punto de vista de la Reina Malvada (Julia Roberts).
Blancanieves (Lily Collins) con ayuda de los enanitos, intentará recuperar su reino y conquistar al Príncipe Andrew (Armie Hammer), con el que la Reina planea casarse.

Lo único que merece realmente la pena de la cinta es Julia Roberts, siempre tan magnífica, que logra conservar la dignidad con una profesionalidad admirable en esta película tan mala.
Sean Bean y Nathan Lane también están bien, pero pasan más desapercibidos.
El resto del reparto es más o menos mediocre y nada atractivo.
La protagonista Lily Collins es sosa, aburrida y muy exasperante.

Con esta película esperaban contar una nueva historia, crear algo innovador, pero es más de lo mismo. Es tediosa, muy lenta, y ni los efectos especiales ni la banda sonora ayudan a mejorarla. La verdad es que no hay por donde cogerla.
El número de baile final es la gota que colma el vaso, que sinceramente, sobraba.

El diseño, los escenarios que abusan de pantalla verde y el vestuario, son excéntricos, tan llamativos que casi hieren a la vista.

La película obtuvo una nominación a los Oscar por el mejor diseño de vestuario.
        
            