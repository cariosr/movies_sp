
                      El Hombre Hormiga llega al Universo Cinematográfico de Marvel interpretado por el carismático Paul Rudd, lo mejor de una película que no está a la altura de la épica 'El Soldado de Invierno' o de la divertida 'Guardianes de la Galaxia'. Una buena combinación de acción y comedia (maravillosa la escena del tren) que flojea en su previsible y aburrido guion .
        
            