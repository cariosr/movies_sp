
                      Buena película bélica que nos habla de lo que pasa después de la guerra y todo lo que tienen que aguantar los soldados después de ver cosas muy turbias, luchar y matar para salvar sus vidas. La película tiene un buen guión, dirección bastante buena y actuaciones decentes, pero hay algo en la película que no me acaba de convencer, me esperaba mucho más de esta película, pero aún así es una película aceptable.
        
            