
                      Con un plantel que no está mal -aunque se hubiera apreciado más ver a la veteranía actuando- se logra una película visualmente grandilocuente y aparentemente fantástica, siendo el resultado un espectáculo de magia cuya visión ha fascinado y entretenido. Personalmente se podría haber logrado mucho más que eso. No está mal como película veraniega o para ver un día cualquiera.
        
            