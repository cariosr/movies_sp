
                      Owen Wilson y Jennifer Aniston están bastante decentes en sus interpretaciones. Pero la que realmente asusta es Kathleen Turner en su breve aparición dónde casi está irreconocible.
 
Es un film disfrutable, de principio a fin, con escenas cómicas entrañables y divertidas, mezcladas con una lectura sobre la vida en común, el sacrificio y sobre como crecemos como personas.
        
            