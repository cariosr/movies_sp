
                      Acabo de regresar del cine (acá en Perú la estrenaron algo tarde). Por el comienzo me esperaba una película de revancha pura al estilo "Dulce Venganza", más sádica y cavernícola, sin embargo conforme si iba mostrando la vida del protagonista que interpreta Robert De Niro mis expectativas fueron hacia algo más psicológico. ¿Qué me deparaba la película a partir de ese momento en el que se cruzan los dos protagonistas por primera vez? Lo que parecía prometedor en un inicio por el calibre de los actores en juego se convirtió en algo monótono, con menos profundidad en cada minuto que pasaba y como muchos dicen, poco creíble. Escenas en las que intercambiaban sesiones de tortura y la búsqueda de un camino a redimir los pecados -que parecía nunca llegar- hacen que la película pierda el peso que anunciaba, específicamente por Robert y John, que son de "larga trayectoria" y en otra cinta hubieran ofrecido mucho más. 

¿Qué es lo BUENO? Paisajes extraordinarios y el personaje serbio que interpreta Travolta, más allá del desenvolvimiento que tiene en la película. La conversación en la cabaña antes de "la caza" resulta de lo más interesante.

¿Qué es lo MALO? La inverosimilitud de varias escenas y ese "dame que te doy" también resulta algo ridículo para mi gusto. Trama simple para lo que se esperaba.

¿Qué nos APORTA? La trama nos hace reflexionar sobre los seres individuales, que pueden resultar diferentes a simple vista pero muchas veces comparten mismos sentimientos de culpa y sufrimiento devenidos del pasado. A veces se necesita ver a esas personas, conocer un poco más su historia para entender la nuestra. La redención se puede encontrar en donde menos la buscas y al final, nos da lo que más deseamos: paz con uno mismo.
        
            