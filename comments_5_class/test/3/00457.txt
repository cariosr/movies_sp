
                      Fantasía distópica sobre la Humanidad y el problema de la superpoblación en un film que empieza de una forma más liviana y se va adentrando en el drama (aunque con pinceladas de comedia). El mayor problema que presenta es que el giro de género resulta algo brusco y se adentra en el trasfondo social de un modo que no acaba de enganchar, resultando por momentos un poco tediosa y perdiendo la frescura inicial
        
            