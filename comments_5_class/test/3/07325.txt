
                      Aunque la idea parte de una premisa original y tiene ese toque que ha hecho de esta serie algo especial y único, el concepto de hacer un episodio al estilo de los libros "Elige tu propia aventura" puede resultar diferente e interesante, pero tras varios visionados, cuando una y otra vez te vuelve al punto de la elección de nuevo, acaba haciéndose repetitivo, dando la sensación de ser un bucle. Las interpretaciones y la ambientación, por lo demás, son notables.
        
            