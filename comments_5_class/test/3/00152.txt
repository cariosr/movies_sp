
                      Jack Black ahora ha aparecido en dos películas espeluznantes este otoño. Pero donde "La casa con un reloj en sus paredes", de Eli Roth, se sintió un poco demasiado oscuro y aterrador para los niños que deberían haber sido su público objetivo, el "Goosebumps 2: Halloween encantado para niños" de Ari Sandel es más problemático en otros lugares.

Black, quien interpretó a R.L. Stine en "Goosebumps" de 2015, reaviva su papel como autor de las populares novelas de terror para niños. En la primera película, los niños usan accidentalmente la vasta biblioteca de Stine para desatar un desfile de monstruos en una ciudad desprevenida.

Aunque Stine es más un personaje de fondo esta vez, la trama es prácticamente la misma. Sarah (Madison Iseman) es una estudiante de último año de secundaria en la pequeña ciudad ficticia de Wardenclyffe, Nueva York, que espera ser aceptada en la Universidad de Columbia. Mientras tanto, se ha quedado atascada viendo a su hermano menor, Sonny (Jeremy Ray Taylor) y su mejor amigo Sam (Caleel Harris), mientras que su madre soltera (Wendi McLendon-Covey) trabaja en el asilo de ancianos local.
        
            