
                      Con varias escenas para el recuerdo, es cierto que la película sorprende por su aire ochentero y su acción más que explícita, pero algunos silencios más que excesivos y el final no me convencen. Una lástima que no tire por el sendero que marcaban los 5 primeros 
minutos de la cinta. Un 6.
        
            