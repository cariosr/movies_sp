
                      Antes que nada quiero aclarar que no vi la versión original de 1987 por lo que no puedo compararla. En esta ocasión se trata de una comedia liviana que saca pocas risas y apunta ineficazmente a valorar la familia por encima del lujo y el confort del dinero a lo Nicholas Cage en "Hombre de familia". Un mensaje noble que no logra conmover y cuenta con actuaciones exageradas. Bastante regularona.
        
            