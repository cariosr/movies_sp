
                      Entretenido largometraje, en la línea clásica, como nos tiene acostumbrados el gran Clint Eastwood, en el que unos pilotos veteranos (interpretados por actores veteranos) tienen que salvar a la humanidad de un satélite ruso. Sin grandes efectos especiales, es una historia de ciencia ficción, que atrae por el factor humano que contiene. Eastwood nos vuelve a llevar por donde él quiere y como él quiere. En resumen: cine clásico con grandes actores.
        
            