
                      Un argumento que va directamente al hueso de la cuestión: la venganza de la protagonista tras la muerte de su hija y su marido. No se detiene en detalles ni en giros complicados. Gran metamorfosis de la víctima (de empleada bancaria a implacable asesina) que no titubea ni por un segundo en tomar justicia por mano propia luego de que la corrupta justicia libere a los autores materiales del hecho. Transmite empatía su drama personal y el desamparo legal que la acecha.
Entretiene y ofrece la acción que uno busca. La recomiendo.
        
            