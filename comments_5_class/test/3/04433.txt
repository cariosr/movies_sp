
                      Esta madrugada he visto la última película de Spielberg, Bridge of Spies porque me llamaba la atención el tema en sí, y por supuesto, la actuación de Tom Hanks, al que no había visto desde 2013 en Saving Mr. Banks (haciendo el papel Walt Disney). 

En sus primeros minutos de metraje, aparenta ser un argumento interesante, basado en hechos reales, que nos desvelará algo nuevo de la historia del espionaje en los años de la Guerra Fría entre EEUU y la Unión soviética. Aparece un personaje en escena, Rudolph Abel, presuntamente espía ruso que tiene información sobre los proyectos nucleares (con la posibilidad de un posible ataque contra los Estados Unidos) de la URSS, y que pasa el tiempo pintando hermosas acuarelas frente al puente de Brooklyn. 

El FBI va tras su pista y finalmente Abel es encarcelado a la espera de un juicio absurdo, ya que la Fiscalía, el gobierno, y la mayoría de los americanos saben cual será el veredicto final. Sorprendentemente, Mark Rylance (en el papel del espía Abel) hace un trabajo que merece un Oscar, con una abnegada pasividad al sentirse acorralado, sin poder volver a su Rusia natal, y aceptando los acontecimientos como van viniendo. 
Y es que Rylance tiene muchas tablas: británico de pies a cabeza, ha obtenido todos los premios de teatro de su país y ha interpretado grandes obras shakespearianas.

Tom Hanks es el abogado de un importante bufete que por razones de integridad personal es escogido para defender al espía ruso Abel, y a la vez la CIA le solicita que haga de mediador, organizando un intercambio entre el espía y un piloto estadounidense arrestado por los soviéticos, y que tiene importante información que haría peligrar los intereses militares americanos. Es un abogado honesto, que por encima de todo, prima su humanidad e integridad hacia el prójimo. 

Spielberg nos lleva hacia el Berlín de principios de los años 60 , cuando se levantó el muro (este año ha hecho 30 años de su caída), que dividió Alemania en dos, y que fue otro episodio trágico para los anales históricos germanos, en una época que también se hacían intercambios de espías y prisioneros por el famoso Puente de los espías.

Por lo demás, la película es políticamente correcta, rozando el sentimentalismo sobretodo al final. La narrativa de filme no es complicada, no ahonda en dar más información que para Spielberg parece que no era necesaria en este filme; es bastante lineal, un poco larga, y en algún momento parece que se ameniza en plenas negociaciones de Hanks y los servicios de inteligencia rusos y alemanes en Berlín, o cuando le roban el abrigo en la Alemania Oriental (se nota el toque de humor negro de los hermanos Coen que han realizado el guión). Totalmente recomendable.
        
            