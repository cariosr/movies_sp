
                      Hay un dilema con esta película, y es que cualquier fan de las películas románticas, verá en esta un referente, pero habrá otras muchas personas (Entre ellas, un servidor) que ven esta película aburrida y larga.
No es una película mala, pero tampoco podría considerarse la obra maestra de James Cameron, simplemente tuvo lo que pedía el público más sentimental e influenciado por las galas de los Oscars, una historia de amor y después un final trágico, que era de lo que realmente trataba esta historia, del hundimiento del Titanic y de las vidas de los que iban a bordo, no sólo de estos dos enamorados.
Si que muestran de buena manera como se diferenciaba en aquella época la clase alta con la clase baja incluso en los cruceros, pero la historia se centra demasiado en un romance prohibido y le quita importancia a otros asuntos u otras historias que podrían resultar interesantes antes de que sucediera el desastre.
Los actores hacen un buen papel por lo general, y resultan creíbles.
La banda sonora es de las mejores que se han hecho, eso es innegable. 
La pelicula consigue que tengas que sacar en pañuelo para secarte las lágrimas de los ojos en ciertas escenas, pero en su mayoría resulta demasiado larga, y sobraría trama que se podría haber obviado o resumido de otra manera. 
En resumen, es una película para ver alguna vez, pero no resulta realmente fluida ni demasiado interesante, sino que se hace pesada en ciertos momentos, y sólo al final consigue mantenerte en tensión o emocionarte, el resto es solo un muy extenso largometraje de amor, de los que nuestras abuelas veían después de comer
        
            