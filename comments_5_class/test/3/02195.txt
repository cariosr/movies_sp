
                      Para empezar, es mejor qe la 2a parte, aunqe es cierto qe es una película qe abría qe valorar por partes,
 debido a sus virtudes y carencias. Virtudes: visualmente ablando es espectacular, efectos muy buenos y acción a raudales. Carencias: guión, repetitivo e inverosímil; actuaciones, en su mayoría deprimentes (excepto turturro y duhamel); fantasía, desbordada; fidelidad a la serie, inexistente; duración, un poco excesiva; y recurso de la épica del héroe americano, muy excesiva y pedante. En resumen, si SOLO quieres entretenerte un rato, esta es tu película, pero si qieres algo mas, ni siqiera lo intentes.
        
            