
                      El Yeti existe, pero no son los abominables. Sin embargo, son enormes y peludos, viven cerca de la cima de una enorme montaña sobre las nubes en el Himalaya.

La sociedad Yeti está gobernada por los Stones, no esos Stones, aunque el Yeti que comienza a cuestionar las historias que les han contado no está recibiendo mucha satisfacción.

En una leyenda solo un poco más extraña que la que rodeaba el nacimiento del cristianismo, los Yeti creen que fueron creados a partir de la culata del Gran Yak, y que el sol es en realidad un Gran Caracol que se mueve lentamente a través del cielo. Para despertar al Gran Caracol, y asegurar su progreso continuo, cada mañana el senior Yeti Dorgle (Danny DeVito) es catapultado a través de varios cientos de pies de cielo abierto, con su cabeza ahora aplanada apuntando directamente (¿redondeado?) A un gong gigante.
        
            