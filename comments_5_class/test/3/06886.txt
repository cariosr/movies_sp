
                      Ha llegado lo último de Guy Ritchie, en esta ocasión una historia de espías ambientada en plena guerra fría, donde la K.G.B. y la C.I.A. se ven obligadas a colaborar para detener a un enemigo común. Se trata de Operación U.N.C.L.E.

En esta esta ocasión ha contado con Henry Cavill, Armie Hammer, Alicia Vikander y Elizabeth Debicki en los roles principales.

Parte de una historia sencilla de espías tradicional y Guy Ritchie sabe dotarla de unos peculiares encuadres que cambian constantemente, una fotografía excelente y sobre todo una ambientación estupenda, donde el vestuario, los vehículos y el entorno te transportan de lleno a la mitad del siglo pasado, a la época del telón de acero. La película combina geniales escenas de acción con toques de humor que surgen en el choque de diferentes personalidades de nuestros espías protagonistas. Destacar también algunos momentos donde la acción se presenta en multipantalla

El director ha querido dar un toque característico a la película dotando a todos los personajes de un elegante estilismo en su vestimenta en todo momento, y con constantes referencias a las marcas y al mundo de la moda, lo cual hace que nuestros protagonistas luzcan un aspecto impecable en todo momento.

Acompaña la historia una potente banda sonora genial en todo momento a cargo de Daniel Pemberton.

Todo lo anterior la convierten en una buena película que resulta entretenida, pero no va más allá de eso. Hay un buen reparto de actores pero no consiguen la complicidad necesaria para que triunfe del todo la historia. 

Armie Hammer hace bien su papel de ruso con problemas de auto control, pero le falta el toque de rudeza creíble para su personaje que para mi resulta excesivamente guapo para su papel, ya que no tiene por que ser un personaje atractivo y en ese aspecto no me encaja. Henry Cavill si que hace de americano guapo, confiado y seductor y está acorde con su personaje aunque choca que a la vista esté mucho más cachas que el ruso pero no esté a su altura en las peleas. Alicia Vikander luce con mucha soltura el estilismo y los vestidos de la época, y Elizabeth Debicki da el pego como villana, pero creo que ambas están desaprovechadas, centrando demasiado la película en los dos protagonistas y su relación dejando un pelín de lado a las secundarias, así como la motivación del villano y deberían explotarlo más. Añadir también que algunos giros de guión están cogidos con pinzas para hacerlos encajar en la historia.

A pesar de todo esto la película vale la pena verla y no os aburrirá en absoluto, simplemente que no irá más allá y pasará a ser una película más del montón.

Como siempre esta es mi opinión y con esa sensación salí del cine. Todo es discutible y opinable.
        
            