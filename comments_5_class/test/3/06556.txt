
                      Sin dudas esta película es una buena propuesta para los más pequeños.

Ingeniosa, divertida y rica en contenido. 
La película tiene un buen argumento acompañado de una excelente animación con increíbles escenarios. El apartado técnico no tiene nada que envidiar a las mas recientes producciones. Los personajes estan bien desarrollados y la historia es ingeniosa, simple de entender pero a la ves muy compleja. 

En resumen una entretenidilla propuesta de Pixar para los más pequeños.

Nota:4,5
        
            