
                      Si bien la película de Zemeckis tiene esa trepidancia tan característica de sus trabajos, a la par que la impregna de una bonita ambientación navideña (lo que la hace un producto familiar estupendo para esas fechas), hay que decir que con los años se le notan las "costuras", y desde el punto de vista técnico no ha envejecido bien, máxime si comparamos con otras películas de animación contemporáneas a Polar Express. De todos modos tiene ese "algo" que la hace mágica y diferente y que la ha convertido en un clásico de las Navidades.
        
            