
                      Buenas actuaciones, fotografía y dirección. Trama interesante hasta que se rompe en la última media hora y lo que pretende ser un final de antología, lo pierden con base en una intención desmedida de sacudir emocionalmente al espectador, lo cual no se logra, haciendo que mucha gente pierda el hilo la historia. Como toda apuesta importante por lograr un final climático, hay el riesgo de ganarlo todo o perderlo todo. Siento que en este film, perdieron la apuesta.
        
            