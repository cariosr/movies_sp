
                      Y todo comienza con un nugget de pollo, jajajaja.
Película que no hay que tomarse muy en serio, y creo que es muy del montón. La película cuenta con el equipo de guionistas responsables de aquella serie musical tan conocida y aclamada, hablamos de "Glee" y también la famosa saga de películas de "Saw". Todo funciona bastante coral, hay muchos personajes que van pasando por una historia un poco de adolescentes. Es terror, pero terror light y cómico, pero a su favor está bien logrado. Muchos gags y chistes, algunos con mas gracia que otros. Tiene su puntito y a medida que avanza su ritmo se anima y va muy fluida; con aires ochenteros, con esa música y esos efectos sonoros que garantizan que el espectador esté atento a lo que pasa, pero con todo esto que te cuento, también digo que no es gran cosa; guión con toques de humor para darle frescura, pero que en su mayoría absurdos. Y buena cantidad de casquería, mucha sangre y visceras. Os diré que Elijah Wood no me dice demasiado, lo he visto en otras muchas películas y como que no me despierta interés; aquí es Clint Hudson, escritor que está escribiendo una novela de terror y además profesor sustituto.  
La película entretiene y punto, no hay que darle importancia, pero a mi no me ha gustado demasiado; tiene bastantes fallos y se ven conforme lo vamos viendo.
FRASE:  
Yo me escabulliré y me iré por donde no haya ningún niño
Ohhh, te escabullirás como un pequeño Hobbit.....
(jajajajaja, esto le dice el profesor de gimnasia a Elijah Wood)
        
            