
                      Típica comedia americana de grupo de mujeres en el que destacaría la buena química entre Mila Kunis, Kristen Bell y Kathryn Hahn y en la que desde un prisma totalmente en tono divertido se hace un retrato de la dificultad de ser madre trabajadora y todo lo que ello conlleva. Aunque acaba cayendo en topicazos (como sucede frecuentemente en este tipo de películas), es lo bastante divertida y original para al menos pasar un buen rato
        
            