
                      Reconozco que Pablo Berger ha sido valiente y arriesgado, capaz de volver a la crueldad del relato clásico y alejarse de los cuentos de nuestra infancia, pero yo sigo viendo una película que vuelve a sacar a la luz las historias de siempre, plagadas de tópicos sobre la rancia España de charanga y pandereta (o tonadilleras y toreros). Me hubiera gustado más que en lugar de Blancanieves hubiera seguido siendo Carmen Villalta.
        
            