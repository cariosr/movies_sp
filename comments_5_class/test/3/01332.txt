
                      La gran falla de "Spider-Man 3" es que no me distrajo de lo que es un savia Peter Parker. Se prolonga tanto tiempo durante el dopey romance entre Peter y la sufrida Mary Jane que me encontré haciendo la pregunta: ¿Se podría hacer una película completa sobre la relación entre estos dos veinte años? Y mi respuesta fue: No, porque el público de hoy nunca aceptaría un héroe tan despistado y una heroína tan dócil. ¿Y no es un poco inusual proponer matrimonio después de compartir solo un beso, y el de la película anterior, y al revés?
        
            