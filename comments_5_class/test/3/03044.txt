
                      Una comedia ligera francesa que cuenta la historia de una marroquí que vive del mundo de la moda en París que reniega totalmente de su origen, que es expulsada de la Capital del Amor y descubre muy a su pesar en su Marruecos natal su cultura y costumbres. Película sin ninguna pretensión más que la de entretener, muy bien hecha, son unos expertos los franceses haciendo cine; muy bonita la banda sonora con planos secuencia.
        
            