
                      Nos encontramos ante un David Cronenberg más clásico y austero que de costumbre, y seguramente más aburrido y tedioso, por lo menos para mí gusto. El largometraje está basado en la obra teatral “The thalking cure” de Christopher Hamptón que también firma el guión. Que aborda la relación a tres bandas entre un doctor y su paciente, y el doctor y su mentor.
En cuanto a los actores están todos sobrios y correctos, a excepción de Keira Knightley, que sobreactúa exageradamente, Viggo Mortensen esta irreconocible en el papel del psicoanalista Sigmun Freud, pero yo me quedo con el breve pero no menos importante trabajo de Vicent Cassel como el libertino Otto Gross.
A destacar las localizaciones, la fotografía y la Bso. Quizás sea necesario un doble visionado para poder apreciar la magnitud de la obra, pero en un primer visionado a mí personalmente me ha dejado indiferente.
        
            