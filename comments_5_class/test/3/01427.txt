
                      Teniendo en cuenta de las elogiosas críticas que había leído sobre este filme antes de verlo, me quedé un poco decepcionado. El montaje es un poco caótico, los flashbacks no están demasiado bien identificados ni bien gestionados, y la mezcla final es harto difícil de digerir. El tercer tercio de al película remonta un poco el nivel con la aparición de Cristiano, el medio-hermano "extraíble" y algunas escenas que aportan un poco de emoción a esta historia  totalmente plana.
        
            