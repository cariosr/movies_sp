
                      Un espíritu que se aloja detrás de un espejo y manipula a la débil protagonista para apoderarse de su cuerpo. Es suspenso adolescente medio pelo. No te va a aportar demasiado ni te va a sobresaltar, solo algo de entretenimiento con la metamorfosis protagónica dentro de su nefasto entorno y un final muy previsible del cual ya te van dando pistas desde el comienzo. Regular.
        
            