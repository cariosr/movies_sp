
                      Como Raimi mostró con Spider-Man cuál es la forma correcta de transferir un superhéroe a la pantalla grande, por lo que Bryan Singer dio lecciones sobre cómo tratar a un equipo de superhéroes. Obviamente ese día Tim Story había hecho un corte. 

De lo contrario, lo habría sabido, sin importar lo fascinantes que sean las escenas de acción, y lo impresionantes que son los efectos de los superpoderes de los héroes, no es posible hacer una diferencia si su guión no cumple con algunas condiciones específicas.

Aunque Fantastic Four es el cómic más antiguo de Marvell, y más o menos todo el mundo sabe, o al menos comprende el título, que este es un grupo de cuatro personas que obtienen fantásticos poderes, me parece lógico que el director y sus guionistas quieran dedicar un poco de tiempo a construir a sus superiores, no solo para tener sus fundamentos cinematográficos, sino también para hacer que las producciones trabajen para las secuelas más fáciles. Pero esto no es lo mismo que perder el tiempo en diálogos superficiales sobre si los héroes quieren o no sus superpoderes, mientras que deberían estar fuera y probarlos explotando cosas. O evitar que otros exploten. Tambien

Cuando su guión se gasta en mochilas infantiles, no tiene tiempo para absorber su maldad con asombro y terror, y tiene una historia algo compleja para justificar sus tendencias creadas por el hombre. McMahon hace lo que puede con el papel del Dr. McMahon. Doom, pero cuando los guionistas te tienen en el ojo, ¿cuánto puedes hacer también ...
        
            