
                      Mickey ojos azules es una comedia romántica británica dirigida por Kelly Makin.

Michael Felgate (Hugh Grant) trabaja en una casa de subastas de arte. Cuando decide casarse con su novia Gina (Jeanne Tripplehorn) tras tres meses de noviazgo, su vida cambia por completo. Al conocer al padre de Gina (James Caan), descubre que es un capo mafioso. A partir de ahí, Michael se verá envuelto en una serie de líos que él no ha buscado.

En la película se recogen muchos de los estereotipos de películas de gansters, y lo intentan llevar todo a la comedia. El resultado es bueno, pero podría haber dado mucho más de si.
La historia empieza muy fuerte, con un hilarante Hugh Grant, presentando los cuadros que ha de subastar. El nivel va bajando conforme la película avanza, y termina con un final muy previsible y nada original.

Lo mejor del filme es, sin duda, la primera parte, y Grant y James Caan.
        
            