
                      Magnífica película. Interpretación genial de Leonardo di Caprio, se la puede catalogar de dura, inmoral, fuerte, etc.,  que lo es,  pero q responde con gran fidelidad a la biografía de Jordán Belfort un broker neoyorkino de agitada, por llamarla suavemente, vida.  La larga duración del film no es obstáculo para que estés atento permanentemente a la pantalla y sólo en algún instante seas consciente de ello. El grupo de actores y actrices q apoyan a di Caprio no desentonan en esta gran producción magistralmente dirigida por Scorsese.
        
            