
                      Después de sorprender a propios y extraños con 'Prisoners', una película en la que todo, absolutamente todo (guión, actores, realización, banda sonora), funcionaba, el encargado en gran parte de todo esto, Denis Villeneuve, vuelve con una historia sobre el narcotráfico no por poco original menos disfrutable. Emily Blunt protagoniza una película de un suspense tremendo y una tensión constante realmente conseguida (a destacar la escena del atasco y el tiroteo bajo tierra), acompañada de un discreto Josh Brolin y un carismático Benicio Del Toro en un papel, sí, muy tópico y predecible, pero defendido con tremendo oficio.

'Sicario' consigue trascender todos sus fallos en gran parte gracias a la energía del realizador, que consigue que lo pasemos realmente mal durante las dos horas que dura sin apenas mostrar sangre: sólo disparos que suenan realistas y ensordecedores, y algunos de ellos ni siquiera con un blanco visible. Una pasada.
        
            