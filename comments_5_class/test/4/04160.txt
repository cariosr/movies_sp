
                      Alicia a través del espejo, es una película de fantasía dirigida por James Bobin y producida por Tim Burton.

Alicia (Mia Wasikowska) vuelve a El País de las Maravillas para ayudar a su amigo El Sombrerero Loco (Johnny Depp), para ello tendrá que hacerse con la cronoesfera y viajar al pasado.

Esta cinta es mejor que su antecesora, mucho más entretenida y amena. Sacha Baron Cohen es el principal responsable de esto, que junto a Helena Bonham Carter forman una pareja fascinantemente extravagante.
Johnny Depp, irreconocible debajo de la piel de su personaje, realiza una actuación intachable, como de costumbre.
Por otro lado, se agradece el que personajes como el de Anne Hathaway aparezcan menos tiempo en pantalla.

Otro de los puntos fuertes son los momentos cómicos, en especial el de la escena del té con el Sombrerero y el Tiempo.

Los efectos especiales son bastante buenos, y junto a la mágica banda sonora de Danny Elfman, el maquillaje y el exquisito vestuario, crean un ambiente que logra envolver al espectador y cautivarlo.
        
            