
                      Una película recomendable. Ya se sabe cómo son las de este actor 'Transporter'. Acción con un buen argumento. A veces sorprende la capacidad de recuperación que tiene a los balazos, cuchillazos y golpes pero es una buena película. No se 'flipan' demasiado como en otras y te tiene enganchado hasta el final.
        
            