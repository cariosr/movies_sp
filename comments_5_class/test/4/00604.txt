
                      Como si los horrores de la guerra no fueran lo suficientemente aterradores, el productor J.J. Abrams enfrenta a un escuadrón de inexpertos paracaidistas de la segunda guerra mundial contra un grupo de científicos nazis y sus creaciones de muertos vivientes mutantes. Inyectando una clásica aventura de hombres en una misión con un cóctel viscoso de horror cronenbergiano, Overlord se reanima, la historia revisionista en su mejor momento empapado de sangre.
        
            