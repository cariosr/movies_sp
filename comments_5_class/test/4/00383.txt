
                      Cry-Baby es una película musical dirigida por el estadounidense John Waters, director también de Hairspray.

Cry-Baby (El lágrima) es una parodia de otras películas ambientadas en la época de los 50, como la citada Hairspray y Grease, donde el instituto se divide en dos pandillas. 

El protagonista es un joven Johnny Depp, que muestra una vez más lo polifacético que es.
La banda sonora, en su mayoría rock 'n' roll, es original y muy pegadiza.

Respecto a la interpretación, en general, todos los actores tienden a sobreactuar. Esto, teniendo el propósito de ser cómico, le quita encanto a la película.

El director no pareció molestarse en que algunos efectos especiales, como la constante lágrima en el ojo de Cry-Baby, resultaran poco reales.

Se le podría haber sacado mayor partido a esta película si la parodia no hubiese sido tan exagerada.
Sin embargo, ha de constar que el vestuario y los escenarios están bien elegidos, y a pesar de los exagerados personajes que creó el director, siguen siendo entrañables.

Otro punto a favor es el argumento, que aún siendo parodia de otras películas, tiene su propia historia.
        
            