
                      'Mad max' es de esas películas que amas u odias, a muerte, sin término medio. Acción desbocada, excentricismo, caos, diálogos previsibles, trama simplona, adrenalina... Puro entretenimiento. En resumen, un film no acto para todos los públicos, pero que sabe muy bien a la audiencia a la que se dirige, y para ellos ofrece todo lo que piden.
        
            