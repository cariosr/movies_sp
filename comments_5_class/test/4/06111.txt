
                      Empezar diciendo que nada más lejos de mi parecer la nominación de Gabriel Byrne a los Premios Razzie como Peor Actor Secundario en 1999 me parece subrrealista, es un actor que le da seriedad a su personaje aunque quizá las comparaciones con otros en este mismo sentido de la trama; pueda deslucir un poco su interpretación.

La película refuerza un aspecto que ya hemos visto en anteriores cintas sobre esta temática (la religión de su protagonista) y que si bien no son novedosas, siempre logra atraer al público amante de las posesiones y los misterios ocultos detrás de esos fenómenos que siempre podrán darnos grandes satisfacciones como flagrantes decepciones a los considerados amantes de este género entre los que me incluyo.

La forma de realizar las secuencias dentro de la normalidad existente en la vida de un poseso, se refleja adecuadamente en la cinta. El recorrer del camino de un ser humano en esas situaciones no se puede antojar más aterrador dada la alteración que sufren en su psique incluso llegando a pensar en la locura como producto de este tipo de fenómenos, algo que lejos de cualquier tipo de religión, tiene un componente psicológico demostrado al ser las personas influenciables ante cualquier estimulo en su cerebro. Esta sería una parte de lo que entendí de la cinta más allá de cualquier apreciación o creencia que pueda tener en una materia que tanto me apasiona.

La conjunción de los personajes me pareció correcta. Arquette hizo en mi opinión uno de los mejores papeles que le recuerdo. Llevo de forma creíble la historia obviando claro está, que no es una de las afamadas actrices dentro de la industria. Sobre Byrne, ya empecé la reseña hablando de él así que poco más podría decir. Ambos me llevaron a disfrutar con sus aciertos y fallos, de un film lleno de muchos matices en el tema de las posesiones.
En definitiva, buena película.
        
            