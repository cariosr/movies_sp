
                      Tal y como imaginaba, la fórmula "Purga" sigue convenciendo. Mas cuidada, mas elaborada, mas currada, mas bestia... En conclusión: Va ser difícil seguir convenciendo con mas purgas, siempre y cuando no nos sorprendan con guiones realmente buenos. La cuestión es que en esta tercera entrega se pasa bastante bien.
Recomendable.
        
            