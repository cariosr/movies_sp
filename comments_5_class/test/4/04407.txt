
                      Las películas de Amat  Escalante no aburren, son estimulantes y siempre hablan de Méjico. Por lo menos de la sociedad mejicana.
Que sean películas impactantes, violentas, que los hombres carguen con su papel de macho y las mujeres vivan una vida ajena, aunque sea la suya, ya es lo habitual en su cine.
Él lo único que hace y es mucho, es cargar las historias de fuerza arrolladoramente visual y expresiva, introducir algún ingrediente extraño, como se hace en las comidas, para potenciar el “sabor” del plato y una libertad total para hilar los sucesos. Sin ninguna barrera.
Aunque parezca mentira Amat Escalante mima al espectador, aunque lo mime para ponerle delante lo más sórdido que uno se pueda imaginar.
Esta película más simbólica que “Heli” y menos reivindicativa que “Los bastardos” y por lo tanto menos acomodada al hilo de la actualidad latente mejicana es sin embargo más reflexiva. No para en un momento determinado de la historia de Méjico, si no que la abarca, contempla su lugar en el mundo.
El estrambótico extraterrestre es una excusa que pone sobre la mesa las carencias, los tabús, que como país ha arrastrado Méjico hasta el momento actual.
Para eso sirve el arte, para que el artista traduzca lo innombrable, lo raro, lo malévolo al lenguaje de la emoción y los sentimientos.
Toda esta intensidad narrativa de Amat Escalante oculta o deja en la sombra lo que viene a ser la técnica cinematográfica: Interpretación, guión, fotografía, música, ritmo, armonía, etc., etc. Está por encima de todo la historia. Por eso ciertas inexactitudes, ciertas imágenes, las interpretaciones planas, la falta de guión, no se tienen en cuenta a pesar de estar ahí. Cine de autor con todas sus consecuencias.
Por ahora no hay que perderse ninguna película de Amat Escalante. Por ahora. Todo cansa.
        
            