
                      Excelente retrato de una situación realmente trágica pero tratada con una sencillez, humanidad y naturalidad que realmente te atrapa y te emociona sin caer en la lágrima fácil. El trabajo de Darín y Cámara y la química que se palpa es soberbia. Ni cae en la comedia negra ni en la tragedia, temas que va bordeando de principio a fin, pero que el director sabe plasmar haciéndote esbozar una sonrisa o emocionándote con mucha normalidad y sutileza
        
            