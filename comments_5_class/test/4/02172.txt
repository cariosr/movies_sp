
                      Happy Thank you More Please es una comedia dramática dirigida, escrita y protagonizada por Josh Radnor, director de Amor y Letras.

La película narra tres historias paralelas. La primera es sobre Sam, un escritor que se encuentra con Rasheen, un niño que se separa de su madre en la estación del metro.
La segunda historia narra los problemas con los hombres de la mejor amiga de Sam. Y la última es sobre cómo la relación entre Charlie y Mary se ve amenaza por una oportunidad de trabajo de él.

Es la primera película de Josh Radnor como director y guionista, y el resultado es más que sobresaliente.
Los diálogos son inteligentes, rápidos y con un profundo sentido. La comedia, el drama y el romanticismo se mezclan de una manera exquisita. 
Además. el reparto de actores está muy bien seleccionado.

La película ganó dos premios; a la mejor película, y el premio otorgado por el público.
        
            