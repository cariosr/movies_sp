
                      Animales Nocturnos (2016).
El poco prolífico Tom Ford dirige (con un estilo que, se me antoja, recuerda al célebre David Linch) y guioniza esta cinta basada en una novela, y en la que encontramos tres historias entrelazadas: la de la protagonista, una mujer casada y con todo a sus pies, pero infeliz, encarnada por la fantástica Amy Adams; la del pasado de ésta con su primer marido (un novelista inédito interpretado por Jake Gyllenhaal); y la de la novela que este mismo le envía para su lectura y donde también aparece él como protagonista.

A nuestros primeros espadas los acompañan varios secundarios conocidos, como el británico Michael Sheen o el imponente nuevo "Llanero Solitario" Armie Hammer como el segundo esposo de Adams. Pero el que se sale de sus registros habituales de "chico bueno" es Aaron Taylor-Johnson, quien nos muestra su lado más desagradable, escena escatológica incluida, demostrando su valía en este cinematográfico mundo.

La película, rarezas aparte, es interesante y dura, pero como se suele decir: "bien está lo que bien acaba", y el final de esta historia no gustará mucho a bastantes espectadores. 
Merece tenerse en cuenta el maquillaje y caracterización de los personajes principales. Es de lo más sutil y bien realizada. Laura Linney está casi irreconocible, pero repito; de manera muy sutil.

No me atrevo a recomendarla, pues aunque tiene sus momentos, no es un drama al uso, y no es fácil de visualizar. Además, la crítica la califica con un notable, y eso siempre me ha dado miedo...

Raúl Cabral.
        
            