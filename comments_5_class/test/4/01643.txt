
                      Una película distinta de las demás cintas de '' Zombies''  , en cuanto al curso y sentido de la historia. La trama se centra en una serie de conflictos que inevitablemente ocurriría entre un grupos de personas que se vieron afectadas por sucesos que marcaron su vida durante este mundo post-apocalíptico,y como estos logran superar esos problemas ante la presencia de un peligro que amenaza la supervivencia de los personajes; En fin es una película interesante con buenas escenas de suspenso y un final de acción decente, sin embargo fallos en la explicación de ciertos sucesos en la historia y la actuación poco deseable de cierto actor dejan mucho que desear de esta película.

Yo personalmente la clasifico como una película para pasar un buen rato de terror y suspenso.
        
            