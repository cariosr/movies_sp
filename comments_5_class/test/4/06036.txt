
                      Sinceramente, Ant-Man estuvo bien y eso, pero no conquistó mi corazón. Sin embargo, ésta lo ha conseguido. Los efectos han mejorado mucho, llegando a la altura de lo que se espera de Marvel. Es una historia ligera, familiar y de acción con puntos cómicos muy buenos y la mejor escena post-créditos de todo el universo de Marvel. Si son fans de Marvel y creen que pasarán de esta película, NO LO HAGAN, se arrepentirán.
        
            