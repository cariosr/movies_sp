
                      Una película de cáncer de mierda de un libro de cáncer de mierda. Se honesto, eso es lo que estás pensando. Es fácil prejuzgar cuando se trata de The Fault in Our Stars, la versión cinematográfica del best-seller de jóvenes adultos de 2012 de John Green sobre un Romeo y Julieta de la actualidad, ambos protagonizados por el Big C.

Resulta que La falla en nuestras estrellas no es una porquería en la página o en la pantalla. Green hizo la sabia elección de ser gracioso al contar su triste historia. Y la película, dirigida por Josh Boone a partir de un guión ingeniosamente matizado por Scott Neustadter y Michael H. Weber, de (500) Days of Summer, hace lo mismo.
        
            