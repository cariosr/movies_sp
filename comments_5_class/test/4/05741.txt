
                      Intentar hacer una película que triunfe en las salas y en las críticas usando un género un tanto fuera de fecha se nos antojaba, inicialmente, un poco quimera, pero os tenemos que reconocer que Damien Chazelle lo ha conseguido con "La La Land" gracias a una magnífica BSO, un trabajo brutal de Emma Stone y a cierta distancia, Ryan Gosling. 

A nosotros nos ha gustado, aunque os debemos asumir que las 14 nominaciones a los Oscar, nos parecen algo excesivas. Más detalles en nuestra crítica.
        
            