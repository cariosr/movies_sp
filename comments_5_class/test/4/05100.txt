
                      Está demasiado apañada, con soluciones finales que resultan claramente forzadas en aras a buscar el lado feliz de todos los problemas, pero aun así no es una película mediocre y hasta hay que reconocerle algunos méritos tratándose de la opera prima del director ,el terrorismo y las consecuencias que para un terrorista conlleva tener que vivir unos días con las personas que van a ser sus víctimas.
        
            