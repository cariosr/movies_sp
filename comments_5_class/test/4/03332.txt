
                      Una forma diferente de ver a los vampiros de siempre. Con historia destacable que mezcla el drama y el romance, vemos una película que a pesar de no empezar con mucho ritmo, va de menos a mas durante su transcurso. Los dos tiempos de esta cinta (Pasado y presente), representados por una especie de flashbacks, están bien llevados y no desentonan. Yo lo que destacaría seria la fotografía. Muy lograda por las localizaciones y la oscura atmósfera que no pueden faltar en una película de vampiros.
        
            