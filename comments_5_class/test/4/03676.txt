
                      Con una fotografía y un corte que podrían recordar a films como "300", las aventuras de supervivencia que se nos muestran en Alpha resultan entretenidas y es visualmente bonita. No se nota demasiado abuso del cgi en general y la acción es muy correcta a grandes rasgos. Es una película muy disfrutable para ver en familia y los escenarios son visualmente muy potentes.
        
            