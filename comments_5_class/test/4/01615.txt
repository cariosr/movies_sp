
                      Daniel Espinosa nos trae Life, película que se nota bastante que tiene muchas referencias a el Octavo Pasajero, y aún teniendo la misma estética, acaba desmarcándose un poco, no esperaba mucho de ella, pero que me ha agradado gratamente.

El guión, pese a no ser original ni novedoso, cuenta con muy buena premisa centrándose en la vida extraterrestre, la historia esta bastante bien donde nos explican desde primera mano como consiguen una célula procedente de Marte, y como logran reactivarla, dándole el nombre de Calvin, haciéndose cada vez más grande hasta ser un organismo pluricelular, mostrando gran inteligencia como gran violencia, cuenta con muy buen ritmo y cada vez va a más.

la trama es muy simplona y prácticamente deja muchas dudas, sobre todo con el extraterrestre, ya que van sucediendo cosas pero no se explica ninguna de ellas, a destacar cabe la gran estética, como al estar encerrado en el espacio y manteniendo bastante bien la tensión más el añadido de terror que contiene, aunque es poco, pero lo suficiente para crear buenas muertes. Los efectos son impresionantes, la fotografía es muy buena, el montaje esta bien, los escenarios son espectaculares y la música acompaña muy bien. El final es bueno, pero se hace algo predecible, y dejándote con ganas de más, y gracias a ello la dejan bastante abierta.

Las actuaciones, es lo que me falla, ya que cuenta con un gran reparto, en primer lugar tenemos a Jake Gyllenhaal, un actor que me gusta bastante, y tengo que decir, que me esperaba mucho más de su persona en esta cinta. Rebecca Ferguson, ha sido el personaje que más me ha gustado, bastante expresiva. Ryan Reynolds, está pasable, aunque es el personaje que menos metraje ha tenido. Por otro lado tenemos a Hiroyuki Sanada, no está mal. 
Olga Dihovichnaya, correcta. Y Ariyon Bakare, está bastante bien.
En definitiva, buena película.
        
            