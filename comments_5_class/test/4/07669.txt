
                      Podía haberse desarrollado la historia en una familia de abogados, de médicos o de comerciantes pero se desarrolla en una familia de delincuentes y desequilibrados porque la tesis que muestra la película debía cumplirse y además en las condiciones más desfavorables y poco atractivas.
Se ha clasificado la película como un drama- thriller, y lo es, pero también un ejemplo de estudio antropológico de lo poderoso que es el sentido de pertenencia y de tribu.
David Michôd escribe el guión y dirige la película con claridad meridiana sobre un tema que aparentemente ya se ha tocado en bastantes películas, por ejemplo la estupenda “Down Terrace” de Ben Wheatley. En la misma campaña comercial se dice que es la respuesta australiana a “Uno de los nuestros” de Scorsese, y tiene parecido pero no es lo mismo.
Lo que diferencia este film de otros semejantes en cuanto al universo en el que transcurre es que aquí hay una línea narrativa poderosa que no queda subyugada a la acción y lo que acaece, si no que es la viga maestra que sostiene lo que sucede.
Un joven pierde a su madre por sobredosis y al quedarse solo vuelve a casa de su abuela, donde ésta, fantástica la interpretación de la actriz en su papel de matriarca, lucida y resignada, le da cobijo en su hogar en el que viven también sus tios que se dedican a los variados trapicheos, incluido el asesinato. La policía anda tras ellos.
A partir de ese momento se le planteara al joven la disyuntiva de qué lado escoger, si el regido por las reglas y las normas legales que le permitirá tener una existencia dentro de la sociedad, o el otro de la tribu familiar, a la que pertenece y donde tiene su origen.
David Michôd no cesa de ponerle obstáculos en un lado y facilidades en el otro, en un intento de que triunfe el buen sentido, pero al final se cumple la tesis y el sentido de pertenencia a través de unos hechos terribles, fríos y sangrientos acaba triunfando en un final que hiela la sangre.
Un auténtico reino animal que acaba o que sigue su curso con el consabido “un nuevo macho, joven y poderoso ha llegado”.
De lo mejorcito que he visto en películas sobre familias criminales.
        
            