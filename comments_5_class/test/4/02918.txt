
                      Resulta la película más entretenida hasta el momento de la saga del hijo de Odín, aderezado en esta ocasión con tintes más cómicos, que la acercan a filmes como Guardianes de la Galaxia, dando matices más ricos al personaje principal y al resto de secundarios. Añadimos mejores efectos especiales y el resultado es la mejor (hasta el momento) entrega de la trilogía
        
            