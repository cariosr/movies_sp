
                      Si disfrutaste de Las Ventajas de ser un marginado, no puedes perderte The Spectacular Now. Lo que podría haber sido una película de instituto más, en el que la chica antisocial acaba conquistando al chico malo de la clase, se convierte en una agridulce y profunda reflexión sobre la madurez. La intensidad de la trama y de los sentimientos se intensifica según avanza la película dejándonos en su desenlace con el corazón en un puño. Merece la pena.
        
            