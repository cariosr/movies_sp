
                      1. Voces 6
2. Sonido 10
3. Cinematografía 10
4. Música 10
5. Efectos visuales 10
6. Dirección 5
7.Guión 6
8. Edición  5
9. Originalidad 5
10. Escenografía 10

Calificación: 71/100

La película es un poco ñoña con su chistes, el guión es interesante pero pierde sentido si lo piensas demasiado, no es original y la elección de voces no es de mi agrado, pero la animación es hermosa, los personajes o su diseño es exquisito al igual que los escenarios y las tomas de la película, es por ello que por ser una cinta de niños creo que se salva y es pasable para toda la familia, y si si vale la pena verla.
        
            