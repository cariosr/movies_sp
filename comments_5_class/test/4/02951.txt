
                      El director Jacques Audiard da un giro en su carrera con una tremenda historia de gran emoción y superación entre dos personas muy diferentes. Cada uno de un mundo diferente y con sus propios problemas pero juntos crean algo especial. Cada plano y cada interpretación aporta una simplicidad impresionante con el espectador. Es una historia muy bonita que no defraudará.
        
            