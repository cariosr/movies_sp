
                      Feedback, una película que llega sin hacer mucho ruido, pero que ha superado mis expectativas.
Cierto es que no había leído nada sobre esta película hasta la semana pasada que me encontré con su frenético tráiler que logró captar mi atención.
Sobre la película... No creo que estemos frente a lo que se podría llamar una de las sorpresas del año, ya que ni tiene esas aspiraciones, pero es cierto que no te dejará indiferente.
Se presenta la típica trama en la que tu cabeza comienza a adelantarse a los hechos con el fin de descifrar el final, pero lo curioso es que sus giros inesperados de guión consiguen romperte los esquemas hasta más de una vez.
Los personajes en general son bastante obviables, ya que son presentados de tal manera que a mí personalmente me costó empatizar con ellos. Sólo destacar a su protagonista Eddie Marsan (El inspector de policía en Sherlock Holmes) y la presencia de nuestro queridísimo Paul Anderson (Arthur Shelby).
Por último, mencionar la dirección y el guión de Pedro C. Alonso que consigue sacarle el máximo jugo posible a este thriller violento.
Así que, por mi parte, si te apetece pasar un rato entretenido, Feedback es una buena opción.
FDO: X.A.
        
            