
                      La deliciosa casa de muñecas de Wes Anderson

"Todavía queda un atisbo de civilización en este matadero de barbarie que conocemos como Humanidad", clama el refinado conserje del mítico Hotel Budapest, Gustave H., en más de un momento de la historia. Un personaje y un edificio que representan los buenos modales en tiempo de entreguerras, en un país ficticio enfrentado a una ocupación inminente por parte de una también ficticia facción que avanza por Europa sin cuartel. El fin de un estilo de vida al que se resiste su protagonista con la misma elegancia aristocrática con la que el señor Zorro se oponía ante el avance de Boggis, Bunce y Bean.

Y, a la vez, una frase que supone toda una declaración de intenciones de Wes Anderson. “El gran Hotel Budapest”, bajo su pomposidad, la plasticidad de unos planos que bien podrían pasar por cuadros y que desprenden olor a dulces de Mendl, su carácter de farsa, su hilaridad propia del cine mudo, y su galería de estrafalarios personajes, esconde el sentimiento de nostalgia de un romántico ante una época perdida, en la que sin duda le habría encantado vivir. 

Anderson abandona el encanto infantil de su anterior trabajo y lo cambia por la malicia de un relato de una enorme ligereza argumental pero de una aún más enorme complejidad artística. Habrá quien la acuse de ser demasiado ligera, pero este aspecto lo compensa el cineasta tratando al hotel que da título a la cinta como si de una fastuosa casa de muñecas se tratase. Y, para ser más exactos, como si la misma casa fuera en sí misma una gigantesca muñeca rusa, relatando una historia que en realidad está compuesta de relatos que cuentan otros relatos.

Absurda en su concepción del sentido del humor, pero tratada con la seriedad y cariño suficientes por parte de su autor como para que el conjunto no caiga en el ridículo, “El gran Hotel Budapest” es la enésima constatación de que Wes Anderson sigue utilizando los mismos recursos de siempre, pero que su universo es infinitamente creativo y está en continua expansión. Aquí exhibe un dominio absoluto de su propio universo, haciéndonos atravesar hasta cuatro épocas distintas, narradas con estilos cinematográficos y tonos bien distintos.

Y, como guinde del pastel, un reparto de altura, también marca de la casa, capitaneado por un estiloso, comedido e hilarante Ralph Fiennes. De fondo, la música del nuevamente acertado Alexandre Desplat. Ante la cámara, una galería de personajes que bien merecería un corto para ellos solos. Travellings horizontales, planos cenitales, una obsesión desmesurada por los detalles, un ritmo frenético… En definitiva, una absoluta delicia para los sentidos, y una casa de muñecas con la que queda claro que Anderson se ha divertido de lo lindo. Y nosotros con él.

A favor: es puro Wes Anderson, le pese a quien le pese
En contra: habrá quien la acuse de ser demasiado ligera
        
            