
                      "a good year" es una atípica comedia romántica aparentemente convencional, donde Ridley Scott hace de una historia común, una sutil comedia con impecables toques de humor británico y con un sorprendente trabajo visual.

El film es una afable analogía del amor al vino refiriéndose al amor a la vida y es gratamente interpretada por Russell Crowe y por una Marion Cotillard en su extrema naturalidad en un papel hecho a medida. 
El director nos ilustra con su cálida Provenza y su frío Londres, como nuestra sociedad moderna e industrializada está acabando con la preciada sensibilidad humana. Enseñándonos humildemente a través de los ojos del joven y el adulto Max Skinner, lecciones sobre la vida y como, los placeres más simples, son simplemente los únicos.
        
            