
                      Como es una constante en el cine de su director, nos encontramos ante un guión original y una forma de rodar única, si bien en esta cinta, realmente aterradora, la elección del casting no resulta tan acertada, quedando el trabajo de Mark Wahlberg bastante por debajo de lo que suele ocurrir con los protagonistas de Shyamalan en otros trabajos. Aún así, es una película diferente y original, aunque no llegue estar en el top de los trabajos del magnífico director
        
            