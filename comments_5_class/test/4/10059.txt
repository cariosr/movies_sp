
                      Las siete historias tienen su sello: en esto reside en parte su sabroso correr y es a la vez, su talón de Aquiles. Por esas  diferencias de cada segmento que funcionan como cortos , pasamos de un humor tierno a una notrama algo abúlica , como la historia DIARIO DE UN PRINCIPIANTE, del costumbrismo risueño de AMARGO DULCE, al expresionismo oscuro de EL RITUAL. 
La música acompaña: hay boleros, reagettton, salsa, son; hay religión, santería yuruba, tradición, transgresión, hay Fidel, Kusturica y Guillén.
        
            