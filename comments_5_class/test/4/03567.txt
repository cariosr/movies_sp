
                      Pasan los años, cambian las personas, las compañías, las vidas... pero si esta pandilla te hizo llorar de risa, volverán hacerlo con esa habilidad tan particular de generar situaciones
 absolutamente embarazosas a la par que disparatadamente absurdas. Gran rato, y nostálgica. Un 7,5.
        
            