
                      "La prima cosa bella" hace reír, hace llorar y te engancha de principio a fin. Es una película deliciosa, con una gran banda sonora, que sin duda merece la pena ver.
        
            