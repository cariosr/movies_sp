
                      Una película independiente que me ha sorprendido y mucho, no solo por el tema que trata sino por la gran interpretación de Chris Evans y la pequeña  Mckenna Grace  y la química que hay entre ellos. 
Me ha parecido una película maravillosa llena de valores, toques cómicos y drama que hace que el corazón se encoja y te pone piel de gallina.
        
            