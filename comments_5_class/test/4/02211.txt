
                      Una relación de pareja frustrada a causa de la inacción y el fracaso laboral del marido hasta que un suceso cambia radicalmente la situación de ambos y desata la locura. Atrapante en todo momento, muy buenas actuaciones y un giro argumental notable en el desenlace. La recomiendo.
        
            