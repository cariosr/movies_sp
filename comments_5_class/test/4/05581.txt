
                      1. Voces 10
2. Sonido 10
3. Cinematografía 10
4. Música 10
5. Efectos visuales 10
6. Dirección 10
7.Guión 7
8. Edición  7
9. Originalidad 0
10. Escenografía 10

Calificación: 84/100

Otra película en Nueva York, la ciudad es hermosa pero que no tienen otra bonita ciudad en Estados Unidos, bueno a pesar de eso la animación es hermosa, la elección de doblaje, y todo lo técnico pero el guión se cae un poco y a veces se le olvida que está dirigida a un público infantil.

Aún así la calificó como una buena película, no es una obra de arte pero si un producto que vale la pena ver y disfrutar en familia, sin exigirle un mensaje profundo.
        
            