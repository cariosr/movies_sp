
                      No es la mejor película del año pero tampoco la peor. Lentita y con poca acción, pero la historia vale la pena. El guión igual que el libro. La banda sonora, los momentos de tensión, la actuación, los efectos especiales son GENIALES . Sinceramente ESPECTACULAR. Era tal y como me imaginaba. La única pega que le pongo es... De verdad tenemos que esperar otro año más? Es necesario? :S
        
            