
                      Tengo que decir que me sorprendió mucho, para bien.
 A pesar de que estas viendo durante toda la película lo mismo (prácticamente solo al chico con un móvil y un mechero) no se hace nada pesada, te mantiene constantemente en vilo, incluso consigue transmitir la angustia del hombre, y da algo de claustrofobia. En mi humilde opinión, hay que verla.
        
            