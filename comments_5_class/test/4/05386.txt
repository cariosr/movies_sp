
                      Árboles envueltos en una densa niebla mecidos por el viento, olas que azotan las rocas donde los percebeiros se ganan la vida y se juegan la muerte, arenales de bajamar en los que las mariscadoras escarban su salario, piedras con poderes mágicos en los roquedales de Muxía, donde los creyentes ponen su fe para curar sus huesos enfermos. “Costa da Morte” es un homenaje a la naturaleza, un hermoso canto de vida y muerte, de regeneración ante las adversidades y las catástrofes naturales y humanas.
El joven director gallego nos muestra en su documental inmensos lienzos, hermosas pinturas de la naturaleza de la que surgen sonidos y cobran vida las diminutas figuras humanas que habitan la zona y que forman parte del paisaje. A pesar de la distancia que la cámara establece con ellas, escuchamos claramente sus voces: nos hablan de su vida, de su historia, de naufragios y de las innumerables leyendas que aun se mantienen vivas en la costa más occidental de Europa.

Leer reseña completa en:
        
            