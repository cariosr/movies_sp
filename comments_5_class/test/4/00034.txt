
                      Producción de Guillermo del Toro bastante anterior a Coco y que al igual que ésta trata el tema de la cultura mexicana sobre el mundo de los muertos y todo lo que gira alrededor. Original, colorista, con una animación muy particular, más cercana a los mundos de Tim Burton que a los de Pixar, resulta una película de animación más que interesante
        
            