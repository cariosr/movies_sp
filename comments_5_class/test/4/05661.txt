
                      Una película estupenda de animación con un gran cuidado de los detalles que la convierten en por ahora la mejor película de animación hecha por ordenador. 
Pero además de su calidad técnica me ha gustado la historia, es bien cierto que es una adaptación de un libro, y quizá por eso es muchísimo mejor que otras que cuentan con un guión muy flojo. 
Aquí hay una buena historia detrás, previsible pero hay que recordar que es para niños, y con una gran animación.
        
            