
                      Magnífica aventura, con el tono clásico de Disney (magia y encanto), muchas canciones y muy buenas (esto va directo a un musical de Broadway) y personajes bien reflejados (tal vez el malvado algo flojo, pero bueno...)
Técnicamente es alucinante todo lo relacionado con la nieve (el 90 % del metraje) y, sobre todo, el castillo de hielo.
Eso sí, el corto de Mickey que veremos antes, es imprescindible verlo en 3D.
        
            