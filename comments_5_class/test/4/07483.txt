
                      Una adaptación maravillosa de una novela más maravillosa aún. La película consigue llegar al corazón y está magníficamente hecha. Nada más que se pueda decir porque el resto es para verlo. Disfrutad del buen cine.
        
            