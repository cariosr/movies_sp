
                      Una excelente película de investigación periodística. Lo mejor del desarrollo de la trama es la ausencia de adornos; únicamente se centra en los hechos, y consecuencias, alrededor de la labor del equipo de investigación del periódico The Boston Globe sobre los casos de pederastia sacerdotal en la ciudad y el encubrimiento de las altas esferas. 

Estamos ante una película sobre el poder y la influencia de la Iglesia católica manejada por McCarthy, con el sentido más clásico de la puesta en escena, y también del guion. 

Spotlight cumple con su propósito de olvidar lo superfluo para centrarse en lo principal. Como en el extraordinario discurso del director Liev Schreiber a sus redactores, "la gran historia no está en los curas, como individuos, está en la institución; práctica y política, hay que apuntar contra los males del sistema", la película es una guía profesional para el desarrollo del periodismo.
        
            