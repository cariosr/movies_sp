
                      Una genial ida de pinza, muy del estilo de 'God Bless America'. Te arranca las carcajadas mientras te quedas con cara de "qué narices acaba de pasar". Humor ingles del bueno por doquier y buen gusto por el cine. Y se agradecen las películas en las que los protagonistas no son ideales ni guapisimos, lo hace siempre más verosímil.
        
            