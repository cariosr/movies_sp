
                      Okja es sin duda la mejor película de Netflix hasta la fecha.

Película que nos habla sobre la tierna historia entre una niña y su amigo/mascota Okja, pero esa historia no se queda ahí, tiene un trasfondo que nos habla sobretodo del maltrato animal en la industria alimenticia, con un mensaje ecologista nos hace vivir una dulce trama que se convierte en un drama global, con escenas bastante duras, pero reales, es una película para todos los públicos, no únicamente para niños. Esta hecha de tal forma y puede ser tan real que lo único que cambiarías es por ejemplo a Okja por una vaca y ahí tienes el resultado. Buena película con un gran mensaje.
        
            